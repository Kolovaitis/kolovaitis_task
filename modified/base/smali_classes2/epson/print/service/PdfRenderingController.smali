.class public Lepson/print/service/PdfRenderingController;
.super Ljava/lang/Object;
.source "PdfRenderingController.java"

# interfaces
.implements Lepson/print/service/RenderingController;


# instance fields
.field private mDeque:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mEpsonService:Lepson/print/service/PrintService;

.field private mIsLandscape:Z

.field private mOriginalFilename:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mPdfFilename:Ljava/lang/String;

.field private mPdfRenderThread:Lepson/print/service/PdfRenderThread;

.field private mPrintRange:[I


# direct methods
.method public constructor <init>(Lepson/print/service/PrintService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[I)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lepson/print/service/PdfRenderingController;->mEpsonService:Lepson/print/service/PrintService;

    .line 34
    iput-object p2, p0, Lepson/print/service/PdfRenderingController;->mPdfFilename:Ljava/lang/String;

    if-eqz p3, :cond_0

    move-object p2, p3

    .line 35
    :cond_0
    iput-object p2, p0, Lepson/print/service/PdfRenderingController;->mOriginalFilename:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lepson/print/service/PdfRenderingController;->mPassword:Ljava/lang/String;

    .line 37
    iput-boolean p5, p0, Lepson/print/service/PdfRenderingController;->mIsLandscape:Z

    .line 38
    iput-object p6, p0, Lepson/print/service/PdfRenderingController;->mPrintRange:[I

    return-void
.end method

.method public static getKindStatic()Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    .line 119
    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    return-object v0
.end method


# virtual methods
.method public drawBeforeStartJob()V
    .locals 0

    return-void
.end method

.method public getImageList()Lepson/print/EPImageList;
    .locals 2

    .line 58
    iget-object v0, p0, Lepson/print/service/PdfRenderingController;->mPdfRenderThread:Lepson/print/service/PdfRenderThread;

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Lepson/print/service/PdfRenderThread;->getImageList()Lepson/print/EPImageList;

    move-result-object v0

    return-object v0

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "startDrawAfterStartJob() not called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getKind()Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    .line 115
    invoke-static {}, Lepson/print/service/PdfRenderingController;->getKindStatic()Lepson/print/screen/PrintSetting$Kind;

    move-result-object v0

    return-object v0
.end method

.method getPdfRenderThread(I)Lepson/print/service/PdfRenderThread;
    .locals 12
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 66
    iget-object v0, p0, Lepson/print/service/PdfRenderingController;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0, p1}, Lepson/print/service/PrintService;->getLocalPrinterLayout1AreaSize(I)[I

    move-result-object v9

    .line 68
    new-instance v0, Lepson/print/service/PdfRenderThread;

    iget-object v2, p0, Lepson/print/service/PdfRenderingController;->mEpsonService:Lepson/print/service/PrintService;

    new-instance v3, Lepson/print/pdf/AreaPdfRenderer;

    invoke-direct {v3}, Lepson/print/pdf/AreaPdfRenderer;-><init>()V

    iget-object v4, p0, Lepson/print/service/PdfRenderingController;->mPdfFilename:Ljava/lang/String;

    iget-object v5, p0, Lepson/print/service/PdfRenderingController;->mOriginalFilename:Ljava/lang/String;

    iget-object v6, p0, Lepson/print/service/PdfRenderingController;->mPassword:Ljava/lang/String;

    iget-boolean v7, p0, Lepson/print/service/PdfRenderingController;->mIsLandscape:Z

    iget-object v8, p0, Lepson/print/service/PdfRenderingController;->mDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    iget-object v10, p0, Lepson/print/service/PdfRenderingController;->mPrintRange:[I

    move-object v1, v0

    move v11, p1

    invoke-direct/range {v1 .. v11}, Lepson/print/service/PdfRenderThread;-><init>(Lepson/print/service/PrintService;Lepson/print/pdf/AreaPdfRenderer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/concurrent/LinkedBlockingDeque;[I[II)V

    return-object v0
.end method

.method public interruptSubThreads()V
    .locals 1

    .line 97
    iget-object v0, p0, Lepson/print/service/PdfRenderingController;->mPdfRenderThread:Lepson/print/service/PdfRenderThread;

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {v0}, Lepson/print/service/PdfRenderThread;->interrupt()V

    :cond_0
    return-void
.end method

.method public joinSubThread(J)V
    .locals 1

    .line 104
    iget-object v0, p0, Lepson/print/service/PdfRenderingController;->mPdfRenderThread:Lepson/print/service/PdfRenderThread;

    if-eqz v0, :cond_0

    .line 106
    :try_start_0
    invoke-virtual {v0, p1, p2}, Lepson/print/service/PdfRenderThread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public startDrawAfterStartJob(I)V
    .locals 1

    .line 49
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Lepson/print/service/PdfRenderingController;->mDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 51
    invoke-virtual {p0, p1}, Lepson/print/service/PdfRenderingController;->getPdfRenderThread(I)Lepson/print/service/PdfRenderThread;

    move-result-object p1

    iput-object p1, p0, Lepson/print/service/PdfRenderingController;->mPdfRenderThread:Lepson/print/service/PdfRenderThread;

    .line 52
    iget-object p1, p0, Lepson/print/service/PdfRenderingController;->mPdfRenderThread:Lepson/print/service/PdfRenderThread;

    invoke-virtual {p1}, Lepson/print/service/PdfRenderThread;->createEpImageList()V

    .line 53
    iget-object p1, p0, Lepson/print/service/PdfRenderingController;->mPdfRenderThread:Lepson/print/service/PdfRenderThread;

    invoke-virtual {p1}, Lepson/print/service/PdfRenderThread;->start()V

    return-void
.end method

.method public waitPage(II)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Lepson/print/service/LocalInterrupt;
        }
    .end annotation

    :goto_0
    if-ge p1, p2, :cond_2

    .line 83
    iget-object v0, p0, Lepson/print/service/PdfRenderingController;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v0

    if-nez v0, :cond_1

    .line 87
    iget-object v0, p0, Lepson/print/service/PdfRenderingController;->mDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->takeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 89
    :cond_0
    new-instance p2, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "page = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "] que ["

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 84
    :cond_1
    new-instance p1, Lepson/print/service/LocalInterrupt;

    invoke-direct {p1}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw p1

    :cond_2
    return-void
.end method

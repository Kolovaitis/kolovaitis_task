.class public interface abstract Lepson/print/service/PrintService;
.super Ljava/lang/Object;
.source "PrintService.java"


# virtual methods
.method public abstract createPrintImage(Lepson/print/EPImageList;IIIIIIII)Ljava/lang/String;
.end method

.method public abstract endJob()I
.end method

.method public abstract endPage(Z)I
.end method

.method public abstract epsNotifyError(IIZ)V
.end method

.method public abstract getCancelPrinting()Z
.end method

.method public abstract getLang()I
.end method

.method public abstract getLocalApplicationContext()Landroid/content/Context;
.end method

.method public abstract getLocalPrinterLayout1AreaSize(I)[I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getPrintLockObject()Ljava/lang/Object;
.end method

.method public abstract getPrintableArea()[I
.end method

.method public abstract getPrinterStatus()[I
.end method

.method public abstract getStartJobResolution()I
.end method

.method public abstract initImage(Ljava/lang/String;)I
.end method

.method public abstract notifyEndJob(I)V
.end method

.method public abstract pageSneedRotate(I)Z
.end method

.method public abstract printPage()I
.end method

.method public abstract releaseImage()I
.end method

.method public abstract setCancelPrinting(Z)V
.end method

.method public abstract setPrinting(Z)V
.end method

.method public abstract startJob(IIIIIIIIIIIZ)I
.end method

.method public abstract startPage()I
.end method

.method public abstract updateApfProgress(I)Z
.end method

.method public abstract waitIfSimpleAp()V
.end method

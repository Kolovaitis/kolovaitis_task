.class public Lepson/print/service/LocalPrintThread;
.super Ljava/lang/Thread;
.source "LocalPrintThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/service/LocalPrintThread$LocalEscprException;
    }
.end annotation


# static fields
.field public static final SUB_THREAD_JOIN_LIMIT:I = 0xea60

.field private static final TAG:Ljava/lang/String; = "LocalPrintThread"


# instance fields
.field private gImageList:Lepson/print/EPImageList;

.field private final mEpsonService:Lepson/print/service/PrintService;

.field private mIsBkRetry:Z

.field private final mKind:Lepson/print/screen/PrintSetting$Kind;

.field private final mRenderingController:Lepson/print/service/RenderingController;

.field private final printingLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lepson/print/service/PrintService;Lepson/print/service/RenderingController;Z)V
    .locals 1

    const-string v0, "print-loop"

    .line 35
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    .line 38
    iput-object p2, p0, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    .line 39
    iget-object p1, p0, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {p1}, Lepson/print/service/RenderingController;->getKind()Lepson/print/screen/PrintSetting$Kind;

    move-result-object p1

    iput-object p1, p0, Lepson/print/service/LocalPrintThread;->mKind:Lepson/print/screen/PrintSetting$Kind;

    .line 40
    iput-boolean p3, p0, Lepson/print/service/LocalPrintThread;->mIsBkRetry:Z

    .line 41
    iget-object p1, p0, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {p1}, Lepson/print/service/PrintService;->getPrintLockObject()Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lepson/print/service/LocalPrintThread;->printingLock:Ljava/lang/Object;

    return-void
.end method

.method private doPrint()V
    .locals 28

    move-object/from16 v1, p0

    const-string v0, " LocalPrintThread  doPrint"

    .line 59
    invoke-static {v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;)V

    .line 61
    new-instance v0, Lepson/print/screen/PrintSetting;

    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/IprintApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Lepson/print/service/LocalPrintThread;->mKind:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, v2, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 62
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 64
    iget v2, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 65
    iget v6, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 66
    iget v3, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 67
    iget v15, v0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    .line 68
    iget v14, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 69
    iget v9, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 70
    iget v13, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 71
    iget v12, v0, Lepson/print/screen/PrintSetting;->copiesValue:I

    .line 73
    iget v11, v0, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 74
    iget v10, v0, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 75
    iget v8, v0, Lepson/print/screen/PrintSetting;->saturationValue:I

    .line 76
    iget v7, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 77
    iget v5, v0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    .line 78
    iget v0, v0, Lepson/print/screen/PrintSetting;->printdate:I

    move/from16 v17, v11

    move/from16 v16, v12

    move/from16 v18, v15

    const/4 v15, 0x0

    const/16 v19, 0x0

    .line 89
    :try_start_0
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v4}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v4

    move/from16 v21, v0

    const/4 v0, 0x1

    invoke-static {v4, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 91
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v4}, Lepson/print/service/RenderingController;->drawBeforeStartJob()V

    .line 92
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mIsBkRetry = "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v11, v1, Lepson/print/service/LocalPrintThread;->mIsBkRetry:Z

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 93
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    iget-boolean v12, v1, Lepson/print/service/LocalPrintThread;->mIsBkRetry:Z

    const/16 v11, 0x28

    move/from16 v20, v5

    move v5, v2

    move/from16 v24, v7

    move v7, v3

    move/from16 v25, v8

    move v8, v14

    move/from16 v26, v10

    move v10, v13

    move/from16 v11, v17

    move/from16 v0, v16

    move/from16 v16, v12

    move/from16 v12, v26

    move/from16 v22, v13

    move/from16 v13, v25

    move/from16 v23, v14

    move/from16 v14, v20

    move/from16 v20, v0

    move/from16 v0, v18

    move/from16 v15, v24

    invoke-interface/range {v4 .. v16}, Lepson/print/service/PrintService;->startJob(IIIIIIIIIIIZ)I

    move-result v15
    :try_end_0
    .catch Lepson/print/service/LocalInterrupt; {:try_start_0 .. :try_end_0} :catch_1b
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1b
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_0 .. :try_end_0} :catch_18
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_15
    .catchall {:try_start_0 .. :try_end_0} :catchall_a

    if-nez v15, :cond_18

    .line 98
    :try_start_1
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v4}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v4

    if-nez v4, :cond_17

    .line 102
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v4, v0}, Lepson/print/service/RenderingController;->startDrawAfterStartJob(I)V

    .line 103
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v4}, Lepson/print/service/RenderingController;->getImageList()Lepson/print/EPImageList;

    move-result-object v4

    iput-object v4, v1, Lepson/print/service/LocalPrintThread;->gImageList:Lepson/print/EPImageList;
    :try_end_1
    .catch Lepson/print/service/LocalInterrupt; {:try_start_1 .. :try_end_1} :catch_1b
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1b
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_1 .. :try_end_1} :catch_13
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_15
    .catchall {:try_start_1 .. :try_end_1} :catchall_8

    const/high16 v4, 0x10000

    const/4 v5, 0x4

    const/4 v6, 0x2

    if-eq v0, v4, :cond_2

    const/high16 v4, 0x20000

    if-eq v0, v4, :cond_0

    const/high16 v4, 0x40000

    if-eq v0, v4, :cond_0

    .line 125
    :try_start_2
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->gImageList:Lepson/print/EPImageList;

    invoke-virtual {v4}, Lepson/print/EPImageList;->size()I

    move-result v4

    move v14, v4

    const/4 v12, 0x1

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v2, v0

    move-object/from16 v0, v19

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_e

    :catch_0
    move-object/from16 v2, v19

    :catch_1
    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_11

    :catch_2
    move-exception v0

    move-object/from16 v2, v19

    :goto_0
    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_10

    :catch_3
    move-object/from16 v2, v19

    const/4 v0, 0x0

    :goto_1
    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_1c

    .line 118
    :cond_0
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->gImageList:Lepson/print/EPImageList;

    invoke-virtual {v4}, Lepson/print/EPImageList;->size()I

    move-result v4

    div-int/2addr v4, v5

    .line 119
    iget-object v6, v1, Lepson/print/service/LocalPrintThread;->gImageList:Lepson/print/EPImageList;

    invoke-virtual {v6}, Lepson/print/EPImageList;->size()I

    move-result v6

    rem-int/2addr v6, v5
    :try_end_2
    .catch Lepson/print/service/LocalInterrupt; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-lez v6, :cond_1

    add-int/lit8 v4, v4, 0x1

    move v14, v4

    const/4 v12, 0x4

    goto :goto_2

    :cond_1
    move v14, v4

    const/4 v12, 0x4

    goto :goto_2

    .line 110
    :cond_2
    :try_start_3
    iget-object v4, v1, Lepson/print/service/LocalPrintThread;->gImageList:Lepson/print/EPImageList;

    invoke-virtual {v4}, Lepson/print/EPImageList;->size()I

    move-result v4

    div-int/2addr v4, v6

    .line 111
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->gImageList:Lepson/print/EPImageList;

    invoke-virtual {v5}, Lepson/print/EPImageList;->size()I

    move-result v5

    rem-int/2addr v5, v6

    if-lez v5, :cond_3

    add-int/lit8 v4, v4, 0x1

    move v14, v4

    const/4 v12, 0x2

    goto :goto_2

    :cond_3
    move v14, v4

    const/4 v12, 0x2

    .line 129
    :goto_2
    new-instance v13, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;

    move-object v4, v13

    move v5, v2

    move/from16 v6, v22

    move/from16 v7, v24

    move v8, v3

    move v9, v0

    move/from16 v10, v23

    move/from16 v11, v21

    invoke-direct/range {v4 .. v11}, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;-><init>(IIIIIII)V

    .line 132
    new-instance v2, Lepson/print/service/CreatePrintImageThread;

    iget-object v8, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    iget-object v9, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    iget-object v11, v1, Lepson/print/service/LocalPrintThread;->gImageList:Lepson/print/EPImageList;

    move-object v7, v2

    move-object v10, v13

    move v13, v14

    invoke-direct/range {v7 .. v13}, Lepson/print/service/CreatePrintImageThread;-><init>(Lepson/print/service/RenderingController;Lepson/print/service/PrintService;Lepson/print/service/CreatePrintImageThread$LocalImageCreator;Lepson/print/EPImageList;II)V
    :try_end_3
    .catch Lepson/print/service/LocalInterrupt; {:try_start_3 .. :try_end_3} :catch_1b
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1b
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_3 .. :try_end_3} :catch_13
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_15
    .catchall {:try_start_3 .. :try_end_3} :catchall_8

    .line 136
    :try_start_4
    invoke-virtual {v2}, Lepson/print/service/CreatePrintImageThread;->start()V
    :try_end_4
    .catch Lepson/print/service/LocalInterrupt; {:try_start_4 .. :try_end_4} :catch_12
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_12
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_4 .. :try_end_4} :catch_11
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_7

    move/from16 v0, v20

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_3
    if-ge v3, v0, :cond_14

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v14, :cond_11

    .line 142
    :try_start_5
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v5
    :try_end_5
    .catch Lepson/print/service/LocalInterrupt; {:try_start_5 .. :try_end_5} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_d
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_5 .. :try_end_5} :catch_c
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    if-nez v5, :cond_10

    if-nez v3, :cond_5

    .line 148
    :try_start_6
    invoke-virtual {v2}, Lepson/print/service/CreatePrintImageThread;->waitePrintImage()I

    move-result v5

    if-ne v5, v4, :cond_4

    goto :goto_5

    .line 150
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error. : createImageThread.waitePrintImage()"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Lepson/print/service/LocalInterrupt; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    move v7, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_8

    :catch_4
    move v15, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_12

    :catch_5
    move-exception v0

    move v7, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_14

    :catch_6
    move v0, v4

    goto/16 :goto_1

    .line 154
    :cond_5
    :goto_5
    :try_start_7
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->gImageList:Lepson/print/EPImageList;

    invoke-virtual {v5, v4}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v5

    .line 155
    iget-object v6, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    iget-object v5, v5, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    invoke-interface {v6, v5}, Lepson/print/service/PrintService;->initImage(Ljava/lang/String;)I

    move-result v15
    :try_end_7
    .catch Lepson/print/service/LocalInterrupt; {:try_start_7 .. :try_end_7} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_d
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_7 .. :try_end_7} :catch_c
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_a
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    if-eqz v15, :cond_7

    .line 157
    :try_start_8
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 158
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0

    .line 161
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_8
    .catch Lepson/print/service/LocalInterrupt; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catchall_2
    move-exception v0

    move v7, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_18

    :catch_7
    move-exception v0

    move v7, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_15

    .line 163
    :cond_7
    :try_start_9
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v5

    if-nez v5, :cond_f

    const-string v5, "LocalPrintThread"

    const-string v6, "next startPage()"

    .line 167
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->startPage()I

    move-result v15

    if-nez v15, :cond_e

    .line 172
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v5

    if-nez v5, :cond_d

    const-string v5, "LocalPrintThread"

    const-string v6, "next printPage()"

    .line 176
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->printPage()I

    move-result v15

    if-nez v15, :cond_c

    .line 180
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v5

    if-nez v5, :cond_b

    const-string v5, "LocalPrintThread"

    const-string v6, "next endPage()"

    .line 184
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Lepson/print/service/LocalInterrupt; {:try_start_9 .. :try_end_9} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_d
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_a
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    add-int/lit8 v5, v14, -0x1

    if-ge v4, v5, :cond_8

    .line 186
    :try_start_a
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lepson/print/service/PrintService;->endPage(Z)I

    move-result v5
    :try_end_a
    .catch Lepson/print/service/LocalInterrupt; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_6
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_a .. :try_end_a} :catch_7
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move v15, v5

    const/4 v6, 0x0

    goto :goto_6

    .line 188
    :cond_8
    :try_start_b
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;
    :try_end_b
    .catch Lepson/print/service/LocalInterrupt; {:try_start_b .. :try_end_b} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_d
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_b .. :try_end_b} :catch_8
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    const/4 v6, 0x0

    :try_start_c
    invoke-interface {v5, v6}, Lepson/print/service/PrintService;->endPage(Z)I

    move-result v5

    move v15, v5

    :goto_6
    if-nez v15, :cond_a

    .line 192
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v5

    if-nez v5, :cond_9

    .line 196
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->releaseImage()I

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_4

    .line 193
    :cond_9
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0

    .line 191
    :cond_a
    new-instance v0, Lepson/print/service/LocalPrintThread$LocalEscprException;

    invoke-direct {v0}, Lepson/print/service/LocalPrintThread$LocalEscprException;-><init>()V

    throw v0

    :cond_b
    const/4 v6, 0x0

    .line 181
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0

    :cond_c
    const/4 v6, 0x0

    .line 179
    new-instance v0, Lepson/print/service/LocalPrintThread$LocalEscprException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "printPage() returns ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lepson/print/service/LocalPrintThread$LocalEscprException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    const/4 v6, 0x0

    .line 173
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0

    :cond_e
    const/4 v6, 0x0

    .line 171
    new-instance v0, Lepson/print/service/LocalPrintThread$LocalEscprException;

    invoke-direct {v0}, Lepson/print/service/LocalPrintThread$LocalEscprException;-><init>()V

    throw v0

    :cond_f
    const/4 v6, 0x0

    .line 164
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0
    :try_end_c
    .catch Lepson/print/service/LocalInterrupt; {:try_start_c .. :try_end_c} :catch_e
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_e
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_c .. :try_end_c} :catch_f
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_b
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    :catchall_3
    move-exception v0

    const/4 v6, 0x0

    goto :goto_a

    :catch_8
    move-exception v0

    const/4 v6, 0x0

    goto :goto_b

    :cond_10
    const/4 v6, 0x0

    .line 143
    :try_start_d
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0
    :try_end_d
    .catch Lepson/print/service/LocalInterrupt; {:try_start_d .. :try_end_d} :catch_e
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_e
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_d .. :try_end_d} :catch_9
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_b
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    :catchall_4
    move-exception v0

    goto :goto_7

    :catch_9
    move-exception v0

    goto :goto_9

    :catchall_5
    move-exception v0

    const/4 v6, 0x0

    :goto_7
    move v7, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    :goto_8
    const/4 v15, 0x0

    goto/16 :goto_18

    :catch_a
    const/4 v6, 0x0

    :catch_b
    move v15, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    goto/16 :goto_12

    :catch_c
    move-exception v0

    const/4 v6, 0x0

    :goto_9
    move v7, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    goto/16 :goto_14

    :catch_d
    const/4 v6, 0x0

    :catch_e
    move v0, v4

    const-wide/32 v4, 0xea60

    goto/16 :goto_1c

    :cond_11
    const/4 v6, 0x0

    if-eqz v24, :cond_12

    if-eqz v4, :cond_12

    .line 199
    :try_start_e
    rem-int/lit8 v5, v4, 0x2

    const/4 v7, 0x1

    if-ne v5, v7, :cond_13

    add-int/lit8 v12, v0, -0x1

    if-eq v3, v12, :cond_13

    .line 202
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5}, Lepson/print/service/PrintService;->startPage()I

    .line 204
    iget-object v5, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v5, v6}, Lepson/print/service/PrintService;->endPage(Z)I
    :try_end_e
    .catch Lepson/print/service/LocalInterrupt; {:try_start_e .. :try_end_e} :catch_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_e
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_e .. :try_end_e} :catch_f
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_b
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    goto :goto_c

    :catchall_6
    move-exception v0

    :goto_a
    move v7, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    goto/16 :goto_18

    :catch_f
    move-exception v0

    :goto_b
    move v7, v4

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    goto/16 :goto_15

    :cond_12
    const/4 v7, 0x1

    :cond_13
    :goto_c
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_14
    const/4 v6, 0x0

    const-string v0, "LocalPrintThread"

    const-string v3, "enter finally"

    .line 221
    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->endJob()I

    .line 224
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v0}, Lepson/print/service/RenderingController;->interruptSubThreads()V

    .line 226
    invoke-virtual {v2}, Lepson/print/service/CreatePrintImageThread;->interrupt()V

    const/16 v3, 0x28

    if-eq v15, v3, :cond_15

    .line 232
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_15
    const/4 v15, 0x0

    .line 236
    :cond_16
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    const-wide/32 v4, 0xea60

    .line 240
    :try_start_f
    invoke-virtual {v2, v4, v5}, Lepson/print/service/CreatePrintImageThread;->join(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_10

    goto/16 :goto_17

    :catch_10
    move-exception v0

    move-object v2, v0

    goto/16 :goto_16

    :catchall_7
    move-exception v0

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    const/4 v7, 0x0

    goto/16 :goto_18

    :catch_11
    move-exception v0

    goto/16 :goto_0

    :catch_12
    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    goto/16 :goto_1b

    :cond_17
    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    .line 99
    :try_start_10
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0

    :catchall_8
    move-exception v0

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    :goto_d
    move-object v2, v0

    move-object/from16 v0, v19

    :goto_e
    const/4 v7, 0x0

    goto/16 :goto_19

    :catch_13
    move-exception v0

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    :goto_f
    move-object/from16 v2, v19

    :goto_10
    const/4 v7, 0x0

    goto/16 :goto_15

    :cond_18
    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    .line 97
    new-instance v0, Lepson/print/service/LocalPrintThread$LocalEscprException;

    invoke-direct {v0}, Lepson/print/service/LocalPrintThread$LocalEscprException;-><init>()V

    throw v0
    :try_end_10
    .catch Lepson/print/service/LocalInterrupt; {:try_start_10 .. :try_end_10} :catch_1c
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_1c
    .catch Lepson/print/service/LocalPrintThread$LocalEscprException; {:try_start_10 .. :try_end_10} :catch_14
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_16
    .catchall {:try_start_10 .. :try_end_10} :catchall_9

    :catchall_9
    move-exception v0

    goto :goto_d

    :catch_14
    move-exception v0

    goto :goto_f

    :catchall_a
    move-exception v0

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    move-object v2, v0

    move-object/from16 v0, v19

    const/4 v7, 0x0

    const/4 v15, 0x0

    goto/16 :goto_19

    :catch_15
    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    :catch_16
    move-object/from16 v2, v19

    :goto_11
    const/4 v15, 0x0

    :goto_12
    const/16 v7, -0x1451

    .line 219
    :try_start_11
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    const/4 v8, -0x1

    invoke-interface {v0, v8, v7, v6}, Lepson/print/service/PrintService;->epsNotifyError(IIZ)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_b

    const-string v0, "LocalPrintThread"

    const-string v3, "enter finally"

    .line 221
    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->endJob()I

    .line 224
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v0}, Lepson/print/service/RenderingController;->interruptSubThreads()V

    if-eqz v2, :cond_19

    .line 226
    invoke-virtual {v2}, Lepson/print/service/CreatePrintImageThread;->interrupt()V

    .line 232
    :cond_19
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v7, 0x0

    .line 236
    :cond_1a
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v15}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_1b

    .line 240
    :try_start_12
    invoke-virtual {v2, v4, v5}, Lepson/print/service/CreatePrintImageThread;->join(J)V
    :try_end_12
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_12} :catch_17

    goto :goto_13

    :catch_17
    move-exception v0

    move-object v2, v0

    .line 242
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 245
    :cond_1b
    :goto_13
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v0, v4, v5}, Lepson/print/service/RenderingController;->joinSubThread(J)V

    const-string v0, "LocalPrintThread"

    const-string v2, "join() finished"

    .line 246
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->waitIfSimpleAp()V

    .line 251
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0, v7}, Lepson/print/service/PrintService;->notifyEndJob(I)V

    goto/16 :goto_1e

    :catchall_b
    move-exception v0

    move v7, v15

    const/16 v15, -0x1451

    goto :goto_18

    :catch_18
    move-exception v0

    const/16 v3, 0x28

    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    move-object/from16 v2, v19

    const/4 v7, 0x0

    :goto_14
    const/4 v15, 0x0

    .line 215
    :goto_15
    :try_start_13
    invoke-virtual {v0}, Lepson/print/service/LocalPrintThread$LocalEscprException;->printStackTrace()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_c

    const-string v0, "LocalPrintThread"

    const-string v8, "enter finally"

    .line 221
    invoke-static {v0, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->endJob()I

    .line 224
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v0}, Lepson/print/service/RenderingController;->interruptSubThreads()V

    if-eqz v2, :cond_1c

    .line 226
    invoke-virtual {v2}, Lepson/print/service/CreatePrintImageThread;->interrupt()V

    :cond_1c
    if-eq v15, v3, :cond_1d

    .line 232
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v0

    if-eqz v0, :cond_1e

    :cond_1d
    const/4 v15, 0x0

    .line 236
    :cond_1e
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_1f

    .line 240
    :try_start_14
    invoke-virtual {v2, v4, v5}, Lepson/print/service/CreatePrintImageThread;->join(J)V
    :try_end_14
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_14} :catch_19

    goto :goto_17

    :catch_19
    move-exception v0

    move-object v2, v0

    .line 242
    :goto_16
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 245
    :cond_1f
    :goto_17
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v0, v4, v5}, Lepson/print/service/RenderingController;->joinSubThread(J)V

    const-string v0, "LocalPrintThread"

    const-string v2, "join() finished"

    .line 246
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->waitIfSimpleAp()V

    .line 251
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0, v15}, Lepson/print/service/PrintService;->notifyEndJob(I)V

    goto/16 :goto_1e

    :catchall_c
    move-exception v0

    :goto_18
    move-object/from16 v27, v2

    move-object v2, v0

    move-object/from16 v0, v27

    :goto_19
    const-string v8, "LocalPrintThread"

    const-string v9, "enter finally"

    .line 221
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v8, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v8}, Lepson/print/service/PrintService;->endJob()I

    .line 224
    iget-object v8, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v8}, Lepson/print/service/RenderingController;->interruptSubThreads()V

    if-eqz v0, :cond_20

    .line 226
    invoke-virtual {v0}, Lepson/print/service/CreatePrintImageThread;->interrupt()V

    :cond_20
    if-eq v15, v3, :cond_21

    .line 232
    iget-object v3, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v3}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v3

    if-eqz v3, :cond_22

    :cond_21
    const/4 v15, 0x0

    .line 236
    :cond_22
    iget-object v3, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v3}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v0, :cond_23

    .line 240
    :try_start_15
    invoke-virtual {v0, v4, v5}, Lepson/print/service/CreatePrintImageThread;->join(J)V
    :try_end_15
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_15} :catch_1a

    goto :goto_1a

    :catch_1a
    move-exception v0

    move-object v3, v0

    .line 242
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 245
    :cond_23
    :goto_1a
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v0, v4, v5}, Lepson/print/service/RenderingController;->joinSubThread(J)V

    const-string v0, "LocalPrintThread"

    const-string v3, "join() finished"

    .line 246
    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->waitIfSimpleAp()V

    .line 251
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0, v15}, Lepson/print/service/PrintService;->notifyEndJob(I)V

    .line 254
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    throw v2

    :catch_1b
    const-wide/32 v4, 0xea60

    const/4 v6, 0x0

    :catch_1c
    move-object/from16 v2, v19

    :goto_1b
    const/4 v0, 0x0

    :goto_1c
    const-string v3, "LocalPrintThread"

    const-string v7, "enter finally"

    .line 221
    invoke-static {v3, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v3, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v3}, Lepson/print/service/PrintService;->endJob()I

    .line 224
    iget-object v3, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v3}, Lepson/print/service/RenderingController;->interruptSubThreads()V

    if-eqz v2, :cond_24

    .line 226
    invoke-virtual {v2}, Lepson/print/service/CreatePrintImageThread;->interrupt()V

    .line 232
    :cond_24
    iget-object v3, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v3}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v3

    .line 236
    iget-object v3, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v3}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_25

    .line 240
    :try_start_16
    invoke-virtual {v2, v4, v5}, Lepson/print/service/CreatePrintImageThread;->join(J)V
    :try_end_16
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_16} :catch_1d

    goto :goto_1d

    :catch_1d
    move-exception v0

    move-object v2, v0

    .line 242
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 245
    :cond_25
    :goto_1d
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v0, v4, v5}, Lepson/print/service/RenderingController;->joinSubThread(J)V

    const-string v0, "LocalPrintThread"

    const-string v2, "join() finished"

    .line 246
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->waitIfSimpleAp()V

    .line 251
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0, v6}, Lepson/print/service/PrintService;->notifyEndJob(I)V

    .line 254
    :goto_1e
    iget-object v0, v1, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 46
    iget-object v0, p0, Lepson/print/service/LocalPrintThread;->printingLock:Ljava/lang/Object;

    monitor-enter v0

    .line 47
    :try_start_0
    iget-object v1, p0, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lepson/print/service/PrintService;->setPrinting(Z)V

    .line 48
    iget-object v1, p0, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lepson/print/service/PrintService;->setCancelPrinting(Z)V

    .line 50
    invoke-direct {p0}, Lepson/print/service/LocalPrintThread;->doPrint()V

    .line 52
    iget-object v1, p0, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v1, v2}, Lepson/print/service/PrintService;->setPrinting(Z)V

    .line 53
    iget-object v1, p0, Lepson/print/service/LocalPrintThread;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v1, v2}, Lepson/print/service/PrintService;->setCancelPrinting(Z)V

    .line 54
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

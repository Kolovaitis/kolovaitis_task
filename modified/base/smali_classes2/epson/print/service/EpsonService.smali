.class public Lepson/print/service/EpsonService;
.super Landroid/app/Service;
.source "EpsonService.java"

# interfaces
.implements Lcom/epson/iprint/apf/ApfEpImageAdapter$ProgressCallback;
.implements Lepson/print/service/PrintService;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/service/EpsonService$SearchingThread;
    }
.end annotation


# static fields
.field static final DISABLE_SIMPLE_AP_TIMEOUT:I = 0x708

.field public static final EPS_ERR_MEMORY_ALLOCATION:I = -0x3e9

.field public static final EPS_ERR_NONE:I = 0x0

.field private static final EPS_MLID_BORDERLESS:I = 0x1

.field static final EPS_MSID_A3:I = 0x3e

.field static final EPS_MSID_B4:I = 0x3f

.field static final EPS_MSID_LEGAL:I = 0x2

.field public static final MEDIA_INFO_MEMORY_ERROR:I = -0x1451

.field static final NOT_SET_TIMEOUT:I = -0x1

.field public static final PREFS_NAME:Ljava/lang/String; = "PrintSetting"

.field public static final PROGRESS_NOTIFY_TYPE_APF:I = 0x2

.field public static final PROGRESS_NOTIFY_TYPE_PRINTING:I = 0x1

.field private static volatile bCancelPrinting:Z = false

.field public static bNotifyPrinter:Z = false

.field private static isInitDriver:Z = false

.field static final mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lepson/print/service/IEpsonServiceCallback;",
            ">;"
        }
    .end annotation
.end field

.field private static final mCancelLock:Ljava/lang/Object;

.field private static final mConnect:Ljava/lang/Object;

.field private static final mLock:Ljava/lang/Object;

.field private static final mLockPrintingStatus:Ljava/lang/Object;

.field private static final mLockSearchingStatus:Ljava/lang/Object;

.field private static final mPrinting:Ljava/lang/Object;


# instance fields
.field private final EPS_ERR_PRINTER_NOT_SET:I

.field private final EPS_PRNERR_COMM:I

.field private final EPS_PRNERR_NOERROR:I

.field private final EPS_PRNST_BUSY:I

.field private final EPS_PRNST_ERROR:I

.field private final TAG:Ljava/lang/String;

.field private accessKey:Ljava/lang/String;

.field private bEnd:Z

.field private volatile bPrinting:Z

.field private volatile bSearching:Z

.field bSetPrinter:Z

.field private clientId:Ljava/lang/String;

.field public debugString:Ljava/lang/String;

.field private epsJobattrib:Lepson/print/ecclient/EpsJobAttrib;

.field private id:Ljava/lang/String;

.field private ip:Ljava/lang/String;

.field private final mBinder:Lepson/print/service/IEpsonService$Stub;

.field private mCreatetSheetIndex:I

.field volatile mEPImageCreator:Lepson/print/EPImageCreator;

.field private final mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

.field mHandler:Landroid/os/Handler;

.field private mLocalPrintThread:Lepson/print/service/LocalPrintThread;

.field private volatile mSearchCount:I

.field private mailAddress:Ljava/lang/String;

.field private printingThread:Ljava/lang/Thread;

.field private probeMethod:I

.field private searchingPrinter:Lepson/print/service/EpsonService$SearchingThread;

.field private timeout_sec:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 212
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    sput-object v0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 2515
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    .line 2516
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/service/EpsonService;->mLockPrintingStatus:Ljava/lang/Object;

    .line 2517
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/service/EpsonService;->mLockSearchingStatus:Ljava/lang/Object;

    .line 2518
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/service/EpsonService;->mPrinting:Ljava/lang/Object;

    .line 2519
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/service/EpsonService;->mConnect:Ljava/lang/Object;

    .line 2520
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/service/EpsonService;->mCancelLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 59
    invoke-static {}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getInstance()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iput-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    const-string v0, "EpsonService"

    .line 67
    iput-object v0, p0, Lepson/print/service/EpsonService;->TAG:Ljava/lang/String;

    const/4 v0, -0x1

    .line 74
    iput v0, p0, Lepson/print/service/EpsonService;->timeout_sec:I

    const/4 v0, 0x0

    .line 82
    iput-boolean v0, p0, Lepson/print/service/EpsonService;->bSetPrinter:Z

    .line 83
    iput-boolean v0, p0, Lepson/print/service/EpsonService;->bPrinting:Z

    .line 86
    iput-boolean v0, p0, Lepson/print/service/EpsonService;->bSearching:Z

    .line 88
    iput v0, p0, Lepson/print/service/EpsonService;->mCreatetSheetIndex:I

    const/4 v1, 0x0

    .line 90
    iput-object v1, p0, Lepson/print/service/EpsonService;->searchingPrinter:Lepson/print/service/EpsonService$SearchingThread;

    .line 91
    iput-object v1, p0, Lepson/print/service/EpsonService;->printingThread:Ljava/lang/Thread;

    const/4 v2, 0x2

    .line 94
    iput v2, p0, Lepson/print/service/EpsonService;->EPS_PRNST_BUSY:I

    const/4 v2, 0x4

    .line 95
    iput v2, p0, Lepson/print/service/EpsonService;->EPS_PRNST_ERROR:I

    const/16 v2, 0x66

    .line 96
    iput v2, p0, Lepson/print/service/EpsonService;->EPS_PRNERR_COMM:I

    const/16 v2, -0x547

    .line 102
    iput v2, p0, Lepson/print/service/EpsonService;->EPS_ERR_PRINTER_NOT_SET:I

    .line 103
    iput v0, p0, Lepson/print/service/EpsonService;->EPS_PRNERR_NOERROR:I

    const-string v2, ""

    .line 111
    iput-object v2, p0, Lepson/print/service/EpsonService;->mailAddress:Ljava/lang/String;

    const-string v2, ""

    .line 112
    iput-object v2, p0, Lepson/print/service/EpsonService;->accessKey:Ljava/lang/String;

    .line 113
    iput-object v1, p0, Lepson/print/service/EpsonService;->clientId:Ljava/lang/String;

    .line 114
    new-instance v1, Lepson/print/ecclient/EpsJobAttrib;

    invoke-direct {v1}, Lepson/print/ecclient/EpsJobAttrib;-><init>()V

    iput-object v1, p0, Lepson/print/service/EpsonService;->epsJobattrib:Lepson/print/ecclient/EpsJobAttrib;

    .line 215
    iput v0, p0, Lepson/print/service/EpsonService;->mSearchCount:I

    .line 217
    new-instance v0, Lepson/print/service/EpsonService$1;

    invoke-direct {v0, p0}, Lepson/print/service/EpsonService$1;-><init>(Lepson/print/service/EpsonService;)V

    iput-object v0, p0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    .line 1635
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/service/EpsonService$2;

    invoke-direct {v1, p0}, Lepson/print/service/EpsonService$2;-><init>(Lepson/print/service/EpsonService;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/service/EpsonService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .line 56
    sget-object v0, Lepson/print/service/EpsonService;->mLockSearchingStatus:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lepson/print/service/EpsonService;)Ljava/lang/String;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->id:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1000()Ljava/lang/Object;
    .locals 1

    .line 56
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$102(Lepson/print/service/EpsonService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 56
    iput-object p1, p0, Lepson/print/service/EpsonService;->id:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lepson/print/service/EpsonService;Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;)I
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2}, Lepson/print/service/EpsonService;->createECPrintingThread(Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;)I

    move-result p0

    return p0
.end method

.method static synthetic access$1200(Lepson/print/service/EpsonService;Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;Z)V
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lepson/print/service/EpsonService;->startLocalNonPdfPrintThread(Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;Z)V

    return-void
.end method

.method static synthetic access$1300(Lepson/print/service/EpsonService;)Ljava/lang/Thread;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->printingThread:Ljava/lang/Thread;

    return-object p0
.end method

.method static synthetic access$1302(Lepson/print/service/EpsonService;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    .line 56
    iput-object p1, p0, Lepson/print/service/EpsonService;->printingThread:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic access$1400(Lepson/print/service/EpsonService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIZ)V
    .locals 0

    .line 56
    invoke-direct/range {p0 .. p7}, Lepson/print/service/EpsonService;->startPdfPrintThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIZ)V

    return-void
.end method

.method static synthetic access$1500()Ljava/lang/Object;
    .locals 1

    .line 56
    sget-object v0, Lepson/print/service/EpsonService;->mPrinting:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1600()Ljava/lang/Object;
    .locals 1

    .line 56
    sget-object v0, Lepson/print/service/EpsonService;->mLockPrintingStatus:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1700(Lepson/print/service/EpsonService;)Z
    .locals 0

    .line 56
    iget-boolean p0, p0, Lepson/print/service/EpsonService;->bPrinting:Z

    return p0
.end method

.method static synthetic access$1702(Lepson/print/service/EpsonService;Z)Z
    .locals 0

    .line 56
    iput-boolean p1, p0, Lepson/print/service/EpsonService;->bPrinting:Z

    return p1
.end method

.method static synthetic access$1800(Lepson/print/service/EpsonService;)Z
    .locals 0

    .line 56
    iget-boolean p0, p0, Lepson/print/service/EpsonService;->bSearching:Z

    return p0
.end method

.method static synthetic access$1802(Lepson/print/service/EpsonService;Z)Z
    .locals 0

    .line 56
    iput-boolean p1, p0, Lepson/print/service/EpsonService;->bSearching:Z

    return p1
.end method

.method static synthetic access$1900()Z
    .locals 1

    .line 56
    sget-boolean v0, Lepson/print/service/EpsonService;->bCancelPrinting:Z

    return v0
.end method

.method static synthetic access$1902(Z)Z
    .locals 0

    .line 56
    sput-boolean p0, Lepson/print/service/EpsonService;->bCancelPrinting:Z

    return p0
.end method

.method static synthetic access$200(Lepson/print/service/EpsonService;)Ljava/lang/String;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->ip:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2000()Ljava/lang/Object;
    .locals 1

    .line 56
    sget-object v0, Lepson/print/service/EpsonService;->mCancelLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lepson/print/service/EpsonService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 56
    iput-object p1, p0, Lepson/print/service/EpsonService;->ip:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lepson/print/service/EpsonService;)Lepson/print/service/LocalPrintThread;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->mLocalPrintThread:Lepson/print/service/LocalPrintThread;

    return-object p0
.end method

.method static synthetic access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->epsJobattrib:Lepson/print/ecclient/EpsJobAttrib;

    return-object p0
.end method

.method static synthetic access$2300(Lepson/print/service/EpsonService;)Ljava/lang/String;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->mailAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2400(Lepson/print/service/EpsonService;)Ljava/lang/String;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->accessKey:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2500()Ljava/lang/Object;
    .locals 1

    .line 56
    sget-object v0, Lepson/print/service/EpsonService;->mConnect:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2600(Lepson/print/service/EpsonService;)Ljava/lang/String;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->clientId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2700(Lepson/print/service/EpsonService;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lepson/print/service/EpsonService;->updateLoginInfo()V

    return-void
.end method

.method static synthetic access$2802(Lepson/print/service/EpsonService;Z)Z
    .locals 0

    .line 56
    iput-boolean p1, p0, Lepson/print/service/EpsonService;->bEnd:Z

    return p1
.end method

.method static synthetic access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/service/EpsonService;)I
    .locals 0

    .line 56
    iget p0, p0, Lepson/print/service/EpsonService;->probeMethod:I

    return p0
.end method

.method static synthetic access$3000(Lepson/print/service/EpsonService;)I
    .locals 0

    .line 56
    iget p0, p0, Lepson/print/service/EpsonService;->mCreatetSheetIndex:I

    return p0
.end method

.method static synthetic access$3002(Lepson/print/service/EpsonService;I)I
    .locals 0

    .line 56
    iput p1, p0, Lepson/print/service/EpsonService;->mCreatetSheetIndex:I

    return p1
.end method

.method static synthetic access$302(Lepson/print/service/EpsonService;I)I
    .locals 0

    .line 56
    iput p1, p0, Lepson/print/service/EpsonService;->probeMethod:I

    return p1
.end method

.method static synthetic access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->searchingPrinter:Lepson/print/service/EpsonService$SearchingThread;

    return-object p0
.end method

.method static synthetic access$500(Lepson/print/service/EpsonService;)I
    .locals 0

    .line 56
    iget p0, p0, Lepson/print/service/EpsonService;->mSearchCount:I

    return p0
.end method

.method static synthetic access$504(Lepson/print/service/EpsonService;)I
    .locals 1

    .line 56
    iget v0, p0, Lepson/print/service/EpsonService;->mSearchCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lepson/print/service/EpsonService;->mSearchCount:I

    return v0
.end method

.method static synthetic access$506(Lepson/print/service/EpsonService;)I
    .locals 1

    .line 56
    iget v0, p0, Lepson/print/service/EpsonService;->mSearchCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lepson/print/service/EpsonService;->mSearchCount:I

    return v0
.end method

.method static synthetic access$600()Z
    .locals 1

    .line 56
    sget-boolean v0, Lepson/print/service/EpsonService;->isInitDriver:Z

    return v0
.end method

.method static synthetic access$602(Z)Z
    .locals 0

    .line 56
    sput-boolean p0, Lepson/print/service/EpsonService;->isInitDriver:Z

    return p0
.end method

.method static synthetic access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    return-object p0
.end method

.method static synthetic access$800(Lepson/print/service/EpsonService;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lepson/print/service/EpsonService;->createSearchingThread()V

    return-void
.end method

.method static synthetic access$900(Lepson/print/service/EpsonService;)I
    .locals 0

    .line 56
    iget p0, p0, Lepson/print/service/EpsonService;->timeout_sec:I

    return p0
.end method

.method static synthetic access$902(Lepson/print/service/EpsonService;I)I
    .locals 0

    .line 56
    iput p1, p0, Lepson/print/service/EpsonService;->timeout_sec:I

    return p1
.end method

.method private clearRemotePrinterInfo()V
    .locals 2

    .line 1513
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->invalidateCache()V

    .line 1515
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-virtual {p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1521
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackRemotePrinterInfo()V

    return-void
.end method

.method private declared-synchronized createECPrintingThread(Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;)I
    .locals 1

    monitor-enter p0

    .line 1698
    :try_start_0
    new-instance v0, Lepson/print/service/EpsonService$3;

    invoke-direct {v0, p0, p1, p2}, Lepson/print/service/EpsonService$3;-><init>(Lepson/print/service/EpsonService;Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;)V

    iput-object v0, p0, Lepson/print/service/EpsonService;->printingThread:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x0

    .line 1976
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized createSearchingThread()V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "Epson"

    const-string v1, "createSearchingThread() call"

    .line 1525
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1526
    new-instance v0, Lepson/print/service/EpsonService$SearchingThread;

    invoke-direct {v0, p0}, Lepson/print/service/EpsonService$SearchingThread;-><init>(Lepson/print/service/EpsonService;)V

    iput-object v0, p0, Lepson/print/service/EpsonService;->searchingPrinter:Lepson/print/service/EpsonService$SearchingThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1527
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getStartPageResolution(ZII)I
    .locals 0

    if-eqz p1, :cond_0

    .line 2226
    invoke-static {p3, p2}, Lepson/print/EPImageCreator;->getPrintAreaResolution(II)I

    move-result p1

    return p1

    .line 2228
    :cond_0
    iget-object p1, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_start_job_resolution2()I

    move-result p1

    return p1
.end method

.method public static notifyProgress(II)V
    .locals 5

    .line 2541
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2542
    :try_start_0
    sget-object v1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 2545
    :try_start_1
    sget-object v3, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v3, p0, p1}, Lepson/print/service/IEpsonServiceCallback;->onNotifyProgress(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "NotificationService"

    .line 2549
    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2552
    :cond_0
    sget-object p0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2553
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method public static onNotifyContinueable(I)V
    .locals 5

    .line 2557
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2558
    :try_start_0
    sget-object v1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 2561
    :try_start_1
    sget-object v3, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v3, p0}, Lepson/print/service/IEpsonServiceCallback;->onNotifyContinueable(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "NotificationService"

    .line 2565
    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2568
    :cond_0
    sget-object p0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2569
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method public static onNotifyEndJob(I)V
    .locals 5

    .line 2593
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2594
    :try_start_0
    sget-object v1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 2597
    :try_start_1
    sget-object v3, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v3, p0}, Lepson/print/service/IEpsonServiceCallback;->onNotifyEndJob(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "NotificationService"

    .line 2601
    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2604
    :cond_0
    sget-object p0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2605
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method public static onNotifyError(IIZ)V
    .locals 5

    .line 2610
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2611
    :try_start_0
    sget-object v1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 2614
    :try_start_1
    sget-object v3, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v3, p0, p1, p2}, Lepson/print/service/IEpsonServiceCallback;->onNotifyError(IIZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "NotificationService"

    .line 2618
    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2621
    :cond_0
    sget-object p0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2622
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method public static onNotifyProgress(I)V
    .locals 5

    .line 2524
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2525
    :try_start_0
    sget-object v1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 2528
    :try_start_1
    sget-object v3, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lepson/print/service/IEpsonServiceCallback;

    const/4 v4, 0x1

    invoke-interface {v3, v4, p0}, Lepson/print/service/IEpsonServiceCallback;->onNotifyProgress(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "NotificationService"

    .line 2532
    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2535
    :cond_0
    sget-object p0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2536
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method private startLocalNonPdfPrintThread(Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;Z)V
    .locals 1

    .line 1682
    new-instance v0, Lepson/print/service/LegacyRenderingController;

    invoke-direct {v0, p0, p2, p1}, Lepson/print/service/LegacyRenderingController;-><init>(Lepson/print/service/PrintService;Lepson/print/EPImageList;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1683
    new-instance p1, Lepson/print/service/LocalPrintThread;

    invoke-direct {p1, p0, v0, p3}, Lepson/print/service/LocalPrintThread;-><init>(Lepson/print/service/PrintService;Lepson/print/service/RenderingController;Z)V

    iput-object p1, p0, Lepson/print/service/EpsonService;->mLocalPrintThread:Lepson/print/service/LocalPrintThread;

    .line 1684
    iget-object p1, p0, Lepson/print/service/EpsonService;->mLocalPrintThread:Lepson/print/service/LocalPrintThread;

    invoke-virtual {p1}, Lepson/print/service/LocalPrintThread;->start()V

    return-void
.end method

.method private startPdfPrintThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIZ)V
    .locals 8

    .line 1690
    new-instance v7, Lepson/print/service/PdfRenderingController;

    const/4 v0, 0x2

    new-array v6, v0, [I

    const/4 v0, 0x0

    aput p5, v6, v0

    const/4 p5, 0x1

    aput p6, v6, p5

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lepson/print/service/PdfRenderingController;-><init>(Lepson/print/service/PrintService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[I)V

    .line 1693
    new-instance p1, Lepson/print/service/LocalPrintThread;

    invoke-direct {p1, p0, v7, p7}, Lepson/print/service/LocalPrintThread;-><init>(Lepson/print/service/PrintService;Lepson/print/service/RenderingController;Z)V

    iput-object p1, p0, Lepson/print/service/EpsonService;->mLocalPrintThread:Lepson/print/service/LocalPrintThread;

    .line 1694
    iget-object p1, p0, Lepson/print/service/EpsonService;->mLocalPrintThread:Lepson/print/service/LocalPrintThread;

    invoke-virtual {p1}, Lepson/print/service/LocalPrintThread;->start()V

    return-void
.end method

.method private stopSearchingThread()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1675
    iget-object v0, p0, Lepson/print/service/EpsonService;->searchingPrinter:Lepson/print/service/EpsonService$SearchingThread;

    if-nez v0, :cond_0

    return-void

    .line 1678
    :cond_0
    iget-object v0, p0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    invoke-virtual {v0}, Lepson/print/service/IEpsonService$Stub;->cancelSearchPrinter()I

    return-void
.end method

.method private updateLoginInfo()V
    .locals 3

    .line 1490
    invoke-direct {p0}, Lepson/print/service/EpsonService;->clearRemotePrinterInfo()V

    .line 1493
    invoke-static {p0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object v0

    const-string v1, "PRINTER_CLIENT_ID"

    const/4 v2, 0x0

    .line 1499
    invoke-virtual {v0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/service/EpsonService;->clientId:Ljava/lang/String;

    .line 1500
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u25b2clientId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/service/EpsonService;->clientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 1502
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 1503
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/service/EpsonService;->mailAddress:Ljava/lang/String;

    .line 1504
    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->getRemotePrinterAccessKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/service/EpsonService;->accessKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public createPrintImage(Lepson/print/EPImageList;IIIIIIII)Ljava/lang/String;
    .locals 17

    move-object/from16 v1, p0

    move/from16 v0, p3

    move/from16 v2, p5

    move/from16 v9, p6

    .line 1985
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->d()V

    .line 1987
    invoke-virtual/range {p1 .. p2}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v15

    .line 1988
    new-instance v3, Lepson/print/EPImageCreator;

    invoke-virtual/range {p0 .. p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lepson/print/EPImageCreator;-><init>(Landroid/content/Context;)V

    .line 1990
    sget-object v4, Lepson/print/service/EpsonService;->mCancelLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1991
    :try_start_0
    iput-object v3, v1, Lepson/print/service/EpsonService;->mEPImageCreator:Lepson/print/EPImageCreator;

    .line 1992
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1995
    invoke-static {}, Ljava/lang/System;->gc()V

    const/4 v14, 0x0

    .line 1999
    :try_start_1
    sget-boolean v4, Lepson/print/service/EpsonService;->bCancelPrinting:Z

    const/4 v5, 0x1

    if-eq v4, v5, :cond_13

    .line 2004
    iget-object v4, v1, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v4}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_printable_area()[I

    move-result-object v4

    .line 2006
    invoke-static/range {p0 .. p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    .line 2007
    invoke-static/range {p0 .. p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v13

    .line 2008
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " \u2606\u2606 isRemotePrinter  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const/4 v6, 0x2

    const/4 v7, 0x0

    if-ne v13, v5, :cond_6

    .line 2012
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v0}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object v4

    .line 2013
    new-array v8, v6, [I

    if-ne v9, v6, :cond_0

    .line 2016
    invoke-virtual {v4}, Lepson/common/Info_paper;->getPaper_width_boder()I

    move-result v10

    aput v10, v8, v7

    .line 2017
    invoke-virtual {v4}, Lepson/common/Info_paper;->getPaper_height_boder()I

    move-result v4

    aput v4, v8, v5

    goto :goto_0

    :cond_0
    if-ne v9, v5, :cond_1

    .line 2019
    invoke-virtual {v4}, Lepson/common/Info_paper;->getPaper_width_boderless()I

    move-result v10

    aput v10, v8, v7

    .line 2020
    invoke-virtual {v4}, Lepson/common/Info_paper;->getPaper_height_boderless()I

    move-result v4

    aput v4, v8, v5

    .line 2023
    :cond_1
    :goto_0
    iget v4, v15, Lepson/print/EPImage;->srcWidth:I

    aget v10, v8, v7

    if-le v4, v10, :cond_2

    iget v4, v15, Lepson/print/EPImage;->srcHeight:I

    aget v10, v8, v5

    if-le v4, v10, :cond_2

    goto :goto_1

    .line 2028
    :cond_2
    iget v4, v15, Lepson/print/EPImage;->srcWidth:I

    aget v10, v8, v7

    if-le v4, v10, :cond_3

    .line 2033
    aget v4, v8, v7

    int-to-float v4, v4

    aget v10, v8, v5

    int-to-float v10, v10

    div-float/2addr v4, v10

    .line 2036
    iget v10, v15, Lepson/print/EPImage;->srcHeight:I

    aput v10, v8, v5

    .line 2037
    aget v10, v8, v5

    int-to-float v10, v10

    mul-float v10, v10, v4

    float-to-int v4, v10

    aput v4, v8, v7

    goto :goto_1

    .line 2038
    :cond_3
    iget v4, v15, Lepson/print/EPImage;->srcHeight:I

    aget v10, v8, v5

    if-le v4, v10, :cond_4

    .line 2043
    aget v4, v8, v5

    int-to-float v4, v4

    aget v10, v8, v7

    int-to-float v10, v10

    div-float/2addr v4, v10

    .line 2046
    iget v10, v15, Lepson/print/EPImage;->srcWidth:I

    aput v10, v8, v7

    .line 2047
    aget v10, v8, v7

    int-to-float v10, v10

    mul-float v10, v10, v4

    float-to-int v4, v10

    aput v4, v8, v5

    goto :goto_1

    .line 2053
    :cond_4
    iget v4, v15, Lepson/print/EPImage;->srcWidth:I

    iget v10, v15, Lepson/print/EPImage;->srcHeight:I

    if-ge v4, v10, :cond_5

    .line 2055
    aget v4, v8, v5

    int-to-float v4, v4

    aget v10, v8, v7

    int-to-float v10, v10

    div-float/2addr v4, v10

    .line 2058
    iget v10, v15, Lepson/print/EPImage;->srcWidth:I

    aput v10, v8, v7

    .line 2059
    aget v10, v8, v7

    int-to-float v10, v10

    mul-float v10, v10, v4

    float-to-int v4, v10

    aput v4, v8, v5

    goto :goto_1

    .line 2062
    :cond_5
    aget v4, v8, v7

    int-to-float v4, v4

    aget v10, v8, v5

    int-to-float v10, v10

    div-float/2addr v4, v10

    .line 2065
    iget v10, v15, Lepson/print/EPImage;->srcHeight:I

    aput v10, v8, v5

    .line 2066
    aget v10, v8, v5

    int-to-float v10, v10

    mul-float v10, v10, v4

    float-to-int v4, v10

    aput v4, v8, v7

    goto :goto_1

    :cond_6
    move-object v8, v4

    .line 2072
    :goto_1
    iput v7, v15, Lepson/print/EPImage;->rotate:I

    .line 2075
    iget-object v4, v1, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    invoke-virtual {v4}, Lepson/print/service/IEpsonService$Stub;->getLang()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_4

    .line 2102
    :pswitch_0
    iget-object v4, v1, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v4, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->pageS_needRotate2(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2104
    aget v4, v8, v7

    .line 2105
    aget v10, v8, v5

    aput v10, v8, v7

    .line 2106
    aput v4, v8, v5

    if-nez p7, :cond_7

    .line 2110
    iget-boolean v4, v15, Lepson/print/EPImage;->isPaperLandScape:Z

    if-nez v4, :cond_7

    .line 2111
    iget v4, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit8 v4, v4, 0x5a

    iput v4, v15, Lepson/print/EPImage;->rotate:I

    :cond_7
    if-ne v2, v6, :cond_8

    .line 2114
    rem-int/lit8 v2, p2, 0x2

    if-ne v2, v5, :cond_8

    .line 2115
    iget v2, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit16 v2, v2, 0xb4

    iput v2, v15, Lepson/print/EPImage;->rotate:I

    :cond_8
    const/16 v16, 0x1

    goto/16 :goto_5

    :cond_9
    if-nez p7, :cond_a

    .line 2120
    iget-boolean v4, v15, Lepson/print/EPImage;->isPaperLandScape:Z

    if-ne v4, v5, :cond_a

    .line 2121
    iget v4, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit8 v4, v4, 0x5a

    iput v4, v15, Lepson/print/EPImage;->rotate:I

    :cond_a
    if-ne v2, v5, :cond_f

    .line 2124
    rem-int/lit8 v2, p2, 0x2

    if-ne v2, v5, :cond_f

    .line 2125
    iget v2, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit16 v2, v2, 0xb4

    iput v2, v15, Lepson/print/EPImage;->rotate:I

    goto :goto_4

    :pswitch_1
    if-nez p7, :cond_f

    .line 2138
    iget-boolean v2, v15, Lepson/print/EPImage;->isPaperLandScape:Z

    if-ne v2, v5, :cond_f

    .line 2139
    iget v2, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit8 v2, v2, 0x5a

    iput v2, v15, Lepson/print/EPImage;->rotate:I

    goto :goto_4

    .line 2078
    :pswitch_2
    aget v4, v8, v7

    aget v10, v8, v5

    if-gt v4, v10, :cond_b

    const/4 v4, 0x1

    goto :goto_2

    :cond_b
    const/4 v4, 0x0

    :goto_2
    if-nez p7, :cond_d

    if-ne v4, v5, :cond_c

    .line 2082
    iget-boolean v10, v15, Lepson/print/EPImage;->isPaperLandScape:Z

    if-ne v10, v5, :cond_c

    .line 2083
    iget v10, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit8 v10, v10, 0x5a

    iput v10, v15, Lepson/print/EPImage;->rotate:I

    goto :goto_3

    :cond_c
    if-nez v4, :cond_d

    .line 2084
    iget-boolean v10, v15, Lepson/print/EPImage;->isPaperLandScape:Z

    if-nez v10, :cond_d

    .line 2085
    iget v10, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit8 v10, v10, 0x5a

    iput v10, v15, Lepson/print/EPImage;->rotate:I

    .line 2090
    :cond_d
    :goto_3
    rem-int/lit8 v10, p2, 0x2

    if-ne v10, v5, :cond_f

    if-ne v4, v5, :cond_e

    if-ne v2, v5, :cond_e

    .line 2092
    iget v2, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit16 v2, v2, 0xb4

    iput v2, v15, Lepson/print/EPImage;->rotate:I

    goto :goto_4

    :cond_e
    if-nez v4, :cond_f

    if-ne v2, v6, :cond_f

    .line 2094
    iget v2, v15, Lepson/print/EPImage;->rotate:I

    add-int/lit16 v2, v2, 0xb4

    iput v2, v15, Lepson/print/EPImage;->rotate:I

    :cond_f
    :goto_4
    const/16 v16, 0x0

    .line 2145
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/EpsonService;->getLang()I

    move-result v2

    move/from16 v4, p8

    invoke-direct {v1, v13, v4, v2}, Lepson/print/service/EpsonService;->getStartPageResolution(ZII)I

    move-result v6

    .line 2147
    invoke-virtual/range {p1 .. p1}, Lepson/print/EPImageList;->getRenderingMode()I

    move-result v2

    if-ne v2, v5, :cond_11

    if-nez p7, :cond_10

    .line 2149
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    aget v7, v8, v7

    aget v8, v8, v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    move-object v3, v4

    move-object v4, v15

    move v5, v6

    move v6, v7

    move v7, v8

    move/from16 v8, p3

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p4

    move/from16 v12, p9

    move-object v0, v14

    move/from16 v14, v16

    :try_start_2
    invoke-virtual/range {v2 .. v14}, Lepson/print/EPImageCreator;->createPrintImage(Landroid/content/Context;Lepson/print/EPImage;IIIIIIIIZZ)Ljava/lang/String;

    .line 2160
    iget-object v2, v15, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iput-object v2, v15, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v14, v0

    goto/16 :goto_7

    :catch_0
    move-object v14, v0

    goto/16 :goto_6

    .line 2163
    :cond_10
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    aget v9, v8, v7

    aget v10, v8, v5

    iget v13, v15, Lepson/print/EPImage;->rotate:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v2, v3

    move-object v3, v4

    move/from16 v4, p7

    move v5, v6

    move-object/from16 v6, p1

    move/from16 v7, p2

    move v8, v9

    move v9, v10

    move/from16 v10, p3

    move/from16 v11, p4

    move/from16 v12, p9

    move-object v0, v14

    move/from16 v14, v16

    :try_start_4
    invoke-virtual/range {v2 .. v14}, Lepson/print/EPImageCreator;->createMultiPrintImage(Landroid/content/Context;IILepson/print/EPImageList;IIIIIIIZ)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v15, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-object v14, v0

    goto :goto_7

    :cond_11
    if-eqz p7, :cond_12

    :try_start_5
    const-string v2, "\u5272\u308a\u4ed8\u3051\u753b\u50cf\u3092\u751f\u6210"

    .line 2178
    invoke-static {v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 2181
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    aget v9, v8, v7

    aget v10, v8, v5

    iget v13, v15, Lepson/print/EPImage;->rotate:I

    move-object v2, v3

    move-object v3, v4

    move/from16 v4, p7

    move v5, v6

    move-object/from16 v6, p1

    move/from16 v7, p2

    move v8, v9

    move v9, v10

    move/from16 v10, p3

    move/from16 v11, p4

    move/from16 v12, p9

    invoke-virtual/range {v2 .. v13}, Lepson/print/EPImageCreator;->createMultiPrintImage_org(Landroid/content/Context;IILepson/print/EPImageList;IIIIIII)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v15, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    goto :goto_7

    .line 2195
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    aget v7, v8, v7

    aget v8, v8, v5

    move-object v2, v3

    move-object v3, v4

    move-object v4, v15

    move v5, v6

    move v6, v7

    move v7, v8

    move/from16 v8, p3

    move/from16 v9, p6

    move/from16 v10, p4

    move/from16 v11, p9

    move v12, v13

    invoke-virtual/range {v2 .. v12}, Lepson/print/EPImageCreator;->createPrintImage_origi(Landroid/content/Context;Lepson/print/EPImage;IIIIIIIZ)Ljava/lang/String;

    .line 2206
    iget-object v0, v15, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iput-object v0, v15, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    goto :goto_7

    .line 2000
    :cond_13
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 2211
    :catch_1
    :goto_6
    iput-object v14, v15, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    .line 2214
    :goto_7
    sget-object v2, Lepson/print/service/EpsonService;->mCancelLock:Ljava/lang/Object;

    monitor-enter v2

    .line 2215
    :try_start_6
    iput-object v14, v1, Lepson/print/service/EpsonService;->mEPImageCreator:Lepson/print/EPImageCreator;

    .line 2216
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2217
    iget-object v0, v15, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    return-object v0

    :catchall_0
    move-exception v0

    .line 2216
    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    .line 1992
    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public endJob()I
    .locals 1

    .line 2347
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->end_job()I

    move-result v0

    return v0
.end method

.method public endPage(Z)I
    .locals 1

    .line 2365
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->end_page(Z)I

    move-result p1

    return p1
.end method

.method public epsNotifyContinueable(I)V
    .locals 5

    .line 2574
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2575
    :try_start_0
    sget-object v1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 2579
    :try_start_1
    sget-object v3, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v3, p1}, Lepson/print/service/IEpsonServiceCallback;->onNotifyContinueable(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "NotificationService"

    .line 2583
    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2586
    :cond_0
    sget-object p1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2587
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public epsNotifyDataChange(Ljava/lang/String;)V
    .locals 13

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " *****************  epsNotifyDataChange() call: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const-string v0, ""

    const-string v1, ""

    const-string v2, "\\|\\|"

    .line 150
    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 152
    sget-object v3, Lepson/print/service/EpsonService;->mLockSearchingStatus:Ljava/lang/Object;

    monitor-enter v3

    .line 153
    :try_start_0
    sget-boolean v4, Lepson/print/service/EpsonService;->bNotifyPrinter:Z

    const/4 v5, 0x1

    if-nez v4, :cond_0

    .line 154
    sput-boolean v5, Lepson/print/service/EpsonService;->bNotifyPrinter:Z

    .line 156
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 158
    sget-object v4, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    const-string v3, ""

    const-string v6, ""

    .line 163
    array-length v7, v2

    const/4 v8, 0x5

    const/4 v9, 0x2

    const/4 v10, 0x4

    const/4 v11, 0x0

    const/4 v12, 0x3

    if-lt v7, v8, :cond_3

    .line 164
    aget-object p1, v2, v11

    .line 165
    aget-object v0, v2, v5

    .line 166
    aget-object v1, v2, v9

    .line 167
    aget-object v5, v2, v12

    if-eqz v5, :cond_1

    .line 168
    aget-object v3, v2, v12

    .line 170
    :cond_1
    aget-object v5, v2, v10

    if-eqz v5, :cond_2

    .line 171
    aget-object v6, v2, v10

    move-object v2, v6

    goto :goto_0

    :cond_2
    move-object v2, v6

    goto :goto_0

    .line 173
    :cond_3
    array-length v7, v2

    if-ne v7, v10, :cond_5

    .line 174
    aget-object p1, v2, v11

    .line 175
    aget-object v0, v2, v5

    .line 176
    aget-object v1, v2, v9

    .line 177
    aget-object v5, v2, v12

    if-eqz v5, :cond_4

    .line 178
    aget-object v3, v2, v12

    move-object v2, v6

    goto :goto_0

    :cond_4
    move-object v2, v6

    goto :goto_0

    .line 180
    :cond_5
    array-length v7, v2

    if-ne v7, v12, :cond_6

    .line 181
    aget-object p1, v2, v11

    .line 182
    aget-object v0, v2, v5

    .line 183
    aget-object v1, v2, v9

    move-object v2, v6

    goto :goto_0

    :cond_6
    move-object v2, v6

    .line 188
    :goto_0
    sget-object v5, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-ge v11, v12, :cond_7

    .line 191
    :try_start_2
    sget-object v5, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5, v11}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lepson/print/service/IEpsonServiceCallback;

    move-object v6, p1

    move-object v7, v0

    move-object v8, v1

    move-object v9, v3

    move-object v10, v2

    invoke-interface/range {v5 .. v10}, Lepson/print/service/IEpsonServiceCallback;->onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v5

    :try_start_3
    const-string v6, "Epson"

    .line 196
    invoke-virtual {v5}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 200
    :cond_7
    sget-object p1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 202
    monitor-exit v4

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    .line 156
    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw p1
.end method

.method public epsNotifyError(IIZ)V
    .locals 5

    .line 2631
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2632
    :try_start_0
    sget-object v1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 2635
    :try_start_1
    sget-object v3, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v3, p1, p2, p3}, Lepson/print/service/IEpsonServiceCallback;->onNotifyError(IIZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "NotificationService"

    .line 2639
    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2642
    :cond_0
    sget-object p1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2643
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public epsNotifyPage(I)V
    .locals 5

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " \u0414 percent = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;)V

    .line 128
    sget-object v0, Lepson/print/service/EpsonService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 129
    :try_start_0
    sget-object v1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 132
    :try_start_1
    sget-object v3, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lepson/print/service/IEpsonServiceCallback;

    const/4 v4, 0x1

    invoke-interface {v3, v4, p1}, Lepson/print/service/IEpsonServiceCallback;->onNotifyProgress(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "Epson"

    .line 136
    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    :cond_0
    sget-object p1, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {p1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 140
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method epsonConnectChangePrintSetting()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2263
    iget-object v0, p0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    invoke-virtual {v0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectChangePrintSetting()I

    move-result v0

    return v0
.end method

.method epsonConnectCreateJob(ILjava/lang/String;IIIIIIIIIIIII)I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    move-object/from16 v0, p0

    .line 2243
    iget-object v1, v0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    move/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    move/from16 v15, p14

    move/from16 v16, p15

    invoke-virtual/range {v1 .. v16}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectCreateJob(ILjava/lang/String;IIIIIIIIIIIII)I

    move-result v1

    return v1
.end method

.method epsonConnectEndJob()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2251
    iget-object v0, p0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    invoke-virtual {v0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectEndJob()I

    move-result v0

    return v0
.end method

.method epsonConnectStartPrint(II)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2269
    iget-object v0, p0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    invoke-virtual {v0, p1, p2}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectStartPrint(II)I

    move-result p1

    return p1
.end method

.method epsonConnectUploadFile(Ljava/lang/String;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2257
    iget-object v0, p0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    invoke-virtual {v0, p1, p2}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectUploadFile(Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method public getCancelPrinting()Z
    .locals 1

    .line 2424
    sget-boolean v0, Lepson/print/service/EpsonService;->bCancelPrinting:Z

    return v0
.end method

.method public getLang()I
    .locals 1

    .line 2359
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_lang()I

    move-result v0

    return v0
.end method

.method public getLocalApplicationContext()Landroid/content/Context;
    .locals 1

    .line 2441
    invoke-virtual {p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getLocalPrinterLayout1AreaSize(I)[I
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 2389
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_printable_area()[I

    move-result-object v0

    if-nez p1, :cond_0

    return-object v0

    .line 2394
    :cond_0
    iget-object v1, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_start_job_resolution2()I

    move-result v1

    const/4 v2, 0x0

    .line 2395
    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-static {p1, v2, v0, v1}, Lepson/common/Info_paper;->getMultiLayoutArea(IIII)[I

    move-result-object p1

    return-object p1
.end method

.method public getPrintLockObject()Ljava/lang/Object;
    .locals 1

    .line 2412
    sget-object v0, Lepson/print/service/EpsonService;->mPrinting:Ljava/lang/Object;

    return-object v0
.end method

.method public getPrintableArea()[I
    .locals 1

    .line 2383
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_printable_area()[I

    move-result-object v0

    return-object v0
.end method

.method public getPrinterStatus()[I
    .locals 1

    .line 2406
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_printer_status()[I

    move-result-object v0

    return-object v0
.end method

.method public getStartJobResolution()I
    .locals 1

    .line 2335
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_start_job_resolution2()I

    move-result v0

    return v0
.end method

.method public initImage(Ljava/lang/String;)I
    .locals 1

    .line 2371
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->init_image(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public javaDebugCB()V
    .locals 2

    .line 121
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lepson/print/service/EpsonService;->debugString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v0, ""

    .line 122
    iput-object v0, p0, Lepson/print/service/EpsonService;->debugString:Ljava/lang/String;

    return-void
.end method

.method public notifyEndJob(I)V
    .locals 0

    .line 2436
    invoke-static {p1}, Lepson/print/service/EpsonService;->onNotifyEndJob(I)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .line 2457
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 2459
    :try_start_0
    sget-boolean p1, Lepson/print/service/EpsonService;->isInitDriver:Z

    if-nez p1, :cond_0

    .line 2460
    iget-object p1, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->init_driver(Landroid/content/Context;Ljava/lang/String;)I

    const/4 p1, 0x1

    .line 2461
    sput-boolean p1, Lepson/print/service/EpsonService;->isInitDriver:Z

    .line 2464
    :cond_0
    invoke-direct {p0}, Lepson/print/service/EpsonService;->updateLoginInfo()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2466
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2468
    :goto_0
    iget-object p1, p0, Lepson/print/service/EpsonService;->mBinder:Lepson/print/service/IEpsonService$Stub;

    return-object p1
.end method

.method public onCreate()V
    .locals 0

    .line 2451
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 2452
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    .line 2511
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 2512
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 3

    .line 2473
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 2474
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->release_driver()I

    const/4 v0, 0x0

    .line 2475
    sput-boolean v0, Lepson/print/service/EpsonService;->isInitDriver:Z

    .line 2477
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->init_driver(Landroid/content/Context;Ljava/lang/String;)I

    const/4 v0, 0x1

    .line 2478
    sput-boolean v0, Lepson/print/service/EpsonService;->isInitDriver:Z

    .line 2485
    invoke-static {p0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object v0

    const-string v1, "PRINTER_CLIENT_ID"

    const/4 v2, 0x0

    .line 2486
    invoke-virtual {v0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/service/EpsonService;->clientId:Ljava/lang/String;

    .line 2487
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u25b2clientId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/service/EpsonService;->clientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 2489
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 2490
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/service/EpsonService;->mailAddress:Ljava/lang/String;

    .line 2491
    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->getRemotePrinterAccessKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/service/EpsonService;->accessKey:Ljava/lang/String;

    .line 2493
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .line 2498
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 2501
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setHanlder(Landroid/os/Handler;)V

    .line 2503
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->release_driver()I

    const/4 v0, 0x0

    .line 2504
    sput-boolean v0, Lepson/print/service/EpsonService;->isInitDriver:Z

    .line 2506
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result p1

    return p1
.end method

.method public pageSneedRotate(I)Z
    .locals 1

    .line 2400
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->pageS_needRotate2(I)Z

    move-result p1

    return p1
.end method

.method public printPage()I
    .locals 1

    .line 2353
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->print_page()I

    move-result v0

    return v0
.end method

.method public releaseImage()I
    .locals 1

    .line 2377
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->release_image()I

    move-result v0

    return v0
.end method

.method public setCancelPrinting(Z)V
    .locals 0

    .line 2418
    sput-boolean p1, Lepson/print/service/EpsonService;->bCancelPrinting:Z

    return-void
.end method

.method setEpsonConnectCopies(I)V
    .locals 1

    .line 2274
    iget-object v0, p0, Lepson/print/service/EpsonService;->epsJobattrib:Lepson/print/ecclient/EpsJobAttrib;

    iput p1, v0, Lepson/print/ecclient/EpsJobAttrib;->mCopies:I

    return-void
.end method

.method public setPrinting(Z)V
    .locals 0

    .line 2430
    iput-boolean p1, p0, Lepson/print/service/EpsonService;->bPrinting:Z

    return-void
.end method

.method public startJob(IIIIIIIIIIIZ)I
    .locals 15

    .line 2322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "service:: paperSize = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " paperType = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v4, p2

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " layout = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v5, p3

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;)V

    move-object v0, p0

    .line 2323
    iget-object v2, v0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    move/from16 v3, p1

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v14, p12

    invoke-virtual/range {v2 .. v14}, Lcom/epson/mobilephone/common/escpr/EscprLib;->start_job2(IIIIIIIIIIIZ)I

    move-result v1

    return v1
.end method

.method public startPage()I
    .locals 1

    .line 2341
    iget-object v0, p0, Lepson/print/service/EpsonService;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->start_page()I

    move-result v0

    return v0
.end method

.method public updateApfProgress(I)Z
    .locals 1

    .line 1665
    sget-boolean v0, Lepson/print/service/EpsonService;->bCancelPrinting:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 v0, 0x2

    .line 1669
    invoke-static {v0, p1}, Lepson/print/service/EpsonService;->notifyProgress(II)V

    const/4 p1, 0x1

    return p1
.end method

.method public waitIfSimpleAp()V
    .locals 6

    .line 2285
    invoke-virtual {p0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    .line 2287
    :try_start_0
    new-array v0, v0, [I

    const-wide/16 v2, 0x1388

    .line 2291
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2296
    :cond_0
    invoke-virtual {p0}, Lepson/print/service/EpsonService;->getPrinterStatus()[I

    move-result-object v3

    const-wide/16 v4, 0x3e8

    .line 2298
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    add-int/2addr v2, v1

    const/16 v4, 0x708

    if-lt v2, v4, :cond_1

    goto :goto_0

    .line 2306
    :cond_1
    aget v3, v3, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 2308
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_2
    :goto_0
    return-void
.end method

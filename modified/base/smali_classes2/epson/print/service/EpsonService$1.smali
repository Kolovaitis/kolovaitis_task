.class Lepson/print/service/EpsonService$1;
.super Lepson/print/service/IEpsonService$Stub;
.source "EpsonService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/service/EpsonService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mEcClientLib:Lepson/print/ecclient/EcClientLib;

.field mEpsonConnectlogin:Z

.field mRemotePrintMaxFileSize:I

.field final synthetic this$0:Lepson/print/service/EpsonService;


# direct methods
.method constructor <init>(Lepson/print/service/EpsonService;)V
    .locals 1

    .line 217
    iput-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-direct {p0}, Lepson/print/service/IEpsonService$Stub;-><init>()V

    const/4 p1, 0x0

    .line 218
    iput p1, p0, Lepson/print/service/EpsonService$1;->mRemotePrintMaxFileSize:I

    .line 220
    new-instance v0, Lepson/print/ecclient/EcClientLib;

    invoke-direct {v0}, Lepson/print/ecclient/EcClientLib;-><init>()V

    iput-object v0, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    .line 222
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->Initialize()Z

    .line 224
    iput-boolean p1, p0, Lepson/print/service/EpsonService$1;->mEpsonConnectlogin:Z

    return-void
.end method


# virtual methods
.method public EpsonConnectCancel()V
    .locals 1

    .line 1478
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->cancel()V

    return-void
.end method

.method public EpsonConnectChangePrintSetting()I
    .locals 5

    const-string v0, "EpsonService"

    const-string v1, "EpsonConnectChangePrintSetting() called."

    .line 1218
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v1

    iget-object v2, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    .line 1222
    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2300(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    .line 1223
    invoke-static {v3}, Lepson/print/service/EpsonService;->access$2400(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 1221
    invoke-virtual {v0, v1, v2, v3, v4}, Lepson/print/ecclient/EcClientLib;->ChangePrintSetting(Lepson/print/ecclient/EpsJobAttrib;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public EpsonConnectCheckSupportedFileType(Ljava/lang/String;)I
    .locals 1

    const-string p1, "EpsonService"

    const-string v0, "EpsonConnectCheckSupportedFileType() called."

    .line 1279
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public EpsonConnectCreateJob(ILjava/lang/String;IIIIIIIIIIIII)I
    .locals 9

    move-object v1, p0

    move v0, p1

    const-string v2, "EpsonService"

    const-string v3, "EpsonConnectCreateJob() called."

    .line 1138
    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146
    iget-object v2, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v2}, Lepson/print/ecclient/EcClientLib;->resetCancel()V

    .line 1149
    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v2

    move v3, p3

    iput v3, v2, Lepson/print/ecclient/EpsJobAttrib;->mMediaSizeIdx:I

    .line 1150
    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v2

    move v3, p4

    iput v3, v2, Lepson/print/ecclient/EpsJobAttrib;->mMediaTypeIdx:I

    .line 1151
    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v2

    move v3, p5

    iput v3, v2, Lepson/print/ecclient/EpsJobAttrib;->mPrintLayout:I

    .line 1152
    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v2

    move v3, p6

    iput v3, v2, Lepson/print/ecclient/EpsJobAttrib;->mPrintQuality:I

    .line 1153
    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v2

    move/from16 v3, p7

    iput v3, v2, Lepson/print/ecclient/EpsJobAttrib;->mPaperSource:I

    const/4 v2, 0x2

    const/4 v3, 0x1

    move/from16 v4, p8

    if-ne v4, v3, :cond_0

    .line 1159
    iget-object v4, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v4}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v4

    iput v2, v4, Lepson/print/ecclient/EpsJobAttrib;->mColorMode:I

    goto :goto_0

    .line 1161
    :cond_0
    iget-object v4, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v4}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v4

    iput v3, v4, Lepson/print/ecclient/EpsJobAttrib;->mColorMode:I

    .line 1163
    :goto_0
    iget-object v4, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v4}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v4

    move/from16 v5, p9

    iput v5, v4, Lepson/print/ecclient/EpsJobAttrib;->mBrightness:I

    .line 1164
    iget-object v4, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v4}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v4

    move/from16 v5, p10

    iput v5, v4, Lepson/print/ecclient/EpsJobAttrib;->mContrast:I

    .line 1165
    iget-object v4, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v4}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v4

    move/from16 v5, p11

    iput v5, v4, Lepson/print/ecclient/EpsJobAttrib;->mSaturation:I

    .line 1166
    iget-object v4, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v4}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v4

    move/from16 v5, p12

    iput v5, v4, Lepson/print/ecclient/EpsJobAttrib;->mPrintDirection:I

    .line 1167
    iget-object v4, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v4}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v4

    move/from16 v5, p13

    iput v5, v4, Lepson/print/ecclient/EpsJobAttrib;->mDuplex:I

    const/4 v4, 0x0

    .line 1169
    invoke-virtual {p0, v4}, Lepson/print/service/EpsonService$1;->ensureLogin(Z)I

    move-result v5

    if-eqz v5, :cond_1

    return v5

    :cond_1
    if-ne v0, v3, :cond_2

    .line 1178
    new-instance v3, Ljava/io/File;

    move-object v6, p2

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1179
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    long-to-int v4, v3

    move/from16 v3, p14

    move/from16 v7, p15

    goto :goto_1

    :cond_2
    move-object v6, p2

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 1185
    :goto_1
    :try_start_0
    invoke-static {p1, p2}, Lepson/print/ecclient/EcClientLibUtil;->getCreateJobFileName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v6, :cond_4

    if-eq v0, v2, :cond_3

    const-string v2, "iPrint"

    goto :goto_2

    :cond_3
    const-string v2, "iPrint Photo"

    goto :goto_2

    :cond_4
    move-object v2, v6

    .line 1204
    :goto_2
    :try_start_1
    iget-object v6, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v8, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    .line 1205
    invoke-static {v8}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v8

    move-object p2, v6

    move p3, p1

    move-object p4, v2

    move p5, v4

    move-object p6, v8

    move/from16 p7, v3

    move/from16 p8, v7

    .line 1204
    invoke-virtual/range {p2 .. p8}, Lepson/print/ecclient/EcClientLib;->CreateJob(ILjava/lang/String;ILepson/print/ecclient/EpsJobAttrib;II)I

    move-result v5

    .line 1207
    iget-object v0, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v0, v0, Lepson/print/ecclient/EcClientLib;->mEccJobInfo:Lepson/print/ecclient/EccJobInfo;

    iget v0, v0, Lepson/print/ecclient/EccJobInfo;->mMaxFileSize:I

    iput v0, v1, Lepson/print/service/EpsonService$1;->mRemotePrintMaxFileSize:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    .line 1210
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    :goto_3
    return v5

    :catch_1
    move-exception v0

    move-object v2, v0

    .line 1188
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, -0x1

    return v0
.end method

.method public EpsonConnectEndJob()I
    .locals 2

    const-string v0, "EpsonService"

    const-string v1, "EpsonConnectEndJob() called."

    .line 1260
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->EndJob()I

    move-result v0

    .line 1271
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v1}, Lepson/print/ecclient/EcClientLib;->resetCancel()V

    return v0
.end method

.method public EpsonConnectGetPreview(ILjava/lang/String;)I
    .locals 3

    .line 1313
    invoke-static {}, Lepson/print/service/EpsonService;->access$2500()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    const-string v1, "EpsonService"

    const-string v2, "EpsonConnectGetPreview() called."

    .line 1314
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v1, p1, p2}, Lepson/print/ecclient/EcClientLib;->DownloadPreview(ILjava/lang/String;)I

    move-result p1

    .line 1318
    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public EpsonConnectGetRemotePrintMaxFileSize()I
    .locals 1

    .line 1472
    iget v0, p0, Lepson/print/service/EpsonService$1;->mRemotePrintMaxFileSize:I

    return v0
.end method

.method public EpsonConnectGetRenderingStatus([I)I
    .locals 5

    .line 1292
    invoke-static {}, Lepson/print/service/EpsonService;->access$2500()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1293
    :try_start_0
    aput v1, p1, v2

    .line 1294
    aput v2, p1, v1

    .line 1297
    iget-object v3, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v3}, Lepson/print/ecclient/EcClientLib;->GetRenderingStatus()I

    move-result v3

    if-nez v3, :cond_0

    .line 1301
    iget-object v4, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget v4, v4, Lepson/print/ecclient/EcClientLib;->mRenderStatus:I

    aput v4, p1, v2

    .line 1302
    iget-object v2, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget v2, v2, Lepson/print/ecclient/EcClientLib;->mCompletePage:I

    aput v2, p1, v1

    .line 1304
    :cond_0
    monitor-exit v0

    return v3

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public EpsonConnectGetSupportedMedia(ILjava/lang/String;Ljava/lang/String;)I
    .locals 16

    move-object/from16 v1, p0

    move/from16 v0, p1

    .line 1326
    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1329
    iget-object v3, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    if-eqz p2, :cond_0

    .line 1331
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 1333
    iput-boolean v4, v1, Lepson/print/service/EpsonService$1;->mEpsonConnectlogin:Z

    .line 1336
    invoke-static/range {p2 .. p2}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1337
    invoke-static/range {p3 .. p3}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1338
    iget-object v6, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v6}, Lepson/print/service/EpsonService;->access$2600(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v6

    .line 1339
    iget-object v7, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v7, v3, v5, v6}, Lepson/print/ecclient/EcClientLib;->Login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    return v3

    .line 1344
    :cond_0
    invoke-virtual {v1, v4}, Lepson/print/service/EpsonService$1;->ensureLogin(Z)I

    move-result v3

    if-eqz v3, :cond_1

    return v3

    .line 1350
    :cond_1
    iget-object v3, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v3}, Lepson/print/ecclient/EcClientLib;->GetCapability()I

    move-result v3

    if-eqz v3, :cond_2

    return v3

    :cond_2
    packed-switch v0, :pswitch_data_0

    const/4 v2, -0x1

    const-string v0, "EpsonService"

    const-string v3, "sourceType is invalid."

    .line 1368
    invoke-static {v0, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    .line 1365
    :pswitch_0
    iget-object v5, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v5, v5, Lepson/print/ecclient/EcClientLib;->mMediaAtLocal:Lepson/print/ecclient/EpsSupportedMedia;

    goto :goto_0

    .line 1359
    :pswitch_1
    iget-object v5, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v5, v5, Lepson/print/ecclient/EcClientLib;->mMediaAtPhoto:Lepson/print/ecclient/EpsSupportedMedia;

    goto :goto_0

    .line 1362
    :pswitch_2
    iget-object v5, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v5, v5, Lepson/print/ecclient/EcClientLib;->mMediaAtRemote:Lepson/print/ecclient/EpsSupportedMedia;

    .line 1373
    :goto_0
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1374
    new-instance v7, Ljava/io/DataOutputStream;

    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v7, v8}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v8, 0x20

    .line 1377
    new-array v8, v8, [B

    .line 1378
    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 1379
    sget-object v10, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1380
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1382
    iget v10, v5, Lepson/print/ecclient/EpsSupportedMedia;->mJpegSizeLimit:I

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1383
    iget-object v10, v5, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    array-length v10, v10

    .line 1384
    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1386
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v11

    invoke-virtual {v7, v8, v4, v11}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1387
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v10, :cond_4

    .line 1390
    iget-object v12, v5, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v12, v12, v11

    .line 1392
    iget v13, v12, Lepson/print/ecclient/EpsMediaSize;->mMediaSizeID:I

    invoke-virtual {v9, v13}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1393
    iget-object v13, v12, Lepson/print/ecclient/EpsMediaSize;->mTypeList:[Lepson/print/ecclient/EpsMediaType;

    array-length v13, v13

    .line 1394
    invoke-virtual {v9, v13}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1396
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v14

    invoke-virtual {v7, v8, v4, v14}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1397
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    const/4 v14, 0x0

    :goto_2
    if-ge v14, v13, :cond_3

    .line 1400
    iget-object v15, v12, Lepson/print/ecclient/EpsMediaSize;->mTypeList:[Lepson/print/ecclient/EpsMediaType;

    aget-object v15, v15, v14

    .line 1403
    iget v3, v15, Lepson/print/ecclient/EpsMediaType;->mMediaTypeId:I

    invoke-virtual {v9, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1405
    iget v3, v15, Lepson/print/ecclient/EpsMediaType;->mLayout:I

    invoke-virtual {v9, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1406
    iget v3, v15, Lepson/print/ecclient/EpsMediaType;->mQuality:I

    invoke-virtual {v9, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1407
    iget v3, v15, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    invoke-virtual {v9, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1409
    iget v3, v15, Lepson/print/ecclient/EpsMediaType;->mDuplex:I

    invoke-virtual {v9, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1411
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v7, v8, v4, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1412
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1415
    :cond_4
    iget v3, v5, Lepson/print/ecclient/EpsSupportedMedia;->mResolution:I

    invoke-virtual {v9, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v3, 0x1

    .line 1416
    invoke-virtual {v9, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1418
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v7, v8, v4, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1419
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1420
    invoke-virtual {v7}, Ljava/io/DataOutputStream;->close()V

    const-string v3, "EpsonService"

    .line 1422
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "finishu output SUPPORTED_MEDIA for EC. filename = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " sourceType = "

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " filesize = "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1423
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1422
    invoke-static {v3, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    new-instance v0, Ljava/io/File;

    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1431
    invoke-static {v6, v0}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v4

    :catch_0
    move-exception v0

    const-string v2, "EpsonService"

    const-string v3, "get_epsonconnect_supported_media(): IOExceptin."

    .line 1434
    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const/4 v2, -0x1

    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public EpsonConnectStartPrint(II)I
    .locals 2

    const-string v0, "EpsonService"

    const-string v1, "EpsonConnectStartPrint() called."

    .line 1249
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0, p1, p2}, Lepson/print/ecclient/EcClientLib;->StartPrint(II)I

    move-result p1

    return p1
.end method

.method public EpsonConnectUpdatePrinterSettings(Ljava/lang/String;)I
    .locals 16

    move-object/from16 v1, p0

    const-string v0, "EpsonService"

    const-string v2, "EpsonConnectUpdatePrinterSettings() called."

    .line 868
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    iget-object v0, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    move-object/from16 v2, p1

    invoke-static {v0, v2}, Lepson/print/screen/PrintSetting;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lepson/print/screen/PrintSetting;

    move-result-object v0

    .line 870
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 872
    iget v2, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 873
    iget v3, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 874
    iget v4, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 875
    iget v5, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 876
    iget v6, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 877
    iget v7, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 878
    iget v8, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 889
    :try_start_0
    iget-object v10, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 894
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getUpdateSettingsRemoteSourceType()I

    move-result v10

    const/4 v12, 0x0

    const/4 v13, 0x1

    .line 895
    invoke-virtual {v1, v13, v10, v12, v12}, Lepson/print/service/EpsonService$1;->getSupportedMedia(ZILjava/lang/String;Ljava/lang/String;)I

    move-result v12

    packed-switch v10, :pswitch_data_0

    const-string v0, "EpsonService"

    goto/16 :goto_e

    .line 909
    :pswitch_0
    iget-object v10, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v10, v10, Lepson/print/ecclient/EcClientLib;->mMediaAtLocal:Lepson/print/ecclient/EpsSupportedMedia;

    .line 910
    iget-object v14, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget v14, v14, Lepson/print/ecclient/EcClientLib;->mColorModeAtLocal:I

    goto :goto_0

    .line 901
    :pswitch_1
    iget-object v10, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v10, v10, Lepson/print/ecclient/EcClientLib;->mMediaAtPhoto:Lepson/print/ecclient/EpsSupportedMedia;

    .line 902
    iget-object v14, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget v14, v14, Lepson/print/ecclient/EcClientLib;->mColorModeAtPhoto:I

    goto :goto_0

    .line 905
    :pswitch_2
    iget-object v10, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget-object v10, v10, Lepson/print/ecclient/EcClientLib;->mMediaAtRemote:Lepson/print/ecclient/EpsSupportedMedia;

    .line 906
    iget-object v14, v1, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget v14, v14, Lepson/print/ecclient/EcClientLib;->mColorModeAtRemote:I

    :goto_0
    if-nez v12, :cond_24

    const/4 v12, 0x0

    .line 921
    :goto_1
    iget-object v9, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    array-length v9, v9

    if-ge v12, v9, :cond_1

    .line 922
    iget-object v9, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v9, v9, v12

    iget v9, v9, Lepson/print/ecclient/EpsMediaSize;->mMediaSizeID:I

    if-ne v2, v9, :cond_0

    const/4 v2, 0x0

    goto :goto_2

    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x1

    const/4 v12, 0x0

    :goto_2
    if-ne v2, v13, :cond_4

    .line 931
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperSize()I

    move-result v9

    .line 932
    :goto_3
    iget-object v15, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    array-length v15, v15

    if-ge v11, v15, :cond_3

    .line 933
    iget-object v15, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v15, v15, v11

    iget v15, v15, Lepson/print/ecclient/EpsMediaSize;->mMediaSizeID:I

    if-ne v9, v15, :cond_2

    const-string v2, "EpsonService"

    .line 936
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "EpsonConnectUpdatePrinterSettings paperSize_index = "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_4

    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_3
    move v11, v12

    :goto_4
    if-ne v2, v13, :cond_5

    const/4 v11, 0x0

    goto :goto_5

    :cond_4
    move v11, v12

    :cond_5
    :goto_5
    const/4 v2, 0x0

    .line 949
    :goto_6
    iget-object v9, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v9, v9, v11

    iget-object v9, v9, Lepson/print/ecclient/EpsMediaSize;->mTypeList:[Lepson/print/ecclient/EpsMediaType;

    array-length v9, v9

    if-ge v2, v9, :cond_7

    .line 950
    iget-object v9, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v9, v9, v11

    iget-object v9, v9, Lepson/print/ecclient/EpsMediaSize;->mTypeList:[Lepson/print/ecclient/EpsMediaType;

    aget-object v9, v9, v2

    iget v9, v9, Lepson/print/ecclient/EpsMediaType;->mMediaTypeId:I

    if-ne v3, v9, :cond_6

    move v3, v2

    const/4 v2, 0x0

    goto :goto_7

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_7
    const/4 v2, 0x1

    const/4 v3, 0x0

    :goto_7
    if-eqz v2, :cond_a

    .line 958
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperType()I

    move-result v9

    const/4 v12, 0x0

    .line 959
    :goto_8
    iget-object v15, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v15, v15, v11

    iget-object v15, v15, Lepson/print/ecclient/EpsMediaSize;->mTypeList:[Lepson/print/ecclient/EpsMediaType;

    array-length v15, v15

    if-ge v12, v15, :cond_9

    .line 960
    iget-object v15, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v15, v15, v11

    iget-object v15, v15, Lepson/print/ecclient/EpsMediaSize;->mTypeList:[Lepson/print/ecclient/EpsMediaType;

    aget-object v15, v15, v12

    iget v15, v15, Lepson/print/ecclient/EpsMediaType;->mMediaTypeId:I

    if-ne v9, v15, :cond_8

    const-string v2, "EpsonService"

    .line 963
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EpsonConnectUpdatePrinterSettings paperType_index = "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v12

    const/4 v2, 0x0

    goto :goto_9

    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    :cond_9
    :goto_9
    if-ne v2, v13, :cond_a

    const/4 v3, 0x0

    .line 974
    :cond_a
    iget-object v2, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v2, v2, v11

    iget v2, v2, Lepson/print/ecclient/EpsMediaSize;->mMediaSizeID:I

    .line 975
    iget-object v9, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v9, v9, v11

    iget-object v9, v9, Lepson/print/ecclient/EpsMediaSize;->mTypeList:[Lepson/print/ecclient/EpsMediaType;

    aget-object v9, v9, v3

    iget v9, v9, Lepson/print/ecclient/EpsMediaType;->mMediaTypeId:I

    .line 976
    iget-object v10, v10, Lepson/print/ecclient/EpsSupportedMedia;->mSizeList:[Lepson/print/ecclient/EpsMediaSize;

    aget-object v10, v10, v11

    iget-object v10, v10, Lepson/print/ecclient/EpsMediaSize;->mTypeList:[Lepson/print/ecclient/EpsMediaType;

    aget-object v3, v10, v3

    .line 979
    iget v10, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    and-int/2addr v10, v6

    const/16 v12, 0x10

    const/16 v15, 0x8

    const/4 v11, 0x4

    if-ne v10, v6, :cond_b

    move v10, v6

    goto :goto_a

    .line 981
    :cond_b
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    and-int/2addr v6, v13

    if-ne v6, v13, :cond_c

    const/4 v10, 0x1

    goto :goto_a

    .line 983
    :cond_c
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    const/4 v10, 0x2

    and-int/2addr v6, v10

    if-ne v6, v10, :cond_d

    const/4 v10, 0x2

    goto :goto_a

    .line 985
    :cond_d
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    and-int/2addr v6, v11

    if-ne v6, v11, :cond_e

    const/4 v10, 0x4

    goto :goto_a

    .line 987
    :cond_e
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    and-int/2addr v6, v15

    if-ne v6, v15, :cond_f

    const/16 v10, 0x8

    goto :goto_a

    .line 989
    :cond_f
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    and-int/2addr v6, v12

    if-ne v6, v12, :cond_10

    const/16 v10, 0x10

    goto :goto_a

    .line 991
    :cond_10
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    const/16 v10, 0x80

    and-int/2addr v6, v10

    if-ne v6, v10, :cond_11

    goto :goto_a

    .line 993
    :cond_11
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    const v10, 0x8000

    and-int/2addr v6, v10

    if-ne v6, v10, :cond_12

    goto :goto_a

    .line 995
    :cond_12
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    const/16 v12, 0x20

    and-int/2addr v6, v12

    if-ne v6, v12, :cond_13

    const/16 v10, 0x20

    goto :goto_a

    .line 997
    :cond_13
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mPaperSource:I

    const/16 v10, 0x40

    and-int/2addr v6, v10

    if-ne v6, v10, :cond_14

    goto :goto_a

    :cond_14
    const/4 v10, 0x2

    .line 1005
    :goto_a
    iget v6, v3, Lepson/print/ecclient/EpsMediaType;->mLayout:I

    and-int/2addr v6, v4

    if-ne v6, v4, :cond_16

    if-nez v4, :cond_15

    const/4 v15, 0x2

    goto :goto_b

    :cond_15
    move v15, v4

    goto :goto_b

    .line 1015
    :cond_16
    iget v4, v3, Lepson/print/ecclient/EpsMediaType;->mLayout:I

    and-int/2addr v4, v13

    if-ne v4, v13, :cond_17

    const/4 v15, 0x1

    goto :goto_b

    .line 1017
    :cond_17
    iget v4, v3, Lepson/print/ecclient/EpsMediaType;->mLayout:I

    const/4 v6, 0x2

    and-int/2addr v4, v6

    if-ne v4, v6, :cond_18

    const/4 v15, 0x2

    goto :goto_b

    .line 1019
    :cond_18
    iget v4, v3, Lepson/print/ecclient/EpsMediaType;->mLayout:I

    and-int/2addr v4, v11

    if-ne v4, v11, :cond_19

    const/4 v15, 0x4

    goto :goto_b

    .line 1021
    :cond_19
    iget v4, v3, Lepson/print/ecclient/EpsMediaType;->mLayout:I

    and-int/2addr v4, v15

    if-ne v4, v15, :cond_1a

    goto :goto_b

    :cond_1a
    const/4 v15, 0x2

    .line 1030
    :goto_b
    iget v4, v3, Lepson/print/ecclient/EpsMediaType;->mQuality:I

    and-int/2addr v4, v5

    if-ne v4, v5, :cond_1b

    if-nez v5, :cond_1f

    const/4 v5, 0x2

    goto :goto_c

    .line 1040
    :cond_1b
    iget v4, v3, Lepson/print/ecclient/EpsMediaType;->mQuality:I

    and-int/2addr v4, v13

    if-ne v4, v13, :cond_1c

    const/4 v5, 0x1

    goto :goto_c

    .line 1042
    :cond_1c
    iget v4, v3, Lepson/print/ecclient/EpsMediaType;->mQuality:I

    const/4 v5, 0x2

    and-int/2addr v4, v5

    if-ne v4, v5, :cond_1d

    const/4 v5, 0x2

    goto :goto_c

    .line 1044
    :cond_1d
    iget v4, v3, Lepson/print/ecclient/EpsMediaType;->mQuality:I

    and-int/2addr v4, v11

    if-ne v4, v11, :cond_1e

    const/4 v5, 0x4

    goto :goto_c

    :cond_1e
    const/4 v5, 0x2

    .line 1054
    :cond_1f
    :goto_c
    iget v3, v3, Lepson/print/ecclient/EpsMediaType;->mDuplex:I

    if-nez v3, :cond_20

    if-eqz v7, :cond_20

    const/4 v7, 0x0

    :cond_20
    shl-int v3, v13, v8

    and-int/2addr v3, v14

    if-eqz v3, :cond_21

    goto :goto_d

    :cond_21
    and-int/lit8 v3, v14, 0x1

    if-eqz v3, :cond_22

    const/4 v8, 0x0

    goto :goto_d

    :cond_22
    const/4 v3, 0x2

    and-int/2addr v3, v14

    if-eqz v3, :cond_23

    const/4 v8, 0x1

    goto :goto_d

    :cond_23
    const/4 v8, 0x0

    .line 1084
    :goto_d
    iput v2, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 1085
    iput v9, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 1086
    iput v15, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    const/4 v2, 0x0

    .line 1087
    iput v2, v0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    .line 1088
    iput v5, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 1089
    iput v10, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 1090
    iput v7, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 1091
    iput v8, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 1092
    iput v2, v0, Lepson/print/screen/PrintSetting;->printdate:I

    .line 1093
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->saveSettings()V

    return v2

    .line 1101
    :cond_24
    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    .line 1103
    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v3

    .line 1104
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1105
    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v2

    .line 1106
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1112
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperSize()I

    move-result v2

    iput v2, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 1113
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperType()I

    move-result v2

    iput v2, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    const/4 v2, 0x2

    .line 1114
    iput v2, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    const/4 v3, 0x0

    .line 1115
    iput v3, v0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    .line 1116
    iput v2, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 1117
    iput v2, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 1118
    iput v3, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 1119
    iput v3, v0, Lepson/print/screen/PrintSetting;->printdate:I

    .line 1120
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->saveSettings()V

    const/4 v2, -0x1

    return v2

    :goto_e
    const-string v2, "remoteSourceType is invalid."

    .line 913
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, -0x1

    return v2

    :catch_0
    move-exception v0

    .line 1125
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, -0x1

    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public EpsonConnectUploadFile(Ljava/lang/String;I)I
    .locals 3

    const-string v0, "EpsonService"

    .line 1231
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EpsonConnectUploadFile() called. - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1234
    invoke-virtual {p0, v0}, Lepson/print/service/EpsonService$1;->ensureLogin(Z)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 1239
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1240
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 1241
    iget-object v2, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    long-to-int v1, v0

    invoke-virtual {v2, p1, v1, p2}, Lepson/print/ecclient/EcClientLib;->UploadFile(Ljava/lang/String;II)I

    move-result p1

    return p1
.end method

.method public cancelPrint()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 404
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->cancel_print()I

    move-result v0

    return v0
.end method

.method public declared-synchronized cancelSearchPrinter()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "Epson"

    const-string v1, "cancelSearchPrinter() call"

    .line 290
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "Epson"

    const-string v2, "cancelSearchPrinter() finish1"

    .line 292
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    monitor-exit p0

    return v1

    .line 296
    :cond_0
    :try_start_1
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/service/EpsonService$SearchingThread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v0, "EpsonService"

    const-string v2, "searchingPrinter.join() enter"

    .line 300
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Lepson/print/service/EpsonService$SearchingThread;->join(J)V

    const/4 v0, 0x0

    .line 302
    :goto_0
    sget-object v4, Ljava/lang/Thread$State;->RUNNABLE:Ljava/lang/Thread$State;

    iget-object v5, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v5}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object v5

    invoke-virtual {v5}, Lepson/print/service/EpsonService$SearchingThread;->getState()Ljava/lang/Thread$State;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Thread$State;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v4, v0, 0x1

    const/16 v5, 0x3c

    if-le v0, v5, :cond_1

    const-string v0, "EpsonService"

    const-string v2, "mSearchThread.join() timeout"

    .line 304
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v0, "EpsonService"

    .line 307
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "retry doCancelFindPrinter() "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/service/EpsonService$SearchingThread;->interrupt()V

    .line 310
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lepson/print/service/EpsonService$SearchingThread;->join(J)V

    move v0, v4

    goto :goto_0

    :cond_2
    :goto_1
    const-string v0, "EpsonService"

    const-string v2, "searchingPrinter.join() leave"

    .line 312
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 314
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_2
    const-string v0, "Epson"

    const-string v2, "cancelSearchPrinter() finish"

    .line 317
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 318
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public confirmCancel(Z)I
    .locals 5

    const-string v0, "LAM DAI HIEP"

    const-string v1, "confirmCancel() call"

    .line 561
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "cancel "

    .line 562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "iscancel "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 567
    :cond_0
    invoke-static {}, Lepson/print/service/EpsonService;->access$1600()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    const/4 v2, 0x1

    .line 568
    :try_start_0
    invoke-static {v2}, Lepson/print/service/EpsonService;->access$1902(Z)Z

    .line 569
    invoke-static {}, Lepson/print/service/EpsonService;->access$2000()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 570
    :try_start_1
    iget-object v4, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    iget-object v4, v4, Lepson/print/service/EpsonService;->mEPImageCreator:Lepson/print/EPImageCreator;

    if-eqz v4, :cond_1

    .line 571
    iget-object v4, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    iget-object v4, v4, Lepson/print/service/EpsonService;->mEPImageCreator:Lepson/print/EPImageCreator;

    invoke-virtual {v4}, Lepson/print/EPImageCreator;->requestStop()V

    .line 573
    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574
    :try_start_2
    iget-object v3, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v3}, Lepson/print/service/EpsonService;->access$1700(Lepson/print/service/EpsonService;)Z

    move-result v3

    if-ne v3, v2, :cond_3

    .line 575
    iget-object v2, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2100(Lepson/print/service/EpsonService;)Lepson/print/service/LocalPrintThread;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 576
    iget-object v2, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2100(Lepson/print/service/EpsonService;)Lepson/print/service/LocalPrintThread;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/service/LocalPrintThread;->interrupt()V

    :cond_2
    const-string v2, "lam dai hiep"

    .line 579
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "confirm_cancel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    iget-object v2, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->confirm_cancel(Z)I

    goto :goto_0

    .line 583
    :cond_3
    invoke-virtual {p0}, Lepson/print/service/EpsonService$1;->isSearchingPrinter()Z

    move-result p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p1, :cond_4

    .line 585
    :try_start_3
    invoke-virtual {p0}, Lepson/print/service/EpsonService$1;->cancelSearchPrinter()I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 588
    :try_start_4
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 592
    :cond_4
    :goto_0
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return v0

    :catchall_0
    move-exception p1

    .line 573
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw p1

    :catchall_1
    move-exception p1

    .line 592
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw p1
.end method

.method public confirmContinueable(Z)I
    .locals 3

    const-string v0, "LAM DAI HIEP"

    .line 597
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "confirmContinueable() call: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->confirm_continue(Z)I

    move-result p1

    return p1
.end method

.method public ensureLogin(Z)I
    .locals 5

    .line 1446
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2700(Lepson/print/service/EpsonService;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 1450
    iput-boolean v0, p0, Lepson/print/service/EpsonService$1;->mEpsonConnectlogin:Z

    .line 1453
    :cond_0
    iget-boolean p1, p0, Lepson/print/service/EpsonService$1;->mEpsonConnectlogin:Z

    if-nez p1, :cond_2

    .line 1454
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$2300(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1455
    iget-object v2, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2400(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1456
    iget-object v3, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v3}, Lepson/print/service/EpsonService;->access$2600(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v3

    .line 1457
    iget-object v4, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v4, p1, v2, v3}, Lepson/print/ecclient/EcClientLib;->Login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_1

    return p1

    .line 1461
    :cond_1
    iput-boolean v1, p0, Lepson/print/service/EpsonService$1;->mEpsonConnectlogin:Z

    :cond_2
    return v0
.end method

.method public getColor(ZIII)[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 484
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    .line 507
    :try_start_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    if-ne p2, v1, :cond_0

    .line 512
    iget-object p2, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget p2, p2, Lepson/print/ecclient/EcClientLib;->mColorModeAtRemote:I

    goto :goto_0

    .line 514
    :cond_0
    iget-object p2, p0, Lepson/print/service/EpsonService$1;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    iget p2, p2, Lepson/print/ecclient/EcClientLib;->mColorModeAtLocal:I

    :goto_0
    and-int/lit8 p3, p2, 0x1

    const/4 p4, 0x0

    if-eqz p3, :cond_1

    .line 519
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_2

    .line 522
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 526
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p2

    .line 527
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p3

    new-array p3, p3, [I

    :goto_1
    if-ge p4, p2, :cond_4

    .line 529
    invoke-virtual {p1, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, p3, p4

    add-int/lit8 p4, p4, 0x1

    goto :goto_1

    .line 534
    :cond_3
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    invoke-virtual {p1, p3, p4}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_color(II)[I

    move-result-object p3

    .line 536
    :cond_4
    monitor-exit v0

    return-object p3

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getDuplex(II)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 542
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 543
    :try_start_0
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_duplex(II)I

    move-result p1

    .line 544
    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getLang()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 409
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 410
    :try_start_0
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_lang()I

    move-result v1

    .line 411
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getLayout(II)[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 460
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 461
    :try_start_0
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_layout(II)[I

    move-result-object p1

    .line 462
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getPaperSize()[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 444
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 445
    :try_start_0
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_paper_size()[I

    move-result-object v1

    .line 446
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getPaperSource(III)[I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 476
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object p3

    monitor-enter p3

    .line 477
    :try_start_0
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_paper_source(II)[I

    move-result-object p1

    .line 478
    monitor-exit p3

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getPaperType(I)[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 452
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 453
    :try_start_0
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_paper_type(I)[I

    move-result-object p1

    .line 454
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getQuality(II)[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 468
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 469
    :try_start_0
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_quality(II)[I

    move-result-object p1

    .line 470
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getSupportedMedia(ZILjava/lang/String;Ljava/lang/String;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 425
    invoke-static {}, Lepson/print/service/EpsonService;->access$1500()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 426
    :try_start_0
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 427
    :try_start_1
    invoke-static {}, Lepson/print/service/EpsonService;->access$1500()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    .line 432
    :try_start_2
    invoke-virtual {p0, p2, p3, p4}, Lepson/print/service/EpsonService$1;->EpsonConnectGetSupportedMedia(ILjava/lang/String;Ljava/lang/String;)I

    move-result p1

    goto :goto_0

    .line 434
    :cond_0
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_supported_media2()I

    move-result p1

    .line 436
    :goto_0
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 437
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 438
    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    return p1

    :catchall_0
    move-exception p1

    .line 436
    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw p1

    :catchall_1
    move-exception p1

    .line 437
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw p1

    :catchall_2
    move-exception p1

    .line 438
    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw p1
.end method

.method public isPrinting()Z
    .locals 2

    .line 549
    invoke-static {}, Lepson/print/service/EpsonService;->access$1600()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 550
    :try_start_0
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$1700(Lepson/print/service/EpsonService;)Z

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 551
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isSearchingPrinter()Z
    .locals 2

    .line 555
    invoke-static {}, Lepson/print/service/EpsonService;->access$000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 556
    :try_start_0
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$1800(Lepson/print/service/EpsonService;)Z

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 557
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public print(Lepson/print/EPImageList;Ljava/lang/String;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 341
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 344
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object v0

    const-string v1, "PRINTER_LOCATION"

    const/4 v2, 0x0

    .line 345
    invoke-virtual {v0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 346
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u2606\u2606\u3000\u3000\u3000\u3000 isRemotePrinter  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 350
    invoke-static {p2}, Lepson/print/screen/PrintSetting$Kind;->valueOf(Ljava/lang/String;)Lepson/print/screen/PrintSetting$Kind;

    move-result-object p2

    if-ne v0, v1, :cond_2

    .line 353
    iget-object p3, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p3, p2, p1}, Lepson/print/service/EpsonService;->access$1100(Lepson/print/service/EpsonService;Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;)I

    .line 360
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$1300(Lepson/print/service/EpsonService;)Ljava/lang/Thread;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 361
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$1300(Lepson/print/service/EpsonService;)Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    :cond_1
    return v2

    .line 355
    :cond_2
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, p2, p1, p3}, Lepson/print/service/EpsonService;->access$1200(Lepson/print/service/EpsonService;Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;Z)V

    return v2
.end method

.method public printLocalPdf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIZ)I
    .locals 8

    .line 378
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-static/range {v0 .. v7}, Lepson/print/service/EpsonService;->access$1400(Lepson/print/service/EpsonService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIZ)V

    const/4 p1, 0x0

    return p1
.end method

.method public printWithImagesAndLayouts(Ljava/util/List;Z)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/cameracopy/printlayout/ImageAndLayout;",
            ">;Z)I"
        }
    .end annotation

    .line 396
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    new-instance v1, Lepson/print/service/PrintAndLayoutThread;

    invoke-direct {v1, v0, p1, p2}, Lepson/print/service/PrintAndLayoutThread;-><init>(Lepson/print/service/EpsonService;Ljava/util/List;Z)V

    invoke-static {v0, v1}, Lepson/print/service/EpsonService;->access$1302(Lepson/print/service/EpsonService;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 397
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$1300(Lepson/print/service/EpsonService;)Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    const/4 p1, 0x0

    return p1
.end method

.method public refreshRemotePrinterLogin()V
    .locals 1

    .line 1482
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2700(Lepson/print/service/EpsonService;)V

    return-void
.end method

.method public registerCallback(Lepson/print/service/IEpsonServiceCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 236
    sget-object v0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    :cond_0
    return-void
.end method

.method public declared-synchronized searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    .line 254
    :try_start_0
    invoke-static {}, Lepson/print/service/EpsonService;->access$000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    .line 255
    :try_start_1
    sput-boolean v1, Lepson/print/service/EpsonService;->bNotifyPrinter:Z

    .line 256
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    :try_start_2
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, p1}, Lepson/print/service/EpsonService;->access$102(Lepson/print/service/EpsonService;Ljava/lang/String;)Ljava/lang/String;

    .line 259
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1, p2}, Lepson/print/service/EpsonService;->access$202(Lepson/print/service/EpsonService;Ljava/lang/String;)Ljava/lang/String;

    .line 260
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1, p3}, Lepson/print/service/EpsonService;->access$302(Lepson/print/service/EpsonService;I)I

    .line 262
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p1, "EpsonService"

    const-string p2, "searchPrinters() search intterrupt()"

    .line 263
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/service/EpsonService$SearchingThread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 268
    :try_start_3
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/service/EpsonService$SearchingThread;->join()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 270
    :try_start_4
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 274
    :cond_0
    :goto_0
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$504(Lepson/print/service/EpsonService;)I

    .line 275
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$500(Lepson/print/service/EpsonService;)I

    move-result p1

    const/4 p2, 0x1

    if-le p1, p2, :cond_1

    const-string p1, "EpsonService"

    .line 276
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "search count error! mSearchCount => "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$500(Lepson/print/service/EpsonService;)I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_1
    invoke-static {}, Lepson/print/service/EpsonService;->access$600()Z

    move-result p1

    if-nez p1, :cond_2

    .line 279
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->release_driver()I

    .line 280
    invoke-static {p2}, Lepson/print/service/EpsonService;->access$602(Z)Z

    .line 281
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    iget-object p2, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {p2}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    iget-object p3, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {p3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p3

    invoke-virtual {p3}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/epson/mobilephone/common/escpr/EscprLib;->init_driver(Landroid/content/Context;Ljava/lang/String;)I

    .line 284
    :cond_2
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$800(Lepson/print/service/EpsonService;)V

    .line 285
    iget-object p1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {p1}, Lepson/print/service/EpsonService;->access$400(Lepson/print/service/EpsonService;)Lepson/print/service/EpsonService$SearchingThread;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/service/EpsonService$SearchingThread;->start()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 286
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    .line 256
    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setCurPrinter(I)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "EpsonService"

    .line 328
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCurPrinter(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-virtual {p0}, Lepson/print/service/EpsonService$1;->cancelSearchPrinter()I

    const-string v0, "Epson"

    const-string v1, "Finish cancelSearchPrinter()"

    .line 331
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    const-string v1, "EpsonService"

    const-string v2, "Start set_printer()"

    .line 333
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v1, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->set_printer(I)I

    move-result p1

    const-string v1, "EpsonService"

    .line 335
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setCurPrinter: result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setTimeOut(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "EpsonService"

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "timeout = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, p1}, Lepson/print/service/EpsonService;->access$902(Lepson/print/service/EpsonService;I)I

    return-void
.end method

.method public unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 229
    sget-object v0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_0
    return-void
.end method

.method public updatePrinterSettings(Ljava/lang/String;)I
    .locals 16

    move-object/from16 v1, p0

    const-string v0, "EpsonService"

    .line 609
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "call updatePrinterSettings() :: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    new-instance v0, Lepson/print/screen/PrintSetting;

    iget-object v2, v1, Lepson/print/service/EpsonService$1;->this$0:Lepson/print/service/EpsonService;

    invoke-static/range {p1 .. p1}, Lepson/print/screen/PrintSetting$Kind;->valueOf(Ljava/lang/String;)Lepson/print/screen/PrintSetting$Kind;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 611
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 613
    iget v2, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 614
    iget v3, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 615
    iget v4, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 616
    iget v5, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 617
    iget v6, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 618
    iget v7, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 619
    iget v8, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 628
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/EpsonService$1;->getPaperSize()[I

    move-result-object v10

    if-eqz v10, :cond_2a

    .line 630
    array-length v11, v10

    if-nez v11, :cond_0

    goto/16 :goto_1d

    :cond_0
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 635
    :goto_0
    array-length v13, v10

    if-ge v12, v13, :cond_2

    .line 636
    aget v13, v10, v12

    if-ne v2, v13, :cond_1

    move v13, v12

    goto :goto_1

    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_2
    const/4 v13, 0x0

    .line 643
    :goto_1
    array-length v14, v10

    if-lt v12, v14, :cond_4

    .line 644
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperSize()I

    move-result v12

    const/4 v14, 0x0

    .line 645
    :goto_2
    array-length v15, v10

    if-ge v14, v15, :cond_5

    .line 646
    aget v15, v10, v14

    if-ne v12, v15, :cond_3

    .line 648
    aget v2, v10, v14

    const-string v12, "EpsonService"

    .line 649
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updatePrinterSettings paperSize = "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v13, v14

    goto :goto_3

    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_4
    move v14, v12

    .line 655
    :cond_5
    :goto_3
    array-length v12, v10

    if-lt v14, v12, :cond_6

    .line 657
    aget v2, v10, v11

    const-string v10, "EpsonService"

    .line 658
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "updatePrinterSettings paperSize = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v13, 0x0

    .line 664
    :cond_6
    invoke-virtual {v1, v13}, Lepson/print/service/EpsonService$1;->getPaperType(I)[I

    move-result-object v10

    if-eqz v10, :cond_29

    .line 666
    array-length v12, v10

    if-nez v12, :cond_7

    goto/16 :goto_1c

    :cond_7
    const/4 v12, 0x0

    .line 671
    :goto_4
    array-length v14, v10

    if-ge v12, v14, :cond_9

    .line 672
    aget v14, v10, v12

    if-ne v3, v14, :cond_8

    move v14, v12

    goto :goto_5

    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_9
    const/4 v14, 0x0

    .line 679
    :goto_5
    array-length v15, v10

    if-lt v12, v15, :cond_b

    .line 680
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperType()I

    move-result v12

    const/4 v15, 0x0

    .line 681
    :goto_6
    array-length v9, v10

    if-ge v15, v9, :cond_c

    .line 682
    aget v9, v10, v15

    if-ne v12, v9, :cond_a

    .line 684
    aget v3, v10, v15

    const-string v9, "EpsonService"

    .line 685
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "updatePrinterSettings paperType = "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v14, v15

    goto :goto_7

    :cond_a
    add-int/lit8 v15, v15, 0x1

    goto :goto_6

    :cond_b
    move v15, v12

    .line 692
    :cond_c
    :goto_7
    array-length v9, v10

    if-lt v15, v9, :cond_12

    .line 693
    sget-object v9, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-static/range {p1 .. p1}, Lepson/print/screen/PrintSetting$Kind;->valueOf(Ljava/lang/String;)Lepson/print/screen/PrintSetting$Kind;

    move-result-object v12

    invoke-virtual {v9, v12}, Lepson/print/screen/PrintSetting$Kind;->compareTo(Ljava/lang/Enum;)I

    move-result v9

    if-nez v9, :cond_e

    .line 694
    sget-object v9, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v9}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v9

    const/4 v15, 0x0

    .line 695
    :goto_8
    array-length v12, v10

    if-ge v15, v12, :cond_12

    .line 696
    aget v12, v10, v15

    if-ne v9, v12, :cond_d

    .line 698
    aget v3, v10, v15

    const-string v9, "EpsonService"

    .line 699
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "updatePrinterSettings paperType = "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v14, v15

    goto :goto_c

    :cond_d
    add-int/lit8 v15, v15, 0x1

    goto :goto_8

    .line 705
    :cond_e
    sget-object v9, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v9}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v9

    const/4 v15, 0x0

    .line 706
    :goto_9
    array-length v12, v10

    if-ge v15, v12, :cond_10

    .line 707
    aget v12, v10, v15

    if-ne v9, v12, :cond_f

    .line 709
    aget v3, v10, v15

    const-string v9, "EpsonService"

    .line 710
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "updatePrinterSettings paperType = "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v14, v15

    goto :goto_a

    :cond_f
    add-int/lit8 v15, v15, 0x1

    goto :goto_9

    .line 714
    :cond_10
    :goto_a
    array-length v9, v10

    if-lt v15, v9, :cond_12

    .line 716
    sget-object v9, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_AUTO_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v9}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v9

    const/4 v15, 0x0

    .line 717
    :goto_b
    array-length v12, v10

    if-ge v15, v12, :cond_12

    .line 718
    aget v12, v10, v15

    if-ne v9, v12, :cond_11

    .line 720
    aget v3, v10, v15

    const-string v9, "EpsonService"

    .line 721
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "updatePrinterSettings paperType = "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v14, v15

    goto :goto_c

    :cond_11
    add-int/lit8 v15, v15, 0x1

    goto :goto_b

    .line 729
    :cond_12
    :goto_c
    array-length v9, v10

    if-lt v15, v9, :cond_13

    .line 731
    aget v3, v10, v11

    const-string v9, "EpsonService"

    .line 732
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updatePrinterSettings paperType = "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v14, 0x0

    .line 738
    :cond_13
    invoke-virtual {v1, v13, v14}, Lepson/print/service/EpsonService$1;->getLayout(II)[I

    move-result-object v9

    const/4 v10, 0x2

    if-eqz v9, :cond_16

    const/4 v12, 0x0

    .line 741
    :goto_d
    array-length v15, v9

    if-ge v12, v15, :cond_15

    .line 742
    aget v15, v9, v12

    if-ne v4, v15, :cond_14

    .line 743
    aget v4, v9, v12

    goto :goto_e

    :cond_14
    add-int/lit8 v12, v12, 0x1

    goto :goto_d

    .line 747
    :cond_15
    :goto_e
    array-length v9, v9

    if-lt v12, v9, :cond_17

    const-string v4, "EpsonService"

    const-string v9, "updatePrinterSettings layout = EPS_MLID_BORDERS"

    .line 749
    invoke-static {v4, v9}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x2

    goto :goto_f

    :cond_16
    const-string v4, "EpsonService"

    const-string v9, "updatePrinterSettings layout = EPS_MLID_BORDERS"

    .line 753
    invoke-static {v4, v9}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x2

    .line 759
    :cond_17
    :goto_f
    invoke-virtual {v1, v13, v14}, Lepson/print/service/EpsonService$1;->getQuality(II)[I

    move-result-object v9

    if-eqz v9, :cond_1b

    const/4 v12, 0x0

    .line 762
    :goto_10
    array-length v15, v9

    if-ge v12, v15, :cond_19

    .line 763
    aget v15, v9, v12

    if-ne v5, v15, :cond_18

    .line 764
    aget v5, v9, v12

    goto :goto_11

    :cond_18
    add-int/lit8 v12, v12, 0x1

    goto :goto_10

    .line 768
    :cond_19
    :goto_11
    array-length v15, v9

    if-lt v12, v15, :cond_1c

    .line 769
    array-length v5, v9

    if-lez v5, :cond_1a

    .line 770
    aget v5, v9, v11

    goto :goto_12

    :cond_1a
    const/4 v5, 0x2

    :goto_12
    const-string v9, "EpsonService"

    .line 774
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updatePrinterSettings quality = "

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_13

    :cond_1b
    const-string v5, "EpsonService"

    .line 778
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updatePrinterSettings quality = "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x2

    .line 784
    :cond_1c
    :goto_13
    invoke-virtual {v1, v13, v14, v11}, Lepson/print/service/EpsonService$1;->getPaperSource(III)[I

    move-result-object v9

    if-eqz v9, :cond_20

    const/4 v12, 0x0

    .line 787
    :goto_14
    array-length v15, v9

    if-ge v12, v15, :cond_1e

    .line 788
    aget v15, v9, v12

    if-ne v6, v15, :cond_1d

    .line 789
    aget v6, v9, v12

    goto :goto_15

    :cond_1d
    add-int/lit8 v12, v12, 0x1

    goto :goto_14

    .line 793
    :cond_1e
    :goto_15
    array-length v15, v9

    if-lt v12, v15, :cond_21

    .line 794
    array-length v6, v9

    if-lez v6, :cond_1f

    .line 795
    aget v6, v9, v11

    goto :goto_16

    :cond_1f
    const/4 v6, 0x0

    :goto_16
    const-string v9, "EpsonService"

    .line 799
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updatePrinterSettings paperSource = "

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_17

    :cond_20
    const-string v6, "EpsonService"

    const-string v9, "updatePrinterSettings paperSource = EPS_MPID_NOT_SPEC"

    .line 803
    invoke-static {v6, v9}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x0

    .line 809
    :cond_21
    :goto_17
    invoke-virtual {v1, v13, v14}, Lepson/print/service/EpsonService$1;->getDuplex(II)I

    move-result v9

    if-eqz v9, :cond_22

    const/4 v12, 0x1

    if-ne v4, v12, :cond_23

    and-int/2addr v9, v10

    if-eqz v9, :cond_23

    :cond_22
    if-eqz v7, :cond_23

    const-string v7, "EpsonService"

    const-string v9, "updatePrinterSettings duplex = EPS_DUPLEX_NONE"

    .line 814
    invoke-static {v7, v9}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    .line 821
    :cond_23
    invoke-virtual {v1, v11, v11, v13, v14}, Lepson/print/service/EpsonService$1;->getColor(ZIII)[I

    move-result-object v9

    if-eqz v9, :cond_27

    const/4 v10, 0x0

    .line 824
    :goto_18
    array-length v12, v9

    if-ge v10, v12, :cond_25

    .line 825
    aget v12, v9, v10

    if-ne v8, v12, :cond_24

    .line 826
    aget v8, v9, v10

    goto :goto_19

    :cond_24
    add-int/lit8 v10, v10, 0x1

    goto :goto_18

    .line 830
    :cond_25
    :goto_19
    array-length v12, v9

    if-lt v10, v12, :cond_28

    .line 831
    array-length v8, v9

    if-lez v8, :cond_26

    .line 832
    aget v8, v9, v11

    goto :goto_1a

    :cond_26
    const/4 v8, 0x0

    :goto_1a
    const-string v9, "EpsonService"

    .line 836
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updatePrinterSettings color = "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1b

    :cond_27
    const-string v8, "EpsonService"

    .line 840
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "updatePrinterSettings color = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v8, 0x0

    .line 849
    :cond_28
    :goto_1b
    iput v2, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 850
    iput v3, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 851
    iput v4, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 852
    iput v5, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 853
    iput v6, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 854
    iput v7, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 855
    iput v8, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 856
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->saveSettings()V

    return v11

    :cond_29
    :goto_1c
    :try_start_1
    const-string v0, "EpsonService"

    const-string v2, "updatePrinterSettings Failed getPaperType"

    .line 667
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, -0x1

    return v2

    :cond_2a
    :goto_1d
    const-string v0, "EpsonService"

    const-string v2, "updatePrinterSettings Failed getPaperSize"

    .line 631
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v2, -0x1

    return v2

    :catch_0
    move-exception v0

    const/4 v2, -0x1

    .line 845
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    return v2
.end method

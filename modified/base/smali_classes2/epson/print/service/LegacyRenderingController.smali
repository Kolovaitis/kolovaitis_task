.class public Lepson/print/service/LegacyRenderingController;
.super Ljava/lang/Object;
.source "LegacyRenderingController.java"

# interfaces
.implements Lepson/print/service/RenderingController;


# instance fields
.field mEpImageList:Lepson/print/EPImageList;

.field mEpsonService:Lepson/print/service/PrintService;

.field mKind:Lepson/print/screen/PrintSetting$Kind;


# direct methods
.method public constructor <init>(Lepson/print/service/PrintService;Lepson/print/EPImageList;Lepson/print/screen/PrintSetting$Kind;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lepson/print/service/LegacyRenderingController;->mEpsonService:Lepson/print/service/PrintService;

    .line 19
    iput-object p2, p0, Lepson/print/service/LegacyRenderingController;->mEpImageList:Lepson/print/EPImageList;

    .line 20
    iput-object p3, p0, Lepson/print/service/LegacyRenderingController;->mKind:Lepson/print/screen/PrintSetting$Kind;

    return-void
.end method


# virtual methods
.method public drawBeforeStartJob()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/service/LocalInterrupt;
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lepson/print/service/LegacyRenderingController;->mEpImageList:Lepson/print/EPImageList;

    iget v0, v0, Lepson/print/EPImageList;->apfModeInPrinting:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lepson/print/service/LegacyRenderingController;->mEpImageList:Lepson/print/EPImageList;

    invoke-virtual {v0}, Lepson/print/EPImageList;->getSharpness()I

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    :cond_0
    iget-object v0, p0, Lepson/print/service/LegacyRenderingController;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lepson/print/service/LegacyRenderingController;->mEpImageList:Lepson/print/EPImageList;

    iget-object v2, p0, Lepson/print/service/LegacyRenderingController;->mEpsonService:Lepson/print/service/PrintService;

    invoke-static {v0, v1, v2}, Lcom/epson/iprint/apf/ApfEpImageAdapter;->getApfImageList(Landroid/content/Context;Lepson/print/EPImageList;Lepson/print/service/PrintService;)Lepson/print/EPImageList;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lepson/print/service/LegacyRenderingController;->mEpImageList:Lepson/print/EPImageList;

    invoke-virtual {v1}, Lepson/print/EPImageList;->getRenderingMode()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/EPImageList;->setRenderingMode(I)V

    .line 30
    iget-object v1, p0, Lepson/print/service/LegacyRenderingController;->mEpsonService:Lepson/print/service/PrintService;

    invoke-interface {v1}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_2

    .line 36
    iput-object v0, p0, Lepson/print/service/LegacyRenderingController;->mEpImageList:Lepson/print/EPImageList;

    :cond_1
    return-void

    .line 34
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 31
    :cond_3
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0
.end method

.method public getImageList()Lepson/print/EPImageList;
    .locals 1

    .line 47
    iget-object v0, p0, Lepson/print/service/LegacyRenderingController;->mEpImageList:Lepson/print/EPImageList;

    return-object v0
.end method

.method public getKind()Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    .line 76
    iget-object v0, p0, Lepson/print/service/LegacyRenderingController;->mKind:Lepson/print/screen/PrintSetting$Kind;

    return-object v0
.end method

.method public interruptSubThreads()V
    .locals 0

    return-void
.end method

.method public joinSubThread(J)V
    .locals 0

    return-void
.end method

.method public startDrawAfterStartJob(I)V
    .locals 0

    return-void
.end method

.method public waitPage(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-void
.end method

.class Lepson/print/service/EpsonService$2;
.super Ljava/lang/Object;
.source "EpsonService.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/service/EpsonService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/service/EpsonService;


# direct methods
.method constructor <init>(Lepson/print/service/EpsonService;)V
    .locals 0

    .line 1635
    iput-object p1, p0, Lepson/print/service/EpsonService$2;->this$0:Lepson/print/service/EpsonService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3

    .line 1637
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1647
    :pswitch_0
    iget-object v0, p0, Lepson/print/service/EpsonService$2;->this$0:Lepson/print/service/EpsonService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, v1, v2, p1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    goto :goto_0

    .line 1643
    :pswitch_1
    iget-object v0, p0, Lepson/print/service/EpsonService$2;->this$0:Lepson/print/service/EpsonService;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, p1}, Lepson/print/service/EpsonService;->epsNotifyContinueable(I)V

    goto :goto_0

    .line 1639
    :pswitch_2
    iget-object v0, p0, Lepson/print/service/EpsonService$2;->this$0:Lepson/print/service/EpsonService;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Lepson/print/service/EpsonService;->epsNotifyDataChange(Ljava/lang/String;)V

    goto :goto_0

    .line 1651
    :cond_0
    iget-object v0, p0, Lepson/print/service/EpsonService$2;->this$0:Lepson/print/service/EpsonService;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, p1}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    :goto_0
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class Lepson/print/service/PrintAndLayoutThread;
.super Ljava/lang/Thread;
.source "PrintAndLayoutThread.java"


# static fields
.field private static final DUPLEX_NONE:I = 0x0

.field private static final REMOTE_FILE_SIZE_MAX:J = 0x989680L

.field private static final REMOTE_IMAGE_CREATE_PROGRESS:[I

.field private static final REMOTE_JPEG_QUALITYS:[I


# instance fields
.field final EPS_JOB_CANCELED:I

.field private mColorValue:I

.field private mCopies:I

.field private final mEpsonService:Lepson/print/service/EpsonService;

.field private final mImageAndLayoutList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/cameracopy/printlayout/ImageAndLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mIsBkRetry:Z

.field private mLayout:I

.field private mPaperSizeId:I

.field private mRemoteImageCreateProgress:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    .line 42
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lepson/print/service/PrintAndLayoutThread;->REMOTE_JPEG_QUALITYS:[I

    const/4 v0, 0x5

    .line 45
    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lepson/print/service/PrintAndLayoutThread;->REMOTE_IMAGE_CREATE_PROGRESS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x5f
        0x50
    .end array-data

    :array_1
    .array-data 4
        0x14
        0x19
        0x32
        0x46
        0x4b
    .end array-data
.end method

.method public constructor <init>(Lepson/print/service/EpsonService;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/service/EpsonService;",
            "Ljava/util/List<",
            "Lcom/epson/cameracopy/printlayout/ImageAndLayout;",
            ">;Z)V"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/16 v0, 0x28

    .line 50
    iput v0, p0, Lepson/print/service/PrintAndLayoutThread;->EPS_JOB_CANCELED:I

    .line 64
    iput-object p1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    .line 65
    iput-object p2, p0, Lepson/print/service/PrintAndLayoutThread;->mImageAndLayoutList:Ljava/util/List;

    .line 66
    iput-boolean p3, p0, Lepson/print/service/PrintAndLayoutThread;->mIsBkRetry:Z

    return-void
.end method

.method private callStartJob()I
    .locals 17

    move-object/from16 v0, p0

    .line 71
    new-instance v1, Lepson/print/screen/PrintSetting;

    iget-object v2, v0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v1, v2, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 73
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 75
    iget v5, v1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 76
    iput v5, v0, Lepson/print/service/PrintAndLayoutThread;->mPaperSizeId:I

    .line 77
    iget v6, v1, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 78
    iget v7, v1, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 79
    iput v7, v0, Lepson/print/service/PrintAndLayoutThread;->mLayout:I

    .line 81
    iget v8, v1, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 82
    iget v9, v1, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 83
    iget v10, v1, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 84
    iput v10, v0, Lepson/print/service/PrintAndLayoutThread;->mColorValue:I

    .line 85
    iget v2, v1, Lepson/print/screen/PrintSetting;->copiesValue:I

    .line 86
    iput v2, v0, Lepson/print/service/PrintAndLayoutThread;->mCopies:I

    .line 94
    iget-object v2, v0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getLang()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    iget-object v2, v0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getLang()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v14, 0x0

    goto :goto_1

    .line 95
    :cond_1
    :goto_0
    iget v1, v1, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    move v14, v1

    .line 99
    :goto_1
    iget-object v4, v0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/4 v15, 0x0

    iget-boolean v1, v0, Lepson/print/service/PrintAndLayoutThread;->mIsBkRetry:Z

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move/from16 v16, v1

    invoke-virtual/range {v4 .. v16}, Lepson/print/service/EpsonService;->startJob(IIIIIIIIIIIZ)I

    move-result v1

    return v1
.end method

.method static convertBmpToJpeg(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 462
    new-instance v0, Lepson/image/epsonImage;

    invoke-direct {v0}, Lepson/image/epsonImage;-><init>()V

    .line 464
    invoke-virtual {v0, p0, p1}, Lepson/image/epsonImage;->bmp2Jpg2(Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method private getPrintFileName(I)Ljava/lang/String;
    .locals 6

    .line 152
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "_%02d.bmp"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    add-int/2addr p1, v4

    .line 153
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v4, 0x0

    aput-object p1, v5, v4

    invoke-static {v2, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getPrintRotation()I
    .locals 2

    .line 115
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->getLang()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 119
    :cond_0
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    iget v1, p0, Lepson/print/service/PrintAndLayoutThread;->mPaperSizeId:I

    .line 120
    invoke-virtual {v0, v1}, Lepson/print/service/EpsonService;->pageSneedRotate(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private localPrint()I
    .locals 8

    const/4 v0, 0x0

    .line 196
    :try_start_0
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 199
    invoke-direct {p0}, Lepson/print/service/PrintAndLayoutThread;->callStartJob()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 284
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v1

    .line 203
    :cond_0
    :try_start_1
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v3, 0x28

    if-eqz v1, :cond_1

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v3

    .line 207
    :cond_1
    :try_start_2
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getPrintableArea()[I

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 209
    :goto_0
    :try_start_3
    iget v6, p0, Lepson/print/service/PrintAndLayoutThread;->mCopies:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-ge v4, v6, :cond_e

    .line 212
    :try_start_4
    iget-object v5, p0, Lepson/print/service/PrintAndLayoutThread;->mImageAndLayoutList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/epson/cameracopy/printlayout/ImageAndLayout;

    .line 218
    invoke-direct {p0, v0}, Lepson/print/service/PrintAndLayoutThread;->getPrintFileName(I)Ljava/lang/String;

    move-result-object v7

    .line 220
    invoke-virtual {p0, v6, v7, v1}, Lepson/print/service/PrintAndLayoutThread;->createPrintImage(Lcom/epson/cameracopy/printlayout/ImageAndLayout;Ljava/lang/String;[I)Z

    move-result v6

    if-nez v6, :cond_3

    const/16 v1, -0x1451

    .line 226
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1, v0}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 284
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v1

    .line 230
    :cond_3
    :try_start_5
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v6, :cond_4

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v3

    .line 234
    :cond_4
    :try_start_6
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6, v7}, Lepson/print/service/EpsonService;->initImage(Ljava/lang/String;)I

    move-result v6
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v6, :cond_5

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v6

    .line 237
    :cond_5
    :try_start_7
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v6
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v6, :cond_6

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v3

    .line 241
    :cond_6
    :try_start_8
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->startPage()I

    move-result v6
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v6, :cond_7

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v6

    .line 244
    :cond_7
    :try_start_9
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v6
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v6, :cond_8

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v3

    .line 248
    :cond_8
    :try_start_a
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->printPage()I

    move-result v6
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v6, :cond_9

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v6

    .line 251
    :cond_9
    :try_start_b
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v6
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-eqz v6, :cond_a

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v3

    .line 259
    :cond_a
    :try_start_c
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6, v0}, Lepson/print/service/EpsonService;->endPage(Z)I

    move-result v6
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v6, :cond_b

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v6

    .line 264
    :cond_b
    :try_start_d
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v6
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    if-eqz v6, :cond_c

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v3

    .line 268
    :cond_c
    :try_start_e
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->releaseImage()I

    .line 270
    iget-object v6, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v6}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v6
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    if-eqz v6, :cond_2

    .line 284
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v3

    :cond_d
    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v1

    const/4 v5, 0x0

    goto :goto_2

    :catch_1
    move-exception v1

    const/4 v5, 0x0

    .line 282
    :goto_1
    :try_start_f
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 284
    :cond_e
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v1}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return v0

    :catchall_1
    move-exception v1

    .line 284
    :goto_2
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->endJob()I

    .line 287
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 291
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->waitIfSimpleAp()V

    .line 294
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    throw v1
.end method

.method private makeRemoteFile(Ljava/lang/String;Ljava/lang/String;)I
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 500
    iget-object v3, v0, Lepson/print/service/PrintAndLayoutThread;->mImageAndLayoutList:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/epson/cameracopy/printlayout/ImageAndLayout;

    .line 502
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/PrintAndLayoutThread;->getEPImageUtil()Lepson/print/EPImageUtil;

    move-result-object v5

    .line 503
    iget v6, v0, Lepson/print/service/PrintAndLayoutThread;->mPaperSizeId:I

    iget v7, v0, Lepson/print/service/PrintAndLayoutThread;->mLayout:I

    invoke-virtual {v0, v6, v7}, Lepson/print/service/PrintAndLayoutThread;->getRemotePrintableArea(II)[I

    move-result-object v6

    .line 507
    iput v4, v0, Lepson/print/service/PrintAndLayoutThread;->mRemoteImageCreateProgress:I

    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x2

    if-ge v7, v8, :cond_6

    .line 513
    invoke-virtual {v0, v3, v1, v6}, Lepson/print/service/PrintAndLayoutThread;->createPrintImage(Lcom/epson/cameracopy/printlayout/ImageAndLayout;Ljava/lang/String;[I)Z

    move-result v9

    if-nez v9, :cond_0

    const/16 v1, -0x1451

    return v1

    .line 521
    :cond_0
    invoke-direct/range {p0 .. p0}, Lepson/print/service/PrintAndLayoutThread;->updateRemoteImageCreateProgress()V

    .line 522
    iget-object v9, v0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v9}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v9

    if-eqz v9, :cond_1

    return v4

    .line 528
    :cond_1
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x1

    if-lt v7, v10, :cond_2

    .line 531
    sget-object v11, Lepson/print/service/PrintAndLayoutThread;->REMOTE_JPEG_QUALITYS:[I

    array-length v11, v11

    sub-int/2addr v11, v10

    goto :goto_1

    :cond_2
    const/4 v11, 0x0

    .line 535
    :goto_1
    sget-object v12, Lepson/print/service/PrintAndLayoutThread;->REMOTE_JPEG_QUALITYS:[I

    array-length v13, v12

    if-ge v11, v13, :cond_5

    .line 536
    aget v12, v12, v11

    .line 537
    invoke-virtual {v5, v1, v2, v12}, Lepson/print/EPImageUtil;->bmp2jpg(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v12

    .line 539
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v13

    if-nez v12, :cond_3

    const-wide/16 v15, 0x0

    cmp-long v12, v13, v15

    if-lez v12, :cond_3

    const-wide/32 v15, 0x989680

    cmp-long v12, v13, v15

    if-gez v12, :cond_3

    return v4

    .line 545
    :cond_3
    invoke-direct/range {p0 .. p0}, Lepson/print/service/PrintAndLayoutThread;->updateRemoteImageCreateProgress()V

    .line 546
    iget-object v12, v0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v12}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v12

    if-eqz v12, :cond_4

    return v4

    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_5
    add-int/lit8 v7, v7, 0x1

    .line 557
    aget v9, v6, v4

    div-int/2addr v9, v8

    aput v9, v6, v4

    .line 558
    aget v9, v6, v10

    div-int/2addr v9, v8

    aput v9, v6, v10

    goto :goto_0

    :cond_6
    const/16 v1, -0x4b5

    return v1
.end method

.method private remoteCreateJob()I
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    move-object/from16 v0, p0

    .line 302
    new-instance v1, Lepson/print/screen/PrintSetting;

    iget-object v2, v0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v1, v2, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 304
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 306
    iget v7, v1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 307
    iput v7, v0, Lepson/print/service/PrintAndLayoutThread;->mPaperSizeId:I

    .line 308
    iget v8, v1, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 309
    iget v9, v1, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 310
    iput v9, v0, Lepson/print/service/PrintAndLayoutThread;->mLayout:I

    .line 312
    iget v10, v1, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 313
    iget v11, v1, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 314
    iget v12, v1, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 315
    iput v12, v0, Lepson/print/service/PrintAndLayoutThread;->mColorValue:I

    .line 316
    iget v1, v1, Lepson/print/screen/PrintSetting;->copiesValue:I

    .line 317
    iput v1, v0, Lepson/print/service/PrintAndLayoutThread;->mCopies:I

    .line 329
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMddHHmm"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 330
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".jpg"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 332
    iget-object v4, v0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v5, 0x2

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual/range {v4 .. v19}, Lepson/print/service/EpsonService;->epsonConnectCreateJob(ILjava/lang/String;IIIIIIIIIIIII)I

    move-result v1

    return v1
.end method

.method private remotePrint()I
    .locals 7

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 351
    :try_start_0
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_c
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_0

    .line 420
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 428
    :try_start_1
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_0
    return v1

    .line 355
    :cond_0
    :try_start_2
    invoke-direct {p0}, Lepson/print/service/PrintAndLayoutThread;->remoteCreateJob()I

    move-result v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v2, :cond_2

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_1

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_1
    :try_start_3
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_1
    return v2

    .line 360
    :cond_2
    :try_start_4
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 361
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_b
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v3, :cond_4

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_3

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_3
    :try_start_5
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_2
    return v1

    .line 365
    :cond_4
    :try_start_6
    invoke-direct {p0, v1}, Lepson/print/service/PrintAndLayoutThread;->getPrintFileName(I)Ljava/lang/String;

    move-result-object v3

    .line 366
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\\.\\w+$"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 368
    invoke-direct {p0, v3, v4}, Lepson/print/service/PrintAndLayoutThread;->makeRemoteFile(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v2, :cond_6

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_5

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_5
    :try_start_7
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_3

    :catch_3
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_3
    return v2

    .line 373
    :cond_6
    :try_start_8
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/16 v5, 0x4b

    invoke-virtual {v3, v5}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 374
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v3
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_b
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v3, :cond_8

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_7

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_7
    :try_start_9
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_4

    goto :goto_4

    :catch_4
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_4
    return v1

    .line 379
    :cond_8
    :try_start_a
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lepson/print/service/EpsonService;->epsonConnectUploadFile(Ljava/lang/String;I)I

    move-result v2
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_b
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v2, :cond_a

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_9

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_9
    :try_start_b
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_5

    goto :goto_5

    :catch_5
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_5
    return v2

    .line 385
    :cond_a
    :try_start_c
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/16 v4, 0x50

    invoke-virtual {v3, v4}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 386
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v3
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_b
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v3, :cond_c

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_b

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_b
    :try_start_d
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_6

    goto :goto_6

    :catch_6
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_6
    return v1

    .line 391
    :cond_c
    :try_start_e
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    iget v4, p0, Lepson/print/service/PrintAndLayoutThread;->mCopies:I

    invoke-virtual {v3, v4}, Lepson/print/service/EpsonService;->setEpsonConnectCopies(I)V

    .line 392
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->epsonConnectChangePrintSetting()I

    move-result v2
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_b
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    if-eqz v2, :cond_e

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_d

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_d
    :try_start_f
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_7

    goto :goto_7

    :catch_7
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_7
    return v2

    .line 397
    :cond_e
    :try_start_10
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/16 v4, 0x5a

    invoke-virtual {v3, v4}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 398
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getCancelPrinting()Z

    move-result v3
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_b
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    if-eqz v3, :cond_10

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_f

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_f
    :try_start_11
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_11} :catch_8

    goto :goto_8

    :catch_8
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_8
    return v1

    .line 403
    :cond_10
    :try_start_12
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v1, v1}, Lepson/print/service/EpsonService;->epsonConnectStartPrint(II)I

    move-result v2
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_b
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    if-eqz v2, :cond_12

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_11

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_11
    :try_start_13
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_13} :catch_9

    goto :goto_9

    :catch_9
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_9
    return v2

    .line 410
    :cond_12
    :try_start_14
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_a
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_13

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_13
    :try_start_15
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_15} :catch_d

    goto :goto_d

    :catch_a
    move-exception v3

    goto :goto_c

    :catchall_0
    move-exception v3

    goto :goto_a

    :catch_b
    move-exception v3

    goto :goto_b

    :catchall_1
    move-exception v3

    const/4 v2, 0x0

    :goto_a
    const/4 v5, 0x0

    goto :goto_e

    :catch_c
    move-exception v3

    const/4 v2, 0x0

    :goto_b
    const/4 v5, 0x0

    .line 414
    :goto_c
    :try_start_16
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    const/16 v2, -0x4b0

    .line 420
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 424
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :try_start_17
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_17} :catch_d

    goto :goto_d

    :catch_d
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_d
    return v1

    :catchall_2
    move-exception v3

    .line 420
    :goto_e
    iget-object v4, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v4}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v5}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    if-eqz v2, :cond_14

    .line 424
    iget-object v4, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v4, v0, v2, v1}, Lepson/print/service/EpsonService;->epsNotifyError(IIZ)V

    .line 428
    :cond_14
    :try_start_18
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->epsonConnectEndJob()I
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_e

    goto :goto_f

    :catch_e
    move-exception v0

    .line 431
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 432
    :goto_f
    throw v3
.end method

.method private updateRemoteImageCreateProgress()V
    .locals 3

    .line 484
    iget v0, p0, Lepson/print/service/PrintAndLayoutThread;->mRemoteImageCreateProgress:I

    sget-object v1, Lepson/print/service/PrintAndLayoutThread;->REMOTE_IMAGE_CREATE_PROGRESS:[I

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 485
    iget-object v2, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    aget v0, v1, v0

    invoke-virtual {v2, v0}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 486
    iget v0, p0, Lepson/print/service/PrintAndLayoutThread;->mRemoteImageCreateProgress:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lepson/print/service/PrintAndLayoutThread;->mRemoteImageCreateProgress:I

    goto :goto_0

    .line 488
    :cond_0
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method protected createPrintImage(Lcom/epson/cameracopy/printlayout/ImageAndLayout;Ljava/lang/String;[I)Z
    .locals 5

    .line 130
    invoke-direct {p0}, Lepson/print/service/PrintAndLayoutThread;->getPrintRotation()I

    move-result v0

    const/4 v1, 0x2

    .line 131
    new-array v1, v1, [I

    and-int/lit8 v2, v0, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_0

    .line 135
    aget v2, p3, v4

    aput v2, v1, v3

    .line 136
    aget p3, p3, v3

    aput p3, v1, v4

    goto :goto_0

    .line 138
    :cond_0
    aget v2, p3, v3

    aput v2, v1, v3

    .line 139
    aget p3, p3, v4

    aput p3, v1, v4

    .line 142
    :goto_0
    iget p3, p0, Lepson/print/service/PrintAndLayoutThread;->mColorValue:I

    if-ne p3, v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {p1, p2, v3, v1, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->createPrintData(Ljava/lang/String;Z[II)Z

    move-result p1

    return p1
.end method

.method protected getEPImageUtil()Lepson/print/EPImageUtil;
    .locals 1

    .line 476
    new-instance v0, Lepson/print/EPImageUtil;

    invoke-direct {v0}, Lepson/print/EPImageUtil;-><init>()V

    return-object v0
.end method

.method protected getRemotePrintableArea(II)[I
    .locals 4

    .line 443
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object p1

    const/4 v0, 0x2

    .line 444
    new-array v1, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne p2, v0, :cond_0

    .line 447
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width_boder()I

    move-result p2

    aput p2, v1, v2

    .line 448
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height_boder()I

    move-result p1

    aput p1, v1, v3

    goto :goto_0

    :cond_0
    if-ne p2, v3, :cond_1

    .line 450
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width_boderless()I

    move-result p2

    aput p2, v1, v2

    .line 451
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height_boderless()I

    move-result p1

    aput p1, v1, v3

    :cond_1
    :goto_0
    return-object v1
.end method

.method public run()V
    .locals 4

    .line 160
    iget-object v0, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v0}, Lepson/print/service/EpsonService;->getPrintLockObject()Ljava/lang/Object;

    move-result-object v0

    .line 161
    monitor-enter v0

    .line 162
    :try_start_0
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lepson/print/service/EpsonService;->setPrinting(Z)V

    .line 163
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lepson/print/service/EpsonService;->setCancelPrinting(Z)V

    .line 167
    iget-object v1, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    invoke-direct {p0}, Lepson/print/service/PrintAndLayoutThread;->remotePrint()I

    move-result v1

    goto :goto_0

    .line 170
    :cond_0
    invoke-direct {p0}, Lepson/print/service/PrintAndLayoutThread;->localPrint()I

    move-result v1

    :goto_0
    const/16 v3, 0x28

    if-ne v1, v3, :cond_1

    const/4 v1, 0x0

    .line 176
    :cond_1
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v2}, Lepson/print/service/EpsonService;->setPrinting(Z)V

    .line 177
    iget-object v3, p0, Lepson/print/service/PrintAndLayoutThread;->mEpsonService:Lepson/print/service/EpsonService;

    invoke-virtual {v3, v2}, Lepson/print/service/EpsonService;->setCancelPrinting(Z)V

    .line 181
    invoke-static {v1}, Lepson/print/service/EpsonService;->onNotifyEndJob(I)V

    .line 182
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected test_makeRemoteFile(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    .line 566
    invoke-direct {p0, p1, p2}, Lepson/print/service/PrintAndLayoutThread;->makeRemoteFile(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.class Lepson/print/service/EpsonService$SearchingThread;
.super Ljava/lang/Thread;
.source "EpsonService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/service/EpsonService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SearchingThread"
.end annotation


# instance fields
.field private volatile mExecuting_search_printer:Z

.field final synthetic this$0:Lepson/print/service/EpsonService;


# direct methods
.method constructor <init>(Lepson/print/service/EpsonService;)V
    .locals 0

    .line 1529
    iput-object p1, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 p1, 0x0

    .line 1531
    iput-boolean p1, p0, Lepson/print/service/EpsonService$SearchingThread;->mExecuting_search_printer:Z

    return-void
.end method


# virtual methods
.method public interrupt()V
    .locals 3

    .line 1617
    iget-boolean v0, p0, Lepson/print/service/EpsonService$SearchingThread;->mExecuting_search_printer:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1618
    iget-object v0, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->cancel_search_printer()I

    move-result v0

    const/16 v2, -0x519

    if-ne v0, v2, :cond_0

    .line 1620
    iput-boolean v1, p0, Lepson/print/service/EpsonService$SearchingThread;->mExecuting_search_printer:Z

    .line 1621
    iget-object v0, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lepson/print/service/EpsonService;->access$2802(Lepson/print/service/EpsonService;Z)Z

    .line 1624
    :cond_0
    invoke-super {p0}, Ljava/lang/Thread;->interrupt()V

    .line 1628
    invoke-static {}, Lepson/print/service/EpsonService;->access$000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1629
    :try_start_0
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2, v1}, Lepson/print/service/EpsonService;->access$1802(Lepson/print/service/EpsonService;Z)Z

    .line 1630
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public run()V
    .locals 7

    .line 1537
    iget-object v0, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lepson/print/service/EpsonService;->access$2802(Lepson/print/service/EpsonService;Z)Z

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1541
    :goto_0
    invoke-static {}, Lepson/print/service/EpsonService$SearchingThread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v2, "EpsonService"

    const-string v3, "user cancel searching printer"

    .line 1542
    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    goto :goto_1

    .line 1546
    :cond_0
    invoke-static {}, Lepson/print/service/EpsonService;->access$1600()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1547
    :try_start_0
    iget-object v4, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v4}, Lepson/print/service/EpsonService;->access$1700(Lepson/print/service/EpsonService;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1548
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :goto_1
    if-eqz v2, :cond_1

    const-string v0, "EpsonService"

    const-string v1, "bCancel = true"

    .line 1563
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    iget-object v0, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$506(Lepson/print/service/EpsonService;)I

    return-void

    .line 1569
    :cond_1
    invoke-static {}, Lepson/print/service/EpsonService;->access$000()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1570
    :try_start_1
    iget-object v3, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v3, v0}, Lepson/print/service/EpsonService;->access$1802(Lepson/print/service/EpsonService;Z)Z

    .line 1571
    iput-boolean v0, p0, Lepson/print/service/EpsonService$SearchingThread;->mExecuting_search_printer:Z

    .line 1572
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const-string v2, "EpsonService"

    const-string v3, "begin search printer"

    .line 1573
    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$900(Lepson/print/service/EpsonService;)I

    move-result v2

    const/4 v3, -0x1

    const/16 v4, 0x3c

    if-le v2, v3, :cond_2

    .line 1579
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$900(Lepson/print/service/EpsonService;)I

    move-result v4

    goto :goto_2

    .line 1581
    :cond_2
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$300(Lepson/print/service/EpsonService;)I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    .line 1589
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$100(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 1584
    :cond_3
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$200(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v2

    .line 1595
    :goto_2
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 1597
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v2

    iget-object v3, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    iget-object v3, v3, Lepson/print/service/EpsonService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setHanlder(Landroid/os/Handler;)V

    .line 1598
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SearchingThread : search_printer2 \u2606\u2606\u3000timeout = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 1599
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$700(Lepson/print/service/EpsonService;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v2

    iget-object v3, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v3}, Lepson/print/service/EpsonService;->access$100(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v5}, Lepson/print/service/EpsonService;->access$200(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v6}, Lepson/print/service/EpsonService;->access$300(Lepson/print/service/EpsonService;)I

    move-result v6

    invoke-virtual {v2, v3, v5, v6, v4}, Lcom/epson/mobilephone/common/escpr/EscprLib;->search_printer2(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v2

    if-nez v2, :cond_4

    .line 1601
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$100(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$100(Lepson/print/service/EpsonService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 1602
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    iput-boolean v0, v2, Lepson/print/service/EpsonService;->bSetPrinter:Z

    .line 1604
    :cond_4
    invoke-static {}, Lepson/print/service/EpsonService;->access$000()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1605
    :try_start_2
    iget-object v2, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2, v1}, Lepson/print/service/EpsonService;->access$1802(Lepson/print/service/EpsonService;Z)Z

    .line 1606
    sput-boolean v1, Lepson/print/service/EpsonService;->bNotifyPrinter:Z

    .line 1607
    iput-boolean v1, p0, Lepson/print/service/EpsonService$SearchingThread;->mExecuting_search_printer:Z

    .line 1608
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1609
    iget-object v0, p0, Lepson/print/service/EpsonService$SearchingThread;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$506(Lepson/print/service/EpsonService;)I

    const-string v0, "Epson"

    const-string v1, "search printer finish"

    .line 1610
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v1

    .line 1608
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    :catchall_1
    move-exception v0

    .line 1572
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1550
    :cond_5
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    const-wide/16 v3, 0x3e8

    .line 1553
    :try_start_6
    invoke-static {v3, v4}, Lepson/print/service/EpsonService$SearchingThread;->sleep(J)V

    const-string v3, "EpsonService"

    const-string v4, "sleep some second to waiting printing thread"

    .line 1554
    invoke-static {v3, v4}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    .line 1557
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    const/4 v2, 0x1

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    .line 1550
    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0
.end method

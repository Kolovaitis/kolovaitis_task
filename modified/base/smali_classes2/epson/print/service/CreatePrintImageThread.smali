.class public Lepson/print/service/CreatePrintImageThread;
.super Ljava/lang/Thread;
.source "CreatePrintImageThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/service/CreatePrintImageThread$LocalImageCreator;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CreatePrintImageThread"


# instance fields
.field private mException:Ljava/lang/Exception;

.field private mImageList:Lepson/print/EPImageList;

.field private mImagePerPage:I

.field private mLocalImageCreator:Lepson/print/service/CreatePrintImageThread$LocalImageCreator;

.field private mLoopEnd:Z

.field private mPrintService:Lepson/print/service/PrintService;

.field private mQueue:Ljava/util/concurrent/SynchronousQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/SynchronousQueue<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRenderingController:Lepson/print/service/RenderingController;

.field private mTotalPage:I


# direct methods
.method public constructor <init>(Lepson/print/service/RenderingController;Lepson/print/service/PrintService;Lepson/print/service/CreatePrintImageThread$LocalImageCreator;Lepson/print/EPImageList;II)V
    .locals 1

    const-string v0, "CreatePrintImageThread"

    .line 32
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 33
    new-instance v0, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v0}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    iput-object v0, p0, Lepson/print/service/CreatePrintImageThread;->mQueue:Ljava/util/concurrent/SynchronousQueue;

    .line 34
    iput-object p1, p0, Lepson/print/service/CreatePrintImageThread;->mRenderingController:Lepson/print/service/RenderingController;

    .line 35
    iput-object p2, p0, Lepson/print/service/CreatePrintImageThread;->mPrintService:Lepson/print/service/PrintService;

    .line 36
    iput-object p3, p0, Lepson/print/service/CreatePrintImageThread;->mLocalImageCreator:Lepson/print/service/CreatePrintImageThread$LocalImageCreator;

    .line 38
    iput-object p4, p0, Lepson/print/service/CreatePrintImageThread;->mImageList:Lepson/print/EPImageList;

    .line 39
    iput p5, p0, Lepson/print/service/CreatePrintImageThread;->mImagePerPage:I

    .line 40
    iput p6, p0, Lepson/print/service/CreatePrintImageThread;->mTotalPage:I

    return-void
.end method

.method private createImageLoop()V
    .locals 5

    const/4 v0, 0x0

    .line 52
    iput-object v0, p0, Lepson/print/service/CreatePrintImageThread;->mException:Ljava/lang/Exception;

    .line 54
    iget-object v0, p0, Lepson/print/service/CreatePrintImageThread;->mImageList:Lepson/print/EPImageList;

    invoke-virtual {v0}, Lepson/print/EPImageList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 56
    :goto_0
    :try_start_0
    iget v3, p0, Lepson/print/service/CreatePrintImageThread;->mTotalPage:I

    if-ge v1, v3, :cond_4

    .line 57
    iget v3, p0, Lepson/print/service/CreatePrintImageThread;->mImagePerPage:I

    add-int/2addr v3, v2

    if-le v3, v0, :cond_0

    move v3, v0

    .line 61
    :cond_0
    iget-object v4, p0, Lepson/print/service/CreatePrintImageThread;->mRenderingController:Lepson/print/service/RenderingController;

    invoke-interface {v4, v2, v3}, Lepson/print/service/RenderingController;->waitPage(II)V

    .line 63
    iget-object v2, p0, Lepson/print/service/CreatePrintImageThread;->mPrintService:Lepson/print/service/PrintService;

    invoke-interface {v2}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v2

    if-nez v2, :cond_3

    .line 66
    iget-object v2, p0, Lepson/print/service/CreatePrintImageThread;->mImageList:Lepson/print/EPImageList;

    invoke-direct {p0, v2, v1}, Lepson/print/service/CreatePrintImageThread;->localCreateImage(Lepson/print/EPImageList;I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_1

    .line 71
    :cond_1
    iget-object v2, p0, Lepson/print/service/CreatePrintImageThread;->mPrintService:Lepson/print/service/PrintService;

    invoke-interface {v2}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v2

    if-nez v2, :cond_2

    .line 74
    invoke-direct {p0, v1}, Lepson/print/service/CreatePrintImageThread;->putSheet(I)V

    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_0

    .line 72
    :cond_2
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0

    .line 64
    :cond_3
    new-instance v0, Lepson/print/service/LocalInterrupt;

    invoke-direct {v0}, Lepson/print/service/LocalInterrupt;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 78
    iput-object v0, p0, Lepson/print/service/CreatePrintImageThread;->mException:Ljava/lang/Exception;

    .line 79
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_4
    :goto_1
    return-void
.end method

.method private localCreateImage(Lepson/print/EPImageList;I)Ljava/lang/String;
    .locals 2

    .line 84
    iget-object v0, p0, Lepson/print/service/CreatePrintImageThread;->mLocalImageCreator:Lepson/print/service/CreatePrintImageThread$LocalImageCreator;

    iget-object v1, p0, Lepson/print/service/CreatePrintImageThread;->mPrintService:Lepson/print/service/PrintService;

    invoke-virtual {v0, v1, p1, p2}, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->createImage(Lepson/print/service/PrintService;Lepson/print/EPImageList;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private putSheet(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const-string v0, "CreatePrintImageThread"

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queue.put "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v0, p0, Lepson/print/service/CreatePrintImageThread;->mQueue:Ljava/util/concurrent/SynchronousQueue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/concurrent/SynchronousQueue;->put(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v0, 0x0

    .line 45
    iput-boolean v0, p0, Lepson/print/service/CreatePrintImageThread;->mLoopEnd:Z

    .line 46
    invoke-direct {p0}, Lepson/print/service/CreatePrintImageThread;->createImageLoop()V

    const/4 v0, 0x1

    .line 47
    iput-boolean v0, p0, Lepson/print/service/CreatePrintImageThread;->mLoopEnd:Z

    const-string v0, "CreatePrintImageThread"

    const-string v1, "run() end"

    .line 48
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public waitePrintImage()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lepson/print/service/CreatePrintImageThread;->mException:Ljava/lang/Exception;

    if-nez v0, :cond_2

    .line 110
    iget-boolean v0, p0, Lepson/print/service/CreatePrintImageThread;->mLoopEnd:Z

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "CreatePrintImageThread"

    const-string v2, "next take"

    .line 115
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v0, p0, Lepson/print/service/CreatePrintImageThread;->mQueue:Ljava/util/concurrent/SynchronousQueue;

    const-wide/16 v2, 0xb4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/SynchronousQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const-string v2, "CreatePrintImageThread"

    .line 117
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "queue.take "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    return v1

    .line 122
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 108
    :cond_2
    throw v0
.end method

.class public Lepson/print/PhotoImageConvertViewModel;
.super Landroid/arch/lifecycle/AndroidViewModel;
.source "PhotoImageConvertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;,
        Lepson/print/PhotoImageConvertViewModel$Result;
    }
.end annotation


# instance fields
.field private final mApplication:Landroid/app/Application;

.field private final mImageFilenameLibData:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0
    .param p1    # Landroid/app/Application;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 29
    invoke-direct {p0, p1}, Landroid/arch/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 30
    iput-object p1, p0, Lepson/print/PhotoImageConvertViewModel;->mApplication:Landroid/app/Application;

    .line 31
    new-instance p1, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    invoke-direct {p1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;-><init>()V

    iput-object p1, p0, Lepson/print/PhotoImageConvertViewModel;->mImageFilenameLibData:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    return-void
.end method

.method static synthetic access$300(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    .line 24
    invoke-static {p0}, Lepson/print/PhotoImageConvertViewModel;->initTempDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static clearTempDirectory(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 45
    invoke-static {p0}, Lepson/print/PhotoImageConvertViewModel;->initTempDirectory(Landroid/content/Context;)Ljava/lang/String;

    return-void
.end method

.method private static initTempDirectory(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 36
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p0

    .line 37
    sget-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->PHOTO_FILE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->initTempCacheDirectory(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getData()Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;
    .locals 1

    .line 49
    iget-object v0, p0, Lepson/print/PhotoImageConvertViewModel;->mImageFilenameLibData:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    return-object v0
.end method

.method public setOriginalImageList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lepson/print/PhotoImageConvertViewModel;->mImageFilenameLibData:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    iget-object v1, p0, Lepson/print/PhotoImageConvertViewModel;->mApplication:Landroid/app/Application;

    invoke-virtual {v0, p1, v1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->setOriginalList(Ljava/util/ArrayList;Landroid/app/Application;)V

    return-void
.end method

.class Lepson/print/ActivityDocsPrintPreview$25;
.super Ljava/lang/Object;
.source "ActivityDocsPrintPreview.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityDocsPrintPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;


# direct methods
.method constructor <init>(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 2966
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$25;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    .line 2979
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$25;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p2}, Lepson/print/service/IEpsonService$Stub;->asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/ActivityDocsPrintPreview;->access$802(Lepson/print/ActivityDocsPrintPreview;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    .line 2981
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$25;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2983
    :try_start_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$25;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object p2, p0, Lepson/print/ActivityDocsPrintPreview$25;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p2}, Lepson/print/ActivityDocsPrintPreview;->access$4500(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object p2

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->registerCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2985
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .line 2970
    :try_start_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$25;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$25;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$4500(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2972
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2975
    :goto_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$25;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$802(Lepson/print/ActivityDocsPrintPreview;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    return-void
.end method

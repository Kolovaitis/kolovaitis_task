.class public Lepson/print/Util/FitSizeTextView;
.super Landroid/widget/TextView;
.source "FitSizeTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private refitText(Ljava/lang/String;I)V
    .locals 4

    if-lez p2, :cond_1

    .line 25
    invoke-virtual {p0}, Lepson/print/Util/FitSizeTextView;->getPaddingLeft()I

    move-result v0

    sub-int/2addr p2, v0

    .line 26
    invoke-virtual {p0}, Lepson/print/Util/FitSizeTextView;->getPaddingRight()I

    move-result v0

    sub-int/2addr p2, v0

    int-to-float p2, p2

    .line 28
    invoke-virtual {p0}, Lepson/print/Util/FitSizeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 29
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    const/4 v2, 0x0

    .line 30
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, p1, v2, v3, v1}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 31
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    .line 32
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    :goto_0
    cmpl-float v2, v2, p2

    if-ltz v2, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    .line 37
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 38
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p0}, Lepson/print/Util/FitSizeTextView;->setSingleLine()V

    :cond_1
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1

    .line 49
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 50
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 51
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 52
    invoke-virtual {p0}, Lepson/print/Util/FitSizeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lepson/print/Util/FitSizeTextView;->refitText(Ljava/lang/String;I)V

    .line 53
    invoke-virtual {p0, p1, p2}, Lepson/print/Util/FitSizeTextView;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    if-eq p1, p3, :cond_0

    .line 65
    invoke-virtual {p0}, Lepson/print/Util/FitSizeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2, p1}, Lepson/print/Util/FitSizeTextView;->refitText(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 59
    invoke-virtual {p0}, Lepson/print/Util/FitSizeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lepson/print/Util/FitSizeTextView;->getWidth()I

    move-result p2

    invoke-direct {p0, p1, p2}, Lepson/print/Util/FitSizeTextView;->refitText(Ljava/lang/String;I)V

    return-void
.end method

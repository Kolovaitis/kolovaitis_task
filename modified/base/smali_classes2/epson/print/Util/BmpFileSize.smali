.class public Lepson/print/Util/BmpFileSize;
.super Ljava/lang/Object;
.source "BmpFileSize.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static check24BitBmpCapacity(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .line 17
    invoke-static {p0}, Lepson/print/Util/BmpFileSize;->get24BitBmpSize(Ljava/lang/String;)I

    move-result p0

    const/4 v0, 0x0

    if-gtz p0, :cond_0

    return v0

    .line 24
    :cond_0
    :try_start_0
    invoke-static {p1, p0}, Lepson/print/Util/BmpFileSize;->checkDiskCapacity(Ljava/lang/String;I)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return v0
.end method

.method public static checkDiskCapacity(Ljava/lang/String;I)Z
    .locals 2

    .line 58
    new-instance v0, Landroid/os/StatFs;

    invoke-direct {v0, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 60
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt p0, v1, :cond_0

    .line 61
    invoke-static {v0, p1}, Lepson/print/Util/BmpFileSize;->checkDiskCapacityApi18OrMore(Landroid/os/StatFs;I)Z

    move-result p0

    return p0

    .line 63
    :cond_0
    invoke-static {v0, p1}, Lepson/print/Util/BmpFileSize;->checkDiskCapacityApiLessThan18(Landroid/os/StatFs;I)Z

    move-result p0

    return p0
.end method

.method private static checkDiskCapacityApi18OrMore(Landroid/os/StatFs;I)Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .line 69
    invoke-virtual {p0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    int-to-long v2, p1

    .line 70
    div-long/2addr v2, v0

    const-wide/16 v0, 0x1

    add-long/2addr v2, v0

    .line 71
    invoke-virtual {p0}, Landroid/os/StatFs;->getFreeBlocksLong()J

    move-result-wide p0

    cmp-long v0, p0, v2

    if-gez v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method private static checkDiskCapacityApiLessThan18(Landroid/os/StatFs;I)Z
    .locals 1

    .line 80
    invoke-virtual {p0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    .line 81
    div-int/2addr p1, v0

    const/4 v0, 0x1

    add-int/2addr p1, v0

    .line 82
    invoke-virtual {p0}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result p0

    if-ge p0, p1, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    return v0
.end method

.method private static get24BitBmpSize(Ljava/lang/String;)I
    .locals 1

    .line 41
    new-instance v0, Lepson/print/Util/BmpFileInfo;

    invoke-direct {v0}, Lepson/print/Util/BmpFileInfo;-><init>()V

    .line 42
    invoke-virtual {v0, p0}, Lepson/print/Util/BmpFileInfo;->readParams(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, -0x1

    return p0

    .line 48
    :cond_0
    invoke-virtual {v0}, Lepson/print/Util/BmpFileInfo;->getWidth()I

    move-result p0

    invoke-virtual {v0}, Lepson/print/Util/BmpFileInfo;->getHeight()I

    move-result v0

    mul-int p0, p0, v0

    mul-int/lit8 p0, p0, 0x3

    add-int/lit16 p0, p0, 0x8a

    return p0
.end method

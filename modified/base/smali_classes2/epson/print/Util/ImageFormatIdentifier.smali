.class public Lepson/print/Util/ImageFormatIdentifier;
.super Ljava/lang/Object;
.source "ImageFormatIdentifier.java"


# static fields
.field public static final FILE_FORMAT_BMP:I = 0x1

.field public static final FILE_FORMAT_JPEG:I = 0x3

.field public static final FILE_FORMAT_PNG:I = 0x2

.field public static final FILE_FORMAT_UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static identifyImageFormat(Ljava/lang/String;)I
    .locals 2

    .line 21
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 22
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 23
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 25
    invoke-static {v0}, Lepson/print/Util/ImageFormatIdentifier;->mimeTypeToInt(Landroid/graphics/BitmapFactory$Options;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 32
    :cond_0
    invoke-static {p0}, Lepson/print/Util/ImageFormatIdentifier;->localIdentifyImageFormat(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static localIdentifyImageFormat(Ljava/lang/String;)I
    .locals 1

    .line 60
    new-instance v0, Lepson/print/Util/BmpFileInfo;

    invoke-direct {v0}, Lepson/print/Util/BmpFileInfo;-><init>()V

    .line 61
    invoke-virtual {v0, p0}, Lepson/print/Util/BmpFileInfo;->readParams(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static mimeTypeToInt(Landroid/graphics/BitmapFactory$Options;)I
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    .line 36
    iget-object v1, p0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_0

    .line 40
    :cond_0
    iget-object p0, p0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v1, "image/bmp"

    .line 42
    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    const-string v1, "image/png"

    .line 44
    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p0, 0x2

    return p0

    :cond_2
    const-string v1, "image/jpeg"

    .line 46
    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 p0, 0x3

    return p0

    :cond_3
    return v0

    :cond_4
    :goto_0
    return v0
.end method

.class public Lepson/print/Util/BmpFileInfo;
.super Ljava/lang/Object;
.source "BmpFileInfo.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mBitPerPixel:I

.field private mDataOffset:I

.field private mFileName:Ljava/lang/String;

.field private mFileSize:I

.field private mHeaderSize:I

.field private mHeight:I

.field private mImageDataBytes:I

.field private mOfs:Ljava/io/RandomAccessFile;

.field private mWidth:I

.field private mWriteChannel:Ljava/nio/channels/FileChannel;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createH0Instance(IILjava/lang/String;)Lepson/print/Util/BmpFileInfo;
    .locals 1

    .line 426
    new-instance v0, Lepson/print/Util/BmpFileInfo;

    invoke-direct {v0}, Lepson/print/Util/BmpFileInfo;-><init>()V

    .line 427
    iput-object p2, v0, Lepson/print/Util/BmpFileInfo;->mFileName:Ljava/lang/String;

    const/16 p2, 0x28

    .line 428
    iput p2, v0, Lepson/print/Util/BmpFileInfo;->mHeaderSize:I

    const/16 p2, 0x36

    .line 429
    iput p2, v0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    .line 430
    iput p0, v0, Lepson/print/Util/BmpFileInfo;->mWidth:I

    const/4 p0, 0x0

    .line 431
    iput p0, v0, Lepson/print/Util/BmpFileInfo;->mHeight:I

    .line 432
    iput p1, v0, Lepson/print/Util/BmpFileInfo;->mBitPerPixel:I

    .line 434
    iput p0, v0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    .line 435
    iget p0, v0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    iput p0, v0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    return-object v0
.end method

.method private writeHeader(Ljava/nio/channels/FileChannel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x36

    .line 251
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 252
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 253
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    const/16 v1, 0x42

    .line 255
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v1, 0x4d

    .line 256
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 257
    iget v1, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    .line 259
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 260
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 261
    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 263
    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mHeaderSize:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 264
    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mWidth:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 266
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v2, 0x1

    .line 268
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 269
    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mBitPerPixel:I

    int-to-short v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 271
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 272
    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 274
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 275
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 277
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 278
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const-wide/16 v1, 0x0

    .line 280
    invoke-virtual {p1, v1, v2}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 281
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 282
    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    return-void
.end method


# virtual methods
.method protected addSize(II)V
    .locals 1

    .line 416
    iget v0, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    add-int/2addr v0, p1

    iput v0, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    .line 417
    iget v0, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    add-int/2addr v0, p1

    iput v0, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    .line 418
    iget p1, p0, Lepson/print/Util/BmpFileInfo;->mHeight:I

    add-int/2addr p1, p2

    iput p1, p0, Lepson/print/Util/BmpFileInfo;->mHeight:I

    return-void
.end method

.method public closeWriteableChannel()V
    .locals 2

    .line 331
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 333
    :try_start_0
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :catch_0
    iput-object v1, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    .line 340
    :cond_0
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_1

    .line 342
    :try_start_1
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 346
    :catch_1
    iput-object v1, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    :cond_1
    return-void
.end method

.method public getBitPerPixel()I
    .locals 1

    .line 179
    iget v0, p0, Lepson/print/Util/BmpFileInfo;->mBitPerPixel:I

    return v0
.end method

.method public getFileSize()I
    .locals 1

    .line 167
    iget v0, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .line 175
    iget v0, p0, Lepson/print/Util/BmpFileInfo;->mHeight:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .line 171
    iget v0, p0, Lepson/print/Util/BmpFileInfo;->mWidth:I

    return v0
.end method

.method protected getWritableChannelChannel()Ljava/nio/channels/WritableByteChannel;
    .locals 1

    .line 352
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    return-object v0
.end method

.method public prepareAppendData()Z
    .locals 4

    const/4 v0, 0x0

    .line 296
    iput-object v0, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    .line 299
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lepson/print/Util/BmpFileInfo;->mFileName:Ljava/lang/String;

    const-string v3, "rw"

    invoke-direct {v1, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    .line 300
    iget-object v1, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    iput-object v1, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    .line 302
    iget-object v1, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    invoke-direct {p0, v1}, Lepson/print/Util/BmpFileInfo;->writeHeader(Ljava/nio/channels/FileChannel;)V

    .line 304
    iget-object v1, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v1

    .line 307
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 309
    iget-object v1, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_0

    .line 311
    :try_start_1
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    nop

    .line 316
    :cond_0
    :goto_0
    iget-object v1, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    if-eqz v1, :cond_1

    .line 318
    :try_start_2
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 322
    :catch_2
    iput-object v0, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public readParams(Ljava/lang/String;)Z
    .locals 10

    .line 45
    iput-object p1, p0, Lepson/print/Util/BmpFileInfo;->mFileName:Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 47
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 53
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_15
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 54
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    const/16 p1, 0x200

    .line 56
    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 58
    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 59
    invoke-virtual {v2, p1}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v5
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_12
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long v5, v5

    const-wide/16 v7, 0x1e

    cmp-long v9, v5, v7

    if-gez v9, :cond_1

    .line 150
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    if-eqz v2, :cond_0

    .line 158
    :try_start_3
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    :cond_0
    return v3

    .line 64
    :cond_1
    :try_start_4
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 67
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    .line 68
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v6

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    .line 69
    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    const/16 v7, 0x42

    if-ne v5, v7, :cond_15

    invoke-virtual {v6}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    const/16 v6, 0x4d

    if-eq v5, v6, :cond_2

    goto/16 :goto_2

    .line 72
    :cond_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    iput v5, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    .line 73
    iget v5, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    int-to-long v5, v5

    cmp-long v7, v5, v0

    if-eqz v7, :cond_4

    .line 74
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "filesize error. header filesize <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ">. actual file size <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ">"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_12
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 150
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    if-eqz v2, :cond_3

    .line 158
    :try_start_6
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    :cond_3
    return v3

    :cond_4
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_7

    .line 81
    :try_start_7
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_12
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v1, :cond_6

    .line 150
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    if-eqz v2, :cond_5

    .line 158
    :try_start_9
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    :cond_5
    return v3

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_7
    :try_start_a
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    .line 88
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lepson/print/Util/BmpFileInfo;->mHeaderSize:I

    .line 89
    iget v0, p0, Lepson/print/Util/BmpFileInfo;->mHeaderSize:I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_12
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const/16 v5, 0x28

    if-ge v0, v5, :cond_9

    .line 150
    :try_start_b
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    if-eqz v2, :cond_8

    .line 158
    :try_start_c
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    :catch_7
    :cond_8
    return v3

    .line 93
    :cond_9
    :try_start_d
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lepson/print/Util/BmpFileInfo;->mWidth:I

    .line 94
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lepson/print/Util/BmpFileInfo;->mHeight:I

    .line 95
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    const/4 v5, 0x1

    if-eq v0, v5, :cond_b

    .line 97
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "plain error <"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "> != 1"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_12
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 150
    :try_start_e
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    :catch_8
    if-eqz v2, :cond_a

    .line 158
    :try_start_f
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_9

    :catch_9
    :cond_a
    return v3

    .line 100
    :cond_b
    :try_start_10
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    iput v0, p0, Lepson/print/Util/BmpFileInfo;->mBitPerPixel:I

    .line 102
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    if-eqz v0, :cond_d

    .line 104
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "compression error <"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "> != 0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_12
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 150
    :try_start_11
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    :catch_a
    if-eqz v2, :cond_c

    .line 158
    :try_start_12
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_b

    :catch_b
    :cond_c
    return v3

    .line 109
    :cond_d
    :try_start_13
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p1

    iput p1, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    .line 110
    iget p1, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    const/4 v0, 0x4

    const/4 v6, 0x3

    if-nez p1, :cond_12

    .line 112
    iget p1, p0, Lepson/print/Util/BmpFileInfo;->mBitPerPixel:I
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_12
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    const/16 v7, 0x8

    if-eq p1, v7, :cond_10

    const/16 v7, 0x18

    if-eq p1, v7, :cond_f

    .line 150
    :try_start_14
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_c

    :catch_c
    if-eqz v2, :cond_e

    .line 158
    :try_start_15
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_d

    :catch_d
    :cond_e
    return v3

    :cond_f
    const/4 p1, 0x3

    goto :goto_1

    :cond_10
    const/4 p1, 0x1

    .line 121
    :goto_1
    :try_start_16
    iget v7, p0, Lepson/print/Util/BmpFileInfo;->mWidth:I

    mul-int p1, p1, v7

    and-int/lit8 v7, p1, 0x3

    if-eqz v7, :cond_11

    rsub-int/lit8 v7, v7, 0x4

    add-int/2addr p1, v7

    .line 128
    :cond_11
    iget v7, p0, Lepson/print/Util/BmpFileInfo;->mHeight:I

    mul-int p1, p1, v7

    iput p1, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    .line 131
    :cond_12
    iget p1, p0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    iget v7, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    add-int/2addr p1, v7

    iget v7, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    if-eq p1, v7, :cond_13

    .line 132
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "unsupported data size, file size."

    invoke-virtual {p1, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 133
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, " file size <%d>, data size <%d> data offset <%d>. sum <%d>"

    new-array v0, v0, [Ljava/lang/Object;

    iget v9, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    .line 137
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v0, v3

    iget v9, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v0, v5

    iget v9, p0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    .line 138
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v0, v1

    iget v1, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    iget v9, p0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    add-int/2addr v1, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    .line 135
    invoke-static {v7, v8, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_12
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 150
    :cond_13
    :try_start_17
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_e

    :catch_e
    if-eqz v2, :cond_14

    .line 158
    :try_start_18
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_f

    :catch_f
    :cond_14
    return v5

    .line 150
    :cond_15
    :goto_2
    :try_start_19
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_10

    :catch_10
    if-eqz v2, :cond_16

    .line 158
    :try_start_1a
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_11

    :catch_11
    :cond_16
    return v3

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_12
    nop

    goto :goto_5

    :catchall_1
    move-exception p1

    move-object v4, v2

    :goto_3
    if-eqz v4, :cond_17

    .line 150
    :try_start_1b
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_13

    goto :goto_4

    :catch_13
    nop

    :cond_17
    :goto_4
    if-eqz v2, :cond_18

    .line 158
    :try_start_1c
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_14

    .line 160
    :catch_14
    :cond_18
    throw p1

    :catch_15
    move-object v4, v2

    :goto_5
    if-eqz v4, :cond_19

    .line 150
    :try_start_1d
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_16

    goto :goto_6

    :catch_16
    nop

    :cond_19
    :goto_6
    if-eqz v2, :cond_1a

    .line 158
    :try_start_1e
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_17

    :catch_17
    :cond_1a
    return v3
.end method

.method public transferImageData(Ljava/nio/channels/WritableByteChannel;)I
    .locals 10

    .line 186
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mFileName:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 193
    :cond_0
    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    if-lez v2, :cond_9

    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    if-gtz v2, :cond_1

    goto/16 :goto_5

    :cond_1
    const/4 v2, 0x0

    .line 200
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 201
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 203
    :try_start_2
    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mDataOffset:I

    int-to-long v5, v2

    iget v2, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    int-to-long v7, v2

    move-object v4, v0

    move-object v9, p1

    invoke-virtual/range {v4 .. v9}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    move-result-wide v4

    .line 205
    iget p1, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    int-to-long v6, p1

    cmp-long p1, v4, v6

    if-gez p1, :cond_3

    .line 214
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    if-eqz v0, :cond_2

    .line 221
    :try_start_4
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    :cond_2
    return v1

    .line 214
    :cond_3
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    if-eqz v0, :cond_4

    .line 221
    :try_start_6
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 228
    :catch_3
    :cond_4
    iget p1, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    return p1

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_4
    move-exception p1

    goto :goto_0

    :catchall_1
    move-exception p1

    move-object v0, v2

    goto :goto_3

    :catch_5
    move-exception p1

    move-object v0, v2

    :goto_0
    move-object v2, v3

    goto :goto_1

    :catchall_2
    move-exception p1

    move-object v0, v2

    move-object v3, v0

    goto :goto_3

    :catch_6
    move-exception p1

    move-object v0, v2

    .line 209
    :goto_1
    :try_start_7
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v2, :cond_5

    .line 214
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    goto :goto_2

    :catch_7
    nop

    :cond_5
    :goto_2
    if-eqz v0, :cond_6

    .line 221
    :try_start_9
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    :catch_8
    :cond_6
    return v1

    :catchall_3
    move-exception p1

    move-object v3, v2

    :goto_3
    if-eqz v3, :cond_7

    .line 214
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    goto :goto_4

    :catch_9
    nop

    :cond_7
    :goto_4
    if-eqz v0, :cond_8

    .line 221
    :try_start_b
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    .line 224
    :catch_a
    :cond_8
    throw p1

    :cond_9
    :goto_5
    return v1
.end method

.method public transferImageData(Lepson/print/Util/BmpFileInfo;)Z
    .locals 2

    .line 232
    invoke-virtual {p1}, Lepson/print/Util/BmpFileInfo;->getWritableChannelChannel()Ljava/nio/channels/WritableByteChannel;

    move-result-object v0

    .line 233
    invoke-virtual {p0, v0}, Lepson/print/Util/BmpFileInfo;->transferImageData(Ljava/nio/channels/WritableByteChannel;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 238
    :cond_0
    iget v1, p0, Lepson/print/Util/BmpFileInfo;->mHeight:I

    invoke-virtual {p1, v0, v1}, Lepson/print/Util/BmpFileInfo;->addSize(II)V

    const/4 p1, 0x1

    return p1
.end method

.method public updateHeader()Z
    .locals 10

    const/16 v0, 0x26

    .line 367
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 369
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 372
    :try_start_0
    iget-object v3, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 373
    iget-object v3, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v6, v3

    const-wide/16 v8, 0x26

    cmp-long v3, v6, v8

    if-gez v3, :cond_2

    .line 393
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    if-eqz v0, :cond_0

    .line 395
    :try_start_1
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 399
    :catch_0
    iput-object v2, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    .line 401
    :cond_0
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_1

    .line 403
    :try_start_2
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 407
    :catch_1
    iput-object v2, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    :cond_1
    return v1

    :cond_2
    const/4 v3, 0x2

    .line 378
    :try_start_3
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 379
    iget v3, p0, Lepson/print/Util/BmpFileInfo;->mFileSize:I

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/16 v3, 0x16

    .line 380
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 381
    iget v3, p0, Lepson/print/Util/BmpFileInfo;->mHeight:I

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/16 v3, 0x22

    .line 382
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 383
    iget v3, p0, Lepson/print/Util/BmpFileInfo;->mImageDataBytes:I

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 385
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 386
    iget-object v3, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 387
    iget-object v3, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 393
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    if-eqz v0, :cond_3

    .line 395
    :try_start_4
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 399
    :catch_2
    iput-object v2, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    .line 401
    :cond_3
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_4

    .line 403
    :try_start_5
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 407
    :catch_3
    iput-object v2, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    :cond_4
    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_4
    move-exception v0

    .line 390
    :try_start_6
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 393
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    if-eqz v0, :cond_5

    .line 395
    :try_start_7
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 399
    :catch_5
    iput-object v2, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    .line 401
    :cond_5
    iget-object v0, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_6

    .line 403
    :try_start_8
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 407
    :catch_6
    iput-object v2, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    :cond_6
    return v1

    .line 393
    :goto_0
    iget-object v1, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    if-eqz v1, :cond_7

    .line 395
    :try_start_9
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 399
    :catch_7
    iput-object v2, p0, Lepson/print/Util/BmpFileInfo;->mWriteChannel:Ljava/nio/channels/FileChannel;

    .line 401
    :cond_7
    iget-object v1, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_8

    .line 403
    :try_start_a
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 407
    :catch_8
    iput-object v2, p0, Lepson/print/Util/BmpFileInfo;->mOfs:Ljava/io/RandomAccessFile;

    :cond_8
    throw v0
.end method

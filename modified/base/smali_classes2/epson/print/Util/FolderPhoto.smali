.class public Lepson/print/Util/FolderPhoto;
.super Ljava/lang/Object;
.source "FolderPhoto.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private FolPath:Ljava/lang/String;

.field private count:I

.field private mb:[Landroid/graphics/Bitmap;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lepson/print/Util/FolderPhoto;->count:I

    .line 32
    iput-object p2, p0, Lepson/print/Util/FolderPhoto;->FolPath:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lepson/print/Util/FolderPhoto;->name:Ljava/lang/String;

    const/4 p1, 0x0

    .line 34
    iput-object p1, p0, Lepson/print/Util/FolderPhoto;->mb:[Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x1

    if-ne p0, p1, :cond_1

    return v1

    .line 70
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v0

    .line 73
    :cond_2
    check-cast p1, Lepson/print/Util/FolderPhoto;

    .line 74
    invoke-virtual {p0}, Lepson/print/Util/FolderPhoto;->getFolPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lepson/print/Util/FolderPhoto;->getFolPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    return v1

    :cond_3
    return v0
.end method

.method public getCount()I
    .locals 1

    .line 46
    iget v0, p0, Lepson/print/Util/FolderPhoto;->count:I

    return v0
.end method

.method public getFolPath()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lepson/print/Util/FolderPhoto;->FolPath:Ljava/lang/String;

    return-object v0
.end method

.method public getMb()[Landroid/graphics/Bitmap;
    .locals 1

    .line 22
    iget-object v0, p0, Lepson/print/Util/FolderPhoto;->mb:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lepson/print/Util/FolderPhoto;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setCount(I)V
    .locals 0

    .line 50
    iput p1, p0, Lepson/print/Util/FolderPhoto;->count:I

    return-void
.end method

.method public setFolPath(Ljava/lang/String;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lepson/print/Util/FolderPhoto;->FolPath:Ljava/lang/String;

    return-void
.end method

.method public setMb([Landroid/graphics/Bitmap;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lepson/print/Util/FolderPhoto;->mb:[Landroid/graphics/Bitmap;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lepson/print/Util/FolderPhoto;->name:Ljava/lang/String;

    return-void
.end method

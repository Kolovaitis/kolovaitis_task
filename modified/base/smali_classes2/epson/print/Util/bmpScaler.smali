.class public Lepson/print/Util/bmpScaler;
.super Ljava/lang/Object;
.source "bmpScaler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/Util/bmpScaler$Size;
    }
.end annotation


# instance fields
.field private scaled:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 22
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 23
    :try_start_1
    invoke-direct {p0, v1, p2}, Lepson/print/Util/bmpScaler;->getRoughSize(Ljava/io/InputStream;I)Lepson/print/Util/bmpScaler$Size;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 25
    :try_start_2
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 26
    :try_start_3
    invoke-direct {p0, v2, v0}, Lepson/print/Util/bmpScaler;->roughScaleImage(Ljava/io/InputStream;Lepson/print/Util/bmpScaler$Size;)V

    if-nez p3, :cond_0

    .line 28
    invoke-direct {p0, p2}, Lepson/print/Util/bmpScaler;->scaleImage(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 31
    :cond_0
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 34
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_0
    move-exception p1

    move-object v0, v2

    goto :goto_1

    :catchall_1
    move-exception p1

    move-object v0, v2

    goto :goto_0

    :catchall_2
    move-exception p1

    move-object v0, v1

    .line 31
    :goto_0
    :try_start_5
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    :catchall_3
    move-exception p1

    move-object v0, v1

    goto :goto_1

    :catchall_4
    move-exception p1

    .line 34
    :goto_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw p1
.end method

.method private getRoughSize(III)Lepson/print/Util/bmpScaler$Size;
    .locals 3

    .line 76
    new-instance v0, Lepson/print/Util/bmpScaler$Size;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lepson/print/Util/bmpScaler$Size;-><init>(Lepson/print/Util/bmpScaler$1;)V

    .line 77
    div-int v1, p1, p3

    int-to-float v1, v1

    iput v1, v0, Lepson/print/Util/bmpScaler$Size;->scale:F

    const/4 v1, 0x1

    .line 78
    iput v1, v0, Lepson/print/Util/bmpScaler$Size;->sample:I

    int-to-float v1, p2

    .line 81
    iget v2, v0, Lepson/print/Util/bmpScaler$Size;->scale:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 83
    :goto_0
    div-int/lit8 p1, p1, 0x2

    if-lt p1, p3, :cond_1

    div-int/lit8 p2, p2, 0x2

    if-ge p2, v1, :cond_0

    goto :goto_1

    .line 88
    :cond_0
    iget v2, v0, Lepson/print/Util/bmpScaler$Size;->sample:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, v0, Lepson/print/Util/bmpScaler$Size;->sample:I

    goto :goto_0

    :cond_1
    :goto_1
    return-object v0
.end method

.method private getRoughSize(Ljava/io/InputStream;I)Lepson/print/Util/bmpScaler$Size;
    .locals 2

    .line 68
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 69
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v1, 0x0

    .line 70
    invoke-static {p1, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 71
    iget p1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {p0, p1, v0, p2}, Lepson/print/Util/bmpScaler;->getRoughSize(III)Lepson/print/Util/bmpScaler$Size;

    move-result-object p1

    return-object p1
.end method

.method private roughScaleImage(Ljava/io/InputStream;Lepson/print/Util/bmpScaler$Size;)V
    .locals 3

    .line 53
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 54
    iget v1, p2, Lepson/print/Util/bmpScaler$Size;->scale:F

    iget v2, p2, Lepson/print/Util/bmpScaler$Size;->scale:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 55
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 58
    :goto_0
    :try_start_0
    iget v1, p2, Lepson/print/Util/bmpScaler$Size;->sample:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v1, 0x0

    .line 59
    invoke-static {p1, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lepson/print/Util/bmpScaler;->scaled:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 62
    :catch_0
    iget v1, p2, Lepson/print/Util/bmpScaler$Size;->sample:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p2, Lepson/print/Util/bmpScaler$Size;->sample:I

    goto :goto_0
.end method

.method private scaleImage(I)V
    .locals 8

    .line 41
    iget-object v0, p0, Lepson/print/Util/bmpScaler;->scaled:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 42
    iget-object v0, p0, Lepson/print/Util/bmpScaler;->scaled:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float p1, p1

    int-to-float v0, v4

    div-float v0, p1, v0

    .line 45
    iget-object v1, p0, Lepson/print/Util/bmpScaler;->scaled:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, p1

    int-to-float p1, v5

    div-float v1, p1, v1

    float-to-int v1, v1

    int-to-float v1, v1

    div-float/2addr v1, p1

    .line 48
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 49
    invoke-virtual {v6, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 50
    iget-object v1, p0, Lepson/print/Util/bmpScaler;->scaled:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/Util/bmpScaler;->scaled:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method public getScaled()Landroid/graphics/Bitmap;
    .locals 1

    .line 38
    iget-object v0, p0, Lepson/print/Util/bmpScaler;->scaled:Landroid/graphics/Bitmap;

    return-object v0
.end method

.class public Lepson/print/ActivityViewImageSelect;
.super Lepson/print/ActivityIACommon;
.source "ActivityViewImageSelect.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lepson/print/phlayout/PhotoPreview$ViewSizeChangeListener;
.implements Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;,
        Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;,
        Lepson/print/ActivityViewImageSelect$AllReloadTask;
    }
.end annotation


# static fields
.field private static final ALL_REFRESH_DRAW_END:I = 0x4

.field private static final DIALOG_ALL_UPDATE:Ljava/lang/String; = "dialog_all_update"

.field private static final DIALOG_KEY_STORE_DIALOG:Ljava/lang/String; = "store-dialog"

.field private static final DIALOG_LOAD:Ljava/lang/String; = "dialog_load"

.field private static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field private static final FROM_EPSON_PHOTO:I = 0x0

.field private static final FROM_NOT_PHOTO:I = 0x1

.field public static final LIST_ALBUM:Ljava/lang/String; = "listAlbum"

.field private static final LOAD_ALL_IMAGE:I = 0x3

.field static final MAX_IMAGE_NUMBER:I = 0x1e

.field private static final MESSAGE_OUT_OF_MEMORY_ERROR_NOTIFY:I = 0x8

.field private static final OUT_OF_MEMORY_ERROR:I = 0x7

.field public static final PARAMS_KEY_EPSON_COLOR_MODE:Ljava/lang/String; = "epson_color_mode"

.field public static final PARAMS_KEY_FROM_EPSON:Ljava/lang/String; = "FROM_EPSON"

.field public static final PARAMS_KEY_IMAGE_LIST:Ljava/lang/String; = "imageList"

.field public static final PARAMS_KEY_PRINT_LOG:Ljava/lang/String; = "print_log"

.field private static final PREVIEW_LOAD_DIALOG_DISMISS:I = 0x1

.field private static final PREVIEW_LOAD_DIALOG_SHOW:I = 0x0

.field private static final REQUEST_CODE_LICENSE_CHECK:I = 0xa

.field private static final RESULT_RUNTIMEPERMMISSION:I = 0x9

.field private static final START_PRINT:I = 0x5

.field private static final TAG:Ljava/lang/String; = "ActivityViewImageSelect"

.field private static final UPDATE_PAPERSIZE_TEXT:I = 0x6

.field private static final ZOOM_CONTROL:I = 0x2


# instance fields
.field private aPaperSourceSetting:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/screen/PaperSourceSetting;",
            ">;"
        }
    .end annotation
.end field

.field private addButton:Landroid/widget/ImageButton;

.field bAutoStartPrint:Z

.field bRequestPermission:Z

.field private context:Landroid/content/Context;

.field private currentColor:I

.field private currentLayout:I

.field private currentLayoutMulti:I

.field private currentPaperSize:I

.field private currentPrinterName:Ljava/lang/String;

.field private deleteButton:Landroid/widget/Button;

.field private enableShowWarning:Z

.field private gallery:Landroid/widget/Gallery;

.field private galleryAdapter:Lepson/print/GalleryAdapter;

.field private final imageList:Lepson/print/phlayout/PhotoPreviewImageList;

.field private imageNo:I

.field private isCustomAction:Z

.field private isPaperLandscape:Z

.field private isPrintedOK:Z

.field private isRemotePrinter:Z

.field private mAllReloadTaskDone:Z

.field protected final mAllReloadTaskLock:Ljava/lang/Object;

.field private volatile mAllReloadTaskRetryRequested:Z

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private mEnableEpsonColorDisplay:Z

.field private mEpsonColorImageView:Landroid/widget/ImageView;

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field private mFileCheckEnd:Z

.field mHandler:Landroid/os/Handler;

.field private final mImageFilenameListFromIntent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIntentFileType:I

.field protected volatile mIsAllReloadTaskRetryAcceptable:Z

.field private mLicenseCheckDone:Z

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mOnSizeChangeReceived:Z

.field private mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

.field private mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

.field private mStartActivityFromPhoto:I

.field private pageTextView:Landroid/widget/TextView;

.field private paperMissmath:Landroid/widget/ImageView;

.field paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

.field private papersizeTextView:Landroid/widget/TextView;

.field protected previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

.field private printButton:Landroid/widget/Button;

.field private printerId:Ljava/lang/String;

.field private reloadTask:Lepson/print/ActivityViewImageSelect$AllReloadTask;

.field private rotateButton:Landroid/widget/Button;

.field private zoomControls:Landroid/widget/ZoomControls;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 94
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 139
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->papersizeTextView:Landroid/widget/TextView;

    .line 142
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->pageTextView:Landroid/widget/TextView;

    .line 143
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->zoomControls:Landroid/widget/ZoomControls;

    .line 144
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->rotateButton:Landroid/widget/Button;

    .line 145
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    .line 146
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->addButton:Landroid/widget/ImageButton;

    .line 147
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->deleteButton:Landroid/widget/Button;

    .line 148
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    .line 149
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->paperMissmath:Landroid/widget/ImageView;

    const/4 v1, 0x0

    .line 159
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    .line 163
    new-instance v2, Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-direct {v2}, Lepson/print/phlayout/PhotoPreviewImageList;-><init>()V

    iput-object v2, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    .line 170
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    .line 172
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->galleryAdapter:Lepson/print/GalleryAdapter;

    .line 173
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    const-string v2, ""

    .line 175
    iput-object v2, p0, Lepson/print/ActivityViewImageSelect;->currentPrinterName:Ljava/lang/String;

    const-string v2, ""

    .line 176
    iput-object v2, p0, Lepson/print/ActivityViewImageSelect;->printerId:Ljava/lang/String;

    const/4 v2, 0x2

    .line 177
    iput v2, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    .line 178
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    .line 179
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->currentPaperSize:I

    .line 180
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->currentColor:I

    .line 181
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->isPaperLandscape:Z

    .line 185
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    const/4 v2, 0x1

    .line 188
    iput-boolean v2, p0, Lepson/print/ActivityViewImageSelect;->enableShowWarning:Z

    .line 191
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->reloadTask:Lepson/print/ActivityViewImageSelect$AllReloadTask;

    .line 192
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->context:Landroid/content/Context;

    .line 195
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->isPrintedOK:Z

    .line 196
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->isCustomAction:Z

    .line 201
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mIntentFileType:I

    .line 204
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->bRequestPermission:Z

    .line 222
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    .line 229
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskLock:Ljava/lang/Object;

    .line 258
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->bAutoStartPrint:Z

    .line 1547
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lepson/print/ActivityViewImageSelect$4;

    invoke-direct {v2, p0}, Lepson/print/ActivityViewImageSelect$4;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    .line 2524
    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 2525
    new-instance v0, Lepson/print/ActivityViewImageSelect$9;

    invoke-direct {v0, p0}, Lepson/print/ActivityViewImageSelect$9;-><init>(Lepson/print/ActivityViewImageSelect;)V

    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 2550
    new-instance v0, Lepson/print/ActivityViewImageSelect$10;

    invoke-direct {v0, p0}, Lepson/print/ActivityViewImageSelect$10;-><init>(Lepson/print/ActivityViewImageSelect;)V

    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 94
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->showStoreDialog()V

    return-void
.end method

.method static synthetic access$100(Lepson/print/ActivityViewImageSelect;)I
    .locals 0

    .line 94
    iget p0, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    return p0
.end method

.method static synthetic access$1000(Lepson/print/ActivityViewImageSelect;)Lepson/print/ActivityViewImageSelect$AllReloadTask;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->reloadTask:Lepson/print/ActivityViewImageSelect$AllReloadTask;

    return-object p0
.end method

.method static synthetic access$1002(Lepson/print/ActivityViewImageSelect;Lepson/print/ActivityViewImageSelect$AllReloadTask;)Lepson/print/ActivityViewImageSelect$AllReloadTask;
    .locals 0

    .line 94
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect;->reloadTask:Lepson/print/ActivityViewImageSelect$AllReloadTask;

    return-object p1
.end method

.method static synthetic access$102(Lepson/print/ActivityViewImageSelect;I)I
    .locals 0

    .line 94
    iput p1, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    return p1
.end method

.method static synthetic access$1100(Lepson/print/ActivityViewImageSelect;)Landroid/content/Context;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->context:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$1200(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 94
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->startPrint()V

    return-void
.end method

.method static synthetic access$1300(Lepson/print/ActivityViewImageSelect;)I
    .locals 0

    .line 94
    iget p0, p0, Lepson/print/ActivityViewImageSelect;->currentPaperSize:I

    return p0
.end method

.method static synthetic access$1302(Lepson/print/ActivityViewImageSelect;I)I
    .locals 0

    .line 94
    iput p1, p0, Lepson/print/ActivityViewImageSelect;->currentPaperSize:I

    return p1
.end method

.method static synthetic access$1400(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->papersizeTextView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1500(Lepson/print/ActivityViewImageSelect;I)V
    .locals 0

    .line 94
    invoke-direct {p0, p1}, Lepson/print/ActivityViewImageSelect;->showSimpleDialog(I)V

    return-void
.end method

.method static synthetic access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    return-object p0
.end method

.method static synthetic access$1700(Lepson/print/ActivityViewImageSelect;)Landroid/widget/Button;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->deleteButton:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$1800(Lepson/print/ActivityViewImageSelect;)Landroid/widget/Button;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$1900(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->pageTextView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$200(Lepson/print/ActivityViewImageSelect;)Landroid/widget/Gallery;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    return-object p0
.end method

.method static synthetic access$2000(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 94
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->updateGallery()V

    return-void
.end method

.method static synthetic access$2100(Lepson/print/ActivityViewImageSelect;)Ljava/util/ArrayList;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->aPaperSourceSetting:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$2102(Lepson/print/ActivityViewImageSelect;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .line 94
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect;->aPaperSourceSetting:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$2200(Lepson/print/ActivityViewImageSelect;)Landroid/widget/ImageView;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->paperMissmath:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$2700(Lepson/print/ActivityViewImageSelect;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$2702(Lepson/print/ActivityViewImageSelect;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 94
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$2800(Lepson/print/ActivityViewImageSelect;)Z
    .locals 0

    .line 94
    iget-boolean p0, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    return p0
.end method

.method static synthetic access$2900(Lepson/print/ActivityViewImageSelect;)I
    .locals 0

    .line 94
    iget p0, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    return p0
.end method

.method static synthetic access$2902(Lepson/print/ActivityViewImageSelect;I)I
    .locals 0

    .line 94
    iput p1, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    return p1
.end method

.method static synthetic access$3000(Lepson/print/ActivityViewImageSelect;)I
    .locals 0

    .line 94
    iget p0, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    return p0
.end method

.method static synthetic access$3002(Lepson/print/ActivityViewImageSelect;I)I
    .locals 0

    .line 94
    iput p1, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    return p1
.end method

.method static synthetic access$3100(Lepson/print/ActivityViewImageSelect;)I
    .locals 0

    .line 94
    iget p0, p0, Lepson/print/ActivityViewImageSelect;->currentColor:I

    return p0
.end method

.method static synthetic access$3102(Lepson/print/ActivityViewImageSelect;I)I
    .locals 0

    .line 94
    iput p1, p0, Lepson/print/ActivityViewImageSelect;->currentColor:I

    return p1
.end method

.method static synthetic access$3200(Lepson/print/ActivityViewImageSelect;)Z
    .locals 0

    .line 94
    iget-boolean p0, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskRetryRequested:Z

    return p0
.end method

.method static synthetic access$3202(Lepson/print/ActivityViewImageSelect;Z)Z
    .locals 0

    .line 94
    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskRetryRequested:Z

    return p1
.end method

.method static synthetic access$3300(Lepson/print/ActivityViewImageSelect;)Z
    .locals 0

    .line 94
    iget-boolean p0, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskDone:Z

    return p0
.end method

.method static synthetic access$3302(Lepson/print/ActivityViewImageSelect;Z)Z
    .locals 0

    .line 94
    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskDone:Z

    return p1
.end method

.method static synthetic access$3400(Lepson/print/ActivityViewImageSelect;)Z
    .locals 0

    .line 94
    iget-boolean p0, p0, Lepson/print/ActivityViewImageSelect;->isPaperLandscape:Z

    return p0
.end method

.method static synthetic access$3402(Lepson/print/ActivityViewImageSelect;Z)Z
    .locals 0

    .line 94
    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->isPaperLandscape:Z

    return p1
.end method

.method static synthetic access$3500(Lepson/print/ActivityViewImageSelect;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$400(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 94
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->clearListToastAndFinish()V

    return-void
.end method

.method static synthetic access$500(Lepson/print/ActivityViewImageSelect;Ljava/util/ArrayList;)Z
    .locals 0

    .line 94
    invoke-direct {p0, p1}, Lepson/print/ActivityViewImageSelect;->setPhotoImageList(Ljava/util/ArrayList;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$600(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 94
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->updateImageListDisplay()V

    return-void
.end method

.method static synthetic access$702(Lepson/print/ActivityViewImageSelect;Z)Z
    .locals 0

    .line 94
    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->mFileCheckEnd:Z

    return p1
.end method

.method static synthetic access$800(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 94
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->loadImage()V

    return-void
.end method

.method static synthetic access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;
    .locals 0

    .line 94
    iget-object p0, p0, Lepson/print/ActivityViewImageSelect;->mModelDialog:Lepson/common/DialogProgressViewModel;

    return-object p0
.end method

.method private addImageByIntent(Landroid/content/Intent;Landroid/net/Uri;)V
    .locals 7

    .line 923
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object p1

    .line 925
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://"

    .line 927
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "_data"

    .line 930
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    .line 936
    :try_start_0
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    if-nez v0, :cond_0

    .line 949
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://com.android.gallery3d.provider"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ActivityViewImageSelect"

    .line 950
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Actual URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "com.android.gallery3d"

    const-string v1, "com.google.android.gallery3d"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    const-string v0, "ActivityViewImageSelect"

    .line 953
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Effective URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 956
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 960
    :cond_1
    :goto_0
    invoke-direct {p0, p2, p1}, Lepson/print/ActivityViewImageSelect;->getContentFromCR(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 962
    iget-object p2, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    .line 967
    :cond_2
    invoke-virtual {p0, p2}, Lepson/print/ActivityViewImageSelect;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 969
    iget-object p2, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string p1, "ActivityViewImageSelect"

    const-string p2, "addImageByIntent by getRealPathFromURI"

    .line 970
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_3
    const-string v1, "file://"

    .line 974
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz p1, :cond_4

    .line 978
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 979
    invoke-direct {p0, p2, p1}, Lepson/print/ActivityViewImageSelect;->getContentFromCR(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 981
    iget-object p2, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    .line 987
    :cond_4
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    const-string p2, "/file:"

    .line 991
    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_5

    const/4 p2, 0x6

    .line 992
    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 996
    :cond_5
    invoke-static {p1}, Lepson/common/Utils;->isPhotoFile(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_6

    .line 997
    iget-object p2, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string p1, "ActivityViewImageSelect"

    const-string p2, "addImageByIntent by isPhotoFile()"

    .line 998
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 1004
    :cond_6
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->clearListToastAndFinish()V

    return-void
.end method

.method private addPhotos()V
    .locals 1

    .line 1731
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->getArgumentFileList()Ljava/util/ArrayList;

    move-result-object v0

    .line 1732
    invoke-static {p0, v0}, Lepson/print/imgsel/PhotoImageSelectActivity;->startAddImageSelect(Landroid/app/Activity;Ljava/util/ArrayList;)V

    return-void
.end method

.method public static checkBmpNot24Bit(Lepson/print/EPImage;)Z
    .locals 2
    .param p0    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 902
    iget-object v0, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    const-string v1, "image/bmp"

    invoke-static {v0, v1}, Lepson/common/Utils;->checkMimeType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 904
    new-instance v0, Lepson/print/EPImageUtil;

    invoke-direct {v0}, Lepson/print/EPImageUtil;-><init>()V

    .line 906
    iget-object p0, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lepson/print/EPImageUtil;->getBitCount(Ljava/lang/String;)I

    move-result p0

    const/16 v0, 0x18

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static checkEPImage(Lepson/print/EPImage;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 1121
    :cond_0
    iget v1, p0, Lepson/print/EPImage;->srcWidth:I

    if-lez v1, :cond_2

    iget p0, p0, Lepson/print/EPImage;->srcHeight:I

    if-gtz p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0

    :cond_2
    :goto_0
    return v0
.end method

.method private checkExternalStorageReadPermission()Z
    .locals 1

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 543
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private clearListToastAndFinish()V
    .locals 2

    const v0, 0x7f0e0373

    .line 891
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 892
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->clear()V

    .line 893
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->onBackPressed()V

    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 2120
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 2122
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private getContentFromCR(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const-string v0, "ActivityViewImageSelect"

    .line 1016
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getContentFromCR uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " fType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    :try_start_0
    invoke-static {p2}, Lepson/common/Utils;->makeTempFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "ActivityViewImageSelect"

    .line 1023
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getContentFromCR Unknown fType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    :goto_0
    const/4 v0, 0x0

    .line 1042
    :try_start_1
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p1

    .line 1043
    new-instance v1, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getTempCRDir()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    new-instance p2, Ljava/io/FileOutputStream;

    invoke-direct {p2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v2, 0x400

    .line 1045
    new-array v2, v2, [B

    .line 1047
    :goto_1
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v4, 0x0

    .line 1048
    invoke-virtual {p2, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1

    .line 1050
    :cond_0
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V

    .line 1055
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->getExtention(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1057
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->getRealMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 1059
    invoke-static {p1}, Lepson/common/Utils;->getMimeExt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_1
    move-object p1, v0

    :goto_2
    if-eqz p1, :cond_2

    .line 1063
    new-instance p2, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1064
    invoke-virtual {v1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_3

    :cond_2
    move-object p2, v1

    :goto_3
    const-string p1, "ActivityViewImageSelect"

    const-string v1, "getContentFromCR success"

    .line 1070
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    .line 1075
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    return-object v0
.end method

.method private getPaperSizeName()Ljava/lang/String;
    .locals 3

    .line 1139
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    .line 1141
    new-instance v1, Lepson/print/screen/PrintSetting;

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v1, p0, v2}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1142
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 1144
    iget v1, v1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;->getStringId(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isEpsonColorMode()Z
    .locals 5

    .line 1385
    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mEnableEpsonColorDisplay:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1389
    :cond_0
    new-instance v0, Lepson/print/screen/PrintSetting;

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, p0, v2}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1390
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 1392
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getApfValue(Lepson/print/screen/PrintSetting;)I

    move-result v2

    .line 1394
    iget v3, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    iget v0, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {p0, v3, v0, v1}, Lcom/epson/iprint/apf/ApfAdapter;->isEpsonColor(Landroid/content/Context;IIZ)Z

    move-result v0

    return v0
.end method

.method private isPermissionRequestRequired(Ljava/util/ArrayList;)Z
    .locals 3
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 845
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 850
    :cond_0
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->checkExternalStorageReadPermission()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 857
    :cond_1
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getWorkingDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 859
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 861
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 p1, 0x1

    return p1

    :cond_3
    return v1
.end method

.method public static synthetic lambda$onCreate$0(Lepson/print/ActivityViewImageSelect;Ljava/util/Deque;)V
    .locals 2

    .line 295
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 297
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 298
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 301
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 302
    invoke-direct {p0, v0}, Lepson/print/ActivityViewImageSelect;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 305
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 306
    invoke-direct {p0, v0}, Lepson/print/ActivityViewImageSelect;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private loadImage()V
    .locals 3

    .line 873
    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mFileCheckEnd:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mOnSizeChangeReceived:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mLicenseCheckDone:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 878
    :cond_0
    new-instance v0, Lepson/print/GalleryAdapter;

    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v2}, Lepson/print/phlayout/PhotoPreviewImageList;->getGalleryFileList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lepson/print/GalleryAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->galleryAdapter:Lepson/print/GalleryAdapter;

    .line 879
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->galleryAdapter:Lepson/print/GalleryAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 880
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    iget v1, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setSelection(I)V

    .line 881
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->galleryAdapter:Lepson/print/GalleryAdapter;

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    invoke-virtual {v0, v1}, Lepson/print/GalleryAdapter;->setAlignLeft(Landroid/view/View;)V

    .line 884
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private localUnbindService()V
    .locals 2
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .line 1476
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 1478
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I

    .line 1480
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mEpsonService:Lepson/print/service/IEpsonService;

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 1481
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private observeImageList(Lepson/print/phlayout/PhotoPreviewImageList;)V
    .locals 2

    .line 508
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/print/PhotoImageConvertViewModel;

    .line 509
    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/print/PhotoImageConvertViewModel;

    .line 511
    invoke-virtual {p1}, Lepson/print/PhotoImageConvertViewModel;->getData()Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityViewImageSelect$3;

    invoke-direct {v1, p0}, Lepson/print/ActivityViewImageSelect$3;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-virtual {v0, p0, v1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 529
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Lepson/print/PhotoImageConvertViewModel;->setOriginalImageList(Ljava/util/ArrayList;)V

    return-void
.end method

.method private onPrintSettingEnd(I)V
    .locals 14

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto/16 :goto_3

    .line 1267
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    const-string p1, "PrintSetting"

    const/4 v2, 0x0

    .line 1270
    invoke-virtual {p0, p1, v2}, Lepson/print/ActivityViewImageSelect;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const v3, 0x7f0e04d6

    .line 1271
    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "PRINTER_ID"

    const/4 v6, 0x0

    .line 1272
    invoke-interface {p1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lepson/print/ActivityViewImageSelect;->printerId:Ljava/lang/String;

    const/4 v5, 0x2

    if-eqz p1, :cond_2

    const-string v4, "PRINTER_NAME"

    .line 1284
    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1286
    new-instance p1, Lepson/print/screen/PrintSetting;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {p1, p0, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1287
    invoke-virtual {p1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 1289
    iget v3, p1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 1290
    iget v5, p1, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 1291
    iget v6, p1, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    .line 1292
    iget v7, p1, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 1293
    iget v8, p1, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    .line 1294
    iget v9, p1, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    .line 1296
    iget v10, p1, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 1297
    sget-object v11, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v11}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v11

    .line 1298
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "KIND = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lepson/print/screen/PrintSetting;->getKind()Lepson/print/screen/PrintSetting$Kind;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v13, " NEW   setteing = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v13, " plain = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-ne v10, v11, :cond_1

    .line 1299
    invoke-virtual {p1}, Lepson/print/screen/PrintSetting;->getKind()Lepson/print/screen/PrintSetting$Kind;

    move-result-object p1

    sget-object v10, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    if-ne p1, v10, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1303
    :goto_0
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v10

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 1307
    :goto_1
    iget-object v11, p0, Lepson/print/ActivityViewImageSelect;->currentPrinterName:Ljava/lang/String;

    invoke-virtual {v11, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 1309
    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->currentPrinterName:Ljava/lang/String;

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    .line 1315
    :goto_2
    iget v11, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    if-eq v11, v5, :cond_4

    .line 1316
    iput v5, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    .line 1317
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget v5, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    iget v11, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    invoke-virtual {v4, v5, v11}, Lcom/epson/iprint/apf/ApfPreviewView;->setLayout(II)V

    const/4 v4, 0x1

    .line 1322
    :cond_4
    iget v5, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    if-eq v5, v6, :cond_5

    .line 1323
    iput v6, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    .line 1324
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget v5, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    iget v6, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    invoke-virtual {v4, v5, v6}, Lcom/epson/iprint/apf/ApfPreviewView;->setLayout(II)V

    const/4 v4, 0x1

    .line 1330
    :cond_5
    iget v5, p0, Lepson/print/ActivityViewImageSelect;->currentPaperSize:I

    if-eq v5, v3, :cond_6

    .line 1331
    iput v3, p0, Lepson/print/ActivityViewImageSelect;->currentPaperSize:I

    .line 1332
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v4, v3}, Lcom/epson/iprint/apf/ApfPreviewView;->setPaper(I)Z

    .line 1333
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->papersizeTextView:Landroid/widget/TextView;

    new-instance v5, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    .line 1334
    invoke-virtual {v5, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;->getStringId(I)I

    move-result v3

    .line 1333
    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x1

    .line 1339
    :cond_6
    iget v3, p0, Lepson/print/ActivityViewImageSelect;->currentColor:I

    if-eq v3, v7, :cond_7

    .line 1340
    iput v7, p0, Lepson/print/ActivityViewImageSelect;->currentColor:I

    .line 1341
    iget-object v3, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget v4, p0, Lepson/print/ActivityViewImageSelect;->currentColor:I

    invoke-virtual {v3, v4}, Lcom/epson/iprint/apf/ApfPreviewView;->setColor(I)V

    const/4 v4, 0x1

    .line 1346
    :cond_7
    iget-object v3, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v3}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v3

    if-nez v3, :cond_8

    .line 1347
    iget-object v3, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1351
    :cond_8
    iget-boolean v2, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    if-eq v2, v10, :cond_9

    .line 1352
    iput-boolean v10, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    const/4 v4, 0x1

    :cond_9
    if-nez v10, :cond_c

    .line 1357
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v2}, Lcom/epson/iprint/apf/ApfPreviewView;->getApfMode()I

    move-result v2

    if-eq v2, v8, :cond_a

    const/4 v4, 0x1

    .line 1360
    :cond_a
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v2}, Lcom/epson/iprint/apf/ApfPreviewView;->getSharpnessValue()I

    move-result v2

    if-eq v2, v9, :cond_b

    const/4 v4, 0x1

    .line 1363
    :cond_b
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v2}, Lcom/epson/iprint/apf/ApfPreviewView;->getClearlyVividMode()I

    move-result v2

    if-eq v2, p1, :cond_c

    const/4 v4, 0x1

    .line 1368
    :cond_c
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->updateEpsonColorStatus()V

    if-eqz v4, :cond_d

    .line 1373
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_d
    :goto_3
    return-void
.end method

.method private permissionCheck()Z
    .locals 1

    .line 1155
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/common/IprintLicenseInfo;->isAgreedCurrentVersion(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    .line 1159
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mLicenseCheckDone:Z

    .line 1160
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->requestPermissionOrObserveImageList()V

    return v0
.end method

.method private requestExternalStoragePermissionIfNeeded()Z
    .locals 1

    .line 834
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lepson/print/ActivityViewImageSelect;->isPermissionRequestRequired(Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 835
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->requestReadExternalStoragePermission()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private requestPermissionOrObserveImageList()V
    .locals 1

    .line 824
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->requestExternalStoragePermissionIfNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 827
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-direct {p0, v0}, Lepson/print/ActivityViewImageSelect;->observeImageList(Lepson/print/phlayout/PhotoPreviewImageList;)V

    return-void
.end method

.method private requestReadExternalStoragePermission()V
    .locals 7

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 552
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    .line 553
    new-array v2, v1, [Ljava/lang/String;

    const v3, 0x7f0e0422

    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v2, v5

    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 554
    new-array v1, v1, [Ljava/lang/String;

    const v3, 0x7f0e0420

    .line 555
    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    .line 556
    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v6, 0x7f0e0424

    invoke-virtual {p0, v6}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v3, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    .line 558
    new-instance v3, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v0, v0, v5

    invoke-direct {v3, v0, v2, v1}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    const/16 v0, 0x9

    .line 559
    invoke-static {p0, v3, v0}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    .line 560
    iput-boolean v4, p0, Lepson/print/ActivityViewImageSelect;->bRequestPermission:Z

    return-void
.end method

.method private setFieldFromStartIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 607
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->isCustomAction:Z

    .line 609
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 610
    iput v0, p0, Lepson/print/ActivityViewImageSelect;->mIntentFileType:I

    const/4 v1, 0x1

    .line 611
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    :try_start_0
    const-string v2, "print_log"

    .line 614
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/epson/iprint/prtlogger/PrintLog;

    iput-object v2, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string v2, "com.epson.iprint.photo"

    .line 619
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/16 v3, 0x1e

    if-eqz v2, :cond_5

    .line 622
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->isCustomAction:Z

    const-string v2, "extParam"

    .line 624
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/shared/SharedParamPhoto;

    if-nez p1, :cond_0

    return-void

    .line 629
    :cond_0
    invoke-virtual {p1}, Lcom/epson/iprint/shared/SharedParamPhoto;->getArrayFileFullPath()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    return-void

    :cond_1
    const/4 v4, 0x0

    .line 635
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_3

    if-ge v4, v3, :cond_2

    .line 637
    iget-object v5, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 645
    :cond_3
    invoke-virtual {p1}, Lcom/epson/iprint/shared/SharedParamPhoto;->isFileTypeValid()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 646
    invoke-virtual {p1}, Lcom/epson/iprint/shared/SharedParamPhoto;->getFile_type()I

    move-result p1

    iput p1, p0, Lepson/print/ActivityViewImageSelect;->mIntentFileType:I

    .line 649
    :cond_4
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    goto/16 :goto_5

    :cond_5
    const-string v2, "listAlbum"

    .line 653
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_6

    const-string v2, "imageList"

    .line 654
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_6

    const-string v2, "from"

    const/4 v4, -0x1

    .line 664
    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-ne v4, v2, :cond_6

    .line 666
    new-instance v2, Lepson/print/screen/PrintSetting;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v4}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    invoke-virtual {v2}, Lepson/print/screen/PrintSetting;->resetPrintSettings()V

    :cond_6
    const-string v2, "listAlbum"

    .line 670
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_a

    const-string v2, "typeprint"

    .line 673
    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 674
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    const-string v1, "typeprint"

    .line 675
    invoke-virtual {p1, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_7
    const-string v1, "listAlbum"

    .line 677
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_8

    return-void

    .line 681
    :cond_8
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_9

    .line 682
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_9
    const-string v0, "listAlbum"

    .line 684
    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_a
    const-string v2, "FROM_EPSON"

    .line 686
    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 687
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    const-string v1, "android.intent.extra.STREAM"

    .line 689
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    if-eqz v1, :cond_c

    .line 692
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_b

    return-void

    .line 696
    :cond_b
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_c
    const-string v1, "android.intent.extra.STREAM"

    .line 701
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    if-nez p1, :cond_d

    return-void

    .line 707
    :cond_d
    :goto_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_16

    .line 708
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_e
    const-string v2, "android.intent.action.SEND"

    .line 712
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 714
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    .line 715
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->setPrintLogForExternalApp()V

    :try_start_1
    const-string v0, "android.intent.extra.STREAM"

    .line 718
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-nez v0, :cond_f

    return-void

    .line 722
    :cond_f
    invoke-direct {p0, p1, v0}, Lepson/print/ActivityViewImageSelect;->addImageByIntent(Landroid/content/Intent;Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_5

    :catch_1
    return-void

    :cond_10
    const-string v2, "android.intent.action.VIEW"

    .line 730
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 732
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    .line 733
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->setPrintLogForExternalApp()V

    .line 735
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 737
    :try_start_2
    invoke-direct {p0, p1, v0}, Lepson/print/ActivityViewImageSelect;->addImageByIntent(Landroid/content/Intent;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_5

    :catch_2
    nop

    goto :goto_5

    :cond_11
    const-string v2, "android.intent.action.SEND_MULTIPLE"

    .line 742
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 744
    iput v1, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    .line 745
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->setPrintLogForExternalApp()V

    .line 747
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_12

    return-void

    :cond_12
    const-string v2, "android.intent.extra.STREAM"

    .line 751
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_13

    :try_start_3
    const-string v1, "android.intent.extra.STREAM"

    .line 754
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    if-eqz v1, :cond_16

    .line 760
    :goto_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_16

    if-ge v0, v3, :cond_16

    .line 762
    :try_start_4
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-direct {p0, p1, v2}, Lepson/print/ActivityViewImageSelect;->addImageByIntent(Landroid/content/Intent;Landroid/net/Uri;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :catch_3
    return-void

    :catch_4
    return-void

    .line 771
    :cond_13
    :try_start_5
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 772
    invoke-direct {p0, p1, v0}, Lepson/print/ActivityViewImageSelect;->addImageByIntent(Landroid/content/Intent;Landroid/net/Uri;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_5

    :catch_5
    return-void

    .line 786
    :cond_14
    iput v0, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    const-string v1, "imageList"

    .line 787
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    if-nez p1, :cond_15

    return-void

    .line 792
    :cond_15
    :goto_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_16

    .line 793
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 798
    :cond_16
    :goto_5
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez p1, :cond_17

    .line 800
    new-instance p1, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {p1}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object p1, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    :cond_17
    return-void
.end method

.method private setPhotoImageList(Ljava/util/ArrayList;)Z
    .locals 3
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;",
            ">;)Z"
        }
    .end annotation

    .line 568
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->clear()V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 573
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    .line 574
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v2, v1}, Lepson/print/phlayout/PhotoPreviewImageList;->add(Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;)V

    goto :goto_0

    .line 578
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {p1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result p1

    if-gtz p1, :cond_2

    return v0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method private setPrintLogForExternalApp()V
    .locals 2

    .line 808
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez v0, :cond_0

    .line 809
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    .line 812
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    iget-object v0, v0, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 813
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    .line 816
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    iget v0, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    if-gtz v0, :cond_2

    .line 819
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    const/16 v1, 0x1001

    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    :cond_2
    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 3

    .line 2098
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x7fd36f82

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    const v1, -0x14b98bc

    if-eq v0, v1, :cond_1

    const v1, 0x1668d7d

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_load"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v0, "dialog_all_update"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, -0x1

    :goto_1
    const v1, 0x7f0e04d9

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_2

    .line 2107
    :pswitch_0
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    .line 2104
    :pswitch_1
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    .line 2100
    :pswitch_2
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_4

    .line 2111
    invoke-virtual {v0, v2}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 2112
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showSimpleDialog(I)V
    .locals 2

    .line 1687
    invoke-static {p1}, Lepson/print/SimpleMessageDialogFragment;->newInstance(I)Lepson/print/SimpleMessageDialogFragment;

    move-result-object p1

    .line 1688
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "simple-dialog"

    invoke-virtual {p1, v0, v1}, Lepson/print/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showStoreDialog()V
    .locals 3

    .line 491
    invoke-static {}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->newInstance()Lcom/epson/mobilephone/common/ReviewInvitationDialog;

    move-result-object v0

    .line 492
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "store-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startLicenseCheckActivity()V
    .locals 3

    .line 594
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/common/IprintLicenseInfo;->beforeLicenseCheck(Landroid/content/Context;)V

    .line 595
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lepson/common/IprintLicenseInfo;

    invoke-direct {v1}, Lepson/common/IprintLicenseInfo;-><init>()V

    new-instance v2, Lepson/common/IprintUserSurveyInfo;

    invoke-direct {v2}, Lepson/common/IprintUserSurveyInfo;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;Lcom/epson/mobilephone/common/license/UserSurveyInfo;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xa

    .line 597
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityViewImageSelect;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startPrint()V
    .locals 5

    .line 1696
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1697
    invoke-static {p0}, Lepson/common/OsAssistant;->lockScreenRotation(Landroid/app/Activity;)V

    .line 1699
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 1701
    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const-string v0, "PrintSetting"

    .line 1703
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityViewImageSelect;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1704
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "SOURCE_TYPE"

    const/4 v4, 0x2

    .line 1705
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1706
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1709
    :cond_0
    new-instance v0, Lepson/print/screen/PrintSetting;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, p0, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1710
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 1711
    iget-object v3, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v3}, Lcom/epson/iprint/apf/ApfPreviewView;->getImageList()Lepson/print/EPImageList;

    move-result-object v3

    .line 1712
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getApfValue(Lepson/print/screen/PrintSetting;)I

    move-result v4

    iput v4, v3, Lepson/print/EPImageList;->apfModeInPrinting:I

    .line 1713
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getSharpnessValue(Lepson/print/screen/PrintSetting;)I

    move-result v4

    invoke-virtual {v3, v4}, Lepson/print/EPImageList;->setSharpness(I)V

    .line 1714
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getClearlyVividValue(Lepson/print/screen/PrintSetting;)I

    move-result v0

    invoke-virtual {v3, v0}, Lepson/print/EPImageList;->setClearlyVivid(I)V

    .line 1716
    invoke-virtual {v3, v2}, Lepson/print/EPImageList;->setMultifileMode(Z)V

    .line 1718
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez v0, :cond_1

    .line 1720
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    .line 1722
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    iput v2, v0, Lcom/epson/iprint/prtlogger/PrintLog;->previewType:I

    .line 1723
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->isEpsonColorMode()Z

    move-result v0

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-static {p0, v3, v1, v0, v2}, Lepson/print/screen/PrintProgress;->getPrintIntent(Landroid/content/Context;Lepson/print/EPImageList;ZZLcom/epson/iprint/prtlogger/PrintLog;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xff

    .line 1726
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityViewImageSelect;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private unlockScreenRotation()V
    .locals 1

    const/4 v0, -0x1

    .line 1692
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->setRequestedOrientation(I)V

    return-void
.end method

.method private updateEpsonColorStatus()V
    .locals 2

    .line 1402
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->isEpsonColorMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    .line 1409
    :goto_0
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->mEpsonColorImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private updateGallery()V
    .locals 2

    .line 533
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->galleryAdapter:Lepson/print/GalleryAdapter;

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->getGalleryFileList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/GalleryAdapter;->setImageList(Ljava/util/ArrayList;)V

    return-void
.end method

.method private updateImageListDisplay()V
    .locals 3

    .line 586
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0, v1}, Lcom/epson/iprint/apf/ApfPreviewView;->setImageList(Lepson/print/phlayout/PhotoPreviewImageList;)V

    .line 587
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->pageTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "1/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v2}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 589
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->deleteButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected adjustImageNo()V
    .locals 2

    .line 1865
    iget v0, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1866
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    .line 1868
    :cond_0
    iget v0, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    if-gez v0, :cond_1

    const/4 v0, 0x0

    .line 1870
    iput v0, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    :cond_1
    return-void
.end method

.method public callPrintSetting()V
    .locals 3

    .line 1964
    invoke-static {}, Ljava/lang/System;->gc()V

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 1966
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityViewImageSelect;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1967
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SOURCE_TYPE"

    const/4 v2, 0x2

    .line 1968
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1969
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1971
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/screen/SettingScr;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xfe

    .line 1972
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityViewImageSelect;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method check3GAndStartPrint()V
    .locals 2

    .line 1541
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected executeAllReloadTaskAgain()Z
    .locals 2

    .line 2063
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2064
    :try_start_0
    iget-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->mIsAllReloadTaskRetryAcceptable:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 2065
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskRetryRequested:Z

    .line 2067
    monitor-exit v0

    return v1

    :cond_0
    const/4 v1, 0x0

    .line 2069
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 2070
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected getApfValue(Lepson/print/screen/PrintSetting;)I
    .locals 1

    .line 2004
    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    if-nez v0, :cond_0

    .line 2005
    iget p1, p1, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected getClearlyVividValue(Lepson/print/screen/PrintSetting;)I
    .locals 4

    .line 2020
    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    if-nez v0, :cond_0

    .line 2021
    iget v0, p1, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 2022
    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v1

    .line 2023
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setteing = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "plain = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-ne v0, v1, :cond_0

    .line 2024
    invoke-virtual {p1}, Lepson/print/screen/PrintSetting;->getKind()Lepson/print/screen/PrintSetting$Kind;

    move-result-object p1

    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected getEpsonColorModeFromIntent(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "epson_color_mode"

    const/4 v1, 0x0

    .line 1133
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;
    .locals 7

    const-string v0, "_data"

    .line 1082
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v3

    .line 1084
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    const-string v1, "_data"

    .line 1095
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 1096
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1097
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 1101
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1104
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0

    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    return-object v0
.end method

.method protected getSharpnessValue(Lepson/print/screen/PrintSetting;)I
    .locals 1

    .line 2013
    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    if-nez v0, :cond_0

    .line 2014
    iget p1, p1, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public invitationDialogClicked(Z)V
    .locals 0

    .line 2054
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->setStartStoreEnd()V

    return-void
.end method

.method protected lastCheckAllReloadTaskRetryRequest()Z
    .locals 2

    .line 2083
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2084
    :try_start_0
    iget-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->mAllReloadTaskRetryRequested:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 2085
    monitor-exit v0

    return v1

    :cond_0
    const/4 v1, 0x0

    .line 2087
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->mIsAllReloadTaskRetryAcceptable:Z

    .line 2089
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 2090
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 1168
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x0

    const/4 v0, 0x1

    const/4 v1, -0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_0

    .line 1172
    :sswitch_0
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->unlockScreenRotation()V

    if-eqz p2, :cond_1

    const/4 p1, 0x4

    if-eq p2, p1, :cond_2

    const/16 p1, 0x3e8

    if-eq p2, p1, :cond_0

    goto/16 :goto_0

    .line 1201
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1202
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    goto/16 :goto_0

    .line 1176
    :cond_1
    invoke-virtual {p0, p2}, Lepson/print/ActivityViewImageSelect;->setResult(I)V

    .line 1177
    iget-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->isCustomAction:Z

    if-eqz p1, :cond_2

    .line 1178
    invoke-virtual {p0, p3}, Lepson/print/ActivityViewImageSelect;->setResult(I)V

    .line 1179
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->finish()V

    return-void

    .line 1185
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-static {p2}, Lepson/print/screen/PrintProgress;->isPrintSuccess(I)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->onPrintEnd(Z)V

    .line 1186
    iget-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->isCustomAction:Z

    if-eqz p1, :cond_3

    .line 1187
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->setResult(I)V

    .line 1188
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->finish()V

    return-void

    .line 1192
    :cond_3
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1193
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    goto :goto_0

    .line 1208
    :sswitch_1
    invoke-direct {p0, p2}, Lepson/print/ActivityViewImageSelect;->onPrintSettingEnd(I)V

    goto :goto_0

    .line 1252
    :sswitch_2
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->permissionCheck()Z

    move-result p1

    if-nez p1, :cond_6

    .line 1253
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string p2, "dialog_load"

    invoke-virtual {p1, p2}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    .line 1254
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->finish()V

    return-void

    :sswitch_3
    if-eq p2, v1, :cond_4

    .line 1246
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->clearListToastAndFinish()V

    goto :goto_0

    .line 1243
    :cond_4
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->requestPermissionOrObserveImageList()V

    goto :goto_0

    :sswitch_4
    if-ne p2, v1, :cond_6

    const-string p1, "PrintSetting"

    .line 1215
    invoke-virtual {p0, p1, p3}, Lepson/print/ActivityViewImageSelect;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string p2, "PRINTER_NAME"

    const p3, 0x7f0e04d6

    .line 1216
    invoke-virtual {p0, p3}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1217
    iget-object p3, p0, Lepson/print/ActivityViewImageSelect;->currentPrinterName:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_5

    .line 1219
    iput-object p2, p0, Lepson/print/ActivityViewImageSelect;->currentPrinterName:Ljava/lang/String;

    const-string p2, "PRINTER_ID"

    const/4 p3, 0x0

    .line 1220
    invoke-interface {p1, p2, p3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/ActivityViewImageSelect;->printerId:Ljava/lang/String;

    .line 1222
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->updateEpsonColorStatus()V

    .line 1227
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->bAutoStartPrint:Z

    .line 1230
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1233
    :cond_5
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect;->onClick(Landroid/view/View;)V

    const-string p1, "ActivityViewImageSelect"

    const-string p2, "onClick(printButton)"

    .line 1234
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_4
        0x9 -> :sswitch_3
        0xa -> :sswitch_2
        0xfe -> :sswitch_1
        0xff -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .line 1517
    iget v0, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    if-nez v0, :cond_0

    .line 1518
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1519
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->getArgumentFileList()Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "imageList"

    .line 1520
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/16 v1, 0x12

    .line 1521
    invoke-virtual {p0, v1, v0}, Lepson/print/ActivityViewImageSelect;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1525
    :cond_0
    iget-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->isPrintedOK:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, -0x1

    .line 1526
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->setResult(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 1528
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->setResult(I)V

    .line 1532
    :goto_0
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->finish()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 1738
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0e04f2

    const/4 v1, 0x0

    const/4 v2, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_1

    .line 1784
    :sswitch_0
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    .line 1786
    iget-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    if-ne p1, v2, :cond_0

    iget-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->enableShowWarning:Z

    if-ne p1, v2, :cond_0

    .line 1787
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->context:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1788
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0e0362

    .line 1789
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x7f0e0363

    .line 1790
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1789
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e052b

    .line 1791
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityViewImageSelect$7;

    invoke-direct {v1, p0}, Lepson/print/ActivityViewImageSelect$7;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04e6

    .line 1797
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityViewImageSelect$6;

    invoke-direct {v1, p0}, Lepson/print/ActivityViewImageSelect$6;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1802
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 1803
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->currentPrinterName:Ljava/lang/String;

    const v2, 0x7f0e04d6

    invoke-virtual {p0, v2}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1805
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->context:Landroid/content/Context;

    invoke-direct {p1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1806
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v1, 0x7f0e0439

    .line 1807
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v1, 0x7f0e043b

    .line 1808
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1809
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityViewImageSelect$8;

    invoke-direct {v1, p0}, Lepson/print/ActivityViewImageSelect$8;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1814
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 1816
    :cond_1
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->check3GAndStartPrint()V

    goto/16 :goto_1

    .line 1842
    :sswitch_1
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->aPaperSourceSetting:Ljava/util/ArrayList;

    if-eqz p1, :cond_2

    .line 1844
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 1845
    const-class v0, Lepson/print/screen/PaperSourceSettingScr;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v0, "PAPERSOURCEINFO"

    .line 1846
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->aPaperSourceSetting:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "print-setting-type"

    .line 1847
    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    .line 1848
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v1

    .line 1847
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xfe

    .line 1849
    invoke-virtual {p0, p1, v0}, Lepson/print/ActivityViewImageSelect;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 1854
    :cond_2
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->callPrintSetting()V

    goto :goto_1

    .line 1757
    :sswitch_2
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->removeCurrentImage()V

    goto :goto_1

    .line 1740
    :sswitch_3
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {p1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result p1

    const/16 v1, 0x1e

    if-ne p1, v1, :cond_3

    .line 1741
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e03a7

    .line 1742
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1743
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityViewImageSelect$5;

    invoke-direct {v1, p0}, Lepson/print/ActivityViewImageSelect$5;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1748
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 1750
    :cond_3
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->addPhotos()V

    .line 1751
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->finish()V

    return-void

    .line 1823
    :sswitch_4
    iget-boolean p1, p0, Lepson/print/ActivityViewImageSelect;->isPaperLandscape:Z

    if-ne p1, v2, :cond_4

    .line 1824
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->isPaperLandscape:Z

    goto :goto_0

    .line 1826
    :cond_4
    iput-boolean v2, p0, Lepson/print/ActivityViewImageSelect;->isPaperLandscape:Z

    .line 1830
    :goto_0
    iget p1, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    const/4 v0, 0x4

    if-eq p1, v0, :cond_5

    .line 1833
    new-instance p1, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;-><init>(Lepson/print/ActivityViewImageSelect;Lepson/print/ActivityViewImageSelect$1;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p1, v0, v1}, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_5
    return-void

    :goto_1
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f08008a -> :sswitch_4
        0x7f080093 -> :sswitch_3
        0x7f080095 -> :sswitch_2
        0x7f08009a -> :sswitch_1
        0x7f08009d -> :sswitch_0
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 1997
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 270
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a00a9

    .line 271
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->setContentView(I)V

    .line 272
    iput-object p0, p0, Lepson/print/ActivityViewImageSelect;->context:Landroid/content/Context;

    .line 274
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    .line 275
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->getShowInvitationLiveData()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityViewImageSelect$1;

    invoke-direct {v1, p0}, Lepson/print/ActivityViewImageSelect$1;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    const/4 v0, 0x0

    .line 284
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mOnSizeChangeReceived:Z

    .line 285
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mLicenseCheckDone:Z

    .line 286
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mFileCheckEnd:Z

    .line 289
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v1

    const-class v2, Lepson/common/DialogProgressViewModel;

    invoke-virtual {v1, v2}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v1

    check-cast v1, Lepson/common/DialogProgressViewModel;

    iput-object v1, p0, Lepson/print/ActivityViewImageSelect;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 290
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {v1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object v1

    new-instance v2, Lepson/print/-$$Lambda$ActivityViewImageSelect$ayVI9XEx_SvpZx3_dyAU1_mdLfU;

    invoke-direct {v2, p0}, Lepson/print/-$$Lambda$ActivityViewImageSelect$ayVI9XEx_SvpZx3_dyAU1_mdLfU;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-virtual {v1, p0, v2}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 311
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->stopLoggerIfNotAgreed(Landroid/content/Context;)V

    const v1, 0x7f0e0429

    const/4 v2, 0x1

    .line 314
    invoke-virtual {p0, v1, v2}, Lepson/print/ActivityViewImageSelect;->setActionBar(IZ)V

    .line 317
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 318
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->initTempCRDir()V

    .line 319
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 322
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_0

    const v4, 0x7f0e03f3

    .line 325
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 326
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->finish()V

    .line 330
    :cond_0
    invoke-direct {p0, v3}, Lepson/print/ActivityViewImageSelect;->setFieldFromStartIntent(Landroid/content/Intent;)V

    .line 332
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 333
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->clearListToastAndFinish()V

    return-void

    .line 337
    :cond_1
    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->getEpsonColorModeFromIntent(Landroid/content/Intent;)Z

    move-result v4

    iput-boolean v4, p0, Lepson/print/ActivityViewImageSelect;->mEnableEpsonColorDisplay:Z

    const v4, 0x7f080264

    .line 342
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lepson/print/phlayout/PhotoPreview;

    .line 343
    invoke-virtual {v4, p0}, Lepson/print/phlayout/PhotoPreview;->setViewSizeChangeListener(Lepson/print/phlayout/PhotoPreview$ViewSizeChangeListener;)V

    .line 345
    new-instance v5, Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-direct {v5}, Lcom/epson/iprint/apf/ApfPreviewView;-><init>()V

    iput-object v5, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    .line 346
    iget-object v5, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v5, p0, v4}, Lcom/epson/iprint/apf/ApfPreviewView;->init(Landroid/content/Context;Lepson/print/phlayout/PhotoPreview;)V

    const-string v4, "PrintSetting"

    .line 350
    invoke-virtual {p0, v4, v0}, Lepson/print/ActivityViewImageSelect;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v5, "PRINTER_NAME"

    const v6, 0x7f0e04d6

    .line 352
    invoke-virtual {p0, v6}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lepson/print/ActivityViewImageSelect;->currentPrinterName:Ljava/lang/String;

    const-string v5, "PRINTER_ID"

    const/4 v6, 0x0

    .line 353
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->printerId:Ljava/lang/String;

    .line 355
    new-instance v4, Lepson/print/screen/PrintSetting;

    sget-object v5, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v4, p0, v5}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 356
    invoke-virtual {v4}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 357
    iget v5, v4, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    iput v5, p0, Lepson/print/ActivityViewImageSelect;->currentPaperSize:I

    .line 358
    iget v5, v4, Lepson/print/screen/PrintSetting;->layoutValue:I

    iput v5, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    .line 359
    iget v5, v4, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    iput v5, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    .line 360
    iget v4, v4, Lepson/print/screen/PrintSetting;->colorValue:I

    iput v4, p0, Lepson/print/ActivityViewImageSelect;->currentColor:I

    .line 363
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget v5, p0, Lepson/print/ActivityViewImageSelect;->currentLayout:I

    iget v6, p0, Lepson/print/ActivityViewImageSelect;->currentLayoutMulti:I

    invoke-virtual {v4, v5, v6}, Lcom/epson/iprint/apf/ApfPreviewView;->setLayout(II)V

    .line 364
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget v5, p0, Lepson/print/ActivityViewImageSelect;->currentPaperSize:I

    invoke-virtual {v4, v5}, Lcom/epson/iprint/apf/ApfPreviewView;->setPaper(I)Z

    .line 365
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget v5, p0, Lepson/print/ActivityViewImageSelect;->currentColor:I

    invoke-virtual {v4, v5}, Lcom/epson/iprint/apf/ApfPreviewView;->setColor(I)V

    .line 368
    :cond_2
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v4, v0, v0, v0}, Lcom/epson/iprint/apf/ApfPreviewView;->setApfLibParams(III)V

    const v4, 0x7f08009a

    .line 371
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->papersizeTextView:Landroid/widget/TextView;

    .line 372
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->papersizeTextView:Landroid/widget/TextView;

    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->getPaperSizeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->papersizeTextView:Landroid/widget/TextView;

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f08009b

    .line 376
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->pageTextView:Landroid/widget/TextView;

    .line 380
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    if-eqz v4, :cond_3

    const/4 v5, 0x2

    .line 381
    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    const v4, 0x7f08008a

    .line 385
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->rotateButton:Landroid/widget/Button;

    .line 386
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->rotateButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f080160

    .line 389
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Gallery;

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    .line 391
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_4

    .line 392
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    invoke-virtual {v4, v0}, Landroid/widget/Gallery;->setCallbackDuringFling(Z)V

    .line 394
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    new-instance v5, Lepson/print/ActivityViewImageSelect$2;

    invoke-direct {v5, p0}, Lepson/print/ActivityViewImageSelect$2;-><init>(Lepson/print/ActivityViewImageSelect;)V

    invoke-virtual {v4, v5}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_4
    const v4, 0x7f080093

    .line 415
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->addButton:Landroid/widget/ImageButton;

    .line 416
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->addButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f080095

    .line 419
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->deleteButton:Landroid/widget/Button;

    .line 420
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->deleteButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 421
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->mImageFilenameListFromIntent:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_5

    .line 422
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->deleteButton:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 426
    :cond_5
    iget v4, p0, Lepson/print/ActivityViewImageSelect;->mStartActivityFromPhoto:I

    const/16 v5, 0x8

    if-eqz v4, :cond_6

    .line 427
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->addButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 428
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->deleteButton:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_6
    const v4, 0x7f08009d

    .line 432
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    .line 433
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 436
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v6, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Lcom/epson/iprint/apf/ApfPreviewView;->setZoomControlHandler(Landroid/os/Handler;)V

    const v4, 0x7f08018d

    .line 439
    invoke-virtual {p0, v4}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lepson/print/ActivityViewImageSelect;->paperMissmath:Landroid/widget/ImageView;

    .line 442
    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v6, "dialog_load"

    invoke-virtual {v4, v6}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    const-string v4, "PREFS_EPSON_CONNECT"

    .line 445
    invoke-virtual {p0, v4, v0}, Lepson/print/ActivityViewImageSelect;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v6, "ENABLE_SHOW_WARNING"

    .line 446
    invoke-interface {v4, v6, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lepson/print/ActivityViewImageSelect;->enableShowWarning:Z

    .line 449
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v4

    iput-boolean v4, p0, Lepson/print/ActivityViewImageSelect;->isRemotePrinter:Z

    const-string v4, "from"

    .line 451
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const v3, 0x7f0801c6

    packed-switch v0, :pswitch_data_0

    .line 465
    invoke-virtual {p0, v1}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0e03d9

    .line 454
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->setTitle(Ljava/lang/CharSequence;)V

    .line 455
    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0e031c

    .line 459
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->setTitle(Ljava/lang/CharSequence;)V

    .line 460
    invoke-virtual {p0, v3}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    const v0, 0x7f080129

    .line 469
    invoke-virtual {p0, v0}, Lepson/print/ActivityViewImageSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->mEpsonColorImageView:Landroid/widget/ImageView;

    .line 470
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->updateEpsonColorStatus()V

    .line 473
    invoke-static {p0}, Lepson/print/screen/PaperSourceInfo;->getInstance(Landroid/content/Context;)Lepson/print/screen/PaperSourceInfo;

    move-result-object v0

    iput-object v0, p0, Lepson/print/ActivityViewImageSelect;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    .line 476
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_7

    .line 477
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/service/EpsonService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v2}, Lepson/print/ActivityViewImageSelect;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_7
    if-nez p1, :cond_8

    .line 482
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->startLicenseCheckActivity()V

    goto :goto_1

    .line 484
    :cond_8
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->permissionCheck()Z

    move-result p1

    if-nez p1, :cond_9

    .line 485
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->finish()V

    :cond_9
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 1956
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1957
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0008

    .line 1958
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "ActivityImageViewSelect"

    const-string v1, "onDestroy"

    .line 1467
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1468
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .line 1978
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    const-string p1, "key search"

    const-string p2, "diable"

    .line 1980
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 1983
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x4

    if-ne p1, p2, :cond_1

    .line 1985
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->onBackPressed()V

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .line 1429
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onNewIntent(Landroid/content/Intent;)V

    .line 1431
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1433
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->reloadTask:Lepson/print/ActivityViewImageSelect$AllReloadTask;

    if-eqz v1, :cond_0

    .line 1435
    sget-object v1, Lepson/print/ActivityViewImageSelect$11;->$SwitchMap$android$os$AsyncTask$Status:[I

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect;->reloadTask:Lepson/print/ActivityViewImageSelect$AllReloadTask;

    invoke-virtual {v2}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/AsyncTask$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    .line 1438
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1439
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTag(Landroid/content/Context;Landroid/content/Intent;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1447
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1448
    const-class v1, Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "connectInfo"

    .line 1449
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "changeMode"

    const/4 v1, 0x1

    .line 1450
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p1, 0x5

    .line 1451
    invoke-virtual {p0, v0, p1}, Lepson/print/ActivityViewImageSelect;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 1945
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080021

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 1947
    :cond_0
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->callPrintSetting()V

    .line 1951
    :goto_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "ActivityImageViewSelect"

    const-string v1, "onPause"

    .line 1492
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 1495
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    .line 1498
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    invoke-virtual {v0}, Lepson/print/screen/PaperSourceInfo;->stop()V

    .line 1500
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1501
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->localUnbindService()V

    .line 1503
    invoke-static {p0}, Lcom/epson/iprint/apf/ApfPreviewView;->deleteWorkingDirectory(Landroid/content/Context;)V

    .line 1504
    invoke-static {p0}, Lepson/print/PhotoImageConvertViewModel;->clearTempDirectory(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method protected onRestart()V
    .locals 2

    const-string v0, "ActivityImageViewSelect"

    const-string v1, "onRestart"

    .line 1510
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1511
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onRestart()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "ActivityImageViewSelect"

    const-string v1, "onResume"

    .line 1417
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x0

    .line 1420
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    .line 1423
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->paperMissmath:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1424
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1}, Lepson/print/screen/PaperSourceInfo;->start(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

.method public onViewSizeChanged()V
    .locals 2

    const-string v0, "ActivityViewImageSelect"

    const-string v1, "onViewSizeChanged()"

    .line 2039
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ActivityViewImageSelect"

    const-string v1, "onViewSizeChanged() isFinishing == true"

    .line 2043
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 2047
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect;->mOnSizeChangeReceived:Z

    .line 2049
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->loadImage()V

    return-void
.end method

.method protected removeCurrentImage()V
    .locals 2

    .line 1881
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v0}, Lcom/epson/iprint/apf/ApfPreviewView;->invalidateImageNo()V

    .line 1882
    iget v0, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    if-ltz v0, :cond_0

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1884
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    iget v1, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    invoke-virtual {v0, v1}, Lepson/print/phlayout/PhotoPreviewImageList;->remove(I)V

    .line 1885
    iget v0, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    .line 1888
    :cond_0
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect;->updateGallery()V

    .line 1890
    invoke-virtual {p0}, Lepson/print/ActivityViewImageSelect;->adjustImageNo()V

    .line 1892
    new-instance v0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;-><init>(Lepson/print/ActivityViewImageSelect;Lepson/print/ActivityViewImageSelect$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected updateScreen()V
    .locals 5

    .line 1901
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0x1e

    if-ge v0, v2, :cond_0

    .line 1905
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->addButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1906
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->addButton:Landroid/widget/ImageButton;

    const v2, 0x7f07010d

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1913
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v0

    const/4 v2, 0x0

    if-lez v0, :cond_1

    .line 1914
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->pageTextView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    add-int/2addr v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lepson/print/ActivityViewImageSelect;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v4}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1915
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->gallery:Landroid/widget/Gallery;

    iget v3, p0, Lepson/print/ActivityViewImageSelect;->imageNo:I

    invoke-virtual {v0, v3}, Landroid/widget/Gallery;->setSelection(I)V

    goto :goto_0

    .line 1918
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->deleteButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1919
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->printButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1921
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->pageTextView:Landroid/widget/TextView;

    const-string v3, "0/0"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1928
    :goto_0
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v0}, Lcom/epson/iprint/apf/ApfPreviewView;->getIsPaperLandscape()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1929
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect;->isPaperLandscape:Z

    goto :goto_1

    .line 1931
    :cond_2
    iput-boolean v2, p0, Lepson/print/ActivityViewImageSelect;->isPaperLandscape:Z

    .line 1936
    :goto_1
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    const/4 v1, 0x2

    .line 1937
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    return-void
.end method

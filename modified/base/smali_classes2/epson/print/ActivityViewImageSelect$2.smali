.class Lepson/print/ActivityViewImageSelect$2;
.super Ljava/lang/Object;
.source "ActivityViewImageSelect.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityViewImageSelect;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityViewImageSelect;


# direct methods
.method constructor <init>(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 394
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect$2;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 397
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$2;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result p1

    if-ne p1, p3, :cond_0

    return-void

    .line 402
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$2;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1, p3}, Lepson/print/ActivityViewImageSelect;->access$102(Lepson/print/ActivityViewImageSelect;I)I

    .line 403
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$2;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$200(Lepson/print/ActivityViewImageSelect;)Landroid/widget/Gallery;

    move-result-object p1

    iget-object p2, p0, Lepson/print/ActivityViewImageSelect$2;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p2}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/Gallery;->setSelection(I)V

    .line 406
    new-instance p1, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;

    iget-object p2, p0, Lepson/print/ActivityViewImageSelect$2;->this$0:Lepson/print/ActivityViewImageSelect;

    const/4 p3, 0x0

    invoke-direct {p1, p2, p3}, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;-><init>(Lepson/print/ActivityViewImageSelect;Lepson/print/ActivityViewImageSelect$1;)V

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    return-void
.end method

.class public Lepson/print/ActivityRequestPermissions$Permission;
.super Ljava/lang/Object;
.source "ActivityRequestPermissions.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityRequestPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Permission"
.end annotation


# static fields
.field static final DIALOG_MESSAGE:I = 0x2

.field static final DIALOG_TITLE:I = 0x1

.field static final PARAMETER_SIZE:I = 0x3

.field static final PERMISSION_KEY:I


# instance fields
.field private mArrayPermission:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "[[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p2    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/ActivityRequestPermissions$Permission;->mArrayPermission:Ljava/util/ArrayList;

    .line 271
    invoke-virtual {p0, p1, p2, p3}, Lepson/print/ActivityRequestPermissions$Permission;->addPermission(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addPermission(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p2    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x3

    .line 275
    new-array v0, v0, [[Ljava/lang/String;

    const/4 v1, 0x1

    .line 277
    new-array v2, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    aput-object v2, v0, v3

    aput-object p2, v0, v1

    const/4 p1, 0x2

    aput-object p3, v0, p1

    .line 281
    iget-object p1, p0, Lepson/print/ActivityRequestPermissions$Permission;->mArrayPermission:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getArrayPermission()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "[[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 285
    iget-object v0, p0, Lepson/print/ActivityRequestPermissions$Permission;->mArrayPermission:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDialogMessage(II)Ljava/lang/String;
    .locals 1

    .line 327
    :try_start_0
    iget-object v0, p0, Lepson/print/ActivityRequestPermissions$Permission;->mArrayPermission:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [[Ljava/lang/String;

    const/4 v0, 0x2

    .line 328
    aget-object p1, p1, v0

    aget-object p1, p1, p2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getDialogTitle(II)Ljava/lang/String;
    .locals 1

    .line 316
    :try_start_0
    iget-object v0, p0, Lepson/print/ActivityRequestPermissions$Permission;->mArrayPermission:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [[Ljava/lang/String;

    const/4 v0, 0x1

    .line 317
    aget-object p1, p1, v0

    aget-object p1, p1, p2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getPermissionKey(I)Ljava/lang/String;
    .locals 1

    .line 304
    :try_start_0
    iget-object v0, p0, Lepson/print/ActivityRequestPermissions$Permission;->mArrayPermission:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [[Ljava/lang/String;

    const/4 v0, 0x0

    .line 306
    aget-object p1, p1, v0

    aget-object p1, p1, v0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getPermissionKey()[Ljava/lang/String;
    .locals 4

    .line 293
    iget-object v0, p0, Lepson/print/ActivityRequestPermissions$Permission;->mArrayPermission:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 294
    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 296
    invoke-virtual {p0, v2}, Lepson/print/ActivityRequestPermissions$Permission;->getPermissionKey(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public getPermissionSize()I
    .locals 1

    .line 289
    iget-object v0, p0, Lepson/print/ActivityRequestPermissions$Permission;->mArrayPermission:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

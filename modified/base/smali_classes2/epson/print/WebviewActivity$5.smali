.class Lepson/print/WebviewActivity$5;
.super Landroid/os/Handler;
.source "WebviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/WebviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/WebviewActivity;


# direct methods
.method constructor <init>(Lepson/print/WebviewActivity;)V
    .locals 0

    .line 635
    iput-object p1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .line 637
    iget p1, p1, Landroid/os/Message;->what:I

    const v0, 0x7f0e04a0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_1

    .line 651
    :pswitch_0
    iget-object p1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1900(Lepson/print/WebviewActivity;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto/16 :goto_1

    .line 656
    :cond_0
    iget-object p1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$2000(Lepson/print/WebviewActivity;)F

    move-result p1

    .line 658
    iget-object v0, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$300(Lepson/print/WebviewActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    const/4 v2, 0x3

    if-ge v0, v1, :cond_1

    .line 659
    iget-object p1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    iget-object p1, p1, Lepson/print/WebviewActivity;->myHandler:Landroid/os/Handler;

    const-wide/16 v0, 0x7d0

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 660
    :cond_1
    iget-object v0, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$2100(Lepson/print/WebviewActivity;)I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$2100(Lepson/print/WebviewActivity;)I

    move-result v0

    iget-object v1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v1}, Lepson/print/WebviewActivity;->access$300(Lepson/print/WebviewActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, p1

    float-to-int v1, v1

    if-ge v0, v1, :cond_2

    goto :goto_0

    .line 665
    :cond_2
    iget-object p1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$2200(Lepson/print/WebviewActivity;)V

    goto :goto_1

    .line 661
    :cond_3
    :goto_0
    iget-object v0, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$300(Lepson/print/WebviewActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, p1

    float-to-int p1, v1

    invoke-static {v0, p1}, Lepson/print/WebviewActivity;->access$2102(Lepson/print/WebviewActivity;I)I

    .line 662
    iget-object p1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    iget-object p1, p1, Lepson/print/WebviewActivity;->myHandler:Landroid/os/Handler;

    const-wide/16 v0, 0x1388

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const-string p1, "WebviewActivity"

    .line 663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Waiting not change mLastHeight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v1}, Lepson/print/WebviewActivity;->access$2100(Lepson/print/WebviewActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 643
    :pswitch_1
    :try_start_0
    iget-object p1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    iget-object v1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v1, v0}, Lepson/print/WebviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    const v2, 0x7f0e04f2

    .line 644
    invoke-virtual {v1, v2}, Lepson/print/WebviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 643
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 644
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 646
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 639
    :pswitch_2
    iget-object p1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1}, Lepson/print/WebviewActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lepson/print/WebviewActivity$5;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v1, v0}, Lepson/print/WebviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class public Lepson/print/ThreeButtonDialog;
.super Landroid/support/v7/app/AppCompatDialogFragment;
.source "ThreeButtonDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/ThreeButtonDialog$DialogCallback;
    }
.end annotation


# static fields
.field private static final PARAM_LAYOUT_RESOURCE_ID:Ljava/lang/String; = "layout_resource_id"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/print/ThreeButtonDialog;I)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lepson/print/ThreeButtonDialog;->execCallbackAndDismiss(I)V

    return-void
.end method

.method private execCallbackAndDismiss(I)V
    .locals 1

    .line 124
    invoke-direct {p0}, Lepson/print/ThreeButtonDialog;->getDialogCallback()Lepson/print/ThreeButtonDialog$DialogCallback;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 129
    :cond_0
    invoke-interface {v0, p1}, Lepson/print/ThreeButtonDialog$DialogCallback;->callback(I)V

    .line 131
    invoke-virtual {p0}, Lepson/print/ThreeButtonDialog;->dismiss()V

    return-void
.end method

.method private getDialogCallback()Lepson/print/ThreeButtonDialog$DialogCallback;
    .locals 3

    .line 135
    invoke-virtual {p0}, Lepson/print/ThreeButtonDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 142
    :cond_0
    :try_start_0
    check-cast v0, Lepson/print/ThreeButtonDialog$DialogCallback;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 144
    :catch_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " must implement ThreeButtonDialog.DialogCallback"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static newInstance(I)Lepson/print/ThreeButtonDialog;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 39
    new-instance v0, Lepson/print/ThreeButtonDialog;

    invoke-direct {v0}, Lepson/print/ThreeButtonDialog;-><init>()V

    .line 40
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "layout_resource_id"

    .line 41
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 42
    invoke-virtual {v0, v1}, Lepson/print/ThreeButtonDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 50
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatDialogFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    const/4 v0, 0x0

    .line 52
    invoke-virtual {p0, p1, v0}, Lepson/print/ThreeButtonDialog;->setStyle(II)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .line 57
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/print/ThreeButtonDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {p0}, Lepson/print/ThreeButtonDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 60
    invoke-virtual {p0}, Lepson/print/ThreeButtonDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v3, "layout_resource_id"

    .line 63
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    const v1, 0x7f0a0059

    :cond_1
    const/4 v3, 0x0

    .line 70
    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 71
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0800a3

    .line 73
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 75
    new-instance v3, Lepson/print/ThreeButtonDialog$1;

    invoke-direct {v3, p0}, Lepson/print/ThreeButtonDialog$1;-><init>(Lepson/print/ThreeButtonDialog;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v1, 0x7f0800a4

    .line 83
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-eqz v1, :cond_3

    .line 85
    new-instance v3, Lepson/print/ThreeButtonDialog$2;

    invoke-direct {v3, p0}, Lepson/print/ThreeButtonDialog$2;-><init>(Lepson/print/ThreeButtonDialog;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    const v1, 0x7f0800a5

    .line 93
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_4

    .line 95
    new-instance v1, Lepson/print/ThreeButtonDialog$3;

    invoke-direct {v1, p0}, Lepson/print/ThreeButtonDialog$3;-><init>(Lepson/print/ThreeButtonDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    :cond_4
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 107
    new-instance v0, Lepson/print/ThreeButtonDialog$4;

    invoke-direct {v0, p0}, Lepson/print/ThreeButtonDialog$4;-><init>(Lepson/print/ThreeButtonDialog;)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 118
    invoke-virtual {p0, v2}, Lepson/print/ThreeButtonDialog;->setCancelable(Z)V

    return-object p1
.end method

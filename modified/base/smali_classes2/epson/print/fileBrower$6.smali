.class Lepson/print/fileBrower$6;
.super Ljava/lang/Object;
.source "fileBrower.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/fileBrower;->onContextItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/fileBrower;

.field final synthetic val$Suffix:Ljava/lang/String;

.field final synthetic val$folderCurrent:Ljava/io/File;

.field final synthetic val$input:Landroid/widget/EditText;

.field final synthetic val$oldFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lepson/print/fileBrower;Landroid/widget/EditText;Ljava/io/File;Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    .line 268
    iput-object p1, p0, Lepson/print/fileBrower$6;->this$0:Lepson/print/fileBrower;

    iput-object p2, p0, Lepson/print/fileBrower$6;->val$input:Landroid/widget/EditText;

    iput-object p3, p0, Lepson/print/fileBrower$6;->val$oldFile:Ljava/io/File;

    iput-object p4, p0, Lepson/print/fileBrower$6;->val$Suffix:Ljava/lang/String;

    iput-object p5, p0, Lepson/print/fileBrower$6;->val$folderCurrent:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .line 270
    iget-object p1, p0, Lepson/print/fileBrower$6;->val$input:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const p2, 0x7f0e04b4

    const/4 v0, 0x1

    if-eqz p1, :cond_4

    .line 271
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 273
    :try_start_0
    iget-object v1, p0, Lepson/print/fileBrower$6;->val$oldFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lepson/print/fileBrower$6;->val$Suffix:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 274
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lepson/print/fileBrower$6;->val$Suffix:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 277
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lepson/print/fileBrower$6;->val$oldFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 280
    invoke-static {p1}, Lepson/print/fileBrower;->isAvailableFileName(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 281
    iget-object p1, p0, Lepson/print/fileBrower$6;->this$0:Lepson/print/fileBrower;

    invoke-virtual {p1}, Lepson/print/fileBrower;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 287
    iget-object p1, p0, Lepson/print/fileBrower$6;->val$oldFile:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 288
    iget-object p1, p0, Lepson/print/fileBrower$6;->this$0:Lepson/print/fileBrower;

    invoke-virtual {p1}, Lepson/print/fileBrower;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 290
    :cond_2
    iget-object p1, p0, Lepson/print/fileBrower$6;->val$oldFile:Ljava/io/File;

    invoke-virtual {p1, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 292
    iget-object p1, p0, Lepson/print/fileBrower$6;->this$0:Lepson/print/fileBrower;

    iget-object p2, p0, Lepson/print/fileBrower$6;->val$folderCurrent:Ljava/io/File;

    invoke-static {p1, p2}, Lepson/print/fileBrower;->access$300(Lepson/print/fileBrower;Ljava/io/File;)V

    goto :goto_0

    .line 294
    :cond_3
    iget-object p1, p0, Lepson/print/fileBrower$6;->this$0:Lepson/print/fileBrower;

    invoke-virtual {p1}, Lepson/print/fileBrower;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 297
    :catch_0
    iget-object p1, p0, Lepson/print/fileBrower$6;->this$0:Lepson/print/fileBrower;

    invoke-virtual {p1}, Lepson/print/fileBrower;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0e036b

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 300
    :cond_4
    iget-object p1, p0, Lepson/print/fileBrower$6;->this$0:Lepson/print/fileBrower;

    invoke-virtual {p1}, Lepson/print/fileBrower;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :cond_5
    :goto_0
    return-void
.end method

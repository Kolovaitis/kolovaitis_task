.class Lepson/print/WebviewActivity$CustomProDialog;
.super Landroid/app/Dialog;
.source "WebviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/WebviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomProDialog"
.end annotation


# instance fields
.field private mLayoutId:I

.field final synthetic this$0:Lepson/print/WebviewActivity;


# direct methods
.method public constructor <init>(Lepson/print/WebviewActivity;Landroid/content/Context;II)V
    .locals 0

    .line 773
    iput-object p1, p0, Lepson/print/WebviewActivity$CustomProDialog;->this$0:Lepson/print/WebviewActivity;

    .line 774
    invoke-direct {p0, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 775
    iput p4, p0, Lepson/print/WebviewActivity$CustomProDialog;->mLayoutId:I

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .line 800
    iget-object v0, p0, Lepson/print/WebviewActivity$CustomProDialog;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v0}, Lepson/print/WebviewActivity;->stopDoBackGroundThread()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 780
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 781
    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity$CustomProDialog;->requestWindowFeature(I)Z

    .line 782
    invoke-virtual {p0}, Lepson/print/WebviewActivity$CustomProDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    iget v0, p0, Lepson/print/WebviewActivity$CustomProDialog;->mLayoutId:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    const v0, 0x7f0800cf

    .line 783
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 784
    new-instance v1, Lepson/print/WebviewActivity$CustomProDialog$1;

    invoke-direct {v1, p0}, Lepson/print/WebviewActivity$CustomProDialog$1;-><init>(Lepson/print/WebviewActivity$CustomProDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 794
    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity$CustomProDialog;->setContentView(Landroid/view/View;)V

    return-void
.end method

.class public final Lepson/print/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ALT:I = 0x7f080000

.field public static final AboutRemoteFrame:I = 0x7f080001

.field public static final AdjustmentBarSeekBar:I = 0x7f080002

.field public static final AdjustmentBarTitle:I = 0x7f080003

.field public static final AdjustmentModeSwitch:I = 0x7f080004

.field public static final AdjustmentModeSwitchLeft:I = 0x7f080005

.field public static final AdjustmentModeSwitchRight:I = 0x7f080006

.field public static final CTRL:I = 0x7f080007

.field public static final CopyMagnification:I = 0x7f080008

.field public static final DeviceGrubLinearLayout01:I = 0x7f080009

.field public static final DeviceGrubLinearLayout02:I = 0x7f08000a

.field public static final FUNCTION:I = 0x7f08000b

.field public static final ImageCollectAdjustmentBrightness:I = 0x7f08000c

.field public static final ImageCollectAdjustmentContrast:I = 0x7f08000d

.field public static final ImageCollectAdjustmentSaturation:I = 0x7f08000e

.field public static final ImageCollectLayout:I = 0x7f08000f

.field public static final ImageCollectPallet:I = 0x7f080010

.field public static final ImageCollectPalletButton:I = 0x7f080011

.field public static final ImageCollectPalletLayout:I = 0x7f080012

.field public static final ImageCollectPaperSizeInfo:I = 0x7f080013

.field public static final ImageCollectPreview:I = 0x7f080014

.field public static final ImageCollectPreviewLayout:I = 0x7f080015

.field public static final ImageCollectSwitch:I = 0x7f080016

.field public static final ImageCollectSwitchLayout:I = 0x7f080017

.field public static final ImageCollectSwitchLeft:I = 0x7f080018

.field public static final ImageCollectSwitchMiddle:I = 0x7f080019

.field public static final ImageCollectSwitchRight:I = 0x7f08001a

.field public static final ImageCollectToolbarColorAdjustment:I = 0x7f08001b

.field public static final ImageCollectToolbarCropImage:I = 0x7f08001c

.field public static final ImageCollectToolbarEnhanceText:I = 0x7f08001d

.field public static final ImageCollectView:I = 0x7f08001e

.field public static final LinearLayout01:I = 0x7f08001f

.field public static final META:I = 0x7f080020

.field public static final Menu_Show_PrintSettings:I = 0x7f080021

.field public static final RadioGroup:I = 0x7f080022

.field public static final RelativeLayout1:I = 0x7f080023

.field public static final SHIFT:I = 0x7f080024

.field public static final SYM:I = 0x7f080025

.field public static final SettingButonCopiesCountDown:I = 0x7f080026

.field public static final SettingButonCopiesCountUp:I = 0x7f080027

.field public static final SettingButonCopyMagnificationCountDown:I = 0x7f080028

.field public static final SettingButonCopyMagnificationCountUp:I = 0x7f080029

.field public static final SettingButonXDensityCountDown:I = 0x7f08002a

.field public static final SettingButonXDensityCountUp:I = 0x7f08002b

.field public static final SettingCopyMagnification:I = 0x7f08002c

.field public static final SettingEditCopies:I = 0x7f08002d

.field public static final TextView01:I = 0x7f08002e

.field public static final TextView02:I = 0x7f08002f

.field public static final TextView03:I = 0x7f080030

.field public static final TextView04:I = 0x7f080031

.field public static final TextView0xx:I = 0x7f080032

.field public static final View01:I = 0x7f080033

.field public static final View02:I = 0x7f080034

.field public static final View03:I = 0x7f080035

.field public static final access_key_edit:I = 0x7f080036

.field public static final access_key_layout:I = 0x7f080037

.field public static final access_key_text:I = 0x7f080038

.field public static final action0:I = 0x7f080039

.field public static final action_bar:I = 0x7f08003a

.field public static final action_bar_activity_content:I = 0x7f08003b

.field public static final action_bar_container:I = 0x7f08003c

.field public static final action_bar_root:I = 0x7f08003d

.field public static final action_bar_spinner:I = 0x7f08003e

.field public static final action_bar_subtitle:I = 0x7f08003f

.field public static final action_bar_title:I = 0x7f080040

.field public static final action_container:I = 0x7f080041

.field public static final action_context_bar:I = 0x7f080042

.field public static final action_divider:I = 0x7f080043

.field public static final action_go_preview:I = 0x7f080044

.field public static final action_image:I = 0x7f080045

.field public static final action_menu_divider:I = 0x7f080046

.field public static final action_menu_presenter:I = 0x7f080047

.field public static final action_mode_bar:I = 0x7f080048

.field public static final action_mode_bar_stub:I = 0x7f080049

.field public static final action_mode_close_button:I = 0x7f08004a

.field public static final action_select:I = 0x7f08004b

.field public static final action_text:I = 0x7f08004c

.field public static final action_thumbnail_type_select:I = 0x7f08004d

.field public static final action_zoom:I = 0x7f08004e

.field public static final actions:I = 0x7f08004f

.field public static final active_icon:I = 0x7f080050

.field public static final activity_chooser_view_content:I = 0x7f080051

.field public static final activity_i2_double_side_scan_setting:I = 0x7f080052

.field public static final activity_i2_scan:I = 0x7f080053

.field public static final add:I = 0x7f080054

.field public static final add_bookmark_title:I = 0x7f080055

.field public static final add_bookmark_url:I = 0x7f080056

.field public static final add_button:I = 0x7f080057

.field public static final adjust_height:I = 0x7f080058

.field public static final adjust_width:I = 0x7f080059

.field public static final agreeButton:I = 0x7f08005a

.field public static final alertTitle:I = 0x7f08005b

.field public static final alert_messeage:I = 0x7f08005c

.field public static final all:I = 0x7f08005d

.field public static final always:I = 0x7f08005e

.field public static final analytics_content:I = 0x7f08005f

.field public static final any:I = 0x7f080060

.field public static final apfSeparatorView:I = 0x7f080061

.field public static final apf_setting_imageview:I = 0x7f080062

.field public static final apf_setting_layout:I = 0x7f080063

.field public static final apf_setting_text:I = 0x7f080064

.field public static final appNameText:I = 0x7f080065

.field public static final areaView:I = 0x7f080066

.field public static final async:I = 0x7f080067

.field public static final auto:I = 0x7f080068

.field public static final autoPictureToggleButton:I = 0x7f080069

.field public static final auto_fit:I = 0x7f08006a

.field public static final auto_pict_taken_layout:I = 0x7f08006b

.field public static final auto_select_icon:I = 0x7f08006c

.field public static final back:I = 0x7f08006d

.field public static final baseDirText:I = 0x7f08006e

.field public static final battery_list:I = 0x7f08006f

.field public static final beginning:I = 0x7f080070

.field public static final ble_wifi_setting:I = 0x7f080071

.field public static final blocking:I = 0x7f080072

.field public static final body:I = 0x7f080073

.field public static final bottom:I = 0x7f080074

.field public static final bottomPanel:I = 0x7f080075

.field public static final box_account_description:I = 0x7f080076

.field public static final box_account_initials:I = 0x7f080077

.field public static final box_account_title:I = 0x7f080078

.field public static final box_avatar_image:I = 0x7f080079

.field public static final box_avatar_initials:I = 0x7f08007a

.field public static final box_net:I = 0x7f08007b

.field public static final box_net_next:I = 0x7f08007c

.field public static final boxsdk_accounts_list:I = 0x7f08007d

.field public static final brightness:I = 0x7f08007e

.field public static final brightnessSeparator:I = 0x7f08007f

.field public static final brightness_minus_button:I = 0x7f080080

.field public static final brightness_next_screen_imv:I = 0x7f080081

.field public static final brightness_plus_button:I = 0x7f080082

.field public static final browse_button:I = 0x7f080083

.field public static final browse_folder:I = 0x7f080084

.field public static final bt_BatteryStatus:I = 0x7f080085

.field public static final bt_PowerDetail:I = 0x7f080086

.field public static final bt_PowerText:I = 0x7f080087

.field public static final bt_name:I = 0x7f080088

.field public static final bt_percentage:I = 0x7f080089

.field public static final bt_rotate:I = 0x7f08008a

.field public static final bt_status_icon:I = 0x7f08008b

.field public static final btnCancelScan:I = 0x7f08008c

.field public static final btnMail:I = 0x7f08008d

.field public static final btnNext:I = 0x7f08008e

.field public static final btnPrev:I = 0x7f08008f

.field public static final btnPrint:I = 0x7f080090

.field public static final btnSave:I = 0x7f080091

.field public static final btnScan:I = 0x7f080092

.field public static final btn_add_photo:I = 0x7f080093

.field public static final btn_clear_url:I = 0x7f080094

.field public static final btn_deselect:I = 0x7f080095

.field public static final btn_finish:I = 0x7f080096

.field public static final btn_ip:I = 0x7f080097

.field public static final btn_local:I = 0x7f080098

.field public static final btn_next:I = 0x7f080099

.field public static final btn_paper_size:I = 0x7f08009a

.field public static final btn_photo_count:I = 0x7f08009b

.field public static final btn_pre:I = 0x7f08009c

.field public static final btn_print:I = 0x7f08009d

.field public static final btn_remote:I = 0x7f08009e

.field public static final btn_scan_settings:I = 0x7f08009f

.field public static final btn_web_next:I = 0x7f0800a0

.field public static final btn_web_pre:I = 0x7f0800a1

.field public static final btn_web_print:I = 0x7f0800a2

.field public static final button1:I = 0x7f0800a3

.field public static final button2:I = 0x7f0800a4

.field public static final button3:I = 0x7f0800a5

.field public static final buttonPanel:I = 0x7f0800a6

.field public static final buy_ink_arrow:I = 0x7f0800a7

.field public static final buy_ink_separator:I = 0x7f0800a8

.field public static final by_common:I = 0x7f0800a9

.field public static final by_common_header:I = 0x7f0800aa

.field public static final by_org:I = 0x7f0800ab

.field public static final by_org_header:I = 0x7f0800ac

.field public static final by_org_unit:I = 0x7f0800ad

.field public static final by_org_unit_header:I = 0x7f0800ae

.field public static final camera_setting:I = 0x7f0800af

.field public static final cancelButton:I = 0x7f0800b0

.field public static final cancel_action:I = 0x7f0800b1

.field public static final cancel_btn:I = 0x7f0800b2

.field public static final cancel_button:I = 0x7f0800b3

.field public static final cationText1:I = 0x7f0800b4

.field public static final caution_tv:I = 0x7f0800b5

.field public static final center:I = 0x7f0800b6

.field public static final center_horizontal:I = 0x7f0800b7

.field public static final center_vertical:I = 0x7f0800b8

.field public static final changecolor:I = 0x7f0800b9

.field public static final checkBox1:I = 0x7f0800ba

.field public static final checkIcon:I = 0x7f0800bb

.field public static final checkImage:I = 0x7f0800bc

.field public static final checkbox:I = 0x7f0800bd

.field public static final chronometer:I = 0x7f0800be

.field public static final chtc_contacts_groups_layout:I = 0x7f0800bf

.field public static final clean_check:I = 0x7f0800c0

.field public static final clear_access_key_btn:I = 0x7f0800c1

.field public static final clear_button:I = 0x7f0800c2

.field public static final clear_id_btn:I = 0x7f0800c3

.field public static final clear_mail_title_btn:I = 0x7f0800c4

.field public static final clear_name_btn:I = 0x7f0800c5

.field public static final clear_password_btn:I = 0x7f0800c6

.field public static final clear_printer_email_address_btn:I = 0x7f0800c7

.field public static final clear_printer_ip_address_btn:I = 0x7f0800c8

.field public static final clear_printer_name_btn:I = 0x7f0800c9

.field public static final clear_title_bookmark_btn:I = 0x7f0800ca

.field public static final clear_title_btn:I = 0x7f0800cb

.field public static final clip_horizontal:I = 0x7f0800cc

.field public static final clip_vertical:I = 0x7f0800cd

.field public static final closeImage:I = 0x7f0800ce

.field public static final close_btn:I = 0x7f0800cf

.field public static final close_button:I = 0x7f0800d0

.field public static final collapseActionView:I = 0x7f0800d1

.field public static final color:I = 0x7f0800d2

.field public static final colorModeSeparator:I = 0x7f0800d3

.field public static final color_adjustment_button:I = 0x7f0800d4

.field public static final color_info:I = 0x7f0800d5

.field public static final color_next_screen_imv:I = 0x7f0800d6

.field public static final comm_messeage_separator:I = 0x7f0800d7

.field public static final container:I = 0x7f0800d8

.field public static final content:I = 0x7f0800d9

.field public static final contentPanel:I = 0x7f0800da

.field public static final contentText:I = 0x7f0800db

.field public static final continue_button:I = 0x7f0800dc

.field public static final contrast:I = 0x7f0800dd

.field public static final contrast_minus_button:I = 0x7f0800de

.field public static final contrast_plus_button:I = 0x7f0800df

.field public static final copies:I = 0x7f0800e0

.field public static final copies_minus_button:I = 0x7f0800e1

.field public static final copies_plus_button:I = 0x7f0800e2

.field public static final copy:I = 0x7f0800e3

.field public static final copy_button:I = 0x7f0800e4

.field public static final copy_color_info:I = 0x7f0800e5

.field public static final copy_dest_local_folder_change:I = 0x7f0800e6

.field public static final copy_document_type_info:I = 0x7f0800e7

.field public static final copy_group1:I = 0x7f0800e8

.field public static final copy_group2:I = 0x7f0800e9

.field public static final copy_group3:I = 0x7f0800ea

.field public static final copy_group4:I = 0x7f0800eb

.field public static final copy_ln_toolbar:I = 0x7f0800ec

.field public static final copy_main:I = 0x7f0800ed

.field public static final copy_paper_size_info:I = 0x7f0800ee

.field public static final copy_paper_source_info:I = 0x7f0800ef

.field public static final copy_paper_type_info:I = 0x7f0800f0

.field public static final copy_quality_info:I = 0x7f0800f1

.field public static final copy_scale_custom:I = 0x7f0800f2

.field public static final copy_scale_group1:I = 0x7f0800f3

.field public static final copy_scale_group2:I = 0x7f0800f4

.field public static final copy_scale_group3:I = 0x7f0800f5

.field public static final copy_scale_info:I = 0x7f0800f6

.field public static final copy_setting_clear_all:I = 0x7f0800f7

.field public static final copy_setting_list:I = 0x7f0800f8

.field public static final copy_type_group:I = 0x7f0800f9

.field public static final copyingMessageText:I = 0x7f0800fa

.field public static final crop_image_frame_layout:I = 0x7f0800fb

.field public static final current_docsize_image:I = 0x7f0800fc

.field public static final custom:I = 0x7f0800fd

.field public static final customPanel:I = 0x7f0800fe

.field public static final custom_select_icon:I = 0x7f0800ff

.field public static final dark:I = 0x7f080100

.field public static final decor_content_parent:I = 0x7f080101

.field public static final default_activity_button:I = 0x7f080102

.field public static final delete_button:I = 0x7f080103

.field public static final density_image:I = 0x7f080104

.field public static final detail:I = 0x7f080105

.field public static final device_address:I = 0x7f080106

.field public static final device_name:I = 0x7f080107

.field public static final device_rssi:I = 0x7f080108

.field public static final dialog_back:I = 0x7f080109

.field public static final dialog_content:I = 0x7f08010a

.field public static final dialog_progress_id:I = 0x7f08010b

.field public static final dialog_progress_text_bottom:I = 0x7f08010c

.field public static final dialog_progress_text_right:I = 0x7f08010d

.field public static final dialog_title:I = 0x7f08010e

.field public static final disableHome:I = 0x7f08010f

.field public static final disagreeButton:I = 0x7f080110

.field public static final docsize_list:I = 0x7f080111

.field public static final docsize_name:I = 0x7f080112

.field public static final document_size:I = 0x7f080113

.field public static final document_size_button:I = 0x7f080114

.field public static final document_size_menu:I = 0x7f080115

.field public static final document_size_name:I = 0x7f080116

.field public static final document_size_scale:I = 0x7f080117

.field public static final document_type:I = 0x7f080118

.field public static final document_type_separator:I = 0x7f080119

.field public static final download_upload_progress:I = 0x7f08011a

.field public static final drop_box:I = 0x7f08011b

.field public static final drop_box_next:I = 0x7f08011c

.field public static final duplex:I = 0x7f08011d

.field public static final duplex_info:I = 0x7f08011e

.field public static final duplex_next_screen_imv:I = 0x7f08011f

.field public static final duplex_view:I = 0x7f080120

.field public static final edit_button:I = 0x7f080121

.field public static final edit_query:I = 0x7f080122

.field public static final empty:I = 0x7f080123

.field public static final end:I = 0x7f080124

.field public static final end_padder:I = 0x7f080125

.field public static final end_page_minus_button:I = 0x7f080126

.field public static final end_page_plus_button:I = 0x7f080127

.field public static final end_page_value:I = 0x7f080128

.field public static final epsonColorImageView:I = 0x7f080129

.field public static final epsonconnect_arrow:I = 0x7f08012a

.field public static final epsonconnect_msg_tv:I = 0x7f08012b

.field public static final epsonconnect_option:I = 0x7f08012c

.field public static final epsonconnect_separator:I = 0x7f08012d

.field public static final epsonconnect_sub_msg_tv:I = 0x7f08012e

.field public static final epsonconnect_title_tv:I = 0x7f08012f

.field public static final etPassword:I = 0x7f080130

.field public static final et_document_size:I = 0x7f080131

.field public static final eulaAgreeButton:I = 0x7f080132

.field public static final eulaCheckMainScrollView:I = 0x7f080133

.field public static final eulaDisagreeButton:I = 0x7f080134

.field public static final eulaTextView:I = 0x7f080135

.field public static final evernote:I = 0x7f080136

.field public static final evernote_next:I = 0x7f080137

.field public static final execute_button:I = 0x7f080138

.field public static final expand_activities_button:I = 0x7f080139

.field public static final expanded_menu:I = 0x7f08013a

.field public static final expires_on:I = 0x7f08013b

.field public static final expires_on_header:I = 0x7f08013c

.field public static final feed_direction:I = 0x7f08013d

.field public static final feed_direction_info:I = 0x7f08013e

.field public static final feed_direction_next_screen_imv:I = 0x7f08013f

.field public static final feed_direction_view:I = 0x7f080140

.field public static final file_folder_icon:I = 0x7f080141

.field public static final file_folder_name:I = 0x7f080142

.field public static final file_item_image:I = 0x7f080143

.field public static final file_item_text:I = 0x7f080144

.field public static final file_size:I = 0x7f080145

.field public static final file_size_label:I = 0x7f080146

.field public static final file_type_for_save_as:I = 0x7f080147

.field public static final file_type_for_upload:I = 0x7f080148

.field public static final filelist:I = 0x7f080149

.field public static final fill:I = 0x7f08014a

.field public static final fill_horizontal:I = 0x7f08014b

.field public static final fill_vertical:I = 0x7f08014c

.field public static final flScanResult:I = 0x7f08014d

.field public static final fl_list:I = 0x7f08014e

.field public static final fl_menulist:I = 0x7f08014f

.field public static final folderNameText:I = 0x7f080150

.field public static final folder_content_layout_for_upload:I = 0x7f080151

.field public static final folder_select_base:I = 0x7f080152

.field public static final folder_select_current:I = 0x7f080153

.field public static final folder_select_text:I = 0x7f080154

.field public static final footerLayout:I = 0x7f080155

.field public static final forever:I = 0x7f080156

.field public static final frameLayout1:I = 0x7f080157

.field public static final front:I = 0x7f080158

.field public static final full_select_icon:I = 0x7f080159

.field public static final full_size:I = 0x7f08015a

.field public static final function_bar:I = 0x7f08015b

.field public static final function_button:I = 0x7f08015c

.field public static final fwupdate_arrow:I = 0x7f08015d

.field public static final fwupdate_text:I = 0x7f08015e

.field public static final fwupdate_view:I = 0x7f08015f

.field public static final gallery:I = 0x7f080160

.field public static final get_mailadress:I = 0x7f080161

.field public static final go_back:I = 0x7f080162

.field public static final go_next:I = 0x7f080163

.field public static final google_doc:I = 0x7f080164

.field public static final google_doc_next:I = 0x7f080165

.field public static final gridView:I = 0x7f080166

.field public static final gridView1:I = 0x7f080167

.field public static final group_divider:I = 0x7f080168

.field public static final grv_album_contain:I = 0x7f080169

.field public static final grv_list_album:I = 0x7f08016a

.field public static final guidanceText:I = 0x7f08016b

.field public static final guidance_webview:I = 0x7f08016c

.field public static final guidance_with_lcd:I = 0x7f08016d

.field public static final guideToggleButton:I = 0x7f08016e

.field public static final guideViewPager:I = 0x7f08016f

.field public static final guideWebViewFrame:I = 0x7f080170

.field public static final guideWebview:I = 0x7f080171

.field public static final guide_dialog_buttons:I = 0x7f080172

.field public static final guide_dialog_horizon_gray_line:I = 0x7f080173

.field public static final guide_dialog_layout:I = 0x7f080174

.field public static final guide_dialog_message:I = 0x7f080175

.field public static final guide_layout:I = 0x7f080176

.field public static final head_clean_arrow:I = 0x7f080177

.field public static final head_clean_text:I = 0x7f080178

.field public static final head_clean_view:I = 0x7f080179

.field public static final headerSeparator:I = 0x7f08017a

.field public static final height:I = 0x7f08017b

.field public static final height_dec_button:I = 0x7f08017c

.field public static final height_inc_button:I = 0x7f08017d

.field public static final home:I = 0x7f08017e

.field public static final homeAsUp:I = 0x7f08017f

.field public static final home_printer:I = 0x7f080180

.field public static final home_printer_name:I = 0x7f080181

.field public static final home_printer_next:I = 0x7f080182

.field public static final home_printer_status:I = 0x7f080183

.field public static final home_screen_top:I = 0x7f080184

.field public static final home_select_printer:I = 0x7f080185

.field public static final home_select_printer_detail:I = 0x7f080186

.field public static final horizontalProgressBar:I = 0x7f080187

.field public static final iPAdressArea:I = 0x7f080188

.field public static final icon:I = 0x7f080189

.field public static final icon_check_mark:I = 0x7f08018a

.field public static final icon_group:I = 0x7f08018b

.field public static final icon_only:I = 0x7f08018c

.field public static final icon_papermissmatch:I = 0x7f08018d

.field public static final id_toggle_use_mediastorage_thumbnail:I = 0x7f08018e

.field public static final ifRoom:I = 0x7f08018f

.field public static final image:I = 0x7f080190

.field public static final imageButton1:I = 0x7f080191

.field public static final imageButton2:I = 0x7f080192

.field public static final imageLoadProgressBar:I = 0x7f080193

.field public static final imageThumbnail:I = 0x7f080194

.field public static final imageView:I = 0x7f080195

.field public static final imageView1:I = 0x7f080196

.field public static final imageView2:I = 0x7f080197

.field public static final imageViewPrinterLocation:I = 0x7f080198

.field public static final imageVolumeText:I = 0x7f080199

.field public static final image_layout:I = 0x7f08019a

.field public static final imv_icon1:I = 0x7f08019b

.field public static final imv_icon2:I = 0x7f08019c

.field public static final imv_icon3:I = 0x7f08019d

.field public static final imv_icon4:I = 0x7f08019e

.field public static final in_progress:I = 0x7f08019f

.field public static final inch_button:I = 0x7f0801a0

.field public static final info:I = 0x7f0801a1

.field public static final information_setting:I = 0x7f0801a2

.field public static final ink_error:I = 0x7f0801a3

.field public static final ink_holder:I = 0x7f0801a4

.field public static final ink_level:I = 0x7f0801a5

.field public static final ink_name:I = 0x7f0801a6

.field public static final ink_text:I = 0x7f0801a7

.field public static final ink_view_ln:I = 0x7f0801a8

.field public static final inkds_arrow:I = 0x7f0801a9

.field public static final ip:I = 0x7f0801aa

.field public static final iprint:I = 0x7f0801ab

.field public static final iprint_connect:I = 0x7f0801ac

.field public static final issued_by_header:I = 0x7f0801ad

.field public static final issued_on:I = 0x7f0801ae

.field public static final issued_on_header:I = 0x7f0801af

.field public static final issued_to_header:I = 0x7f0801b0

.field public static final italic:I = 0x7f0801b1

.field public static final ivDrakerIcon:I = 0x7f0801b2

.field public static final ivResult:I = 0x7f0801b3

.field public static final ivSettingSelected:I = 0x7f0801b4

.field public static final jpeg_type:I = 0x7f0801b5

.field public static final layout:I = 0x7f0801b6

.field public static final layout_image:I = 0x7f0801b7

.field public static final layout_info:I = 0x7f0801b8

.field public static final layout_loading:I = 0x7f0801b9

.field public static final layout_next_screen_imv:I = 0x7f0801ba

.field public static final layout_root:I = 0x7f0801bb

.field public static final left:I = 0x7f0801bc

.field public static final leftBtn:I = 0x7f0801bd

.field public static final leftImageView:I = 0x7f0801be

.field public static final licenseViewGroup:I = 0x7f0801bf

.field public static final light:I = 0x7f0801c0

.field public static final line1:I = 0x7f0801c1

.field public static final line3:I = 0x7f0801c2

.field public static final linearLayout:I = 0x7f0801c3

.field public static final linearLayout1:I = 0x7f0801c4

.field public static final linearLayout2:I = 0x7f0801c5

.field public static final linearLayout3:I = 0x7f0801c6

.field public static final linearLayoutAbout:I = 0x7f0801c7

.field public static final linearLayoutBluetoothStatus:I = 0x7f0801c8

.field public static final linearLayoutDeviceInformation:I = 0x7f0801c9

.field public static final linearLayoutFAQ:I = 0x7f0801ca

.field public static final linearLayoutIPAddress:I = 0x7f0801cb

.field public static final linearLayoutIprintNews:I = 0x7f0801cc

.field public static final linearLayoutLicense:I = 0x7f0801cd

.field public static final linearLayoutOSVersion:I = 0x7f0801ce

.field public static final linearLayoutPrinterIPAddress:I = 0x7f0801cf

.field public static final linearLayoutPrinterName:I = 0x7f0801d0

.field public static final linearLayoutPrinterStatus:I = 0x7f0801d1

.field public static final linearLayoutPrivacy:I = 0x7f0801d2

.field public static final linearLayoutSSIDName:I = 0x7f0801d3

.field public static final linearLayoutSendUageinformation:I = 0x7f0801d4

.field public static final linearLayoutStatusAndIP:I = 0x7f0801d5

.field public static final linearLayoutTutorial:I = 0x7f0801d6

.field public static final linearLayoutWifiState:I = 0x7f0801d7

.field public static final linear_layout:I = 0x7f0801d8

.field public static final linear_layout_count:I = 0x7f0801d9

.field public static final linear_layout_papersize:I = 0x7f0801da

.field public static final link:I = 0x7f0801db

.field public static final list:I = 0x7f0801dc

.field public static final listBookmark:I = 0x7f0801dd

.field public static final listHistory:I = 0x7f0801de

.field public static final listMode:I = 0x7f0801df

.field public static final listView:I = 0x7f0801e0

.field public static final list_empty_message:I = 0x7f0801e1

.field public static final list_item:I = 0x7f0801e2

.field public static final listview:I = 0x7f0801e3

.field public static final llScanArea:I = 0x7f0801e4

.field public static final llScanAreaBackground:I = 0x7f0801e5

.field public static final llScanFunctionBar:I = 0x7f0801e6

.field public static final llScanSettingsAdvance2Sided:I = 0x7f0801e7

.field public static final llScanSettingsColor:I = 0x7f0801e8

.field public static final llScanSettingsDensity:I = 0x7f0801e9

.field public static final llScanSettingsGamma:I = 0x7f0801ea

.field public static final llScanSettingsResolution:I = 0x7f0801eb

.field public static final llScanSettingsSize:I = 0x7f0801ec

.field public static final llScanSettingsSource:I = 0x7f0801ed

.field public static final llScannerName:I = 0x7f0801ee

.field public static final llScanningProgress:I = 0x7f0801ef

.field public static final ln_input_url:I = 0x7f0801f0

.field public static final ln_next:I = 0x7f0801f1

.field public static final ln_pre:I = 0x7f0801f2

.field public static final ln_toolbar:I = 0x7f0801f3

.field public static final ln_web_next:I = 0x7f0801f4

.field public static final ln_web_pre:I = 0x7f0801f5

.field public static final ln_web_zoomview:I = 0x7f0801f6

.field public static final ln_zoomview:I = 0x7f0801f7

.field public static final loading_tv:I = 0x7f0801f8

.field public static final local_memory:I = 0x7f0801f9

.field public static final location:I = 0x7f0801fa

.field public static final log_in_fail_notification:I = 0x7f0801fb

.field public static final login_basic_screen:I = 0x7f0801fc

.field public static final login_password_content:I = 0x7f0801fd

.field public static final login_password_label:I = 0x7f0801fe

.field public static final login_username_content:I = 0x7f0801ff

.field public static final login_username_label:I = 0x7f080200

.field public static final lowerListView:I = 0x7f080201

.field public static final lowerText:I = 0x7f080202

.field public static final lvScannerSettingOptions:I = 0x7f080203

.field public static final lvScannerSettingOptions2:I = 0x7f080204

.field public static final mainScrollView:I = 0x7f080205

.field public static final main_view:I = 0x7f080206

.field public static final maintenance_box_info_layout:I = 0x7f080207

.field public static final maintenance_box_text:I = 0x7f080208

.field public static final maintenance_inkds:I = 0x7f080209

.field public static final manual:I = 0x7f08020a

.field public static final manual_connect:I = 0x7f08020b

.field public static final media_actions:I = 0x7f08020c

.field public static final memcardPassword:I = 0x7f08020d

.field public static final memcardUsername:I = 0x7f08020e

.field public static final mentenance_buy_ink:I = 0x7f08020f

.field public static final mentenance_buy_ink_frame:I = 0x7f080210

.field public static final menuCancel:I = 0x7f080211

.field public static final menuSettingsDone:I = 0x7f080212

.field public static final menu_item_btn:I = 0x7f080213

.field public static final menu_list:I = 0x7f080214

.field public static final menu_signin:I = 0x7f080215

.field public static final menu_signout:I = 0x7f080216

.field public static final menu_text:I = 0x7f080217

.field public static final message:I = 0x7f080218

.field public static final messageText:I = 0x7f080219

.field public static final messageTitle:I = 0x7f08021a

.field public static final message_text:I = 0x7f08021b

.field public static final middle:I = 0x7f08021c

.field public static final mm_button:I = 0x7f08021d

.field public static final multiply:I = 0x7f08021e

.field public static final myToggle:I = 0x7f08021f

.field public static final myToggleImage:I = 0x7f080220

.field public static final myToggleText:I = 0x7f080221

.field public static final name:I = 0x7f080222

.field public static final navigation_bar:I = 0x7f080223

.field public static final never:I = 0x7f080224

.field public static final nextButton:I = 0x7f080225

.field public static final ng_text:I = 0x7f080226

.field public static final no_lcd:I = 0x7f080227

.field public static final none:I = 0x7f080228

.field public static final normal:I = 0x7f080229

.field public static final normal_copy_tab:I = 0x7f08022a

.field public static final not_found_printer_epsonsn:I = 0x7f08022b

.field public static final notification_background:I = 0x7f08022c

.field public static final notification_main_column:I = 0x7f08022d

.field public static final notification_main_column_container:I = 0x7f08022e

.field public static final nozzle_check_text:I = 0x7f08022f

.field public static final nozzle_check_view:I = 0x7f080230

.field public static final oauth_container:I = 0x7f080231

.field public static final oauthview:I = 0x7f080232

.field public static final ok:I = 0x7f080233

.field public static final okButton:I = 0x7f080234

.field public static final ok_button:I = 0x7f080235

.field public static final ok_text:I = 0x7f080236

.field public static final onedrive:I = 0x7f080237

.field public static final onedrive_next:I = 0x7f080238

.field public static final online_registration:I = 0x7f080239

.field public static final online_registration_arrow:I = 0x7f08023a

.field public static final online_registration_separator:I = 0x7f08023b

.field public static final open_in:I = 0x7f08023c

.field public static final ossLicenseLayout:I = 0x7f08023d

.field public static final oss_license_title_text:I = 0x7f08023e

.field public static final page:I = 0x7f08023f

.field public static final pageDisplayViewGroup:I = 0x7f080240

.field public static final pageDot1:I = 0x7f080241

.field public static final page_range_next_screen_imv:I = 0x7f080242

.field public static final page_range_separator:I = 0x7f080243

.field public static final page_range_setting:I = 0x7f080244

.field public static final page_range_value:I = 0x7f080245

.field public static final pagebar_bottom:I = 0x7f080246

.field public static final pagebar_top:I = 0x7f080247

.field public static final pagebar_web_top:I = 0x7f080248

.field public static final pager:I = 0x7f080249

.field public static final paper_size:I = 0x7f08024a

.field public static final paper_size_button:I = 0x7f08024b

.field public static final paper_size_info:I = 0x7f08024c

.field public static final paper_size_next_screen_imv:I = 0x7f08024d

.field public static final paper_size_text:I = 0x7f08024e

.field public static final paper_source:I = 0x7f08024f

.field public static final paper_source_info:I = 0x7f080250

.field public static final paper_source_next_screen_imv:I = 0x7f080251

.field public static final paper_type:I = 0x7f080252

.field public static final paper_type_info:I = 0x7f080253

.field public static final paper_type_next_screen_imv:I = 0x7f080254

.field public static final paper_type_next_screen_imv2:I = 0x7f080255

.field public static final parentPanel:I = 0x7f080256

.field public static final password_edit:I = 0x7f080257

.field public static final password_message:I = 0x7f080258

.field public static final password_view:I = 0x7f080259

.field public static final pdf_type:I = 0x7f08025a

.field public static final percent:I = 0x7f08025b

.field public static final photo_select_button:I = 0x7f08025c

.field public static final photocopy_button:I = 0x7f08025d

.field public static final photograph_button:I = 0x7f08025e

.field public static final pictureResolutionImageView:I = 0x7f08025f

.field public static final pictureResolutionText:I = 0x7f080260

.field public static final picture_resolution_layout:I = 0x7f080261

.field public static final pixtureResText:I = 0x7f080262

.field public static final placeholder:I = 0x7f080263

.field public static final previewImageView:I = 0x7f080264

.field public static final preview_papersetting:I = 0x7f080265

.field public static final preview_view:I = 0x7f080266

.field public static final print_all_toggle_button:I = 0x7f080267

.field public static final print_button:I = 0x7f080268

.field public static final print_cut_line_layout:I = 0x7f080269

.field public static final print_cut_line_next_icon:I = 0x7f08026a

.field public static final print_cut_line_style_layout:I = 0x7f08026b

.field public static final print_cut_line_style_next_icon:I = 0x7f08026c

.field public static final print_cut_line_style_separator:I = 0x7f08026d

.field public static final print_cut_line_style_value:I = 0x7f08026e

.field public static final print_cut_line_value:I = 0x7f08026f

.field public static final print_cut_line_weight_layout:I = 0x7f080270

.field public static final print_cut_line_weight_next_icon:I = 0x7f080271

.field public static final print_cut_line_weight_separator:I = 0x7f080272

.field public static final print_cut_line_weight_value:I = 0x7f080273

.field public static final print_history:I = 0x7f080274

.field public static final print_history_arrow:I = 0x7f080275

.field public static final print_history_separator:I = 0x7f080276

.field public static final print_local_icon:I = 0x7f080277

.field public static final print_option:I = 0x7f080278

.field public static final print_preview:I = 0x7f080279

.field public static final print_remote_icon:I = 0x7f08027a

.field public static final printdate:I = 0x7f08027b

.field public static final printdate_info:I = 0x7f08027c

.field public static final printdate_next_screen_imv:I = 0x7f08027d

.field public static final printer:I = 0x7f08027e

.field public static final printerNameArea:I = 0x7f08027f

.field public static final printer_email_address_edit:I = 0x7f080280

.field public static final printer_email_text:I = 0x7f080281

.field public static final printer_icon:I = 0x7f080282

.field public static final printer_ip_address_edit:I = 0x7f080283

.field public static final printer_name:I = 0x7f080284

.field public static final printer_name_edit:I = 0x7f080285

.field public static final printer_name_layout:I = 0x7f080286

.field public static final printer_name_text:I = 0x7f080287

.field public static final printer_next_screen_imv:I = 0x7f080288

.field public static final printer_setting:I = 0x7f080289

.field public static final printer_setting_arrow:I = 0x7f08028a

.field public static final printer_setting_separator:I = 0x7f08028b

.field public static final printer_status:I = 0x7f08028c

.field public static final printer_status_detail:I = 0x7f08028d

.field public static final printtarget_size_text:I = 0x7f08028e

.field public static final privacyStatementViewGroup:I = 0x7f08028f

.field public static final progress:I = 0x7f080290

.field public static final progressBar:I = 0x7f080291

.field public static final progressBar1:I = 0x7f080292

.field public static final progressBar2:I = 0x7f080293

.field public static final progressBar3:I = 0x7f080294

.field public static final progressBar4:I = 0x7f080295

.field public static final progressBarGetConnectStrings:I = 0x7f080296

.field public static final progressGetOption:I = 0x7f080297

.field public static final progress_bar:I = 0x7f080298

.field public static final progress_circular:I = 0x7f080299

.field public static final progress_copies:I = 0x7f08029a

.field public static final progress_horizontal:I = 0x7f08029b

.field public static final progress_page:I = 0x7f08029c

.field public static final progress_percent:I = 0x7f08029d

.field public static final progress_status:I = 0x7f08029e

.field public static final prtlog_webview:I = 0x7f08029f

.field public static final quality:I = 0x7f0802a0

.field public static final quality_info:I = 0x7f0802a1

.field public static final quality_next_screen_imv:I = 0x7f0802a2

.field public static final radio:I = 0x7f0802a3

.field public static final remove_background_layout:I = 0x7f0802a4

.field public static final remove_background_next_icon:I = 0x7f0802a5

.field public static final remove_background_separator:I = 0x7f0802a6

.field public static final remove_background_value:I = 0x7f0802a7

.field public static final repeat_copy_layout:I = 0x7f0802a8

.field public static final repeat_copy_layout_value:I = 0x7f0802a9

.field public static final repeat_copy_tab:I = 0x7f0802aa

.field public static final reset_button:I = 0x7f0802ab

.field public static final resolution_next_screen_imv:I = 0x7f0802ac

.field public static final resolution_next_screen_imv2:I = 0x7f0802ad

.field public static final retouch_button:I = 0x7f0802ae

.field public static final right:I = 0x7f0802af

.field public static final right_icon:I = 0x7f0802b0

.field public static final right_side:I = 0x7f0802b1

.field public static final rlPrinterSettings:I = 0x7f0802b2

.field public static final rlSeekDensityBar:I = 0x7f0802b3

.field public static final rlWifiSettings:I = 0x7f0802b4

.field public static final rl_album_border:I = 0x7f0802b5

.field public static final rl_album_icon:I = 0x7f0802b6

.field public static final rl_item_contain:I = 0x7f0802b7

.field public static final rl_localremoteselect:I = 0x7f0802b8

.field public static final rl_text:I = 0x7f0802b9

.field public static final rl_web_zoomview:I = 0x7f0802ba

.field public static final rl_zoomview:I = 0x7f0802bb

.field public static final rotate_button:I = 0x7f0802bc

.field public static final row_list_area:I = 0x7f0802bd

.field public static final row_prefix_image:I = 0x7f0802be

.field public static final row_suffix_image:I = 0x7f0802bf

.field public static final saturation:I = 0x7f0802c0

.field public static final saturation_minus_button:I = 0x7f0802c1

.field public static final saturation_plus_button:I = 0x7f0802c2

.field public static final saveDirectoryText:I = 0x7f0802c3

.field public static final save_as_dialog_file_name:I = 0x7f0802c4

.field public static final save_as_dialog_file_name_label:I = 0x7f0802c5

.field public static final save_as_dialog_file_type_label:I = 0x7f0802c6

.field public static final save_as_file_size:I = 0x7f0802c7

.field public static final save_as_file_size_label:I = 0x7f0802c8

.field public static final save_as_in_progress:I = 0x7f0802c9

.field public static final save_as_jpeg_type:I = 0x7f0802ca

.field public static final save_as_pdf_type:I = 0x7f0802cb

.field public static final save_button:I = 0x7f0802cc

.field public static final save_directory_layout:I = 0x7f0802cd

.field public static final save_section:I = 0x7f0802ce

.field public static final save_to_local_section:I = 0x7f0802cf

.field public static final save_to_onlonestrage:I = 0x7f0802d0

.field public static final scale:I = 0x7f0802d1

.field public static final scale_fl_list:I = 0x7f0802d2

.field public static final scale_list:I = 0x7f0802d3

.field public static final scale_text:I = 0x7f0802d4

.field public static final scanContinue_msg_tv:I = 0x7f0802d5

.field public static final scanContinue_title_tv:I = 0x7f0802d6

.field public static final scanLocation:I = 0x7f0802d7

.field public static final scanModelName:I = 0x7f0802d8

.field public static final scanProgressText:I = 0x7f0802d9

.field public static final scanSelected:I = 0x7f0802da

.field public static final scanSizeSeparator:I = 0x7f0802db

.field public static final screen:I = 0x7f0802dc

.field public static final screen_image:I = 0x7f0802dd

.field public static final screen_text:I = 0x7f0802de

.field public static final scroll:I = 0x7f0802df

.field public static final scrollIndicatorDown:I = 0x7f0802e0

.field public static final scrollIndicatorUp:I = 0x7f0802e1

.field public static final scrollView:I = 0x7f0802e2

.field public static final scrollView1:I = 0x7f0802e3

.field public static final search_badge:I = 0x7f0802e4

.field public static final search_bar:I = 0x7f0802e5

.field public static final search_button:I = 0x7f0802e6

.field public static final search_close_btn:I = 0x7f0802e7

.field public static final search_edit_frame:I = 0x7f0802e8

.field public static final search_go_btn:I = 0x7f0802e9

.field public static final search_mag_icon:I = 0x7f0802ea

.field public static final search_plate:I = 0x7f0802eb

.field public static final search_src_text:I = 0x7f0802ec

.field public static final search_voice_btn:I = 0x7f0802ed

.field public static final seekDensity:I = 0x7f0802ee

.field public static final selectButton:I = 0x7f0802ef

.field public static final select_button:I = 0x7f0802f0

.field public static final select_dialog_listview:I = 0x7f0802f1

.field public static final serial:I = 0x7f0802f2

.field public static final serial_no:I = 0x7f0802f3

.field public static final serial_separator:I = 0x7f0802f4

.field public static final setting_active_icon:I = 0x7f0802f5

.field public static final setting_name:I = 0x7f0802f6

.field public static final setting_value:I = 0x7f0802f7

.field public static final setting_view_ln:I = 0x7f0802f8

.field public static final sharpness_setting_imageview:I = 0x7f0802f9

.field public static final sharpness_setting_layout:I = 0x7f0802fa

.field public static final sharpness_setting_text:I = 0x7f0802fb

.field public static final shortcut:I = 0x7f0802fc

.field public static final showCustom:I = 0x7f0802fd

.field public static final showHome:I = 0x7f0802fe

.field public static final showTitle:I = 0x7f0802ff

.field public static final signal:I = 0x7f080300

.field public static final source_next_screen_imv:I = 0x7f080301

.field public static final space_upper:I = 0x7f080302

.field public static final spacer:I = 0x7f080303

.field public static final split_action_bar:I = 0x7f080304

.field public static final src_atop:I = 0x7f080305

.field public static final src_in:I = 0x7f080306

.field public static final src_over:I = 0x7f080307

.field public static final standard:I = 0x7f080308

.field public static final start:I = 0x7f080309

.field public static final start_page_minus_button:I = 0x7f08030a

.field public static final start_page_plus_button:I = 0x7f08030b

.field public static final start_page_value:I = 0x7f08030c

.field public static final status:I = 0x7f08030d

.field public static final status_bar_latest_event_content:I = 0x7f08030e

.field public static final stop_screen_txt:I = 0x7f08030f

.field public static final storage_progress_horizontalProgressBar:I = 0x7f080310

.field public static final storage_progress_messageTitle:I = 0x7f080311

.field public static final sub_pain:I = 0x7f080312

.field public static final submenuarrow:I = 0x7f080313

.field public static final submit_area:I = 0x7f080314

.field public static final tab1Layout:I = 0x7f080315

.field public static final tab2Layout:I = 0x7f080316

.field public static final tabMode:I = 0x7f080317

.field public static final tag_transition_group:I = 0x7f080318

.field public static final tag_unhandled_key_event_manager:I = 0x7f080319

.field public static final tag_unhandled_key_listeners:I = 0x7f08031a

.field public static final tb_send_usageinformation:I = 0x7f08031b

.field public static final te_url:I = 0x7f08031c

.field public static final text:I = 0x7f08031d

.field public static final text2:I = 0x7f08031e

.field public static final textFileNumber:I = 0x7f08031f

.field public static final textFileSize:I = 0x7f080320

.field public static final textSelectedImageNum:I = 0x7f080321

.field public static final textSelectedNum:I = 0x7f080322

.field public static final textSpacerNoButtons:I = 0x7f080323

.field public static final textSpacerNoTitle:I = 0x7f080324

.field public static final textView:I = 0x7f080325

.field public static final textView1:I = 0x7f080326

.field public static final textView2:I = 0x7f080327

.field public static final textView23:I = 0x7f080328

.field public static final textView3:I = 0x7f080329

.field public static final textView4:I = 0x7f08032a

.field public static final textView5:I = 0x7f08032b

.field public static final textView6:I = 0x7f08032c

.field public static final textViewNoPreviewMsg:I = 0x7f08032d

.field public static final textWriteFolder:I = 0x7f08032e

.field public static final textWritePrt:I = 0x7f08032f

.field public static final text_alert_messeage:I = 0x7f080330

.field public static final text_buy_ink:I = 0x7f080331

.field public static final text_epson_connect:I = 0x7f080332

.field public static final text_inkds:I = 0x7f080333

.field public static final text_maintenance:I = 0x7f080334

.field public static final text_online_registration:I = 0x7f080335

.field public static final text_print_history:I = 0x7f080336

.field public static final text_print_preview:I = 0x7f080337

.field public static final text_printer_setting:I = 0x7f080338

.field public static final thumbNail:I = 0x7f080339

.field public static final time:I = 0x7f08033a

.field public static final title:I = 0x7f08033b

.field public static final titleDivider:I = 0x7f08033c

.field public static final titleDividerNoCustom:I = 0x7f08033d

.field public static final titleDividerTop:I = 0x7f08033e

.field public static final title_separator:I = 0x7f08033f

.field public static final title_template:I = 0x7f080340

.field public static final title_text:I = 0x7f080341

.field public static final to_common:I = 0x7f080342

.field public static final to_common_header:I = 0x7f080343

.field public static final to_org:I = 0x7f080344

.field public static final to_org_header:I = 0x7f080345

.field public static final to_org_unit:I = 0x7f080346

.field public static final to_org_unit_header:I = 0x7f080347

.field public static final toggleAlert:I = 0x7f080348

.field public static final toggleButton2:I = 0x7f080349

.field public static final toggleDensity:I = 0x7f08034a

.field public static final togglePreview:I = 0x7f08034b

.field public static final tool_bar:I = 0x7f08034c

.field public static final toolbar:I = 0x7f08034d

.field public static final top:I = 0x7f08034e

.field public static final topPanel:I = 0x7f08034f

.field public static final topText:I = 0x7f080350

.field public static final tvBrightness:I = 0x7f080351

.field public static final tvDensityValue:I = 0x7f080352

.field public static final tvScanResult:I = 0x7f080353

.field public static final tvScanner2Sided:I = 0x7f080354

.field public static final tvScannerColor:I = 0x7f080355

.field public static final tvScannerDensity:I = 0x7f080356

.field public static final tvScannerGamma:I = 0x7f080357

.field public static final tvScannerName:I = 0x7f080358

.field public static final tvScannerNameTitle:I = 0x7f080359

.field public static final tvScannerResolution:I = 0x7f08035a

.field public static final tvScannerSize:I = 0x7f08035b

.field public static final tvScannerSource:I = 0x7f08035c

.field public static final tvSettingName:I = 0x7f08035d

.field public static final tv_DeviceInformation:I = 0x7f08035e

.field public static final tv_about:I = 0x7f08035f

.field public static final tv_album_name:I = 0x7f080360

.field public static final tv_deviceBluetoothStatus:I = 0x7f080361

.field public static final tv_deviceIPAddress:I = 0x7f080362

.field public static final tv_deviceOSVersion:I = 0x7f080363

.field public static final tv_devicePrinterIPAddress:I = 0x7f080364

.field public static final tv_devicePrinterIPAddressInfo:I = 0x7f080365

.field public static final tv_devicePrinterInformation:I = 0x7f080366

.field public static final tv_devicePrinterName:I = 0x7f080367

.field public static final tv_devicePrinterStatus:I = 0x7f080368

.field public static final tv_devicePrinterStatusInfo:I = 0x7f080369

.field public static final tv_deviceSSIDName:I = 0x7f08036a

.field public static final tv_deviceWifiState:I = 0x7f08036b

.field public static final tv_faq:I = 0x7f08036c

.field public static final tv_info:I = 0x7f08036d

.field public static final tv_items_count:I = 0x7f08036e

.field public static final tv_license:I = 0x7f08036f

.field public static final tv_page_num:I = 0x7f080370

.field public static final tv_page_size:I = 0x7f080371

.field public static final tv_privacy:I = 0x7f080372

.field public static final tv_send_usageinformation:I = 0x7f080373

.field public static final tv_terminalInformation:I = 0x7f080374

.field public static final tv_web_page_size:I = 0x7f080375

.field public static final tvepsonconnect:I = 0x7f080376

.field public static final type_next_screen_imv:I = 0x7f080377

.field public static final uniform:I = 0x7f080378

.field public static final up:I = 0x7f080379

.field public static final upload_dialog_file_name:I = 0x7f08037a

.field public static final upload_dialog_file_name_label:I = 0x7f08037b

.field public static final upload_dialog_file_type_label:I = 0x7f08037c

.field public static final upload_dialog_save_path:I = 0x7f08037d

.field public static final upload_file_name_file_type_section:I = 0x7f08037e

.field public static final upperListView:I = 0x7f08037f

.field public static final useLogo:I = 0x7f080380

.field public static final username_edit:I = 0x7f080381

.field public static final username_view:I = 0x7f080382

.field public static final validity_header:I = 0x7f080383

.field public static final version:I = 0x7f080384

.field public static final view1:I = 0x7f080385

.field public static final view2:I = 0x7f080386

.field public static final viewGroupPhotoEffect:I = 0x7f080387

.field public static final viewScanSettingsAdvance2Sided:I = 0x7f080388

.field public static final viewScanSettingsSource:I = 0x7f080389

.field public static final web_engine:I = 0x7f08038a

.field public static final web_pagebar:I = 0x7f08038b

.field public static final web_zoomview:I = 0x7f08038c

.field public static final webview:I = 0x7f08038d

.field public static final white_ink_level:I = 0x7f08038e

.field public static final wide:I = 0x7f08038f

.field public static final width:I = 0x7f080390

.field public static final width_dec_button:I = 0x7f080391

.field public static final width_inc_button:I = 0x7f080392

.field public static final width_lcd:I = 0x7f080393

.field public static final wifi:I = 0x7f080394

.field public static final wifiSettings:I = 0x7f080395

.field public static final wifi_start_top:I = 0x7f080396

.field public static final withText:I = 0x7f080397

.field public static final wrap_content:I = 0x7f080398

.field public static final zoomview:I = 0x7f080399


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lepson/print/EPImageCreator;
.super Ljava/lang/Object;
.source "EPImageCreator.java"

# interfaces
.implements Lepson/print/CommonDefine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/EPImageCreator$MakeMultiPrintImage;
    }
.end annotation


# static fields
.field private static CANCEL_FILE_NAME:Ljava/lang/String;

.field private static final EPS_ERR_NONE:I

.field private static final lockObject:Ljava/lang/Object;


# instance fields
.field private dst:Landroid/graphics/Rect;

.field private mContext:Landroid/content/Context;

.field private mEpsonImage:Lepson/image/epsonImage;

.field private volatile mStopREquested:Z

.field private src:Landroid/graphics/Rect;

.field util:Lepson/print/EPImageUtil;

.field utilFolder:Lepson/print/Util/Utils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "opencv_java3"

    .line 41
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/EPImageCreator;->lockObject:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 57
    sput-object v0, Lepson/print/EPImageCreator;->CANCEL_FILE_NAME:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 44
    iput-object v0, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    .line 49
    new-instance v0, Lepson/print/EPImageUtil;

    invoke-direct {v0}, Lepson/print/EPImageUtil;-><init>()V

    iput-object v0, p0, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    .line 50
    new-instance v0, Lepson/print/Util/Utils;

    invoke-direct {v0}, Lepson/print/Util/Utils;-><init>()V

    iput-object v0, p0, Lepson/print/EPImageCreator;->utilFolder:Lepson/print/Util/Utils;

    .line 51
    new-instance v0, Lepson/image/epsonImage;

    invoke-direct {v0}, Lepson/image/epsonImage;-><init>()V

    iput-object v0, p0, Lepson/print/EPImageCreator;->mEpsonImage:Lepson/image/epsonImage;

    .line 53
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 44
    iput-object v0, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    .line 49
    new-instance v0, Lepson/print/EPImageUtil;

    invoke-direct {v0}, Lepson/print/EPImageUtil;-><init>()V

    iput-object v0, p0, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    .line 50
    new-instance v0, Lepson/print/Util/Utils;

    invoke-direct {v0}, Lepson/print/Util/Utils;-><init>()V

    iput-object v0, p0, Lepson/print/EPImageCreator;->utilFolder:Lepson/print/Util/Utils;

    .line 51
    new-instance v0, Lepson/image/epsonImage;

    invoke-direct {v0}, Lepson/image/epsonImage;-><init>()V

    iput-object v0, p0, Lepson/print/EPImageCreator;->mEpsonImage:Lepson/image/epsonImage;

    .line 53
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    .line 62
    iput-object p1, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    .line 63
    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cancel.dat"

    invoke-direct {p1, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lepson/print/EPImageCreator;->CANCEL_FILE_NAME:Ljava/lang/String;

    .line 65
    invoke-direct {p0}, Lepson/print/EPImageCreator;->deleteCancelFile()V

    return-void
.end method

.method private LoadJpegFile(Lepson/print/EPImage;I)Ljava/lang/String;
    .locals 11

    .line 109
    invoke-virtual {p0}, Lepson/print/EPImageCreator;->stopRequested()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 110
    invoke-direct {p0}, Lepson/print/EPImageCreator;->deleteCancelFile()V

    .line 111
    iput-object v1, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    return-object v1

    .line 117
    :cond_0
    iget-object v0, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string p1, "EPImageCreator"

    const-string p2, "loadImageFileName is null"

    .line 118
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 123
    :cond_1
    :try_start_0
    iget-object v0, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-static {v0}, Lepson/print/Util/ImageFormatIdentifier;->identifyImageFormat(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    packed-switch v0, :pswitch_data_0

    return-object v1

    .line 160
    :pswitch_0
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v4

    invoke-virtual {v4}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "decode_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p1, Lepson/print/EPImage;->index:I

    add-int/2addr v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ".bmp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 162
    iget-object v0, p0, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    iget-object v4, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    iget-object v5, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5, p2}, Lepson/print/EPImageUtil;->jpg2bmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p2

    if-eqz p2, :cond_3

    .line 164
    invoke-virtual {p0}, Lepson/print/EPImageCreator;->stopRequested()Z

    move-result p2

    if-eqz p2, :cond_2

    const-string p2, "EPImageCreator"

    const-string v0, "EPImageCreator::LoadJpegFile() jpg2bmp stoped."

    .line 165
    invoke-static {p2, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 168
    :cond_2
    iput-object v1, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 169
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-direct {p2}, Ljava/lang/IllegalStateException;-><init>()V

    throw p2

    .line 173
    :cond_3
    new-instance p2, Landroid/media/ExifInterface;

    iget-object v0, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-direct {p2, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string v0, "Orientation"

    .line 174
    invoke-virtual {p2, v0, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result p2

    .line 175
    iget-object v0, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    const/16 v4, 0x8

    const/4 v5, 0x6

    const/4 v6, 0x3

    if-eq p2, v6, :cond_6

    if-eq p2, v5, :cond_5

    if-eq p2, v4, :cond_4

    move-object v7, v0

    goto/16 :goto_0

    .line 191
    :cond_4
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v8

    invoke-virtual {v8}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "decode_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v10, p1, Lepson/print/EPImage;->index:I

    add-int/2addr v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, "_270.bmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 193
    iget-object v8, p0, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v8, v0, v7}, Lepson/print/EPImageUtil;->rotateR270Image(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 179
    :cond_5
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v8

    invoke-virtual {v8}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "decode_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v10, p1, Lepson/print/EPImage;->index:I

    add-int/2addr v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, "_90.bmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 181
    iget-object v8, p0, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v8, v0, v7}, Lepson/print/EPImageUtil;->rotateR90Image(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 185
    :cond_6
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v8

    invoke-virtual {v8}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "decode_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v10, p1, Lepson/print/EPImage;->index:I

    add-int/2addr v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, "_180.bmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 187
    iget-object v8, p0, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v8, v0, v7}, Lepson/print/EPImageUtil;->rotate180Image(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    if-eq p2, v6, :cond_7

    if-eq p2, v5, :cond_7

    if-eq p2, v4, :cond_7

    goto :goto_1

    .line 206
    :cond_7
    iget-object p2, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p2

    invoke-virtual {p2}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_8

    const-string p2, "createPrintImage"

    .line 207
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete decorded image : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p2, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    .line 214
    :cond_8
    :goto_1
    iput-object v7, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    goto :goto_2

    .line 136
    :pswitch_1
    iget-object v0, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    iget-object v4, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-static {v0, v4, p2}, Lepson/common/ImageUtil;->png2jpeg(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    .line 137
    invoke-virtual {p0}, Lepson/print/EPImageCreator;->stopRequested()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 138
    invoke-direct {p0}, Lepson/print/EPImageCreator;->deleteCancelFile()V

    return-object v1

    .line 141
    :cond_9
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v4

    invoke-virtual {v4}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    .line 143
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v4

    invoke-virtual {v4}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "decode_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p1, Lepson/print/EPImage;->index:I

    add-int/2addr v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ".bmp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    iget-object v4, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {v0, p2, v4, v3}, Lepson/print/EPImageUtil;->jpg2bmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p2

    if-eqz p2, :cond_b

    .line 147
    invoke-virtual {p0}, Lepson/print/EPImageCreator;->stopRequested()Z

    move-result p2

    if-eqz p2, :cond_a

    .line 148
    invoke-direct {p0}, Lepson/print/EPImageCreator;->deleteCancelFile()V

    return-object v1

    .line 151
    :cond_a
    iput-object v1, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 152
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-direct {p2}, Ljava/lang/IllegalStateException;-><init>()V

    throw p2

    .line 129
    :pswitch_2
    iget-object p2, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    iput-object p2, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 223
    :cond_b
    :goto_2
    new-instance p2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 224
    iput-boolean v3, p2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 225
    iget-object v0, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 226
    iget v0, p2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v0, :cond_c

    iget v0, p2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-lez v0, :cond_c

    .line 227
    iget v0, p2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v0, p1, Lepson/print/EPImage;->decodeWidth:I

    .line 228
    iget p2, p2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput p2, p1, Lepson/print/EPImage;->decodeHeight:I

    goto :goto_3

    .line 230
    :cond_c
    iget-object p2, p0, Lepson/print/EPImageCreator;->mEpsonImage:Lepson/image/epsonImage;

    if-nez p2, :cond_d

    .line 231
    new-instance p2, Lepson/image/epsonImage;

    invoke-direct {p2}, Lepson/image/epsonImage;-><init>()V

    iput-object p2, p0, Lepson/print/EPImageCreator;->mEpsonImage:Lepson/image/epsonImage;

    .line 234
    :cond_d
    iget-object p2, p0, Lepson/print/EPImageCreator;->mEpsonImage:Lepson/image/epsonImage;

    if-eqz p2, :cond_e

    const/4 p2, 0x2

    .line 235
    new-array p2, p2, [I

    .line 236
    iget-object v0, p0, Lepson/print/EPImageCreator;->mEpsonImage:Lepson/image/epsonImage;

    iget-object v4, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {v0, v4, p2}, Lepson/image/epsonImage;->epsmpGetImageSize2(Ljava/lang/String;[I)I

    .line 237
    aget v0, p2, v2

    iput v0, p1, Lepson/print/EPImage;->decodeWidth:I

    .line 238
    aget p2, p2, v3

    iput p2, p1, Lepson/print/EPImage;->decodeHeight:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p2

    .line 242
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    .line 243
    iput-object v1, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 247
    :cond_e
    :goto_3
    iget-object p1, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private convertRotationValue(I)I
    .locals 0

    .line 507
    div-int/lit8 p1, p1, 0x5a

    and-int/lit8 p1, p1, 0x3

    return p1
.end method

.method private declared-synchronized deleteCancelFile()V
    .locals 2

    monitor-enter p0

    .line 1526
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lepson/print/EPImageCreator;->CANCEL_FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "EPImageCreator"

    .line 1528
    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1530
    :goto_0
    monitor-exit p0

    return-void

    :goto_1
    monitor-exit p0

    throw v0
.end method

.method private getFontSize(I)I
    .locals 2

    const/4 v0, 0x4

    const/16 v1, 0x10

    if-eq p1, v0, :cond_2

    const/16 v0, 0x8

    if-eq p1, v0, :cond_1

    if-eq p1, v1, :cond_0

    const/16 v1, 0x28

    goto :goto_0

    :cond_0
    const/16 v1, 0x42

    goto :goto_0

    :cond_1
    const/16 v1, 0x21

    :cond_2
    :goto_0
    return v1
.end method

.method private getLayoutValues(ILepson/print/EPImage;D)[D
    .locals 6

    const/4 v0, 0x4

    .line 515
    new-array v0, v0, [D

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    if-ne p1, v3, :cond_0

    goto :goto_0

    .line 521
    :cond_0
    iget v4, p2, Lepson/print/EPImage;->previewImageRectBottom:F

    iget v5, p2, Lepson/print/EPImage;->previewImageRectTop:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double v4, v4, p3

    aput-wide v4, v0, v2

    .line 522
    iget v2, p2, Lepson/print/EPImage;->previewImageRectRight:F

    iget v4, p2, Lepson/print/EPImage;->previewImageRectLeft:F

    sub-float/2addr v2, v4

    float-to-double v4, v2

    mul-double v4, v4, p3

    aput-wide v4, v0, v1

    goto :goto_1

    .line 518
    :cond_1
    :goto_0
    iget v4, p2, Lepson/print/EPImage;->previewImageRectRight:F

    iget v5, p2, Lepson/print/EPImage;->previewImageRectLeft:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double v4, v4, p3

    aput-wide v4, v0, v2

    .line 519
    iget v2, p2, Lepson/print/EPImage;->previewImageRectBottom:F

    iget v4, p2, Lepson/print/EPImage;->previewImageRectTop:F

    sub-float/2addr v2, v4

    float-to-double v4, v2

    mul-double v4, v4, p3

    aput-wide v4, v0, v1

    :goto_1
    const/4 v1, 0x3

    packed-switch p1, :pswitch_data_0

    .line 545
    iget p1, p2, Lepson/print/EPImage;->previewImageRectLeft:F

    iget v2, p2, Lepson/print/EPImage;->previewPaperRectLeft:I

    int-to-float v2, v2

    sub-float/2addr p1, v2

    float-to-double v4, p1

    mul-double v4, v4, p3

    aput-wide v4, v0, v3

    .line 546
    iget p1, p2, Lepson/print/EPImage;->previewImageRectTop:F

    iget p2, p2, Lepson/print/EPImage;->previewPaperRectTop:I

    int-to-float p2, p2

    sub-float/2addr p1, p2

    float-to-double p1, p1

    mul-double p1, p1, p3

    aput-wide p1, v0, v1

    goto :goto_2

    .line 540
    :pswitch_0
    iget p1, p2, Lepson/print/EPImage;->previewImageRectTop:F

    iget v2, p2, Lepson/print/EPImage;->previewPaperRectTop:I

    int-to-float v2, v2

    sub-float/2addr p1, v2

    float-to-double v4, p1

    mul-double v4, v4, p3

    aput-wide v4, v0, v3

    .line 541
    iget p1, p2, Lepson/print/EPImage;->previewImageRectRight:F

    neg-float p1, p1

    iget p2, p2, Lepson/print/EPImage;->previewPaperRectRight:I

    int-to-float p2, p2

    add-float/2addr p1, p2

    float-to-double p1, p1

    mul-double p1, p1, p3

    aput-wide p1, v0, v1

    goto :goto_2

    .line 534
    :pswitch_1
    iget p1, p2, Lepson/print/EPImage;->previewImageRectRight:F

    neg-float p1, p1

    iget v2, p2, Lepson/print/EPImage;->previewPaperRectRight:I

    int-to-float v2, v2

    add-float/2addr p1, v2

    float-to-double v4, p1

    mul-double v4, v4, p3

    aput-wide v4, v0, v3

    .line 535
    iget p1, p2, Lepson/print/EPImage;->previewImageRectBottom:F

    neg-float p1, p1

    iget p2, p2, Lepson/print/EPImage;->previewPaperRectBottom:I

    int-to-float p2, p2

    add-float/2addr p1, p2

    float-to-double p1, p1

    mul-double p1, p1, p3

    aput-wide p1, v0, v1

    goto :goto_2

    .line 529
    :pswitch_2
    iget p1, p2, Lepson/print/EPImage;->previewImageRectBottom:F

    neg-float p1, p1

    iget v2, p2, Lepson/print/EPImage;->previewPaperRectBottom:I

    int-to-float v2, v2

    add-float/2addr p1, v2

    float-to-double v4, p1

    mul-double v4, v4, p3

    aput-wide v4, v0, v3

    .line 530
    iget p1, p2, Lepson/print/EPImage;->previewImageRectLeft:F

    iget p2, p2, Lepson/print/EPImage;->previewPaperRectLeft:I

    int-to-float p2, p2

    sub-float/2addr p1, p2

    float-to-double p1, p1

    mul-double p1, p1, p3

    aput-wide p1, v0, v1

    :goto_2
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getPrintAreaResolution(II)I
    .locals 2

    const/4 v0, 0x4

    const/4 v1, 0x1

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_1

    :goto_0
    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_1

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x8

    goto :goto_1

    :cond_0
    const/16 v0, 0x10

    :goto_1
    :pswitch_2
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getPrintAreaSize(Landroid/content/Context;III)[I
    .locals 1

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 563
    invoke-static {p1, p2}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object p1

    const/4 p2, 0x2

    .line 565
    new-array p2, p2, [I

    const/4 p3, 0x0

    .line 566
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width_boderless()I

    move-result p4

    aput p4, p2, p3

    .line 567
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height_boderless()I

    move-result p1

    aput p1, p2, v0

    return-object p2

    .line 572
    :cond_0
    new-instance p3, Lepson/print/phlayout/BorderedLayoutPosition;

    invoke-direct {p3}, Lepson/print/phlayout/BorderedLayoutPosition;-><init>()V

    invoke-virtual {p3, p1, p2, p4}, Lepson/print/phlayout/BorderedLayoutPosition;->getPrintSize(Landroid/content/Context;II)[I

    move-result-object p1

    return-object p1
.end method

.method private getPrintFilename(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .line 578
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ".bmp"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getPrintParPreviewRate(Lepson/print/EPImage;I[I)D
    .locals 2

    const/4 v0, 0x1

    and-int/2addr p2, v0

    if-ne p2, v0, :cond_0

    const/4 p2, 0x0

    .line 495
    aget p2, p3, p2

    goto :goto_0

    :cond_0
    aget p2, p3, v0

    :goto_0
    int-to-double p2, p2

    .line 499
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    iget p1, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    sub-int/2addr v0, p1

    int-to-double v0, v0

    div-double/2addr p2, v0

    return-wide p2
.end method

.method private declared-synchronized makeCancelFile()V
    .locals 4

    monitor-enter p0

    .line 1517
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lepson/print/EPImageCreator;->CANCEL_FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    const-string v0, "EPImageCreator"

    const-string v1, "EPImageCreator::makeCancelFile() create cancel file done."

    .line 1518
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "EPImageCreator"

    .line 1520
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error. can not create cancel_file. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1522
    :goto_0
    monitor-exit p0

    return-void

    :goto_1
    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public createJpegImage(Lepson/print/EPImage;I)I
    .locals 6

    .line 1263
    sget-object p2, Lepson/print/EPImageCreator;->lockObject:Ljava/lang/Object;

    monitor-enter p2

    .line 1265
    :try_start_0
    new-instance v0, Lepson/image/epsonImage;

    invoke-direct {v0}, Lepson/image/epsonImage;-><init>()V

    .line 1270
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    iget-object v4, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    const/16 v5, 0x2f

    .line 1271
    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1272
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const/16 v4, 0x2e

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".jpg"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1278
    iget-object v2, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lepson/image/epsonImage;->bmp2Jpg2(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1281
    iput-object v1, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1283
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    .line 1284
    iput-object v0, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    const/4 v0, -0x1

    .line 1288
    :goto_0
    monitor-exit p2

    return v0

    .line 1289
    :goto_1
    monitor-exit p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public createMultiPrintImage(Landroid/content/Context;IILepson/print/EPImageList;IIIIIIIZ)Ljava/lang/String;
    .locals 26

    move/from16 v13, p2

    move/from16 v14, p3

    move-object/from16 v15, p4

    move/from16 v12, p5

    move/from16 v11, p6

    move/from16 v10, p7

    move/from16 v9, p11

    .line 603
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    .line 606
    invoke-virtual {v15, v8}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0

    iget-boolean v7, v0, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 609
    new-instance v6, Lorg/opencv/core/Mat;

    sget v0, Lorg/opencv/core/CvType;->CV_8UC3:I

    const-wide v1, 0x406fe00000000000L    # 255.0

    invoke-static {v1, v2}, Lorg/opencv/core/Scalar;->all(D)Lorg/opencv/core/Scalar;

    move-result-object v1

    invoke-direct {v6, v10, v11, v0, v1}, Lorg/opencv/core/Mat;-><init>(IIILorg/opencv/core/Scalar;)V

    const/4 v0, 0x1

    if-le v11, v10, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    const/4 v1, 0x0

    const/high16 v4, 0x40000

    const/high16 v3, 0x20000

    const/high16 v2, 0x10000

    if-eq v13, v2, :cond_2

    if-eq v13, v3, :cond_1

    if-eq v13, v4, :cond_1

    move-object/from16 v16, v1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    mul-int/lit8 v1, v12, 0x4

    .line 623
    invoke-static {v11, v10, v14, v0}, Lepson/common/Info_paper;->getRect_4in1(IIIZ)Landroid/graphics/Rect;

    move-result-object v16

    goto :goto_1

    :cond_2
    mul-int/lit8 v1, v12, 0x2

    .line 618
    invoke-static {v11, v10, v14, v0}, Lepson/common/Info_paper;->getRect_2in1(IIIZ)Landroid/graphics/Rect;

    move-result-object v16

    .line 628
    :goto_1
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-le v0, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    .line 635
    :goto_2
    :try_start_0
    invoke-virtual {v15, v1}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_4

    move-object v9, v6

    move v14, v12

    goto/16 :goto_5

    .line 646
    :cond_4
    iget-boolean v3, v0, Lepson/print/EPImage;->isPaperLandScape:Z

    if-eq v3, v2, :cond_6

    if-eqz v5, :cond_5

    .line 648
    iget v3, v0, Lepson/print/EPImage;->rotate:I

    add-int/lit16 v3, v3, 0x10e

    iput v3, v0, Lepson/print/EPImage;->rotate:I

    goto :goto_3

    .line 650
    :cond_5
    iget v3, v0, Lepson/print/EPImage;->rotate:I

    add-int/lit8 v3, v3, 0x5a

    iput v3, v0, Lepson/print/EPImage;->rotate:I

    .line 658
    :cond_6
    :goto_3
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v18

    .line 659
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v19

    const/16 v20, 0x2

    const/16 v21, 0x0

    move-object v3, v0

    move-object/from16 v0, p0

    move/from16 v22, v1

    move-object/from16 v1, p1

    move/from16 v17, v2

    move-object v2, v3

    move/from16 v3, p3

    move/from16 v4, v18

    move/from16 v23, v5

    move/from16 v5, v19

    move-object/from16 v24, v6

    move/from16 v6, p8

    move/from16 v25, v7

    move/from16 v7, v20

    move/from16 v8, p2

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, v21

    move v14, v12

    move/from16 v12, p12

    .line 655
    invoke-virtual/range {v0 .. v12}, Lepson/print/EPImageCreator;->createPrintImage(Landroid/content/Context;Lepson/print/EPImage;IIIIIIIIZZ)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    move-object/from16 v9, v24

    goto/16 :goto_5

    :cond_7
    const/4 v1, 0x3

    const/high16 v2, 0x10000

    if-eq v13, v2, :cond_b

    const/high16 v3, 0x20000

    if-eq v13, v3, :cond_8

    const/high16 v4, 0x40000

    if-eq v13, v4, :cond_9

    move/from16 v8, v23

    move-object/from16 v9, v24

    move/from16 v6, v25

    const/4 v1, 0x0

    goto :goto_4

    :cond_8
    const/high16 v4, 0x40000

    .line 686
    :cond_9
    rem-int/lit8 v5, v22, 0x4

    move/from16 v8, v23

    move/from16 v6, v25

    .line 687
    invoke-static {v13, v8, v6, v5}, Lepson/print/EPImageCreator$MakeMultiPrintImage;->calcLayout4in1(IZZI)I

    move-result v7

    move-object/from16 v9, v24

    .line 686
    invoke-static {v9, v0, v7}, Lepson/print/EPImageCreator$MakeMultiPrintImage;->layoutPrintImage(Lorg/opencv/core/Mat;Ljava/lang/String;I)V

    if-ne v5, v1, :cond_a

    goto :goto_5

    :cond_a
    const/4 v1, 0x0

    goto :goto_4

    :cond_b
    move/from16 v8, v23

    move-object/from16 v9, v24

    move/from16 v6, v25

    const/high16 v3, 0x20000

    const/high16 v4, 0x40000

    .line 674
    rem-int/lit8 v5, v22, 0x2

    packed-switch v5, :pswitch_data_0

    const/4 v1, 0x0

    goto :goto_4

    .line 679
    :pswitch_0
    invoke-static {v9, v0, v1}, Lepson/print/EPImageCreator$MakeMultiPrintImage;->layoutPrintImage(Lorg/opencv/core/Mat;Ljava/lang/String;I)V

    goto :goto_5

    :pswitch_1
    const/4 v1, 0x0

    .line 676
    invoke-static {v9, v0, v1}, Lepson/print/EPImageCreator$MakeMultiPrintImage;->layoutPrintImage(Lorg/opencv/core/Mat;Ljava/lang/String;I)V

    :goto_4
    add-int/lit8 v0, v22, 0x1

    move v1, v0

    move v7, v6

    move v5, v8

    move-object v6, v9

    move v12, v14

    move/from16 v2, v17

    const/4 v8, 0x0

    move/from16 v9, p11

    move/from16 v10, p7

    move/from16 v11, p6

    move/from16 v14, p3

    goto/16 :goto_2

    :catchall_0
    move-object v9, v6

    move v14, v12

    goto :goto_5

    :catch_0
    move-object v9, v6

    move v14, v12

    .line 696
    :goto_5
    new-instance v0, Ljava/io/File;

    invoke-static/range {p1 .. p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "multi_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v14, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ".bmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 698
    invoke-static {v0, v9}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    .line 699
    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    move/from16 v1, p11

    .line 703
    rem-int/lit16 v2, v1, 0x168

    if-nez v2, :cond_c

    .line 704
    new-instance v1, Lepson/print/EPImage;

    invoke-direct {v1}, Lepson/print/EPImage;-><init>()V

    .line 705
    iput-object v0, v1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    goto :goto_6

    .line 707
    :cond_c
    new-instance v2, Lepson/print/EPImage;

    invoke-direct {v2, v0, v14}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    .line 708
    iput v1, v2, Lepson/print/EPImage;->rotate:I

    .line 709
    iput-object v0, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    move/from16 v1, p6

    move/from16 v3, p7

    move-object/from16 v0, p0

    .line 710
    invoke-virtual {v0, v2, v1, v3}, Lepson/print/EPImageCreator;->rotate(Lepson/print/EPImage;II)Ljava/lang/String;

    move-object v1, v2

    .line 713
    :goto_6
    iget-object v1, v1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public createMultiPrintImage_org(Landroid/content/Context;IILepson/print/EPImageList;IIIIIII)Ljava/lang/String;
    .locals 26

    move/from16 v0, p2

    move/from16 v12, p3

    move-object/from16 v13, p4

    move/from16 v14, p5

    move/from16 v15, p6

    move/from16 v11, p7

    move/from16 v10, p11

    .line 971
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    .line 974
    invoke-virtual {v13, v9}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v1

    iget-boolean v8, v1, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 977
    new-instance v7, Lorg/opencv/core/Mat;

    sget v1, Lorg/opencv/core/CvType;->CV_8UC3:I

    const-wide v2, 0x406fe00000000000L    # 255.0

    invoke-static {v2, v3}, Lorg/opencv/core/Scalar;->all(D)Lorg/opencv/core/Scalar;

    move-result-object v2

    invoke-direct {v7, v11, v15, v1, v2}, Lorg/opencv/core/Mat;-><init>(IIILorg/opencv/core/Scalar;)V

    const/4 v1, 0x1

    if-le v15, v11, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    const/4 v2, 0x0

    const/high16 v5, 0x40000

    const/high16 v4, 0x20000

    const/high16 v3, 0x10000

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_1

    if-eq v0, v5, :cond_1

    move-object/from16 v16, v2

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    mul-int/lit8 v2, v14, 0x4

    .line 991
    invoke-static {v15, v11, v12, v1}, Lepson/common/Info_paper;->getRect_4in1(IIIZ)Landroid/graphics/Rect;

    move-result-object v16

    goto :goto_1

    :cond_2
    mul-int/lit8 v2, v14, 0x2

    .line 986
    invoke-static {v15, v11, v12, v1}, Lepson/common/Info_paper;->getRect_2in1(IIIZ)Landroid/graphics/Rect;

    move-result-object v16

    .line 996
    :goto_1
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v1, v3, :cond_3

    const/4 v3, 0x1

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    .line 1003
    :goto_2
    :try_start_0
    invoke-virtual {v13, v2}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_4

    move-object v10, v7

    move v12, v11

    goto/16 :goto_5

    .line 1014
    :cond_4
    iget-boolean v4, v1, Lepson/print/EPImage;->isPaperLandScape:Z

    if-eq v4, v3, :cond_6

    if-eqz v6, :cond_5

    .line 1016
    iget v4, v1, Lepson/print/EPImage;->rotate:I

    add-int/lit16 v4, v4, 0x10e

    iput v4, v1, Lepson/print/EPImage;->rotate:I

    goto :goto_3

    .line 1018
    :cond_5
    iget v4, v1, Lepson/print/EPImage;->rotate:I

    add-int/lit8 v4, v4, 0x5a

    iput v4, v1, Lepson/print/EPImage;->rotate:I

    .line 1026
    :cond_6
    :goto_3
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v18

    .line 1027
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v19

    const/16 v20, 0x2

    const/16 v21, 0x0

    move-object v4, v1

    move-object/from16 v1, p0

    move/from16 v22, v2

    move-object/from16 v2, p1

    move/from16 v17, v3

    move-object v3, v4

    move/from16 v4, p3

    move/from16 v5, v18

    move/from16 v23, v6

    move/from16 v6, v19

    move-object/from16 v24, v7

    move/from16 v7, p8

    move/from16 v25, v8

    move/from16 v8, v20

    move/from16 v9, p9

    move/from16 v10, p10

    move v12, v11

    move/from16 v11, v21

    .line 1023
    invoke-virtual/range {v1 .. v11}, Lepson/print/EPImageCreator;->createPrintImage_origi(Landroid/content/Context;Lepson/print/EPImage;IIIIIIIZ)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    move-object/from16 v10, v24

    goto/16 :goto_5

    :cond_7
    const/4 v2, 0x3

    const/high16 v3, 0x10000

    if-eq v0, v3, :cond_b

    const/high16 v4, 0x20000

    if-eq v0, v4, :cond_8

    const/high16 v5, 0x40000

    if-eq v0, v5, :cond_9

    move/from16 v9, v23

    move-object/from16 v10, v24

    move/from16 v7, v25

    const/4 v2, 0x0

    goto :goto_4

    :cond_8
    const/high16 v5, 0x40000

    .line 1053
    :cond_9
    rem-int/lit8 v6, v22, 0x4

    move/from16 v9, v23

    move/from16 v7, v25

    .line 1054
    invoke-static {v0, v9, v7, v6}, Lepson/print/EPImageCreator$MakeMultiPrintImage;->calcLayout4in1(IZZI)I

    move-result v8

    move-object/from16 v10, v24

    .line 1053
    invoke-static {v10, v1, v8}, Lepson/print/EPImageCreator$MakeMultiPrintImage;->layoutPrintImage(Lorg/opencv/core/Mat;Ljava/lang/String;I)V

    if-ne v6, v2, :cond_a

    goto :goto_5

    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    :cond_b
    move/from16 v9, v23

    move-object/from16 v10, v24

    move/from16 v7, v25

    const/high16 v4, 0x20000

    const/high16 v5, 0x40000

    .line 1041
    rem-int/lit8 v6, v22, 0x2

    packed-switch v6, :pswitch_data_0

    const/4 v2, 0x0

    goto :goto_4

    .line 1046
    :pswitch_0
    invoke-static {v10, v1, v2}, Lepson/print/EPImageCreator$MakeMultiPrintImage;->layoutPrintImage(Lorg/opencv/core/Mat;Ljava/lang/String;I)V

    goto :goto_5

    :pswitch_1
    const/4 v2, 0x0

    .line 1043
    invoke-static {v10, v1, v2}, Lepson/print/EPImageCreator$MakeMultiPrintImage;->layoutPrintImage(Lorg/opencv/core/Mat;Ljava/lang/String;I)V

    :goto_4
    add-int/lit8 v1, v22, 0x1

    move v2, v1

    move v8, v7

    move v6, v9

    move-object v7, v10

    move v11, v12

    move/from16 v3, v17

    const/4 v9, 0x0

    move/from16 v10, p11

    move/from16 v12, p3

    goto/16 :goto_2

    :catchall_0
    move-object v10, v7

    move v12, v11

    goto :goto_5

    :catch_0
    move-object v10, v7

    move v12, v11

    .line 1063
    :goto_5
    new-instance v0, Ljava/io/File;

    invoke-static/range {p1 .. p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "multi_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v14, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ".bmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 1065
    invoke-static {v0, v10}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    .line 1066
    invoke-virtual {v10}, Lorg/opencv/core/Mat;->release()V

    move/from16 v1, p11

    .line 1070
    rem-int/lit16 v2, v1, 0x168

    if-nez v2, :cond_c

    .line 1071
    new-instance v1, Lepson/print/EPImage;

    invoke-direct {v1}, Lepson/print/EPImage;-><init>()V

    .line 1072
    iput-object v0, v1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    goto :goto_6

    .line 1074
    :cond_c
    new-instance v2, Lepson/print/EPImage;

    invoke-direct {v2, v0, v14}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    .line 1075
    iput v1, v2, Lepson/print/EPImage;->rotate:I

    .line 1076
    iput-object v0, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    .line 1077
    invoke-virtual {v0, v2, v15, v12}, Lepson/print/EPImageCreator;->rotate(Lepson/print/EPImage;II)Ljava/lang/String;

    move-object v1, v2

    .line 1080
    :goto_6
    iget-object v1, v1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public createPreviewImage(Lepson/print/EPImage;IIIZ)Ljava/lang/String;
    .locals 20

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v0, p2

    move/from16 v3, p3

    .line 325
    sget-object v4, Lepson/print/EPImageCreator;->lockObject:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    const-string v5, "EPImageCreator"

    const-string v6, "Lcok function by createPreviewImage()."

    .line 326
    invoke-static {v5, v6}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v5, 0x0

    if-nez p5, :cond_0

    .line 329
    :try_start_1
    iget-object v6, v2, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 330
    iget-object v0, v2, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0

    :catch_0
    move-exception v0

    goto/16 :goto_3

    :cond_0
    const/4 v6, 0x1

    if-le v0, v3, :cond_1

    .line 336
    :try_start_3
    iget v7, v2, Lepson/print/EPImage;->srcWidth:I

    if-le v7, v0, :cond_2

    .line 337
    iget v7, v2, Lepson/print/EPImage;->srcWidth:I

    div-int/2addr v7, v0

    goto :goto_0

    .line 340
    :cond_1
    iget v7, v2, Lepson/print/EPImage;->srcHeight:I

    if-le v7, v3, :cond_2

    .line 341
    iget v7, v2, Lepson/print/EPImage;->srcHeight:I

    div-int/2addr v7, v3

    goto :goto_0

    :cond_2
    const/4 v7, 0x1

    :goto_0
    if-ge v7, v6, :cond_3

    const/4 v7, 0x1

    .line 347
    :cond_3
    invoke-direct {v1, v2, v7}, Lepson/print/EPImageCreator;->LoadJpegFile(Lepson/print/EPImage;I)Ljava/lang/String;

    .line 348
    invoke-virtual/range {p0 .. p0}, Lepson/print/EPImageCreator;->stopRequested()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 349
    invoke-direct/range {p0 .. p0}, Lepson/print/EPImageCreator;->deleteCancelFile()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 350
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    return-object v5

    .line 352
    :cond_4
    :try_start_5
    iget-object v7, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    if-eqz v7, :cond_b

    .line 359
    iget v7, v2, Lepson/print/EPImage;->decodeWidth:I

    iget v8, v2, Lepson/print/EPImage;->decodeHeight:I

    if-le v7, v8, :cond_6

    .line 360
    iget v7, v2, Lepson/print/EPImage;->decodeWidth:I

    mul-int v7, v7, v3

    iget v8, v2, Lepson/print/EPImage;->decodeHeight:I

    div-int/2addr v7, v8

    if-ge v7, v0, :cond_5

    .line 362
    iget v3, v2, Lepson/print/EPImage;->decodeHeight:I

    mul-int v3, v3, v0

    iget v7, v2, Lepson/print/EPImage;->decodeWidth:I

    div-int/2addr v3, v7

    goto :goto_1

    :cond_5
    move v0, v7

    :goto_1
    move v15, v3

    move v3, v0

    move/from16 v0, p4

    goto :goto_2

    .line 367
    :cond_6
    iget v7, v2, Lepson/print/EPImage;->decodeHeight:I

    mul-int v7, v7, v0

    iget v8, v2, Lepson/print/EPImage;->decodeWidth:I

    div-int/2addr v7, v8

    if-ge v7, v3, :cond_7

    .line 369
    iget v0, v2, Lepson/print/EPImage;->decodeWidth:I

    mul-int v0, v0, v3

    iget v7, v2, Lepson/print/EPImage;->decodeHeight:I

    div-int/2addr v0, v7

    move v15, v3

    move v3, v0

    move/from16 v0, p4

    goto :goto_2

    :cond_7
    move v3, v0

    move v15, v7

    move/from16 v0, p4

    :goto_2
    if-ne v0, v6, :cond_9

    .line 377
    iget-object v0, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 378
    new-instance v7, Ljava/io/File;

    iget-object v8, v1, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v8

    invoke-virtual {v8}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "preview_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v10, v2, Lepson/print/EPImage;->index:I

    add-int/2addr v10, v6

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, "_gray.bmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 380
    iget-object v8, v1, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v8, v0, v7}, Lepson/print/EPImageUtil;->color2grayscale(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    iget-object v8, v1, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v8

    invoke-virtual {v8}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    const-string v8, "createPrintImage"

    .line 384
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Delete decorded image : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 386
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 389
    :cond_8
    iput-object v7, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 393
    :cond_9
    new-instance v0, Ljava/io/File;

    iget-object v7, v1, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v7

    invoke-virtual {v7}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "preview_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v9, v2, Lepson/print/EPImage;->index:I

    add-int/2addr v9, v6

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, ".bmp"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 395
    iget-object v7, v1, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    iget-object v8, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iget-object v9, v2, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    iget v0, v2, Lepson/print/EPImage;->decodeWidth:I

    add-int/lit8 v14, v0, -0x1

    iget v0, v2, Lepson/print/EPImage;->decodeHeight:I

    sub-int/2addr v0, v6

    const/16 v16, 0x0

    const/16 v17, 0x0

    add-int/lit8 v18, v3, -0x1

    add-int/lit8 v19, v15, -0x1

    move v10, v3

    move v11, v15

    move v6, v15

    move v15, v0

    invoke-virtual/range {v7 .. v19}, Lepson/print/EPImageUtil;->resizeImage(Ljava/lang/String;Ljava/lang/String;IIIIIIIIII)I

    .line 401
    iget-object v0, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iget-object v7, v1, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v7

    invoke-virtual {v7}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "createPrintImage"

    .line 402
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Delete decorded image : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    new-instance v0, Ljava/io/File;

    iget-object v7, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 404
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 405
    iput-object v5, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 408
    :cond_a
    iput v3, v2, Lepson/print/EPImage;->previewWidth:I

    .line 409
    iput v6, v2, Lepson/print/EPImage;->previewHeight:I

    goto :goto_4

    .line 353
    :cond_b
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 411
    :goto_3
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 412
    iput-object v5, v2, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 415
    :goto_4
    iget-object v0, v2, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    monitor-exit v4

    return-object v0

    :catchall_0
    move-exception v0

    .line 416
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0
.end method

.method public createPrintImage(Landroid/content/Context;Lepson/print/EPImage;IIIIIIIIZZ)Ljava/lang/String;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p7

    move/from16 v3, p10

    .line 451
    iget v4, v1, Lepson/print/EPImage;->rotate:I

    invoke-direct {v0, v4}, Lepson/print/EPImageCreator;->convertRotationValue(I)I

    move-result v14

    const/4 v4, 0x2

    const/4 v15, 0x1

    const/4 v12, 0x0

    const/4 v5, 0x4

    if-ne v2, v5, :cond_0

    .line 454
    new-array v5, v4, [I

    aput p4, v5, v12

    aput p5, v5, v15

    goto :goto_0

    .line 456
    :cond_0
    iget-object v5, v0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    move/from16 v6, p6

    move/from16 v7, p8

    invoke-direct {v0, v5, v6, v2, v7}, Lepson/print/EPImageCreator;->getPrintAreaSize(Landroid/content/Context;III)[I

    move-result-object v5

    if-eqz p12, :cond_1

    .line 458
    aget v6, v5, v12

    .line 459
    aget v7, v5, v15

    aput v7, v5, v12

    .line 460
    aput v6, v5, v15

    .line 464
    :cond_1
    :goto_0
    invoke-direct {v0, v1, v14, v5}, Lepson/print/EPImageCreator;->getPrintParPreviewRate(Lepson/print/EPImage;I[I)D

    move-result-wide v6

    .line 466
    new-instance v13, Lcom/epson/cameracopy/printlayout/ImageAndLayout;

    invoke-direct {v13}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;-><init>()V

    .line 467
    aget v8, v5, v12

    aget v5, v5, v15

    invoke-virtual {v13, v8, v5}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setLayoutAreaSize(II)V

    .line 469
    invoke-direct {v0, v14, v1, v6, v7}, Lepson/print/EPImageCreator;->getLayoutValues(ILepson/print/EPImage;D)[D

    move-result-object v5

    .line 470
    aget-wide v6, v5, v12

    aget-wide v8, v5, v15

    aget-wide v10, v5, v4

    const/16 v16, 0x3

    aget-wide v16, v5, v16

    move-object v5, v13

    move-object v4, v13

    const/4 v15, 0x0

    move-wide/from16 v12, v16

    invoke-virtual/range {v5 .. v14}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setLayout(DDDDI)V

    .line 472
    iget-object v5, v1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setOrgFileName(Ljava/lang/String;)V

    .line 473
    iget-object v5, v0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    iget v6, v1, Lepson/print/EPImage;->index:I

    invoke-direct {v0, v5, v6}, Lepson/print/EPImageCreator;->getPrintFilename(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 475
    invoke-virtual {v4, v15}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setOpenCvExifRotationCancel(Z)V

    move/from16 v6, p9

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    const/4 v8, 0x2

    .line 476
    new-array v8, v8, [I

    aput p4, v8, v15

    aput p5, v8, v7

    invoke-virtual {v4, v5, v6, v8, v15}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->createPrintData(Ljava/lang/String;Z[II)Z

    .line 482
    iput-object v5, v1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 486
    invoke-virtual/range {p2 .. p2}, Lepson/print/EPImage;->getOriginalFileName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lepson/print/Util/Photo;->getDateTimeSystem(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    if-ne v2, v7, :cond_3

    goto :goto_2

    :cond_3
    const/4 v7, 0x0

    :goto_2
    move-object/from16 p6, p0

    move-object/from16 p7, p2

    move/from16 p8, p4

    move/from16 p9, p5

    move-object/from16 p10, v3

    move/from16 p11, p3

    move/from16 p12, v7

    .line 487
    invoke-virtual/range {p6 .. p12}, Lepson/print/EPImageCreator;->writeDate(Lepson/print/EPImage;IILjava/lang/String;IZ)Ljava/lang/String;

    :cond_4
    return-object v5
.end method

.method public createPrintImage_origi(Landroid/content/Context;Lepson/print/EPImage;IIIIIIIZ)Ljava/lang/String;
    .locals 26

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v1, p7

    move/from16 v2, p9

    .line 746
    sget-object v23, Lepson/print/EPImageCreator;->lockObject:Ljava/lang/Object;

    monitor-enter v23

    :try_start_0
    const-string v5, "EPImageCreator"

    const-string v6, "Lcok function by createPrintImage()."

    .line 747
    invoke-static {v5, v6}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v7, 0x0

    const/4 v5, 0x1

    if-le v3, v4, :cond_0

    .line 753
    :try_start_1
    iget v6, v9, Lepson/print/EPImage;->srcWidth:I

    if-le v6, v3, :cond_1

    .line 754
    iget v6, v9, Lepson/print/EPImage;->srcWidth:I

    div-int/2addr v6, v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v10, v7

    goto/16 :goto_a

    .line 757
    :cond_0
    :try_start_2
    iget v6, v9, Lepson/print/EPImage;->srcHeight:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-le v6, v4, :cond_1

    .line 758
    :try_start_3
    iget v6, v9, Lepson/print/EPImage;->srcHeight:I

    div-int/2addr v6, v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :cond_1
    const/4 v6, 0x1

    :goto_0
    if-ge v6, v5, :cond_2

    const/4 v6, 0x1

    .line 764
    :cond_2
    :try_start_4
    invoke-direct {v8, v9, v6}, Lepson/print/EPImageCreator;->LoadJpegFile(Lepson/print/EPImage;I)Ljava/lang/String;

    .line 765
    invoke-virtual/range {p0 .. p0}, Lepson/print/EPImageCreator;->stopRequested()Z

    move-result v6
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v6, :cond_3

    .line 766
    :try_start_5
    invoke-direct/range {p0 .. p0}, Lepson/print/EPImageCreator;->deleteCancelFile()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 767
    :try_start_6
    monitor-exit v23
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    return-object v7

    .line 769
    :cond_3
    :try_start_7
    iget-object v6, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    if-eqz v6, :cond_13

    .line 773
    iget v6, v9, Lepson/print/EPImage;->decodeWidth:I

    int-to-float v6, v6

    iget v10, v9, Lepson/print/EPImage;->previewWidth:I

    int-to-float v10, v10

    div-float/2addr v6, v10

    .line 774
    iget v10, v9, Lepson/print/EPImage;->decodeHeight:I

    int-to-float v10, v10

    iget v11, v9, Lepson/print/EPImage;->previewHeight:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    .line 775
    invoke-static {v6, v10}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 776
    iget-object v10, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v11, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v12, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    const/4 v15, 0x0

    iput v15, v13, Landroid/graphics/Rect;->bottom:I

    iput v15, v12, Landroid/graphics/Rect;->right:I

    iput v15, v11, Landroid/graphics/Rect;->top:I

    iput v15, v10, Landroid/graphics/Rect;->left:I

    .line 777
    iget-object v10, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    iget-object v11, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    iget-object v12, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    iget-object v13, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    iput v15, v13, Landroid/graphics/Rect;->bottom:I

    iput v15, v12, Landroid/graphics/Rect;->right:I

    iput v15, v11, Landroid/graphics/Rect;->top:I

    iput v15, v10, Landroid/graphics/Rect;->left:I

    .line 787
    iget-object v10, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v11, v9, Lepson/print/EPImage;->previewPaperRectLeft:I

    int-to-float v11, v11

    iget v12, v9, Lepson/print/EPImage;->previewImageRectLeft:F

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->left:I

    .line 788
    iget-object v10, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v11, v9, Lepson/print/EPImage;->previewPaperRectTop:I

    int-to-float v11, v11

    iget v12, v9, Lepson/print/EPImage;->previewImageRectTop:F

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->top:I

    .line 791
    iget-object v10, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-gez v10, :cond_4

    .line 792
    :try_start_8
    iget-object v10, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    neg-int v10, v10

    .line 793
    iget-object v11, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iput v15, v11, Landroid/graphics/Rect;->left:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    :cond_4
    const/4 v10, 0x0

    .line 795
    :goto_1
    :try_start_9
    iget-object v11, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-gez v11, :cond_5

    .line 796
    :try_start_a
    iget-object v11, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    neg-int v11, v11

    .line 797
    iget-object v12, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iput v15, v12, Landroid/graphics/Rect;->top:I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_2

    :cond_5
    const/4 v11, 0x0

    .line 801
    :goto_2
    :try_start_b
    iget v12, v9, Lepson/print/EPImage;->previewPaperRectRight:I

    int-to-float v12, v12

    iget v13, v9, Lepson/print/EPImage;->previewImageRectRight:F
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    cmpg-float v12, v12, v13

    if-gtz v12, :cond_6

    .line 802
    :try_start_c
    iget-object v12, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v9, Lepson/print/EPImage;->previewPaperRectRight:I

    int-to-float v13, v13

    iget v14, v9, Lepson/print/EPImage;->previewImageRectLeft:F

    sub-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->right:I
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    const/4 v12, 0x0

    goto :goto_3

    .line 804
    :cond_6
    :try_start_d
    iget-object v12, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v9, Lepson/print/EPImage;->previewImageRectRight:F

    iget v14, v9, Lepson/print/EPImage;->previewImageRectLeft:F

    sub-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->right:I

    .line 805
    iget v12, v9, Lepson/print/EPImage;->previewPaperRectRight:I

    int-to-float v12, v12

    iget v13, v9, Lepson/print/EPImage;->previewImageRectRight:F

    sub-float/2addr v12, v13

    float-to-int v12, v12

    .line 807
    :goto_3
    iget v13, v9, Lepson/print/EPImage;->previewPaperRectBottom:I

    int-to-float v13, v13

    iget v14, v9, Lepson/print/EPImage;->previewImageRectBottom:F
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    cmpg-float v13, v13, v14

    if-gtz v13, :cond_7

    .line 808
    :try_start_e
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v9, Lepson/print/EPImage;->previewPaperRectBottom:I
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    int-to-float v14, v14

    :try_start_f
    iget v7, v9, Lepson/print/EPImage;->previewImageRectTop:F

    sub-float/2addr v14, v7

    float-to-int v7, v14

    iput v7, v13, Landroid/graphics/Rect;->bottom:I
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    const/4 v7, 0x0

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v1, v0

    const/4 v10, 0x0

    goto/16 :goto_a

    .line 810
    :cond_7
    :try_start_10
    iget-object v7, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v9, Lepson/print/EPImage;->previewImageRectBottom:F

    iget v14, v9, Lepson/print/EPImage;->previewImageRectTop:F

    sub-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v7, Landroid/graphics/Rect;->bottom:I

    .line 811
    iget v7, v9, Lepson/print/EPImage;->previewPaperRectBottom:I

    int-to-float v7, v7

    iget v13, v9, Lepson/print/EPImage;->previewImageRectBottom:F

    sub-float/2addr v7, v13

    float-to-int v7, v7

    .line 816
    :goto_4
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v14, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->left:I

    int-to-float v14, v14

    iget v15, v9, Lepson/print/EPImage;->scaleFactor:F

    div-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->left:I

    .line 817
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v14, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    int-to-float v14, v14

    iget v15, v9, Lepson/print/EPImage;->scaleFactor:F

    div-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->top:I

    .line 818
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v14, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->right:I

    int-to-float v14, v14

    iget v15, v9, Lepson/print/EPImage;->scaleFactor:F

    div-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->right:I

    .line 819
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v14, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    int-to-float v14, v14

    iget v15, v9, Lepson/print/EPImage;->scaleFactor:F

    div-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->bottom:I

    .line 822
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v14, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->left:I

    int-to-float v14, v14

    mul-float v14, v14, v6

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->left:I

    .line 823
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v14, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    int-to-float v14, v14

    mul-float v14, v14, v6

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->top:I

    .line 824
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v14, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->right:I

    int-to-float v14, v14

    mul-float v14, v14, v6

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->right:I

    .line 825
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v14, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    int-to-float v14, v14

    mul-float v14, v14, v6

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->bottom:I

    .line 828
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    iget v14, v9, Lepson/print/EPImage;->decodeWidth:I
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    sub-int/2addr v14, v5

    const/high16 v15, 0x40000000    # 2.0f

    if-le v13, v14, :cond_8

    .line 829
    :try_start_11
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    iget v14, v9, Lepson/print/EPImage;->decodeWidth:I

    sub-int/2addr v13, v14

    add-int/2addr v13, v5

    int-to-float v13, v13

    div-float/2addr v13, v15

    div-float/2addr v13, v6

    float-to-int v13, v13

    add-int/2addr v10, v13

    add-int/2addr v12, v13

    .line 832
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v9, Lepson/print/EPImage;->decodeWidth:I

    sub-int/2addr v14, v5

    iput v14, v13, Landroid/graphics/Rect;->right:I
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_1
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 834
    :cond_8
    :try_start_12
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    iget v14, v9, Lepson/print/EPImage;->decodeHeight:I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_2
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    sub-int/2addr v14, v5

    if-le v13, v14, :cond_9

    .line 835
    :try_start_13
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    iget v14, v9, Lepson/print/EPImage;->decodeHeight:I

    sub-int/2addr v13, v14

    add-int/2addr v13, v5

    int-to-float v13, v13

    div-float/2addr v13, v15

    div-float/2addr v13, v6

    float-to-int v6, v13

    add-int/2addr v11, v6

    add-int/2addr v7, v6

    .line 838
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v9, Lepson/print/EPImage;->decodeHeight:I

    sub-int/2addr v13, v5

    iput v13, v6, Landroid/graphics/Rect;->bottom:I
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_1
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_9
    int-to-float v6, v3

    .line 842
    :try_start_14
    iget v13, v9, Lepson/print/EPImage;->previewPaperRectRight:I

    iget v14, v9, Lepson/print/EPImage;->previewPaperRectLeft:I

    sub-int/2addr v13, v14

    add-int/2addr v13, v5

    int-to-float v13, v13

    div-float/2addr v6, v13

    .line 843
    iget v13, v9, Lepson/print/EPImage;->rotate:I

    rem-int/lit16 v13, v13, 0x168
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_2
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    const/16 v14, 0x5a

    if-eq v13, v14, :cond_b

    :try_start_15
    iget v13, v9, Lepson/print/EPImage;->rotate:I

    rem-int/lit16 v13, v13, 0x168
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_1
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    const/16 v14, 0x10e

    if-ne v13, v14, :cond_a

    goto :goto_5

    :cond_a
    move/from16 v25, v12

    move v12, v7

    move/from16 v7, v25

    goto :goto_6

    :cond_b
    :goto_5
    int-to-float v6, v4

    .line 851
    :try_start_16
    iget v13, v9, Lepson/print/EPImage;->previewPaperRectRight:I

    iget v14, v9, Lepson/print/EPImage;->previewPaperRectLeft:I

    sub-int/2addr v13, v14

    add-int/2addr v13, v5

    int-to-float v13, v13

    div-float/2addr v6, v13

    move/from16 v25, v11

    move v11, v10

    move/from16 v10, v25

    .line 854
    :goto_6
    iget-object v13, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    int-to-float v10, v10

    mul-float v10, v10, v6

    float-to-int v10, v10

    iput v10, v13, Landroid/graphics/Rect;->left:I

    .line 855
    iget-object v10, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    int-to-float v11, v11

    mul-float v11, v11, v6

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->top:I

    .line 856
    iget-object v10, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    add-int/lit8 v11, v3, -0x1

    int-to-float v11, v11

    int-to-float v7, v7

    mul-float v7, v7, v6

    sub-float/2addr v11, v7

    float-to-int v7, v11

    iput v7, v10, Landroid/graphics/Rect;->right:I

    .line 857
    iget-object v7, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    int-to-float v11, v12

    mul-float v11, v11, v6

    sub-float/2addr v10, v11

    float-to-int v6, v10

    iput v6, v7, Landroid/graphics/Rect;->bottom:I
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_2
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    if-ne v1, v5, :cond_d

    .line 860
    :try_start_17
    iget-object v6, v8, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    move/from16 v7, p6

    invoke-static {v6, v7}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object v6

    .line 861
    invoke-virtual {v6}, Lepson/common/Info_paper;->getTopMargin()I

    move-result v7

    neg-int v7, v7

    .line 862
    invoke-virtual {v6}, Lepson/common/Info_paper;->getLeftMargin()I

    move-result v10

    neg-int v10, v10

    .line 863
    invoke-virtual {v6}, Lepson/common/Info_paper;->getBottomMargin()I

    move-result v11

    neg-int v11, v11

    .line 864
    invoke-virtual {v6}, Lepson/common/Info_paper;->getRightMargin()I

    move-result v12

    neg-int v12, v12

    if-eqz p10, :cond_c

    .line 868
    iget-object v13, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v13

    int-to-float v13, v13

    invoke-virtual {v6}, Lepson/common/Info_paper;->getPaper_width_boderless()I

    move-result v6

    sub-int/2addr v6, v5

    int-to-float v6, v6

    div-float/2addr v13, v6

    goto :goto_7

    .line 870
    :cond_c
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    iget-object v13, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v13

    int-to-float v13, v13

    div-float v13, v6, v13

    :goto_7
    int-to-float v6, v7

    mul-float v6, v6, v13

    float-to-int v6, v6

    int-to-float v7, v10

    mul-float v7, v7, v13

    float-to-int v7, v7

    int-to-float v10, v11

    mul-float v10, v10, v13

    float-to-int v10, v10

    int-to-float v11, v12

    mul-float v11, v11, v13

    float-to-int v11, v11

    .line 876
    iget-object v12, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v12, Landroid/graphics/Rect;->top:I

    sub-int/2addr v13, v6

    iput v13, v12, Landroid/graphics/Rect;->top:I

    .line 877
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v12, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v12, v7

    iput v12, v6, Landroid/graphics/Rect;->left:I

    .line 878
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v7, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v10

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    .line 879
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v7, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v11

    iput v7, v6, Landroid/graphics/Rect;->right:I
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_1
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 882
    :cond_d
    :try_start_18
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v7, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget v10, v9, Lepson/print/EPImage;->decodeWidth:I

    sub-int/2addr v10, v5

    invoke-static {v7, v10}, Ljava/lang/Math;->min(II)I

    move-result v7

    const/4 v10, 0x0

    invoke-static {v10, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->left:I

    .line 883
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v7, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    iget v10, v9, Lepson/print/EPImage;->decodeHeight:I

    sub-int/2addr v10, v5

    invoke-static {v7, v10}, Ljava/lang/Math;->min(II)I

    move-result v7

    const/4 v10, 0x0

    invoke-static {v10, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->top:I

    .line 884
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v7, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget v10, v9, Lepson/print/EPImage;->decodeWidth:I

    sub-int/2addr v10, v5

    invoke-static {v7, v10}, Ljava/lang/Math;->min(II)I

    move-result v7

    const/4 v10, 0x0

    invoke-static {v10, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->right:I

    .line 885
    iget-object v6, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget-object v7, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    iget v10, v9, Lepson/print/EPImage;->decodeHeight:I

    sub-int/2addr v10, v5

    invoke-static {v7, v10}, Ljava/lang/Math;->min(II)I

    move-result v7

    const/4 v15, 0x0

    invoke-static {v15, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    .line 888
    iget-object v6, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 891
    invoke-virtual {v8, v9, v3, v4}, Lepson/print/EPImageCreator;->rotate(Lepson/print/EPImage;II)Ljava/lang/String;

    .line 894
    iget v7, v9, Lepson/print/EPImage;->rotate:I
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_2
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    if-eqz v7, :cond_e

    :try_start_19
    iget-object v7, v8, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    .line 895
    invoke-static {v7}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v7

    invoke-virtual {v7}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v7

    .line 894
    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_e

    const-string v7, "createPrintImage"

    .line 896
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Delete decorded image : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 898
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    :cond_e
    if-nez p10, :cond_10

    move/from16 v6, p8

    if-ne v6, v5, :cond_10

    .line 903
    iget-object v6, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 904
    new-instance v7, Ljava/io/File;

    iget-object v10, v8, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v10

    invoke-virtual {v10}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "print_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v12, v9, Lepson/print/EPImage;->index:I

    add-int/2addr v12, v5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, "_gray.bmp"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v7, v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 906
    iget-object v10, v8, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v10, v6, v7}, Lepson/print/EPImageUtil;->color2grayscale(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    iget-object v10, v8, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v10

    invoke-virtual {v10}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    const-string v10, "createPrintImage"

    .line 910
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Delete decorded image : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 912
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 915
    :cond_f
    iput-object v7, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_1
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 919
    :cond_10
    :try_start_1a
    new-instance v6, Ljava/io/File;

    iget-object v7, v8, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v7

    invoke-virtual {v7}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v7

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget v11, v9, Lepson/print/EPImage;->index:I

    add-int/2addr v11, v5

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v11, ".bmp"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v7, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 921
    iget-object v10, v8, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    iget-object v11, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iget-object v7, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v12, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v14, v12, Landroid/graphics/Rect;->top:I

    iget-object v12, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v13, v12, Landroid/graphics/Rect;->right:I

    iget-object v12, v8, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->bottom:I

    iget-object v15, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->left:I

    iget-object v5, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v3, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, v8, Lepson/print/EPImageCreator;->dst:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v12

    move-object v12, v6

    move/from16 v17, v13

    move/from16 v13, p4

    move/from16 v19, v14

    move/from16 v14, p5

    move/from16 v20, v15

    const/16 v24, 0x0

    move v15, v7

    move/from16 v16, v19

    move/from16 v19, v20

    move/from16 v20, v5

    move/from16 v21, v3

    move/from16 v22, v4

    invoke-virtual/range {v10 .. v22}, Lepson/print/EPImageUtil;->resizeImage(Ljava/lang/String;Ljava/lang/String;IIIIIIIIII)I

    .line 927
    iget-object v3, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iget-object v4, v8, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v4

    invoke-virtual {v4}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_2
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    if-eqz v3, :cond_11

    :try_start_1b
    const-string v3, "createPrintImage"

    .line 928
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete decorded image : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    new-instance v3, Ljava/io/File;

    iget-object v4, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 930
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_1
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    .line 933
    :cond_11
    :try_start_1c
    iput-object v6, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    if-eqz v2, :cond_14

    .line 937
    invoke-virtual/range {p2 .. p2}, Lepson/print/EPImage;->getOriginalFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lepson/print/Util/Photo;->getDateTimeSystem(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_2
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_12

    const/4 v7, 0x1

    goto :goto_8

    :cond_12
    const/4 v7, 0x0

    :goto_8
    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v6, p3

    const/4 v10, 0x0

    .line 938
    :try_start_1d
    invoke-virtual/range {v1 .. v7}, Lepson/print/EPImageCreator;->writeDate(Lepson/print/EPImage;IILjava/lang/String;IZ)Ljava/lang/String;

    goto :goto_b

    :catch_2
    move-exception v0

    const/4 v10, 0x0

    goto :goto_9

    :cond_13
    move-object v10, v7

    .line 770
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_3
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    :catch_3
    move-exception v0

    goto :goto_9

    :catch_4
    move-exception v0

    move-object v10, v7

    :goto_9
    move-object v1, v0

    .line 943
    :goto_a
    :try_start_1e
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 944
    iput-object v10, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 947
    :cond_14
    :goto_b
    iget-object v1, v9, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    monitor-exit v23

    return-object v1

    :catchall_0
    move-exception v0

    move-object v1, v0

    .line 948
    monitor-exit v23
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    throw v1
.end method

.method public createThumbnailImage(Lepson/print/EPImage;II)Ljava/lang/String;
    .locals 20

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v0, p2

    move/from16 v3, p3

    .line 251
    sget-object v4, Lepson/print/EPImageCreator;->lockObject:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    const-string v5, "EPImageCreator"

    const-string v6, "Lcok function by createThumbnailImage()."

    .line 252
    invoke-static {v5, v6}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v5, 0x0

    .line 255
    :try_start_1
    iget-object v6, v2, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 256
    iget-object v0, v2, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0

    :cond_0
    const/4 v6, 0x1

    if-le v0, v3, :cond_1

    .line 262
    :try_start_3
    iget v7, v2, Lepson/print/EPImage;->srcWidth:I

    if-le v7, v0, :cond_2

    .line 263
    iget v7, v2, Lepson/print/EPImage;->srcWidth:I

    div-int/2addr v7, v0

    goto :goto_0

    .line 266
    :cond_1
    iget v7, v2, Lepson/print/EPImage;->srcHeight:I

    if-le v7, v3, :cond_2

    .line 267
    iget v7, v2, Lepson/print/EPImage;->srcHeight:I

    div-int/2addr v7, v3

    goto :goto_0

    :cond_2
    const/4 v7, 0x1

    :goto_0
    if-ge v7, v6, :cond_3

    const/4 v7, 0x1

    .line 273
    :cond_3
    invoke-direct {v1, v2, v7}, Lepson/print/EPImageCreator;->LoadJpegFile(Lepson/print/EPImage;I)Ljava/lang/String;

    .line 274
    invoke-virtual/range {p0 .. p0}, Lepson/print/EPImageCreator;->stopRequested()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 275
    invoke-direct/range {p0 .. p0}, Lepson/print/EPImageCreator;->deleteCancelFile()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 276
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    return-object v5

    .line 278
    :cond_4
    :try_start_5
    iget-object v7, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    if-eqz v7, :cond_8

    .line 283
    iget v7, v2, Lepson/print/EPImage;->decodeWidth:I

    iget v8, v2, Lepson/print/EPImage;->decodeHeight:I

    if-le v7, v8, :cond_6

    .line 284
    iget v7, v2, Lepson/print/EPImage;->decodeWidth:I

    mul-int v7, v7, v3

    iget v8, v2, Lepson/print/EPImage;->decodeHeight:I

    div-int/2addr v7, v8

    if-ge v7, v0, :cond_5

    .line 286
    iget v3, v2, Lepson/print/EPImage;->decodeHeight:I

    mul-int v3, v3, v0

    iget v7, v2, Lepson/print/EPImage;->decodeWidth:I

    div-int/2addr v3, v7

    goto :goto_1

    :cond_5
    move v0, v7

    :goto_1
    move v10, v0

    move v11, v3

    goto :goto_2

    .line 291
    :cond_6
    iget v7, v2, Lepson/print/EPImage;->decodeHeight:I

    mul-int v7, v7, v0

    iget v8, v2, Lepson/print/EPImage;->decodeWidth:I

    div-int/2addr v7, v8

    if-ge v7, v3, :cond_7

    .line 293
    iget v0, v2, Lepson/print/EPImage;->decodeWidth:I

    mul-int v0, v0, v3

    iget v7, v2, Lepson/print/EPImage;->decodeHeight:I

    div-int/2addr v0, v7

    move v10, v0

    move v11, v3

    goto :goto_2

    :cond_7
    move v10, v0

    move v11, v7

    .line 300
    :goto_2
    new-instance v0, Ljava/io/File;

    iget-object v3, v1, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thumbnail_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v8, v2, Lepson/print/EPImage;->index:I

    add-int/2addr v8, v6

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, ".bmp"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    .line 302
    iget-object v7, v1, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    iget-object v8, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iget-object v9, v2, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    iget v0, v2, Lepson/print/EPImage;->decodeWidth:I

    add-int/lit8 v14, v0, -0x1

    iget v0, v2, Lepson/print/EPImage;->decodeHeight:I

    add-int/lit8 v15, v0, -0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    add-int/lit8 v18, v10, -0x1

    add-int/lit8 v19, v11, -0x1

    invoke-virtual/range {v7 .. v19}, Lepson/print/EPImageUtil;->resizeImage(Ljava/lang/String;Ljava/lang/String;IIIIIIIIII)I

    .line 308
    iget-object v0, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iget-object v3, v1, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "createPrintImage"

    .line 309
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Delete decorded image : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    new-instance v0, Ljava/io/File;

    iget-object v3, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 311
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 312
    iput-object v5, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    goto :goto_3

    .line 279
    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_0
    move-exception v0

    .line 316
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 317
    iput-object v5, v2, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    .line 320
    :cond_9
    :goto_3
    iget-object v0, v2, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    monitor-exit v4

    return-object v0

    :catchall_0
    move-exception v0

    .line 321
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 74
    invoke-direct {p0}, Lepson/print/EPImageCreator;->deleteCancelFile()V

    return-void
.end method

.method public declared-synchronized requestStop()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    .line 78
    :try_start_0
    iput-boolean v0, p0, Lepson/print/EPImageCreator;->mStopREquested:Z

    .line 79
    invoke-direct {p0}, Lepson/print/EPImageCreator;->makeCancelFile()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method rotate(Lepson/print/EPImage;II)Ljava/lang/String;
    .locals 4

    .line 1305
    iget-object p2, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 1306
    new-instance p3, Ljava/io/File;

    iget-object v0, p0, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rotate_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Lepson/print/EPImage;->rotate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Lepson/print/EPImage;->index:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ".bmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p3, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p3

    .line 1310
    iget v0, p1, Lepson/print/EPImage;->rotate:I

    rem-int/lit16 v0, v0, 0x168

    iput v0, p1, Lepson/print/EPImage;->rotate:I

    .line 1311
    iget v0, p1, Lepson/print/EPImage;->rotate:I

    if-nez v0, :cond_0

    .line 1313
    iget-object p1, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    return-object p1

    .line 1317
    :cond_0
    iget v0, p1, Lepson/print/EPImage;->rotate:I

    const/16 v1, 0x10e

    const/16 v2, 0x5a

    if-eq v0, v2, :cond_1

    iget v0, p1, Lepson/print/EPImage;->rotate:I

    if-ne v0, v1, :cond_2

    .line 1318
    :cond_1
    iget v0, p1, Lepson/print/EPImage;->decodeWidth:I

    .line 1319
    iget v3, p1, Lepson/print/EPImage;->decodeHeight:I

    iput v3, p1, Lepson/print/EPImage;->decodeWidth:I

    .line 1320
    iput v0, p1, Lepson/print/EPImage;->decodeHeight:I

    .line 1322
    iget v0, p1, Lepson/print/EPImage;->previewWidth:I

    .line 1323
    iget v3, p1, Lepson/print/EPImage;->previewHeight:I

    iput v3, p1, Lepson/print/EPImage;->previewWidth:I

    .line 1324
    iput v0, p1, Lepson/print/EPImage;->previewHeight:I

    .line 1328
    :cond_2
    iget v0, p1, Lepson/print/EPImage;->rotate:I

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb4

    if-eq v0, v2, :cond_4

    if-eq v0, v1, :cond_3

    goto/16 :goto_0

    .line 1347
    :cond_3
    iget-object v0, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 1348
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1349
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, p1, Lepson/print/EPImage;->decodeHeight:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1350
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1351
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, p1, Lepson/print/EPImage;->decodeHeight:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1338
    :cond_4
    iget-object v0, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 1339
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, p1, Lepson/print/EPImage;->decodeWidth:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1340
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, p1, Lepson/print/EPImage;->decodeWidth:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1341
    iget-object v0, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 1342
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, p1, Lepson/print/EPImage;->decodeHeight:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1343
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, p1, Lepson/print/EPImage;->decodeHeight:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1330
    :cond_5
    iget-object v0, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 1331
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, p1, Lepson/print/EPImage;->decodeWidth:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1332
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1333
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v2, p1, Lepson/print/EPImage;->decodeWidth:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1334
    iget-object v1, p0, Lepson/print/EPImageCreator;->src:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1361
    :goto_0
    iget-object v0, p0, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    iget v1, p1, Lepson/print/EPImage;->rotate:I

    invoke-virtual {v0, p2, p3, v1}, Lepson/print/EPImageUtil;->rotateImage(Ljava/lang/String;Ljava/lang/String;I)I

    .line 1362
    iput-object p3, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 1364
    iget-object p1, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    return-object p1
.end method

.method public declared-synchronized stopRequested()Z
    .locals 1

    monitor-enter p0

    .line 84
    :try_start_0
    iget-boolean v0, p0, Lepson/print/EPImageCreator;->mStopREquested:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public writeDate(Lepson/print/EPImage;IILjava/lang/String;IZ)Ljava/lang/String;
    .locals 15

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v0, p4

    move/from16 v3, p6

    .line 1369
    new-instance v4, Ljava/io/File;

    iget-object v5, v1, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v5

    invoke-virtual {v5}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "date_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, v2, Lepson/print/EPImage;->index:I

    const/4 v8, 0x1

    add-int/2addr v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ".bmp"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1370
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    move/from16 v5, p5

    .line 1374
    invoke-direct {p0, v5}, Lepson/print/EPImageCreator;->getFontSize(I)I

    move-result v5

    if-eqz v0, :cond_d

    .line 1382
    :try_start_0
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v6

    mul-int v6, v6, v5

    const-string v7, "drawTextToImage"

    .line 1386
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "font_size="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, " width="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, " height="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    if-lez v6, :cond_c

    if-lez v5, :cond_c

    .line 1392
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7, v8}, Landroid/graphics/Paint;-><init>(I)V

    const/16 v9, 0xe2

    const/16 v10, 0x66

    const/16 v11, 0xa

    .line 1393
    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, -0x1000000

    const/high16 v11, 0x40000000    # 2.0f

    .line 1394
    invoke-virtual {v7, v9, v11, v11, v10}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    int-to-float v9, v5

    .line 1395
    invoke-virtual {v7, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1396
    sget-object v10, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v7, v10}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const-string v10, "drawTextToImage"

    .line 1397
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "strDateTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    mul-int v10, v6, v5

    .line 1399
    new-array v11, v10, [I

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v10, :cond_0

    const/4 v14, -0x1

    .line 1401
    aput v14, v11, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 1405
    :cond_0
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v6, v5, v10}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 1410
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v5, v10, v8}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 1414
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 1416
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1417
    invoke-virtual {v7}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v11

    .line 1418
    iget v13, v11, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v11, v11, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v13, v11

    sub-float/2addr v13, v9

    int-to-float v6, v6

    sub-float/2addr v6, v13

    sub-float/2addr v9, v13

    .line 1419
    invoke-virtual {v5, v0, v6, v9, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1421
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v10, v0, v12}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1425
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 1427
    new-instance v5, Lepson/common/BMPFile;

    invoke-direct {v5}, Lepson/common/BMPFile;-><init>()V

    .line 1428
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v5, v4, v0, v6, v7}, Lepson/common/BMPFile;->saveBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;II)V

    .line 1430
    iget-object v5, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 1432
    new-instance v6, Ljava/io/File;

    iget-object v7, v1, Lepson/print/EPImageCreator;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v7

    invoke-virtual {v7}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "date2_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v10, v2, Lepson/print/EPImage;->index:I

    add-int/2addr v10, v8

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, ".bmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 1437
    iget v7, v2, Lepson/print/EPImage;->rotate:I

    if-eqz v7, :cond_7

    const/16 v9, 0x5a

    const/16 v10, 0x4e

    if-eq v7, v9, :cond_5

    const/16 v9, 0xb4

    if-eq v7, v9, :cond_3

    const/16 v9, 0x10e

    if-eq v7, v9, :cond_1

    const/4 v0, 0x0

    goto :goto_3

    .line 1470
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v0, p2, v0

    sub-int/2addr v0, v8

    if-ne v3, v8, :cond_2

    add-int/lit8 v0, v0, -0x24

    add-int/lit8 v0, v0, -0x2a

    const/16 v3, 0x52

    move v12, v0

    goto :goto_1

    :cond_2
    move v12, v0

    const/4 v3, 0x0

    .line 1476
    :goto_1
    iget-object v0, v1, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v0, v4, v6}, Lepson/print/EPImageUtil;->rotateR270Image(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    move-object v4, v6

    goto :goto_3

    :cond_3
    if-ne v3, v8, :cond_4

    const/16 v0, 0x72

    const/16 v12, 0x4e

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    .line 1465
    :goto_2
    iget-object v3, v1, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v3, v4, v6}, Lepson/print/EPImageUtil;->rotate180Image(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    goto :goto_3

    .line 1449
    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v0, p3, v0

    sub-int/2addr v0, v8

    if-ne v3, v8, :cond_6

    add-int/lit8 v0, v0, -0x48

    add-int/lit8 v0, v0, -0x2a

    const/16 v12, 0x4e

    .line 1454
    :cond_6
    iget-object v3, v1, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v3, v4, v6}, Lepson/print/EPImageUtil;->rotateR90Image(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    goto :goto_3

    .line 1439
    :cond_7
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int v6, p2, v6

    add-int/lit8 v12, v6, -0x1

    .line 1440
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v0, p3, v0

    sub-int/2addr v0, v8

    if-ne v3, v8, :cond_8

    add-int/lit8 v12, v12, -0x24

    add-int/lit8 v12, v12, -0x2a

    add-int/lit8 v0, v0, -0x48

    add-int/lit8 v0, v0, -0x2a

    .line 1484
    :cond_8
    :goto_3
    iget-object v3, v1, Lepson/print/EPImageCreator;->util:Lepson/print/EPImageUtil;

    invoke-virtual {v3, v5, v4, v12, v0}, Lepson/print/EPImageUtil;->writeDate(Ljava/lang/String;Ljava/lang/String;II)I

    .line 1485
    iput-object v5, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    goto :goto_5

    .line 1423
    :cond_9
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    .line 1412
    :cond_a
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    .line 1407
    :cond_b
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    .line 1388
    :cond_c
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    goto :goto_4

    .line 1380
    :cond_d
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1487
    :goto_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1490
    :goto_5
    iget-object v0, v2, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    return-object v0
.end method

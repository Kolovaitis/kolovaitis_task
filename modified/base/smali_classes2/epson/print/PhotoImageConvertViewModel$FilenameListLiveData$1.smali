.class Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;
.super Landroid/os/AsyncTask;
.source "PhotoImageConvertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->setOriginalList(Ljava/util/ArrayList;Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/print/PhotoImageConvertViewModel$Result;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

.field final synthetic val$application:Landroid/app/Application;

.field final synthetic val$previewImageList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;Ljava/util/ArrayList;Landroid/app/Application;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->this$0:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    iput-object p2, p0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->val$previewImageList:Ljava/util/ArrayList;

    iput-object p3, p0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->val$application:Landroid/app/Application;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/PhotoImageConvertViewModel$Result;
    .locals 2

    .line 85
    iget-object p1, p0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->this$0:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    iget-object v0, p0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->val$previewImageList:Ljava/util/ArrayList;

    iget-object v1, p0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->val$application:Landroid/app/Application;

    .line 86
    invoke-static {p1, v0, v1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->access$000(Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;Ljava/util/ArrayList;Landroid/app/Application;)Ljava/util/ArrayList;

    move-result-object p1

    .line 87
    iget-object v0, p0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->this$0:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    invoke-static {v0, p1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->access$100(Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    new-instance p1, Lepson/print/PhotoImageConvertViewModel$Result;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lepson/print/PhotoImageConvertViewModel$Result;-><init>(Ljava/util/ArrayList;)V

    return-object p1

    .line 91
    :cond_0
    new-instance v0, Lepson/print/PhotoImageConvertViewModel$Result;

    invoke-direct {v0, p1}, Lepson/print/PhotoImageConvertViewModel$Result;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 81
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->doInBackground([Ljava/lang/Void;)Lepson/print/PhotoImageConvertViewModel$Result;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Lepson/print/PhotoImageConvertViewModel$Result;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->this$0:Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;

    invoke-static {v0, p1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->access$200(Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 81
    check-cast p1, Lepson/print/PhotoImageConvertViewModel$Result;

    invoke-virtual {p0, p1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->onPostExecute(Lepson/print/PhotoImageConvertViewModel$Result;)V

    return-void
.end method

.class public Lepson/print/EPImageList;
.super Ljava/lang/Object;
.source "EPImageList.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/print/EPImageList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public apfModeInPrinting:I

.field public imageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/EPImage;",
            ">;"
        }
    .end annotation
.end field

.field private mClearlyVivid:I

.field private mMultiFileMode:Z

.field private mRenderingMode:I

.field private mSharpness:I

.field private paperHeight:I

.field private paperWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 195
    new-instance v0, Lepson/print/EPImageList$1;

    invoke-direct {v0}, Lepson/print/EPImageList$1;-><init>()V

    sput-object v0, Lepson/print/EPImageList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lepson/print/EPImageList;->paperWidth:I

    .line 13
    iput v0, p0, Lepson/print/EPImageList;->paperHeight:I

    .line 16
    iput-boolean v0, p0, Lepson/print/EPImageList;->mMultiFileMode:Z

    .line 41
    iput-boolean v0, p0, Lepson/print/EPImageList;->mMultiFileMode:Z

    .line 42
    iput v0, p0, Lepson/print/EPImageList;->apfModeInPrinting:I

    .line 43
    iput v0, p0, Lepson/print/EPImageList;->mSharpness:I

    .line 44
    iput v0, p0, Lepson/print/EPImageList;->mClearlyVivid:I

    .line 45
    iput v0, p0, Lepson/print/EPImageList;->mRenderingMode:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lepson/print/EPImageList;->paperWidth:I

    .line 13
    iput v0, p0, Lepson/print/EPImageList;->paperHeight:I

    .line 16
    iput-boolean v0, p0, Lepson/print/EPImageList;->mMultiFileMode:Z

    .line 211
    sget-object v1, Lepson/print/EPImage;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lepson/print/EPImageList;->setPaperWidth(I)V

    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lepson/print/EPImageList;->setPaperHeight(I)V

    const/4 v1, 0x1

    .line 217
    new-array v1, v1, [Z

    .line 218
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 219
    aget-boolean v0, v1, v0

    iput-boolean v0, p0, Lepson/print/EPImageList;->mMultiFileMode:Z

    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImageList;->apfModeInPrinting:I

    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImageList;->mSharpness:I

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImageList;->mClearlyVivid:I

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lepson/print/EPImageList;->mRenderingMode:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lepson/print/EPImageList$1;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lepson/print/EPImageList;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/EPImage;",
            ">;I)V"
        }
    .end annotation

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    const/4 p2, 0x0

    .line 12
    iput p2, p0, Lepson/print/EPImageList;->paperWidth:I

    .line 13
    iput p2, p0, Lepson/print/EPImageList;->paperHeight:I

    .line 16
    iput-boolean p2, p0, Lepson/print/EPImageList;->mMultiFileMode:Z

    .line 228
    iput-object p1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public add(ILepson/print/EPImage;)Z
    .locals 1

    .line 76
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 77
    iget-object p1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 80
    :cond_0
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/4 p1, 0x1

    return p1
.end method

.method public add(Lepson/print/EPImage;)Z
    .locals 1

    .line 71
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public add(Ljava/lang/String;)Z
    .locals 3

    .line 66
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    new-instance v1, Lepson/print/EPImage;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, p1, v2}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public clear()V
    .locals 1

    .line 121
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public clearExceptSrcFileName()V
    .locals 5

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 152
    :goto_0
    iget-object v2, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 153
    new-instance v2, Lepson/print/EPImage;

    iget-object v3, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/EPImage;

    iget-object v3, v3, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 156
    iput-object v1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    .line 157
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 158
    iput-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public get(I)Lepson/print/EPImage;
    .locals 1

    if-ltz p1, :cond_1

    .line 168
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 172
    :cond_0
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/EPImage;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getClearlyVivid()I
    .locals 1

    .line 57
    iget v0, p0, Lepson/print/EPImageList;->mClearlyVivid:I

    return v0
.end method

.method public getPaperHeight()I
    .locals 1

    .line 240
    iget v0, p0, Lepson/print/EPImageList;->paperHeight:I

    return v0
.end method

.method public getPaperWidth()I
    .locals 1

    .line 232
    iget v0, p0, Lepson/print/EPImageList;->paperWidth:I

    return v0
.end method

.method public getRenderingMode()I
    .locals 1

    .line 255
    iget v0, p0, Lepson/print/EPImageList;->mRenderingMode:I

    return v0
.end method

.method public getSharpness()I
    .locals 1

    .line 49
    iget v0, p0, Lepson/print/EPImageList;->mSharpness:I

    return v0
.end method

.method public isMultifileMode()Z
    .locals 1

    .line 144
    iget-boolean v0, p0, Lepson/print/EPImageList;->mMultiFileMode:Z

    return v0
.end method

.method public remove(I)Lepson/print/EPImage;
    .locals 1

    if-ltz p1, :cond_1

    .line 102
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/EPImage;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public remove(Ljava/lang/String;)Lepson/print/EPImage;
    .locals 2

    const/4 v0, 0x0

    .line 110
    :goto_0
    iget-object v1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 111
    iget-object v1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/EPImage;

    iget-object v1, v1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    iget-object p1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/EPImage;

    return-object p1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public set(ILepson/print/EPImage;)Lepson/print/EPImage;
    .locals 1

    if-ltz p1, :cond_1

    .line 94
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 98
    :cond_0
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/EPImage;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public set(ILjava/lang/String;)Lepson/print/EPImage;
    .locals 2

    if-ltz p1, :cond_1

    .line 85
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    new-instance v1, Lepson/print/EPImage;

    invoke-direct {v1, p2, p1}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/EPImage;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public setClearlyVivid(I)V
    .locals 0

    .line 61
    iput p1, p0, Lepson/print/EPImageList;->mClearlyVivid:I

    return-void
.end method

.method public setFileType(I)V
    .locals 2

    const/4 v0, 0x0

    .line 127
    :goto_0
    iget-object v1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 128
    iget-object v1, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/EPImage;

    invoke-virtual {v1, p1}, Lepson/print/EPImage;->setType(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setMultifileMode(Z)V
    .locals 0

    .line 136
    iput-boolean p1, p0, Lepson/print/EPImageList;->mMultiFileMode:Z

    return-void
.end method

.method public setPaperHeight(I)V
    .locals 0

    .line 244
    iput p1, p0, Lepson/print/EPImageList;->paperHeight:I

    return-void
.end method

.method public setPaperWidth(I)V
    .locals 0

    .line 236
    iput p1, p0, Lepson/print/EPImageList;->paperWidth:I

    return-void
.end method

.method public setRenderingMode(I)V
    .locals 0

    .line 251
    iput p1, p0, Lepson/print/EPImageList;->mRenderingMode:I

    return-void
.end method

.method public setSharpness(I)V
    .locals 0

    .line 53
    iput p1, p0, Lepson/print/EPImageList;->mSharpness:I

    return-void
.end method

.method public size()I
    .locals 1

    .line 163
    iget-object v0, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 180
    iget-object p2, p0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 182
    invoke-virtual {p0}, Lepson/print/EPImageList;->getPaperWidth()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 183
    invoke-virtual {p0}, Lepson/print/EPImageList;->getPaperHeight()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 p2, 0x1

    .line 186
    new-array p2, p2, [Z

    .line 187
    iget-boolean v0, p0, Lepson/print/EPImageList;->mMultiFileMode:Z

    const/4 v1, 0x0

    aput-boolean v0, p2, v1

    .line 188
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 189
    iget p2, p0, Lepson/print/EPImageList;->apfModeInPrinting:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 190
    iget p2, p0, Lepson/print/EPImageList;->mSharpness:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 191
    iget p2, p0, Lepson/print/EPImageList;->mClearlyVivid:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 192
    iget p2, p0, Lepson/print/EPImageList;->mRenderingMode:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

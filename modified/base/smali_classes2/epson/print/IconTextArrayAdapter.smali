.class public Lepson/print/IconTextArrayAdapter;
.super Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;
.source "IconTextArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter<",
        "Lepson/print/IconTextArrayItem;",
        ">;"
    }
.end annotation


# static fields
.field private static LANDSCAPE_FONTSIZE_RATIO:F = 4.2f

.field private static LANDSCAPE_RATIO:F = 2.8f

.field private static PORTRATE_FONTSIZE_RATIO:F = 7.5f

.field private static PORTRATE_RATIO:F = 1.0f

.field private static final TAG:Ljava/lang/String; = "IconTextArrayAdapter"


# instance fields
.field private height:I

.field private inflater:Landroid/view/LayoutInflater;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/print/IconTextArrayItem;",
            ">;"
        }
    .end annotation
.end field

.field private line:I

.field private textViewResourceId:I

.field private viewGroup:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List<",
            "Lepson/print/IconTextArrayItem;",
            ">;",
            "Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;",
            ")V"
        }
    .end annotation

    .line 60
    invoke-direct {p0, p1, p4, p3}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;-><init>(Landroid/content/Context;Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;Ljava/util/List;)V

    const/4 p4, 0x0

    .line 34
    iput-object p4, p0, Lepson/print/IconTextArrayAdapter;->viewGroup:Landroid/view/ViewGroup;

    const/4 p4, 0x0

    .line 37
    iput p4, p0, Lepson/print/IconTextArrayAdapter;->line:I

    .line 40
    iput p4, p0, Lepson/print/IconTextArrayAdapter;->height:I

    .line 63
    iput p2, p0, Lepson/print/IconTextArrayAdapter;->textViewResourceId:I

    .line 64
    iput-object p3, p0, Lepson/print/IconTextArrayAdapter;->items:Ljava/util/List;

    const-string p2, "layout_inflater"

    .line 67
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lepson/print/IconTextArrayAdapter;->inflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getActualView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    .line 83
    iget-object p2, p0, Lepson/print/IconTextArrayAdapter;->inflater:Landroid/view/LayoutInflater;

    iget p3, p0, Lepson/print/IconTextArrayAdapter;->textViewResourceId:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 89
    :cond_0
    iget p3, p0, Lepson/print/IconTextArrayAdapter;->line:I

    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 106
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f050077

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 103
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f050076

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 100
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f050075

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 97
    :pswitch_3
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f050074

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 94
    :pswitch_4
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f050073

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 91
    :pswitch_5
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f050072

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 112
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p3

    iget p3, p3, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x0

    packed-switch p3, :pswitch_data_1

    goto :goto_1

    .line 118
    :pswitch_6
    move-object p3, p2

    check-cast p3, Landroid/widget/LinearLayout;

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const p3, 0x7f0802dd

    .line 121
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    .line 122
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 123
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v2, -0x1

    .line 124
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 125
    invoke-virtual {p3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 126
    invoke-virtual {p3}, Landroid/view/View;->requestLayout()V

    const v1, 0x7f0802b9

    .line 128
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 130
    iput v0, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 131
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 132
    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    invoke-virtual {p3}, Landroid/view/View;->requestLayout()V

    .line 139
    :goto_1
    :pswitch_7
    iget-object p3, p0, Lepson/print/IconTextArrayAdapter;->items:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/IconTextArrayItem;

    const-string p3, "text"

    .line 142
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 143
    iget v1, p1, Lepson/print/IconTextArrayItem;->menuId:I

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(I)V

    .line 151
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v1, :pswitch_data_2

    goto :goto_2

    .line 156
    :pswitch_8
    iget v1, p0, Lepson/print/IconTextArrayAdapter;->height:I

    int-to-float v1, v1

    sget v2, Lepson/print/IconTextArrayAdapter;->LANDSCAPE_FONTSIZE_RATIO:F

    div-float/2addr v1, v2

    invoke-virtual {p3, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    const/16 v0, 0x13

    .line 157
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_2

    .line 153
    :pswitch_9
    iget v1, p0, Lepson/print/IconTextArrayAdapter;->height:I

    int-to-float v1, v1

    sget v2, Lepson/print/IconTextArrayAdapter;->PORTRATE_FONTSIZE_RATIO:F

    div-float/2addr v1, v2

    invoke-virtual {p3, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    :goto_2
    const-string p3, "icon"

    .line 162
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 163
    iget p1, p1, Lepson/print/IconTextArrayItem;->imageID:I

    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object p2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .line 175
    iput p1, p0, Lepson/print/IconTextArrayAdapter;->line:I

    .line 178
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 183
    :pswitch_0
    move-object v0, p3

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getColumnWidth()I

    move-result v0

    int-to-float v0, v0

    sget v1, Lepson/print/IconTextArrayAdapter;->LANDSCAPE_RATIO:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lepson/print/IconTextArrayAdapter;->height:I

    goto :goto_0

    .line 180
    :pswitch_1
    move-object v0, p3

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getColumnWidth()I

    move-result v0

    int-to-float v0, v0

    sget v1, Lepson/print/IconTextArrayAdapter;->PORTRATE_RATIO:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lepson/print/IconTextArrayAdapter;->height:I

    .line 188
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x0

    .line 192
    :goto_1
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    .line 193
    invoke-virtual {v0, p3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 194
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 195
    iget v2, p0, Lepson/print/IconTextArrayAdapter;->height:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 196
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    :cond_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

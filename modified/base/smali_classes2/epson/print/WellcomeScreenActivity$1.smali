.class Lepson/print/WellcomeScreenActivity$1;
.super Ljava/lang/Thread;
.source "WellcomeScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/WellcomeScreenActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/WellcomeScreenActivity;

.field wait:I


# direct methods
.method constructor <init>(Lepson/print/WellcomeScreenActivity;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lepson/print/WellcomeScreenActivity$1;->this$0:Lepson/print/WellcomeScreenActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 p1, 0x0

    .line 20
    iput p1, p0, Lepson/print/WellcomeScreenActivity$1;->wait:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 24
    :try_start_0
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 25
    iget-object v0, p0, Lepson/print/WellcomeScreenActivity$1;->this$0:Lepson/print/WellcomeScreenActivity;

    invoke-static {v0}, Lepson/print/EPPrinterManager;->checkV3RemotePrinterData(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lepson/print/WellcomeScreenActivity$1;->this$0:Lepson/print/WellcomeScreenActivity;

    invoke-static {v0}, Lepson/print/EPPrinterManager;->convertV3RemotePrinter(Landroid/content/Context;)V

    .line 31
    :cond_0
    :goto_0
    iget v0, p0, Lepson/print/WellcomeScreenActivity$1;->wait:I

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_1

    const-wide/16 v0, 0x64

    .line 32
    invoke-static {v0, v1}, Lepson/print/WellcomeScreenActivity$1;->sleep(J)V

    .line 33
    iget v0, p0, Lepson/print/WellcomeScreenActivity$1;->wait:I

    add-int/lit8 v0, v0, 0x64

    iput v0, p0, Lepson/print/WellcomeScreenActivity$1;->wait:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 37
    :cond_1
    iget-object v0, p0, Lepson/print/WellcomeScreenActivity$1;->this$0:Lepson/print/WellcomeScreenActivity;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lepson/print/IprintHome;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lepson/print/WellcomeScreenActivity$1;->this$0:Lepson/print/WellcomeScreenActivity;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lepson/print/IprintHome;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lepson/print/WellcomeScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 38
    iget-object v1, p0, Lepson/print/WellcomeScreenActivity$1;->this$0:Lepson/print/WellcomeScreenActivity;

    invoke-virtual {v1}, Lepson/print/WellcomeScreenActivity;->finish()V

    throw v0

    .line 37
    :catch_0
    iget-object v0, p0, Lepson/print/WellcomeScreenActivity$1;->this$0:Lepson/print/WellcomeScreenActivity;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lepson/print/IprintHome;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_1
    invoke-virtual {v0, v1}, Lepson/print/WellcomeScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 38
    iget-object v0, p0, Lepson/print/WellcomeScreenActivity$1;->this$0:Lepson/print/WellcomeScreenActivity;

    invoke-virtual {v0}, Lepson/print/WellcomeScreenActivity;->finish()V

    return-void
.end method

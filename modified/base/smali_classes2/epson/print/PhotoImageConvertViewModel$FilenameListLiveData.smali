.class public Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;
.super Landroid/arch/lifecycle/LiveData;
.source "PhotoImageConvertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/PhotoImageConvertViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilenameListLiveData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/arch/lifecycle/LiveData<",
        "Lepson/print/PhotoImageConvertViewModel$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Landroid/arch/lifecycle/LiveData;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;Ljava/util/ArrayList;Landroid/app/Application;)Ljava/util/ArrayList;
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->convertHeifImageList(Ljava/util/ArrayList;Landroid/app/Application;)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;Ljava/util/ArrayList;)Z
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->checkEpImage(Ljava/util/ArrayList;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;Ljava/lang/Object;)V
    .locals 0

    .line 78
    invoke-virtual {p0, p1}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method private checkEpImage(Ljava/util/ArrayList;)Z
    .locals 3
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 151
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    .line 153
    iget-object v2, v1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->checkEPImage(Lepson/print/EPImage;)Z

    move-result v2

    if-nez v2, :cond_2

    return v0

    .line 157
    :cond_2
    iget-object v1, v1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    invoke-static {v1}, Lepson/print/ActivityViewImageSelect;->checkBmpNot24Bit(Lepson/print/EPImage;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method private checkFilePath(Ljava/util/ArrayList;Ljava/lang/String;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x1

    if-nez p2, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    .line 111
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    .line 119
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 120
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-nez v3, :cond_1

    return v1

    .line 127
    :cond_1
    :try_start_1
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 134
    invoke-virtual {v3, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    return v1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    return v1

    :cond_3
    return v0

    :catch_1
    return v1
.end method

.method private convertHeifImageList(Ljava/util/ArrayList;Landroid/app/Application;)Ljava/util/ArrayList;
    .locals 6
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/app/Application;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/app/Application;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;",
            ">;"
        }
    .end annotation

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    return-object v0

    .line 179
    :cond_0
    invoke-static {p2}, Lepson/print/PhotoImageConvertViewModel;->access$300(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    .line 181
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 182
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_1

    goto :goto_1

    .line 191
    :cond_1
    invoke-static {v2}, Lepson/common/ImageUtil;->isHEIFFormat(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_4

    const/16 v3, 0x1e

    .line 193
    invoke-static {v2, p2, v3}, Lepson/common/ExternalFileUtils;->getNotDuplicateFilename(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    return-object v4

    .line 199
    :cond_2
    invoke-static {v2, v3}, Lepson/common/ImageUtil;->convertHEIFtoJPEG(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    return-object v4

    .line 204
    :cond_3
    new-instance v4, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    invoke-direct {v4, v3, v2, v1}, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 206
    :cond_4
    new-instance v3, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    invoke-direct {v3, v2, v4, v1}, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    return-object v0
.end method


# virtual methods
.method public setOriginalList(Ljava/util/ArrayList;Landroid/app/Application;)V
    .locals 1
    .param p2    # Landroid/app/Application;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/app/Application;",
            ")V"
        }
    .end annotation

    .line 81
    new-instance v0, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;

    invoke-direct {v0, p0, p1, p2}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;-><init>(Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData;Ljava/util/ArrayList;Landroid/app/Application;)V

    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    .line 98
    invoke-virtual {v0, p1, p2}, Lepson/print/PhotoImageConvertViewModel$FilenameListLiveData$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

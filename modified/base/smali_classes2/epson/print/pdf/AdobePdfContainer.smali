.class public Lepson/print/pdf/AdobePdfContainer;
.super Ljava/lang/Object;
.source "AdobePdfContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/pdf/AdobePdfContainer$ParameterException;,
        Lepson/print/pdf/AdobePdfContainer$NoPdfException;,
        Lepson/print/pdf/AdobePdfContainer$PasswordException;,
        Lepson/print/pdf/AdobePdfContainer$PdfException;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AdobePdfContainer"


# instance fields
.field private adpbeMPSInterface:Lcom/adobe/mps/MPS;

.field private bm:Landroid/graphics/Bitmap;

.field colorComponent:I

.field private final colorspace_ARGB:I

.field private mContext:Landroid/content/Context;

.field private pageCount:I

.field rawBuffer:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/adobe/mps/MPS;

    invoke-direct {v0}, Lcom/adobe/mps/MPS;-><init>()V

    iput-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    const/4 v0, 0x0

    .line 48
    iput-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    .line 51
    iput v1, p0, Lepson/print/pdf/AdobePdfContainer;->pageCount:I

    .line 54
    iput-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->bm:Landroid/graphics/Bitmap;

    const/4 v0, 0x4

    .line 57
    iput v0, p0, Lepson/print/pdf/AdobePdfContainer;->colorComponent:I

    const/4 v0, 0x1

    .line 58
    iput v0, p0, Lepson/print/pdf/AdobePdfContainer;->colorspace_ARGB:I

    .line 62
    iput-object p1, p0, Lepson/print/pdf/AdobePdfContainer;->mContext:Landroid/content/Context;

    const-string p1, "AdobePdfContainer"

    const-string v0, "adpbeMPSInterface.MPSInit()"

    .line 64
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "com/adobe/mps/ARAESCryptor"

    const-string v0, "com/adobe/mps/ARSHADigest"

    .line 72
    iget-object v1, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    invoke-virtual {v1, p1, v0}, Lcom/adobe/mps/MPS;->MPSInit(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public CountPages()I
    .locals 1

    .line 111
    iget v0, p0, Lepson/print/pdf/AdobePdfContainer;->pageCount:I

    return v0
.end method

.method public GetPageSizeX(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/pdf/AdobePdfContainer$ParameterException;
        }
    .end annotation

    .line 137
    invoke-virtual {p0}, Lepson/print/pdf/AdobePdfContainer;->CountPages()I

    move-result v0

    if-lt v0, p1, :cond_0

    .line 141
    iget-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    invoke-virtual {v0, p1}, Lcom/adobe/mps/MPS;->PDFGetPageAttributes(I)[I

    move-result-object p1

    const/4 v0, 0x0

    aget p1, p1, v0

    return p1

    .line 138
    :cond_0
    new-instance p1, Lepson/print/pdf/AdobePdfContainer$ParameterException;

    invoke-direct {p1, p0}, Lepson/print/pdf/AdobePdfContainer$ParameterException;-><init>(Lepson/print/pdf/AdobePdfContainer;)V

    throw p1
.end method

.method public GetPageSizeY(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/pdf/AdobePdfContainer$ParameterException;
        }
    .end annotation

    .line 147
    invoke-virtual {p0}, Lepson/print/pdf/AdobePdfContainer;->CountPages()I

    move-result v0

    if-lt v0, p1, :cond_0

    .line 151
    iget-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    invoke-virtual {v0, p1}, Lcom/adobe/mps/MPS;->PDFGetPageAttributes(I)[I

    move-result-object p1

    const/4 v0, 0x1

    aget p1, p1, v0

    return p1

    .line 148
    :cond_0
    new-instance p1, Lepson/print/pdf/AdobePdfContainer$ParameterException;

    invoke-direct {p1, p0}, Lepson/print/pdf/AdobePdfContainer$ParameterException;-><init>(Lepson/print/pdf/AdobePdfContainer;)V

    throw p1
.end method

.method public LoadDocument(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/pdf/AdobePdfContainer$PasswordException;,
            Lepson/print/pdf/AdobePdfContainer$NoPdfException;
        }
    .end annotation

    const-string v0, "AdobePdfContainer"

    const-string v1, "call LoadDocument()"

    .line 89
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    .line 91
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 94
    :cond_0
    iget-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    invoke-virtual {v0, p1, p2}, Lcom/adobe/mps/MPS;->PDFDocInit(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/pdf/AdobePdfContainer;->pageCount:I

    goto :goto_1

    .line 92
    :cond_1
    :goto_0
    iget-object p2, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    invoke-virtual {p2, p1}, Lcom/adobe/mps/MPS;->PDFDocInit(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/pdf/AdobePdfContainer;->pageCount:I

    .line 98
    :goto_1
    iget p1, p0, Lepson/print/pdf/AdobePdfContainer;->pageCount:I

    packed-switch p1, :pswitch_data_0

    return-void

    .line 100
    :pswitch_0
    new-instance p1, Lepson/print/pdf/AdobePdfContainer$NoPdfException;

    invoke-direct {p1, p0}, Lepson/print/pdf/AdobePdfContainer$NoPdfException;-><init>(Lepson/print/pdf/AdobePdfContainer;)V

    throw p1

    .line 102
    :pswitch_1
    new-instance p1, Lepson/print/pdf/AdobePdfContainer$PasswordException;

    invoke-direct {p1, p0}, Lepson/print/pdf/AdobePdfContainer$PasswordException;-><init>(Lepson/print/pdf/AdobePdfContainer;)V

    throw p1

    .line 104
    :pswitch_2
    new-instance p1, Lepson/print/pdf/AdobePdfContainer$PasswordException;

    invoke-direct {p1, p0}, Lepson/print/pdf/AdobePdfContainer$PasswordException;-><init>(Lepson/print/pdf/AdobePdfContainer;)V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public decodeToJpegFile(Ljava/lang/String;III)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/pdf/AdobePdfContainer$ParameterException;
        }
    .end annotation

    .line 254
    invoke-virtual {p0}, Lepson/print/pdf/AdobePdfContainer;->CountPages()I

    move-result v0

    if-lt v0, p2, :cond_1

    .line 258
    iget-object v1, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    const/4 v6, 0x0

    move v2, p2

    move-object v3, p1

    move v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/adobe/mps/MPS;->PDFPagetoImage(ILjava/lang/String;III)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    .line 255
    :cond_1
    new-instance p1, Lepson/print/pdf/AdobePdfContainer$ParameterException;

    invoke-direct {p1, p0}, Lepson/print/pdf/AdobePdfContainer$ParameterException;-><init>(Lepson/print/pdf/AdobePdfContainer;)V

    throw p1
.end method

.method protected finalize()V
    .locals 2

    const-string v0, "AdobePdfContainer"

    const-string v1, "finalize()"

    .line 76
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getPageBitmap(IIII)Landroid/graphics/Bitmap;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "AdobePdfContainer"

    const-string v1, "getPageBitmap() Start"

    .line 156
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lepson/print/pdf/AdobePdfContainer;->CountPages()I

    move-result v0

    if-lt v0, p1, :cond_3

    .line 163
    iget-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->bm:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 165
    iput-object v1, p0, Lepson/print/pdf/AdobePdfContainer;->bm:Landroid/graphics/Bitmap;

    .line 168
    :cond_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 169
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->bm:Landroid/graphics/Bitmap;

    mul-int v0, p2, p3

    .line 172
    iget v2, p0, Lepson/print/pdf/AdobePdfContainer;->colorComponent:I

    mul-int v0, v0, v2

    .line 173
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->rawBuffer:Ljava/nio/ByteBuffer;

    const/4 v0, 0x4

    .line 176
    new-array v9, v0, [I

    fill-array-data v9, :array_0

    .line 178
    iget-object v7, p0, Lepson/print/pdf/AdobePdfContainer;->rawBuffer:Ljava/nio/ByteBuffer;

    if-eqz v7, :cond_2

    .line 179
    iget-object v2, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    const/4 v6, 0x1

    const/4 v8, 0x0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v2 .. v9}, Lcom/adobe/mps/MPS;->PDFPageRender(IIIILjava/nio/ByteBuffer;I[I)I

    move-result p1

    if-nez p1, :cond_1

    .line 182
    iget-object p1, p0, Lepson/print/pdf/AdobePdfContainer;->bm:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->rawBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    if-eqz p4, :cond_1

    .line 186
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    int-to-float p1, p4

    .line 187
    invoke-virtual {v7, p1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 188
    iget-object v2, p0, Lepson/print/pdf/AdobePdfContainer;->bm:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x0

    move v5, p2

    move v6, p3

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/pdf/AdobePdfContainer;->bm:Landroid/graphics/Bitmap;

    .line 192
    :cond_1
    iget-object p1, p0, Lepson/print/pdf/AdobePdfContainer;->rawBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 193
    iput-object v1, p0, Lepson/print/pdf/AdobePdfContainer;->rawBuffer:Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_2
    const-string p1, "AdobePdfContainer"

    const-string p2, "Failed ByteBuffer.allocateDirect()"

    .line 195
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string p1, "AdobePdfContainer"

    const-string p2, "getPageBitmap() End"

    .line 198
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object p1, p0, Lepson/print/pdf/AdobePdfContainer;->bm:Landroid/graphics/Bitmap;

    return-object p1

    .line 160
    :cond_3
    new-instance p1, Lepson/print/pdf/AdobePdfContainer$ParameterException;

    invoke-direct {p1, p0}, Lepson/print/pdf/AdobePdfContainer$ParameterException;-><init>(Lepson/print/pdf/AdobePdfContainer;)V

    throw p1

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public getPageBitmapFile(IIIZ)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "AdobePdfContainer"

    const-string v1, "getPageBitmapFile() Start"

    .line 215
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0}, Lepson/print/pdf/AdobePdfContainer;->CountPages()I

    move-result v0

    if-lt v0, p1, :cond_2

    .line 222
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lepson/print/pdf/AdobePdfContainer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v1

    const-string v2, "tmpPDF.jpg"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 224
    iget-object v3, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    const/4 v8, 0x0

    move v4, p1

    move-object v5, v0

    move v6, p2

    move v7, p3

    invoke-virtual/range {v3 .. v8}, Lcom/adobe/mps/MPS;->PDFPagetoImage(ILjava/lang/String;III)I

    move-result p1

    if-nez p1, :cond_1

    if-eqz p4, :cond_0

    .line 229
    new-instance p1, Ljava/io/File;

    iget-object p2, p0, Lepson/print/pdf/AdobePdfContainer;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p2

    invoke-virtual {p2}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object p2

    const-string p3, "tmpPDF90.jpg"

    invoke-direct {p1, p2, p3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    .line 230
    new-instance p2, Lepson/image/epsonImage;

    invoke-direct {p2}, Lepson/image/epsonImage;-><init>()V

    .line 231
    invoke-virtual {p2, v0, p1}, Lepson/image/epsonImage;->epsmpRotateImage2(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    const-string p2, "AdobePdfContainer"

    const-string p3, "getPageBitmapFile() End"

    .line 234
    invoke-static {p2, p3}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1

    .line 219
    :cond_2
    new-instance p1, Lepson/print/pdf/AdobePdfContainer$ParameterException;

    invoke-direct {p1, p0}, Lepson/print/pdf/AdobePdfContainer$ParameterException;-><init>(Lepson/print/pdf/AdobePdfContainer;)V

    throw p1
.end method

.method public hasPrintPermission()I
    .locals 2

    .line 126
    iget-object v0, p0, Lepson/print/pdf/AdobePdfContainer;->adpbeMPSInterface:Lcom/adobe/mps/MPS;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/adobe/mps/MPS;->PDFHasPrintPermission(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

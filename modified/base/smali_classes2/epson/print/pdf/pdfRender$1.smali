.class Lepson/print/pdf/pdfRender$1;
.super Ljava/lang/Object;
.source "pdfRender.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/pdf/pdfRender;->startConvertPage(III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/pdf/pdfRender;

.field final synthetic val$endPage:I

.field final synthetic val$mode:I

.field final synthetic val$startPage:I


# direct methods
.method constructor <init>(Lepson/print/pdf/pdfRender;III)V
    .locals 0

    .line 74
    iput-object p1, p0, Lepson/print/pdf/pdfRender$1;->this$0:Lepson/print/pdf/pdfRender;

    iput p2, p0, Lepson/print/pdf/pdfRender$1;->val$mode:I

    iput p3, p0, Lepson/print/pdf/pdfRender$1;->val$startPage:I

    iput p4, p0, Lepson/print/pdf/pdfRender$1;->val$endPage:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 77
    :try_start_0
    iget-object v0, p0, Lepson/print/pdf/pdfRender$1;->this$0:Lepson/print/pdf/pdfRender;

    invoke-static {v0}, Lepson/print/pdf/pdfRender;->access$000(Lepson/print/pdf/pdfRender;)Lepson/print/pdf/AdobePdfContainer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 78
    iget v0, p0, Lepson/print/pdf/pdfRender$1;->val$mode:I

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lepson/print/pdf/pdfRender$1;->this$0:Lepson/print/pdf/pdfRender;

    iput-boolean v1, v0, Lepson/print/pdf/pdfRender;->mPreviewmode:Z

    .line 80
    iget-object v0, p0, Lepson/print/pdf/pdfRender$1;->this$0:Lepson/print/pdf/pdfRender;

    invoke-static {v0}, Lepson/print/pdf/pdfRender;->access$100(Lepson/print/pdf/pdfRender;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, "PDF_CONVERTER"

    const-string v1, "convert4Preview :start"

    .line 81
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v0, p0, Lepson/print/pdf/pdfRender$1;->this$0:Lepson/print/pdf/pdfRender;

    iget v1, p0, Lepson/print/pdf/pdfRender$1;->val$startPage:I

    invoke-virtual {v0, v1}, Lepson/print/pdf/pdfRender;->convert4Preview(I)V

    const-string v0, "PDF_CONVERTER"

    const-string v1, "convert4Preview :finish"

    .line 83
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 84
    :cond_0
    iget v0, p0, Lepson/print/pdf/pdfRender$1;->val$mode:I

    if-ne v0, v1, :cond_1

    .line 86
    iget-object v0, p0, Lepson/print/pdf/pdfRender$1;->this$0:Lepson/print/pdf/pdfRender;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lepson/print/pdf/pdfRender;->mPreviewmode:Z

    const-string v0, "PDF_CONVERTER"

    const-string v1, "printing"

    .line 87
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v0, p0, Lepson/print/pdf/pdfRender$1;->this$0:Lepson/print/pdf/pdfRender;

    iget v1, p0, Lepson/print/pdf/pdfRender$1;->val$startPage:I

    iget v2, p0, Lepson/print/pdf/pdfRender$1;->val$endPage:I

    invoke-virtual {v0, v1, v2}, Lepson/print/pdf/pdfRender;->convert4Print(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PDF_CONVERTER"

    .line 92
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GOT ERROR WHEN CONVERT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_1
    :goto_0
    iget-object v0, p0, Lepson/print/pdf/pdfRender$1;->this$0:Lepson/print/pdf/pdfRender;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lepson/print/pdf/pdfRender;->access$202(Lepson/print/pdf/pdfRender;Ljava/lang/Thread;)Ljava/lang/Thread;

    return-void
.end method

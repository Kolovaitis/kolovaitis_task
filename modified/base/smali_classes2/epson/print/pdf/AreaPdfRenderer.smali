.class public Lepson/print/pdf/AreaPdfRenderer;
.super Ljava/lang/Object;
.source "AreaPdfRenderer.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AreaPdfRenderer"


# instance fields
.field private mPdfContainer:Lepson/print/pdf/AdobePdfContainer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkFileSize(Ljava/lang/String;)Z
    .locals 6

    .line 53
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 54
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result p1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return v1

    .line 57
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long p1, v2, v4

    if-gtz p1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public static getCircumscribedSize([I[I)[I
    .locals 5
    .param p0    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    .line 108
    aget v1, p1, v0

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v3, p1, v2

    int-to-float v3, v3

    aget v4, p0, v0

    int-to-float v4, v4

    aget p0, p0, v2

    int-to-float p0, p0

    invoke-static {v1, v3, v4, p0}, Lepson/print/pdf/AreaPdfRenderer;->getInscribedScaleFactor(FFFF)F

    move-result p0

    const/4 v1, 0x2

    .line 110
    new-array v1, v1, [I

    aget v3, p1, v0

    int-to-float v3, v3

    div-float/2addr v3, p0

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    aput v3, v1, v0

    aget p1, p1, v2

    int-to-float p1, p1

    div-float/2addr p1, p0

    float-to-double p0, p1

    invoke-static {p0, p1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p0

    double-to-int p0, p0

    aput p0, v1, v2

    return-object v1
.end method

.method public static getInscribeSize([IFF)[I
    .locals 3

    const/4 v0, 0x0

    .line 122
    aget v1, p0, v0

    int-to-float v1, v1

    const/4 v2, 0x1

    aget p0, p0, v2

    int-to-float p0, p0

    invoke-static {v1, p0, p1, p2}, Lepson/print/pdf/AreaPdfRenderer;->getInscribedScaleFactor(FFFF)F

    move-result p0

    const/4 v1, 0x2

    .line 126
    new-array v1, v1, [I

    mul-float p1, p1, p0

    float-to-int p1, p1

    aput p1, v1, v0

    mul-float p2, p2, p0

    float-to-int p0, p2

    aput p0, v1, v2

    return-object v1
.end method

.method public static getInscribedScaleFactor(FFFF)F
    .locals 0

    div-float/2addr p0, p2

    div-float/2addr p1, p3

    .line 136
    invoke-static {p0, p1}, Ljava/lang/Math;->min(FF)F

    move-result p0

    return p0
.end method

.method public static getPdfDecodeFilename(Landroid/content/Context;I)Ljava/io/File;
    .locals 3

    .line 144
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p0

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ".jpg"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getPortraitOrLandscapeSize([IZ)[I
    .locals 4
    .param p0    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    .line 98
    aget v1, p0, v0

    const/4 v2, 0x1

    aget v3, p0, v2

    if-gt v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eq v1, p1, :cond_1

    const/4 p1, 0x2

    .line 99
    new-array p1, p1, [I

    aget v1, p0, v2

    aput v1, p1, v0

    aget p0, p0, v0

    aput p0, p1, v2

    move-object p0, p1

    :cond_1
    return-object p0
.end method


# virtual methods
.method public convertPageForPrint(Ljava/lang/String;I[I)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/pdf/AdobePdfContainer$ParameterException;
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {v0, p2}, Lepson/print/pdf/AdobePdfContainer;->GetPageSizeX(I)I

    move-result v0

    int-to-float v0, v0

    .line 84
    iget-object v1, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {v1, p2}, Lepson/print/pdf/AdobePdfContainer;->GetPageSizeY(I)I

    move-result v1

    int-to-float v1, v1

    .line 85
    invoke-static {p3, v0, v1}, Lepson/print/pdf/AreaPdfRenderer;->getInscribeSize([IFF)[I

    move-result-object p3

    .line 86
    iget-object v0, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    const/4 v1, 0x0

    aget v1, p3, v1

    const/4 v2, 0x1

    aget p3, p3, v2

    invoke-virtual {v0, p1, p2, v1, p3}, Lepson/print/pdf/AdobePdfContainer;->decodeToJpegFile(Ljava/lang/String;III)Z

    move-result p1

    return p1
.end method

.method public isPageLandscape(I)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/pdf/AdobePdfContainer$ParameterException;
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {v0, p1}, Lepson/print/pdf/AdobePdfContainer;->GetPageSizeX(I)I

    move-result v0

    .line 78
    iget-object v1, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {v1, p1}, Lepson/print/pdf/AdobePdfContainer;->GetPageSizeY(I)I

    move-result p1

    if-le v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public openPdfFile(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1

    .line 33
    new-instance v0, Lepson/print/pdf/AdobePdfContainer;

    invoke-direct {v0, p3}, Lepson/print/pdf/AdobePdfContainer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    const/4 p3, 0x0

    .line 38
    :try_start_0
    invoke-direct {p0, p1}, Lepson/print/pdf/AreaPdfRenderer;->checkFileSize(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return p3

    .line 41
    :cond_0
    iget-object v0, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {v0, p1, p2}, Lepson/print/pdf/AdobePdfContainer;->LoadDocument(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    return p3
.end method

.method setPdfContainer(Lepson/print/pdf/AdobePdfContainer;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 24
    iput-object p1, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    return-void
.end method

.method public totalPages()I
    .locals 1

    .line 65
    iget-object v0, p0, Lepson/print/pdf/AreaPdfRenderer;->mPdfContainer:Lepson/print/pdf/AdobePdfContainer;

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {v0}, Lepson/print/pdf/AdobePdfContainer;->CountPages()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

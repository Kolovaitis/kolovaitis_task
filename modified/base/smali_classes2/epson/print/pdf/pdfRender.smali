.class public Lepson/print/pdf/pdfRender;
.super Ljava/lang/Object;
.source "pdfRender.java"

# interfaces
.implements Lepson/print/CommonDefine;


# static fields
.field private static final DEF_ZOOM_PREVIEW:F = 0.5f

.field private static final DEF_ZOOM_PRINT:F = 1.5f

.field private static final TAG:Ljava/lang/String; = "PDF_CONVERTER"


# instance fields
.field private backgroundThread:Ljava/lang/Thread;

.field private mContext:Landroid/content/Context;

.field private mConvertedName:Ljava/lang/String;

.field private mConvertedNamePrint:Ljava/lang/String;

.field protected volatile mIsPreviewCon:Z

.field protected mOpenError:Z

.field private mPdfFile:Lepson/print/pdf/AdobePdfContainer;

.field private mPdfFilename:Ljava/lang/String;

.field protected mPreviewmode:Z

.field protected volatile mPrintingCon:Z

.field protected volatile mRendererStart:Z

.field protected mRotate:Z

.field protected mRotateCheck:Z

.field public mTotalPage:I

.field private mUiHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mPreviewmode:Z

    const/4 v1, 0x0

    .line 35
    iput-object v1, p0, Lepson/print/pdf/pdfRender;->mContext:Landroid/content/Context;

    .line 52
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRotate:Z

    .line 53
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRotateCheck:Z

    .line 54
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mOpenError:Z

    .line 56
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRendererStart:Z

    .line 59
    iput-object p1, p0, Lepson/print/pdf/pdfRender;->mContext:Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lepson/print/pdf/pdfRender;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/print/pdf/pdfRender;)Lepson/print/pdf/AdobePdfContainer;
    .locals 0

    .line 26
    iget-object p0, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    return-object p0
.end method

.method static synthetic access$100(Lepson/print/pdf/pdfRender;)Landroid/os/Handler;
    .locals 0

    .line 26
    iget-object p0, p0, Lepson/print/pdf/pdfRender;->mUiHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$202(Lepson/print/pdf/pdfRender;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    .line 26
    iput-object p1, p0, Lepson/print/pdf/pdfRender;->backgroundThread:Ljava/lang/Thread;

    return-object p1
.end method

.method private convertPage(IFII)Z
    .locals 6

    .line 237
    iget-object p2, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    const/4 p4, 0x1

    if-eqz p2, :cond_b

    .line 241
    :try_start_0
    invoke-virtual {p2, p1}, Lepson/print/pdf/AdobePdfContainer;->GetPageSizeX(I)I

    move-result p2

    int-to-float p2, p2

    .line 242
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {v0, p1}, Lepson/print/pdf/AdobePdfContainer;->GetPageSizeY(I)I

    move-result v0

    int-to-float v0, v0

    .line 243
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lepson/print/pdf/pdfRender;->mPdfFilename:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lepson/print/pdf/pdfRender;->mTotalPage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PDF_CONVERTER"

    .line 245
    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    if-ne p1, p4, :cond_1

    cmpl-float v2, p2, v0

    if-lez v2, :cond_0

    .line 250
    iput-boolean p4, p0, Lepson/print/pdf/pdfRender;->mRotate:Z

    goto :goto_0

    .line 253
    :cond_0
    iput-boolean v1, p0, Lepson/print/pdf/pdfRender;->mRotate:Z

    .line 257
    :goto_0
    iput-boolean p4, p0, Lepson/print/pdf/pdfRender;->mRotateCheck:Z

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    cmpl-float v2, p2, v0

    if-lez v2, :cond_3

    .line 260
    iget-boolean v2, p0, Lepson/print/pdf/pdfRender;->mRotate:Z

    if-nez v2, :cond_2

    const/16 v2, 0x5a

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 266
    :cond_3
    iget-boolean v2, p0, Lepson/print/pdf/pdfRender;->mRotate:Z

    if-nez v2, :cond_4

    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    const/16 v2, 0x10e

    .line 288
    :goto_1
    iget-boolean v3, p0, Lepson/print/pdf/pdfRender;->mPreviewmode:Z

    if-nez v3, :cond_5

    const/16 v3, 0x1071

    goto :goto_2

    :cond_5
    const/16 v3, 0x2be

    :goto_2
    cmpl-float v4, p2, v0

    int-to-float v3, v3

    div-float v4, v3, p2

    div-float/2addr v3, v0

    cmpl-float v5, v4, v3

    if-lez v5, :cond_6

    goto :goto_3

    :cond_6
    move v3, v4

    :goto_3
    mul-float p2, p2, v3

    mul-float v3, v3, v0

    const-string v0, "PDF_CONVERTER"

    .line 332
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "scaledWidth = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v5, ",scaledHeight = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    invoke-static {}, Ljava/lang/System;->gc()V

    const-string v0, ""

    .line 336
    iput-object v0, p0, Lepson/print/pdf/pdfRender;->mConvertedName:Ljava/lang/String;

    const/4 v0, 0x6

    if-eq p3, p4, :cond_8

    .line 340
    iget-object p3, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    float-to-int p2, p2

    float-to-int v3, v3

    invoke-virtual {p3, p1, p2, v3, v2}, Lepson/print/pdf/AdobePdfContainer;->getPageBitmap(IIII)Landroid/graphics/Bitmap;

    move-result-object p2

    if-nez p2, :cond_7

    .line 343
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v1

    .line 347
    :cond_7
    new-instance p3, Ljava/io/File;

    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPdfDir()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ".jpg"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/pdf/pdfRender;->mConvertedName:Ljava/lang/String;

    .line 350
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mConvertedName:Ljava/lang/String;

    invoke-direct {p0, p2, p1}, Lepson/print/pdf/pdfRender;->saveImageToJpg(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 351
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    const-string p1, "PDF_CONVERTER"

    .line 354
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "saveImageToJpg Finish : "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lepson/print/pdf/pdfRender;->mConvertedName:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 359
    :cond_8
    iget-object p3, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    float-to-int p2, p2

    float-to-int v3, v3

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    goto :goto_4

    :cond_9
    const/4 v2, 0x0

    :goto_4
    invoke-virtual {p3, p1, p2, v3, v2}, Lepson/print/pdf/AdobePdfContainer;->getPageBitmapFile(IIIZ)Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_a

    .line 362
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v1

    .line 367
    :cond_a
    invoke-virtual {p0, p1}, Lepson/print/pdf/pdfRender;->getDecodeJpegFilename(I)Ljava/lang/String;

    move-result-object p1

    .line 368
    new-instance p3, Ljava/io/File;

    invoke-direct {p3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 369
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 370
    invoke-virtual {p3, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    const-string p2, "PDF_CONVERTER"

    .line 372
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "renameTo Finish : "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception p1

    const-string p2, "PDF_CONVERTER"

    .line 378
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "GOT ERROR WHEN CONVERT: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    :goto_5
    return p4
.end method

.method private parsePDF(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    const/4 v0, 0x6

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 425
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 427
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    const-string p1, "PDF_CONVERTER"

    const-string p2, "file is empty"

    .line 429
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_0
    const-string v5, "PDF_CONVERTER"

    .line 432
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lepson/print/pdf/pdfRender;->mPdfFilename:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "has"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, "byte"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    invoke-virtual {p0, p1, p2}, Lepson/print/pdf/pdfRender;->openFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lepson/print/pdf/AdobePdfContainer$PasswordException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 441
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 442
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 443
    iput-boolean v1, p0, Lepson/print/pdf/pdfRender;->mOpenError:Z

    goto :goto_0

    :catch_1
    const-string p1, "PDF_CONVERTER"

    const-string p2, "Password Locked"

    .line 438
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    :goto_0
    return v2
.end method

.method private readBytes(Ljava/io/File;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 459
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v1, v0

    .line 461
    new-array v0, v1, [B

    .line 462
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    add-int/lit8 p1, v1, 0x0

    const/4 v3, 0x0

    .line 464
    invoke-virtual {v2, v0, v3, p1}, Ljava/io/FileInputStream;->read([BII)I

    move-result p1

    :goto_0
    if-lez p1, :cond_0

    add-int/2addr v3, p1

    sub-int p1, v1, v3

    .line 467
    invoke-virtual {v2, v0, v3, p1}, Ljava/io/FileInputStream;->read([BII)I

    move-result p1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private saveImageToJpg(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 4

    .line 398
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 401
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 403
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 404
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 405
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    .line 407
    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 408
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 409
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    const-string p1, "PDF_CONVERTER"

    .line 410
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " converted"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 414
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public RendererStart()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRendererStart:Z

    return v0
.end method

.method public StartReloadDocument(Ljava/lang/String;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public canPrintableDoc()Z
    .locals 1

    .line 141
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {v0}, Lepson/print/pdf/AdobePdfContainer;->hasPrintPermission()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public convert4Preview(I)V
    .locals 4

    .line 151
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPdfDir()V

    const/4 v0, 0x1

    .line 152
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mIsPreviewCon:Z

    const/4 v1, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 154
    invoke-direct {p0, p1, v2, v1, v0}, Lepson/print/pdf/pdfRender;->convertPage(IFII)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "PDF_CONVERTER"

    .line 157
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "convertPage Preview Finish :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0xc8

    .line 160
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 163
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 166
    :goto_0
    iget-boolean v0, p0, Lepson/print/pdf/pdfRender;->mIsPreviewCon:Z

    if-eqz v0, :cond_1

    .line 169
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "CONVERTED_PAGE"

    .line 170
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 171
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 172
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 173
    iput v1, p1, Landroid/os/Message;->what:I

    .line 174
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :cond_1
    const-string p1, "PDF_CONVERTER"

    const-string v0, "Convert Preview NG : mIsPreviewCon = false"

    .line 177
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->initPdfDir()V

    :goto_1
    return-void
.end method

.method public convert4Print(II)V
    .locals 4

    .line 183
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    const/4 v0, 0x1

    .line 184
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mPrintingCon:Z

    const/4 v1, 0x1

    .line 187
    :goto_0
    iget-boolean v2, p0, Lepson/print/pdf/pdfRender;->mPrintingCon:Z

    if-ne v2, v0, :cond_2

    add-int/lit8 v2, p1, -0x1

    add-int/lit8 v3, p2, -0x1

    if-gt v2, v3, :cond_1

    .line 191
    :try_start_0
    iget-boolean v2, p0, Lepson/print/pdf/pdfRender;->mPrintingCon:Z

    if-eqz v2, :cond_1

    .line 192
    iget-object v2, p0, Lepson/print/pdf/pdfRender;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->initPdfDir()V

    const/high16 v2, 0x3fc00000    # 1.5f

    .line 194
    invoke-direct {p0, p1, v2, v0, v1}, Lepson/print/pdf/pdfRender;->convertPage(IFII)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 197
    :cond_0
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRendererStart:Z

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 208
    iput-boolean v2, p0, Lepson/print/pdf/pdfRender;->mPrintingCon:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 213
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public finalize()V
    .locals 0

    return-void
.end method

.method public getConvertedPagePreview()Ljava/lang/String;
    .locals 1

    .line 494
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mConvertedName:Ljava/lang/String;

    return-object v0
.end method

.method public getConvertedPagePrint()Ljava/lang/String;
    .locals 1

    .line 490
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mConvertedNamePrint:Ljava/lang/String;

    return-object v0
.end method

.method public getDecodeJpegFilename(I)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 392
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lepson/print/pdf/pdfRender;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ".jpg"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isFileExit()Z
    .locals 1

    .line 473
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public openFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/pdf/AdobePdfContainer$PasswordException;,
            Lepson/print/pdf/AdobePdfContainer$NoPdfException;
        }
    .end annotation

    .line 454
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {v0, p1, p2}, Lepson/print/pdf/AdobePdfContainer;->LoadDocument(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public openPdfFile(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 2

    const/4 v0, 0x0

    .line 106
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRotateCheck:Z

    .line 107
    new-instance v1, Lepson/print/pdf/AdobePdfContainer;

    invoke-direct {v1, p3}, Lepson/print/pdf/AdobePdfContainer;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    .line 111
    iput-object p1, p0, Lepson/print/pdf/pdfRender;->mPdfFilename:Ljava/lang/String;

    .line 114
    :try_start_0
    invoke-direct {p0, p1, p2}, Lepson/print/pdf/pdfRender;->parsePDF(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 117
    :goto_0
    :try_start_1
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    .line 118
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    invoke-virtual {p1}, Lepson/print/pdf/AdobePdfContainer;->CountPages()I

    move-result p1

    iput p1, p0, Lepson/print/pdf/pdfRender;->mTotalPage:I

    .line 119
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->mUiHandler:Landroid/os/Handler;

    const/4 p3, 0x2

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 p2, 0x0

    goto :goto_2

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    const/4 p2, 0x0

    :goto_1
    const-string p3, "PDF_CONVERTER"

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GOT ERROR WHEN CONVERT: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_2
    return p2
.end method

.method public rotate()Z
    .locals 2

    .line 222
    :cond_0
    iget-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRotateCheck:Z

    if-nez v0, :cond_1

    const-wide/16 v0, 0xa

    .line 224
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 227
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 229
    :goto_0
    iget-boolean v0, p0, Lepson/print/pdf/pdfRender;->mOpenError:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 233
    :cond_1
    iget-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRotate:Z

    return v0
.end method

.method public setMIsPreviewCon(Z)V
    .locals 0

    .line 524
    iput-boolean p1, p0, Lepson/print/pdf/pdfRender;->mIsPreviewCon:Z

    return-void
.end method

.method public setPrintingStt(Z)V
    .locals 2

    const-wide/16 v0, 0xc8

    .line 499
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 502
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 504
    :goto_0
    iput-boolean p1, p0, Lepson/print/pdf/pdfRender;->mPrintingCon:Z

    if-nez p1, :cond_0

    .line 511
    :goto_1
    :try_start_1
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->backgroundThread:Ljava/lang/Thread;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lepson/print/pdf/pdfRender;->backgroundThread:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-eqz p1, :cond_0

    const-wide/16 v0, 0x7d0

    .line 512
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    const-string p1, "Convert Preview"

    const-string v0, "convert4Print shutdown..."

    .line 513
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    :cond_0
    return-void
.end method

.method public declared-synchronized startConvertPage(III)V
    .locals 2

    monitor-enter p0

    .line 64
    :try_start_0
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->backgroundThread:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 65
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 72
    :try_start_1
    iput-boolean v0, p0, Lepson/print/pdf/pdfRender;->mRendererStart:Z

    .line 74
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lepson/print/pdf/pdfRender$1;

    invoke-direct {v1, p0, p3, p1, p2}, Lepson/print/pdf/pdfRender$1;-><init>(Lepson/print/pdf/pdfRender;III)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lepson/print/pdf/pdfRender;->backgroundThread:Ljava/lang/Thread;

    .line 97
    iget-object p1, p0, Lepson/print/pdf/pdfRender;->backgroundThread:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public totalPages()I
    .locals 1

    .line 481
    iget-object v0, p0, Lepson/print/pdf/pdfRender;->mPdfFile:Lepson/print/pdf/AdobePdfContainer;

    if-eqz v0, :cond_0

    .line 482
    invoke-virtual {v0}, Lepson/print/pdf/AdobePdfContainer;->CountPages()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

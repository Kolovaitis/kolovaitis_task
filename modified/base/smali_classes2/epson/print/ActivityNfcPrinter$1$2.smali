.class Lepson/print/ActivityNfcPrinter$1$2;
.super Landroid/os/AsyncTask;
.source "ActivityNfcPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityNfcPrinter$1;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/ActivityNfcPrinter$1;


# direct methods
.method constructor <init>(Lepson/print/ActivityNfcPrinter$1;)V
    .locals 0

    .line 530
    iput-object p1, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2

    .line 541
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 544
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setHanlder(Landroid/os/Handler;)V

    .line 549
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    const/16 v0, 0xc0

    const/16 v1, 0x3c

    invoke-virtual {p1, v0, v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doFindPrinter(II)I

    move-result p1

    .line 557
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 530
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter$1$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    .line 562
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$500(Lepson/print/ActivityNfcPrinter;)Lepson/print/MyPrinter;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 565
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    .line 566
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/ActivityNfcPrinter;->access$500(Lepson/print/ActivityNfcPrinter;)Lepson/print/MyPrinter;

    move-result-object v0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 568
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/ActivityNfcPrinter;->access$700(Lepson/print/ActivityNfcPrinter;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0xd

    .line 577
    iput v0, p1, Landroid/os/Message;->what:I

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xc

    .line 572
    iput v0, p1, Landroid/os/Message;->what:I

    .line 581
    :goto_0
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 585
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$2;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v0, 0x7f0e01b2

    const v1, 0x7f0e01a7

    invoke-virtual {p1, v0, v1}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 530
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter$1$2;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

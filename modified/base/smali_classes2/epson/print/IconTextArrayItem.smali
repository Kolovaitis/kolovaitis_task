.class public Lepson/print/IconTextArrayItem;
.super Ljava/lang/Object;
.source "IconTextArrayItem.java"

# interfaces
.implements Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;


# instance fields
.field AppDiscId:I

.field ClassName:Ljava/lang/String;

.field PackageName:Ljava/lang/String;

.field appId:Ljava/lang/String;

.field private columnSpan:I

.field googleStoreUrl:Ljava/lang/String;

.field imageID:I

.field isInstall:Z

.field isRemoteAvailable:Z

.field key:Ljava/lang/String;

.field menuId:I

.field private rowSpan:I

.field showMenu:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 32
    iput v0, p0, Lepson/print/IconTextArrayItem;->imageID:I

    .line 33
    iput v0, p0, Lepson/print/IconTextArrayItem;->menuId:I

    const/4 v0, 0x1

    .line 34
    iput-boolean v0, p0, Lepson/print/IconTextArrayItem;->showMenu:Z

    .line 35
    iput v0, p0, Lepson/print/IconTextArrayItem;->columnSpan:I

    .line 36
    iput v0, p0, Lepson/print/IconTextArrayItem;->rowSpan:I

    return-void
.end method

.method constructor <init>(IILjava/lang/String;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lepson/print/IconTextArrayItem;->imageID:I

    .line 42
    iput p2, p0, Lepson/print/IconTextArrayItem;->menuId:I

    const-string p1, "epson.print"

    .line 43
    iput-object p1, p0, Lepson/print/IconTextArrayItem;->PackageName:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lepson/print/IconTextArrayItem;->ClassName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 45
    iput-boolean p1, p0, Lepson/print/IconTextArrayItem;->isRemoteAvailable:Z

    .line 46
    iput-boolean p1, p0, Lepson/print/IconTextArrayItem;->showMenu:Z

    .line 47
    iput p1, p0, Lepson/print/IconTextArrayItem;->columnSpan:I

    .line 48
    iput p1, p0, Lepson/print/IconTextArrayItem;->rowSpan:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getColumnSpan()I
    .locals 1

    .line 53
    iget v0, p0, Lepson/print/IconTextArrayItem;->columnSpan:I

    return v0
.end method

.method public getRowSpan()I
    .locals 1

    .line 58
    iget v0, p0, Lepson/print/IconTextArrayItem;->rowSpan:I

    return v0
.end method

.method public setColumnSpan(I)V
    .locals 0

    .line 62
    iput p1, p0, Lepson/print/IconTextArrayItem;->columnSpan:I

    return-void
.end method

.method public setRowSpan(I)V
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/IconTextArrayItem;->rowSpan:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method

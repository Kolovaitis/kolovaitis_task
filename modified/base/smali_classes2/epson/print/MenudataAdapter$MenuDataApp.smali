.class Lepson/print/MenudataAdapter$MenuDataApp;
.super Ljava/lang/Object;
.source "MenudataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/MenudataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MenuDataApp"
.end annotation


# static fields
.field private static final VERSIONCODE_3D:I = 0x7594

.field private static final VERSIONCODE_CREATIVE:I = 0xea60

.field private static final VERSIONCODE_MEISHI:I = 0xf4628

.field private static final VERSIONCODE_MROOL:I = 0x1ed2a0

.field public static final VERSIONCODE_NENGA:I = 0xc08bfe0


# instance fields
.field ClassName:Ljava/lang/String;

.field PackageName:Ljava/lang/String;

.field appDescription:Ljava/lang/String;

.field appID:Ljava/lang/String;

.field appfunctions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataFunction;",
            ">;"
        }
    .end annotation
.end field

.field googlePlayURL:Ljava/lang/String;

.field icon:Ljava/lang/String;

.field isInstalled:Ljava/lang/Boolean;

.field isRemoteAvailable:Z

.field name:Ljava/lang/String;

.field final synthetic this$0:Lepson/print/MenudataAdapter;


# direct methods
.method constructor <init>(Lepson/print/MenudataAdapter;)V
    .locals 0

    .line 607
    iput-object p1, p0, Lepson/print/MenudataAdapter$MenuDataApp;->this$0:Lepson/print/MenudataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isInstalled(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 3

    const-string v0, "isInstalled"

    const-string v1, "OK"

    .line 632
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 634
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iget-object v1, p0, Lepson/print/MenudataAdapter$MenuDataApp;->PackageName:Ljava/lang/String;

    const/16 v2, 0x80

    invoke-virtual {p1, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p1

    const-string v1, "Creative"

    .line 637
    iget-object v2, p0, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0xea60

    .line 638
    iget v2, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    if-le v1, v2, :cond_0

    .line 639
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    const-string v1, "3DFramePrint"

    .line 644
    iget-object v2, p0, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x7594

    .line 645
    iget v2, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    if-le v1, v2, :cond_1

    .line 646
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_1
    const-string v1, "CardPrint"

    .line 651
    iget-object v2, p0, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0xf4628

    .line 652
    iget v2, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    if-le v1, v2, :cond_2

    .line 653
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_2
    const-string v1, "MultiRollPrint"

    .line 658
    iget-object v2, p0, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x1ed2a0

    .line 659
    iget p1, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    if-le v1, p1, :cond_3

    .line 660
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_3
    const/4 p1, 0x1

    .line 674
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 672
    :catch_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.class public Lepson/print/widgets/PrinterInfoDetailBuilder;
.super Lepson/print/widgets/AbstractListBuilder;
.source "PrinterInfoDetailBuilder.java"


# static fields
.field private static final ACTIVE:I = 0x1

.field private static final NAME:I


# instance fields
.field curValue:I

.field private mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lepson/print/widgets/AbstractListBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    const/4 p1, -0x1

    .line 70
    iput p1, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->curValue:I

    return-void
.end method


# virtual methods
.method public addPrinter([Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public addPrinterInfo([ILcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)V
    .locals 3

    .line 82
    iput-object p2, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const/4 p2, 0x0

    .line 83
    :goto_0
    array-length v0, p1

    if-ge p2, v0, :cond_0

    .line 84
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->mData:Ljava/util/Vector;

    new-instance v1, Lepson/print/widgets/CommonDataKinds$PrinterInfo;

    aget v2, p1, p2

    invoke-direct {v1, v2}, Lepson/print/widgets/CommonDataKinds$PrinterInfo;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 86
    :cond_0
    iget-object p1, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected buildData()V
    .locals 1

    .line 27
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void
.end method

.method public buildViewHolder()Landroid/view/View;
    .locals 3

    .line 37
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a00ac

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public buildViewHolderContent(Landroid/view/View;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const v1, 0x7f080222

    .line 43
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f080050

    .line 44
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public buildViewHolderContentData(ILjava/util/Vector;Ljava/util/Vector;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/widgets/CommonDataKinds$PrinterInfo;

    invoke-virtual {p1}, Lepson/print/widgets/CommonDataKinds$PrinterInfo;->getValue()I

    move-result p1

    .line 52
    iget-object p3, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-eq p3, v1, :cond_0

    .line 55
    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const/4 p3, 0x1

    .line 59
    invoke-virtual {p2, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    iget p3, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->curValue:I

    if-ne p1, p3, :cond_1

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public destructor()V
    .locals 1

    .line 94
    invoke-super {p0}, Lepson/print/widgets/AbstractListBuilder;->destructor()V

    .line 95
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    :cond_0
    return-void
.end method

.method public getIndexer()Landroid/widget/AlphabetIndexer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected notifyDataChange()V
    .locals 0

    return-void
.end method

.method public refresh()V
    .locals 0

    .line 90
    invoke-virtual {p0}, Lepson/print/widgets/PrinterInfoDetailBuilder;->buildData()V

    return-void
.end method

.method public setResource(Ljava/lang/Object;)V
    .locals 0

    .line 74
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lepson/print/widgets/PrinterInfoDetailBuilder;->curValue:I

    return-void
.end method

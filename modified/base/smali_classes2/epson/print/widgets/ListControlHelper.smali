.class public Lepson/print/widgets/ListControlHelper;
.super Ljava/lang/Object;
.source "ListControlHelper.java"

# interfaces
.implements Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;


# instance fields
.field builder:Lepson/print/widgets/PrinterInfoBuilder;


# direct methods
.method public constructor <init>(Lepson/print/widgets/PrinterInfoBuilder;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lepson/print/widgets/ListControlHelper;->builder:Lepson/print/widgets/PrinterInfoBuilder;

    return-void
.end method


# virtual methods
.method public addPrinter(Ljava/lang/Object;I)V
    .locals 0

    .line 21
    check-cast p1, Lepson/print/MyPrinter;

    .line 22
    invoke-virtual {p1, p2}, Lepson/print/MyPrinter;->setPrinterIndex(I)V

    .line 23
    iget-object p2, p0, Lepson/print/widgets/ListControlHelper;->builder:Lepson/print/widgets/PrinterInfoBuilder;

    invoke-virtual {p2, p1}, Lepson/print/widgets/PrinterInfoBuilder;->addPrinter(Lepson/print/MyPrinter;)V

    return-void
.end method

.method public replacePrinter(ILjava/lang/Object;I)V
    .locals 0

    .line 28
    check-cast p2, Lepson/print/MyPrinter;

    .line 29
    invoke-virtual {p2, p3}, Lepson/print/MyPrinter;->setPrinterIndex(I)V

    .line 30
    iget-object p3, p0, Lepson/print/widgets/ListControlHelper;->builder:Lepson/print/widgets/PrinterInfoBuilder;

    invoke-virtual {p3, p2, p1}, Lepson/print/widgets/PrinterInfoBuilder;->replacePrinter(Lepson/print/MyPrinter;I)V

    return-void
.end method

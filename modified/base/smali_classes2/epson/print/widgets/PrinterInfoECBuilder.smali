.class public Lepson/print/widgets/PrinterInfoECBuilder;
.super Lepson/print/widgets/AbstractListBuilder;
.source "PrinterInfoECBuilder.java"


# static fields
.field private static final ACTIVE:I = 0x4

.field private static final IP:I = 0x1

.field private static final NAME:I = 0x0

.field private static final PRINT_LOCAL:I = 0x3

.field private static final PRINT_REMOTE:I = 0x2


# instance fields
.field curPrinterId:Ljava/lang/String;

.field private printerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/EPPrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private printerManager:Lepson/print/EPPrinterManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lepson/print/widgets/AbstractListBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    const/4 p1, 0x0

    .line 23
    iput-object p1, p0, Lepson/print/widgets/PrinterInfoECBuilder;->printerManager:Lepson/print/EPPrinterManager;

    .line 24
    iput-object p1, p0, Lepson/print/widgets/PrinterInfoECBuilder;->printerList:Ljava/util/ArrayList;

    .line 131
    iput-object p1, p0, Lepson/print/widgets/PrinterInfoECBuilder;->curPrinterId:Ljava/lang/String;

    .line 29
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object p2, p0, Lepson/print/widgets/PrinterInfoECBuilder;->mContext:Landroid/content/Context;

    invoke-direct {p1, p2}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/widgets/PrinterInfoECBuilder;->printerManager:Lepson/print/EPPrinterManager;

    return-void
.end method


# virtual methods
.method public addPrinter([Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected buildData()V
    .locals 10

    .line 34
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoECBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 37
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoECBuilder;->printerManager:Lepson/print/EPPrinterManager;

    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->getRemotePrinterList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lepson/print/widgets/PrinterInfoECBuilder;->printerList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 38
    :goto_0
    iget-object v1, p0, Lepson/print/widgets/PrinterInfoECBuilder;->printerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 39
    iget-object v1, p0, Lepson/print/widgets/PrinterInfoECBuilder;->printerList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/EPPrinterInfo;

    .line 40
    iget-object v2, p0, Lepson/print/widgets/PrinterInfoECBuilder;->mData:Ljava/util/Vector;

    new-instance v9, Lepson/print/MyPrinter;

    iget-object v4, v1, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    iget-object v5, v1, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    iget-object v6, v1, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    iget-object v7, v1, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    iget-object v8, v1, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_0
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoECBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public buildViewHolder()Landroid/view/View;
    .locals 3

    .line 52
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoECBuilder;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a005e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public buildViewHolderContent(Landroid/view/View;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 57
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const v1, 0x7f080222

    .line 58
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f0801aa

    .line 65
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f08027a

    .line 67
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f080277

    .line 68
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f080050

    .line 69
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public buildViewHolderContentData(ILjava/util/Vector;Ljava/util/Vector;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 79
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    iget-object v1, p0, Lepson/print/widgets/PrinterInfoECBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lepson/print/MyPrinter;->getUserDefName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 80
    invoke-virtual {p2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x4

    .line 83
    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 84
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/print/widgets/PrinterInfoECBuilder;->curPrinterId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/16 v3, 0x8

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    .line 83
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x3

    .line 87
    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x2

    .line 88
    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    .line 91
    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 92
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    .line 91
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public destructor()V
    .locals 1

    .line 185
    invoke-super {p0}, Lepson/print/widgets/AbstractListBuilder;->destructor()V

    const/4 v0, 0x0

    .line 187
    iput-object v0, p0, Lepson/print/widgets/PrinterInfoECBuilder;->printerList:Ljava/util/ArrayList;

    return-void
.end method

.method public getIndexer()Landroid/widget/AlphabetIndexer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected notifyDataChange()V
    .locals 0

    return-void
.end method

.method public setResource(Ljava/lang/Object;)V
    .locals 0

    .line 135
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lepson/print/widgets/PrinterInfoECBuilder;->curPrinterId:Ljava/lang/String;

    return-void
.end method

.class public Lepson/print/widgets/LongTapRepeatAdapter;
.super Ljava/lang/Object;
.source "LongTapRepeatAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;
    }
.end annotation


# static fields
.field private static final REPEAT_INTERVAL:I = 0x64


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bless(ILandroid/view/View;)V
    .locals 3

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 31
    new-instance v1, Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;-><init>(Z)V

    .line 33
    new-instance v2, Lepson/print/widgets/LongTapRepeatAdapter$1;

    invoke-direct {v2, p1, v1, v0, p0}, Lepson/print/widgets/LongTapRepeatAdapter$1;-><init>(Landroid/view/View;Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;Landroid/os/Handler;I)V

    .line 53
    new-instance p0, Lepson/print/widgets/LongTapRepeatAdapter$2;

    invoke-direct {p0, v1, v0, v2}, Lepson/print/widgets/LongTapRepeatAdapter$2;-><init>(Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;Landroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 64
    new-instance p0, Lepson/print/widgets/LongTapRepeatAdapter$3;

    invoke-direct {p0, v1}, Lepson/print/widgets/LongTapRepeatAdapter$3;-><init>(Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public static bless(Landroid/view/View;)V
    .locals 1

    const/16 v0, 0x64

    .line 19
    invoke-static {v0, p0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(ILandroid/view/View;)V

    return-void
.end method

.class public Lepson/print/widgets/SwichClickable;
.super Landroid/widget/Switch;
.source "SwichClickable.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SwichClickable"


# instance fields
.field onClicklistener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lepson/print/widgets/SwichClickable;->onClicklistener:Landroid/view/View$OnClickListener;

    .line 26
    invoke-virtual {p0, p0}, Lepson/print/widgets/SwichClickable;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/Switch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lepson/print/widgets/SwichClickable;->onClicklistener:Landroid/view/View$OnClickListener;

    .line 31
    invoke-virtual {p0, p0}, Lepson/print/widgets/SwichClickable;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Switch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lepson/print/widgets/SwichClickable;->onClicklistener:Landroid/view/View$OnClickListener;

    .line 36
    invoke-virtual {p0, p0}, Lepson/print/widgets/SwichClickable;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 41
    iget-object p2, p0, Lepson/print/widgets/SwichClickable;->onClicklistener:Landroid/view/View$OnClickListener;

    if-eqz p2, :cond_0

    .line 42
    invoke-interface {p2, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lepson/print/widgets/SwichClickable;->onClicklistener:Landroid/view/View$OnClickListener;

    return-void
.end method

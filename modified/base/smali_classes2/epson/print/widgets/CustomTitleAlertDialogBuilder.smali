.class public Lepson/print/widgets/CustomTitleAlertDialogBuilder;
.super Landroid/app/AlertDialog$Builder;
.source "CustomTitleAlertDialogBuilder.java"


# static fields
.field private static final TITLES_COLOR:I = 0x7f05001a

.field private static final TITLES_LINES:I = 0x5


# instance fields
.field titleView:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 29
    iput-object p1, p0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->titleView:Landroid/view/ViewGroup;

    .line 33
    invoke-virtual {p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setUpCustomTitle()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 p1, 0x0

    .line 29
    iput-object p1, p0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->titleView:Landroid/view/ViewGroup;

    .line 38
    invoke-virtual {p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setUpCustomTitle()V

    return-void
.end method


# virtual methods
.method public setCustomTitle(Ljava/lang/CharSequence;)V
    .locals 2

    .line 96
    iget-object v0, p0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->titleView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    const v1, 0x7f08005b

    .line 97
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setCustomTitleIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 109
    iget-object v0, p0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->titleView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    const v1, 0x7f080189

    .line 110
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setIcon(I)Landroid/app/AlertDialog$Builder;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 68
    invoke-super {p0, p1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCustomTitleIcon(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 60
    invoke-super {p0, p1}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {p0, p1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCustomTitleIcon(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public setTitle(I)Landroid/app/AlertDialog$Builder;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 52
    invoke-super {p0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 53
    invoke-virtual {p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCustomTitle(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 44
    invoke-super {p0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {p0, p1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCustomTitle(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method setUpCustomTitle()V
    .locals 3

    .line 78
    invoke-virtual {p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0a0041

    const/4 v2, 0x0

    .line 79
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->titleView:Landroid/view/ViewGroup;

    .line 80
    iget-object v0, p0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->titleView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0, v0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 82
    iget-object v0, p0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->titleView:Landroid/view/ViewGroup;

    const v1, 0x7f08005b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/4 v1, 0x5

    .line 84
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 85
    invoke-virtual {p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    return-void
.end method

.class Lepson/print/widgets/JWrapTextView$Hyphenation;
.super Ljava/lang/Object;
.source "JWrapTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/widgets/JWrapTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Hyphenation"
.end annotation


# static fields
.field private static final HYP_JPN_BACK:[C

.field private static final HYP_JPN_FRONT:[C

.field private static final HYP_JPN_FRONT_PUNCTUATION:[C

.field private static final NEWLINE_CHARA:C = '\n'

.field private static isPunctuationToEnd:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x32

    .line 52
    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lepson/print/widgets/JWrapTextView$Hyphenation;->HYP_JPN_FRONT:[C

    const/4 v0, 0x4

    .line 66
    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lepson/print/widgets/JWrapTextView$Hyphenation;->HYP_JPN_FRONT_PUNCTUATION:[C

    const/16 v0, 0xd

    .line 71
    new-array v0, v0, [C

    fill-array-data v0, :array_2

    sput-object v0, Lepson/print/widgets/JWrapTextView$Hyphenation;->HYP_JPN_BACK:[C

    const/4 v0, 0x0

    .line 80
    sput-boolean v0, Lepson/print/widgets/JWrapTextView$Hyphenation;->isPunctuationToEnd:Z

    return-void

    :array_0
    .array-data 2
        0x29s
        -0xf7s
        0x5ds
        -0xa3s
        0x3009s
        0x300bs
        0x300ds
        0x300fs
        0x3011s
        0x301fs
        0x2019s
        0x201ds
        0x226bs
        0x30fcs
        0x30a1s
        0x30a3s
        0x30a5s
        0x30a7s
        0x30a9s
        0x30c3s
        0x30e3s
        0x30e5s
        0x30e7s
        0x30ees
        0x30f5s
        0x30f6s
        0x3041s
        0x3043s
        0x3045s
        0x3047s
        0x3049s
        0x3063s
        0x3083s
        0x3085s
        0x3087s
        0x2212s
        0x2ds
        0x301cs
        0x3fs
        -0xe1s
        0x21s
        -0xffs
        -0x9bs
        0x30fbs
        0x2025s
        0x2026s
        0x3as
        0x3bs
        -0xe6s
        -0xe5s
    .end array-data

    :array_1
    .array-data 2
        0x3002s
        0x2es
        0x3001s
        0x2cs
    .end array-data

    :array_2
    .array-data 2
        0x28s
        -0xf8s
        0x5bs
        -0xa5s
        0x3008s
        0x300as
        0x300cs
        0x300es
        0x3010s
        0x301ds
        0x2018s
        0x201cs
        0x226as
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static japaneseHyphenation(Ljava/lang/String;Landroid/widget/TextView;)Ljava/lang/CharSequence;
    .locals 2

    .line 97
    invoke-virtual {p1}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 98
    invoke-static {p0, p1, v0}, Lepson/print/widgets/JWrapTextView$Hyphenation;->japaneseHyphenation(Ljava/lang/String;Landroid/widget/TextView;I)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static japaneseHyphenation(Ljava/lang/String;Landroid/widget/TextView;I)Ljava/lang/CharSequence;
    .locals 13

    .line 109
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object p1

    .line 110
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 115
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_b

    add-int/lit8 v4, v2, 0x1

    .line 118
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 119
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0xa

    const/4 v8, 0x1

    if-ne v6, v7, :cond_0

    .line 123
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v3, "\n"

    .line 124
    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move v3, v4

    goto/16 :goto_9

    .line 126
    :cond_0
    invoke-static {v5, p1}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v4

    int-to-float v5, p2

    cmpl-float v4, v4, v5

    if-lez v4, :cond_a

    const/4 v4, 0x0

    .line 128
    :goto_1
    sget-object v5, Lepson/print/widgets/JWrapTextView$Hyphenation;->HYP_JPN_FRONT:[C

    array-length v9, v5

    if-ge v4, v9, :cond_2

    .line 129
    aget-char v5, v5, v4

    if-ne v6, v5, :cond_1

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    const/4 v2, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    move v4, v2

    const/4 v2, 0x0

    .line 137
    :goto_2
    sget-boolean v5, Lepson/print/widgets/JWrapTextView$Hyphenation;->isPunctuationToEnd:Z

    if-nez v5, :cond_4

    if-nez v2, :cond_4

    const/4 v5, 0x0

    .line 138
    :goto_3
    sget-object v9, Lepson/print/widgets/JWrapTextView$Hyphenation;->HYP_JPN_FRONT_PUNCTUATION:[C

    array-length v10, v9

    if-ge v5, v10, :cond_4

    .line 139
    aget-char v9, v9, v5

    if-ne v6, v9, :cond_3

    add-int/lit8 v4, v4, -0x1

    const/4 v2, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_4
    :goto_4
    if-nez v2, :cond_6

    if-lez v4, :cond_6

    add-int/lit8 v5, v4, -0x1

    .line 149
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/4 v9, 0x0

    .line 150
    :goto_5
    sget-object v10, Lepson/print/widgets/JWrapTextView$Hyphenation;->HYP_JPN_BACK:[C

    array-length v11, v10

    if-ge v9, v11, :cond_6

    .line 151
    aget-char v10, v10, v9

    if-ne v5, v10, :cond_5

    add-int/lit8 v4, v4, -0x1

    const/4 v2, 0x1

    goto :goto_6

    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 161
    :cond_6
    :goto_6
    sget-boolean v5, Lepson/print/widgets/JWrapTextView$Hyphenation;->isPunctuationToEnd:Z

    if-eqz v5, :cond_8

    if-nez v2, :cond_8

    const/4 v2, 0x0

    .line 162
    :goto_7
    sget-object v5, Lepson/print/widgets/JWrapTextView$Hyphenation;->HYP_JPN_FRONT_PUNCTUATION:[C

    array-length v9, v5

    if-ge v2, v9, :cond_8

    .line 163
    aget-char v5, v5, v2

    if-ne v6, v5, :cond_7

    const/4 v2, 0x1

    goto :goto_8

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int v5, v4, v2

    .line 170
    invoke-virtual {p0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v3, "\n"

    .line 171
    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    add-int/2addr v2, v4

    .line 175
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 176
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v7, :cond_9

    add-int/lit8 v3, v4, 0x1

    add-int/lit8 v2, v2, 0x1

    move v12, v3

    move v3, v2

    move v2, v12

    goto :goto_9

    :cond_9
    move v3, v2

    move v2, v4

    :cond_a
    :goto_9
    add-int/2addr v2, v8

    goto/16 :goto_0

    .line 185
    :cond_b
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_c

    .line 186
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_c
    return-object v0
.end method

.method public static japaneseHyphenationInCharacters(Ljava/lang/String;Landroid/widget/TextView;I)Ljava/lang/CharSequence;
    .locals 2

    .line 200
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const-string v1, "\u3042"

    .line 201
    invoke-static {v1, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    float-to-int v0, v0

    mul-int v0, v0, p2

    .line 202
    invoke-static {p0, p1, v0}, Lepson/print/widgets/JWrapTextView$Hyphenation;->japaneseHyphenation(Ljava/lang/String;Landroid/widget/TextView;I)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static setIsPunctuationToEnd(Z)V
    .locals 0

    .line 87
    sput-boolean p0, Lepson/print/widgets/JWrapTextView$Hyphenation;->isPunctuationToEnd:Z

    return-void
.end method

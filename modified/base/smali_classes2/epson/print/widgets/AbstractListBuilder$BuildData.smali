.class Lepson/print/widgets/AbstractListBuilder$BuildData;
.super Landroid/os/AsyncTask;
.source "AbstractListBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/widgets/AbstractListBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BuildData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/widgets/AbstractListBuilder;


# direct methods
.method private constructor <init>(Lepson/print/widgets/AbstractListBuilder;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lepson/print/widgets/AbstractListBuilder$BuildData;->this$0:Lepson/print/widgets/AbstractListBuilder;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/widgets/AbstractListBuilder;Lepson/print/widgets/AbstractListBuilder$1;)V
    .locals 0

    .line 125
    invoke-direct {p0, p1}, Lepson/print/widgets/AbstractListBuilder$BuildData;-><init>(Lepson/print/widgets/AbstractListBuilder;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 125
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/widgets/AbstractListBuilder$BuildData;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    .line 151
    iget-object p1, p0, Lepson/print/widgets/AbstractListBuilder$BuildData;->this$0:Lepson/print/widgets/AbstractListBuilder;

    iget-object p1, p1, Lepson/print/widgets/AbstractListBuilder;->mData:Ljava/util/Vector;

    monitor-enter p1

    .line 152
    :try_start_0
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder$BuildData;->this$0:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->buildData()V

    .line 153
    monitor-exit p1

    const/4 p1, 0x0

    return-object p1

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 125
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/widgets/AbstractListBuilder$BuildData;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    .line 138
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 141
    iget-object p1, p0, Lepson/print/widgets/AbstractListBuilder$BuildData;->this$0:Lepson/print/widgets/AbstractListBuilder;

    iget-object p1, p1, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 142
    iget-object p1, p0, Lepson/print/widgets/AbstractListBuilder$BuildData;->this$0:Lepson/print/widgets/AbstractListBuilder;

    iget-object p1, p1, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->postInvalidate()V

    .line 144
    iget-object p1, p0, Lepson/print/widgets/AbstractListBuilder$BuildData;->this$0:Lepson/print/widgets/AbstractListBuilder;

    iget-object p1, p1, Lepson/print/widgets/AbstractListBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 145
    iget-object p1, p0, Lepson/print/widgets/AbstractListBuilder$BuildData;->this$0:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->notifyDataChange()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 130
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 133
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder$BuildData;->this$0:Lepson/print/widgets/AbstractListBuilder;

    iget-object v0, v0, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.class final Lepson/print/widgets/LongTapRepeatAdapter$1;
.super Ljava/lang/Object;
.source "LongTapRepeatAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/widgets/LongTapRepeatAdapter;->bless(ILandroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$handler:Landroid/os/Handler;

.field final synthetic val$isContinue:Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;

.field final synthetic val$repeatInterval:I

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;Landroid/os/Handler;I)V
    .locals 0

    .line 33
    iput-object p1, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$view:Landroid/view/View;

    iput-object p2, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$isContinue:Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;

    iput-object p3, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$handler:Landroid/os/Handler;

    iput p4, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$repeatInterval:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 37
    iget-object v0, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    iget-object v0, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$isContinue:Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;->value:Z

    .line 42
    :cond_0
    iget-object v0, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$isContinue:Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;

    iget-boolean v0, v0, Lepson/print/widgets/LongTapRepeatAdapter$BooleanWrapper;->value:Z

    if-nez v0, :cond_1

    return-void

    .line 47
    :cond_1
    iget-object v0, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 49
    iget-object v0, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$handler:Landroid/os/Handler;

    iget v1, p0, Lepson/print/widgets/LongTapRepeatAdapter$1;->val$repeatInterval:I

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

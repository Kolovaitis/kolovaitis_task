.class public Lepson/print/widgets/PrinterInfoIpBuilder;
.super Lepson/print/widgets/AbstractListBuilder;
.source "PrinterInfoIpBuilder.java"


# static fields
.field private static final ACTIVE:I = 0x2

.field private static final IP:I = 0x1

.field private static final NAME:I = 0x0

.field public static final PRINTER:I = 0x0

.field public static final SCANNER:I = 0x1


# instance fields
.field private kind:I

.field private printerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/EPPrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private printerManager:Lepson/print/EPPrinterManager;

.field selectedKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lepson/print/widgets/AbstractListBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    const/4 p1, 0x0

    .line 24
    iput-object p1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->printerManager:Lepson/print/EPPrinterManager;

    .line 25
    iput-object p1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->printerList:Ljava/util/ArrayList;

    const/4 p2, 0x0

    .line 27
    iput p2, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->kind:I

    .line 110
    iput-object p1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->selectedKey:Ljava/lang/String;

    .line 32
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object p2, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->mContext:Landroid/content/Context;

    invoke-direct {p1, p2}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->printerManager:Lepson/print/EPPrinterManager;

    .line 33
    iput p3, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->kind:I

    return-void
.end method


# virtual methods
.method public addPrinter([Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected buildData()V
    .locals 10

    .line 38
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 41
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->printerManager:Lepson/print/EPPrinterManager;

    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->printerList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 42
    :goto_0
    iget-object v1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->printerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 43
    iget-object v1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->printerList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/EPPrinterInfo;

    .line 45
    new-instance v9, Lepson/print/MyPrinter;

    iget-object v3, v1, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    iget-object v4, v1, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    iget-object v5, v1, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    iget-object v6, v1, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    iget-object v7, v1, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    const/4 v8, 0x3

    move-object v2, v9

    invoke-direct/range {v2 .. v8}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 47
    iget-object v1, v1, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    invoke-virtual {v9, v1}, Lepson/print/MyPrinter;->setScannerId(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v1, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public buildViewHolder()Landroid/view/View;
    .locals 3

    .line 62
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a00ad

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public buildViewHolderContent(Landroid/view/View;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 67
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const v1, 0x7f080222

    .line 68
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f0801aa

    .line 69
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f080050

    .line 70
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public buildViewHolderContentData(ILjava/util/Vector;Ljava/util/Vector;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 77
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    iget-object v1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lepson/print/MyPrinter;->getUserDefName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 78
    invoke-virtual {p2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget v0, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->kind:I

    const/4 v2, 0x1

    const/16 v3, 0x8

    const/4 v4, 0x2

    if-eq v0, v2, :cond_1

    .line 94
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 95
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lepson/print/MyPrinter;

    invoke-virtual {v4}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->selectedKey:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    .line 94
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 83
    :cond_1
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getScannerId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 85
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    iget-object v5, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->selectedKey:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v3, 0x0

    :cond_2
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 86
    invoke-virtual {p2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f05001a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 88
    :cond_3
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 89
    invoke-virtual {p2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f05007b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 99
    :goto_1
    invoke-virtual {p2, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 100
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p1

    .line 99
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public getIndexer()Landroid/widget/AlphabetIndexer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected notifyDataChange()V
    .locals 0

    return-void
.end method

.method public setResource(Ljava/lang/Object;)V
    .locals 0

    .line 114
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lepson/print/widgets/PrinterInfoIpBuilder;->selectedKey:Ljava/lang/String;

    return-void
.end method

.class public Lepson/print/widgets/MyAdapter;
.super Landroid/widget/BaseAdapter;
.source "MyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/widgets/MyAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mBuilder:Lepson/print/widgets/AbstractListBuilder;


# direct methods
.method public constructor <init>(Lepson/print/widgets/AbstractListBuilder;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 14
    iput-object p1, p0, Lepson/print/widgets/MyAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 18
    iget-object v0, p0, Lepson/print/widgets/MyAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 22
    iget-object v0, p0, Lepson/print/widgets/MyAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    .line 36
    new-instance p2, Lepson/print/widgets/MyAdapter$ViewHolder;

    const/4 p3, 0x0

    invoke-direct {p2, p3}, Lepson/print/widgets/MyAdapter$ViewHolder;-><init>(Lepson/print/widgets/MyAdapter$1;)V

    .line 37
    iget-object p3, p0, Lepson/print/widgets/MyAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p3}, Lepson/print/widgets/AbstractListBuilder;->buildViewHolder()Landroid/view/View;

    move-result-object p3

    .line 38
    iget-object v0, p0, Lepson/print/widgets/MyAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0, p3}, Lepson/print/widgets/AbstractListBuilder;->buildViewHolderContent(Landroid/view/View;)Ljava/util/Vector;

    move-result-object v0

    iput-object v0, p2, Lepson/print/widgets/MyAdapter$ViewHolder;->views:Ljava/util/Vector;

    .line 39
    invoke-virtual {p3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lepson/print/widgets/MyAdapter$ViewHolder;

    move-object v2, p3

    move-object p3, p2

    move-object p2, v2

    .line 43
    :goto_0
    iget-object v0, p0, Lepson/print/widgets/MyAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    iget-object p2, p2, Lepson/print/widgets/MyAdapter$ViewHolder;->views:Ljava/util/Vector;

    iget-object v1, p0, Lepson/print/widgets/MyAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v1}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lepson/print/widgets/AbstractListBuilder;->buildViewHolderContentData(ILjava/util/Vector;Ljava/util/Vector;)V

    return-object p3
.end method

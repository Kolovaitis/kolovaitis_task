.class public abstract Lepson/print/widgets/AbstractBuilder;
.super Ljava/lang/Object;
.source "AbstractBuilder.java"


# instance fields
.field protected mLayout:Landroid/view/ViewGroup;

.field protected mLayoutInflater:Landroid/view/LayoutInflater;

.field protected mParentView:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p2, p0, Lepson/print/widgets/AbstractBuilder;->mParentView:Landroid/view/ViewGroup;

    .line 21
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lepson/print/widgets/AbstractBuilder;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public abstract build()V
.end method

.method protected abstract buildLayout(Landroid/view/ViewGroup;)V
.end method

.method public abstract refresh()V
.end method

.method public abstract setResource(Ljava/lang/Object;)V
.end method

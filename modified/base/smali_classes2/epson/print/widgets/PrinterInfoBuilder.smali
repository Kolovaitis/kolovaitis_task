.class public Lepson/print/widgets/PrinterInfoBuilder;
.super Lepson/print/widgets/AbstractListBuilder;
.source "PrinterInfoBuilder.java"


# static fields
.field private static final ACTIVE:I = 0x2

.field private static final IP:I = 0x1

.field private static final NAME:I = 0x0

.field public static final PRINTER:I = 0x0

.field public static final SCANNER:I = 0x1


# instance fields
.field curPrinterId:Ljava/lang/String;

.field private kind:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lepson/print/widgets/AbstractListBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    const/4 p1, 0x0

    .line 25
    iput p1, p0, Lepson/print/widgets/PrinterInfoBuilder;->kind:I

    const/4 p1, 0x0

    .line 117
    iput-object p1, p0, Lepson/print/widgets/PrinterInfoBuilder;->curPrinterId:Ljava/lang/String;

    .line 29
    iput p3, p0, Lepson/print/widgets/PrinterInfoBuilder;->kind:I

    return-void
.end method


# virtual methods
.method public addPrinter(Lepson/print/MyPrinter;)V
    .locals 1

    .line 140
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object p1, p0, Lepson/print/widgets/PrinterInfoBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public addPrinter([Ljava/lang/String;)V
    .locals 5

    const-string v0, ""

    .line 127
    array-length v1, p1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    const/4 v0, 0x4

    .line 128
    aget-object v0, p1, v0

    .line 131
    :cond_0
    new-instance v1, Lepson/print/MyPrinter;

    const/4 v2, 0x1

    aget-object v2, p1, v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    const/4 v4, 0x3

    aget-object v4, p1, v4

    invoke-direct {v1, v2, v3, v4, v0}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 132
    aget-object p1, p1, v0

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v1, p1}, Lepson/print/MyPrinter;->setPrinterIndex(I)V

    .line 134
    iget-object p1, p0, Lepson/print/widgets/PrinterInfoBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {p1, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object p1, p0, Lepson/print/widgets/PrinterInfoBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected buildData()V
    .locals 1

    .line 34
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void
.end method

.method public buildViewHolder()Landroid/view/View;
    .locals 3

    .line 44
    iget-object v0, p0, Lepson/print/widgets/PrinterInfoBuilder;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a00ad

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public buildViewHolderContent(Landroid/view/View;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 49
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const v1, 0x7f080222

    .line 50
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f0801aa

    .line 51
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f080050

    .line 52
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public buildViewHolderContentData(ILjava/util/Vector;Ljava/util/Vector;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 60
    invoke-virtual {p2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget v0, p0, Lepson/print/widgets/PrinterInfoBuilder;->kind:I

    const/4 v2, 0x1

    const/16 v3, 0x8

    const/4 v4, 0x2

    if-eq v0, v2, :cond_3

    .line 84
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    iget-object v5, p0, Lepson/print/widgets/PrinterInfoBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lepson/print/MyPrinter;

    invoke-virtual {v6}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectType(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 86
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lepson/print/MyPrinter;

    invoke-virtual {v4}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lepson/print/widgets/PrinterInfoBuilder;->curPrinterId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    .line 85
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 88
    :cond_1
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 89
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lepson/print/MyPrinter;

    invoke-virtual {v4}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lepson/print/widgets/PrinterInfoBuilder;->curPrinterId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    const/16 v1, 0x8

    .line 88
    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 66
    :cond_3
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    iget-object v5, p0, Lepson/print/widgets/PrinterInfoBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lepson/print/MyPrinter;

    invoke-virtual {v6}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectType(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 67
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getScannerId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 69
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    iget-object v5, p0, Lepson/print/widgets/PrinterInfoBuilder;->curPrinterId:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 71
    :cond_5
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 74
    :cond_6
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 76
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    iget-object v5, p0, Lepson/print/widgets/PrinterInfoBuilder;->curPrinterId:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    goto :goto_3

    :cond_7
    const/16 v1, 0x8

    :goto_3
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 78
    :cond_8
    invoke-virtual {p2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 94
    :goto_4
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v0

    .line 95
    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    iget-object v3, p0, Lepson/print/widgets/PrinterInfoBuilder;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectType(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 97
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getCommonDeviceName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 98
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p3

    if-nez p3, :cond_9

    .line 99
    invoke-virtual {p2, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " ("

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 101
    :cond_9
    invoke-virtual {p2, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 105
    :cond_a
    invoke-virtual {p2, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/print/widgets/PrinterInfoBuilder;->mContext:Landroid/content/Context;

    .line 106
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    .line 105
    invoke-static {v1, v0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    return-void
.end method

.method public getIndexer()Landroid/widget/AlphabetIndexer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected notifyDataChange()V
    .locals 0

    return-void
.end method

.method public replacePrinter(Lepson/print/MyPrinter;I)V
    .locals 1

    .line 145
    invoke-virtual {p0}, Lepson/print/widgets/PrinterInfoBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 146
    iget-object p1, p0, Lepson/print/widgets/PrinterInfoBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setResource(Ljava/lang/Object;)V
    .locals 0

    .line 121
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lepson/print/widgets/PrinterInfoBuilder;->curPrinterId:Ljava/lang/String;

    return-void
.end method

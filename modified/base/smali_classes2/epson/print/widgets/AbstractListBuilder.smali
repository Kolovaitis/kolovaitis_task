.class public abstract Lepson/print/widgets/AbstractListBuilder;
.super Lepson/print/widgets/AbstractBuilder;
.source "AbstractListBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/widgets/AbstractListBuilder$BuildData;,
        Lepson/print/widgets/AbstractListBuilder$FactoryContentObserver;
    }
.end annotation


# instance fields
.field protected mAdapter:Landroid/widget/BaseAdapter;

.field protected mContentResolver:Landroid/content/ContentResolver;

.field protected mContext:Landroid/content/Context;

.field protected mCursor:Landroid/database/Cursor;

.field protected mData:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mHandlerObserver:Landroid/os/Handler;

.field protected mObserver:Lepson/print/widgets/AbstractListBuilder$FactoryContentObserver;

.field protected mProjection:[Ljava/lang/String;

.field protected mSelection:Ljava/lang/String;

.field protected mSelectionArgs:[Ljava/lang/String;

.field protected mSortOrder:Ljava/lang/String;

.field protected mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 1

    .line 111
    invoke-direct {p0, p1, p2}, Lepson/print/widgets/AbstractBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 65
    new-instance p2, Landroid/os/Handler;

    new-instance v0, Lepson/print/widgets/AbstractListBuilder$1;

    invoke-direct {v0, p0}, Lepson/print/widgets/AbstractListBuilder$1;-><init>(Lepson/print/widgets/AbstractListBuilder;)V

    invoke-direct {p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object p2, p0, Lepson/print/widgets/AbstractListBuilder;->mHandlerObserver:Landroid/os/Handler;

    .line 112
    iput-object p1, p0, Lepson/print/widgets/AbstractListBuilder;->mContext:Landroid/content/Context;

    .line 113
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    iput-object p1, p0, Lepson/print/widgets/AbstractListBuilder;->mData:Ljava/util/Vector;

    .line 114
    iget-object p1, p0, Lepson/print/widgets/AbstractListBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iput-object p1, p0, Lepson/print/widgets/AbstractListBuilder;->mContentResolver:Landroid/content/ContentResolver;

    .line 115
    new-instance p1, Lepson/print/widgets/AbstractListBuilder$FactoryContentObserver;

    iget-object p2, p0, Lepson/print/widgets/AbstractListBuilder;->mHandlerObserver:Landroid/os/Handler;

    invoke-direct {p1, p0, p2}, Lepson/print/widgets/AbstractListBuilder$FactoryContentObserver;-><init>(Lepson/print/widgets/AbstractListBuilder;Landroid/os/Handler;)V

    iput-object p1, p0, Lepson/print/widgets/AbstractListBuilder;->mObserver:Lepson/print/widgets/AbstractListBuilder$FactoryContentObserver;

    return-void
.end method


# virtual methods
.method public abstract addPrinter([Ljava/lang/String;)V
.end method

.method public build()V
    .locals 1

    .line 161
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lepson/print/widgets/AbstractListBuilder;->buildLayout(Landroid/view/ViewGroup;)V

    return-void
.end method

.method protected abstract buildData()V
.end method

.method protected buildLayout(Landroid/view/ViewGroup;)V
    .locals 2

    .line 176
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a0095

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    .line 178
    iget-object p1, p0, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    const v0, 0x102000a

    .line 179
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    .line 181
    new-instance v0, Lepson/print/widgets/MyAdapter;

    invoke-direct {v0, p0}, Lepson/print/widgets/MyAdapter;-><init>(Lepson/print/widgets/AbstractListBuilder;)V

    iput-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    .line 182
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 183
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    .line 184
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 185
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public abstract buildViewHolder()Landroid/view/View;
.end method

.method public abstract buildViewHolderContent(Landroid/view/View;)Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method

.method public abstract buildViewHolderContentData(ILjava/util/Vector;Ljava/util/Vector;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method protected delete()V
    .locals 4

    .line 40
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lepson/print/widgets/AbstractListBuilder;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lepson/print/widgets/AbstractListBuilder;->mSelection:Ljava/lang/String;

    iget-object v3, p0, Lepson/print/widgets/AbstractListBuilder;->mSelectionArgs:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public destructor()V
    .locals 1

    .line 189
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void
.end method

.method public getAdapter()Landroid/widget/BaseAdapter;
    .locals 1

    .line 121
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method public getData()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mData:Ljava/util/Vector;

    return-object v0
.end method

.method public abstract getIndexer()Landroid/widget/AlphabetIndexer;
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 2

    .line 195
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    return-object v0
.end method

.method protected abstract notifyDataChange()V
.end method

.method protected query()Landroid/database/Cursor;
    .locals 6

    .line 35
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lepson/print/widgets/AbstractListBuilder;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lepson/print/widgets/AbstractListBuilder;->mProjection:[Ljava/lang/String;

    iget-object v3, p0, Lepson/print/widgets/AbstractListBuilder;->mSelection:Ljava/lang/String;

    iget-object v4, p0, Lepson/print/widgets/AbstractListBuilder;->mSelectionArgs:[Ljava/lang/String;

    iget-object v5, p0, Lepson/print/widgets/AbstractListBuilder;->mSortOrder:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public refresh()V
    .locals 1

    .line 165
    invoke-virtual {p0}, Lepson/print/widgets/AbstractListBuilder;->buildData()V

    .line 166
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected registerObserver()V
    .locals 4

    .line 74
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lepson/print/widgets/AbstractListBuilder;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lepson/print/widgets/AbstractListBuilder;->mObserver:Lepson/print/widgets/AbstractListBuilder$FactoryContentObserver;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method protected triggerEmptyView()V
    .locals 3

    .line 80
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x1020004

    .line 81
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    iget-object v1, p0, Lepson/print/widgets/AbstractListBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/16 v2, 0x8

    if-lez v1, :cond_0

    .line 83
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 86
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lepson/print/widgets/AbstractListBuilder;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.class public Lepson/print/EPPrinterInfo;
.super Ljava/lang/Object;
.source "EPPrinterInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x416632f81bfdcd4fL


# instance fields
.field public printerAccessKey:Ljava/lang/String;

.field public printerEmailAddress:Ljava/lang/String;

.field public printerID:Ljava/lang/String;

.field public printerIP:Ljava/lang/String;

.field public printerLocation:I

.field public printerName:Ljava/lang/String;

.field public printerSerialNo:Ljava/lang/String;

.field public scannerID:Ljava/lang/String;

.field public userDefName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 35
    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    const-string v0, ""

    .line 36
    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    const-string v0, ""

    .line 37
    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    const-string v0, ""

    .line 38
    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    const/4 v0, 0x0

    .line 39
    iput v0, p0, Lepson/print/EPPrinterInfo;->printerLocation:I

    const-string v0, ""

    .line 40
    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    const-string v0, ""

    .line 41
    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    const-string v0, ""

    .line 44
    iput-object v0, p0, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    const-string v0, ""

    .line 45
    iput-object v0, p0, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public readJSON(Ljava/io/FileInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 87
    invoke-virtual {p1}, Ljava/io/FileInputStream;->available()I

    move-result v0

    .line 88
    new-array v0, v0, [B

    .line 89
    invoke-virtual {p1, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 91
    new-instance p1, Lorg/json/JSONObject;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-direct {p1, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "printerName"

    .line 93
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    const-string v0, "printerID"

    .line 94
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    const-string v0, "printerIP"

    .line 95
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    const-string v0, "printerSerialNo"

    .line 96
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    const-string v0, "printerLocation"

    .line 97
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/print/EPPrinterInfo;->printerLocation:I

    const-string v0, "printerEmailAddress"

    .line 98
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    const-string v0, "printerAccessKey"

    .line 99
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    const-string v0, "userDefName"

    .line 101
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    :try_start_0
    const-string v0, "scannerID"

    .line 103
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 105
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public writeJSON(Ljava/io/FileOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 60
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "printerName"

    .line 62
    iget-object v2, p0, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "printerID"

    .line 63
    iget-object v2, p0, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "printerIP"

    .line 64
    iget-object v2, p0, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "printerSerialNo"

    .line 65
    iget-object v2, p0, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "printerLocation"

    .line 66
    iget v2, p0, Lepson/print/EPPrinterInfo;->printerLocation:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "printerEmailAddress"

    .line 67
    iget-object v2, p0, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "printerAccessKey"

    .line 68
    iget-object v2, p0, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "userDefName"

    .line 70
    iget-object v2, p0, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "scannerID"

    .line 71
    iget-object v2, p0, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 74
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/FileOutputStream;->write([B)V

    return-void
.end method

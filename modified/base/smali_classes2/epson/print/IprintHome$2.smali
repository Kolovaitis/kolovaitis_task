.class Lepson/print/IprintHome$2;
.super Ljava/lang/Object;
.source "IprintHome.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/IprintHome;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/IprintHome;


# direct methods
.method constructor <init>(Lepson/print/IprintHome;)V
    .locals 0

    .line 495
    iput-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 10

    const-string v0, "IprintHome"

    const-string v1, "HandlerCallback"

    .line 498
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    const v2, 0x7f0e043c

    const/4 v3, 0x3

    const v4, 0x7f0e02f6

    const/4 v5, 0x0

    const/4 v6, 0x1

    packed-switch v0, :pswitch_data_0

    const-string p1, "IprintHome"

    const-string v0, "default"

    .line 587
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->printerName:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    const v1, 0x7f0e0439

    invoke-virtual {v0, v1}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 589
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    const v0, 0x7f080282

    invoke-virtual {p1, v0}, Lepson/print/IprintHome;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    :pswitch_0
    const-string v0, "IprintHome"

    const-string v7, "STATUS_ERROR"

    .line 557
    invoke-static {v0, v7}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "STATUS_ERROR"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object p1

    const v0, 0x7f0e0542

    if-eqz p1, :cond_4

    const-string v7, "IprintHome"

    .line 561
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "statusError[0] : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v9, p1, v5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "IprintHome"

    .line 562
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "statusError[1] : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v9, p1, v6

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    aget v7, p1, v5

    if-eq v7, v1, :cond_3

    aget v1, p1, v5

    if-eq v1, v6, :cond_3

    aget v1, p1, v5

    if-ne v1, v3, :cond_0

    goto :goto_0

    .line 569
    :cond_0
    aget v1, p1, v5

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    aget v1, p1, v6

    const/16 v2, 0x66

    if-eq v1, v2, :cond_1

    aget p1, p1, v6

    const/16 v1, -0x514

    if-ne p1, v1, :cond_2

    .line 571
    :cond_1
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->printerStatus:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v0, v4}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string p1, "IprintHome"

    .line 572
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stt title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v1, v4}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 575
    :cond_2
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->printerStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v1, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string p1, "IprintHome"

    .line 576
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v2, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 567
    :cond_3
    :goto_0
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->printerStatus:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v0, v2}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string p1, "IprintHome"

    .line 568
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stt title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v1, v2}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 580
    :cond_4
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->printerStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v1, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string p1, "IprintHome"

    .line 581
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt titlef: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v2, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 552
    :pswitch_1
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->printerStatus:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v0, v4}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string p1, "IprintHome"

    .line 553
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stt title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v1, v4}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :pswitch_2
    const-string p1, "IprintHome"

    const-string v0, "UPDATE_PRINTER_STATUS"

    .line 513
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    sget-object v0, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/IprintHome;->access$102(Lepson/print/IprintHome;[I)[I

    .line 516
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "status "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-static {v0}, Lepson/print/IprintHome;->access$100(Lepson/print/IprintHome;)[I

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 517
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "printerLocation "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-static {v0}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 518
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "finishProbe"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-boolean v0, v0, Lepson/print/IprintHome;->finishProbe:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const-string p1, ""

    .line 521
    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-static {v0}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result v0

    if-ne v0, v6, :cond_5

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    .line 522
    invoke-virtual {v0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "printer"

    invoke-static {v0, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 524
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    const v0, 0x7f0e0547

    invoke-virtual {p1, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_3

    .line 525
    :cond_5
    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-static {v0}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result v0

    if-eq v0, v6, :cond_a

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    .line 526
    invoke-static {v0}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result v0

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {v0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_2

    .line 531
    :cond_6
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-static {p1}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result p1

    if-eq p1, v1, :cond_8

    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    .line 532
    invoke-static {p1}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result p1

    if-ne p1, v3, :cond_7

    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p1}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_7

    goto :goto_1

    .line 544
    :cond_7
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    const v0, 0x7f0e03f6

    invoke-virtual {p1, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 534
    :cond_8
    :goto_1
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p1}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isOnline(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_9

    .line 537
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    const v0, 0x7f0e02f0

    invoke-virtual {p1, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 540
    :cond_9
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    const v0, 0x7f0e0445

    invoke-virtual {p1, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 528
    :cond_a
    :goto_2
    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-boolean v0, v0, Lepson/print/IprintHome;->finishProbe:Z

    if-ne v0, v6, :cond_b

    .line 529
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p1, v2}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 546
    :cond_b
    :goto_3
    iget-object v0, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object v0, v0, Lepson/print/IprintHome;->printerStatus:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "IprintHome"

    .line 547
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_3
    const-string p1, "IprintHome"

    const-string v0, "PROBE_PRINTER"

    .line 502
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iput-boolean v5, p1, Lepson/print/IprintHome;->finishProbe:Z

    .line 504
    iget-object p1, p1, Lepson/print/IprintHome;->task:Lepson/print/IprintHome$ProbePrinter;

    if-nez p1, :cond_c

    .line 505
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    new-instance v0, Lepson/print/IprintHome$ProbePrinter;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lepson/print/IprintHome$ProbePrinter;-><init>(Lepson/print/IprintHome;Lepson/print/IprintHome$1;)V

    iput-object v0, p1, Lepson/print/IprintHome;->task:Lepson/print/IprintHome$ProbePrinter;

    .line 506
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->task:Lepson/print/IprintHome$ProbePrinter;

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {p1, v0, v1}, Lepson/print/IprintHome$ProbePrinter;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 507
    iget-object p1, p0, Lepson/print/IprintHome$2;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_c
    :goto_4
    return v6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class public Lepson/print/ImageItem;
.super Ljava/lang/Object;
.source "ImageItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private databaseId:J

.field private path:Ljava/lang/String;

.field private selected:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lepson/print/ImageItem$1;

    invoke-direct {v0}, Lepson/print/ImageItem$1;-><init>()V

    sput-object v0, Lepson/print/ImageItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 15
    iput-object v0, p0, Lepson/print/ImageItem;->bitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    .line 16
    iput v1, p0, Lepson/print/ImageItem;->selected:I

    .line 17
    iput-object v0, p0, Lepson/print/ImageItem;->path:Ljava/lang/String;

    const-wide/16 v0, -0x1

    .line 18
    iput-wide v0, p0, Lepson/print/ImageItem;->databaseId:J

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;ILjava/lang/String;J)V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lepson/print/ImageItem;->bitmap:Landroid/graphics/Bitmap;

    .line 24
    iput p2, p0, Lepson/print/ImageItem;->selected:I

    .line 25
    iput-object p3, p0, Lepson/print/ImageItem;->path:Ljava/lang/String;

    .line 26
    iput-wide p4, p0, Lepson/print/ImageItem;->databaseId:J

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p0, p1}, Lepson/print/ImageItem;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getCREATOR()Landroid/os/Parcelable$Creator;
    .locals 1

    .line 79
    sget-object v0, Lepson/print/ImageItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .line 118
    instance-of v0, p1, Lepson/print/ImageItem;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 121
    :cond_0
    invoke-virtual {p0}, Lepson/print/ImageItem;->getDatabaseId()J

    move-result-wide v2

    check-cast p1, Lepson/print/ImageItem;

    invoke-virtual {p1}, Lepson/print/ImageItem;->getDatabaseId()J

    move-result-wide v4

    cmp-long p1, v2, v4

    if-nez p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 47
    iget-object v0, p0, Lepson/print/ImageItem;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getDatabaseId()J
    .locals 2

    .line 71
    iget-wide v0, p0, Lepson/print/ImageItem;->databaseId:J

    return-wide v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lepson/print/ImageItem;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getSelected()I
    .locals 1

    .line 55
    iget v0, p0, Lepson/print/ImageItem;->selected:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    .line 96
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lepson/print/ImageItem;->bitmap:Landroid/graphics/Bitmap;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/ImageItem;->selected:I

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/ImageItem;->path:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lepson/print/ImageItem;->databaseId:J

    return-void
.end method

.method public release()V
    .locals 1

    .line 103
    iget-object v0, p0, Lepson/print/ImageItem;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    .line 105
    iput-object v0, p0, Lepson/print/ImageItem;->bitmap:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lepson/print/ImageItem;->bitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setDatabaseId(J)V
    .locals 0

    .line 75
    iput-wide p1, p0, Lepson/print/ImageItem;->databaseId:J

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lepson/print/ImageItem;->path:Ljava/lang/String;

    return-void
.end method

.method public setSelected(I)V
    .locals 0

    .line 59
    iput p1, p0, Lepson/print/ImageItem;->selected:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 89
    iget-object p2, p0, Lepson/print/ImageItem;->bitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 90
    iget p2, p0, Lepson/print/ImageItem;->selected:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget-object p2, p0, Lepson/print/ImageItem;->path:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget-wide v0, p0, Lepson/print/ImageItem;->databaseId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method

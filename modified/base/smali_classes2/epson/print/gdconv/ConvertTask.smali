.class public Lepson/print/gdconv/ConvertTask;
.super Landroid/os/AsyncTask;
.source "ConvertTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/gdconv/ConvertTask$TaskCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Lepson/print/gdconv/ConvertStatus;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final FOLDER_FOR_CONVERT:Ljava/lang/String; = "Epson iPrint"


# instance fields
.field private final mContextReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mOutFile:Ljava/io/File;

.field private final mTaskCallbackWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lepson/print/gdconv/ConvertTask$TaskCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lepson/print/gdconv/ConvertTask$TaskCallback;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 57
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/print/gdconv/ConvertTask;->mContextReference:Ljava/lang/ref/WeakReference;

    .line 59
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lepson/print/gdconv/ConvertTask;->mTaskCallbackWeakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic access$000(Lepson/print/gdconv/ConvertTask;[Ljava/lang/Object;)V
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method private createOutFilename(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 120
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    .line 122
    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    sget-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->GOOGLE_DRIVE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    .line 123
    invoke-virtual {p1, v0}, Lepson/common/ExternalFileUtils;->getTempCacheDir(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;

    move-result-object p1

    .line 124
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ".pdf"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private doNotifyTaskEnd(I)V
    .locals 1

    .line 136
    iget-object v0, p0, Lepson/print/gdconv/ConvertTask;->mTaskCallbackWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/gdconv/ConvertTask$TaskCallback;

    if-eqz v0, :cond_0

    .line 138
    invoke-interface {v0, p1}, Lepson/print/gdconv/ConvertTask$TaskCallback;->notifyTaskEnd(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 9

    const/16 v0, 0x65

    if-eqz p1, :cond_4

    const/4 v1, 0x0

    .line 64
    aget-object v2, p1, v1

    if-nez v2, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    aget-object v4, p1, v1

    .line 70
    iget-object p1, p0, Lepson/print/gdconv/ConvertTask;->mContextReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    if-nez p1, :cond_1

    const/16 p1, 0x67

    .line 73
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 76
    :cond_1
    new-instance v2, Lcom/epson/iprint/storage/Network;

    invoke-direct {v2}, Lcom/epson/iprint/storage/Network;-><init>()V

    .line 77
    invoke-static {p1}, Lepson/common/Utils;->isConnecting(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 p1, 0x66

    .line 79
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 83
    :cond_2
    :try_start_0
    invoke-static {p1}, Lcom/epson/iprint/storage/gdrivev3/DriveWriter;->createDriveWriter(Landroid/content/Context;)Lcom/epson/iprint/storage/gdrivev3/DriveWriter;

    move-result-object v3

    .line 85
    invoke-static {v4}, Lepson/common/Utils;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    .line 88
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_3
    const/4 v2, 0x1

    .line 91
    new-array v2, v2, [Lepson/print/gdconv/ConvertStatus;

    sget-object v6, Lepson/print/gdconv/ConvertStatus;->UPLOAD:Lepson/print/gdconv/ConvertStatus;

    aput-object v6, v2, v1

    invoke-virtual {p0, v2}, Lepson/print/gdconv/ConvertTask;->publishProgress([Ljava/lang/Object;)V

    .line 92
    invoke-direct {p0, p1, v4}, Lepson/print/gdconv/ConvertTask;->createOutFilename(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    .line 93
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :try_start_1
    iput-object v7, p0, Lepson/print/gdconv/ConvertTask;->mOutFile:Ljava/io/File;

    .line 95
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string p1, "Epson iPrint"

    .line 97
    invoke-virtual {v3, p1}, Lcom/epson/iprint/storage/gdrivev3/DriveWriter;->findOrCreateFolder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 98
    new-instance v8, Lepson/print/gdconv/ConvertTask$1;

    invoke-direct {v8, p0}, Lepson/print/gdconv/ConvertTask$1;-><init>(Lepson/print/gdconv/ConvertTask;)V

    invoke-virtual/range {v3 .. v8}, Lcom/epson/iprint/storage/gdrivev3/DriveWriter;->convertFileToPdf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lcom/epson/iprint/storage/gdrivev3/DriveWriter$ConvertStatusNotifier;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/16 p1, 0x64

    .line 109
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    .line 95
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 106
    :catch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 66
    :cond_4
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public declared-synchronized getOutFile()Ljava/io/File;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    monitor-enter p0

    .line 54
    :try_start_0
    iget-object v0, p0, Lepson/print/gdconv/ConvertTask;->mOutFile:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onCancelled()V
    .locals 1

    const/16 v0, 0x67

    .line 149
    invoke-direct {p0, v0}, Lepson/print/gdconv/ConvertTask;->doNotifyTaskEnd(I)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 144
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/16 p1, 0x65

    :goto_0
    invoke-direct {p0, p1}, Lepson/print/gdconv/ConvertTask;->doNotifyTaskEnd(I)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lepson/print/gdconv/ConvertStatus;)V
    .locals 3

    .line 129
    iget-object v0, p0, Lepson/print/gdconv/ConvertTask;->mTaskCallbackWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/gdconv/ConvertTask$TaskCallback;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    .line 130
    aget-object v2, p1, v1

    if-eqz v2, :cond_0

    .line 131
    aget-object p1, p1, v1

    invoke-interface {v0, p1}, Lepson/print/gdconv/ConvertTask$TaskCallback;->messageChange(Lepson/print/gdconv/ConvertStatus;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, [Lepson/print/gdconv/ConvertStatus;

    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertTask;->onProgressUpdate([Lepson/print/gdconv/ConvertStatus;)V

    return-void
.end method

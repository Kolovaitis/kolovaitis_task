.class public Lepson/print/gdconv/ConvertProgressActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "ConvertProgressActivity.java"

# interfaces
.implements Lepson/print/gdconv/ConvertTask$TaskCallback;


# static fields
.field private static final REQUEST_CODE_SIGN_IN:I = 0xa


# instance fields
.field private mConvertTask:Lepson/print/gdconv/ConvertTask;

.field private mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

.field private mLocalOfficePath:Ljava/lang/String;

.field private mMessageText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/print/gdconv/ConvertProgressActivity;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lepson/print/gdconv/ConvertProgressActivity;->cancelTaskAndFinish()V

    return-void
.end method

.method private cancelTaskAndFinish()V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .line 108
    iget-object v0, p0, Lepson/print/gdconv/ConvertProgressActivity;->mConvertTask:Lepson/print/gdconv/ConvertTask;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 109
    invoke-virtual {v0, v1}, Lepson/print/gdconv/ConvertTask;->cancel(Z)Z

    :cond_0
    const/16 v0, 0x67

    .line 111
    invoke-virtual {p0, v0}, Lepson/print/gdconv/ConvertProgressActivity;->setResult(I)V

    .line 112
    invoke-virtual {p0}, Lepson/print/gdconv/ConvertProgressActivity;->finish()V

    return-void
.end method

.method public static getStartIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 63
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/gdconv/ConvertProgressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "fileName"

    .line 64
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private setLocalOfficePathFromIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string v1, "fileName"

    .line 125
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/gdconv/ConvertProgressActivity;->mLocalOfficePath:Ljava/lang/String;

    .line 126
    iget-object p1, p0, Lepson/print/gdconv/ConvertProgressActivity;->mLocalOfficePath:Ljava/lang/String;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private startConvertTask()V
    .locals 5

    .line 147
    new-instance v0, Lepson/print/gdconv/ConvertTask;

    invoke-virtual {p0}, Lepson/print/gdconv/ConvertProgressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lepson/print/gdconv/ConvertTask;-><init>(Landroid/content/Context;Lepson/print/gdconv/ConvertTask$TaskCallback;)V

    iput-object v0, p0, Lepson/print/gdconv/ConvertProgressActivity;->mConvertTask:Lepson/print/gdconv/ConvertTask;

    .line 148
    iget-object v0, p0, Lepson/print/gdconv/ConvertProgressActivity;->mConvertTask:Lepson/print/gdconv/ConvertTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lepson/print/gdconv/ConvertProgressActivity;->mLocalOfficePath:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lepson/print/gdconv/ConvertTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public changeMessageText(Lepson/print/gdconv/ConvertStatus;)V
    .locals 1

    .line 153
    sget-object v0, Lepson/print/gdconv/ConvertProgressActivity$2;->$SwitchMap$epson$print$gdconv$ConvertStatus:[I

    invoke-virtual {p1}, Lepson/print/gdconv/ConvertStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const p1, 0x7f0e030b

    goto :goto_0

    :pswitch_0
    const p1, 0x7f0e0540

    goto :goto_0

    :pswitch_1
    const p1, 0x7f0e0325

    .line 165
    :goto_0
    iget-object v0, p0, Lepson/print/gdconv/ConvertProgressActivity;->mMessageText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public messageChange(Lepson/print/gdconv/ConvertStatus;)V
    .locals 0

    .line 175
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->changeMessageText(Lepson/print/gdconv/ConvertStatus;)V

    return-void
.end method

.method public notifyTaskEnd(I)V
    .locals 3

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lepson/print/gdconv/ConvertProgressActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "downloadPath"

    .line 187
    iget-object v2, p0, Lepson/print/gdconv/ConvertProgressActivity;->mConvertTask:Lepson/print/gdconv/ConvertTask;

    invoke-virtual {v2}, Lepson/print/gdconv/ConvertTask;->getOutFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    invoke-virtual {p0, v0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 190
    :cond_0
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->setResult(I)V

    .line 192
    :goto_0
    invoke-virtual {p0}, Lepson/print/gdconv/ConvertProgressActivity;->finish()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    const/16 p3, 0xa

    if-eq p1, p3, :cond_0

    return-void

    :cond_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_2

    .line 134
    iget-object p1, p0, Lepson/print/gdconv/ConvertProgressActivity;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    sget-object p2, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;->UPLOAD:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;

    .line 135
    invoke-virtual {p1, p0, p2}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->isSignInValid(Landroid/content/Context;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    .line 140
    :cond_1
    invoke-direct {p0}, Lepson/print/gdconv/ConvertProgressActivity;->startConvertTask()V

    return-void

    :cond_2
    :goto_0
    const/4 p1, 0x0

    .line 136
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->setResult(I)V

    .line 137
    invoke-virtual {p0}, Lepson/print/gdconv/ConvertProgressActivity;->finish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 170
    invoke-direct {p0}, Lepson/print/gdconv/ConvertProgressActivity;->cancelTaskAndFinish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 70
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0024

    .line 71
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->setContentView(I)V

    .line 73
    new-instance p1, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-direct {p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;-><init>()V

    iput-object p1, p0, Lepson/print/gdconv/ConvertProgressActivity;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    const p1, 0x7f0800cf

    .line 75
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    .line 76
    new-instance v0, Lepson/print/gdconv/ConvertProgressActivity$1;

    invoke-direct {v0, p0}, Lepson/print/gdconv/ConvertProgressActivity$1;-><init>(Lepson/print/gdconv/ConvertProgressActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801f8

    .line 83
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/gdconv/ConvertProgressActivity;->mMessageText:Landroid/widget/TextView;

    .line 86
    sget-object p1, Lepson/print/gdconv/ConvertStatus;->INIT:Lepson/print/gdconv/ConvertStatus;

    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->changeMessageText(Lepson/print/gdconv/ConvertStatus;)V

    .line 89
    invoke-virtual {p0}, Lepson/print/gdconv/ConvertProgressActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->setLocalOfficePathFromIntent(Landroid/content/Intent;)Z

    move-result p1

    if-nez p1, :cond_0

    const/16 p1, 0x65

    .line 90
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->setResult(I)V

    .line 91
    invoke-virtual {p0}, Lepson/print/gdconv/ConvertProgressActivity;->finish()V

    return-void

    .line 95
    :cond_0
    iget-object p1, p0, Lepson/print/gdconv/ConvertProgressActivity;->mLocalOfficePath:Ljava/lang/String;

    invoke-static {p1}, Lepson/print/GdataConvert;->checkFileSize(Ljava/lang/String;)I

    move-result p1

    const/16 v0, 0x64

    if-eq p1, v0, :cond_1

    .line 97
    invoke-virtual {p0, p1}, Lepson/print/gdconv/ConvertProgressActivity;->setResult(I)V

    .line 98
    invoke-virtual {p0}, Lepson/print/gdconv/ConvertProgressActivity;->finish()V

    return-void

    .line 103
    :cond_1
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, Lepson/print/gdconv/ConvertProgressActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

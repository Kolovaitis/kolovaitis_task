.class public final enum Lepson/print/gdconv/ConvertStatus;
.super Ljava/lang/Enum;
.source "ConvertStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/gdconv/ConvertStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/gdconv/ConvertStatus;

.field public static final enum DOWNLOAD:Lepson/print/gdconv/ConvertStatus;

.field public static final enum INIT:Lepson/print/gdconv/ConvertStatus;

.field public static final enum UPLOAD:Lepson/print/gdconv/ConvertStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 7
    new-instance v0, Lepson/print/gdconv/ConvertStatus;

    const-string v1, "INIT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/gdconv/ConvertStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/gdconv/ConvertStatus;->INIT:Lepson/print/gdconv/ConvertStatus;

    .line 8
    new-instance v0, Lepson/print/gdconv/ConvertStatus;

    const-string v1, "UPLOAD"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/gdconv/ConvertStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/gdconv/ConvertStatus;->UPLOAD:Lepson/print/gdconv/ConvertStatus;

    .line 9
    new-instance v0, Lepson/print/gdconv/ConvertStatus;

    const-string v1, "DOWNLOAD"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/gdconv/ConvertStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/gdconv/ConvertStatus;->DOWNLOAD:Lepson/print/gdconv/ConvertStatus;

    const/4 v0, 0x3

    .line 6
    new-array v0, v0, [Lepson/print/gdconv/ConvertStatus;

    sget-object v1, Lepson/print/gdconv/ConvertStatus;->INIT:Lepson/print/gdconv/ConvertStatus;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/gdconv/ConvertStatus;->UPLOAD:Lepson/print/gdconv/ConvertStatus;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/gdconv/ConvertStatus;->DOWNLOAD:Lepson/print/gdconv/ConvertStatus;

    aput-object v1, v0, v4

    sput-object v0, Lepson/print/gdconv/ConvertStatus;->$VALUES:[Lepson/print/gdconv/ConvertStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/gdconv/ConvertStatus;
    .locals 1

    .line 6
    const-class v0, Lepson/print/gdconv/ConvertStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/gdconv/ConvertStatus;

    return-object p0
.end method

.method public static values()[Lepson/print/gdconv/ConvertStatus;
    .locals 1

    .line 6
    sget-object v0, Lepson/print/gdconv/ConvertStatus;->$VALUES:[Lepson/print/gdconv/ConvertStatus;

    invoke-virtual {v0}, [Lepson/print/gdconv/ConvertStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/gdconv/ConvertStatus;

    return-object v0
.end method

.class Lepson/print/ActivityPrintWeb$10;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "ActivityPrintWeb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityPrintWeb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityPrintWeb;


# direct methods
.method constructor <init>(Lepson/print/ActivityPrintWeb;)V
    .locals 0

    .line 1112
    iput-object p1, p0, Lepson/print/ActivityPrintWeb$10;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyEndJob(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyProgress(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

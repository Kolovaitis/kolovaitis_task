.class public Lepson/print/CustomLayoutDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "CustomLayoutDialogFragment.java"


# static fields
.field private static final TAG_CLOSE_BUTTON_STRING_ID:Ljava/lang/String; = "close_button_string_id"

.field private static final TAG_DIALOG_ID:Ljava/lang/String; = "dialog_id"

.field private static final TAG_MESSAGE:Ljava/lang/String; = "message"

.field private static final TAG_POSITIVE_BUTTON_STRING_ID:Ljava/lang/String; = "positive_button_string_id"

.field private static final TAG_TITLE_ID:Ljava/lang/String; = "title_id"

.field private static final TAG_TITLE_STRING:Ljava/lang/String; = "title_string"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 79
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(ILjava/lang/String;III)Lepson/print/CustomLayoutDialogFragment;
    .locals 3

    .line 43
    new-instance v0, Lepson/print/CustomLayoutDialogFragment;

    invoke-direct {v0}, Lepson/print/CustomLayoutDialogFragment;-><init>()V

    .line 45
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "dialog_id"

    .line 46
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "message"

    .line 47
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "title_id"

    .line 48
    invoke-virtual {v1, p0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "positive_button_string_id"

    .line 49
    invoke-virtual {v1, p0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "close_button_string_id"

    .line 50
    invoke-virtual {v1, p0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 51
    invoke-virtual {v0, v1}, Lepson/print/CustomLayoutDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static newInstance(ILjava/lang/String;Ljava/lang/String;II)Lepson/print/CustomLayoutDialogFragment;
    .locals 3

    .line 63
    new-instance v0, Lepson/print/CustomLayoutDialogFragment;

    invoke-direct {v0}, Lepson/print/CustomLayoutDialogFragment;-><init>()V

    .line 65
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "dialog_id"

    .line 66
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "message"

    .line 67
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "title_string"

    .line 68
    invoke-virtual {v1, p0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "positive_button_string_id"

    .line 69
    invoke-virtual {v1, p0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "close_button_string_id"

    .line 70
    invoke-virtual {v1, p0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 71
    invoke-virtual {v0, v1}, Lepson/print/CustomLayoutDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10

    .line 85
    invoke-virtual {p0}, Lepson/print/CustomLayoutDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "dialog_id"

    .line 86
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "message"

    .line 87
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "title_id"

    .line 88
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "title_string"

    .line 89
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "positive_button_string_id"

    .line 90
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "close_button_string_id"

    .line 91
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    .line 94
    invoke-virtual {p0}, Lepson/print/CustomLayoutDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Lepson/print/CustomTitleDialogFragment$Callback;

    .line 96
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/print/CustomLayoutDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-virtual {p0}, Lepson/print/CustomLayoutDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0a0051

    const/4 v9, 0x0

    .line 99
    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f080341

    .line 101
    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 103
    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 105
    :cond_0
    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const v2, 0x7f08021b

    .line 108
    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 109
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0800d0

    .line 111
    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 112
    new-instance v2, Lepson/print/CustomLayoutDialogFragment$1;

    invoke-direct {v2, p0, v5, v0}, Lepson/print/CustomLayoutDialogFragment$1;-><init>(Lepson/print/CustomLayoutDialogFragment;Lepson/print/CustomTitleDialogFragment$Callback;I)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(I)V

    const p1, 0x7f080083

    .line 122
    invoke-virtual {v7, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    .line 123
    new-instance v1, Lepson/print/CustomLayoutDialogFragment$2;

    invoke-direct {v1, p0, v5, v0}, Lepson/print/CustomLayoutDialogFragment$2;-><init>(Lepson/print/CustomLayoutDialogFragment;Lepson/print/CustomTitleDialogFragment$Callback;I)V

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v4, :cond_1

    .line 131
    invoke-virtual {p1, v4}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    .line 133
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 136
    :goto_1
    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 139
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 140
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 141
    new-instance v0, Lepson/print/CustomLayoutDialogFragment$3;

    invoke-direct {v0, p0}, Lepson/print/CustomLayoutDialogFragment$3;-><init>(Lepson/print/CustomLayoutDialogFragment;)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object p1
.end method

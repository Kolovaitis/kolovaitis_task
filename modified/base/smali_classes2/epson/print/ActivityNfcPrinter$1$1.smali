.class Lepson/print/ActivityNfcPrinter$1$1;
.super Landroid/os/AsyncTask;
.source "ActivityNfcPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityNfcPrinter$1;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/ActivityNfcPrinter$1;


# direct methods
.method constructor <init>(Lepson/print/ActivityNfcPrinter$1;)V
    .locals 0

    .line 371
    iput-object p1, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4

    .line 382
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setHanlder(Landroid/os/Handler;)V

    .line 383
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setSearchStt(Z)V

    .line 385
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/ActivityNfcPrinter;->access$300(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {p1, v1, v2, v0, v3}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p1

    .line 387
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setSearchStt(Z)V

    .line 389
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 371
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter$1$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1

    .line 394
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 396
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$500(Lepson/print/ActivityNfcPrinter;)Lepson/print/MyPrinter;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$600(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/ActivityNfcPrinter;->access$500(Lepson/print/ActivityNfcPrinter;)Lepson/print/MyPrinter;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 398
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$500(Lepson/print/ActivityNfcPrinter;)Lepson/print/MyPrinter;

    move-result-object p1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lepson/print/MyPrinter;->setLocation(I)V

    .line 400
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    .line 401
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/ActivityNfcPrinter;->access$500(Lepson/print/ActivityNfcPrinter;)Lepson/print/MyPrinter;

    move-result-object v0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 403
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/ActivityNfcPrinter;->access$700(Lepson/print/ActivityNfcPrinter;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0xd

    .line 412
    iput v0, p1, Landroid/os/Message;->what:I

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xc

    .line 407
    iput v0, p1, Landroid/os/Message;->what:I

    .line 416
    :goto_0
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 419
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$1;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 371
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter$1$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

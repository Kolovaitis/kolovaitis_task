.class Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;
.super Landroid/os/AsyncTask;
.source "ActivityViewImageSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityViewImageSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PaperDirectionRotateTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityViewImageSelect;


# direct methods
.method private constructor <init>(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 2499
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/ActivityViewImageSelect;Lepson/print/ActivityViewImageSelect$1;)V
    .locals 0

    .line 2499
    invoke-direct {p0, p1}, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;-><init>(Lepson/print/ActivityViewImageSelect;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2499
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 2515
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object p1, p1, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$3400(Lepson/print/ActivityViewImageSelect;)Z

    move-result v0

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {p1, v0, v1}, Lcom/epson/iprint/apf/ApfPreviewView;->setIsPaperLandscape(ZLandroid/app/Activity;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 2499
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    .line 2507
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {p1}, Lepson/print/ActivityViewImageSelect;->isFinishing()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2510
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;

    move-result-object p1

    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 2502
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$PaperDirectionRotateTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

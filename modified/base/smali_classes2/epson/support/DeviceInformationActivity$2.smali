.class synthetic Lepson/support/DeviceInformationActivity$2;
.super Ljava/lang/Object;
.source "DeviceInformationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/support/DeviceInformationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 171
    invoke-static {}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->values()[Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lepson/support/DeviceInformationActivity$2;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    :try_start_0
    sget-object v0, Lepson/support/DeviceInformationActivity$2;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->WiFiP2P:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lepson/support/DeviceInformationActivity$2;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lepson/support/DeviceInformationActivity$2;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method

.class public Lepson/support/SupportActivity;
.super Lepson/print/ActivityIACommon;
.source "SupportActivity.java"

# interfaces
.implements Lepson/print/CommonDefine;
.implements Landroid/view/View$OnClickListener;
.implements Lepson/print/ThreeButtonDialog$DialogCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/support/SupportActivity$SwitchAdapter;
    }
.end annotation


# static fields
.field private static final REQUEST_CODE_USER_SURVEY:I = 0x1

.field private static final TUTORIAL_URL:Ljava/lang/String; = "https://www.epsonconnect.com/iguide/"


# instance fields
.field private mUserSurveySwitch:Lepson/support/SupportActivity$SwitchAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/support/SupportActivity;Z)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lepson/support/SupportActivity;->onCheckChanged(Z)V

    return-void
.end method

.method static synthetic access$100(Lepson/support/SupportActivity;I)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lepson/support/SupportActivity;->startDocumentDisplayActivity(I)V

    return-void
.end method

.method private onCheckChanged(Z)V
    .locals 1

    .line 132
    new-instance v0, Lepson/common/IprintUserSurveyInfo;

    invoke-direct {v0}, Lepson/common/IprintUserSurveyInfo;-><init>()V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 135
    invoke-virtual {v0, p0, p1}, Lepson/common/IprintUserSurveyInfo;->setUserSurveyAgreement(Landroid/content/Context;Z)V

    goto :goto_0

    .line 138
    :cond_0
    invoke-direct {p0}, Lepson/support/SupportActivity;->startUserSurveyInvitationActivity()V

    :goto_0
    return-void
.end method

.method private startDocumentDisplayActivity(I)V
    .locals 1

    .line 159
    new-instance v0, Lepson/common/IprintLicenseInfo;

    invoke-direct {v0}, Lepson/common/IprintLicenseInfo;-><init>()V

    .line 160
    invoke-static {p0, v0, p1}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;I)Landroid/content/Intent;

    move-result-object p1

    .line 161
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startUserSurveyInvitationActivity()V
    .locals 2

    .line 143
    new-instance v0, Lepson/common/IprintUserSurveyInfo;

    invoke-direct {v0}, Lepson/common/IprintUserSurveyInfo;-><init>()V

    new-instance v1, Lepson/common/IprintLicenseInfo;

    invoke-direct {v1}, Lepson/common/IprintLicenseInfo;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/UserSurveyInfo;Lcom/epson/mobilephone/common/license/LicenseInfo;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    .line 145
    invoke-virtual {p0, v0, v1}, Lepson/support/SupportActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updateUserSurveySwitch()V
    .locals 2

    .line 174
    new-instance v0, Lepson/common/IprintUserSurveyInfo;

    invoke-direct {v0}, Lepson/common/IprintUserSurveyInfo;-><init>()V

    .line 175
    iget-object v1, p0, Lepson/support/SupportActivity;->mUserSurveySwitch:Lepson/support/SupportActivity$SwitchAdapter;

    invoke-virtual {v0, p0}, Lepson/common/IprintUserSurveyInfo;->isUserSurveyEnabled(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lepson/support/SupportActivity$SwitchAdapter;->setCheckWithoutCallback(Z)V

    return-void
.end method


# virtual methods
.method public callback(I)V
    .locals 0

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    const/4 p2, 0x1

    if-eq p1, p2, :cond_0

    return-void

    .line 168
    :cond_0
    invoke-direct {p0}, Lepson/support/SupportActivity;->updateUserSurveySwitch()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 150
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0801cd

    if-ne v0, v1, :cond_0

    const/4 p1, 0x1

    .line 152
    invoke-direct {p0, p1}, Lepson/support/SupportActivity;->startDocumentDisplayActivity(I)V

    goto :goto_0

    .line 153
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f08023d

    if-ne p1, v0, :cond_1

    const/4 p1, 0x3

    .line 154
    invoke-direct {p0, p1}, Lepson/support/SupportActivity;->startDocumentDisplayActivity(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 42
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0087

    .line 43
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->setContentView(I)V

    const p1, 0x7f0e0537

    const/4 v0, 0x1

    .line 46
    invoke-virtual {p0, p1, v0}, Lepson/support/SupportActivity;->setActionBar(IZ)V

    const p1, 0x7f0801cc

    .line 48
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 49
    new-instance v0, Lepson/support/SupportActivity$1;

    invoke-direct {v0, p0}, Lepson/support/SupportActivity$1;-><init>(Lepson/support/SupportActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    invoke-static {p0}, Lepson/print/Util/BuyInkUrl;->getFaqUrlString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0801ca

    .line 64
    invoke-virtual {p0, v0}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lepson/support/SupportActivity$2;

    invoke-direct {v1, p0, p1}, Lepson/support/SupportActivity$2;-><init>(Lepson/support/SupportActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801d6

    .line 75
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lepson/support/SupportActivity$3;

    invoke-direct {v0, p0}, Lepson/support/SupportActivity$3;-><init>(Lepson/support/SupportActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08031b

    .line 87
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Switch;

    .line 88
    new-instance v0, Lepson/support/SupportActivity$SwitchAdapter;

    new-instance v1, Lepson/support/SupportActivity$4;

    invoke-direct {v1, p0}, Lepson/support/SupportActivity$4;-><init>(Lepson/support/SupportActivity;)V

    invoke-direct {v0, p1, v1}, Lepson/support/SupportActivity$SwitchAdapter;-><init>(Landroid/widget/Switch;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iput-object v0, p0, Lepson/support/SupportActivity;->mUserSurveySwitch:Lepson/support/SupportActivity$SwitchAdapter;

    .line 95
    invoke-direct {p0}, Lepson/support/SupportActivity;->updateUserSurveySwitch()V

    const p1, 0x7f0801d2

    .line 98
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lepson/support/SupportActivity$5;

    invoke-direct {v0, p0}, Lepson/support/SupportActivity$5;-><init>(Lepson/support/SupportActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801c7

    .line 105
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lepson/support/SupportActivity$6;

    invoke-direct {v0, p0}, Lepson/support/SupportActivity$6;-><init>(Lepson/support/SupportActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801cd

    .line 116
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08023d

    .line 119
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801c9

    .line 122
    invoke-virtual {p0, p1}, Lepson/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lepson/support/SupportActivity$7;

    invoke-direct {v0, p0}, Lepson/support/SupportActivity$7;-><init>(Lepson/support/SupportActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    .line 181
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 182
    iget-object v0, p0, Lepson/support/SupportActivity;->mUserSurveySwitch:Lepson/support/SupportActivity$SwitchAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/support/SupportActivity$SwitchAdapter;->setListenerEnabled(Z)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 187
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    .line 188
    iget-object v0, p0, Lepson/support/SupportActivity;->mUserSurveySwitch:Lepson/support/SupportActivity$SwitchAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/support/SupportActivity$SwitchAdapter;->setListenerEnabled(Z)V

    return-void
.end method

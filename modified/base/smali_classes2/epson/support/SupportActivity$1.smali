.class Lepson/support/SupportActivity$1;
.super Ljava/lang/Object;
.source "SupportActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/support/SupportActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/support/SupportActivity;


# direct methods
.method constructor <init>(Lepson/support/SupportActivity;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lepson/support/SupportActivity$1;->this$0:Lepson/support/SupportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 52
    new-instance p1, Lepson/common/InformationGuide;

    iget-object v0, p0, Lepson/support/SupportActivity$1;->this$0:Lepson/support/SupportActivity;

    invoke-virtual {v0}, Lepson/support/SupportActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lepson/common/InformationGuide;-><init>(Landroid/content/Context;)V

    .line 53
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lepson/support/SupportActivity$1;->this$0:Lepson/support/SupportActivity;

    const-class v2, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 54
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_HTML_PATH:Ljava/lang/String;

    invoke-virtual {p1}, Lepson/common/InformationGuide;->getHtmlPath()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 55
    sget-object p1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_GUIDE_VER:Ljava/lang/String;

    sget v1, Lepson/common/InformationGuide;->GUIDE_VER:I

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 56
    sget-object p1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_BOOT_MODE:Ljava/lang/String;

    sget v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->MODE_MANUAL:I

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 57
    iget-object p1, p0, Lepson/support/SupportActivity$1;->this$0:Lepson/support/SupportActivity;

    invoke-virtual {p1, v0}, Lepson/support/SupportActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

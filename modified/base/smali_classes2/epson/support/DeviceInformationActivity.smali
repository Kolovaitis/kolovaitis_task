.class public Lepson/support/DeviceInformationActivity;
.super Lepson/print/ActivityIACommon;
.source "DeviceInformationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/support/DeviceInformationActivity$ProbePrinter;
    }
.end annotation


# static fields
.field private static final EPS_COMM_BID:I = 0x2

.field private static final EPS_ERR_PRINTER_NOT_FOUND:I = -0x514

.field private static final EPS_PRNERR_COMM:I = 0x66

.field private static final PROBE_ERROR:I = 0x1

.field private static final STATUS_ERROR:I = 0x2

.field private static final UPDATE_PRINTER_STATUS:I


# instance fields
.field PrinterIPInfo:Landroid/widget/TextView;

.field PrinterProbeProgress:Landroid/widget/ProgressBar;

.field PrinterStatusInfo:Landroid/widget/TextView;

.field private TAG:Ljava/lang/String;

.field TextBluetoothStatus:Landroid/widget/TextView;

.field TextIPAddress:Landroid/widget/TextView;

.field TextOSVersion:Landroid/widget/TextView;

.field TextPrinterIP:Landroid/widget/TextView;

.field TextPrinterName:Landroid/widget/TextView;

.field TextPrinterStatus:Landroid/widget/TextView;

.field TextWiFiName:Landroid/widget/TextView;

.field TextWiFiState:Landroid/widget/TextView;

.field mHandler:Landroid/os/Handler;

.field private mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field private task:Lepson/support/DeviceInformationActivity$ProbePrinter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 45
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const-string v0, "Device Information"

    .line 46
    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->TAG:Ljava/lang/String;

    .line 48
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 254
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/support/DeviceInformationActivity$1;

    invoke-direct {v1, p0}, Lepson/support/DeviceInformationActivity$1;-><init>(Lepson/support/DeviceInformationActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;
    .locals 0

    .line 45
    iget-object p0, p0, Lepson/support/DeviceInformationActivity;->TAG:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lepson/support/DeviceInformationActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 0

    .line 45
    iget-object p0, p0, Lepson/support/DeviceInformationActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-object p0
.end method

.method static synthetic access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Lepson/support/DeviceInformationActivity;->setPrinterInfo(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$300(Lepson/support/DeviceInformationActivity;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Lepson/support/DeviceInformationActivity;->hideLoadProgressBar()V

    return-void
.end method

.method static synthetic access$400(Landroid/os/Handler;I)V
    .locals 0

    .line 45
    invoke-static {p0, p1}, Lepson/support/DeviceInformationActivity;->sendUpdatePrinterStatus(Landroid/os/Handler;I)V

    return-void
.end method

.method private checkPrinterInfo()V
    .locals 5

    const v0, 0x7f080367

    .line 100
    invoke-virtual {p0, v0}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->TextPrinterName:Landroid/widget/TextView;

    .line 102
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    .line 105
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 106
    iget-object v2, p0, Lepson/support/DeviceInformationActivity;->TextPrinterName:Landroid/widget/TextView;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    invoke-direct {p0}, Lepson/support/DeviceInformationActivity;->showLoadProgressBar()V

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 109
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "IprintHome"

    const-string v2, "Update printer status without execute Probing "

    .line 117
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->mHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Lepson/support/DeviceInformationActivity;->sendUpdatePrinterStatus(Landroid/os/Handler;I)V

    goto :goto_1

    .line 111
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->setDefaultNetworkSimpleAp()V

    const-string v1, "IprintHome"

    const-string v3, "Update printer status after execute Probing"

    .line 112
    invoke-static {v1, v3}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v1, Lepson/support/DeviceInformationActivity$ProbePrinter;

    iget-object v3, p0, Lepson/support/DeviceInformationActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    iget-object v4, p0, Lepson/support/DeviceInformationActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v0, v3, v4}, Lepson/support/DeviceInformationActivity$ProbePrinter;-><init>(Lepson/print/MyPrinter;Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;Landroid/os/Handler;)V

    iput-object v1, p0, Lepson/support/DeviceInformationActivity;->task:Lepson/support/DeviceInformationActivity$ProbePrinter;

    .line 114
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->task:Lepson/support/DeviceInformationActivity$ProbePrinter;

    new-array v1, v2, [Landroid/content/Context;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lepson/support/DeviceInformationActivity$ProbePrinter;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    :goto_1
    return-void
.end method

.method private checkSmartPhoneInfo()V
    .locals 2

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f080361

    .line 140
    invoke-virtual {p0, v1}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lepson/support/DeviceInformationActivity;->TextBluetoothStatus:Landroid/widget/TextView;

    const v1, 0x7f080363

    .line 142
    invoke-virtual {p0, v1}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lepson/support/DeviceInformationActivity;->TextOSVersion:Landroid/widget/TextView;

    const-string v1, "Android "

    .line 143
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    iget-object v1, p0, Lepson/support/DeviceInformationActivity;->TextOSVersion:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const v1, 0x7f0e048c

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->TextBluetoothStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 150
    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->TextBluetoothStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->TextBluetoothStatus:Landroid/widget/TextView;

    const v1, 0x7f0e048d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void
.end method

.method private checkWiFiInfo()V
    .locals 4

    .line 159
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f08036b

    .line 165
    invoke-virtual {p0, v1}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lepson/support/DeviceInformationActivity;->TextWiFiState:Landroid/widget/TextView;

    const v1, 0x7f08036a

    .line 166
    invoke-virtual {p0, v1}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lepson/support/DeviceInformationActivity;->TextWiFiName:Landroid/widget/TextView;

    const v1, 0x7f080362

    .line 167
    invoke-virtual {p0, v1}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lepson/support/DeviceInformationActivity;->TextIPAddress:Landroid/widget/TextView;

    .line 169
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "printer"

    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectType(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v2

    .line 171
    sget-object v3, Lepson/support/DeviceInformationActivity$2;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    goto :goto_0

    .line 174
    :cond_0
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    move-result-object v2

    .line 175
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 177
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    move-result-object v0

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->p2pAddr2MacAddrStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getEstimateSSID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lepson/support/DeviceInformationActivity;->getIpAddressFromP2P(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 186
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 187
    invoke-direct {p0}, Lepson/support/DeviceInformationActivity;->getIpAddressFromWiFi()Ljava/lang/String;

    move-result-object v1

    .line 191
    :goto_1
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isWifiEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 192
    iget-object v2, p0, Lepson/support/DeviceInformationActivity;->TextWiFiState:Landroid/widget/TextView;

    const v3, 0x7f0e0499

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 194
    iget-object v2, p0, Lepson/support/DeviceInformationActivity;->TextWiFiName:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->TextIPAddress:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 198
    :cond_2
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->TextWiFiState:Landroid/widget/TextView;

    const v1, 0x7f0e0498

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_3
    :goto_2
    return-void
.end method

.method public static getIpAddressFromP2P(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 228
    :try_start_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object p0

    .line 229
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getP2PInterfaceInfo()Ljava/net/NetworkInterface;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v0

    .line 234
    :cond_0
    invoke-virtual {p0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object p0

    .line 236
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/InetAddress;

    .line 237
    invoke-virtual {v1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v2

    if-nez v2, :cond_1

    .line 238
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 239
    invoke-static {v1}, Lepson/common/IPAddressUtils;->isIPv4Address(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_1

    return-object v1

    :catch_0
    :cond_2
    return-object v0
.end method

.method private getIpAddressFromWiFi()Ljava/lang/String;
    .locals 3

    .line 207
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 208
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v0

    .line 212
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v0

    :cond_0
    int-to-long v0, v0

    .line 215
    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 218
    :try_start_0
    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    :catch_0
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->TAG:Ljava/lang/String;

    const-string v1, "Unable to get host address."

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private hideLoadProgressBar()V
    .locals 3

    .line 515
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterProbeProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterStatusInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050029

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 517
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterIPInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private static sendUpdatePrinterStatus(Landroid/os/Handler;I)V
    .locals 3
    .param p0    # Landroid/os/Handler;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p0, :cond_0

    return-void

    .line 371
    :cond_0
    invoke-virtual {p0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    .line 372
    iput v1, v0, Landroid/os/Message;->what:I

    .line 373
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "PRINTER_LOCATION"

    .line 374
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 375
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 376
    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setPrinterInfo(Ljava/lang/String;Z)V
    .locals 1

    const v0, 0x7f080368

    .line 126
    invoke-virtual {p0, v0}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->TextPrinterStatus:Landroid/widget/TextView;

    const v0, 0x7f080364

    .line 127
    invoke-virtual {p0, v0}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->TextPrinterIP:Landroid/widget/TextView;

    .line 129
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->TextPrinterStatus:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 131
    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p1

    .line 132
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p1

    .line 134
    iget-object p2, p0, Lepson/support/DeviceInformationActivity;->TextPrinterIP:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private showLoadProgressBar()V
    .locals 3

    const v0, 0x7f08029e

    .line 505
    invoke-virtual {p0, v0}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterProbeProgress:Landroid/widget/ProgressBar;

    const v0, 0x7f080369

    .line 506
    invoke-virtual {p0, v0}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterStatusInfo:Landroid/widget/TextView;

    const v0, 0x7f080365

    .line 507
    invoke-virtual {p0, v0}, Lepson/support/DeviceInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterIPInfo:Landroid/widget/TextView;

    .line 509
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterProbeProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 510
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterStatusInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 511
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->PrinterIPInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private stopProbePrinter()V
    .locals 2

    .line 91
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->task:Lepson/support/DeviceInformationActivity$ProbePrinter;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->TAG:Ljava/lang/String;

    const-string v1, "stopProbePrinter"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 94
    iget-object v0, p0, Lepson/support/DeviceInformationActivity;->task:Lepson/support/DeviceInformationActivity$ProbePrinter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/support/DeviceInformationActivity$ProbePrinter;->cancel(Z)Z

    const/4 v0, 0x0

    .line 95
    iput-object v0, p0, Lepson/support/DeviceInformationActivity;->task:Lepson/support/DeviceInformationActivity$ProbePrinter;

    :cond_0
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .line 80
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    .line 81
    invoke-direct {p0}, Lepson/support/DeviceInformationActivity;->stopProbePrinter()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 68
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0050

    .line 69
    invoke-virtual {p0, p1}, Lepson/support/DeviceInformationActivity;->setContentView(I)V

    const p1, 0x7f0e0491

    const/4 v0, 0x1

    .line 71
    invoke-virtual {p0, p1, v0}, Lepson/support/DeviceInformationActivity;->setActionBar(IZ)V

    .line 73
    invoke-direct {p0}, Lepson/support/DeviceInformationActivity;->checkSmartPhoneInfo()V

    .line 74
    invoke-direct {p0}, Lepson/support/DeviceInformationActivity;->checkPrinterInfo()V

    .line 75
    invoke-direct {p0}, Lepson/support/DeviceInformationActivity;->checkWiFiInfo()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    .line 86
    invoke-direct {p0}, Lepson/support/DeviceInformationActivity;->stopProbePrinter()V

    .line 87
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    return-void
.end method

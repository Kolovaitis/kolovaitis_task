.class Lepson/support/DeviceInformationActivity$ProbePrinter;
.super Landroid/os/AsyncTask;
.source "DeviceInformationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/support/DeviceInformationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProbePrinter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Landroid/content/Context;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final mHandlerReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private final mMyPrinter:Lepson/print/MyPrinter;

.field private mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field private mPrinterLocation:I


# direct methods
.method public constructor <init>(Lepson/print/MyPrinter;Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Lepson/print/MyPrinter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 394
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 395
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mHandlerReference:Ljava/lang/ref/WeakReference;

    .line 397
    iput-object p2, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 398
    iput-object p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mMyPrinter:Lepson/print/MyPrinter;

    return-void
.end method

.method private localSendMessage(I)V
    .locals 4

    .line 474
    iget-object v0, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mHandlerReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 479
    :cond_0
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 480
    iput p1, v1, Landroid/os/Message;->what:I

    .line 481
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "PRINTER_LOCATION"

    .line 482
    iget v3, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinterLocation:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 483
    invoke-virtual {v1, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 484
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private sendStatusError([I)V
    .locals 4

    .line 458
    iget-object v0, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mHandlerReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 463
    :cond_0
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x2

    .line 464
    iput v2, v1, Landroid/os/Message;->what:I

    .line 465
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "STATUS_ERROR"

    .line 466
    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string p1, "PRINTER_LOCATION"

    .line 467
    iget v3, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinterLocation:I

    invoke-virtual {v2, p1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 468
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 469
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 6

    const-string v0, "ProbePrinter doInBackground"

    .line 408
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 410
    iget-object v0, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/4 v1, 0x0

    aget-object p1, p1, v1

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    .line 411
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mMyPrinter:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    iput p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinterLocation:I

    .line 412
    iget p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinterLocation:I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v2, 0x3

    if-ne p1, v2, :cond_5

    .line 414
    :cond_0
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mMyPrinter:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 415
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/16 v2, 0x3c

    iget-object v3, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mMyPrinter:Lepson/print/MyPrinter;

    invoke-virtual {v3}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mMyPrinter:Lepson/print/MyPrinter;

    invoke-virtual {v4}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinterLocation:I

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-eqz p1, :cond_1

    .line 419
    invoke-direct {p0, v0}, Lepson/support/DeviceInformationActivity$ProbePrinter;->localSendMessage(I)V

    .line 420
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 423
    :cond_1
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result p1

    .line 424
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set Printer result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-eqz p1, :cond_2

    .line 427
    invoke-direct {p0, v0}, Lepson/support/DeviceInformationActivity$ProbePrinter;->localSendMessage(I)V

    .line 428
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 431
    :cond_2
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetStatus()I

    move-result p1

    .line 432
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Printer Status result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-eqz p1, :cond_3

    .line 435
    invoke-direct {p0, v0}, Lepson/support/DeviceInformationActivity$ProbePrinter;->localSendMessage(I)V

    .line 436
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 438
    :cond_3
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object p1

    .line 439
    aget v2, p1, v1

    if-eqz v2, :cond_5

    .line 441
    invoke-direct {p0, p1}, Lepson/support/DeviceInformationActivity$ProbePrinter;->sendStatusError([I)V

    .line 442
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 451
    :cond_4
    iput v1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinterLocation:I

    .line 454
    :cond_5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 382
    check-cast p1, [Landroid/content/Context;

    invoke-virtual {p0, p1}, Lepson/support/DeviceInformationActivity$ProbePrinter;->doInBackground([Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled()V
    .locals 1

    const-string v0, "ProbePrinter onCancelled"

    .line 490
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 491
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    const-string v0, "ProbePrinter onPostExecute"

    .line 497
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 498
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 499
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mHandlerReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Handler;

    iget v0, p0, Lepson/support/DeviceInformationActivity$ProbePrinter;->mPrinterLocation:I

    invoke-static {p1, v0}, Lepson/support/DeviceInformationActivity;->access$400(Landroid/os/Handler;I)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 382
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/support/DeviceInformationActivity$ProbePrinter;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method

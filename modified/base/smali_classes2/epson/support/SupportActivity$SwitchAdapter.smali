.class Lepson/support/SupportActivity$SwitchAdapter;
.super Ljava/lang/Object;
.source "SupportActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/support/SupportActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SwitchAdapter"
.end annotation


# instance fields
.field private mOriginalChangeListenerEnabled:Z

.field private mOriginalListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/widget/Switch;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0
    .param p1    # Landroid/widget/Switch;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    iput-object p1, p0, Lepson/support/SupportActivity$SwitchAdapter;->mSwitch:Landroid/widget/Switch;

    .line 206
    iput-object p2, p0, Lepson/support/SupportActivity$SwitchAdapter;->mOriginalListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    const/4 p2, 0x0

    .line 207
    iput-boolean p2, p0, Lepson/support/SupportActivity$SwitchAdapter;->mOriginalChangeListenerEnabled:Z

    .line 209
    invoke-virtual {p1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 225
    iget-boolean v0, p0, Lepson/support/SupportActivity$SwitchAdapter;->mOriginalChangeListenerEnabled:Z

    if-nez v0, :cond_0

    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lepson/support/SupportActivity$SwitchAdapter;->mOriginalListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v0, :cond_1

    .line 229
    invoke-interface {v0, p1, p2}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    :cond_1
    return-void
.end method

.method public setCheckWithoutCallback(Z)V
    .locals 2

    .line 217
    iget-boolean v0, p0, Lepson/support/SupportActivity$SwitchAdapter;->mOriginalChangeListenerEnabled:Z

    const/4 v1, 0x0

    .line 218
    iput-boolean v1, p0, Lepson/support/SupportActivity$SwitchAdapter;->mOriginalChangeListenerEnabled:Z

    .line 219
    iget-object v1, p0, Lepson/support/SupportActivity$SwitchAdapter;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 220
    iput-boolean v0, p0, Lepson/support/SupportActivity$SwitchAdapter;->mOriginalChangeListenerEnabled:Z

    return-void
.end method

.method public setListenerEnabled(Z)V
    .locals 0

    .line 213
    iput-boolean p1, p0, Lepson/support/SupportActivity$SwitchAdapter;->mOriginalChangeListenerEnabled:Z

    return-void
.end method

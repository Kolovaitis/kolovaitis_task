.class Lepson/support/DeviceInformationActivity$1;
.super Ljava/lang/Object;
.source "DeviceInformationActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/support/DeviceInformationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/support/DeviceInformationActivity;


# direct methods
.method constructor <init>(Lepson/support/DeviceInformationActivity;)V
    .locals 0

    .line 254
    iput-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 10

    const-string v0, "IprintHome"

    const-string v1, "HandlerCallback"

    .line 256
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PRINTER_LOCATION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "printerLocation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 261
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    const v3, 0x7f0e0547

    const/4 v4, 0x3

    const v5, 0x7f0e02f6

    const v6, 0x7f0e043c

    const/4 v7, 0x0

    const/4 v8, 0x1

    packed-switch v1, :pswitch_data_0

    const-string p1, "IprintHome"

    const-string v0, "default"

    .line 355
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    const v0, 0x7f0e0439

    invoke-virtual {p1, v0}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v7}, Lepson/support/DeviceInformationActivity;->access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V

    goto/16 :goto_5

    .line 318
    :pswitch_0
    iget-object v1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {v1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v9, "STATUS_ERROR"

    invoke-static {v1, v9}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "STATUS_ERROR"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object p1

    if-ne v0, v8, :cond_0

    .line 321
    iget-object v0, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    .line 322
    invoke-virtual {v0}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "printer"

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v3}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v8}, Lepson/support/DeviceInformationActivity;->access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V

    .line 325
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stt title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {v1, v6}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_0
    const v0, 0x7f0e0542

    if-eqz p1, :cond_5

    .line 328
    iget-object v1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {v1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "statusError[0] : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v9, p1, v7

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-object v1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {v1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "statusError[1] : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v9, p1, v8

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    aget v1, p1, v7

    if-eq v1, v2, :cond_4

    aget v1, p1, v7

    if-eq v1, v8, :cond_4

    aget v1, p1, v7

    if-ne v1, v4, :cond_1

    goto :goto_0

    .line 336
    :cond_1
    aget v1, p1, v7

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    aget v1, p1, v8

    const/16 v2, 0x66

    if-eq v1, v2, :cond_2

    aget p1, p1, v8

    const/16 v1, -0x514

    if-ne p1, v1, :cond_3

    .line 338
    :cond_2
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v5}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v7}, Lepson/support/DeviceInformationActivity;->access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V

    .line 339
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stt title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {v1, v5}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 342
    :cond_3
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v0}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v8}, Lepson/support/DeviceInformationActivity;->access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V

    .line 343
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {v2, v0}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 334
    :cond_4
    :goto_0
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v6}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v8}, Lepson/support/DeviceInformationActivity;->access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V

    .line 335
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stt title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {v1, v6}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 347
    :cond_5
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v0}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v8}, Lepson/support/DeviceInformationActivity;->access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V

    .line 348
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt titlef: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {v2, v0}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 303
    :pswitch_1
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "PROBE_ERROR"

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-ne v0, v8, :cond_6

    .line 304
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    .line 305
    invoke-virtual {p1}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "printer"

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 307
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v3}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 310
    :cond_6
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v5}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 313
    :goto_1
    iget-object v0, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {v0, p1, v7}, Lepson/support/DeviceInformationActivity;->access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V

    .line 314
    iget-object v0, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {v0}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 264
    :pswitch_2
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "UPDATE_PRINTER_STATUS"

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$100(Lepson/support/DeviceInformationActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object p1

    .line 267
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "status "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget p1, p1, v7

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    if-ne v0, v8, :cond_7

    .line 272
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    .line 273
    invoke-virtual {p1}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "printer"

    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 275
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v3}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v7, 0x1

    goto :goto_4

    :cond_7
    if-eq v0, v8, :cond_c

    if-ne v0, v4, :cond_8

    .line 277
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    .line 278
    invoke-virtual {p1}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_8

    goto :goto_3

    :cond_8
    if-eq v0, v2, :cond_a

    if-ne v0, v4, :cond_9

    .line 282
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    .line 283
    invoke-virtual {p1}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_9

    goto :goto_2

    .line 295
    :cond_9
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    const v0, 0x7f0e03f6

    invoke-virtual {p1, v0}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 285
    :cond_a
    :goto_2
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1}, Lepson/support/DeviceInformationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isOnline(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_b

    .line 288
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    const v0, 0x7f0e02f0

    invoke-virtual {p1, v0}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 291
    :cond_b
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    const v0, 0x7f0e0445

    invoke-virtual {p1, v0}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 280
    :cond_c
    :goto_3
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-virtual {p1, v6}, Lepson/support/DeviceInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v7, 0x1

    .line 297
    :goto_4
    iget-object v0, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {v0, p1, v7}, Lepson/support/DeviceInformationActivity;->access$200(Lepson/support/DeviceInformationActivity;Ljava/lang/String;Z)V

    .line 298
    iget-object v0, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {v0}, Lepson/support/DeviceInformationActivity;->access$000(Lepson/support/DeviceInformationActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :goto_5
    iget-object p1, p0, Lepson/support/DeviceInformationActivity$1;->this$0:Lepson/support/DeviceInformationActivity;

    invoke-static {p1}, Lepson/support/DeviceInformationActivity;->access$300(Lepson/support/DeviceInformationActivity;)V

    return v8

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class public Lepson/common/IprintLicenseInfo;
.super Lcom/epson/mobilephone/common/license/DefaultLicenseInfo;
.source "IprintLicenseInfo.java"


# static fields
.field private static final LICENSE_VERSION_CODE:I = 0x11176

.field private static final PRIVACY_STATEMENT_VERSION_CODE:I = 0xecb8


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/DefaultLicenseInfo;-><init>()V

    return-void
.end method

.method public static beforeLicenseCheck(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 99
    new-instance v0, Lepson/common/IprintAppRecord;

    invoke-direct {v0}, Lepson/common/IprintAppRecord;-><init>()V

    .line 100
    invoke-virtual {v0, p0}, Lepson/common/IprintAppRecord;->getLicenseAgreementValue(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->clearRemotePrintersInfo()V

    .line 105
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p0

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->clearIpPrintersInfo()V

    :cond_0
    return-void
.end method

.method private getLicenseText(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "License.txt"

    .line 77
    invoke-static {p1, v0}, Lepson/common/Utils;->getAssetsFileContents(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static isAgreedCurrentVersion(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 89
    new-instance v0, Lepson/common/IprintLicenseInfo;

    invoke-direct {v0}, Lepson/common/IprintLicenseInfo;-><init>()V

    .line 90
    invoke-virtual {v0, p0}, Lepson/common/IprintLicenseInfo;->getLicenseAgreement(Landroid/content/Context;)I

    move-result p0

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public getApplicationName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const v0, 0x7f0e02ac

    .line 82
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getDocumentString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    packed-switch p2, :pswitch_data_0

    .line 72
    invoke-super {p0, p1, p2}, Lcom/epson/mobilephone/common/license/DefaultLicenseInfo;->getDocumentString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_0
    const-string p2, "oss_license.txt"

    .line 55
    invoke-static {p1, p2}, Lepson/common/Utils;->getAssetsFileContents(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 62
    :pswitch_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 64
    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "iPrint_privacy_statement_jp20180122.txt"

    .line 65
    invoke-static {p1, p2}, Lepson/common/Utils;->getAssetsFileContents(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p2, "iPrint_PRIVACY_STATEMENT20180322.txt"

    .line 67
    invoke-static {p1, p2}, Lepson/common/Utils;->getAssetsFileContents(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 58
    :pswitch_2
    invoke-direct {p0, p1}, Lepson/common/IprintLicenseInfo;->getLicenseText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getLicenseAgreement(Landroid/content/Context;)I
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 24
    new-instance v0, Lepson/common/IprintAppRecord;

    invoke-direct {v0}, Lepson/common/IprintAppRecord;-><init>()V

    .line 25
    invoke-virtual {v0, p1}, Lepson/common/IprintAppRecord;->getLicenseAgreementValue(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, -0x1

    return p1

    .line 29
    :cond_0
    invoke-virtual {v0, p1}, Lepson/common/IprintAppRecord;->getLicenseAgreeVersionCode(Landroid/content/Context;)I

    move-result p1

    const v0, 0x11176

    const v1, 0xecb8

    if-ge p1, v0, :cond_2

    if-ge p1, v1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    return p1

    :cond_2
    if-ge p1, v1, :cond_3

    const/4 p1, 0x2

    goto :goto_1

    :cond_3
    const/4 p1, 0x3

    :goto_1
    return p1
.end method

.method public setLicenceAgreement(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 46
    new-instance v0, Lepson/common/IprintAppRecord;

    invoke-direct {v0}, Lepson/common/IprintAppRecord;-><init>()V

    const v1, 0x11176

    const v2, 0xecb8

    .line 47
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/4 v2, 0x1

    .line 46
    invoke-virtual {v0, p1, v1, v2}, Lepson/common/IprintAppRecord;->setLicenseAgreement(Landroid/content/Context;IZ)V

    return-void
.end method

.class public Lepson/common/DialogProgress;
.super Landroid/support/v4/app/DialogFragment;
.source "DialogProgress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/common/DialogProgress$DialogButtonClick;
    }
.end annotation


# static fields
.field public static final MESSAGE_BOTTOM:I = 0x1

.field public static final MESSAGE_ONLY:I = 0x2

.field public static final MESSAGE_RIGHT:I = 0x0

.field private static final TAG_DIALOG:Ljava/lang/String; = "tag"

.field private static final TAG_MESSAGE:Ljava/lang/String; = "message"

.field private static final TAG_NEGATIVE_MESSAGE:Ljava/lang/String; = "negative_dialog_message"

.field private static final TAG_NEUTRAL_MESSAGE:Ljava/lang/String; = "neutral_dialog_message"

.field private static final TAG_POSITION:Ljava/lang/String; = "position"

.field private static final TAG_POSITIVE_MESSAGE:Ljava/lang/String; = "positive_dialog_message"

.field private static final TAG_TITLE_MESSAGE:Ljava/lang/String; = "title_message"


# instance fields
.field private mActivity:Lepson/common/DialogProgress$DialogButtonClick;

.field private mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private deleteFragment(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 0

    .line 89
    invoke-virtual {p1, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgress;

    if-nez p1, :cond_0

    return-void

    .line 94
    :cond_0
    invoke-virtual {p1}, Lepson/common/DialogProgress;->getDialog()Landroid/app/Dialog;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 95
    invoke-virtual {p2}, Landroid/app/Dialog;->isShowing()Z

    move-result p2

    if-nez p2, :cond_1

    goto :goto_0

    .line 99
    :cond_1
    invoke-virtual {p1}, Lepson/common/DialogProgress;->dismiss()V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method public static synthetic lambda$onCreateDialog$0(Lepson/common/DialogProgress;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 135
    invoke-virtual {p0}, Lepson/common/DialogProgress;->dismiss()V

    .line 136
    iget-object p1, p0, Lepson/common/DialogProgress;->mActivity:Lepson/common/DialogProgress$DialogButtonClick;

    if-eqz p1, :cond_0

    .line 137
    iget-object p2, p0, Lepson/common/DialogProgress;->mTag:Ljava/lang/String;

    invoke-interface {p1, p2}, Lepson/common/DialogProgress$DialogButtonClick;->onPositiveClick(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static synthetic lambda$onCreateDialog$1(Lepson/common/DialogProgress;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 144
    invoke-virtual {p0}, Lepson/common/DialogProgress;->dismiss()V

    .line 145
    iget-object p1, p0, Lepson/common/DialogProgress;->mActivity:Lepson/common/DialogProgress$DialogButtonClick;

    if-eqz p1, :cond_0

    .line 146
    iget-object p2, p0, Lepson/common/DialogProgress;->mTag:Ljava/lang/String;

    invoke-interface {p1, p2}, Lepson/common/DialogProgress$DialogButtonClick;->onNeutralClick(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static synthetic lambda$onCreateDialog$2(Lepson/common/DialogProgress;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 153
    invoke-virtual {p0}, Lepson/common/DialogProgress;->dismiss()V

    .line 154
    iget-object p1, p0, Lepson/common/DialogProgress;->mActivity:Lepson/common/DialogProgress$DialogButtonClick;

    if-eqz p1, :cond_0

    .line 155
    iget-object p2, p0, Lepson/common/DialogProgress;->mTag:Ljava/lang/String;

    invoke-interface {p1, p2}, Lepson/common/DialogProgress$DialogButtonClick;->onNegativeClick(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    .line 57
    invoke-static/range {v0 .. v6}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 70
    new-instance v0, Lepson/common/DialogProgress;

    invoke-direct {v0}, Lepson/common/DialogProgress;-><init>()V

    .line 71
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "tag"

    .line 72
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "position"

    .line 73
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "message"

    .line 74
    invoke-virtual {v1, p0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "title_message"

    .line 75
    invoke-virtual {v1, p0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "positive_dialog_message"

    .line 76
    invoke-virtual {v1, p0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "neutral_dialog_message"

    .line 77
    invoke-virtual {v1, p0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "negative_dialog_message"

    .line 78
    invoke-virtual {v1, p0, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .line 196
    iget-object v0, p0, Lepson/common/DialogProgress;->mActivity:Lepson/common/DialogProgress$DialogButtonClick;

    if-eqz v0, :cond_0

    .line 197
    iget-object v1, p0, Lepson/common/DialogProgress;->mTag:Ljava/lang/String;

    invoke-interface {v0, v1}, Lepson/common/DialogProgress$DialogButtonClick;->onCancelDialog(Ljava/lang/String;)V

    .line 199
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 106
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/common/DialogProgress;->mTag:Ljava/lang/String;

    .line 107
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    .line 108
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "title_message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 110
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "positive_dialog_message"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 111
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "neutral_dialog_message"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 112
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "negative_dialog_message"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 115
    new-instance v5, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/common/DialogProgress;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 116
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const/4 v7, 0x0

    const v8, 0x7f0a0056

    .line 117
    invoke-virtual {v6, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 121
    :try_start_0
    invoke-virtual {p0}, Lepson/common/DialogProgress;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    check-cast v8, Lepson/common/DialogProgress$DialogButtonClick;

    iput-object v8, p0, Lepson/common/DialogProgress;->mActivity:Lepson/common/DialogProgress$DialogButtonClick;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 127
    :catch_0
    iput-object v7, p0, Lepson/common/DialogProgress;->mActivity:Lepson/common/DialogProgress$DialogButtonClick;

    :goto_0
    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {v5, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    :cond_0
    if-eqz v2, :cond_1

    .line 134
    new-instance v1, Lepson/common/-$$Lambda$DialogProgress$hUTEtiuDdQfDjz-f3FkA0XQ_9Zo;

    invoke-direct {v1, p0}, Lepson/common/-$$Lambda$DialogProgress$hUTEtiuDdQfDjz-f3FkA0XQ_9Zo;-><init>(Lepson/common/DialogProgress;)V

    invoke-virtual {v5, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    :cond_1
    if-eqz v3, :cond_2

    .line 143
    new-instance v1, Lepson/common/-$$Lambda$DialogProgress$mgMbchJHQpkqjBRnvw9Hq8wXMUg;

    invoke-direct {v1, p0}, Lepson/common/-$$Lambda$DialogProgress$mgMbchJHQpkqjBRnvw9Hq8wXMUg;-><init>(Lepson/common/DialogProgress;)V

    invoke-virtual {v5, v3, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    :cond_2
    if-eqz v4, :cond_3

    .line 152
    new-instance v1, Lepson/common/-$$Lambda$DialogProgress$PAcUN8ul-CNfmrRSt2vhBSmZD7k;

    invoke-direct {v1, p0}, Lepson/common/-$$Lambda$DialogProgress$PAcUN8ul-CNfmrRSt2vhBSmZD7k;-><init>(Lepson/common/DialogProgress;)V

    invoke-virtual {v5, v4, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    :cond_3
    const v1, 0x7f08010b

    .line 162
    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v2, :cond_4

    .line 166
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v2, 0xe

    .line 167
    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 168
    invoke-virtual {v1, p1}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const p1, 0x7f08010c

    .line 170
    invoke-virtual {v6, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 171
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 173
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    const v2, 0x7f08010d

    if-nez p1, :cond_5

    .line 177
    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 178
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 180
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 184
    :cond_5
    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 185
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    :goto_1
    invoke-virtual {v5, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 190
    invoke-virtual {v5}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 0

    .line 204
    invoke-direct {p0, p1, p2}, Lepson/common/DialogProgress;->deleteFragment(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 205
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->showNow(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

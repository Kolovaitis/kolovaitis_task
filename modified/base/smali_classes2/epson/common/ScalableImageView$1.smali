.class Lepson/common/ScalableImageView$1;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "ScalableImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/ScalableImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private focusX:F

.field private focusY:F

.field perFocusX:F

.field perFocusY:F

.field final synthetic this$0:Lepson/common/ScalableImageView;


# direct methods
.method constructor <init>(Lepson/common/ScalableImageView;)V
    .locals 0

    .line 145
    iput-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    const/4 p1, 0x0

    .line 147
    iput p1, p0, Lepson/common/ScalableImageView$1;->focusX:F

    .line 148
    iput p1, p0, Lepson/common/ScalableImageView$1;->focusY:F

    .line 150
    iput p1, p0, Lepson/common/ScalableImageView$1;->perFocusX:F

    .line 151
    iput p1, p0, Lepson/common/ScalableImageView$1;->perFocusY:F

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5

    .line 173
    iget-object v0, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v0}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result p1

    mul-float v1, v1, p1

    invoke-static {v0, v1}, Lepson/common/ScalableImageView;->access$102(Lepson/common/ScalableImageView;F)F

    .line 176
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v0

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, v0}, Lepson/common/ScalableImageView;->access$102(Lepson/common/ScalableImageView;F)F

    .line 178
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result p1

    const/4 v0, 0x0

    cmpl-float p1, p1, v1

    if-nez p1, :cond_0

    .line 181
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1, v0}, Lepson/common/ScalableImageView;->access$002(Lepson/common/ScalableImageView;F)F

    .line 182
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1, v0}, Lepson/common/ScalableImageView;->access$302(Lepson/common/ScalableImageView;F)F

    goto/16 :goto_0

    .line 186
    :cond_0
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    int-to-float p1, p1

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v1

    mul-float p1, p1, v1

    .line 187
    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v2}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v2

    mul-float v1, v1, v2

    .line 190
    iget-object v2, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    iget v3, p0, Lepson/common/ScalableImageView$1;->focusX:F

    iget v4, p0, Lepson/common/ScalableImageView$1;->perFocusX:F

    mul-float p1, p1, v4

    sub-float/2addr v3, p1

    invoke-static {v2, v3}, Lepson/common/ScalableImageView;->access$002(Lepson/common/ScalableImageView;F)F

    .line 191
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    iget v2, p0, Lepson/common/ScalableImageView$1;->focusY:F

    iget v3, p0, Lepson/common/ScalableImageView$1;->perFocusY:F

    mul-float v1, v1, v3

    sub-float/2addr v2, v1

    invoke-static {p1, v2}, Lepson/common/ScalableImageView;->access$302(Lepson/common/ScalableImageView;F)F

    .line 194
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$000(Lepson/common/ScalableImageView;)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {p1, v1}, Lepson/common/ScalableImageView;->access$002(Lepson/common/ScalableImageView;F)F

    .line 195
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$300(Lepson/common/ScalableImageView;)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1, v0}, Lepson/common/ScalableImageView;->access$302(Lepson/common/ScalableImageView;F)F

    .line 196
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$000(Lepson/common/ScalableImageView;)F

    move-result v0

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v2}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v3}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v3

    mul-float v2, v2, v3

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, v0}, Lepson/common/ScalableImageView;->access$002(Lepson/common/ScalableImageView;F)F

    .line 197
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$300(Lepson/common/ScalableImageView;)F

    move-result v0

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v2}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v3}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v3

    mul-float v2, v2, v3

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, v0}, Lepson/common/ScalableImageView;->access$302(Lepson/common/ScalableImageView;F)F

    :goto_0
    const-string p1, "ScalableImageView"

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mScaleFactor = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " mOffsetX = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$000(Lepson/common/ScalableImageView;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " mOffsetY = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$300(Lepson/common/ScalableImageView;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object p1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-virtual {p1}, Lepson/common/ScalableImageView;->invalidate()V

    const/4 p1, 0x1

    return p1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .line 157
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    iput v0, p0, Lepson/common/ScalableImageView$1;->focusX:F

    .line 158
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v0

    iput v0, p0, Lepson/common/ScalableImageView$1;->focusY:F

    .line 161
    iget-object v0, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v0}, Lepson/common/ScalableImageView;->access$000(Lepson/common/ScalableImageView;)F

    move-result v0

    neg-float v0, v0

    iget v1, p0, Lepson/common/ScalableImageView$1;->focusX:F

    add-float/2addr v0, v1

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lepson/common/ScalableImageView$1;->perFocusX:F

    .line 162
    iget-object v0, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v0}, Lepson/common/ScalableImageView;->access$300(Lepson/common/ScalableImageView;)F

    move-result v0

    neg-float v0, v0

    iget v1, p0, Lepson/common/ScalableImageView$1;->focusY:F

    add-float/2addr v0, v1

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lepson/common/ScalableImageView$1;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lepson/common/ScalableImageView$1;->perFocusY:F

    .line 166
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result p1

    return p1
.end method

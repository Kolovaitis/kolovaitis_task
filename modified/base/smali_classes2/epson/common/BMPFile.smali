.class public Lepson/common/BMPFile;
.super Ljava/lang/Object;
.source "BMPFile.java"


# static fields
.field private static final BITMAPFILEHEADER_SIZE:I = 0xe

.field private static final BITMAPINFOHEADER_SIZE:I = 0x28


# instance fields
.field private bfOffBits:I

.field private bfReserved1:I

.field private bfReserved2:I

.field private bfSize:I

.field private bfType:[B

.field private biBitCount:I

.field private biClrImportant:I

.field private biClrUsed:I

.field private biCompression:I

.field private biHeight:I

.field private biPlanes:I

.field private biSize:I

.field private biSizeImage:I

.field private biWidth:I

.field private biXPelsPerMeter:I

.field private biYPelsPerMeter:I

.field private bitmap:[I

.field private fo:Ljava/io/FileOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 14
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lepson/common/BMPFile;->bfType:[B

    const/4 v0, 0x0

    .line 15
    iput v0, p0, Lepson/common/BMPFile;->bfSize:I

    .line 16
    iput v0, p0, Lepson/common/BMPFile;->bfReserved1:I

    .line 17
    iput v0, p0, Lepson/common/BMPFile;->bfReserved2:I

    const/16 v1, 0x36

    .line 18
    iput v1, p0, Lepson/common/BMPFile;->bfOffBits:I

    const/16 v1, 0x28

    .line 21
    iput v1, p0, Lepson/common/BMPFile;->biSize:I

    .line 22
    iput v0, p0, Lepson/common/BMPFile;->biWidth:I

    .line 23
    iput v0, p0, Lepson/common/BMPFile;->biHeight:I

    const/4 v1, 0x1

    .line 24
    iput v1, p0, Lepson/common/BMPFile;->biPlanes:I

    const/16 v1, 0x18

    .line 25
    iput v1, p0, Lepson/common/BMPFile;->biBitCount:I

    .line 26
    iput v0, p0, Lepson/common/BMPFile;->biCompression:I

    const/high16 v1, 0x30000

    .line 27
    iput v1, p0, Lepson/common/BMPFile;->biSizeImage:I

    .line 28
    iput v0, p0, Lepson/common/BMPFile;->biXPelsPerMeter:I

    .line 29
    iput v0, p0, Lepson/common/BMPFile;->biYPelsPerMeter:I

    .line 30
    iput v0, p0, Lepson/common/BMPFile;->biClrUsed:I

    .line 31
    iput v0, p0, Lepson/common/BMPFile;->biClrImportant:I

    return-void

    :array_0
    .array-data 1
        0x42t
        0x4dt
    .end array-data
.end method

.method private convertImage(Landroid/graphics/Bitmap;II)Z
    .locals 10

    const/4 v0, 0x0

    .line 79
    iput-object v0, p0, Lepson/common/BMPFile;->bitmap:[I

    mul-int v0, p2, p3

    .line 80
    new-array v1, v0, [I

    iput-object v1, p0, Lepson/common/BMPFile;->bitmap:[I

    .line 86
    iget-object v3, p0, Lepson/common/BMPFile;->bitmap:[I

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    move v5, p2

    move v8, p2

    move v9, p3

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    mul-int/lit8 p1, p2, 0x3

    .line 88
    rem-int/lit8 p1, p1, 0x4

    rsub-int/lit8 p1, p1, 0x4

    mul-int p1, p1, p3

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, p1

    .line 89
    iput v0, p0, Lepson/common/BMPFile;->biSizeImage:I

    .line 90
    iget p1, p0, Lepson/common/BMPFile;->biSizeImage:I

    add-int/lit8 p1, p1, 0xe

    add-int/lit8 p1, p1, 0x28

    iput p1, p0, Lepson/common/BMPFile;->bfSize:I

    .line 92
    iput p2, p0, Lepson/common/BMPFile;->biWidth:I

    .line 93
    iput p3, p0, Lepson/common/BMPFile;->biHeight:I

    const/4 p1, 0x1

    return p1
.end method

.method private intToDWord(I)[B
    .locals 3

    const/4 v0, 0x4

    .line 222
    new-array v0, v0, [B

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x2

    aput-byte v1, v0, v2

    shr-int/lit8 p1, p1, 0x18

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    return-object v0
.end method

.method private intToWord(I)[B
    .locals 3

    const/4 v0, 0x2

    .line 210
    new-array v0, v0, [B

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    shr-int/lit8 p1, p1, 0x8

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x1

    aput-byte p1, v0, v1

    return-object v0
.end method

.method private save(Landroid/graphics/Bitmap;II)V
    .locals 0

    .line 62
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lepson/common/BMPFile;->convertImage(Landroid/graphics/Bitmap;II)Z

    .line 63
    invoke-direct {p0}, Lepson/common/BMPFile;->writeBitmapFileHeader()V

    .line 64
    invoke-direct {p0}, Lepson/common/BMPFile;->writeBitmapInfoHeader()V

    .line 65
    invoke-direct {p0}, Lepson/common/BMPFile;->writeBitmap()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 68
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private writeBitmap()V
    .locals 12

    const/4 v0, 0x3

    .line 114
    new-array v1, v0, [B

    .line 117
    iget v2, p0, Lepson/common/BMPFile;->biWidth:I

    iget v3, p0, Lepson/common/BMPFile;->biHeight:I

    mul-int v3, v3, v2

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    mul-int/lit8 v2, v2, 0x3

    const/4 v0, 0x4

    .line 119
    rem-int/2addr v2, v0

    rsub-int/lit8 v2, v2, 0x4

    const/4 v5, 0x0

    if-ne v2, v0, :cond_0

    const/4 v2, 0x0

    .line 128
    :cond_0
    iget v0, p0, Lepson/common/BMPFile;->biWidth:I

    sub-int v0, v3, v0

    move v7, v0

    move v9, v7

    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x1

    :goto_0
    add-int/lit8 v10, v3, 0x1

    if-ge v0, v10, :cond_4

    .line 134
    :try_start_0
    iget v10, p0, Lepson/common/BMPFile;->biWidth:I

    if-ge v0, v10, :cond_1

    iget-object v10, p0, Lepson/common/BMPFile;->bitmap:[I

    add-int/lit8 v11, v7, 0x1

    aget v10, v10, v11

    goto :goto_1

    .line 135
    :cond_1
    iget-object v10, p0, Lepson/common/BMPFile;->bitmap:[I

    aget v10, v10, v7

    :goto_1
    and-int/lit16 v11, v10, 0xff

    int-to-byte v11, v11

    aput-byte v11, v1, v5

    shr-int/lit8 v11, v10, 0x8

    and-int/lit16 v11, v11, 0xff

    int-to-byte v11, v11

    aput-byte v11, v1, v4

    const/4 v11, 0x2

    shr-int/lit8 v10, v10, 0x10

    and-int/lit16 v10, v10, 0xff

    int-to-byte v10, v10

    aput-byte v10, v1, v11

    .line 140
    iget-object v10, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    invoke-virtual {v10, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 141
    iget v10, p0, Lepson/common/BMPFile;->biWidth:I

    if-ne v8, v10, :cond_3

    add-int/2addr v6, v2

    const/4 v7, 0x1

    :goto_2
    if-gt v7, v2, :cond_2

    .line 144
    iget-object v8, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    invoke-virtual {v8, v5}, Ljava/io/FileOutputStream;->write(I)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 147
    :cond_2
    iget v7, p0, Lepson/common/BMPFile;->biWidth:I

    sub-int v7, v9, v7

    move v9, v7

    const/4 v8, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v8, v8, 0x1

    :goto_3
    add-int/2addr v7, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_4
    iget v0, p0, Lepson/common/BMPFile;->bfSize:I

    sub-int/2addr v6, v2

    add-int/2addr v0, v6

    iput v0, p0, Lepson/common/BMPFile;->bfSize:I

    .line 156
    iget v0, p0, Lepson/common/BMPFile;->biSizeImage:I

    add-int/2addr v0, v6

    iput v0, p0, Lepson/common/BMPFile;->biSizeImage:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    .line 159
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    return-void
.end method

.method private writeBitmapFileHeader()V
    .locals 2

    .line 169
    :try_start_0
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget-object v1, p0, Lepson/common/BMPFile;->bfType:[B

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 170
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->bfSize:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 171
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->bfReserved1:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 172
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->bfReserved2:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 173
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->bfOffBits:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 176
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private writeBitmapInfoHeader()V
    .locals 2

    .line 187
    :try_start_0
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biSize:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 188
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biWidth:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 189
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biHeight:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 190
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biPlanes:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 191
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biBitCount:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 192
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biCompression:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 193
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biSizeImage:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 194
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biXPelsPerMeter:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 195
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biYPelsPerMeter:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 196
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biClrUsed:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 197
    iget-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    iget v1, p0, Lepson/common/BMPFile;->biClrImportant:I

    invoke-direct {p0, v1}, Lepson/common/BMPFile;->intToDWord(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 200
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public saveBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;II)V
    .locals 1

    .line 43
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    .line 44
    invoke-direct {p0, p2, p3, p4}, Lepson/common/BMPFile;->save(Landroid/graphics/Bitmap;II)V

    .line 45
    iget-object p1, p0, Lepson/common/BMPFile;->fo:Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 48
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

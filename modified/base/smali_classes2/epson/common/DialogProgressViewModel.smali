.class public Lepson/common/DialogProgressViewModel;
.super Landroid/arch/lifecycle/ViewModel;
.source "DialogProgressViewModel.java"


# static fields
.field public static final DO_DISMISS:Ljava/lang/String; = "do_dismiss"

.field public static final DO_SHOW:Ljava/lang/String; = "do_show"


# instance fields
.field private final mJob:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData<",
            "Ljava/util/Deque<",
            "[",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mQueue:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque<",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Landroid/arch/lifecycle/ViewModel;-><init>()V

    .line 21
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lepson/common/DialogProgressViewModel;->mJob:Landroid/arch/lifecycle/MutableLiveData;

    .line 24
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lepson/common/DialogProgressViewModel;->mQueue:Ljava/util/Deque;

    return-void
.end method


# virtual methods
.method public checkQueue()[Ljava/lang/String;
    .locals 3

    .line 47
    iget-object v0, p0, Lepson/common/DialogProgressViewModel;->mQueue:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 50
    iget-object v1, p0, Lepson/common/DialogProgressViewModel;->mJob:Landroid/arch/lifecycle/MutableLiveData;

    iget-object v2, p0, Lepson/common/DialogProgressViewModel;->mQueue:Ljava/util/Deque;

    invoke-virtual {v1, v2}, Landroid/arch/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    :cond_0
    return-object v0
.end method

.method public doDismiss(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    .line 33
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "do_dismiss"

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 34
    iget-object p1, p0, Lepson/common/DialogProgressViewModel;->mQueue:Ljava/util/Deque;

    invoke-interface {p1, v0}, Ljava/util/Deque;->offer(Ljava/lang/Object;)Z

    .line 35
    iget-object p1, p0, Lepson/common/DialogProgressViewModel;->mJob:Landroid/arch/lifecycle/MutableLiveData;

    iget-object v0, p0, Lepson/common/DialogProgressViewModel;->mQueue:Ljava/util/Deque;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    return-void
.end method

.method public doShow(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    .line 27
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "do_show"

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 28
    iget-object p1, p0, Lepson/common/DialogProgressViewModel;->mQueue:Ljava/util/Deque;

    invoke-interface {p1, v0}, Ljava/util/Deque;->offer(Ljava/lang/Object;)Z

    .line 29
    iget-object p1, p0, Lepson/common/DialogProgressViewModel;->mJob:Landroid/arch/lifecycle/MutableLiveData;

    iget-object v0, p0, Lepson/common/DialogProgressViewModel;->mQueue:Ljava/util/Deque;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    return-void
.end method

.method public getDialogJob()Landroid/arch/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/MutableLiveData<",
            "Ljava/util/Deque<",
            "[",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lepson/common/DialogProgressViewModel;->mJob:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

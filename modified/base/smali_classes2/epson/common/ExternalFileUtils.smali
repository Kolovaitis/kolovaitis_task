.class public Lepson/common/ExternalFileUtils;
.super Ljava/lang/Object;
.source "ExternalFileUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/common/ExternalFileUtils$TempCacheDirectory;
    }
.end annotation


# static fields
.field private static AREA_INFO:Ljava/lang/String; = "area_info.dat"

.field private static EPSONIPRINT_DOWNLOAD_FOLDER:Ljava/lang/String; = "download/"

.field private static INFO_FOLDER_IPPRINTER:Ljava/lang/String; = "ipprinters/"

.field private static INFO_FOLDER_REMOTEPRINTER:Ljava/lang/String; = "printers/"

.field private static PREVIEW_FOLDER:Ljava/lang/String; = "tempPDF"

.field private static PRE_AREA_INFO:Ljava/lang/String; = "pre_area_info.dat"

.field private static PRE_SUPPORTED_MEDIA:Ljava/lang/String; = "pre_supported_media.dat"

.field private static PRINT_FOLDER:Ljava/lang/String; = "print"

.field private static PRINT_TEMP_FOLDER:Ljava/lang/String; = "temp"

.field private static SAVE_AREA_INFO:Ljava/lang/String; = "save_area_info.dat"

.field private static SAVE_SUPPORTED_MEDIA:Ljava/lang/String; = "save_supported_media.dat"

.field private static SCAN_SCANNED_IMAGE_PATH:Ljava/lang/String; = "tempScan/"

.field private static final SHARED:Ljava/lang/String; = "shared"

.field private static SUPPORTED_MEDIA:Ljava/lang/String; = "supported_media.dat"

.field private static final TAG:Ljava/lang/String; = "ExternalFileUtils"

.field private static TEMP_CR_FOLDER:Ljava/lang/String; = "tempCR"

.field private static TEMP_VIEW_FOLDER:Ljava/lang/String; = "tempView"

.field private static externalFileUtils:Lepson/common/ExternalFileUtils;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lepson/common/ExternalFileUtils;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static getConvertFilename(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 487
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s_%02x.jpg"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 488
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 p1, 0x1

    aput-object p0, v2, p1

    .line 487
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 489
    new-instance p1, Ljava/io/File;

    invoke-direct {p1, p2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;
    .locals 1

    .line 91
    sget-object v0, Lepson/common/ExternalFileUtils;->externalFileUtils:Lepson/common/ExternalFileUtils;

    if-eqz v0, :cond_0

    return-object v0

    .line 94
    :cond_0
    new-instance v0, Lepson/common/ExternalFileUtils;

    invoke-direct {v0, p0}, Lepson/common/ExternalFileUtils;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static getNotDuplicateFilename(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 475
    :cond_1
    invoke-static {p0, v1, p1}, Lepson/common/ExternalFileUtils;->getConvertFilename(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 476
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    return-object v2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    if-lt v1, p2, :cond_1

    return-object v0
.end method


# virtual methods
.method public clearIpPrintersInfo()V
    .locals 1

    .line 293
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getIpPrintersInfo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public clearRemotePrintersInfo()V
    .locals 1

    .line 286
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getRemotePrintersInfo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public clearTempFoler(Ljava/lang/String;)V
    .locals 5

    .line 145
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 v1, 0x0

    .line 151
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    .line 152
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "ExternalFileUtils"

    .line 153
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clearTempFoler delete() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, p1, v1

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 155
    :cond_0
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 156
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "ExternalFileUtils"

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearTempFoler delete() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_3
    return-void
.end method

.method public createTempFolder(Ljava/lang/String;)Z
    .locals 4

    .line 133
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ExternalFileUtils"

    .line 135
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createTempFolder = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getAreaInfo()Ljava/io/File;
    .locals 3

    .line 211
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->AREA_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCacheDir()Ljava/lang/String;
    .locals 1

    .line 117
    iget-object v0, p0, Lepson/common/ExternalFileUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 122
    :cond_0
    iget-object v0, p0, Lepson/common/ExternalFileUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadDir()Ljava/lang/String;
    .locals 3

    .line 309
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->EPSONIPRINT_DOWNLOAD_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFilesDir()Ljava/lang/String;
    .locals 2

    .line 102
    iget-object v0, p0, Lepson/common/ExternalFileUtils;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 107
    :cond_0
    iget-object v0, p0, Lepson/common/ExternalFileUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIpPrintersInfo()Ljava/lang/String;
    .locals 3

    .line 279
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getWorkingDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->INFO_FOLDER_IPPRINTER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPdfDir()Ljava/lang/String;
    .locals 3

    .line 387
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->PREVIEW_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreAreaInfo()Ljava/io/File;
    .locals 3

    .line 227
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->PRE_AREA_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getPreSupportedMedia()Ljava/io/File;
    .locals 3

    .line 203
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->PRE_SUPPORTED_MEDIA:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getPrintDir()Ljava/lang/String;
    .locals 3

    .line 326
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->PRINT_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrintTmpDir()Ljava/lang/String;
    .locals 3

    .line 335
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->PRINT_TEMP_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemotePrintersInfo()Ljava/lang/String;
    .locals 3

    .line 270
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getWorkingDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->INFO_FOLDER_REMOTEPRINTER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSavedAreaInfo()Ljava/io/File;
    .locals 3

    .line 219
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->SAVE_AREA_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getSavedSupportedMedia()Ljava/io/File;
    .locals 3

    .line 195
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->SAVE_SUPPORTED_MEDIA:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getScannedImageDir()Ljava/lang/String;
    .locals 3

    .line 404
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->SCAN_SCANNED_IMAGE_PATH:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedMedia()Ljava/io/File;
    .locals 3

    .line 187
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->SUPPORTED_MEDIA:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getSupportedMediaDir()Ljava/lang/String;
    .locals 1

    .line 179
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getFilesDir()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTempCRDir()Ljava/lang/String;
    .locals 3

    .line 370
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->TEMP_CR_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTempCacheDir(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;
    .locals 2
    .param p1    # Lepson/common/ExternalFileUtils$TempCacheDirectory;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 441
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils$TempCacheDirectory;->getRelativeDirectory()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getTempSharedDir()Ljava/lang/String;
    .locals 3

    .line 422
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v1

    const-string v2, "shared"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTempViewDir()Ljava/lang/String;
    .locals 3

    .line 353
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils;->TEMP_VIEW_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWorkingDir()Ljava/lang/String;
    .locals 2

    .line 261
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getFilesDir()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initDownloadDir()V
    .locals 1

    .line 316
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getDownloadDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 317
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getDownloadDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public initPdfDir()V
    .locals 1

    .line 394
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getPdfDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 395
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getPdfDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public initPrintDir()V
    .locals 1

    .line 342
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 343
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getPrintTmpDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 344
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public initScannedImageDir()V
    .locals 1

    .line 411
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 412
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public initTempCRDir()V
    .locals 1

    .line 377
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getTempCRDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 378
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getTempCRDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public initTempCacheDirectory(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;
    .locals 1
    .param p1    # Lepson/common/ExternalFileUtils$TempCacheDirectory;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 452
    invoke-virtual {p0, p1}, Lepson/common/ExternalFileUtils;->getTempCacheDir(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;

    move-result-object p1

    .line 453
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 454
    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 455
    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-object p1
.end method

.method public initTempSharedDir()V
    .locals 1

    .line 431
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getTempSharedDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 432
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getTempSharedDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public initTempViewDir()V
    .locals 1

    .line 360
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 361
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    return-void
.end method

.method public removeAreaInfo()V
    .locals 2

    .line 235
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 239
    :cond_0
    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getSavedAreaInfo()Ljava/io/File;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    return-void
.end method

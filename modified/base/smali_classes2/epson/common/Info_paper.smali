.class public Lepson/common/Info_paper;
.super Ljava/lang/Object;
.source "Info_paper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/common/Info_paper$AreaInfoCache;
    }
.end annotation


# static fields
.field public static boder_pixel_300:I = 0x23

.field public static boder_pixel_360:I = 0x2a


# instance fields
.field private leftMargin:I

.field private leftMargin_border:I

.field private paper_height:I

.field private paper_height_boder:I

.field private paper_height_boderless:I

.field private paper_width:I

.field private paper_width_boder:I

.field private paper_width_boderless:I

.field private resolution:I

.field private topMargin:I

.field private topMargin_border:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(IIIIIIIII)V
    .locals 0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput p2, p0, Lepson/common/Info_paper;->paper_height:I

    .line 129
    iput p1, p0, Lepson/common/Info_paper;->paper_width:I

    .line 130
    iput p4, p0, Lepson/common/Info_paper;->paper_height_boder:I

    .line 131
    iput p8, p0, Lepson/common/Info_paper;->paper_height_boderless:I

    sub-int/2addr p1, p3

    .line 133
    div-int/lit8 p1, p1, 0x2

    iput p1, p0, Lepson/common/Info_paper;->leftMargin_border:I

    .line 134
    invoke-static {p9}, Lepson/common/Info_paper;->getBorder_pixel(I)I

    move-result p1

    iput p1, p0, Lepson/common/Info_paper;->topMargin_border:I

    .line 136
    iput p3, p0, Lepson/common/Info_paper;->paper_width_boder:I

    .line 137
    iput p7, p0, Lepson/common/Info_paper;->paper_width_boderless:I

    .line 138
    iput p6, p0, Lepson/common/Info_paper;->topMargin:I

    .line 139
    iput p5, p0, Lepson/common/Info_paper;->leftMargin:I

    return-void
.end method

.method public static get360DpiPaperSizeInfo(I)Lepson/common/Info_paper;
    .locals 12

    .line 185
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMediaInfo(II)[I

    move-result-object p0

    if-eqz p0, :cond_0

    .line 187
    new-instance v0, Lepson/common/Info_paper;

    const/4 v2, 0x0

    aget v3, p0, v2

    aget v4, p0, v1

    const/4 v1, 0x2

    aget v5, p0, v1

    const/4 v1, 0x3

    aget v6, p0, v1

    const/4 v1, 0x4

    aget v7, p0, v1

    const/4 v1, 0x5

    aget v8, p0, v1

    const/4 v1, 0x6

    aget v9, p0, v1

    const/4 v1, 0x7

    aget v10, p0, v1

    const/4 v11, 0x1

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lepson/common/Info_paper;-><init>(IIIIIIIII)V

    return-object v0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getBorder_pixel(I)I
    .locals 1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_2

    const/16 v0, 0x8

    if-eq p0, v0, :cond_1

    const/16 v0, 0x10

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    .line 243
    sget p0, Lepson/common/Info_paper;->boder_pixel_360:I

    return p0

    .line 241
    :pswitch_0
    sget p0, Lepson/common/Info_paper;->boder_pixel_360:I

    mul-int/lit8 p0, p0, 0x2

    return p0

    .line 239
    :pswitch_1
    sget p0, Lepson/common/Info_paper;->boder_pixel_360:I

    return p0

    .line 237
    :cond_0
    sget p0, Lepson/common/Info_paper;->boder_pixel_300:I

    mul-int/lit8 p0, p0, 0x2

    return p0

    .line 235
    :cond_1
    sget p0, Lepson/common/Info_paper;->boder_pixel_300:I

    return p0

    .line 233
    :cond_2
    sget p0, Lepson/common/Info_paper;->boder_pixel_300:I

    div-int/lit8 p0, p0, 0x2

    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getCurPrinterResolution(Landroid/content/Context;)I
    .locals 0

    .line 209
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p0

    invoke-virtual {p0}, Lepson/print/MyPrinter;->getLang()I

    move-result p0

    packed-switch p0, :pswitch_data_0

    const/16 p0, 0x8

    goto :goto_0

    :pswitch_0
    const/4 p0, 0x1

    :goto_0
    return p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;
    .locals 10

    .line 154
    invoke-static {p0}, Lepson/common/Info_paper;->getCurPrinterResolution(Landroid/content/Context;)I

    move-result v9

    .line 157
    invoke-static {p0, v9}, Lepson/common/Info_paper$AreaInfoCache;->getAreaInfo(Landroid/content/Context;I)Ljava/util/Hashtable;

    move-result-object p0

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    .line 159
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 161
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lepson/common/Info_paper;

    return-object p0

    .line 165
    :cond_0
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p0

    invoke-virtual {p0, p1, v9}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMediaInfo(II)[I

    move-result-object p0

    if-eqz p0, :cond_1

    .line 167
    new-instance p1, Lepson/common/Info_paper;

    const/4 v1, 0x0

    aget v1, p0, v1

    aget v2, p0, v0

    const/4 v0, 0x2

    aget v3, p0, v0

    const/4 v0, 0x3

    aget v4, p0, v0

    const/4 v0, 0x4

    aget v5, p0, v0

    const/4 v0, 0x5

    aget v6, p0, v0

    const/4 v0, 0x6

    aget v7, p0, v0

    const/4 v0, 0x7

    aget v8, p0, v0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lepson/common/Info_paper;-><init>(IIIIIIIII)V

    return-object p1

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getMultiLayoutArea(IIII)[I
    .locals 4

    const/high16 v0, 0x10000

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq p0, v0, :cond_1

    const/high16 v0, 0x20000

    if-eq p0, v0, :cond_0

    const/high16 v0, 0x40000

    if-eq p0, v0, :cond_0

    .line 502
    new-array p0, v2, [I

    aput p1, p0, v1

    aput p2, p0, v3

    return-object p0

    .line 499
    :cond_0
    invoke-static {p1, p2, p3, v3}, Lepson/common/Info_paper;->getRect_4in1(IIIZ)Landroid/graphics/Rect;

    move-result-object p0

    goto :goto_0

    .line 495
    :cond_1
    invoke-static {p1, p2, p3, v3}, Lepson/common/Info_paper;->getRect_2in1(IIIZ)Landroid/graphics/Rect;

    move-result-object p0

    .line 504
    :goto_0
    new-array p1, v2, [I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result p2

    aput p2, p1, v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result p0

    aput p0, p1, v3

    return-object p1
.end method

.method public static getRect_2in1(IIIZ)Landroid/graphics/Rect;
    .locals 2

    .line 450
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    const/4 v1, 0x0

    if-gt p0, p1, :cond_1

    .line 454
    div-int/lit8 p1, p1, 0x2

    if-eqz p3, :cond_0

    invoke-static {p2}, Lepson/common/Info_paper;->getBorder_pixel(I)I

    move-result p2

    div-int/lit8 v1, p2, 0x2

    :cond_0
    sub-int/2addr p1, v1

    goto :goto_0

    .line 457
    :cond_1
    div-int/lit8 p0, p0, 0x2

    if-eqz p3, :cond_2

    invoke-static {p2}, Lepson/common/Info_paper;->getBorder_pixel(I)I

    move-result p2

    div-int/lit8 v1, p2, 0x2

    :cond_2
    sub-int/2addr p0, v1

    .line 460
    :goto_0
    iput p0, v0, Landroid/graphics/Rect;->right:I

    .line 461
    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    return-object v0
.end method

.method public static getRect_4in1(IIIZ)Landroid/graphics/Rect;
    .locals 3

    .line 475
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 477
    div-int/lit8 p0, p0, 0x2

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    invoke-static {p2}, Lepson/common/Info_paper;->getBorder_pixel(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    sub-int/2addr p0, v2

    iput p0, v0, Landroid/graphics/Rect;->right:I

    .line 478
    div-int/lit8 p1, p1, 0x2

    if-eqz p3, :cond_1

    invoke-static {p2}, Lepson/common/Info_paper;->getBorder_pixel(I)I

    move-result p0

    div-int/lit8 v1, p0, 0x2

    :cond_1
    sub-int/2addr p1, v1

    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    return-object v0
.end method


# virtual methods
.method public getBottomMargin()I
    .locals 2

    .line 117
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v0

    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height_boderless()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lepson/common/Info_paper;->topMargin:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getBottomMargin_border()I
    .locals 2

    .line 79
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v0

    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height_boder()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lepson/common/Info_paper;->topMargin_border:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getLeftMargin()I
    .locals 1

    .line 99
    iget v0, p0, Lepson/common/Info_paper;->leftMargin:I

    return v0
.end method

.method public getLeftMargin_border()I
    .locals 1

    .line 65
    iget v0, p0, Lepson/common/Info_paper;->leftMargin_border:I

    return v0
.end method

.method public getPaperSize_2in1()Landroid/graphics/Rect;
    .locals 4

    .line 423
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v0

    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v1

    iget v2, p0, Lepson/common/Info_paper;->resolution:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lepson/common/Info_paper;->getRect_2in1(IIIZ)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getPaperSize_4in1()Landroid/graphics/Rect;
    .locals 4

    .line 428
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v0

    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v1

    iget v2, p0, Lepson/common/Info_paper;->resolution:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lepson/common/Info_paper;->getRect_4in1(IIIZ)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getPaper_height()I
    .locals 1

    .line 46
    iget v0, p0, Lepson/common/Info_paper;->paper_height:I

    return v0
.end method

.method public getPaper_height_boder()I
    .locals 1

    .line 59
    iget v0, p0, Lepson/common/Info_paper;->paper_height_boder:I

    return v0
.end method

.method public getPaper_height_boderless()I
    .locals 1

    .line 93
    iget v0, p0, Lepson/common/Info_paper;->paper_height_boderless:I

    return v0
.end method

.method public getPaper_width()I
    .locals 1

    .line 40
    iget v0, p0, Lepson/common/Info_paper;->paper_width:I

    return v0
.end method

.method public getPaper_width_boder()I
    .locals 1

    .line 53
    iget v0, p0, Lepson/common/Info_paper;->paper_width_boder:I

    return v0
.end method

.method public getPaper_width_boderless()I
    .locals 1

    .line 87
    iget v0, p0, Lepson/common/Info_paper;->paper_width_boderless:I

    return v0
.end method

.method public getPrintingArea_2in1()Landroid/graphics/Rect;
    .locals 4

    .line 433
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width_boder()I

    move-result v0

    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height_boder()I

    move-result v1

    iget v2, p0, Lepson/common/Info_paper;->resolution:I

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lepson/common/Info_paper;->getRect_2in1(IIIZ)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getPrintingArea_4in1()Landroid/graphics/Rect;
    .locals 4

    .line 438
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width_boder()I

    move-result v0

    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height_boder()I

    move-result v1

    iget v2, p0, Lepson/common/Info_paper;->resolution:I

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lepson/common/Info_paper;->getRect_4in1(IIIZ)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getRightMargin()I
    .locals 2

    .line 121
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v0

    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width_boderless()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lepson/common/Info_paper;->leftMargin:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getRightMargin_border()I
    .locals 2

    .line 83
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v0

    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width_boder()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lepson/common/Info_paper;->leftMargin_border:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getTopMargin()I
    .locals 1

    .line 105
    iget v0, p0, Lepson/common/Info_paper;->topMargin:I

    return v0
.end method

.method public getTopMargin_border()I
    .locals 1

    .line 71
    iget v0, p0, Lepson/common/Info_paper;->topMargin_border:I

    return v0
.end method

.method public setLeftMargin(I)V
    .locals 0

    .line 102
    iput p1, p0, Lepson/common/Info_paper;->leftMargin:I

    return-void
.end method

.method public setLeftMargin_border(I)V
    .locals 0

    .line 68
    iput p1, p0, Lepson/common/Info_paper;->leftMargin_border:I

    return-void
.end method

.method public setPaper_height(I)V
    .locals 0

    .line 49
    iput p1, p0, Lepson/common/Info_paper;->paper_height:I

    return-void
.end method

.method public setPaper_height_boder(I)V
    .locals 0

    .line 62
    iput p1, p0, Lepson/common/Info_paper;->paper_height_boder:I

    return-void
.end method

.method public setPaper_height_boderless(I)V
    .locals 0

    .line 96
    iput p1, p0, Lepson/common/Info_paper;->paper_height_boderless:I

    return-void
.end method

.method public setPaper_width(I)V
    .locals 0

    .line 43
    iput p1, p0, Lepson/common/Info_paper;->paper_width:I

    return-void
.end method

.method public setPaper_width_boder(I)V
    .locals 0

    .line 56
    iput p1, p0, Lepson/common/Info_paper;->paper_width_boder:I

    return-void
.end method

.method public setPaper_width_boderless(I)V
    .locals 0

    .line 90
    iput p1, p0, Lepson/common/Info_paper;->paper_width_boderless:I

    return-void
.end method

.method public setResolution(I)V
    .locals 0

    .line 112
    iput p1, p0, Lepson/common/Info_paper;->resolution:I

    return-void
.end method

.method public setTopMargin(I)V
    .locals 0

    .line 108
    iput p1, p0, Lepson/common/Info_paper;->topMargin:I

    return-void
.end method

.method public setTopMargin_border(I)V
    .locals 0

    .line 74
    iput p1, p0, Lepson/common/Info_paper;->topMargin_border:I

    return-void
.end method

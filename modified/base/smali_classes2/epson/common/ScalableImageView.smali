.class public Lepson/common/ScalableImageView;
.super Landroid/view/View;
.source "ScalableImageView.java"


# static fields
.field private static final MAX_SCALE_FACTOR:F = 5.0f

.field private static final MIN_SCALE_FACTOR:F = 1.0f

.field private static final TAG:Ljava/lang/String; = "ScalableImageView"


# instance fields
.field private bm:Landroid/graphics/Bitmap;

.field private canvasRect:Landroid/graphics/Rect;

.field private gesDetect:Landroid/view/GestureDetector;

.field private mOffsetX:F

.field private mOffsetY:F

.field private mScaleFactor:F

.field private final onGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private final onScaleGestureListener:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

.field private scaleGesDetect:Landroid/view/ScaleGestureDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 30
    iput-object p2, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    const/high16 v0, 0x3f800000    # 1.0f

    .line 36
    iput v0, p0, Lepson/common/ScalableImageView;->mScaleFactor:F

    const/4 v0, 0x0

    .line 37
    iput v0, p0, Lepson/common/ScalableImageView;->mOffsetX:F

    .line 38
    iput v0, p0, Lepson/common/ScalableImageView;->mOffsetY:F

    .line 40
    iput-object p2, p0, Lepson/common/ScalableImageView;->gesDetect:Landroid/view/GestureDetector;

    .line 41
    iput-object p2, p0, Lepson/common/ScalableImageView;->scaleGesDetect:Landroid/view/ScaleGestureDetector;

    .line 144
    new-instance p2, Lepson/common/ScalableImageView$1;

    invoke-direct {p2, p0}, Lepson/common/ScalableImageView$1;-><init>(Lepson/common/ScalableImageView;)V

    iput-object p2, p0, Lepson/common/ScalableImageView;->onScaleGestureListener:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

    .line 211
    new-instance p2, Lepson/common/ScalableImageView$2;

    invoke-direct {p2, p0}, Lepson/common/ScalableImageView$2;-><init>(Lepson/common/ScalableImageView;)V

    iput-object p2, p0, Lepson/common/ScalableImageView;->onGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 45
    new-instance p2, Landroid/view/ScaleGestureDetector;

    iget-object v0, p0, Lepson/common/ScalableImageView;->onScaleGestureListener:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

    invoke-direct {p2, p1, v0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object p2, p0, Lepson/common/ScalableImageView;->scaleGesDetect:Landroid/view/ScaleGestureDetector;

    .line 46
    new-instance p2, Landroid/view/GestureDetector;

    iget-object v0, p0, Lepson/common/ScalableImageView;->onGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {p2, p1, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p2, p0, Lepson/common/ScalableImageView;->gesDetect:Landroid/view/GestureDetector;

    return-void
.end method

.method static synthetic access$000(Lepson/common/ScalableImageView;)F
    .locals 0

    .line 23
    iget p0, p0, Lepson/common/ScalableImageView;->mOffsetX:F

    return p0
.end method

.method static synthetic access$002(Lepson/common/ScalableImageView;F)F
    .locals 0

    .line 23
    iput p1, p0, Lepson/common/ScalableImageView;->mOffsetX:F

    return p1
.end method

.method static synthetic access$100(Lepson/common/ScalableImageView;)F
    .locals 0

    .line 23
    iget p0, p0, Lepson/common/ScalableImageView;->mScaleFactor:F

    return p0
.end method

.method static synthetic access$102(Lepson/common/ScalableImageView;F)F
    .locals 0

    .line 23
    iput p1, p0, Lepson/common/ScalableImageView;->mScaleFactor:F

    return p1
.end method

.method static synthetic access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;
    .locals 0

    .line 23
    iget-object p0, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    return-object p0
.end method

.method static synthetic access$300(Lepson/common/ScalableImageView;)F
    .locals 0

    .line 23
    iget p0, p0, Lepson/common/ScalableImageView;->mOffsetY:F

    return p0
.end method

.method static synthetic access$302(Lepson/common/ScalableImageView;F)F
    .locals 0

    .line 23
    iput p1, p0, Lepson/common/ScalableImageView;->mOffsetY:F

    return p1
.end method


# virtual methods
.method public SetImageBitmap(Landroid/graphics/Bitmap;Landroid/content/res/Resources;)V
    .locals 0

    .line 58
    iget-object p2, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    if-eq p2, p1, :cond_0

    .line 59
    iput-object p1, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public SetRotate(F)V
    .locals 7

    .line 284
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 285
    invoke-virtual {v5, p1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 287
    iget-object v0, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object p1, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    .line 289
    invoke-virtual {p0}, Lepson/common/ScalableImageView;->invalidate()V

    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 50
    iget-object v0, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getmOffsetX()F
    .locals 1

    .line 72
    iget v0, p0, Lepson/common/ScalableImageView;->mOffsetX:F

    return v0
.end method

.method public getmOffsetY()F
    .locals 1

    .line 80
    iget v0, p0, Lepson/common/ScalableImageView;->mOffsetY:F

    return v0
.end method

.method public getmScaleFactor()F
    .locals 1

    .line 64
    iget v0, p0, Lepson/common/ScalableImageView;->mScaleFactor:F

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    const/high16 p1, 0x3f800000    # 1.0f

    .line 90
    iput p1, p0, Lepson/common/ScalableImageView;->mScaleFactor:F

    const/4 p1, 0x0

    .line 91
    iput p1, p0, Lepson/common/ScalableImageView;->mOffsetX:F

    .line 92
    iput p1, p0, Lepson/common/ScalableImageView;->mOffsetY:F

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 97
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 99
    iget-object v0, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 105
    iget-object v0, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 108
    iget-object v0, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 111
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v4, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v0

    div-float/2addr v4, v3

    sub-float/2addr v2, v4

    float-to-int v2, v2

    iget-object v4, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v3

    iget-object v5, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v0

    div-float/2addr v5, v3

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iget-object v5, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    .line 112
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    iget-object v6, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v0

    div-float/2addr v6, v3

    add-float/2addr v5, v6

    float-to-int v5, v5

    iget-object v6, p0, Lepson/common/ScalableImageView;->canvasRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v3

    iget-object v7, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float v7, v7, v0

    div-float/2addr v7, v3

    add-float/2addr v6, v7

    float-to-int v0, v6

    invoke-direct {v1, v2, v4, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 116
    iget v0, p0, Lepson/common/ScalableImageView;->mOffsetX:F

    iget v2, p0, Lepson/common/ScalableImageView;->mOffsetY:F

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 117
    iget v0, p0, Lepson/common/ScalableImageView;->mScaleFactor:F

    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 118
    iget-object v0, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x0

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 119
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 131
    :cond_0
    iget-object v0, p0, Lepson/common/ScalableImageView;->scaleGesDetect:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 133
    iget-object v0, p0, Lepson/common/ScalableImageView;->scaleGesDetect:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    return v1

    .line 138
    :cond_1
    iget-object v0, p0, Lepson/common/ScalableImageView;->gesDetect:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    return v1
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lepson/common/ScalableImageView;->bm:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setmOffsetX(F)V
    .locals 0

    .line 76
    iput p1, p0, Lepson/common/ScalableImageView;->mOffsetX:F

    return-void
.end method

.method public setmOffsetY(F)V
    .locals 0

    .line 84
    iput p1, p0, Lepson/common/ScalableImageView;->mOffsetY:F

    return-void
.end method

.method public setmScaleFactor(F)V
    .locals 0

    .line 68
    iput p1, p0, Lepson/common/ScalableImageView;->mScaleFactor:F

    return-void
.end method

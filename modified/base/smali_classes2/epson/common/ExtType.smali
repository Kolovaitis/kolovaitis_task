.class public final enum Lepson/common/ExtType;
.super Ljava/lang/Enum;
.source "ExtType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/common/ExtType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/common/ExtType;

.field public static final enum FileType_BMP:Lepson/common/ExtType;

.field public static final enum FileType_CSV:Lepson/common/ExtType;

.field public static final enum FileType_EXCEL_1:Lepson/common/ExtType;

.field public static final enum FileType_EXCEL_2:Lepson/common/ExtType;

.field public static final enum FileType_HEIF:Lepson/common/ExtType;

.field public static final enum FileType_HTML_1:Lepson/common/ExtType;

.field public static final enum FileType_HTML_2:Lepson/common/ExtType;

.field public static final enum FileType_JPEG:Lepson/common/ExtType;

.field public static final enum FileType_JPG:Lepson/common/ExtType;

.field public static final enum FileType_PDF:Lepson/common/ExtType;

.field public static final enum FileType_PNG:Lepson/common/ExtType;

.field public static final enum FileType_POWERPOINT_1:Lepson/common/ExtType;

.field public static final enum FileType_POWERPOINT_2:Lepson/common/ExtType;

.field public static final enum FileType_RTF:Lepson/common/ExtType;

.field public static final enum FileType_TXT:Lepson/common/ExtType;

.field public static final enum FileType_WORD_1:Lepson/common/ExtType;

.field public static final enum FileType_WORD_2:Lepson/common/ExtType;

.field public static final enum UNKNOWN_TYPE:Lepson/common/ExtType;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 8
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_JPG"

    const-string v2, ".jpg"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_JPG:Lepson/common/ExtType;

    .line 9
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_PDF"

    const-string v2, ".pdf"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_PDF:Lepson/common/ExtType;

    .line 10
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_WORD_1"

    const-string v2, ".doc"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_WORD_1:Lepson/common/ExtType;

    .line 11
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_WORD_2"

    const-string v2, ".docx"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_WORD_2:Lepson/common/ExtType;

    .line 12
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_EXCEL_1"

    const-string v2, ".xls"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_EXCEL_1:Lepson/common/ExtType;

    .line 13
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_EXCEL_2"

    const-string v2, ".xlsx"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_EXCEL_2:Lepson/common/ExtType;

    .line 14
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_POWERPOINT_1"

    const-string v2, ".ppt"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_POWERPOINT_1:Lepson/common/ExtType;

    .line 15
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_POWERPOINT_2"

    const-string v2, ".pptx"

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_POWERPOINT_2:Lepson/common/ExtType;

    .line 16
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_TXT"

    const-string v2, ".txt"

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_TXT:Lepson/common/ExtType;

    .line 17
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_RTF"

    const-string v2, ".rtf"

    const/16 v12, 0x9

    invoke-direct {v0, v1, v12, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_RTF:Lepson/common/ExtType;

    .line 18
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_CSV"

    const-string v2, ".csv"

    const/16 v13, 0xa

    invoke-direct {v0, v1, v13, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_CSV:Lepson/common/ExtType;

    .line 19
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_HTML_1"

    const-string v2, ".htm"

    const/16 v14, 0xb

    invoke-direct {v0, v1, v14, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_HTML_1:Lepson/common/ExtType;

    .line 20
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_HTML_2"

    const-string v2, ".html"

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_HTML_2:Lepson/common/ExtType;

    .line 21
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_JPEG"

    const-string v2, ".jpeg"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_JPEG:Lepson/common/ExtType;

    .line 22
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_PNG"

    const-string v2, ".png"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_PNG:Lepson/common/ExtType;

    .line 23
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_BMP"

    const-string v2, ".bmp"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_BMP:Lepson/common/ExtType;

    .line 24
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "FileType_HEIF"

    const-string v2, ".heic"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->FileType_HEIF:Lepson/common/ExtType;

    .line 25
    new-instance v0, Lepson/common/ExtType;

    const-string v1, "UNKNOWN_TYPE"

    const-string v2, ""

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15, v2}, Lepson/common/ExtType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExtType;->UNKNOWN_TYPE:Lepson/common/ExtType;

    const/16 v0, 0x12

    .line 6
    new-array v0, v0, [Lepson/common/ExtType;

    sget-object v1, Lepson/common/ExtType;->FileType_JPG:Lepson/common/ExtType;

    aput-object v1, v0, v3

    sget-object v1, Lepson/common/ExtType;->FileType_PDF:Lepson/common/ExtType;

    aput-object v1, v0, v4

    sget-object v1, Lepson/common/ExtType;->FileType_WORD_1:Lepson/common/ExtType;

    aput-object v1, v0, v5

    sget-object v1, Lepson/common/ExtType;->FileType_WORD_2:Lepson/common/ExtType;

    aput-object v1, v0, v6

    sget-object v1, Lepson/common/ExtType;->FileType_EXCEL_1:Lepson/common/ExtType;

    aput-object v1, v0, v7

    sget-object v1, Lepson/common/ExtType;->FileType_EXCEL_2:Lepson/common/ExtType;

    aput-object v1, v0, v8

    sget-object v1, Lepson/common/ExtType;->FileType_POWERPOINT_1:Lepson/common/ExtType;

    aput-object v1, v0, v9

    sget-object v1, Lepson/common/ExtType;->FileType_POWERPOINT_2:Lepson/common/ExtType;

    aput-object v1, v0, v10

    sget-object v1, Lepson/common/ExtType;->FileType_TXT:Lepson/common/ExtType;

    aput-object v1, v0, v11

    sget-object v1, Lepson/common/ExtType;->FileType_RTF:Lepson/common/ExtType;

    aput-object v1, v0, v12

    sget-object v1, Lepson/common/ExtType;->FileType_CSV:Lepson/common/ExtType;

    aput-object v1, v0, v13

    sget-object v1, Lepson/common/ExtType;->FileType_HTML_1:Lepson/common/ExtType;

    aput-object v1, v0, v14

    sget-object v1, Lepson/common/ExtType;->FileType_HTML_2:Lepson/common/ExtType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/ExtType;->FileType_JPEG:Lepson/common/ExtType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/ExtType;->FileType_PNG:Lepson/common/ExtType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/ExtType;->FileType_BMP:Lepson/common/ExtType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/ExtType;->FileType_HEIF:Lepson/common/ExtType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/ExtType;->UNKNOWN_TYPE:Lepson/common/ExtType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lepson/common/ExtType;->$VALUES:[Lepson/common/ExtType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput-object p3, p0, Lepson/common/ExtType;->name:Ljava/lang/String;

    return-void
.end method

.method public static toExtType(Ljava/lang/String;)Lepson/common/ExtType;
    .locals 5

    .line 42
    invoke-static {}, Lepson/common/ExtType;->values()[Lepson/common/ExtType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 43
    invoke-virtual {v3}, Lepson/common/ExtType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_2

    goto :goto_2

    .line 49
    :cond_2
    sget-object v3, Lepson/common/ExtType;->UNKNOWN_TYPE:Lepson/common/ExtType;

    :goto_2
    return-object v3
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/common/ExtType;
    .locals 1

    .line 6
    const-class v0, Lepson/common/ExtType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/common/ExtType;

    return-object p0
.end method

.method public static values()[Lepson/common/ExtType;
    .locals 1

    .line 6
    sget-object v0, Lepson/common/ExtType;->$VALUES:[Lepson/common/ExtType;

    invoke-virtual {v0}, [Lepson/common/ExtType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/common/ExtType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lepson/common/ExtType;->name:Ljava/lang/String;

    return-object v0
.end method

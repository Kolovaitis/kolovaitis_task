.class public Lepson/common/IprintUserSurveyInfo;
.super Lcom/epson/mobilephone/common/license/DefaultUserSurveyInfo;
.source "IprintUserSurveyInfo.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/DefaultUserSurveyInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public getResponseStatus(Landroid/content/Context;)I
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 18
    invoke-static {p1}, Lcom/epson/iprint/prtlogger/NewLoggerController;->doesAnsweredCurrentInvitation(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x2

    return p1

    .line 24
    :cond_0
    invoke-static {p1}, Lcom/epson/iprint/prtlogger/NewLoggerController;->doesAnsweredInvitation(Landroid/content/Context;)Z

    move-result p1

    return p1
.end method

.method public isUserSurveyEnabled(Landroid/content/Context;)Z
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 35
    invoke-static {p1}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result p1

    return p1
.end method

.method public setUserSurveyAgreement(Landroid/content/Context;Z)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 31
    invoke-static {p1, p2}, Lcom/epson/iprint/prtlogger/NewLoggerController;->setAnswer(Landroid/content/Context;Z)V

    return-void
.end method

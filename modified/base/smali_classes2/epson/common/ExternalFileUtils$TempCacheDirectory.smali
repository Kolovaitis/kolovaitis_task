.class public final enum Lepson/common/ExternalFileUtils$TempCacheDirectory;
.super Ljava/lang/Enum;
.source "ExternalFileUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/ExternalFileUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TempCacheDirectory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/common/ExternalFileUtils$TempCacheDirectory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/common/ExternalFileUtils$TempCacheDirectory;

.field public static final enum GOOGLE_DRIVE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

.field public static final enum MEMORY_CARD_ACCESS:Lepson/common/ExternalFileUtils$TempCacheDirectory;

.field public static final enum PHOTO_FILE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;


# instance fields
.field relativeDirectory:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 65
    new-instance v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;

    const-string v1, "MEMORY_CARD_ACCESS"

    const-string v2, "tempMemcard"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lepson/common/ExternalFileUtils$TempCacheDirectory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->MEMORY_CARD_ACCESS:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    .line 66
    new-instance v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;

    const-string v1, "PHOTO_FILE_CONVERT"

    const-string v2, "tempPhotoFConv"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lepson/common/ExternalFileUtils$TempCacheDirectory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->PHOTO_FILE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    .line 67
    new-instance v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;

    const-string v1, "GOOGLE_DRIVE_CONVERT"

    const-string v2, "tempGConvert"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lepson/common/ExternalFileUtils$TempCacheDirectory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->GOOGLE_DRIVE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    const/4 v0, 0x3

    .line 64
    new-array v0, v0, [Lepson/common/ExternalFileUtils$TempCacheDirectory;

    sget-object v1, Lepson/common/ExternalFileUtils$TempCacheDirectory;->MEMORY_CARD_ACCESS:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    aput-object v1, v0, v3

    sget-object v1, Lepson/common/ExternalFileUtils$TempCacheDirectory;->PHOTO_FILE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    aput-object v1, v0, v4

    sget-object v1, Lepson/common/ExternalFileUtils$TempCacheDirectory;->GOOGLE_DRIVE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    aput-object v1, v0, v5

    sput-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->$VALUES:[Lepson/common/ExternalFileUtils$TempCacheDirectory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput-object p3, p0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->relativeDirectory:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/common/ExternalFileUtils$TempCacheDirectory;
    .locals 1

    .line 64
    const-class v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/common/ExternalFileUtils$TempCacheDirectory;

    return-object p0
.end method

.method public static values()[Lepson/common/ExternalFileUtils$TempCacheDirectory;
    .locals 1

    .line 64
    sget-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->$VALUES:[Lepson/common/ExternalFileUtils$TempCacheDirectory;

    invoke-virtual {v0}, [Lepson/common/ExternalFileUtils$TempCacheDirectory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/common/ExternalFileUtils$TempCacheDirectory;

    return-object v0
.end method


# virtual methods
.method public getRelativeDirectory()Ljava/lang/String;
    .locals 1

    .line 76
    iget-object v0, p0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->relativeDirectory:Ljava/lang/String;

    return-object v0
.end method

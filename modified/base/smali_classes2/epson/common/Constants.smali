.class public Lepson/common/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/common/Constants$ColorName;
    }
.end annotation


# static fields
.field public static final ACTION_CONNECTED_SIMPLEAP:I = 0xa

.field public static final ACTION_PICK_PRINTER:I = 0x1

.field public static final ACTION_PICK_SCANNER:I = 0x1

.field public static final ACTION_SCANNED_EMAIL:I = 0x2

.field public static final ACTION_SCANNED_PRINT:I = 0x3

.field public static final ACTION_SCANNED_SAVE_FILE:I = 0x1

.field public static final ACTION_SCANNER_SET:I = 0x2

.field public static final ACTION_SCAN_SETTINGS_2SIDE:I = 0x7

.field public static final ACTION_SCAN_SETTINGS_BRIGHTNESS:I = 0x8

.field public static final ACTION_SCAN_SETTINGS_GAMMA:I = 0x9

.field public static final ACTION_SCAN_SETTINGS_IMAGE_TYPE:I = 0x5

.field public static final ACTION_SCAN_SETTINGS_RESOLUTION:I = 0x6

.field public static final ACTION_SCAN_SETTINGS_SIZE:I = 0xb

.field public static final ACTION_SCAN_SETTINGS_SOURCE:I = 0x4

.field public static final ACT_RESULT:Ljava/lang/String; = "ACT_RESULT"

.field public static final ACT_RESULT_BACK:Ljava/lang/String; = "ACT_RESULT_BACK"

.field public static final ACT_RESULT_SAVE:Ljava/lang/String; = "ACT_RESULT_SAVE"

.field public static final APF_PHOTO:Ljava/lang/String; = "APF_PHOTO"

.field public static final BREAK_LINE:Ljava/lang/String; = "\n"

.field public static final BRIGHTNESS:Ljava/lang/String; = "BRIGHTNESS"

.field public static final COLOR:Ljava/lang/String; = "COLOR"

.field public static final COLOR_CAMERACOPY:Ljava/lang/String; = "COLOR_CAMERACOPY"

.field public static final COLOR_INFO:Ljava/lang/String; = "COLOR_INFO"

.field public static final COLOR_PHOTO:Ljava/lang/String; = "COLOR_PHOTO"

.field public static final CONTRAST:Ljava/lang/String; = "CONTRAST"

.field public static final COPIES:Ljava/lang/String; = "COPIES"

.field public static final CROSS:Ljava/lang/String; = " / "

.field public static final C_PRINTER_IP:I = 0x3

.field public static final C_PRINTER_LOCAL:I = 0x1

.field public static final C_PRINTER_REMOTE:I = 0x2

.field public static final DELAY_SEARCH_P2P:I = 0x5

.field public static final DENSITY_VALUE:Ljava/lang/String; = "[%s]"

.field public static final DUPLEX:Ljava/lang/String; = "DUPLEX"

.field public static final DUPLEX_CAMERACOPY:Ljava/lang/String; = "DUPLEX_CAMERACOPY"

.field public static final DUPLEX_INFO:Ljava/lang/String; = "DUPLEX_INFO"

.field public static final DUPLEX_PHOTO:Ljava/lang/String; = "DUPLEX_PHOTO"

.field public static final ENABLE_EPSON_CONNECT:Ljava/lang/String; = "ENABLE_EPSON_CONNECT"

.field public static final ENABLE_SHOW_PREVIEW:Ljava/lang/String; = "ENABLE_SHOW_PREVIEW"

.field public static final ENABLE_SHOW_WARNING:Ljava/lang/String; = "ENABLE_SHOW_WARNING"

.field public static final END_PAGE:Ljava/lang/String; = "END_PAGE"

.field public static final EPS_CM_COLOR:I = 0x0

.field public static final EPS_CM_MONOCHROME:I = 0x1

.field public static final EPS_CM_SEPIA:I = 0x2

.field public static final EPS_DUPLEX_ENABLE:I = 0x1

.field public static final EPS_DUPLEX_ENABLE_BORDERLESS:I = 0x2

.field public static final EPS_DUPLEX_LONG:I = 0x1

.field public static final EPS_DUPLEX_NONE:I = 0x0

.field public static final EPS_DUPLEX_SHORT:I = 0x2

.field public static final EPS_ERR_IPPRINTER_CHANGED:I = -0x7a121

.field public static final EPS_FEEDDIR_LANDSCAPE:I = 0x1

.field public static final EPS_FEEDDIR_PORTRAIT:I = 0x0

.field public static final EPS_IR_150X150:I = 0x4

.field public static final EPS_IR_300X300:I = 0x8

.field public static final EPS_IR_360X360:I = 0x1

.field public static final EPS_IR_600X600:I = 0x10

.field public static final EPS_IR_720X720:I = 0x2

.field public static final EPS_LANG_ESCPAGE:I = 0x2

.field public static final EPS_LANG_ESCPAGE_COLOR:I = 0x3

.field public static final EPS_LANG_ESCPAGE_S:I = 0x4

.field public static final EPS_LANG_ESCPAGE_S04:I = 0x7

.field public static final EPS_LANG_ESCPR:I = 0x1

.field public static final EPS_LANG_PCL:I = 0x6

.field public static final EPS_LANG_PCL_COLOR:I = 0x5

.field public static final EPS_LANG_UNKNOWN:I = 0x0

.field public static final EPS_MLID_BORDERLESS:I = 0x1

.field public static final EPS_MLID_BORDERS:I = 0x2

.field public static final EPS_MLID_BORDERS_1:I = 0x0

.field public static final EPS_MLID_BORDERS_2in1:I = 0x10000

.field public static final EPS_MLID_BORDERS_4in1_N:I = 0x40000

.field public static final EPS_MLID_BORDERS_4in1_Z:I = 0x20000

.field public static final EPS_MLID_CDLABEL:I = 0x4

.field public static final EPS_MLID_CUSTOM:I = 0x0

.field public static final EPS_MLID_DIVIDE16:I = 0x8

.field public static final EPS_MPID_AUTO:I = 0x80

.field public static final EPS_MPID_CDTRAY:I = 0x8

.field public static final EPS_MPID_FRONT1:I = 0x2

.field public static final EPS_MPID_FRONT2:I = 0x4

.field public static final EPS_MPID_FRONT3:I = 0x20

.field public static final EPS_MPID_FRONT4:I = 0x40

.field public static final EPS_MPID_HIGHCAP:I = 0x800

.field public static final EPS_MPID_MANUAL2:I = 0x200

.field public static final EPS_MPID_MPTRAY:I = 0x8000

.field public static final EPS_MPID_MPTRAY_IJ:I = 0x400

.field public static final EPS_MPID_NOT_SPEC:I = 0x0

.field public static final EPS_MPID_REAR:I = 0x1

.field public static final EPS_MPID_REARMANUAL:I = 0x10

.field public static final EPS_MPID_ROLL:I = 0x100

.field public static final EPS_MQID_ALL:I = 0x87

.field public static final EPS_MQID_BEST_PLAIN:I = 0x80

.field public static final EPS_MQID_DRAFT:I = 0x1

.field public static final EPS_MQID_HIGH:I = 0x4

.field public static final EPS_MQID_NORMAL:I = 0x2

.field public static final EPS_MQID_UNKNOWN:I = 0x0

.field public static final EPS_PRB_BYADDR:I = 0x2

.field public static final EPS_PRB_BYID:I = 0x1

.field public static final EPS_PRNERR_3DMEDIA_DIRECTION:I = 0x1b

.field public static final EPS_PRNERR_3DMEDIA_FACE:I = 0x1a

.field public static final EPS_PRNERR_ANY:I = 0xc8

.field public static final EPS_PRNERR_BATTERYEMPTY:I = 0x11

.field public static final EPS_PRNERR_BATTERYTEMPERATURE:I = 0x10

.field public static final EPS_PRNERR_BATTERYVOLTAGE:I = 0xf

.field public static final EPS_PRNERR_BATTERY_CHARGING:I = 0x28

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_HIGH:I = 0x29

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_LOW:I = 0x2a

.field public static final EPS_PRNERR_BUSY:I = 0x64

.field public static final EPS_PRNERR_CARDLOADING:I = 0xd

.field public static final EPS_PRNERR_CARTRIDGEOVERFLOW:I = 0xe

.field public static final EPS_PRNERR_CDDVDCONFIG:I = 0x17

.field public static final EPS_PRNERR_CDDVDCONFIG_FEEDBUTTON:I = 0x23

.field public static final EPS_PRNERR_CDDVDCONFIG_STARTBUTTON:I = 0x22

.field public static final EPS_PRNERR_CDGUIDECLOSE:I = 0x6a

.field public static final EPS_PRNERR_CDREXIST_MAINTE:I = 0x18

.field public static final EPS_PRNERR_CDRGUIDEOPEN:I = 0x16

.field public static final EPS_PRNERR_CEMPTY:I = 0x67

.field public static final EPS_PRNERR_CFAIL:I = 0x68

.field public static final EPS_PRNERR_COMM3:I = 0x66

.field public static final EPS_PRNERR_COVEROPEN:I = 0x4

.field public static final EPS_PRNERR_DISABEL_CLEANING:I = 0x6c

.field public static final EPS_PRNERR_DISABLE_DUPLEX:I = 0x33

.field public static final EPS_PRNERR_DOUBLEFEED:I = 0xa

.field public static final EPS_PRNERR_FACTORY:I = 0x65

.field public static final EPS_PRNERR_FATAL:I = 0x2

.field public static final EPS_PRNERR_FEEDERCLOSE:I = 0x19

.field public static final EPS_PRNERR_GENERAL:I = 0x1

.field public static final EPS_PRNERR_INKCOVEROPEN:I = 0xb

.field public static final EPS_PRNERR_INKOUT:I = 0x6

.field public static final EPS_PRNERR_INTERFACE:I = 0x3

.field public static final EPS_PRNERR_INTERRUPT_BY_INKEND:I = 0x24

.field public static final EPS_PRNERR_JPG_LIMIT:I = 0x6b

.field public static final EPS_PRNERR_LOW_BATTERY_FNC:I = 0x27

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE:I = 0x20

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_NOLCD:I = 0x21

.field public static final EPS_PRNERR_MANUALFEED_FAILED:I = 0x1e

.field public static final EPS_PRNERR_MANUALFEED_FAILED_NOLCD:I = 0x1f

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER:I = 0x1c

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_NOLCD:I = 0x1d

.field public static final EPS_PRNERR_NOTRAY:I = 0xc

.field public static final EPS_PRNERR_NOT_INITIALFILL:I = 0x13

.field public static final EPS_PRNERR_NO_BATTERY:I = 0x26

.field public static final EPS_PRNERR_NO_MAINTENANCE_BOX:I = 0x30

.field public static final EPS_PRNERR_NO_STAPLE:I = 0x32

.field public static final EPS_PRNERR_PAPERJAM:I = 0x5

.field public static final EPS_PRNERR_PAPEROUT:I = 0x7

.field public static final EPS_PRNERR_PC_DIRECTION1:I = 0x2b

.field public static final EPS_PRNERR_PC_DIRECTION2:I = 0x2e

.field public static final EPS_PRNERR_PC_FACE1:I = 0x2c

.field public static final EPS_PRNERR_PC_FACE2:I = 0x2d

.field public static final EPS_PRNERR_PRINTPACKEND:I = 0x14

.field public static final EPS_PRNERR_REPLACE_MAINTENANCE_BOX:I = 0x2f

.field public static final EPS_PRNERR_ROLLPAPER_TOOSHORT:I = 0x25

.field public static final EPS_PRNERR_SCANNEROPEN:I = 0x15

.field public static final EPS_PRNERR_SERVICEREQ:I = 0x9

.field public static final EPS_PRNERR_SHUTOFF:I = 0x12

.field public static final EPS_PRNERR_SIZE_TYPE_PATH:I = 0x8

.field public static final EPS_PRNERR_STACKER_FULL:I = 0x31

.field public static final EPS_PRNERR_TRAYCLOSE:I = 0x69

.field public static final EPS_PRNST_BUSY:I = 0x2

.field public static final EPS_PRNST_CANCELLING:I = 0x3

.field public static final EPS_PRNST_ERROR:I = 0x4

.field public static final EPS_PRNST_IDLE:I = 0x0

.field public static final EPS_PRNST_PRINTING:I = 0x1

.field public static final EPS_SEARCH_TIMEOUT:I = 0x3c

.field public static final ERROR_GET_LAYOUT:I = -0x1

.field public static final ESCAN_ADF_DUPLEX_ENABLED:I = 0x1

.field public static final ESCAN_ADF_DUPLEX_NONE:I = 0x0

.field public static final ESCAN_DENSITY_128:I = 0x80

.field public static final ESCAN_ERR_LIB_INTIALIZED:I = -0x41a

.field public static final ESCAN_ERR_LIB_NOT_INITIALIZED:I = -0x41b

.field public static final ESCAN_ERR_MEMORY_ALLOCATION:I = -0x3e9

.field public static final ESCAN_ERR_SCANNER_NOT_FOUND:I = -0x514

.field public static final ESCAN_ERR_SCANNER_NOT_SET:I = 0x7f0e04d6

.field public static final ESCAN_ERR_SCANNER_NOT_USEFUL:I = -0x51a

.field public static final ESCAN_ERR_SCANNER_OCCUPIED:I = -0x5dd

.field public static final ESCAN_GAMMA_1_0:I = 0x0

.field public static final ESCAN_GAMMA_1_8:I = 0x1

.field public static final ESCAN_GUIDE_ALIGN_CENTER:I = 0x1

.field public static final ESCAN_GUIDE_ALIGN_RIGHT:I = 0x0

.field public static final ESCAN_JOB_ATTRIBUTE_ARRAY_ELEMENT:I = 0xd

.field public static final ESCAN_JOB_ATTRIB_VER_1:I = 0x1

.field public static final ESCAN_NUM_OF_RESOLUTIONS:I = 0x40

.field public static final ESCAN_OPTIONCONTROL_ADF:I = 0x1

.field public static final ESCAN_OPTIONCONTROL_ADF_DUPLEX:I = 0x2

.field public static final ESCAN_OPTIONCONTROL_NONE:I = 0x0

.field public static final ESCAN_ORIGIN_CENTER:I = 0x1

.field public static final ESCAN_ORIGIN_UP_LEFT:I = 0x2

.field public static final ESCAN_ORIGIN_UP_RIGHT:I = 0x0

.field public static final ESCAN_PIXELFORMAT_BGR:I = 0x1

.field public static final ESCAN_PIXELFORMAT_GRAYSCALE:I = 0x2

.field public static final ESCAN_PIXELFORMAT_MONOCHROME:I = 0x3

.field public static final ESCAN_PIXELFORMAT_RGB:I = 0x0

.field public static final ESCAN_RESOLUTION_DEFAULT:I = 0x96

.field public static final ESCAN_SCANNER_ERROR_ADF_PAPEROUT:I = 0x64

.field public static final ESCAN_SCANNER_ERROR_BUSY:I = 0x6

.field public static final ESCAN_SCANNER_ERROR_COMM:I = 0x5

.field public static final ESCAN_SCANNER_ERROR_COVEROPEN:I = 0x2

.field public static final ESCAN_SCANNER_ERROR_GENERAL:I = 0x1

.field public static final ESCAN_SCANNER_ERROR_NOERROR:I = 0x0

.field public static final ESCAN_SCANNER_ERROR_PAPERJAM:I = 0x3

.field public static final ESCAN_SCANNER_ERROR_PAPEROUT:I = 0x4

.field public static final ESCAN_SCANNER_ERROR_SDCARD_NOT_MOUNTED:I = 0x65

.field public static final ESCAN_SCANNER_JOB_CANCELED:I = 0x28

.field public static final ESCAN_SCANNER_STATUS_BUSY:I = 0x3

.field public static final ESCAN_SCANNER_STATUS_CANCELING:I = 0x4

.field public static final ESCAN_SCANNER_STATUS_ERROR:I = 0x5

.field public static final ESCAN_SCANNER_STATUS_IDLE:I = 0x0

.field public static final ESCAN_SCANNER_STATUS_SCANNING:I = 0x1

.field public static final ESCAN_SCANNER_STATUS_WARMUP:I = 0x2

.field public static final ESCAN_SOURCE_ADF:I = 0x1

.field public static final ESCAN_SOURCE_DOCTABLE:I = 0x0

.field public static final ESCAN_SPEED_HIGH:I = 0x1

.field public static final ESCAN_SPEED_NORMAL:I = 0x0

.field public static final ESCAN_STATUS_ARRAY_ELEMENT:I = 0x3

.field public static final ESCAN_SUPPORTED_OPTION_ARRAY_ELEMENT:I = 0x7

.field public static final ESP_ERR_COMM_ERROR:I = -0x44c

.field public static final FEED_DIRECTION:Ljava/lang/String; = "FEED_DIRECTION"

.field public static final FEED_DIRECTION_CAMERACOPY:Ljava/lang/String; = "FEED_DIRECTION_CAMERACOPY"

.field public static final FEED_DIRECTION_INFO:Ljava/lang/String; = "FEED_DIRECTION_INFO"

.field public static final FEED_DIRECTION_PHOTO:Ljava/lang/String; = "FEED_DIRECTION_PHOTO"

.field public static final GOOGLE_1:Ljava/lang/String; = "Google/1.0"

.field public static final JPEG_COMPRESS_QUALITY:I = 0x64

.field public static final JPEG_TYPE:Ljava/lang/String; = "JPEG"

.field public static final LANG:Ljava/lang/String; = "LANG"

.field public static final LANG_NETHERLAND:Ljava/lang/String; = "nl"

.field public static final LAYOUT:Ljava/lang/String; = "LAYOUT"

.field public static final LAYOUT_CAMERACOPY:Ljava/lang/String; = "LAYOUT_CAMERACOPY"

.field public static final LAYOUT_INFO:Ljava/lang/String; = "LAYOUT_INFO"

.field public static final LAYOUT_MULTIPAGE:Ljava/lang/String; = "LAYOUT_MULTIPAGE"

.field public static final LAYOUT_MULTIPAGE_PHOTO:Ljava/lang/String; = "LAYOUT_MULTIPAGE_PHOTO"

.field public static final LAYOUT_PHOTO:Ljava/lang/String; = "LAYOUT_PHOTO"

.field public static final LOGGER_DISCARDED:Ljava/lang/String; = "LOGGER_DISCARDED"

.field public static final LOGGER_LASTEXECTIME_CHECKCOLLECTION:Ljava/lang/String; = "LOGGER_LASTEXECTIME_CHECKCOLLECTION"

.field public static final LOGGER_LASTEXECTIME_SENDLOG:Ljava/lang/String; = "LOGGER_LASTEXECTIME_SENDLOG"

.field public static final LOGGER_UUDI:Ljava/lang/String; = "LOGGER_UUDI"

.field public static final NO_SELECTED:I = 0x0

.field public static final OPTION_FALSE:Z = false

.field public static final OPTION_NO:I = 0x0

.field public static final OPTION_TRUE:Z = true

.field public static final OPTION_YES:I = 0x1

.field public static final PAPER_SIZE:Ljava/lang/String; = "PAPER_SIZE"

.field public static final PAPER_SIZE_CAMERACOPY:Ljava/lang/String; = "PAPER_SIZE_CAMERACOPY"

.field public static final PAPER_SIZE_INFO:Ljava/lang/String; = "PAPER_SIZE_INFO"

.field public static final PAPER_SIZE_PHOTO:Ljava/lang/String; = "PAPER_SIZE_PHOTO"

.field public static final PAPER_SOURCE:Ljava/lang/String; = "PAPER_SOURCE"

.field public static final PAPER_SOURCE_CAMERACOPY:Ljava/lang/String; = "PAPER_SOURCE_CAMERACOPY"

.field public static final PAPER_SOURCE_INFO:Ljava/lang/String; = "PAPER_SOURCE_INFO"

.field public static final PAPER_SOURCE_PHOTO:Ljava/lang/String; = "PAPER_SOURCE_PHOTO"

.field public static final PAPER_TYPE:Ljava/lang/String; = "PAPER_TYPE"

.field public static final PAPER_TYPE_CAMERACOPY:Ljava/lang/String; = "PAPER_TYPE_CAMERACOPY"

.field public static final PAPER_TYPE_INFO:Ljava/lang/String; = "PAPER_TYPE_INFO"

.field public static final PAPER_TYPE_PHOTO:Ljava/lang/String; = "PAPER_TYPE_PHOTO"

.field public static final PDF_TYPE:Ljava/lang/String; = "PDF"

.field public static final PHOTO_SELECT_MODE:Ljava/lang/String; = "PHOTO_SELECT_MODE"

.field public static final PREFS_EPSON_CONNECT:Ljava/lang/String; = "PREFS_EPSON_CONNECT"

.field public static final PREFS_INFO_PRINT_SAVE:Ljava/lang/String; = "PrintSettingSaved"

.field public static final PREFS_INFO_SIZETYPESET:Ljava/lang/String; = "PrintSizeTypeSet"

.field public static final PREFS_INFO_SIZETYPESET_CAMERACOPY:Ljava/lang/String; = "PrintSizeTypeSet_CAMERACOPY"

.field public static final PREFS_INFO_SIZETYPESET_PHOTO:Ljava/lang/String; = "PrintSizeTypeSet_PHOTO"

.field public static final PREFS_LOGGER:Ljava/lang/String; = "PREFS_LOGGER"

.field public static final PREFS_PHOTO:Ljava/lang/String; = "PREFS_PHOTO"

.field public static final PREFS_WIFI:Ljava/lang/String; = "PREFS_WIFI"

.field public static final PREFS_WIFI_AUTOCONNECT_SSID:Ljava/lang/String; = "AUTOCONNECT_SSID."

.field public static final PRINTDATE:Ljava/lang/String; = "PRINTDATE"

.field public static final PRINTDATE_INFO:Ljava/lang/String; = "PRINTDATE_INFO"

.field public static final PRINTER_CLIENT_ID:Ljava/lang/String; = "PRINTER_CLIENT_ID"

.field public static final PRINTER_EMAIL_ADDRESS:Ljava/lang/String; = "PRINTER_EMAIL_ADDRESS"

.field public static final PRINTER_ID:Ljava/lang/String; = "PRINTER_ID"

.field public static final PRINTER_IP:Ljava/lang/String; = "PRINTER_IP"

.field public static final PRINTER_KEY:Ljava/lang/String; = "PRINTER_KEY"

.field public static final PRINTER_LOCATION:Ljava/lang/String; = "PRINTER_LOCATION"

.field public static final PRINTER_NAME:Ljava/lang/String; = "PRINTER_NAME"

.field public static final PRINTER_POS:Ljava/lang/String; = "printer position"

.field public static final PRINTER_SERIAL_NO:Ljava/lang/String; = "PRINTER_SERIAL_NO"

.field public static final PRINT_ALL:Ljava/lang/String; = "PRINT_ALL"

.field public static final PRINT_QUALITY_INFO:Ljava/lang/String; = "PRINT_QUALITY_INFO"

.field public static final QUALITY:Ljava/lang/String; = "QUALITY"

.field public static final QUALITY_CAMERACOPY:Ljava/lang/String; = "QUALITY_CAMERACOPY"

.field public static final QUALITY_PHOTO:Ljava/lang/String; = "QUALITY_PHOTO"

.field public static final REGIST_IP_PRINTER_MAX_SIZE:I = 0x20

.field public static final REGIST_REMOTE_PRINTER_MAX_SIZE:I = 0x20

.field public static final SATURATION:Ljava/lang/String; = "SATURATION"

.field public static final SCAN_ADF_DUPLEX_NO:I = 0x0

.field public static final SCAN_ADF_DUPLEX_YES:I = 0x1

.field public static final SCAN_ADF_ROTATE_NO:I = 0x0

.field public static final SCAN_ADF_ROTATE_YES:I = 0x1

.field public static final SCAN_FIND_TIMEOUT:I = 0x3c

.field public static final SCAN_MAX_HEIGHT:I = 0x6db

.field public static final SCAN_MAX_WIDTH:I = 0x4fb

.field public static final SCAN_NATIVE_OBJECT:Ljava/lang/String; = "SCAN_NATIVE_OBJECT"

.field public static final SCAN_PROBE_BY_ID:I = 0x1

.field public static final SCAN_PROBE_BY_IP_ADDR:I = 0x2

.field public static final SCAN_REFS_MAX_HEIGHT:Ljava/lang/String; = "SCAN_REFS_MAX_HEIGHT"

.field public static final SCAN_REFS_MAX_WIDTH:Ljava/lang/String; = "SCAN_REFS_MAX_WIDTH"

.field public static final SCAN_REFS_OPTIONS_ADF_HEIGHT:Ljava/lang/String; = "SCAN_REFS_OPTIONS_ADF_HEIGHT"

.field public static final SCAN_REFS_OPTIONS_ADF_WIDTH:Ljava/lang/String; = "SCAN_REFS_OPTIONS_ADF_WIDTH"

.field public static final SCAN_REFS_OPTIONS_BASIC_RESOLUTION:Ljava/lang/String; = "SCAN_REFS_OPTIONS_BASIC_RESOLUTION"

.field public static final SCAN_REFS_OPTIONS_HEIGHT:Ljava/lang/String; = "SCAN_REFS_OPTIONS_HEIGHT"

.field public static final SCAN_REFS_OPTIONS_RESOLUTION_:Ljava/lang/String; = "SCAN_REFS_OPTIONS_RESOLUTION_"

.field public static final SCAN_REFS_OPTIONS_SUPPORTED_ADF_DUPLEX:Ljava/lang/String; = "SCAN_REFS_OPTIONS_SUPPORTED_ADF_DUPLEX"

.field public static final SCAN_REFS_OPTIONS_VALIDATION:Ljava/lang/String; = "SCAN_REFS_OPTIONS_VALIDATION"

.field public static final SCAN_REFS_OPTIONS_VERSION:Ljava/lang/String; = "SCAN_REFS_OPTIONS_VERSION"

.field public static final SCAN_REFS_OPTIONS_WIDTH:Ljava/lang/String; = "SCAN_REFS_OPTIONS_WIDTH"

.field public static final SCAN_REFS_SCANNER_CHOSEN_SIZE:Ljava/lang/String; = "SCAN_REFS_SCANNER_CHOSEN_SIZE"

.field public static final SCAN_REFS_SCANNER_CHOSEN_SIZE_ADF:Ljava/lang/String; = "SCAN_REFS_SCANNER_CHOSEN_SIZE_ADF"

.field public static final SCAN_REFS_SCANNER_CHOSEN_SIZE_DOC:Ljava/lang/String; = "SCAN_REFS_SCANNER_CHOSEN_SIZE_DOC"

.field public static final SCAN_REFS_SCANNER_FAMILYNAME:Ljava/lang/String; = "SCAN_REFS_SCANNER_FAMILYNAME"

.field public static final SCAN_REFS_SCANNER_ID:Ljava/lang/String; = "SCAN_REFS_SCANNER_ID"

.field public static final SCAN_REFS_SCANNER_INITIALED:Ljava/lang/String; = "SCAN_REFS_SCANNER_INITIALED"

.field public static final SCAN_REFS_SCANNER_IP:Ljava/lang/String; = "SCAN_REFS_SCANNER_LOCATION"

.field public static final SCAN_REFS_SCANNER_LOCATION:Ljava/lang/String; = "SCAN_REFS_SCANNER_ACCESSPATH"

.field public static final SCAN_REFS_SCANNER_MODEL:Ljava/lang/String; = "SCAN_REFS_SCANNER_MODEL"

.field public static final SCAN_REFS_SCANNER_POSITION:Ljava/lang/String; = "SCAN_REFS_SCANNER_POSITION"

.field public static final SCAN_REFS_SCANNER_SIMPLEAP:Ljava/lang/String; = "SCAN_REFS_SCANNER_SIMPLEAP"

.field public static final SCAN_REFS_SETTINGS_2SIDED:Ljava/lang/String; = "SCAN_REFS_SETTINGS_2SIDED"

.field public static final SCAN_REFS_SETTINGS_2SIDED_NAME:Ljava/lang/String; = "SCAN_REFS_SETTINGS_2SIDED_NAME"

.field public static final SCAN_REFS_SETTINGS_ADF_PAPER_GUIDE:Ljava/lang/String; = "SCAN_REFS_SETTINGS_ADF_PAPER_GUIDE"

.field public static final SCAN_REFS_SETTINGS_COLOR:Ljava/lang/String; = "SCAN_REFS_SETTINGS_COLOR"

.field public static final SCAN_REFS_SETTINGS_COLOR_NAME:Ljava/lang/String; = "SCAN_REFS_SETTINGS_COLOR_NAME"

.field public static final SCAN_REFS_SETTINGS_DENSITY:Ljava/lang/String; = "SCAN_REFS_SETTINGS_DENSITY_NAME"

.field public static final SCAN_REFS_SETTINGS_DENSITY_STATUS:Ljava/lang/String; = "SCAN_REFS_SETTINGS_DENSITY_STATUS"

.field public static final SCAN_REFS_SETTINGS_GAMMA:Ljava/lang/String; = "SCAN_REFS_SETTINGS_GAMMA"

.field public static final SCAN_REFS_SETTINGS_GAMMA_NAME:Ljava/lang/String; = "SCAN_REFS_SETTINGS_GAMMA_NAME"

.field public static final SCAN_REFS_SETTINGS_RESEACH:Ljava/lang/String; = "SCAN_REFS_SETTINGS_RESEACH"

.field public static final SCAN_REFS_SETTINGS_RESOLUTION:Ljava/lang/String; = "SCAN_REFS_SETTINGS_RESOLUTION"

.field public static final SCAN_REFS_SETTINGS_RESOLUTION_NAME:Ljava/lang/String; = "SCAN_REFS_SETTINGS_RESOLUTION_NAME"

.field public static final SCAN_REFS_SETTINGS_ROTATE:Ljava/lang/String; = "SCAN_REFS_SETTINGS_ROTATE"

.field public static final SCAN_REFS_SETTINGS_SOURCE:Ljava/lang/String; = "SCAN_REFS_SETTINGS_SOURCE"

.field public static final SCAN_REFS_SETTINGS_SOURCE_NAME:Ljava/lang/String; = "SCAN_REFS_SETTINGS_SOURCE_NAME"

.field public static final SCAN_REFS_SUPPORTED_OPTIONS:Ljava/lang/String; = "epson.scanner.supported.Options"

.field public static final SCAN_REFS_USED_SCANNER_PATH:Ljava/lang/String; = "epson.scanner.SelectedScanner"

.field public static final SCAN_REFS_USED_SCANNER_SETTINGS_PATH:Ljava/lang/String; = "epson.scanner.ScannerSettings"

.field public static final SCAN_SAVED_FILE_FORMAT_BMP:Ljava/lang/String; = ".bmp"

.field public static final SCAN_SAVED_FILE_FORMAT_JPEG:Ljava/lang/String; = ".jpg"

.field public static final SCAN_SETTINGS_DETAIL_TITLE:Ljava/lang/String; = "SCAN_SETTINGS_DETAIL_TITLE"

.field public static final SEARCH_FINISH:I = 0x2

.field public static final SELECTED:I = 0x1

.field public static final SHARPNESS_PHOTO:Ljava/lang/String; = "SHARPNESS_PHOTO"

.field public static final SOURCE_TYPE:Ljava/lang/String; = "SOURCE_TYPE"

.field public static final SSID:Ljava/lang/String; = "SSID: "

.field public static final START_PAGE:Ljava/lang/String; = "START_PAGE"

.field public static final STRING_EMPTY:Ljava/lang/String; = ""

.field public static final UPDATE_FOUND_PRINTER:I = 0x1

.field public static final USE_MEDIASTORE_THUMBNAIL:Ljava/lang/String; = "USE_MEDIASTORE_THUMBNAIL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

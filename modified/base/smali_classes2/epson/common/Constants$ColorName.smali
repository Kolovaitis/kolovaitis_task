.class public final enum Lepson/common/Constants$ColorName;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ColorName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/common/Constants$ColorName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_BLACK:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_BLACK1:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_BLACK2:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_CLEAN:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_CLEAR:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_COMPOSITE:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_CYAN:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_DARKYELLOW:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_GRAY:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_GREEN:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTBLACK:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTCYAN:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTLIGHTBLACK:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTMAGENTA:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTYELLOW:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_MAGENTA:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_MATTEBLACK:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_ORANGE:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_PHOTOBLACK:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_RED:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_UNKNOWN:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_VIOLET:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_WHITE:Lepson/common/Constants$ColorName;

.field public static final enum EPS_COLOR_YELLOW:Lepson/common/Constants$ColorName;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 579
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_BLACK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_BLACK:Lepson/common/Constants$ColorName;

    .line 580
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_CYAN"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_CYAN:Lepson/common/Constants$ColorName;

    .line 581
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_MAGENTA"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_MAGENTA:Lepson/common/Constants$ColorName;

    .line 582
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_YELLOW"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_YELLOW:Lepson/common/Constants$ColorName;

    .line 583
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTCYAN"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTCYAN:Lepson/common/Constants$ColorName;

    .line 584
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTMAGENTA"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTMAGENTA:Lepson/common/Constants$ColorName;

    .line 585
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTYELLOW"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTYELLOW:Lepson/common/Constants$ColorName;

    .line 586
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_DARKYELLOW"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_DARKYELLOW:Lepson/common/Constants$ColorName;

    .line 587
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTBLACK"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTBLACK:Lepson/common/Constants$ColorName;

    .line 588
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_RED"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_RED:Lepson/common/Constants$ColorName;

    .line 589
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_VIOLET"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_VIOLET:Lepson/common/Constants$ColorName;

    .line 590
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_MATTEBLACK"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_MATTEBLACK:Lepson/common/Constants$ColorName;

    .line 591
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTLIGHTBLACK"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTLIGHTBLACK:Lepson/common/Constants$ColorName;

    .line 592
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_PHOTOBLACK"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_PHOTOBLACK:Lepson/common/Constants$ColorName;

    .line 593
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_CLEAR"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_CLEAR:Lepson/common/Constants$ColorName;

    .line 594
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_GRAY"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_GRAY:Lepson/common/Constants$ColorName;

    .line 595
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_UNKNOWN"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_UNKNOWN:Lepson/common/Constants$ColorName;

    .line 598
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_BLACK2"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_BLACK2:Lepson/common/Constants$ColorName;

    .line 599
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_ORANGE"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_ORANGE:Lepson/common/Constants$ColorName;

    .line 600
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_GREEN"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_GREEN:Lepson/common/Constants$ColorName;

    .line 601
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_WHITE"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_WHITE:Lepson/common/Constants$ColorName;

    .line 602
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_CLEAN"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_CLEAN:Lepson/common/Constants$ColorName;

    .line 605
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_COMPOSITE"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_COMPOSITE:Lepson/common/Constants$ColorName;

    .line 608
    new-instance v0, Lepson/common/Constants$ColorName;

    const-string v1, "EPS_COLOR_BLACK1"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lepson/common/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/common/Constants$ColorName;->EPS_COLOR_BLACK1:Lepson/common/Constants$ColorName;

    const/16 v0, 0x18

    .line 578
    new-array v0, v0, [Lepson/common/Constants$ColorName;

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_BLACK:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_CYAN:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v3

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_MAGENTA:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v4

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_YELLOW:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v5

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTCYAN:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v6

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTMAGENTA:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v7

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTYELLOW:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v8

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_DARKYELLOW:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v9

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTBLACK:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v10

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_RED:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v11

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_VIOLET:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v12

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_MATTEBLACK:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v13

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_LIGHTLIGHTBLACK:Lepson/common/Constants$ColorName;

    aput-object v1, v0, v14

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_PHOTOBLACK:Lepson/common/Constants$ColorName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_CLEAR:Lepson/common/Constants$ColorName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_GRAY:Lepson/common/Constants$ColorName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_UNKNOWN:Lepson/common/Constants$ColorName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_BLACK2:Lepson/common/Constants$ColorName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_ORANGE:Lepson/common/Constants$ColorName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_GREEN:Lepson/common/Constants$ColorName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_WHITE:Lepson/common/Constants$ColorName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_CLEAN:Lepson/common/Constants$ColorName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_COMPOSITE:Lepson/common/Constants$ColorName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lepson/common/Constants$ColorName;->EPS_COLOR_BLACK1:Lepson/common/Constants$ColorName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sput-object v0, Lepson/common/Constants$ColorName;->$VALUES:[Lepson/common/Constants$ColorName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 578
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/common/Constants$ColorName;
    .locals 1

    .line 578
    const-class v0, Lepson/common/Constants$ColorName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/common/Constants$ColorName;

    return-object p0
.end method

.method public static values()[Lepson/common/Constants$ColorName;
    .locals 1

    .line 578
    sget-object v0, Lepson/common/Constants$ColorName;->$VALUES:[Lepson/common/Constants$ColorName;

    invoke-virtual {v0}, [Lepson/common/Constants$ColorName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/common/Constants$ColorName;

    return-object v0
.end method

.class public Lepson/common/OsAssistant;
.super Ljava/lang/Object;
.source "OsAssistant.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lockScreenRotation(Landroid/app/Activity;)V
    .locals 7
    .param p0    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 30
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 31
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 32
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    const/16 v2, 0x8

    const/16 v3, 0x9

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    packed-switch v0, :pswitch_data_0

    return-void

    .line 58
    :pswitch_0
    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_0

    .line 59
    invoke-virtual {p0, v6}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {p0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :goto_0
    return-void

    .line 50
    :pswitch_1
    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_1

    .line 51
    invoke-virtual {p0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 53
    :cond_1
    invoke-virtual {p0, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :goto_1
    return-void

    .line 42
    :pswitch_2
    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_2

    .line 43
    invoke-virtual {p0, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_2

    .line 45
    :cond_2
    invoke-virtual {p0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :goto_2
    return-void

    .line 34
    :pswitch_3
    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_3

    .line 35
    invoke-virtual {p0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_3

    .line 37
    :cond_3
    invoke-virtual {p0, v6}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

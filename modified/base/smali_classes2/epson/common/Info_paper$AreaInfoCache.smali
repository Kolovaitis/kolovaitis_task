.class Lepson/common/Info_paper$AreaInfoCache;
.super Ljava/lang/Object;
.source "Info_paper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/Info_paper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AreaInfoCache"
.end annotation


# static fields
.field static final SIZE_OF_EPS_INT32:I = 0x4

.field private static areaInfoCache:Ljava/util/Hashtable; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable<",
            "Ljava/lang/Integer;",
            "Lepson/common/Info_paper;",
            ">;"
        }
    .end annotation
.end field

.field private static areaInfoTimeStamp:J = -0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAreaInfo(Landroid/content/Context;I)Ljava/util/Hashtable;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/Hashtable<",
            "Ljava/lang/Integer;",
            "Lepson/common/Info_paper;",
            ">;"
        }
    .end annotation

    .line 264
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 266
    invoke-static/range {p0 .. p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 270
    sget-object v2, Lepson/common/Info_paper$AreaInfoCache;->areaInfoCache:Ljava/util/Hashtable;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    sget-wide v4, Lepson/common/Info_paper$AreaInfoCache;->areaInfoTimeStamp:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    const-string v0, "Info_paper"

    const-string v1, "Use cache areaInfo"

    .line 271
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    sget-object v0, Lepson/common/Info_paper$AreaInfoCache;->areaInfoCache:Ljava/util/Hashtable;

    return-object v0

    :cond_0
    const-string v2, "Info_paper"

    const-string v3, "Read areaInfo file"

    .line 275
    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x4

    .line 278
    :try_start_0
    new-array v4, v3, [B

    .line 282
    new-instance v5, Ljava/io/BufferedInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v6, 0x0

    .line 285
    :try_start_1
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    if-ne v3, v7, :cond_c

    .line 288
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v7

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v7, :cond_b

    .line 293
    new-instance v9, Lepson/common/Info_paper;

    invoke-direct {v9}, Lepson/common/Info_paper;-><init>()V

    .line 296
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v10

    if-ne v3, v10, :cond_a

    .line 299
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v10

    .line 302
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v11

    if-ne v3, v11, :cond_9

    .line 305
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v11

    invoke-virtual {v9, v11}, Lepson/common/Info_paper;->setPaper_width(I)V

    .line 308
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v11

    if-ne v3, v11, :cond_8

    .line 311
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v11

    invoke-virtual {v9, v11}, Lepson/common/Info_paper;->setPaper_height(I)V

    .line 314
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v11

    if-ne v3, v11, :cond_7

    .line 317
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v11

    const/4 v12, 0x0

    :goto_1
    if-ge v12, v11, :cond_6

    .line 327
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v13

    if-ne v3, v13, :cond_5

    .line 330
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v13

    .line 333
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v14

    if-ne v3, v14, :cond_4

    .line 336
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v14

    .line 339
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v15

    if-ne v3, v15, :cond_3

    .line 342
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v15

    .line 345
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v2

    if-ne v3, v2, :cond_2

    .line 348
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v2

    move/from16 v16, v7

    .line 351
    invoke-virtual {v5, v4, v6, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    if-ne v3, v7, :cond_1

    .line 354
    invoke-static {v4}, Lepson/common/Info_paper$AreaInfoCache;->toInt([B)I

    move-result v7

    packed-switch v13, :pswitch_data_0

    goto :goto_2

    .line 365
    :pswitch_0
    invoke-virtual {v9, v14}, Lepson/common/Info_paper;->setTopMargin_border(I)V

    .line 366
    invoke-virtual {v9, v15}, Lepson/common/Info_paper;->setLeftMargin_border(I)V

    neg-int v13, v14

    .line 367
    invoke-virtual {v9}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v14

    add-int/2addr v13, v14

    sub-int/2addr v13, v2

    invoke-virtual {v9, v13}, Lepson/common/Info_paper;->setPaper_height_boder(I)V

    neg-int v2, v15

    .line 368
    invoke-virtual {v9}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v13

    add-int/2addr v2, v13

    sub-int/2addr v2, v7

    invoke-virtual {v9, v2}, Lepson/common/Info_paper;->setPaper_width_boder(I)V

    goto :goto_2

    .line 358
    :pswitch_1
    invoke-virtual {v9, v14}, Lepson/common/Info_paper;->setTopMargin(I)V

    .line 359
    invoke-virtual {v9, v15}, Lepson/common/Info_paper;->setLeftMargin(I)V

    neg-int v13, v14

    .line 360
    invoke-virtual {v9}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v14

    add-int/2addr v13, v14

    sub-int/2addr v13, v2

    invoke-virtual {v9, v13}, Lepson/common/Info_paper;->setPaper_height_boderless(I)V

    neg-int v2, v15

    .line 361
    invoke-virtual {v9}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v13

    add-int/2addr v2, v13

    sub-int/2addr v2, v7

    invoke-virtual {v9, v2}, Lepson/common/Info_paper;->setPaper_width_boderless(I)V

    :goto_2
    add-int/lit8 v12, v12, 0x1

    move/from16 v7, v16

    goto :goto_1

    .line 352
    :cond_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 346
    :cond_2
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 340
    :cond_3
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 334
    :cond_4
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 328
    :cond_5
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :cond_6
    move/from16 v2, p1

    move/from16 v16, v7

    .line 374
    invoke-virtual {v9, v2}, Lepson/common/Info_paper;->setResolution(I)V

    .line 377
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7, v9}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v8, v8, 0x1

    move/from16 v7, v16

    goto/16 :goto_0

    .line 315
    :cond_7
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 309
    :cond_8
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 303
    :cond_9
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 297
    :cond_a
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 382
    :cond_b
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    sput-wide v1, Lepson/common/Info_paper$AreaInfoCache;->areaInfoTimeStamp:J

    .line 383
    sput-object v0, Lepson/common/Info_paper$AreaInfoCache;->areaInfoCache:Ljava/util/Hashtable;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396
    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return-object v0

    .line 286
    :cond_c
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v2, v5

    goto :goto_3

    :catch_2
    nop

    goto :goto_5

    :catchall_1
    move-exception v0

    const/4 v5, 0x0

    goto :goto_4

    :catch_3
    move-exception v0

    const/4 v2, 0x0

    .line 391
    :goto_3
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v2, :cond_e

    .line 396
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v5, v2

    :goto_4
    if-eqz v5, :cond_d

    :try_start_6
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 399
    :catch_4
    :cond_d
    throw v0

    :catch_5
    const/4 v5, 0x0

    :goto_5
    if-eqz v5, :cond_e

    .line 396
    :try_start_7
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    :catch_6
    :goto_6
    const/4 v1, 0x0

    goto :goto_7

    :cond_e
    const/4 v1, 0x0

    :goto_7
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static toInt([B)I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 415
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    mul-int/lit8 v3, v0, 0x8

    shl-int/2addr v2, v3

    or-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

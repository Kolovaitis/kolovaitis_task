.class Lepson/common/ScalableImageView$2;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ScalableImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/ScalableImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/common/ScalableImageView;


# direct methods
.method constructor <init>(Lepson/common/ScalableImageView;)V
    .locals 0

    .line 212
    iput-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6

    const-string v0, "ScalableImageView"

    const-string v1, "DOBULE TAP"

    .line 218
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v0}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v0}, Lepson/common/ScalableImageView;->access$000(Lepson/common/ScalableImageView;)F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 223
    iget-object v1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$300(Lepson/common/ScalableImageView;)F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v2}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v2}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 226
    iget-object v2, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-static {v2, v3}, Lepson/common/ScalableImageView;->access$102(Lepson/common/ScalableImageView;F)F

    .line 229
    iget-object v2, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v2}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v3}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v3

    mul-float v2, v2, v3

    .line 230
    iget-object v3, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v3}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v4}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v4

    mul-float v3, v3, v4

    .line 233
    iget-object v4, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    mul-float v2, v2, v0

    sub-float/2addr v5, v2

    invoke-static {v4, v5}, Lepson/common/ScalableImageView;->access$002(Lepson/common/ScalableImageView;F)F

    .line 234
    iget-object v0, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    mul-float v3, v3, v1

    sub-float/2addr p1, v3

    invoke-static {v0, p1}, Lepson/common/ScalableImageView;->access$302(Lepson/common/ScalableImageView;F)F

    goto :goto_0

    .line 238
    :cond_0
    iget-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1, v1}, Lepson/common/ScalableImageView;->access$102(Lepson/common/ScalableImageView;F)F

    .line 239
    iget-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lepson/common/ScalableImageView;->access$002(Lepson/common/ScalableImageView;F)F

    .line 240
    iget-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1, v0}, Lepson/common/ScalableImageView;->access$302(Lepson/common/ScalableImageView;F)F

    .line 243
    :goto_0
    iget-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-virtual {p1}, Lepson/common/ScalableImageView;->invalidate()V

    const/4 p1, 0x1

    return p1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    neg-float p1, p3

    neg-float p2, p4

    const/4 p3, 0x0

    cmpl-float p4, p1, p3

    if-lez p4, :cond_0

    .line 258
    iget-object p4, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p4}, Lepson/common/ScalableImageView;->access$000(Lepson/common/ScalableImageView;)F

    move-result v0

    add-float/2addr v0, p1

    invoke-static {v0, p3}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {p4, p1}, Lepson/common/ScalableImageView;->access$002(Lepson/common/ScalableImageView;F)F

    goto :goto_0

    .line 260
    :cond_0
    iget-object p4, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p4}, Lepson/common/ScalableImageView;->access$000(Lepson/common/ScalableImageView;)F

    move-result v0

    add-float/2addr v0, p1

    iget-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    int-to-float p1, p1

    iget-object v1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v1}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v2}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v2

    mul-float v1, v1, v2

    sub-float/2addr p1, v1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-static {p4, p1}, Lepson/common/ScalableImageView;->access$002(Lepson/common/ScalableImageView;F)F

    :goto_0
    cmpl-float p1, p2, p3

    if-lez p1, :cond_1

    .line 264
    iget-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$300(Lepson/common/ScalableImageView;)F

    move-result p4

    add-float/2addr p4, p2

    invoke-static {p4, p3}, Ljava/lang/Math;->min(FF)F

    move-result p2

    invoke-static {p1, p2}, Lepson/common/ScalableImageView;->access$302(Lepson/common/ScalableImageView;F)F

    goto :goto_1

    .line 266
    :cond_1
    iget-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p1}, Lepson/common/ScalableImageView;->access$300(Lepson/common/ScalableImageView;)F

    move-result p3

    add-float/2addr p3, p2

    iget-object p2, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p2}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p2

    int-to-float p2, p2

    iget-object p4, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p4}, Lepson/common/ScalableImageView;->access$200(Lepson/common/ScalableImageView;)Landroid/graphics/Rect;

    move-result-object p4

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result p4

    int-to-float p4, p4

    iget-object v0, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {v0}, Lepson/common/ScalableImageView;->access$100(Lepson/common/ScalableImageView;)F

    move-result v0

    mul-float p4, p4, v0

    sub-float/2addr p2, p4

    invoke-static {p3, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    invoke-static {p1, p2}, Lepson/common/ScalableImageView;->access$302(Lepson/common/ScalableImageView;F)F

    :goto_1
    const-string p1, "ScalableImageView"

    .line 269
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "mOffsetX = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p3}, Lepson/common/ScalableImageView;->access$000(Lepson/common/ScalableImageView;)F

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p3, " mOffsetY = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-static {p3}, Lepson/common/ScalableImageView;->access$300(Lepson/common/ScalableImageView;)F

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object p1, p0, Lepson/common/ScalableImageView$2;->this$0:Lepson/common/ScalableImageView;

    invoke-virtual {p1}, Lepson/common/ScalableImageView;->invalidate()V

    const/4 p1, 0x1

    return p1
.end method

.class public Lepson/common/httpclient/IAHttpClient$HttpGet;
.super Lepson/common/httpclient/IAHttpClient$HttpBase;
.source "IAHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/httpclient/IAHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpGet"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 435
    invoke-direct {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getChunked()Ljava/lang/Boolean;
    .locals 1

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getChunked()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentEncoding()Ljava/lang/String;
    .locals 1

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentLength()Ljava/lang/Integer;
    .locals 1

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentLength()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentType()Ljava/lang/String;
    .locals 1

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getEntity()Ljava/io/ByteArrayOutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getEntityFileDataOffset()I
    .locals 1

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileDataOffset()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getEntityFileDataSize()I
    .locals 1

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileDataSize()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getEntityFileName()Ljava/lang/String;
    .locals 1

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getURI()Ljava/net/URL;
    .locals 1

    .line 432
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getURI()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setChunked(Ljava/lang/Boolean;)V
    .locals 0

    .line 432
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setChunked(Ljava/lang/Boolean;)V

    return-void
.end method

.method public bridge synthetic setContentEncoding(Ljava/lang/String;)V
    .locals 0

    .line 432
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentEncoding(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setContentLength(Ljava/lang/Integer;)V
    .locals 0

    .line 432
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentLength(Ljava/lang/Integer;)V

    return-void
.end method

.method public bridge synthetic setContentType(Ljava/lang/String;)V
    .locals 0

    .line 432
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentType(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setEntity([B)V
    .locals 0

    .line 432
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setEntity([B)V

    return-void
.end method

.method public bridge synthetic setEntityFile(Ljava/lang/String;II)V
    .locals 0

    .line 432
    invoke-super {p0, p1, p2, p3}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setEntityFile(Ljava/lang/String;II)V

    return-void
.end method

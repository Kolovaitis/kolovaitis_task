.class public Lepson/common/httpclient/IAHttpClient;
.super Ljava/lang/Object;
.source "IAHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/common/httpclient/IAHttpClient$HttpStatus;,
        Lepson/common/httpclient/IAHttpClient$HttpBase;,
        Lepson/common/httpclient/IAHttpClient$HttpResponse;,
        Lepson/common/httpclient/IAHttpClient$HttpPost;,
        Lepson/common/httpclient/IAHttpClient$HttpGet;
    }
.end annotation


# static fields
.field private static final CONTENT_ENCODING:Ljava/lang/String; = "Content-Encoding"

.field private static final CONTENT_LENGTH:Ljava/lang/String; = "Content-Length"

.field private static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field private static final REQUEST_GET:Ljava/lang/String; = "GET"

.field private static final REQUEST_POST:Ljava/lang/String; = "POST"

.field private static final TAG:Ljava/lang/String; = "IAHttpClient"

.field private static final TRANSFER_ENCODING:Ljava/lang/String; = "Transfer-Encoding"


# instance fields
.field private final BUFFER_SIZE:I

.field conn:Ljava/net/HttpURLConnection;

.field private connLockObj:Ljava/lang/Object;

.field private connectionTimeout:I

.field private followRedirects:Z

.field private readTimeout:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x400

    .line 31
    iput v0, p0, Lepson/common/httpclient/IAHttpClient;->BUFFER_SIZE:I

    const/16 v0, 0x7530

    .line 41
    iput v0, p0, Lepson/common/httpclient/IAHttpClient;->connectionTimeout:I

    .line 42
    iput v0, p0, Lepson/common/httpclient/IAHttpClient;->readTimeout:I

    const/4 v0, 0x1

    .line 44
    iput-boolean v0, p0, Lepson/common/httpclient/IAHttpClient;->followRedirects:Z

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient;->connLockObj:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 61
    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    return-void
.end method


# virtual methods
.method connect(Lepson/common/httpclient/IAHttpClient$HttpBase;)Ljava/net/HttpURLConnection;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->connLockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 71
    :try_start_0
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getURI()Ljava/net/URL;

    move-result-object p1

    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    check-cast p1, Ljava/net/HttpURLConnection;

    iput-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    .line 72
    iget-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    iget v1, p0, Lepson/common/httpclient/IAHttpClient;->connectionTimeout:I

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 73
    iget-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    iget v1, p0, Lepson/common/httpclient/IAHttpClient;->readTimeout:I

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 74
    iget-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    iget-boolean v1, p0, Lepson/common/httpclient/IAHttpClient;->followRedirects:Z

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 75
    iget-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 76
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public disconnect()V
    .locals 2

    .line 83
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->connLockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 84
    :try_start_0
    iget-object v1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 87
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public declared-synchronized execute(Lepson/common/httpclient/IAHttpClient$HttpGet;)Lepson/common/httpclient/IAHttpClient$HttpResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    .line 98
    :try_start_0
    new-instance v0, Lepson/common/httpclient/IAHttpClient$HttpResponse;

    invoke-direct {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 102
    :try_start_1
    invoke-virtual {p0, p1}, Lepson/common/httpclient/IAHttpClient;->connect(Lepson/common/httpclient/IAHttpClient$HttpBase;)Ljava/net/HttpURLConnection;

    move-result-object v0

    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    .line 104
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 108
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, v0, p1}, Lepson/common/httpclient/IAHttpClient;->makeHeader(Ljava/net/HttpURLConnection;Lepson/common/httpclient/IAHttpClient$HttpBase;)V

    .line 111
    iget-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->connect()V

    .line 114
    iget-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1, v1}, Lepson/common/httpclient/IAHttpClient;->parseResponse(Ljava/net/HttpURLConnection;Z)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :try_start_2
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 123
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 117
    :try_start_3
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 120
    :goto_0
    :try_start_4
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized execute(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    .line 133
    :try_start_0
    new-instance v0, Lepson/common/httpclient/IAHttpClient$HttpResponse;

    invoke-direct {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 137
    :try_start_1
    invoke-virtual {p0, p1}, Lepson/common/httpclient/IAHttpClient;->connect(Lepson/common/httpclient/IAHttpClient$HttpBase;)Ljava/net/HttpURLConnection;

    move-result-object v0

    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    .line 139
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 141
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 144
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, v0, p1}, Lepson/common/httpclient/IAHttpClient;->makeHeader(Ljava/net/HttpURLConnection;Lepson/common/httpclient/IAHttpClient$HttpBase;)V

    .line 146
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v2, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 150
    :try_start_2
    iget-object v2, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 151
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 152
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 158
    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 166
    :catch_0
    :try_start_4
    iget-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1, v1}, Lepson/common/httpclient/IAHttpClient;->parseResponse(Ljava/net/HttpURLConnection;Z)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 172
    :try_start_5
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 175
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    .line 154
    :try_start_6
    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 158
    :goto_0
    :try_start_7
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 161
    :catch_2
    :try_start_8
    throw p1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception p1

    goto :goto_1

    :catch_3
    move-exception p1

    .line 169
    :try_start_9
    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 172
    :goto_1
    :try_start_a
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V

    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public executeFile(Lepson/common/httpclient/IAHttpClient$HttpGet;)Lepson/common/httpclient/IAHttpClient$HttpResponse;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    new-instance v0, Lepson/common/httpclient/IAHttpClient$HttpResponse;

    invoke-direct {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;-><init>()V

    .line 189
    :try_start_0
    invoke-virtual {p0, p1}, Lepson/common/httpclient/IAHttpClient;->connect(Lepson/common/httpclient/IAHttpClient$HttpBase;)Ljava/net/HttpURLConnection;

    move-result-object v0

    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    .line 191
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 195
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, v0, p1}, Lepson/common/httpclient/IAHttpClient;->makeHeader(Ljava/net/HttpURLConnection;Lepson/common/httpclient/IAHttpClient$HttpBase;)V

    .line 198
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 201
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lepson/common/httpclient/IAHttpClient;->parseResponse(Ljava/net/HttpURLConnection;Z)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object v0

    .line 203
    new-instance v2, Ljava/io/DataInputStream;

    iget-object v3, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 204
    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    .line 205
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpGet;->getEntityFileName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const/16 p1, 0x400

    invoke-direct {v3, v4, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 210
    :try_start_1
    new-array p1, p1, [B

    .line 211
    :goto_0
    invoke-virtual {v2, p1}, Ljava/io/DataInputStream;->read([B)I

    move-result v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 213
    :try_start_2
    invoke-virtual {v3, p1, v1, v4}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_3
    const-string v0, "IAHttpClient"

    .line 215
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error: in executeFile() : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    throw p1

    .line 219
    :cond_0
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 225
    :try_start_4
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 232
    :catch_1
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 243
    :catch_2
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V

    return-object v0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_3
    move-exception p1

    .line 221
    :try_start_6
    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 225
    :goto_1
    :try_start_7
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 232
    :catch_4
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 235
    :catch_5
    :try_start_9
    throw p1
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception p1

    goto :goto_2

    :catch_6
    move-exception p1

    .line 240
    :try_start_a
    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 243
    :goto_2
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V

    throw p1
.end method

.method public executeFile(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 257
    new-instance v0, Lepson/common/httpclient/IAHttpClient$HttpResponse;

    invoke-direct {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;-><init>()V

    .line 261
    :try_start_0
    invoke-virtual {p0, p1}, Lepson/common/httpclient/IAHttpClient;->connect(Lepson/common/httpclient/IAHttpClient$HttpBase;)Ljava/net/HttpURLConnection;

    move-result-object v0

    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    .line 263
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 265
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 268
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, v0, p1}, Lepson/common/httpclient/IAHttpClient;->makeHeader(Ljava/net/HttpURLConnection;Lepson/common/httpclient/IAHttpClient$HttpBase;)V

    .line 271
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getContentLength()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getContentLength()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 275
    :cond_0
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v2, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 276
    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    .line 277
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getEntityFileName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x400

    invoke-direct {v2, v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 278
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getEntityFileDataOffset()I

    move-result v3

    int-to-long v5, v3

    invoke-virtual {v2, v5, v6}, Ljava/io/BufferedInputStream;->skip(J)J

    .line 281
    iget-object v3, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 286
    :try_start_1
    new-array v3, v4, [B

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 287
    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    add-int v7, v5, v6

    .line 289
    :try_start_2
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getEntityFileDataSize()I

    move-result v8

    if-ge v7, v8, :cond_1

    .line 290
    invoke-virtual {v0, v3, v4, v6}, Ljava/io/DataOutputStream;->write([BII)V

    move v5, v7

    goto :goto_0

    .line 293
    :cond_1
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getEntityFileDataSize()I

    move-result p1

    sub-int/2addr p1, v5

    invoke-virtual {v0, v3, v4, p1}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_3
    const-string v1, "IAHttpClient"

    .line 297
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error: in executeFile() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    throw p1

    .line 301
    :cond_2
    :goto_1
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 307
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 314
    :catch_1
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 322
    :catch_2
    :try_start_6
    iget-object p1, p0, Lepson/common/httpclient/IAHttpClient;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1, v1}, Lepson/common/httpclient/IAHttpClient;->parseResponse(Ljava/net/HttpURLConnection;Z)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 328
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_3
    move-exception p1

    .line 303
    :try_start_7
    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 307
    :goto_2
    :try_start_8
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 314
    :catch_4
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 317
    :catch_5
    :try_start_a
    throw p1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catchall_1
    move-exception p1

    goto :goto_3

    :catch_6
    move-exception p1

    .line 325
    :try_start_b
    throw p1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 328
    :goto_3
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V

    throw p1
.end method

.method makeHeader(Ljava/net/HttpURLConnection;Lepson/common/httpclient/IAHttpClient$HttpBase;)V
    .locals 2

    .line 342
    invoke-virtual {p2}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "Content-Type"

    .line 343
    invoke-virtual {p2}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :cond_0
    invoke-virtual {p2}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "Content-Encoding"

    .line 346
    invoke-virtual {p2}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentEncoding()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_1
    invoke-virtual {p2}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getChunked()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 349
    invoke-virtual {p2}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getChunked()Ljava/lang/Boolean;

    move-result-object p2

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const-string p2, "Transfer-Encoding"

    const-string v0, "chunked"

    .line 350
    invoke-virtual {p1, p2, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method parseResponse(Ljava/net/HttpURLConnection;Z)Lepson/common/httpclient/IAHttpClient$HttpResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 364
    new-instance v0, Lepson/common/httpclient/IAHttpClient$HttpResponse;

    invoke-direct {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;-><init>()V

    .line 367
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->setResponseCode(I)V

    .line 370
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->setContentLength(Ljava/lang/Integer;)V

    .line 371
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 372
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->setContentEncoding(Ljava/lang/String;)V

    .line 374
    :cond_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 375
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->setContentType(Ljava/lang/String;)V

    :cond_1
    const-string v1, "IAHttpClient"

    .line 378
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ResponseCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " Content-Type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    invoke-virtual {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " Content-Length = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    invoke-virtual {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getContentLength()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 378
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_3

    .line 385
    invoke-virtual {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result p2

    div-int/lit8 p2, p2, 0x64

    packed-switch p2, :pswitch_data_0

    .line 394
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object p1

    goto :goto_0

    .line 389
    :pswitch_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_3

    .line 400
    new-instance p2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x400

    .line 402
    :try_start_0
    new-array v1, v1, [B

    .line 404
    :goto_1
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-gtz v2, :cond_2

    .line 410
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->setEntity([B)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 417
    :goto_2
    :try_start_1
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_4

    :cond_2
    const/4 v3, 0x0

    .line 408
    :try_start_2
    invoke-virtual {p2, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    move-exception p1

    :try_start_3
    const-string v1, "IAHttpClient"

    .line 412
    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 417
    :goto_3
    :try_start_4
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 420
    :catch_1
    throw p1

    :catch_2
    :cond_3
    :goto_4
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setConnectionTimeout(I)V
    .locals 0

    .line 47
    iput p1, p0, Lepson/common/httpclient/IAHttpClient;->connectionTimeout:I

    return-void
.end method

.method public setFollowRedirects(Z)V
    .locals 0

    .line 55
    iput-boolean p1, p0, Lepson/common/httpclient/IAHttpClient;->followRedirects:Z

    return-void
.end method

.method public setSoTimeout(I)V
    .locals 0

    .line 51
    iput p1, p0, Lepson/common/httpclient/IAHttpClient;->readTimeout:I

    return-void
.end method

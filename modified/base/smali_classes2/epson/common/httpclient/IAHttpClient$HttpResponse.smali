.class public Lepson/common/httpclient/IAHttpClient$HttpResponse;
.super Lepson/common/httpclient/IAHttpClient$HttpBase;
.source "IAHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/httpclient/IAHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpResponse"
.end annotation


# instance fields
.field private responseCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 475
    invoke-direct {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;-><init>()V

    const/4 v0, 0x0

    .line 477
    iput v0, p0, Lepson/common/httpclient/IAHttpClient$HttpResponse;->responseCode:I

    return-void
.end method


# virtual methods
.method public bridge synthetic getChunked()Ljava/lang/Boolean;
    .locals 1

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getChunked()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentEncoding()Ljava/lang/String;
    .locals 1

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentLength()Ljava/lang/Integer;
    .locals 1

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentLength()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentType()Ljava/lang/String;
    .locals 1

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getEntity()Ljava/io/ByteArrayOutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getEntityFileDataOffset()I
    .locals 1

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileDataOffset()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getEntityFileDataSize()I
    .locals 1

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileDataSize()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getEntityFileName()Ljava/lang/String;
    .locals 1

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .line 480
    iget v0, p0, Lepson/common/httpclient/IAHttpClient$HttpResponse;->responseCode:I

    return v0
.end method

.method public bridge synthetic getURI()Ljava/net/URL;
    .locals 1

    .line 475
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getURI()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setChunked(Ljava/lang/Boolean;)V
    .locals 0

    .line 475
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setChunked(Ljava/lang/Boolean;)V

    return-void
.end method

.method public bridge synthetic setContentEncoding(Ljava/lang/String;)V
    .locals 0

    .line 475
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentEncoding(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setContentLength(Ljava/lang/Integer;)V
    .locals 0

    .line 475
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentLength(Ljava/lang/Integer;)V

    return-void
.end method

.method public bridge synthetic setContentType(Ljava/lang/String;)V
    .locals 0

    .line 475
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentType(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setEntity([B)V
    .locals 0

    .line 475
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setEntity([B)V

    return-void
.end method

.method public bridge synthetic setEntityFile(Ljava/lang/String;II)V
    .locals 0

    .line 475
    invoke-super {p0, p1, p2, p3}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setEntityFile(Ljava/lang/String;II)V

    return-void
.end method

.method public setResponseCode(I)V
    .locals 0

    .line 484
    iput p1, p0, Lepson/common/httpclient/IAHttpClient$HttpResponse;->responseCode:I

    return-void
.end method

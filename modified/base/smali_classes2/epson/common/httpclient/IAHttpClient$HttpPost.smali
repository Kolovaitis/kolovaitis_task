.class public Lepson/common/httpclient/IAHttpClient$HttpPost;
.super Lepson/common/httpclient/IAHttpClient$HttpBase;
.source "IAHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/httpclient/IAHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpPost"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 446
    invoke-direct {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static getUrlEncodedFormEntity(Ljava/util/List;Ljava/lang/String;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lepson/common/httpclient/BasicNameValuePair;",
            ">;",
            "Ljava/lang/String;",
            ")[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 459
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/common/httpclient/BasicNameValuePair;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :cond_0
    const-string v3, "&"

    .line 463
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    :goto_1
    invoke-virtual {v2}, Lepson/common/httpclient/BasicNameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "="

    .line 466
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 467
    invoke-virtual {v2}, Lepson/common/httpclient/BasicNameValuePair;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 470
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic getChunked()Ljava/lang/Boolean;
    .locals 1

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getChunked()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentEncoding()Ljava/lang/String;
    .locals 1

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentLength()Ljava/lang/Integer;
    .locals 1

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentLength()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentType()Ljava/lang/String;
    .locals 1

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getContentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getEntity()Ljava/io/ByteArrayOutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getEntityFileDataOffset()I
    .locals 1

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileDataOffset()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getEntityFileDataSize()I
    .locals 1

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileDataSize()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getEntityFileName()Ljava/lang/String;
    .locals 1

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getEntityFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getURI()Ljava/net/URL;
    .locals 1

    .line 443
    invoke-super {p0}, Lepson/common/httpclient/IAHttpClient$HttpBase;->getURI()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setChunked(Ljava/lang/Boolean;)V
    .locals 0

    .line 443
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setChunked(Ljava/lang/Boolean;)V

    return-void
.end method

.method public bridge synthetic setContentEncoding(Ljava/lang/String;)V
    .locals 0

    .line 443
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentEncoding(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setContentLength(Ljava/lang/Integer;)V
    .locals 0

    .line 443
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentLength(Ljava/lang/Integer;)V

    return-void
.end method

.method public bridge synthetic setContentType(Ljava/lang/String;)V
    .locals 0

    .line 443
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setContentType(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setEntity([B)V
    .locals 0

    .line 443
    invoke-super {p0, p1}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setEntity([B)V

    return-void
.end method

.method public bridge synthetic setEntityFile(Ljava/lang/String;II)V
    .locals 0

    .line 443
    invoke-super {p0, p1, p2, p3}, Lepson/common/httpclient/IAHttpClient$HttpBase;->setEntityFile(Ljava/lang/String;II)V

    return-void
.end method

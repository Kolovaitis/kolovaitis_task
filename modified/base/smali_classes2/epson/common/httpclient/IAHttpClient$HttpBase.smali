.class abstract Lepson/common/httpclient/IAHttpClient$HttpBase;
.super Ljava/lang/Object;
.source "IAHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/common/httpclient/IAHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "HttpBase"
.end annotation


# instance fields
.field private chunked:Ljava/lang/Boolean;

.field private contentEncoding:Ljava/lang/String;

.field private contentLength:Ljava/lang/Integer;

.field private contentType:Ljava/lang/String;

.field private entityBuffer:[B

.field private entityFileDataOffset:I

.field private entityFileDataSize:I

.field private entityFileName:Ljava/lang/String;

.field private url:Ljava/net/URL;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 493
    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->url:Ljava/net/URL;

    .line 497
    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->chunked:Ljava/lang/Boolean;

    .line 499
    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->contentLength:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 493
    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->url:Ljava/net/URL;

    .line 497
    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->chunked:Ljava/lang/Boolean;

    .line 499
    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->contentLength:Ljava/lang/Integer;

    .line 513
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->url:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 515
    invoke-virtual {p1}, Ljava/net/MalformedURLException;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public getChunked()Ljava/lang/Boolean;
    .locals 1

    .line 540
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->chunked:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getContentEncoding()Ljava/lang/String;
    .locals 1

    .line 532
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->contentEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getContentLength()Ljava/lang/Integer;
    .locals 1

    .line 548
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->contentLength:Ljava/lang/Integer;

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .line 524
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public getEntity()Ljava/io/ByteArrayOutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 560
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    iget-object v1, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityBuffer:[B

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 561
    iget-object v1, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityBuffer:[B

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    return-object v0
.end method

.method public getEntityFileDataOffset()I
    .locals 1

    .line 570
    iget v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityFileDataOffset:I

    return v0
.end method

.method public getEntityFileDataSize()I
    .locals 1

    .line 574
    iget v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityFileDataSize:I

    return v0
.end method

.method public getEntityFileName()Ljava/lang/String;
    .locals 1

    .line 566
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getURI()Ljava/net/URL;
    .locals 1

    .line 520
    iget-object v0, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->url:Ljava/net/URL;

    return-object v0
.end method

.method public setChunked(Ljava/lang/Boolean;)V
    .locals 0

    .line 544
    iput-object p1, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->chunked:Ljava/lang/Boolean;

    return-void
.end method

.method public setContentEncoding(Ljava/lang/String;)V
    .locals 0

    .line 536
    iput-object p1, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->contentEncoding:Ljava/lang/String;

    return-void
.end method

.method public setContentLength(Ljava/lang/Integer;)V
    .locals 0

    .line 552
    iput-object p1, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->contentLength:Ljava/lang/Integer;

    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0

    .line 528
    iput-object p1, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->contentType:Ljava/lang/String;

    return-void
.end method

.method public setEntity([B)V
    .locals 0

    .line 556
    iput-object p1, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityBuffer:[B

    return-void
.end method

.method public setEntityFile(Ljava/lang/String;II)V
    .locals 0

    .line 578
    iput-object p1, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityFileName:Ljava/lang/String;

    .line 579
    iput p2, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityFileDataOffset:I

    .line 580
    iput p3, p0, Lepson/common/httpclient/IAHttpClient$HttpBase;->entityFileDataSize:I

    return-void
.end method

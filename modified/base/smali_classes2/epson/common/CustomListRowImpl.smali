.class public Lepson/common/CustomListRowImpl;
.super Ljava/lang/Object;
.source "CustomListRowImpl.java"

# interfaces
.implements Lepson/common/CustomListRow;


# instance fields
.field private prefixImage:Ljava/lang/Integer;

.field size:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private suffixImage:Ljava/lang/Integer;

.field texts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lepson/common/CustomListRowImpl;->prefixImage:Ljava/lang/Integer;

    .line 9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepson/common/CustomListRowImpl;->suffixImage:Ljava/lang/Integer;

    return-void
.end method

.method public static create()Lepson/common/CustomListRowImpl;
    .locals 2

    .line 16
    new-instance v0, Lepson/common/CustomListRowImpl;

    invoke-direct {v0}, Lepson/common/CustomListRowImpl;-><init>()V

    .line 17
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lepson/common/CustomListRowImpl;->texts:Ljava/util/List;

    .line 18
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lepson/common/CustomListRowImpl;->size:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;
    .locals 1

    .line 36
    iget-object v0, p0, Lepson/common/CustomListRowImpl;->texts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    iget-object p1, p0, Lepson/common/CustomListRowImpl;->size:Ljava/util/List;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addText(Ljava/lang/String;F)Lepson/common/CustomListRowImpl;
    .locals 1

    .line 30
    iget-object v0, p0, Lepson/common/CustomListRowImpl;->texts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    iget-object p1, p0, Lepson/common/CustomListRowImpl;->size:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getPrefixImageId()Ljava/lang/Integer;
    .locals 1

    .line 42
    iget-object v0, p0, Lepson/common/CustomListRowImpl;->prefixImage:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSuffixImageId()Ljava/lang/Integer;
    .locals 1

    .line 46
    iget-object v0, p0, Lepson/common/CustomListRowImpl;->suffixImage:Ljava/lang/Integer;

    return-object v0
.end method

.method public getText(I)Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lepson/common/CustomListRowImpl;->texts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getTextSize(I)F
    .locals 1

    .line 54
    iget-object v0, p0, Lepson/common/CustomListRowImpl;->size:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    return p1
.end method

.method public prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;
    .locals 0

    .line 22
    iput-object p1, p0, Lepson/common/CustomListRowImpl;->prefixImage:Ljava/lang/Integer;

    return-object p0
.end method

.method public sieze()I
    .locals 1

    .line 58
    iget-object v0, p0, Lepson/common/CustomListRowImpl;->texts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;
    .locals 0

    .line 26
    iput-object p1, p0, Lepson/common/CustomListRowImpl;->suffixImage:Ljava/lang/Integer;

    return-object p0
.end method

.class public Lepson/provider/SharedPreferencesProvider$Editor;
.super Ljava/lang/Object;
.source "SharedPreferencesProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/provider/SharedPreferencesProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Editor"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field editor:Landroid/content/SharedPreferences$Editor;

.field private values:Landroid/content/ContentValues;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    .line 365
    iput-object p1, p0, Lepson/provider/SharedPreferencesProvider$Editor;->context:Landroid/content/Context;

    .line 366
    invoke-static {}, Lepson/provider/SharedPreferencesProvider;->access$200()Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iput-object p1, p0, Lepson/provider/SharedPreferencesProvider$Editor;->editor:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lepson/provider/SharedPreferencesProvider$1;)V
    .locals 0

    .line 361
    invoke-direct {p0, p1}, Lepson/provider/SharedPreferencesProvider$Editor;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public apply()V
    .locals 4

    .line 372
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lepson/provider/SharedPreferencesProvider$Editor;->context:Landroid/content/Context;

    const-string v2, "key"

    const-string v3, "type"

    invoke-static {v1, v2, v3}, Lepson/provider/SharedPreferencesProvider;->access$300(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void
.end method

.method public apply(Ljava/lang/String;)V
    .locals 3

    .line 381
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x317564b7

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "analytics_prefs"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-nez v0, :cond_2

    .line 383
    iget-object p1, p0, Lepson/provider/SharedPreferencesProvider$Editor;->context:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->context:Landroid/content/Context;

    const-string v1, "analytics"

    invoke-static {v0, v1}, Lepson/provider/SharedPreferencesProvider;->access$400(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void

    .line 386
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown preference!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clear()V
    .locals 4

    .line 429
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->context:Landroid/content/Context;

    invoke-static {v0}, Lepson/provider/SharedPreferencesProvider;->checkPackageUseSharedPreferencesProvider(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lepson/provider/SharedPreferencesProvider$Editor;->context:Landroid/content/Context;

    const-string v2, "key"

    const-string v3, "type"

    invoke-static {v1, v2, v3}, Lepson/provider/SharedPreferencesProvider;->access$300(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 432
    :cond_0
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    :goto_0
    return-void
.end method

.method public commit()V
    .locals 0

    .line 392
    invoke-virtual {p0}, Lepson/provider/SharedPreferencesProvider$Editor;->apply()V

    return-void
.end method

.method public putBoolean(Ljava/lang/String;Z)Lepson/provider/SharedPreferencesProvider$Editor;
    .locals 1

    .line 406
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object p0
.end method

.method public putFloat(Ljava/lang/String;F)Lepson/provider/SharedPreferencesProvider$Editor;
    .locals 1

    .line 416
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    return-object p0
.end method

.method public putInt(Ljava/lang/String;I)Lepson/provider/SharedPreferencesProvider$Editor;
    .locals 1

    .line 411
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object p0
.end method

.method public putLong(Ljava/lang/String;J)Lepson/provider/SharedPreferencesProvider$Editor;
    .locals 1

    .line 401
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-object p0
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$Editor;
    .locals 1

    .line 396
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1

    .line 421
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$Editor;->values:Landroid/content/ContentValues;

    invoke-virtual {v0, p1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    return-void
.end method

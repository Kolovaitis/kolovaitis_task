.class public Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;
.super Ljava/lang/Object;
.source "SharedPreferencesProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/provider/SharedPreferencesProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SharedPreferencesMulti"
.end annotation


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447
    iput-object p1, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lepson/provider/SharedPreferencesProvider$1;)V
    .locals 0

    .line 438
    invoke-direct {p0, p1}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public edit()Lepson/provider/SharedPreferencesProvider$Editor;
    .locals 3

    .line 451
    new-instance v0, Lepson/provider/SharedPreferencesProvider$Editor;

    iget-object v1, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/provider/SharedPreferencesProvider$Editor;-><init>(Landroid/content/Context;Lepson/provider/SharedPreferencesProvider$1;)V

    return-object v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 7

    .line 470
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    const-string v2, "boolean"

    invoke-static {v0, p1, v2}, Lepson/provider/SharedPreferencesProvider;->access$300(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 471
    invoke-static {p1, p2}, Lepson/provider/SharedPreferencesProvider;->access$800(Landroid/database/Cursor;Z)Z

    move-result p1

    return p1
.end method

.method public getFloat(Ljava/lang/String;F)F
    .locals 7

    .line 465
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    const-string v2, "float"

    invoke-static {v0, p1, v2}, Lepson/provider/SharedPreferencesProvider;->access$300(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 466
    invoke-static {p1, p2}, Lepson/provider/SharedPreferencesProvider;->access$700(Landroid/database/Cursor;F)F

    move-result p1

    return p1
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 7

    .line 475
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    const-string v2, "integer"

    invoke-static {v0, p1, v2}, Lepson/provider/SharedPreferencesProvider;->access$300(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 476
    invoke-static {p1, p2}, Lepson/provider/SharedPreferencesProvider;->access$900(Landroid/database/Cursor;I)I

    move-result p1

    return p1
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 7

    .line 460
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    const-string v2, "long"

    invoke-static {v0, p1, v2}, Lepson/provider/SharedPreferencesProvider;->access$300(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 461
    invoke-static {p1, p2, p3}, Lepson/provider/SharedPreferencesProvider;->access$600(Landroid/database/Cursor;J)J

    move-result-wide p1

    return-wide p1
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 455
    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->context:Landroid/content/Context;

    const-string v2, "string"

    invoke-static {v0, p1, v2}, Lepson/provider/SharedPreferencesProvider;->access$300(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 456
    invoke-static {p1, p2}, Lepson/provider/SharedPreferencesProvider;->access$500(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

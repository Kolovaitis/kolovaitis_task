.class Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;
.super Ljava/lang/Object;
.source "ScanSettingsAdvanceDetailActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)V
    .locals 0

    .line 148
    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 154
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;

    if-eqz p1, :cond_3

    .line 155
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;

    .line 156
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-virtual {p2}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object p2

    .line 157
    iget-object p3, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$000(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)I

    move-result p3

    const p4, 0x7f0e0504

    if-eq p3, p4, :cond_1

    const p4, 0x7f0e050c

    if-eq p3, p4, :cond_0

    goto :goto_0

    :cond_0
    const-string p3, "SCAN_REFS_SETTINGS_GAMMA"

    .line 174
    iget p4, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->value:I

    invoke-virtual {p2, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p3, "SCAN_REFS_SETTINGS_GAMMA_NAME"

    .line 175
    iget-object p1, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->text:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    const/4 p3, -0x1

    invoke-virtual {p1, p3, p2}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->setResult(ILandroid/content/Intent;)V

    .line 179
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->finish()V

    goto :goto_0

    .line 159
    :cond_1
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    iget p3, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->value:I

    invoke-static {p2, p3}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$102(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;I)I

    .line 160
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    iget-object p1, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->text:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$202(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 162
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$300(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;

    move-result-object p1

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p2}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$100(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/activity/ScanSettingsDetailAdapter;->setSelected(I)V

    .line 163
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$300(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/activity/ScanSettingsDetailAdapter;->notifyDataSetChanged()V

    .line 165
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$100(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_2

    .line 166
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$400(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Landroid/widget/ListView;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 168
    :cond_2
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$400(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Landroid/widget/ListView;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setVisibility(I)V

    :cond_3
    :goto_0
    return-void
.end method

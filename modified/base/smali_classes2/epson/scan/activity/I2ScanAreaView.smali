.class public Lepson/scan/activity/I2ScanAreaView;
.super Landroid/view/View;
.source "I2ScanAreaView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/I2ScanAreaView$FlickCallback;
    }
.end annotation


# static fields
.field private static final CONTOUR_AREA_BACK_COLOR:I = -0x1


# instance fields
.field private final SCAN_AREA_CORNER_LENGTH_DP:I

.field private final SCAN_AREA_CORNER_LONG_TAP_AREA_WIDTH_DP:I

.field private final SCAN_AREA_CORNER_WIDTH_DP:I

.field private final SCAN_AREA_MOVE_MODE_AREA_MOVE:I

.field private final SCAN_AREA_MOVE_MODE_LEFT_UPPER:I

.field private final SCAN_AREA_MOVE_MODE_NONE:I

.field private final SCAN_AREA_MOVE_MODE_RIGHT_LOWER:I

.field private mBreakStrings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDashInterval:[F

.field private mDisplayContourArea:Landroid/graphics/Rect;

.field private mDisplayDensity:F

.field private mDisplayMovableArea:Landroid/graphics/Rect;

.field private mDisplayPreviewImageRect:Landroid/graphics/Rect;

.field private mDisplayRate:D

.field private mDisplayTextLeft:F

.field private mDisplayTextYPosition:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mFlickCallback:Lepson/scan/activity/I2ScanAreaView$FlickCallback;

.field private mGestureDetectorCompat:Landroid/support/v4/view/GestureDetectorCompat;

.field private final mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private mMovableAreaMode:Z

.field private mOrgContourHeight:I

.field private mOrgContourWidth:I

.field private mOrgMovableArea:Landroid/graphics/Rect;

.field private mOrgPreviewImageRect:Landroid/graphics/Rect;

.field private mPaint:Landroid/graphics/Paint;

.field private mPhase:F

.field private mPrevX:F

.field private mPrevY:F

.field private mPreviewImageBitmap:Landroid/graphics/Bitmap;

.field private mPreviewImageFile:Ljava/lang/String;

.field private mPromptTextColor:I

.field private mPromptTextDisplay:Z

.field private mPromptTextSize:F

.field private mScanAreaCenterBitmap:Landroid/graphics/Bitmap;

.field private mScanAreaCornerLengthPixel:I

.field private mScanAreaCornerLogTapAreaWidthPixel:I

.field private mScanAreaCornerWidthPixel:I

.field private mScanAreaModified:Z

.field private mScanAreaMoveMode:I

.field private final mScanAreaPath:Landroid/graphics/Path;

.field private mScanPromptString:Ljava/lang/String;

.field private mTextPaint:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 178
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 33
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_NONE:I

    const/4 v0, 0x1

    .line 34
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_LEFT_UPPER:I

    const/4 v0, 0x2

    .line 35
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_RIGHT_LOWER:I

    const/4 v0, 0x3

    .line 36
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_AREA_MOVE:I

    const/16 v0, 0x1e

    .line 39
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_LENGTH_DP:I

    const/4 v0, 0x4

    .line 42
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_WIDTH_DP:I

    const/16 v0, 0x3c

    .line 45
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_LONG_TAP_AREA_WIDTH_DP:I

    const v0, -0xffff01

    .line 57
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextColor:I

    const/high16 v0, 0x41200000    # 10.0f

    .line 58
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextSize:F

    .line 135
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    .line 828
    new-instance v0, Lepson/scan/activity/I2ScanAreaView$1;

    invoke-direct {v0, p0}, Lepson/scan/activity/I2ScanAreaView$1;-><init>(Lepson/scan/activity/I2ScanAreaView;)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    const/4 v0, 0x0

    .line 179
    invoke-direct {p0, v0, p1}, Lepson/scan/activity/I2ScanAreaView;->init(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 183
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 33
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_NONE:I

    const/4 v0, 0x1

    .line 34
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_LEFT_UPPER:I

    const/4 v0, 0x2

    .line 35
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_RIGHT_LOWER:I

    const/4 v0, 0x3

    .line 36
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_AREA_MOVE:I

    const/16 v0, 0x1e

    .line 39
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_LENGTH_DP:I

    const/4 v0, 0x4

    .line 42
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_WIDTH_DP:I

    const/16 v0, 0x3c

    .line 45
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_LONG_TAP_AREA_WIDTH_DP:I

    const v0, -0xffff01

    .line 57
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextColor:I

    const/high16 v0, 0x41200000    # 10.0f

    .line 58
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextSize:F

    .line 135
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    .line 828
    new-instance v0, Lepson/scan/activity/I2ScanAreaView$1;

    invoke-direct {v0, p0}, Lepson/scan/activity/I2ScanAreaView$1;-><init>(Lepson/scan/activity/I2ScanAreaView;)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 184
    invoke-direct {p0, p2, p1}, Lepson/scan/activity/I2ScanAreaView;->init(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 188
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 33
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_NONE:I

    const/4 p1, 0x1

    .line 34
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_LEFT_UPPER:I

    const/4 p1, 0x2

    .line 35
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_RIGHT_LOWER:I

    const/4 p1, 0x3

    .line 36
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_MOVE_MODE_AREA_MOVE:I

    const/16 p1, 0x1e

    .line 39
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_LENGTH_DP:I

    const/4 p1, 0x4

    .line 42
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_WIDTH_DP:I

    const/16 p1, 0x3c

    .line 45
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->SCAN_AREA_CORNER_LONG_TAP_AREA_WIDTH_DP:I

    const p1, -0xffff01

    .line 57
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextColor:I

    const/high16 p1, 0x41200000    # 10.0f

    .line 58
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextSize:F

    .line 135
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    .line 828
    new-instance p1, Lepson/scan/activity/I2ScanAreaView$1;

    invoke-direct {p1, p0}, Lepson/scan/activity/I2ScanAreaView$1;-><init>(Lepson/scan/activity/I2ScanAreaView;)V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 189
    invoke-direct {p0, p2, p3}, Lepson/scan/activity/I2ScanAreaView;->init(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/I2ScanAreaView;F)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanAreaView;->checkFlick(F)V

    return-void
.end method

.method static synthetic access$100(Lepson/scan/activity/I2ScanAreaView;FF)Z
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lepson/scan/activity/I2ScanAreaView;->checkMoveMode(FF)Z

    move-result p0

    return p0
.end method

.method private calcDrawSize(II)V
    .locals 9

    if-eqz p1, :cond_8

    if-eqz p2, :cond_8

    .line 269
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourWidth:I

    if-eqz v0, :cond_8

    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourHeight:I

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 274
    :cond_0
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getPaddingLeft()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getPaddingRight()I

    move-result v0

    sub-int/2addr p1, v0

    .line 275
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getPaddingTop()I

    move-result v0

    sub-int/2addr p2, v0

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getPaddingBottom()I

    move-result v0

    sub-int/2addr p2, v0

    const/4 v0, 0x0

    if-lez p1, :cond_7

    if-gtz p2, :cond_1

    goto/16 :goto_1

    .line 282
    :cond_1
    iget v1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourWidth:I

    int-to-double v1, v1

    int-to-double v3, p1

    div-double/2addr v1, v3

    .line 283
    iget v3, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourHeight:I

    int-to-double v3, v3

    int-to-double v5, p2

    div-double/2addr v3, v5

    .line 285
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    .line 286
    iput-wide v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayRate:D

    .line 288
    iget v3, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourWidth:I

    int-to-double v3, v3

    div-double/2addr v3, v1

    double-to-int v3, v3

    .line 289
    iget v4, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourHeight:I

    int-to-double v4, v4

    div-double/2addr v4, v1

    double-to-int v4, v4

    .line 290
    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    invoke-direct {v5, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    .line 291
    iget-object v5, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getPaddingLeft()I

    move-result v7

    sub-int/2addr p1, v3

    div-int/lit8 p1, p1, 0x2

    add-int/2addr v7, p1

    .line 292
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getPaddingTop()I

    move-result p1

    sub-int/2addr p2, v4

    div-int/lit8 p2, p2, 0x2

    add-int/2addr p1, p2

    .line 291
    invoke-virtual {v5, v7, p1}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 294
    iget-boolean p1, p0, Lepson/scan/activity/I2ScanAreaView;->mMovableAreaMode:Z

    if-eqz p1, :cond_2

    .line 295
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    if-eqz p1, :cond_3

    .line 296
    new-instance p2, Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/Rect;->left:I

    int-to-double v3, p1

    div-double/2addr v3, v1

    .line 297
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int p1, v3

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-double v3, v0

    div-double/2addr v3, v1

    .line 298
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v0, v3

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-double v3, v3

    div-double/2addr v3, v1

    .line 299
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v4, v3

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-double v7, v3

    div-double/2addr v7, v1

    .line 300
    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    long-to-int v3, v7

    invoke-direct {p2, p1, v0, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    .line 302
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_0

    .line 305
    :cond_2
    iput-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    .line 310
    :cond_3
    :goto_0
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgPreviewImageRect:Landroid/graphics/Rect;

    if-eqz p1, :cond_5

    .line 311
    new-instance p2, Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/Rect;->left:I

    int-to-double v3, p1

    div-double/2addr v3, v1

    .line 312
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int p1, v3

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgPreviewImageRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-double v3, v0

    div-double/2addr v3, v1

    .line 313
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v0, v3

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgPreviewImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-double v3, v3

    div-double/2addr v3, v1

    .line 314
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v4, v3

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgPreviewImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-double v7, v3

    div-double/2addr v7, v1

    .line 315
    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v2, v1

    invoke-direct {p2, p1, v0, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayPreviewImageRect:Landroid/graphics/Rect;

    .line 317
    iget-boolean p1, p0, Lepson/scan/activity/I2ScanAreaView;->mMovableAreaMode:Z

    if-nez p1, :cond_4

    .line 320
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    iget-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayPreviewImageRect:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p2

    sub-int/2addr p1, p2

    div-int/lit8 p1, p1, 0x2

    .line 322
    iget-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayPreviewImageRect:Landroid/graphics/Rect;

    invoke-virtual {p2, p1, v6}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 326
    :cond_4
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayPreviewImageRect:Landroid/graphics/Rect;

    iget-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 329
    :cond_5
    invoke-direct {p0}, Lepson/scan/activity/I2ScanAreaView;->readPreviewBitmap()V

    .line 333
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanPromptString:Ljava/lang/String;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_6

    .line 334
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanAreaView;->calcText(Landroid/graphics/Rect;)V

    :cond_6
    return-void

    .line 278
    :cond_7
    :goto_1
    iput-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    return-void

    :cond_8
    :goto_2
    return-void
.end method

.method private calcText(Landroid/graphics/Rect;)V
    .locals 11

    .line 351
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f666666    # 0.9f

    mul-float v0, v0, v1

    .line 352
    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanPromptString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 353
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 357
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mBreakStrings:Ljava/util/ArrayList;

    .line 358
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v5, v1, :cond_0

    .line 361
    iget-object v7, p0, Lepson/scan/activity/I2ScanAreaView;->mTextPaint:Landroid/text/TextPaint;

    iget-object v8, p0, Lepson/scan/activity/I2ScanAreaView;->mScanPromptString:Ljava/lang/String;

    invoke-virtual {v8, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v0, v10}, Landroid/text/TextPaint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v7

    .line 363
    iget-object v8, p0, Lepson/scan/activity/I2ScanAreaView;->mScanPromptString:Ljava/lang/String;

    add-int v9, v5, v7

    invoke-virtual {v8, v5, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 365
    iget-object v8, p0, Lepson/scan/activity/I2ScanAreaView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v8, v5, v4, v7, v2}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 366
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v6, v7

    .line 367
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    iget-object v7, p0, Lepson/scan/activity/I2ScanAreaView;->mBreakStrings:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v9

    goto :goto_0

    .line 374
    :cond_0
    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    const v2, 0x3d4cccd0    # 0.050000012f

    mul-float v0, v0, v2

    add-float/2addr v1, v0

    iput v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayTextLeft:F

    .line 377
    iget v0, p1, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    int-to-float p1, p1

    sub-float/2addr p1, v6

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr p1, v1

    add-float/2addr v0, p1

    .line 378
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayTextYPosition:Ljava/util/ArrayList;

    .line 380
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    .line 381
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayTextYPosition:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v0, v1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private checkFlick(F)V
    .locals 2

    .line 774
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mFlickCallback:Lepson/scan/activity/I2ScanAreaView$FlickCallback;

    if-nez v0, :cond_0

    return-void

    .line 779
    :cond_0
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3dcccccd    # 0.1f

    mul-float v0, v0, v1

    neg-float v1, v0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    .line 783
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mFlickCallback:Lepson/scan/activity/I2ScanAreaView$FlickCallback;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lepson/scan/activity/I2ScanAreaView$FlickCallback;->flick(I)V

    goto :goto_0

    :cond_1
    cmpl-float p1, p1, v0

    if-lez p1, :cond_2

    .line 785
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mFlickCallback:Lepson/scan/activity/I2ScanAreaView$FlickCallback;

    const/4 v0, -0x1

    invoke-interface {p1, v0}, Lepson/scan/activity/I2ScanAreaView$FlickCallback;->flick(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method private checkMoveMode(FF)Z
    .locals 7

    .line 797
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 802
    :cond_0
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPrevX:F

    .line 803
    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mPrevY:F

    .line 806
    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    float-to-double v2, v0

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float/2addr v0, p2

    float-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerLogTapAreaWidthPixel:I

    int-to-double v4, v0

    const/4 v0, 0x1

    cmpg-double v6, v2, v4

    if-gez v6, :cond_1

    .line 809
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    goto :goto_0

    .line 810
    :cond_1
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    sub-float/2addr v2, p1

    float-to-double v2, v2

    iget-object v4, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    sub-float/2addr v4, p2

    float-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    iget v4, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerLogTapAreaWidthPixel:I

    int-to-double v4, v4

    cmpg-double v6, v2, v4

    if-gez v6, :cond_2

    const/4 p1, 0x2

    .line 812
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    goto :goto_0

    .line 813
    :cond_2
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_3

    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    cmpg-float p1, p1, v2

    if-gtz p1, :cond_3

    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/Rect;->top:I

    int-to-float p1, p1

    cmpl-float p1, p2, p1

    if-ltz p1, :cond_3

    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float p1, p1

    cmpg-float p1, p2, p1

    if-gtz p1, :cond_3

    const/4 p1, 0x3

    .line 816
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    .line 821
    :goto_0
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaModified:Z

    .line 823
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->postInvalidate()V

    return v0

    :cond_3
    return v1
.end method

.method private checkTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 521
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    return v2

    .line 533
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-direct {p0, v0, p1}, Lepson/scan/activity/I2ScanAreaView;->moveScanArea(FF)Z

    move-result p1

    return p1

    .line 523
    :pswitch_1
    iget p1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    if-lez p1, :cond_0

    .line 525
    iput v2, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    .line 526
    invoke-direct {p0}, Lepson/scan/activity/I2ScanAreaView;->updateOrgScanArea()V

    .line 527
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->postInvalidate()V

    return v1

    :cond_0
    return v2

    :pswitch_2
    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private drawScanArea(Landroid/graphics/Canvas;)V
    .locals 12

    .line 437
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    return-void

    .line 443
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 444
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    const/4 v1, 0x3

    const/4 v2, 0x2

    if-ne v0, v1, :cond_1

    .line 445
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCenterBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 446
    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCenterBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/2addr v3, v2

    sub-int/2addr v1, v3

    .line 447
    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCenterBitmap:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    int-to-float v1, v1

    iget-object v4, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 450
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 451
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 452
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 453
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 454
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 455
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 457
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    const/4 v1, 0x0

    if-lez v0, :cond_3

    .line 460
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/DashPathEffect;

    iget-object v4, p0, Lepson/scan/activity/I2ScanAreaView;->mDashInterval:[F

    iget v5, p0, Lepson/scan/activity/I2ScanAreaView;->mPhase:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 462
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPhase:F

    iget v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayDensity:F

    add-float/2addr v0, v3

    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPhase:F

    .line 463
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPhase:F

    const v3, 0x461c4000    # 10000.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    const/4 v0, 0x0

    .line 464
    iput v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPhase:F

    .line 466
    :cond_2
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->postInvalidate()V

    goto :goto_0

    .line 468
    :cond_3
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 470
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 471
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 473
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaPath:Landroid/graphics/Path;

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 477
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerWidthPixel:I

    .line 478
    iget v3, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerLengthPixel:I

    .line 480
    iget v4, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    mul-int/lit8 v0, v0, 0x2

    mul-int/lit8 v3, v3, 0x2

    .line 484
    :cond_4
    iget-object v4, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    int-to-float v5, v0

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 485
    iget-object v4, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 486
    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 487
    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    div-int/2addr v0, v2

    sub-int/2addr v1, v0

    .line 488
    iget-object v4, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v0

    int-to-float v0, v1

    int-to-float v11, v4

    add-int/2addr v1, v3

    int-to-float v8, v1

    .line 490
    iget-object v10, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    move-object v5, p1

    move v6, v0

    move v7, v11

    move v9, v11

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/2addr v4, v3

    int-to-float v9, v4

    .line 492
    iget-object v10, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    move v8, v0

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 495
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerWidthPixel:I

    .line 496
    iget v1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerLengthPixel:I

    .line 497
    iget v3, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    if-ne v3, v2, :cond_5

    mul-int/lit8 v0, v0, 0x2

    mul-int/lit8 v1, v1, 0x2

    .line 501
    :cond_5
    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 502
    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    div-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 503
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v0

    int-to-float v0, v3

    int-to-float v10, v2

    sub-int/2addr v2, v1

    int-to-float v8, v2

    .line 505
    iget-object v9, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    move-object v4, p1

    move v5, v0

    move v6, v10

    move v7, v0

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    sub-int/2addr v3, v1

    int-to-float v7, v3

    .line 507
    iget-object v9, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    move v8, v10

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 4

    .line 194
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lepson/print/R$styleable;->I2ScanAreaView:[I

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 197
    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mScanPromptString:Ljava/lang/String;

    .line 199
    iget p2, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextColor:I

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextColor:I

    .line 204
    iget p2, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextSize:F

    const/4 v1, 0x2

    invoke-virtual {p1, v1, p2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextSize:F

    .line 208
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 211
    new-instance p1, Landroid/text/TextPaint;

    invoke-direct {p1}, Landroid/text/TextPaint;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mTextPaint:Landroid/text/TextPaint;

    .line 212
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFlags(I)V

    .line 213
    iget-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mTextPaint:Landroid/text/TextPaint;

    sget-object p2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, p2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 215
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    .line 217
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextDisplay:Z

    .line 218
    iput v2, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    .line 222
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 p2, 0x41f00000    # 30.0f

    mul-float p2, p2, p1

    .line 223
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result p2

    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerLengthPixel:I

    const/high16 p2, 0x40800000    # 4.0f

    mul-float p2, p2, p1

    .line 224
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result p2

    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerWidthPixel:I

    const/high16 p2, 0x42700000    # 60.0f

    mul-float p2, p2, p1

    .line 225
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result p2

    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCornerLogTapAreaWidthPixel:I

    .line 227
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v3, 0x7f070059

    invoke-static {p2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaCenterBitmap:Landroid/graphics/Bitmap;

    .line 228
    new-array p2, v1, [F

    const/high16 v1, 0x41200000    # 10.0f

    mul-float v1, v1, p1

    aput v1, p2, v2

    const/high16 v1, 0x40000000    # 2.0f

    mul-float v1, v1, p1

    aput v1, p2, v0

    iput-object p2, p0, Lepson/scan/activity/I2ScanAreaView;->mDashInterval:[F

    .line 229
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayDensity:F

    .line 231
    iput-boolean v2, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaModified:Z

    .line 234
    new-instance p1, Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getContext()Landroid/content/Context;

    move-result-object p2

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {p1, p2, v0}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mGestureDetectorCompat:Landroid/support/v4/view/GestureDetectorCompat;

    .line 237
    invoke-direct {p0}, Lepson/scan/activity/I2ScanAreaView;->invalidateTextPaintAndMeasurements()V

    return-void
.end method

.method private invalidateTextPaintAndMeasurements()V
    .locals 2

    .line 249
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mTextPaint:Landroid/text/TextPaint;

    iget v1, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextSize:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 250
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mTextPaint:Landroid/text/TextPaint;

    iget v1, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextColor:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    return-void
.end method

.method private moveScanArea(FF)Z
    .locals 5

    .line 569
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 573
    :cond_0
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPrevX:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 574
    iget v2, p0, Lepson/scan/activity/I2ScanAreaView;->mPrevY:F

    sub-float v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 576
    iget v3, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    packed-switch v3, :pswitch_data_0

    return v1

    .line 586
    :pswitch_0
    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v0

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 587
    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v2

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 588
    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v0

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 589
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 582
    :pswitch_1
    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v0

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 583
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 578
    :pswitch_2
    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v0

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 579
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 596
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-le v0, v2, :cond_1

    .line 597
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 598
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 599
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 601
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v0, v2, :cond_2

    .line 602
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 603
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 604
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 608
    :cond_2
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-gez v0, :cond_3

    .line 609
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 611
    :cond_3
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-gez v0, :cond_4

    .line 612
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 614
    :cond_4
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getWidth()I

    move-result v2

    if-lt v0, v2, :cond_5

    .line 615
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 617
    :cond_5
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getHeight()I

    move-result v2

    if-lt v0, v2, :cond_6

    .line 618
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 621
    :cond_6
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPrevX:F

    .line 622
    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mPrevY:F

    .line 624
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->postInvalidate()V

    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private readPreviewBitmap()V
    .locals 5

    .line 764
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPreviewImageFile:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 765
    iput-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPreviewImageBitmap:Landroid/graphics/Bitmap;

    return-void

    .line 769
    :cond_0
    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayPreviewImageRect:Landroid/graphics/Rect;

    .line 770
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayPreviewImageRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 769
    invoke-static {v0, v1, v2, v3, v4}, Lepson/common/ImageUtil;->loadBitmap(Ljava/lang/String;IIZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPreviewImageBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method private updateOrgScanArea()V
    .locals 6

    .line 545
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    return-void

    .line 550
    :cond_0
    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    const/4 v2, 0x0

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 551
    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 552
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-double v2, v0

    iget-wide v4, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayRate:D

    mul-double v2, v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 553
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-double v1, v1

    iget-wide v3, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayRate:D

    mul-double v1, v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v2, v1

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 557
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-double v0, v0

    iget-wide v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayRate:D

    mul-double v0, v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v1, v0

    .line 558
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    int-to-double v2, v0

    iget-wide v4, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayRate:D

    mul-double v2, v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    .line 559
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    return-void
.end method


# virtual methods
.method public getMovableScanArea()Landroid/graphics/Rect;
    .locals 1

    .line 170
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getOrgContour()[I
    .locals 3

    const/4 v0, 0x2

    .line 174
    new-array v0, v0, [I

    iget v1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourWidth:I

    const/4 v2, 0x0

    aput v1, v0, v2

    iget v1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourHeight:I

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object v0
.end method

.method public getPromptString()Ljava/lang/String;
    .locals 1

    .line 635
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanPromptString:Ljava/lang/String;

    return-object v0
.end method

.method public getPromptTextColor()I
    .locals 1

    .line 654
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextColor:I

    return v0
.end method

.method public getPromptTextSize()F
    .locals 1

    .line 673
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextSize:F

    return v0
.end method

.method public isScanAreaModified()Z
    .locals 1

    .line 166
    iget-boolean v0, p0, Lepson/scan/activity/I2ScanAreaView;->mMovableAreaMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaModified:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 389
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 391
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    return-void

    .line 398
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 399
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 400
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayContourArea:Landroid/graphics/Rect;

    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 404
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPreviewImageBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 405
    iget-object v2, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayPreviewImageRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lepson/scan/activity/I2ScanAreaView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 410
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayMovableArea:Landroid/graphics/Rect;

    if-eqz v0, :cond_2

    .line 411
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanAreaView;->drawScanArea(Landroid/graphics/Canvas;)V

    .line 417
    :cond_2
    iget-boolean v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextDisplay:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanPromptString:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mPreviewImageBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_4

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mBreakStrings:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayTextYPosition:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 419
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 421
    iget-object v1, p0, Lepson/scan/activity/I2ScanAreaView;->mBreakStrings:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 422
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 424
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 425
    iget v4, p0, Lepson/scan/activity/I2ScanAreaView;->mDisplayTextLeft:F

    iget-object v5, p0, Lepson/scan/activity/I2ScanAreaView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v2, v4, v3, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 255
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 256
    invoke-direct {p0, p1, p2}, Lepson/scan/activity/I2ScanAreaView;->calcDrawSize(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 512
    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaMoveMode:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lez v0, :cond_2

    .line 513
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanAreaView;->checkTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1

    .line 516
    :cond_2
    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mGestureDetectorCompat:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method public setArea(II)V
    .locals 1

    const/4 v0, 0x0

    .line 703
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaModified:Z

    .line 704
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourWidth:I

    .line 705
    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourHeight:I

    .line 706
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanAreaView;->mMovableAreaMode:Z

    .line 708
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getHeight()I

    move-result p2

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/I2ScanAreaView;->calcDrawSize(II)V

    return-void
.end method

.method public setFlickCallback(Lepson/scan/activity/I2ScanAreaView$FlickCallback;)V
    .locals 0

    .line 245
    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mFlickCallback:Lepson/scan/activity/I2ScanAreaView$FlickCallback;

    return-void
.end method

.method public setMovableAreaModeSize(IILandroid/graphics/Rect;)V
    .locals 1

    const/4 v0, 0x0

    .line 718
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanAreaView;->mScanAreaModified:Z

    .line 719
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourWidth:I

    .line 720
    iput p2, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourHeight:I

    const/4 p1, 0x1

    .line 721
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanAreaView;->mMovableAreaMode:Z

    .line 722
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1, p3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    .line 724
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getHeight()I

    move-result p2

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/I2ScanAreaView;->calcDrawSize(II)V

    return-void
.end method

.method public setPreviewImage(Ljava/lang/String;)V
    .locals 3

    .line 753
    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPreviewImageFile:Ljava/lang/String;

    .line 754
    iget-boolean p1, p0, Lepson/scan/activity/I2ScanAreaView;->mMovableAreaMode:Z

    if-eqz p1, :cond_0

    .line 755
    new-instance p1, Landroid/graphics/Rect;

    iget-object v0, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgMovableArea:Landroid/graphics/Rect;

    invoke-direct {p1, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgPreviewImageRect:Landroid/graphics/Rect;

    goto :goto_0

    .line 757
    :cond_0
    new-instance p1, Landroid/graphics/Rect;

    iget v0, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourWidth:I

    iget v1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgContourHeight:I

    const/4 v2, 0x0

    invoke-direct {p1, v2, v2, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgPreviewImageRect:Landroid/graphics/Rect;

    .line 760
    :goto_0
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getHeight()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/I2ScanAreaView;->calcDrawSize(II)V

    return-void
.end method

.method public setPreviewImage(Ljava/lang/String;[I)V
    .locals 5

    .line 735
    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPreviewImageFile:Ljava/lang/String;

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 737
    array-length p1, p2

    const/4 v0, 0x4

    if-lt p1, v0, :cond_0

    .line 738
    new-instance p1, Landroid/graphics/Rect;

    const/4 v0, 0x2

    aget v1, p2, v0

    const/4 v2, 0x3

    aget v3, p2, v2

    aget v0, p2, v0

    const/4 v4, 0x0

    aget v4, p2, v4

    add-int/2addr v0, v4

    aget v2, p2, v2

    const/4 v4, 0x1

    aget p2, p2, v4

    add-int/2addr v2, p2

    invoke-direct {p1, v1, v3, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgPreviewImageRect:Landroid/graphics/Rect;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 741
    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mOrgPreviewImageRect:Landroid/graphics/Rect;

    .line 742
    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPreviewImageBitmap:Landroid/graphics/Bitmap;

    .line 745
    :goto_0
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->getHeight()I

    move-result p2

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/I2ScanAreaView;->calcDrawSize(II)V

    return-void
.end method

.method public setPromptString(Ljava/lang/String;)V
    .locals 0

    .line 644
    iput-object p1, p0, Lepson/scan/activity/I2ScanAreaView;->mScanPromptString:Ljava/lang/String;

    .line 645
    invoke-direct {p0}, Lepson/scan/activity/I2ScanAreaView;->invalidateTextPaintAndMeasurements()V

    return-void
.end method

.method public setPromptStringVisibility(I)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 691
    :goto_0
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextDisplay:Z

    .line 692
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanAreaView;->invalidate()V

    return-void
.end method

.method public setPromptTextColor(I)V
    .locals 0

    .line 663
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextColor:I

    .line 664
    invoke-direct {p0}, Lepson/scan/activity/I2ScanAreaView;->invalidateTextPaintAndMeasurements()V

    return-void
.end method

.method public setPromptTextSize(F)V
    .locals 0

    .line 682
    iput p1, p0, Lepson/scan/activity/I2ScanAreaView;->mPromptTextSize:F

    .line 683
    invoke-direct {p0}, Lepson/scan/activity/I2ScanAreaView;->invalidateTextPaintAndMeasurements()V

    return-void
.end method

.class Lepson/scan/activity/InputUnitSettingActivity$1;
.super Ljava/lang/Object;
.source "InputUnitSettingActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/InputUnitSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/InputUnitSettingActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/InputUnitSettingActivity;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$1;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 118
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;

    .line 120
    iget-object p2, p0, Lepson/scan/activity/InputUnitSettingActivity$1;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    iget p1, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->value:I

    invoke-static {p2, p1}, Lepson/scan/activity/InputUnitSettingActivity;->access$002(Lepson/scan/activity/InputUnitSettingActivity;I)I

    .line 122
    iget-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$1;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p1}, Lepson/scan/activity/InputUnitSettingActivity;->access$100(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;

    move-result-object p1

    iget-object p2, p0, Lepson/scan/activity/InputUnitSettingActivity$1;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p2}, Lepson/scan/activity/InputUnitSettingActivity;->access$000(Lepson/scan/activity/InputUnitSettingActivity;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/activity/ScanSettingsDetailAdapter;->setSelected(I)V

    .line 123
    iget-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$1;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p1}, Lepson/scan/activity/InputUnitSettingActivity;->access$100(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/activity/ScanSettingsDetailAdapter;->notifyDataSetChanged()V

    .line 125
    iget-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$1;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p1}, Lepson/scan/activity/InputUnitSettingActivity;->access$000(Lepson/scan/activity/InputUnitSettingActivity;)I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 126
    iget-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$1;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p1}, Lepson/scan/activity/InputUnitSettingActivity;->access$200(Lepson/scan/activity/InputUnitSettingActivity;)Landroid/widget/ListView;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 128
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$1;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p1}, Lepson/scan/activity/InputUnitSettingActivity;->access$200(Lepson/scan/activity/InputUnitSettingActivity;)Landroid/widget/ListView;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_0
    return-void
.end method

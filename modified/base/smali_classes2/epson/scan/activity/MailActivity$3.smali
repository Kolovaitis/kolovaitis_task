.class Lepson/scan/activity/MailActivity$3;
.super Ljava/lang/Object;
.source "MailActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/scan/activity/MailActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/MailActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/MailActivity;)V
    .locals 0

    .line 117
    iput-object p1, p0, Lepson/scan/activity/MailActivity$3;->this$0:Lepson/scan/activity/MailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 129
    iget-object p1, p0, Lepson/scan/activity/MailActivity$3;->this$0:Lepson/scan/activity/MailActivity;

    invoke-static {p1}, Lepson/scan/activity/MailActivity;->access$000(Lepson/scan/activity/MailActivity;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 130
    iget-object p1, p0, Lepson/scan/activity/MailActivity$3;->this$0:Lepson/scan/activity/MailActivity;

    invoke-static {p1}, Lepson/scan/activity/MailActivity;->access$200(Lepson/scan/activity/MailActivity;)Landroid/widget/Button;

    move-result-object p1

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 131
    iget-object p1, p0, Lepson/scan/activity/MailActivity$3;->this$0:Lepson/scan/activity/MailActivity;

    invoke-static {p1}, Lepson/scan/activity/MailActivity;->access$300(Lepson/scan/activity/MailActivity;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 133
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/MailActivity$3;->this$0:Lepson/scan/activity/MailActivity;

    invoke-static {p1}, Lepson/scan/activity/MailActivity;->access$200(Lepson/scan/activity/MailActivity;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 134
    iget-object p1, p0, Lepson/scan/activity/MailActivity$3;->this$0:Lepson/scan/activity/MailActivity;

    invoke-static {p1}, Lepson/scan/activity/MailActivity;->access$300(Lepson/scan/activity/MailActivity;)Landroid/widget/Button;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

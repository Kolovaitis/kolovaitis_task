.class Lepson/scan/activity/ScanActivity$17;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/scan/activity/ScanActivity;->confirmationDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 2024
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$17;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 2026
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$17;->this$0:Lepson/scan/activity/ScanActivity;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lepson/scan/activity/ScanActivity;->access$3802(Lepson/scan/activity/ScanActivity;Z)Z

    .line 2027
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$17;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "yes pressed, isConfirmCancel = "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lepson/scan/activity/ScanActivity$17;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$3800(Lepson/scan/activity/ScanActivity;)Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2028
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$17;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->isJobDone()Z

    move-result p1

    if-nez p1, :cond_0

    .line 2029
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$17;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4100(Lepson/scan/activity/ScanActivity;)V

    .line 2031
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$17;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object p1

    const/4 p2, -0x2

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.class public Lepson/scan/activity/MailActivity;
.super Lepson/print/ActivityIACommon;
.source "MailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lepson/print/CommonDefine;


# instance fields
.field private chooseFileType:Landroid/widget/RadioGroup;

.field private clearBtn:Landroid/widget/Button;

.field private count:I

.field private emailIntent:Landroid/content/Intent;

.field private fileAttachment:Ljava/io/File;

.field private fileSizeInByte:Ljava/lang/Long;

.field private fileSizeInKb:F

.field private mFileName:Landroid/widget/EditText;

.field private mFileType:Ljava/lang/String;

.field private saveBtn:Landroid/widget/Button;

.field private saveFilePathArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private totalFileSize:F

.field private utils:Lepson/print/Util/Utils;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 39
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const-string v0, "PDF"

    .line 43
    iput-object v0, p0, Lepson/scan/activity/MailActivity;->mFileType:Ljava/lang/String;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 49
    iput v0, p0, Lepson/scan/activity/MailActivity;->totalFileSize:F

    .line 51
    new-instance v0, Lepson/print/Util/Utils;

    invoke-direct {v0}, Lepson/print/Util/Utils;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/MailActivity;->utils:Lepson/print/Util/Utils;

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/MailActivity;)Landroid/widget/EditText;
    .locals 0

    .line 39
    iget-object p0, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$102(Lepson/scan/activity/MailActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 39
    iput-object p1, p0, Lepson/scan/activity/MailActivity;->mFileType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lepson/scan/activity/MailActivity;)Landroid/widget/Button;
    .locals 0

    .line 39
    iget-object p0, p0, Lepson/scan/activity/MailActivity;->clearBtn:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$300(Lepson/scan/activity/MailActivity;)Landroid/widget/Button;
    .locals 0

    .line 39
    iget-object p0, p0, Lepson/scan/activity/MailActivity;->saveBtn:Landroid/widget/Button;

    return-object p0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 234
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-ne p1, p3, :cond_0

    if-nez p2, :cond_0

    const/4 p1, -0x1

    .line 236
    invoke-virtual {p0, p1}, Lepson/scan/activity/MailActivity;->setResult(I)V

    .line 237
    invoke-virtual {p0}, Lepson/scan/activity/MailActivity;->finish()V

    .line 241
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->saveBtn:Landroid/widget/Button;

    invoke-virtual {p1, p3}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0800c4

    if-eq p1, v0, :cond_a

    const v0, 0x7f0802cc

    if-eq p1, v0, :cond_0

    goto/16 :goto_4

    .line 145
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/print/fileBrower;->isAvailableFileName(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x1

    if-nez p1, :cond_1

    .line 146
    invoke-virtual {p0}, Lepson/scan/activity/MailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f0e04b4

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 151
    :cond_1
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->saveBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 156
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileType:Ljava/lang/String;

    const-string v2, "PDF"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 159
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    goto :goto_0

    .line 160
    :cond_2
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EPSON"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lepson/scan/activity/MailActivity;->utils:Lepson/print/Util/Utils;

    iget v4, p0, Lepson/scan/activity/MailActivity;->count:I

    invoke-virtual {v3, v4}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 163
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x1

    goto :goto_0

    .line 164
    :cond_3
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ne p1, v0, :cond_4

    .line 167
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const-string p1, "EPSON"

    const/4 v2, 0x0

    .line 174
    :goto_0
    :try_start_0
    iget-object v3, p0, Lepson/scan/activity/MailActivity;->mFileType:Ljava/lang/String;

    const-string v4, "PDF"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 175
    new-instance v1, Lepson/scan/lib/libHaru;

    invoke-direct {v1}, Lepson/scan/lib/libHaru;-><init>()V

    .line 176
    new-instance v2, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".pdf"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    .line 178
    iget-object v2, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, p1}, Lepson/scan/lib/libHaru;->createPDF(Ljava/util/ArrayList;Ljava/lang/String;)I

    .line 180
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    .line 181
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lepson/scan/activity/MailActivity;->fileAttachment:Ljava/io/File;

    .line 182
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->fileAttachment:Ljava/io/File;

    invoke-static {p0, p1}, Lepson/provider/ScannedFileProvider;->getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    .line 183
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    .line 184
    iget-object v1, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 187
    :cond_5
    iget-object v3, p0, Lepson/scan/activity/MailActivity;->mFileType:Ljava/lang/String;

    const-string v4, "JPEG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 188
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 189
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    .line 190
    iget v4, p0, Lepson/scan/activity/MailActivity;->count:I

    .line 192
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v5

    .line 193
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v6

    invoke-virtual {v6}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object v6

    .line 192
    invoke-virtual {v5, v6}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 195
    iget-object v5, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v5, v0, :cond_6

    .line 196
    iget-object v2, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 197
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".jpg"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, v1, p1}, Lepson/server/utils/MyUtility;->createTempFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/MailActivity;->fileAttachment:Ljava/io/File;

    .line 198
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->fileAttachment:Ljava/io/File;

    invoke-static {p0, p1}, Lepson/provider/ScannedFileProvider;->getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    .line 199
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    if-ne v2, v0, :cond_7

    const/4 v4, 0x1

    .line 205
    :cond_7
    :goto_1
    iget-object v2, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 206
    iget-object v2, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 208
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lepson/scan/activity/MailActivity;->utils:Lepson/print/Util/Utils;

    invoke-virtual {v6, v4}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".jpg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v2, v5}, Lepson/server/utils/MyUtility;->createTempFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lepson/scan/activity/MailActivity;->fileAttachment:Ljava/io/File;

    add-int/lit8 v4, v4, 0x1

    .line 210
    iget-object v2, p0, Lepson/scan/activity/MailActivity;->fileAttachment:Ljava/io/File;

    invoke-static {p0, v2}, Lepson/provider/ScannedFileProvider;->getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 211
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 214
    :cond_8
    :goto_2
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 217
    :cond_9
    :goto_3
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    const-string v1, "message/rfc822"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    const v2, 0x7f0e02ac

    invoke-virtual {p0, v2}, Lepson/scan/activity/MailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    const v2, 0x7f0e0544

    invoke-virtual {p0, v2}, Lepson/scan/activity/MailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->emailIntent:Landroid/content/Intent;

    const-string v1, "Send mail..."

    invoke-static {p1, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/MailActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception p1

    .line 223
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 227
    :cond_a
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 55
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00b1

    .line 56
    invoke-virtual {p0, p1}, Lepson/scan/activity/MailActivity;->setContentView(I)V

    const p1, 0x7f0e0474

    const/4 v0, 0x1

    .line 58
    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/MailActivity;->setActionBar(IZ)V

    const v1, 0x7f0802c4

    .line 60
    invoke-virtual {p0, v1}, Lepson/scan/activity/MailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    .line 61
    new-array v0, v0, [Landroid/text/InputFilter;

    .line 62
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    const/16 v2, 0x40

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 63
    iget-object v1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 65
    iget-object v0, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    new-instance v1, Lepson/scan/activity/MailActivity$1;

    invoke-direct {v1, p0}, Lepson/scan/activity/MailActivity$1;-><init>(Lepson/scan/activity/MailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f080147

    .line 74
    invoke-virtual {p0, v0}, Lepson/scan/activity/MailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lepson/scan/activity/MailActivity;->chooseFileType:Landroid/widget/RadioGroup;

    const v0, 0x7f0802cc

    .line 76
    invoke-virtual {p0, v0}, Lepson/scan/activity/MailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/scan/activity/MailActivity;->saveBtn:Landroid/widget/Button;

    .line 77
    iget-object v0, p0, Lepson/scan/activity/MailActivity;->saveBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 78
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->saveBtn:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0800c4

    .line 80
    invoke-virtual {p0, p1}, Lepson/scan/activity/MailActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/scan/activity/MailActivity;->clearBtn:Landroid/widget/Button;

    .line 81
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->clearBtn:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->chooseFileType:Landroid/widget/RadioGroup;

    const v0, 0x7f0802cb

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 84
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->chooseFileType:Landroid/widget/RadioGroup;

    new-instance v0, Lepson/scan/activity/MailActivity$2;

    invoke-direct {v0, p0}, Lepson/scan/activity/MailActivity$2;-><init>(Lepson/scan/activity/MailActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 96
    invoke-virtual {p0}, Lepson/scan/activity/MailActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "SAVING_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    .line 98
    invoke-static {p0}, Lepson/print/ScanFileNumber;->getCount(Landroid/content/Context;)I

    move-result p1

    iput p1, p0, Lepson/scan/activity/MailActivity;->count:I

    .line 99
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0e033f

    invoke-virtual {p0, v1}, Lepson/scan/activity/MailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/scan/activity/MailActivity;->utils:Lepson/print/Util/Utils;

    iget v3, p0, Lepson/scan/activity/MailActivity;->count:I

    invoke-virtual {v1, v3}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 101
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 102
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lepson/scan/activity/MailActivity;->saveFilePathArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lepson/scan/activity/MailActivity;->fileAttachment:Ljava/io/File;

    .line 103
    iget-object v0, p0, Lepson/scan/activity/MailActivity;->fileAttachment:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/MailActivity;->fileSizeInByte:Ljava/lang/Long;

    .line 104
    iget-object v0, p0, Lepson/scan/activity/MailActivity;->fileSizeInByte:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v3, 0x400

    div-long/2addr v0, v3

    long-to-float v0, v0

    iput v0, p0, Lepson/scan/activity/MailActivity;->fileSizeInKb:F

    const-string v0, "Data"

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lepson/scan/activity/MailActivity;->fileSizeInKb:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget v0, p0, Lepson/scan/activity/MailActivity;->totalFileSize:F

    iget v1, p0, Lepson/scan/activity/MailActivity;->fileSizeInKb:F

    add-float/2addr v0, v1

    iput v0, p0, Lepson/scan/activity/MailActivity;->totalFileSize:F

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 109
    :cond_0
    iget p1, p0, Lepson/scan/activity/MailActivity;->totalFileSize:F

    const/high16 v0, 0x43fa0000    # 500.0f

    const/4 v1, 0x2

    const v3, 0x7f0802c7

    cmpl-float p1, p1, v0

    if-lez p1, :cond_1

    .line 110
    invoke-virtual {p0, v3}, Lepson/scan/activity/MailActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lepson/scan/activity/MailActivity;->totalFileSize:F

    const/high16 v4, 0x44800000    # 1024.0f

    div-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4, v1}, Lepson/server/utils/MyUtility;->mathRound(DI)D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, "MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 112
    :cond_1
    invoke-virtual {p0, v3}, Lepson/scan/activity/MailActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lepson/scan/activity/MailActivity;->totalFileSize:F

    float-to-double v3, v3

    invoke-static {v3, v4, v1}, Lepson/server/utils/MyUtility;->mathRound(DI)D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, "KB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :goto_1
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_2

    .line 115
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->clearBtn:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 117
    :cond_2
    iget-object p1, p0, Lepson/scan/activity/MailActivity;->mFileName:Landroid/widget/EditText;

    new-instance v0, Lepson/scan/activity/MailActivity$3;

    invoke-direct {v0, p0}, Lepson/scan/activity/MailActivity$3;-><init>(Lepson/scan/activity/MailActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.class Lepson/scan/activity/ScanBaseView$ScanAreaSet;
.super Landroid/view/View;
.source "ScanBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScanAreaSet"
.end annotation


# instance fields
.field private _pBaseBottomRight:Landroid/graphics/Point;

.field private _pBaseCenter:Landroid/graphics/Point;

.field private _pBaseTopLeft:Landroid/graphics/Point;

.field _touchtime:I

.field public redrawCBHandler:Landroid/os/Handler;

.field final synthetic this$0:Lepson/scan/activity/ScanBaseView;


# direct methods
.method public constructor <init>(Lepson/scan/activity/ScanBaseView;Landroid/content/Context;)V
    .locals 0

    .line 427
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    .line 428
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 424
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->redrawCBHandler:Landroid/os/Handler;

    const/4 p1, 0x0

    .line 640
    iput p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_touchtime:I

    .line 431
    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    .line 432
    sget p2, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    int-to-float p2, p2

    mul-float p2, p2, p1

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p2, p1

    float-to-int p1, p2

    sput p1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    return-void
.end method

.method private drawCornerBotLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V
    .locals 7

    .line 772
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x6

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x6

    add-int/lit8 v0, v0, 0x3c

    int-to-float v4, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0x6

    int-to-float v5, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 773
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x6

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x3c

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v4, v0

    iget p2, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 p2, p2, 0x6

    int-to-float v5, p2

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawCornerBotLeftMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V
    .locals 7

    .line 790
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x12

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x12

    add-int/lit16 v0, v0, 0x96

    int-to-float v4, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0x12

    int-to-float v5, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 791
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x12

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0x12

    add-int/lit16 v0, v0, -0x96

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v4, v0

    iget p2, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 p2, p2, 0x12

    int-to-float v5, p2

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawCornerBotRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V
    .locals 7

    .line 768
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x3c

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x6

    int-to-float v4, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0x6

    int-to-float v5, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 769
    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x3c

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x6

    int-to-float v4, v0

    iget p2, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 p2, p2, 0x6

    int-to-float v5, p2

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawCornerBotRightMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V
    .locals 7

    .line 786
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x12

    add-int/lit16 v0, v0, -0x96

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x12

    int-to-float v4, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0x12

    int-to-float v5, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 787
    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0x12

    add-int/lit16 v0, v0, -0x96

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x12

    int-to-float v4, v0

    iget p2, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 p2, p2, 0x12

    int-to-float v5, p2

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawCornerTopLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V
    .locals 7

    .line 764
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x6

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0x6

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x6

    add-int/lit8 v0, v0, 0x3c

    int-to-float v4, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    int-to-float v5, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 765
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x6

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0x6

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v4, v0

    iget p2, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 p2, p2, -0x6

    add-int/lit8 p2, p2, 0x3c

    int-to-float v5, p2

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawCornerTopLeftMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V
    .locals 7

    .line 782
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x12

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0x12

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x12

    add-int/lit16 v0, v0, 0x96

    int-to-float v4, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    int-to-float v5, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 783
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0x12

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0x12

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v4, v0

    iget p2, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 p2, p2, -0x12

    add-int/lit16 p2, p2, 0x96

    int-to-float v5, p2

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawCornerTopRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V
    .locals 7

    .line 776
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x3c

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0x6

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x6

    int-to-float v4, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    int-to-float v5, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 777
    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0x6

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x6

    int-to-float v4, v0

    iget p2, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 p2, p2, -0x6

    add-int/lit8 p2, p2, 0x3c

    int-to-float v5, p2

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawCornerTopRightMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V
    .locals 7

    .line 794
    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x12

    add-int/lit16 v0, v0, -0x96

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0x12

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x12

    int-to-float v4, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    int-to-float v5, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 795
    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0x12

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0x12

    int-to-float v4, v0

    iget p2, p2, Landroid/graphics/Point;->y:I

    add-int/lit8 p2, p2, -0x12

    add-int/lit16 p2, p2, 0x96

    int-to-float v5, p2

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawNormalCorner(Landroid/graphics/Canvas;)V
    .locals 1

    .line 742
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView;->access$400(Lepson/scan/activity/ScanBaseView;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 757
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v0, v0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 758
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v0, v0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 752
    :pswitch_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v0, v0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 753
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v0, v0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 748
    :pswitch_1
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v0, v0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 749
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v0, v0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 744
    :pswitch_2
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v0, v0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 745
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v0, v0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized drawScanArea(Landroid/graphics/Canvas;)V
    .locals 11

    monitor-enter p0

    .line 467
    :try_start_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmTop()Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    .line 468
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmBot()Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    .line 470
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView;->access$200(Lepson/scan/activity/ScanBaseView;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 471
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getBm()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 472
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    const/4 v3, 0x0

    if-lt v0, v2, :cond_0

    const-string v0, "drawScanArea"

    const-string v2, "14"

    .line 477
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getBm()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 479
    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v2}, Lepson/scan/activity/ScanBaseView;->getBmRectF()Landroid/graphics/RectF;

    move-result-object v2

    .line 480
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v0, v4, v2, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 481
    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v2, v0}, Lepson/scan/activity/ScanBaseView;->setBm(Landroid/graphics/Bitmap;)V

    .line 483
    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v2}, Lepson/scan/activity/ScanBaseView;->getBmRectF()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v4}, Lepson/scan/activity/ScanBaseView;->getBmRectF()Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, v0, v2, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 489
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getBm()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v2}, Lepson/scan/activity/ScanBaseView;->getBmRectF()Landroid/graphics/RectF;

    move-result-object v2

    iget-object v4, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v4}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v0, v3, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 494
    :cond_1
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-boolean v0, v0, Lepson/scan/activity/ScanBaseView;->isDoctable:Z

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    .line 495
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView;->access$300(Lepson/scan/activity/ScanBaseView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 497
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawNormalCorner(Landroid/graphics/Canvas;)V

    .line 498
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmCenter()Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    .line 502
    :cond_2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 503
    iget-object v3, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-boolean v3, v3, Lepson/scan/activity/ScanBaseView;->bLongTouch:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget v3, v3, Lepson/scan/activity/ScanBaseView;->mWhere:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    .line 505
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 506
    iget-object v5, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v6, v5

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v7, v5

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v8, v5

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v9, v5

    sget-object v10, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object v5, v3

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 507
    new-instance v5, Landroid/graphics/DashPathEffect;

    new-array v4, v4, [F

    const/high16 v6, 0x41200000    # 10.0f

    aput v6, v4, v2

    const/high16 v6, 0x40000000    # 2.0f

    aput v6, v4, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget v1, v1, Lepson/scan/activity/ScanBaseView;->phase:I

    int-to-float v1, v1

    invoke-direct {v5, v4, v1}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 508
    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    const/high16 v1, 0x3fc00000    # 1.5f

    .line 509
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 510
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 511
    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 515
    :cond_3
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 517
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v5, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v6, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v7, v1

    move-object v3, p1

    move-object v8, v0

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 520
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v5, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v6, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v7, v1

    move-object v3, p1

    move-object v8, v0

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 523
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v5, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v6, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v7, v1

    move-object v3, p1

    move-object v8, v0

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 526
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v5, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v6, v1

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v7, v1

    move-object v3, p1

    move-object v8, v0

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 530
    :goto_1
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-boolean v0, v0, Lepson/scan/activity/ScanBaseView;->bLongTouch:Z

    if-eqz v0, :cond_4

    .line 531
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawTheCornerImage(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 533
    :cond_4
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawNormalCorner(Landroid/graphics/Canvas;)V

    .line 537
    :cond_5
    :goto_2
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-static {p1}, Lepson/scan/activity/ScanBaseView;->access$300(Lepson/scan/activity/ScanBaseView;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 538
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p1}, Lepson/scan/activity/ScanBaseView;->setupScanArea()V

    .line 539
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p1, v2}, Lepson/scan/activity/ScanBaseView;->setSetSize(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    :cond_6
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private drawTheCornerImage(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5

    .line 544
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-static {p2}, Lepson/scan/activity/ScanBaseView;->access$400(Lepson/scan/activity/ScanBaseView;)I

    move-result p2

    const/4 v0, 0x1

    const/4 v1, -0x1

    packed-switch p2, :pswitch_data_0

    goto/16 :goto_0

    .line 596
    :pswitch_0
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget p2, p2, Lepson/scan/activity/ScanBaseView;->mWhere:I

    if-eq p2, v1, :cond_1

    if-eq p2, v0, :cond_0

    .line 606
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 607
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto/16 :goto_0

    .line 602
    :cond_0
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 603
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotLeftMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto/16 :goto_0

    .line 598
    :cond_1
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopRightMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 599
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto/16 :goto_0

    .line 579
    :pswitch_1
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget p2, p2, Lepson/scan/activity/ScanBaseView;->mWhere:I

    if-eq p2, v1, :cond_3

    if-eq p2, v0, :cond_2

    .line 589
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 590
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 585
    :cond_2
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 586
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopLeftMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 581
    :cond_3
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotRightMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 582
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 562
    :pswitch_2
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget p2, p2, Lepson/scan/activity/ScanBaseView;->mWhere:I

    if-eq p2, v1, :cond_5

    if-eq p2, v0, :cond_4

    .line 572
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 573
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 568
    :cond_4
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 569
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopRightMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 564
    :cond_5
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotLeftMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 565
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 546
    :pswitch_3
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget p2, p2, Lepson/scan/activity/ScanBaseView;->mWhere:I

    if-eq p2, v1, :cond_7

    if-eq p2, v0, :cond_6

    .line 556
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 557
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 552
    :cond_6
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopLeft(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 553
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotRightMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    goto :goto_0

    .line 548
    :cond_7
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerTopLeftMoving(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 549
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawCornerBotRight(Landroid/graphics/Canvas;Landroid/graphics/Point;)V

    .line 617
    :goto_0
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-boolean p2, p2, Lepson/scan/activity/ScanBaseView;->bLongTouch:Z

    if-eqz p2, :cond_b

    .line 618
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getLocalClassName()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mWhere = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget v1, v1, Lepson/scan/activity/ScanBaseView;->mWhere:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget p2, p2, Lepson/scan/activity/ScanBaseView;->mWhere:I

    if-nez p2, :cond_8

    .line 620
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object p2, p2, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    if-nez p2, :cond_8

    .line 621
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070059

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-static {v2}, Lepson/scan/activity/ScanBaseView;->access$500(Lepson/scan/activity/ScanBaseView;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p2, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    .line 628
    :cond_8
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object p2, p2, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    if-eqz p2, :cond_b

    .line 629
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    if-nez p2, :cond_9

    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p2}, Lepson/scan/activity/ScanBaseView;->getmCenter()Landroid/graphics/Point;

    move-result-object p2

    iput-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    .line 630
    :cond_9
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-static {p2}, Lepson/scan/activity/ScanBaseView;->access$600(Lepson/scan/activity/ScanBaseView;)I

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_a

    .line 631
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object p2, p2, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v2, v2, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v3}, Lepson/scan/activity/ScanBaseView;->getInSampleSize()I

    move-result v3

    mul-int v2, v2, v3

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v3, v3, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v4}, Lepson/scan/activity/ScanBaseView;->getInSampleSize()I

    move-result v4

    mul-int v3, v3, v4

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 633
    :cond_a
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object p2, p2, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v2, v2, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v3, v3, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_b
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private moveCenter()V
    .locals 5

    .line 721
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmFirstTouch()Landroid/graphics/Point;

    move-result-object v0

    .line 722
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v1}, Lepson/scan/activity/ScanBaseView;->getmTouch()Landroid/graphics/Point;

    move-result-object v1

    .line 724
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    .line 725
    iget v3, v1, Landroid/graphics/Point;->y:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v0

    .line 727
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v4, v2

    iput v4, v0, Landroid/graphics/Point;->x:I

    .line 728
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v4, v3

    iput v4, v0, Landroid/graphics/Point;->y:I

    .line 730
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v4, v2

    iput v4, v0, Landroid/graphics/Point;->x:I

    .line 731
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v4, v3

    iput v4, v0, Landroid/graphics/Point;->y:I

    .line 733
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v4, v2

    iput v4, v0, Landroid/graphics/Point;->x:I

    .line 734
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    iget v2, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Point;->y:I

    .line 737
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanBaseView;->setmFirstTouch(Landroid/graphics/Point;)V

    return-void
.end method


# virtual methods
.method public moveTheTouchPoint()V
    .locals 14

    .line 689
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmWhere()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 702
    :pswitch_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmTouch()Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    .line 703
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Lepson/scan/activity/ScanBaseView;->calculateTheCenterPoint(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    goto :goto_0

    .line 697
    :pswitch_1
    invoke-direct {p0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->moveCenter()V

    .line 698
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanBaseView;->setmCenter(Landroid/graphics/Point;)V

    goto :goto_0

    .line 692
    :pswitch_2
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmTouch()Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    .line 693
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Lepson/scan/activity/ScanBaseView;->calculateTheCenterPoint(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    .line 710
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanBaseView;->setmTop(Landroid/graphics/Point;)V

    .line 711
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanBaseView;->setmBot(Landroid/graphics/Point;)V

    .line 713
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    new-instance v1, Lepson/scan/activity/ScanBaseView$PointInfo;

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v4, v2

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseTopLeft:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-double v6, v2

    move-object v2, v1

    move-object v3, v0

    invoke-direct/range {v2 .. v7}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanBaseView;->setPiBaseTop(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 714
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    new-instance v1, Lepson/scan/activity/ScanBaseView$PointInfo;

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v10, v2

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseBottomRight:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-double v12, v2

    move-object v8, v1

    move-object v9, v0

    invoke-direct/range {v8 .. v13}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanBaseView;->setPiBaseBot(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 716
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_pBaseCenter:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanBaseView;->setmCenter(Landroid/graphics/Point;)V

    return-void

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 445
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 446
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-boolean v0, v0, Lepson/scan/activity/ScanBaseView;->bLongTouch:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget v0, v0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 448
    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->invalidate()V

    .line 449
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget v2, v0, Lepson/scan/activity/ScanBaseView;->phase:I

    add-int/2addr v2, v1

    iput v2, v0, Lepson/scan/activity/ScanBaseView;->phase:I

    .line 450
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget v0, v0, Lepson/scan/activity/ScanBaseView;->phase:I

    const/16 v1, 0x3e8

    if-le v0, v1, :cond_0

    .line 452
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    const/4 v1, 0x0

    iput v1, v0, Lepson/scan/activity/ScanBaseView;->phase:I

    .line 455
    :cond_0
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->drawScanArea(Landroid/graphics/Canvas;)V

    .line 460
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->redrawCBHandler:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const/16 v0, -0x64

    .line 461
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 437
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 645
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-boolean v0, v0, Lepson/scan/activity/ScanBaseView;->isDoctable:Z

    if-eqz v0, :cond_1

    .line 646
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 647
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v1, v0}, Lepson/scan/activity/ScanBaseView;->setmTouch(Landroid/graphics/Point;)V

    .line 648
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v1}, Lepson/scan/activity/ScanBaseView;->detectTheScreenStatus()V

    .line 649
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v1, 0x2

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    return v2

    .line 660
    :pswitch_0
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iget-boolean p1, p1, Lepson/scan/activity/ScanBaseView;->bLongTouch:Z

    if-nez p1, :cond_0

    goto :goto_0

    .line 662
    :cond_0
    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->moveTheTouchPoint()V

    goto :goto_0

    .line 666
    :pswitch_1
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p1, v1}, Lepson/scan/activity/ScanBaseView;->setmWhere(I)V

    .line 667
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lepson/scan/activity/ScanBaseView;->setBmCenter(Landroid/graphics/Bitmap;)V

    .line 668
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iput v2, p1, Lepson/scan/activity/ScanBaseView;->phase:I

    .line 669
    iput v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->_touchtime:I

    .line 670
    invoke-virtual {p1}, Lepson/scan/activity/ScanBaseView;->setupScanArea()V

    .line 672
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    iput-boolean v2, p1, Lepson/scan/activity/ScanBaseView;->bLongTouch:Z

    goto :goto_0

    .line 651
    :pswitch_2
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p1, v1}, Lepson/scan/activity/ScanBaseView;->setmWhere(I)V

    .line 654
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p1}, Lepson/scan/activity/ScanBaseView;->detectTheTouchPoint()V

    .line 655
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {p1, v0}, Lepson/scan/activity/ScanBaseView;->setmFirstTouch(Landroid/graphics/Point;)V

    .line 678
    :goto_0
    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    :cond_1
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

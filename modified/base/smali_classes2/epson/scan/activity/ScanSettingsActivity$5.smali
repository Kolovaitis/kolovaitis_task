.class Lepson/scan/activity/ScanSettingsActivity$5;
.super Ljava/lang/Object;
.source "ScanSettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSettingsActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSettingsActivity;)V
    .locals 0

    .line 446
    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$5;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 448
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$5;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsActivity;->access$100(Lepson/scan/activity/ScanSettingsActivity;)Lepson/scan/lib/ScannerInfo;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$5;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsActivity;->access$300(Lepson/scan/activity/ScanSettingsActivity;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$5;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsActivity;->access$300(Lepson/scan/activity/ScanSettingsActivity;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 449
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$5;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    const-string v0, "SCAN_SETTINGS_DETAIL_TITLE"

    const v1, 0x7f0e050f

    .line 450
    invoke-static {p1}, Lepson/scan/activity/ScanSettingsActivity;->access$100(Lepson/scan/activity/ScanSettingsActivity;)Lepson/scan/lib/ScannerInfo;

    move-result-object v2

    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getResolutionValue()I

    move-result v2

    const/4 v3, 0x6

    .line 449
    invoke-static {p1, v0, v1, v2, v3}, Lepson/scan/activity/ScanSettingsActivity;->access$500(Lepson/scan/activity/ScanSettingsActivity;Ljava/lang/String;III)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

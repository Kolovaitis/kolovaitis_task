.class Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;
.super Landroid/os/AsyncTask;
.source "ScanSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetScannerCapabilityAndFinishTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mCancelRequested:Z

.field private mCancelablePropertyTaker:Lepson/scan/lib/CancelablePropertyTaker;

.field private mEscanErrorCode:Lepson/scan/lib/EscanLibException;

.field private mEscanLib:Lepson/scan/lib/escanLib;

.field private mMyPrinter:Lepson/print/MyPrinter;

.field private mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

.field final synthetic this$0:Lepson/scan/activity/ScanSearchActivity;


# direct methods
.method public constructor <init>(Lepson/scan/activity/ScanSearchActivity;Lepson/scan/lib/escanLib;Lepson/print/MyPrinter;)V
    .locals 0

    .line 1388
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1389
    iput-object p2, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 1390
    iput-object p3, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mMyPrinter:Lepson/print/MyPrinter;

    const/4 p1, 0x0

    .line 1391
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mEscanErrorCode:Lepson/scan/lib/EscanLibException;

    return-void
.end method


# virtual methods
.method cancelTask()V
    .locals 1

    const/4 v0, 0x1

    .line 1470
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mCancelRequested:Z

    .line 1471
    monitor-enter p0

    .line 1472
    :try_start_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mCancelablePropertyTaker:Lepson/scan/lib/CancelablePropertyTaker;

    if-eqz v0, :cond_0

    .line 1473
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mCancelablePropertyTaker:Lepson/scan/lib/CancelablePropertyTaker;

    invoke-virtual {v0}, Lepson/scan/lib/CancelablePropertyTaker;->cancel()V

    .line 1475
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .line 1400
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1200(Lepson/scan/activity/ScanSearchActivity;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 1402
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x1

    .line 1407
    :try_start_0
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 1408
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanSearchActivity;->access$1200(Lepson/scan/activity/ScanSearchActivity;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mCancelRequested:Z

    if-eqz v1, :cond_1

    goto :goto_1

    .line 1412
    :cond_1
    monitor-enter p0
    :try_end_0
    .catch Lepson/scan/lib/EscanLibException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1413
    :try_start_1
    new-instance v1, Lepson/scan/lib/CancelablePropertyTaker;

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-direct {v1, v2}, Lepson/scan/lib/CancelablePropertyTaker;-><init>(Lepson/scan/lib/escanLib;)V

    iput-object v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mCancelablePropertyTaker:Lepson/scan/lib/CancelablePropertyTaker;

    .line 1414
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1416
    :try_start_2
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mCancelablePropertyTaker:Lepson/scan/lib/CancelablePropertyTaker;

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mMyPrinter:Lepson/print/MyPrinter;

    invoke-virtual {v1, v2}, Lepson/scan/lib/CancelablePropertyTaker;->getScannerProperty(Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object v1

    iput-object v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 1418
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanSearchActivity;->access$1200(Lepson/scan/activity/ScanSearchActivity;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mCancelRequested:Z

    if-eqz v1, :cond_2

    goto :goto_0

    .line 1421
    :cond_2
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    if-nez v1, :cond_3

    .line 1422
    new-instance v1, Lepson/scan/lib/EscanLibException;

    const/16 v2, -0x514

    invoke-direct {v1, p1, v2}, Lepson/scan/lib/EscanLibException;-><init>(II)V

    iput-object v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mEscanErrorCode:Lepson/scan/lib/EscanLibException;

    .line 1423
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1
    :try_end_2
    .catch Lepson/scan/lib/EscanLibException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1437
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$2100(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1438
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-object p1

    .line 1427
    :cond_3
    :try_start_3
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    goto :goto_2

    .line 1419
    :cond_4
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1
    :try_end_3
    .catch Lepson/scan/lib/EscanLibException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1437
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$2100(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1438
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-object p1

    :catchall_0
    move-exception v1

    .line 1414
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v1

    .line 1409
    :cond_5
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1
    :try_end_5
    .catch Lepson/scan/lib/EscanLibException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1437
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$2100(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1438
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-object p1

    :catchall_1
    move-exception p1

    goto :goto_3

    .line 1437
    :catch_0
    :goto_2
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$2100(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1438
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1441
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :catch_1
    move-exception p1

    .line 1431
    :try_start_6
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mEscanErrorCode:Lepson/scan/lib/EscanLibException;

    .line 1432
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1437
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$2100(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1438
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-object p1

    .line 1437
    :goto_3
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$2100(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1438
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    throw p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1376
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    .line 1446
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanSearchActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1449
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$2200(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1451
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    .line 1453
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$2300(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1454
    iget-boolean p1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mCancelRequested:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mEscanErrorCode:Lepson/scan/lib/EscanLibException;

    if-eqz p1, :cond_1

    .line 1456
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-virtual {v0, p1}, Lepson/scan/activity/ScanSearchActivity;->showErrorDialog(Lepson/scan/lib/EscanLibException;)V

    :cond_1
    return-void

    .line 1461
    :cond_2
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-static {p1, v0}, Lepson/scan/activity/ScanSearchActivity;->access$2400(Lepson/scan/activity/ScanSearchActivity;Lepson/scan/activity/ScannerPropertyWrapper;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1376
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 1466
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$2500(Lepson/scan/activity/ScanSearchActivity;)V

    return-void
.end method

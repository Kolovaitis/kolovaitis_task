.class Lepson/scan/activity/ScanBaseView$ScanAreaBackground;
.super Landroid/view/View;
.source "ScanBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScanAreaBackground"
.end annotation


# instance fields
.field _paint:Landroid/graphics/Paint;

.field _textPaint:Landroid/graphics/Paint;

.field hideText:Z

.field mScanActivity:Lepson/scan/activity/ScanActivity;

.field text:Ljava/lang/String;

.field final synthetic this$0:Lepson/scan/activity/ScanBaseView;


# direct methods
.method public constructor <init>(Lepson/scan/activity/ScanBaseView;Landroid/content/Context;)V
    .locals 3

    .line 1019
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    .line 1020
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1021
    check-cast p2, Lepson/scan/activity/ScanActivity;

    iput-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->mScanActivity:Lepson/scan/activity/ScanActivity;

    .line 1022
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_paint:Landroid/graphics/Paint;

    .line 1023
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_paint:Landroid/graphics/Paint;

    const/4 v0, -0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1024
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_paint:Landroid/graphics/Paint;

    const v0, 0x7f05007b

    const/high16 v1, 0x40c00000    # 6.0f

    const/high16 v2, 0x40b00000    # 5.5f

    invoke-virtual {p2, v2, v1, v1, v0}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1026
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    .line 1027
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1028
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1029
    iget-object p2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1030
    new-instance p2, Landroid/widget/EditText;

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Landroid/widget/EditText;->getTextSize()F

    move-result p2

    .line 1031
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    const p2, 0x7f0e04e0

    .line 1032
    invoke-virtual {p1, p2}, Lepson/scan/activity/ScanBaseView;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->text:Ljava/lang/String;

    return-void
.end method

.method private drawTheDefaultBackground(Landroid/graphics/Canvas;)V
    .locals 7

    .line 1048
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v5, v0

    iget-object v6, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_paint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1049
    iget-boolean v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->hideText:Z

    if-nez v0, :cond_0

    .line 1050
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->drawTextInCanvas(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method


# virtual methods
.method drawTextInCanvas(Landroid/graphics/Canvas;)V
    .locals 12

    .line 973
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 974
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v1}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v2}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    const v2, 0x3f666666    # 0.9f

    mul-float v1, v1, v2

    .line 975
    iget-object v2, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v2}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v3}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 976
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 977
    iget-object v4, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->text:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    move-object v7, v0

    const/4 v0, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 986
    iget-object v8, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    iget-object v9, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->text:Ljava/lang/String;

    invoke-virtual {v9, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v8, v9, v11, v1, v10}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v8

    .line 987
    iget-object v9, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->text:Ljava/lang/String;

    add-int/2addr v8, v0

    invoke-virtual {v9, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 989
    iget-object v9, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9, v0, v5, v10, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 990
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v9

    add-int/2addr v6, v9

    .line 992
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 993
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move v0, v8

    goto :goto_0

    .line 1000
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v1}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v4}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v4

    int-to-float v1, v1

    const v4, 0x3d4ccccd    # 0.05f

    mul-float v1, v1, v4

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 1001
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-virtual {v1}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    div-int/lit8 v6, v6, 0x2

    int-to-float v2, v6

    sub-float/2addr v1, v2

    float-to-int v1, v1

    const-string v2, "\n"

    .line 1007
    invoke-virtual {v7, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 1008
    :goto_1
    array-length v7, v2

    if-ge v4, v7, :cond_1

    .line 1009
    aget-object v7, v2, v4

    int-to-float v8, v0

    add-int v9, v1, v6

    int-to-float v9, v9

    iget-object v10, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1010
    iget-object v7, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->_textPaint:Landroid/graphics/Paint;

    aget-object v8, v2, v4

    aget-object v9, v2, v4

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v7, v8, v5, v9, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1011
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/2addr v6, v7

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public hideText(Z)V
    .locals 0

    .line 1016
    iput-boolean p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->hideText:Z

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .line 1037
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1043
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->drawTheDefaultBackground(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 967
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 969
    iget-object p1, p0, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->mScanActivity:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->onScanAreaSizeChange()V

    return-void
.end method

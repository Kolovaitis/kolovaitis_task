.class Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;
.super Ljava/lang/Object;
.source "ScanSettingsAdvanceDetailActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const-string p1, "mListOptionClick2"

    .line 197
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "[position]"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "mListOptionClick2"

    .line 198
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "[id]"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p1, p3}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$502(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;I)I

    const p1, 0x7f07013c

    const/4 p2, 0x1

    const p4, 0x7f070145

    const p5, 0x7f070138

    const/4 v0, 0x0

    packed-switch p3, :pswitch_data_0

    goto/16 :goto_0

    .line 208
    :pswitch_0
    iget-object p3, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$600(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p3

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-virtual {v1, p5}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p5

    invoke-virtual {p3, v0, p5}, Lepson/common/CustomListRowAdapter;->setList(ILepson/common/CustomListRow;)V

    .line 209
    iget-object p3, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$600(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p3

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object p5

    const-string v0, ""

    invoke-virtual {p5, v0}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object p5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p5, p1}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {p1, p4}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p1

    invoke-virtual {p3, p2, p1}, Lepson/common/CustomListRowAdapter;->setList(ILepson/common/CustomListRow;)V

    goto :goto_0

    .line 204
    :pswitch_1
    iget-object p3, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$600(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p3

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-virtual {v1, p5}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p5

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {p5, p4}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p4

    invoke-virtual {p3, v0, p4}, Lepson/common/CustomListRowAdapter;->setList(ILepson/common/CustomListRow;)V

    .line 205
    iget-object p3, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$600(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p3

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object p4

    const-string p5, ""

    invoke-virtual {p4, p5}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object p4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p4, p1}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p1

    invoke-virtual {p3, p2, p1}, Lepson/common/CustomListRowAdapter;->setList(ILepson/common/CustomListRow;)V

    .line 216
    :goto_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->access$600(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/CustomListRowAdapter;->notifyDataSetChanged()V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

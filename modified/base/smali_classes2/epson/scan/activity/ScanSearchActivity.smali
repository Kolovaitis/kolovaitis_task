.class public Lepson/scan/activity/ScanSearchActivity;
.super Lepson/print/ActivityIACommon;
.source "ScanSearchActivity.java"

# interfaces
.implements Lepson/print/CustomTitleDialogFragment$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;,
        Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;,
        Lepson/scan/activity/ScanSearchActivity$FindScannerTask;
    }
.end annotation


# static fields
.field private static final DIALOG_ID_PRINTER_NOT_FOUND_WITH_WEB_GUIDANCE:I = 0x1

.field private static final DIALOG_TAG_PRINTER_NOT_FOUND:Ljava/lang/String; = "printer_not_found_dialog"

.field private static final Menu_Delete:I = 0x1

.field private static final Menu_Edit:I = 0x2

.field private static final PARAM_KEY_SCANNER_ID:Ljava/lang/String; = "scanner-id"

.field private static final PARAM_KEY_SCANNER_LOCATION:Ljava/lang/String; = "scanner-location"

.field private static final PARAM_KEY_SCANNER_SSID:Ljava/lang/String; = "scanner-ssid"

.field private static final TAG:Ljava/lang/String; = "ScanSearch"

.field private static final sEscanLibSemaphore:Ljava/util/concurrent/Semaphore;


# instance fields
.field private final ADD_SCANER:I

.field private final COMMONDEVICENAME:Ljava/lang/String;

.field private final CONNECT_SIMPLEAP:I

.field private final DELAY:I

.field private final DELETE_PRINTER:I

.field private final EDIT_IPPRINTER:I

.field private final FINISH_FIND_SCANER:I

.field private final FOUND_SIMPLEAP:I

.field private final PRINTER_ID:Ljava/lang/String;

.field private final PRINTER_IP:Ljava/lang/String;

.field private final PRINTER_NAME:Ljava/lang/String;

.field private final PROBE_SCANNER:I

.field private final SCANER_ID:Ljava/lang/String;

.field private final SCANER_INDEX:Ljava/lang/String;

.field private final SCANER_IP:Ljava/lang/String;

.field private final SCANER_LOCATION:Ljava/lang/String;

.field private final SCANER_NAME:Ljava/lang/String;

.field private final SEARCH_PRINTER_P2P:I

.field private final SEARCH_SCANER:I

.field private final SELECT_SCANER:I

.field private bCheckWiFiStatus:Z

.field private connectionInfo:Ljava/lang/String;

.field private errorDialog:Landroid/app/AlertDialog;

.field helper:Lepson/print/widgets/ListControlHelper;

.field private volatile isFinishSearchScanner:Z

.field private isFocused:Ljava/lang/Boolean;

.field private isJapaneseLocale:Z

.field private volatile isSearchSimpleAp:Z

.field private isSelected:Z

.field private listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

.field private volatile mActivityBackRequested:Z

.field private mActivityForegroundLifetime:Z

.field mAddButton:Landroid/widget/Button;

.field mBuilder:Lepson/scan/activity/ScannerListAdapter;

.field private mContext:Landroid/content/Context;

.field mDeletePos:I

.field private mEscanLib:Lepson/scan/lib/escanLib;

.field private mFindScannerTask:Lepson/scan/activity/ScanSearchActivity$FindScannerTask;

.field private mGetScannerCapabilityAndFinishTask:Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;

.field private mHandler:Landroid/os/Handler;

.field mIpButton:Landroid/widget/RadioButton;

.field mLayout:Landroid/view/ViewGroup;

.field private mListEmptyMessageTextView:Landroid/widget/TextView;

.field private mListView:Landroid/widget/ListView;

.field mLocalButton:Landroid/widget/RadioButton;

.field private mMyPrinterForSimpleApDisconnect:Ljava/lang/String;

.field private volatile mProgress:Z

.field mProgressBar:Landroid/widget/ProgressBar;

.field mProgressDialog:Lepson/print/screen/WorkingDialog;

.field private volatile mScannerFindCancelRequested:Z

.field mSearchButton:Landroid/widget/Button;

.field private mSimpleApSsid:Ljava/lang/String;

.field mTextDetail:Landroid/widget/TextView;

.field mWiFiSettingButton:Landroid/view/View;

.field private scannerId:Ljava/lang/String;

.field private scannerKey:Ljava/lang/String;

.field private scanner_location:I

.field private searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

.field private tvResult:Landroid/widget/TextView;

.field wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 151
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lepson/scan/activity/ScanSearchActivity;->sEscanLibSemaphore:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 65
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 116
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    .line 119
    iput-boolean v1, p0, Lepson/scan/activity/ScanSearchActivity;->isJapaneseLocale:Z

    const/4 v2, 0x1

    .line 121
    iput-boolean v2, p0, Lepson/scan/activity/ScanSearchActivity;->isFinishSearchScanner:Z

    .line 123
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 124
    iput-boolean v1, p0, Lepson/scan/activity/ScanSearchActivity;->bCheckWiFiStatus:Z

    .line 125
    iput-boolean v1, p0, Lepson/scan/activity/ScanSearchActivity;->isSearchSimpleAp:Z

    .line 128
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->connectionInfo:Ljava/lang/String;

    .line 131
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    .line 132
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->helper:Lepson/print/widgets/ListControlHelper;

    .line 273
    iput-boolean v1, p0, Lepson/scan/activity/ScanSearchActivity;->isSelected:Z

    .line 274
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->isFocused:Ljava/lang/Boolean;

    const-string v0, "name"

    .line 593
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->SCANER_NAME:Ljava/lang/String;

    const-string v0, "ip"

    .line 594
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->SCANER_IP:Ljava/lang/String;

    const-string v0, "id"

    .line 595
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->SCANER_ID:Ljava/lang/String;

    const-string v0, "index"

    .line 596
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->SCANER_INDEX:Ljava/lang/String;

    const-string v0, "location"

    .line 597
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->SCANER_LOCATION:Ljava/lang/String;

    const-string v0, "commonDeviceName"

    .line 598
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->COMMONDEVICENAME:Ljava/lang/String;

    .line 599
    iput v2, p0, Lepson/scan/activity/ScanSearchActivity;->SEARCH_SCANER:I

    const/4 v0, 0x4

    .line 600
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->SELECT_SCANER:I

    const/4 v0, 0x5

    .line 601
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->FINISH_FIND_SCANER:I

    const/4 v0, 0x6

    .line 602
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->FOUND_SIMPLEAP:I

    const/4 v0, 0x7

    .line 603
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->CONNECT_SIMPLEAP:I

    const/16 v0, 0x8

    .line 604
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->ADD_SCANER:I

    const/16 v0, 0x9

    .line 605
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->DELETE_PRINTER:I

    const/16 v0, 0xa

    .line 606
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->EDIT_IPPRINTER:I

    const/16 v0, 0xb

    .line 607
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->PROBE_SCANNER:I

    const/16 v0, 0xc

    .line 608
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->SEARCH_PRINTER_P2P:I

    const/16 v0, 0x64

    .line 610
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->DELAY:I

    .line 612
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$11;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$11;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    const-string v0, "name"

    .line 765
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->PRINTER_NAME:Ljava/lang/String;

    const-string v0, "ip"

    .line 766
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->PRINTER_IP:Ljava/lang/String;

    const-string v0, "id"

    .line 767
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->PRINTER_ID:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->startWifiPrinterSelect()V

    return-void
.end method

.method static synthetic access$100(Lepson/scan/activity/ScanSearchActivity;)Z
    .locals 0

    .line 65
    iget-boolean p0, p0, Lepson/scan/activity/ScanSearchActivity;->isSelected:Z

    return p0
.end method

.method static synthetic access$1000(Lepson/scan/activity/ScanSearchActivity;)Landroid/os/Handler;
    .locals 0

    .line 65
    iget-object p0, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$102(Lepson/scan/activity/ScanSearchActivity;Z)Z
    .locals 0

    .line 65
    iput-boolean p1, p0, Lepson/scan/activity/ScanSearchActivity;->isSelected:Z

    return p1
.end method

.method static synthetic access$1100(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->buildElements()V

    return-void
.end method

.method static synthetic access$1200(Lepson/scan/activity/ScanSearchActivity;)Z
    .locals 0

    .line 65
    iget-boolean p0, p0, Lepson/scan/activity/ScanSearchActivity;->mActivityBackRequested:Z

    return p0
.end method

.method static synthetic access$1300(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->search()V

    return-void
.end method

.method static synthetic access$1400(Lepson/scan/activity/ScanSearchActivity;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
    .locals 0

    .line 65
    iget-object p0, p0, Lepson/scan/activity/ScanSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    return-object p0
.end method

.method static synthetic access$1402(Lepson/scan/activity/ScanSearchActivity;Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
    .locals 0

    .line 65
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    return-object p1
.end method

.method static synthetic access$1500(Lepson/scan/activity/ScanSearchActivity;)Z
    .locals 0

    .line 65
    iget-boolean p0, p0, Lepson/scan/activity/ScanSearchActivity;->isSearchSimpleAp:Z

    return p0
.end method

.method static synthetic access$1502(Lepson/scan/activity/ScanSearchActivity;Z)Z
    .locals 0

    .line 65
    iput-boolean p1, p0, Lepson/scan/activity/ScanSearchActivity;->isSearchSimpleAp:Z

    return p1
.end method

.method static synthetic access$1600(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->updateFindScannerResult()V

    return-void
.end method

.method static synthetic access$1700(Lepson/scan/activity/ScanSearchActivity;)Z
    .locals 0

    .line 65
    iget-boolean p0, p0, Lepson/scan/activity/ScanSearchActivity;->isFinishSearchScanner:Z

    return p0
.end method

.method static synthetic access$1702(Lepson/scan/activity/ScanSearchActivity;Z)Z
    .locals 0

    .line 65
    iput-boolean p1, p0, Lepson/scan/activity/ScanSearchActivity;->isFinishSearchScanner:Z

    return p1
.end method

.method static synthetic access$1800(Lepson/scan/activity/ScanSearchActivity;)Z
    .locals 0

    .line 65
    iget-boolean p0, p0, Lepson/scan/activity/ScanSearchActivity;->mScannerFindCancelRequested:Z

    return p0
.end method

.method static synthetic access$1900()Ljava/util/concurrent/Semaphore;
    .locals 1

    .line 65
    sget-object v0, Lepson/scan/activity/ScanSearchActivity;->sEscanLibSemaphore:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$200(Lepson/scan/activity/ScanSearchActivity;)Ljava/lang/Boolean;
    .locals 0

    .line 65
    iget-object p0, p0, Lepson/scan/activity/ScanSearchActivity;->isFocused:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$2000(Lepson/scan/activity/ScanSearchActivity;)Lepson/scan/lib/escanLib;
    .locals 0

    .line 65
    iget-object p0, p0, Lepson/scan/activity/ScanSearchActivity;->mEscanLib:Lepson/scan/lib/escanLib;

    return-object p0
.end method

.method static synthetic access$202(Lepson/scan/activity/ScanSearchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 65
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->isFocused:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$2100(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->disconnectSimpleAp()V

    return-void
.end method

.method static synthetic access$2200(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->dismissProgress()V

    return-void
.end method

.method static synthetic access$2300(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->forceSetPrinterSearchEnd()V

    return-void
.end method

.method static synthetic access$2400(Lepson/scan/activity/ScanSearchActivity;Lepson/scan/activity/ScannerPropertyWrapper;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->localFinish(Lepson/scan/activity/ScannerPropertyWrapper;)V

    return-void
.end method

.method static synthetic access$2500(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->showProgress()V

    return-void
.end method

.method static synthetic access$2600(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->releaseAndFinish()V

    return-void
.end method

.method static synthetic access$300(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->interruptSearch()V

    return-void
.end method

.method static synthetic access$400(Lepson/scan/activity/ScanSearchActivity;)I
    .locals 0

    .line 65
    iget p0, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    return p0
.end method

.method static synthetic access$402(Lepson/scan/activity/ScanSearchActivity;I)I
    .locals 0

    .line 65
    iput p1, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    return p1
.end method

.method static synthetic access$502(Lepson/scan/activity/ScanSearchActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 65
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mSimpleApSsid:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lepson/scan/activity/ScanSearchActivity;Lepson/print/MyPrinter;)Ljava/lang/String;
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->getSimpleApSsid(Lepson/print/MyPrinter;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lepson/scan/activity/ScanSearchActivity;Lepson/print/MyPrinter;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->startGetCapabilityAndFinishTask(Lepson/print/MyPrinter;)V

    return-void
.end method

.method static synthetic access$800(Lepson/scan/activity/ScanSearchActivity;)Landroid/widget/ListView;
    .locals 0

    .line 65
    iget-object p0, p0, Lepson/scan/activity/ScanSearchActivity;->mListView:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$900(Lepson/scan/activity/ScanSearchActivity;)Landroid/content/Context;
    .locals 0

    .line 65
    iget-object p0, p0, Lepson/scan/activity/ScanSearchActivity;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method private buildElements()V
    .locals 8

    .line 277
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080098

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLocalButton:Landroid/widget/RadioButton;

    .line 278
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080097

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    .line 279
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f08015c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mSearchButton:Landroid/widget/Button;

    .line 280
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080298

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 281
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080121

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mAddButton:Landroid/widget/Button;

    .line 282
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080105

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mTextDetail:Landroid/widget/TextView;

    .line 284
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mTextDetail:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f08009e

    .line 287
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070118

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 290
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f080071

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLocalButton:Landroid/widget/RadioButton;

    const v3, 0x7f07011c

    invoke-static {p0, v0, v3}, Lepson/common/Utils;->setDrawble2TextView(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 294
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    const v3, 0x7f07011b

    invoke-static {p0, v0, v3}, Lepson/common/Utils;->setDrawble2TextView(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 300
    iget v0, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    const v3, 0x7f08022b

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x3

    if-eq v0, v6, :cond_0

    .line 336
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 337
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mAddButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 346
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSearchActivity;->searchButtonSetEnabled(Z)V

    .line 348
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->tvResult:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 352
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 356
    new-instance v0, Lepson/scan/activity/ScannerListAdapter;

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const/4 v6, 0x2

    invoke-direct {v0, p0, v2, v6}, Lepson/scan/activity/ScannerListAdapter;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    .line 360
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->scannerId:Ljava/lang/String;

    iget-object v6, p0, Lepson/scan/activity/ScanSearchActivity;->connectionInfo:Ljava/lang/String;

    invoke-static {p0, v2, v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->getCurPrinterString(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepson/scan/activity/ScannerListAdapter;->setResource(Ljava/lang/Object;)V

    .line 363
    new-instance v0, Lepson/print/widgets/ListControlHelper;

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v2}, Lepson/scan/activity/ScannerListAdapter;->getBuilder()Lepson/print/widgets/AbstractListBuilder;

    move-result-object v2

    check-cast v2, Lepson/print/widgets/PrinterInfoBuilder;

    invoke-direct {v0, v2}, Lepson/print/widgets/ListControlHelper;-><init>(Lepson/print/widgets/PrinterInfoBuilder;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->helper:Lepson/print/widgets/ListControlHelper;

    .line 364
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v2}, Lepson/scan/activity/ScannerListAdapter;->getData()Ljava/util/Vector;

    move-result-object v2

    iget-object v6, p0, Lepson/scan/activity/ScanSearchActivity;->helper:Lepson/print/widgets/ListControlHelper;

    iget-object v7, p0, Lepson/scan/activity/ScanSearchActivity;->connectionInfo:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v6, v7}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;-><init>(Landroid/content/Context;Ljava/util/AbstractList;Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;Ljava/lang/String;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    .line 367
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLocalButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 369
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mTextDetail:Landroid/widget/TextView;

    const v2, 0x7f0e048a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 372
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 303
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    const v5, 0x7f0e0437

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 304
    iget-object v5, p0, Lepson/scan/activity/ScanSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    .line 307
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mAddButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->tvResult:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 320
    new-instance v0, Lepson/scan/activity/ScannerListAdapter;

    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v0, p0, v1, v4}, Lepson/scan/activity/ScannerListAdapter;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    .line 321
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->scannerKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScannerListAdapter;->setResource(Ljava/lang/Object;)V

    .line 324
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 326
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mTextDetail:Landroid/widget/TextView;

    const v1, 0x7f0e0488

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 329
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 378
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerListAdapter;->build()V

    .line 379
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerListAdapter;->refresh()V

    .line 382
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 384
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$3;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$3;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 420
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$4;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$4;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 435
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mAddButton:Landroid/widget/Button;

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$5;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$5;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 469
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mSearchButton:Landroid/widget/Button;

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$6;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$6;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 476
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLocalButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$7;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$7;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 491
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$8;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$8;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private disconnectSimpleAp()V
    .locals 3

    .line 1288
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "scanner"

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->mMyPrinterForSimpleApDisconnect:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1290
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1291
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->mMyPrinterForSimpleApDisconnect:Ljava/lang/String;

    .line 1290
    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnectSimpleAP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method private dismissPrinterNotFoundDialog()V
    .locals 2

    .line 1037
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "printer_not_found_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 1039
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private dismissProgress()V
    .locals 1

    .line 1263
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressDialog:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1264
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressDialog:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    .line 1266
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgress:Z

    return-void
.end method

.method private forceSetPrinterSearchEnd()V
    .locals 1

    const/4 v0, 0x1

    .line 1212
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->isFocused:Ljava/lang/Boolean;

    const/4 v0, 0x0

    .line 1213
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->isSelected:Z

    return-void
.end method

.method private static getEscanI1ErrorTitleAndMessageId(I)[I
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const v0, -0x7a121

    const/4 v1, 0x2

    if-eq p0, v0, :cond_3

    const/16 v0, -0x5dd

    if-eq p0, v0, :cond_2

    const/16 v0, -0x514

    if-eq p0, v0, :cond_1

    const/16 v0, -0x44c

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    .line 1153
    new-array p0, v1, [I

    fill-array-data p0, :array_0

    return-object p0

    .line 1139
    :cond_0
    :pswitch_0
    new-array p0, v1, [I

    fill-array-data p0, :array_1

    return-object p0

    .line 1133
    :cond_1
    new-array p0, v1, [I

    fill-array-data p0, :array_2

    return-object p0

    .line 1144
    :cond_2
    :pswitch_1
    new-array p0, v1, [I

    fill-array-data p0, :array_3

    return-object p0

    .line 1148
    :cond_3
    new-array p0, v1, [I

    fill-array-data p0, :array_4

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :array_0
    .array-data 4
        0x7f0e04ac
        0x7f0e04ab
    .end array-data

    :array_1
    .array-data 4
        0x7f0e04a6
        0x7f0e04a5
    .end array-data

    :array_2
    .array-data 4
        0x7f0e04af
        0x7f0e04ae
    .end array-data

    :array_3
    .array-data 4
        0x7f0e04a4
        0x7f0e04a3
    .end array-data

    :array_4
    .array-data 4
        0x7f0e01b3
        0x7f0e04c8
    .end array-data
.end method

.method private static getEscanI2ErrorTitleAndMessageId(I)[I
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, 0x2

    .line 1166
    new-array v0, v0, [I

    const/4 v1, 0x1

    const/4 v2, 0x0

    sparse-switch p0, :sswitch_data_0

    const p0, 0x7f0e04ac

    aput p0, v0, v2

    const p0, 0x7f0e04ab

    aput p0, v0, v1

    goto :goto_0

    :sswitch_0
    const p0, 0x7f0e0471

    aput p0, v0, v2

    const p0, 0x7f0e04a0

    aput p0, v0, v1

    goto :goto_0

    :sswitch_1
    const p0, 0x7f0e04a6

    aput p0, v0, v2

    const p0, 0x7f0e04a5

    aput p0, v0, v1

    goto :goto_0

    :sswitch_2
    const p0, 0x7f0e04af

    aput p0, v0, v2

    const p0, 0x7f0e04ae

    aput p0, v0, v1

    goto :goto_0

    :sswitch_3
    const p0, 0x7f0e04a4

    aput p0, v0, v2

    const p0, 0x7f0e04a3

    aput p0, v0, v1

    :goto_0
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5dd -> :sswitch_3
        -0x514 -> :sswitch_2
        -0x45a -> :sswitch_1
        -0x44c -> :sswitch_1
        -0xd6 -> :sswitch_1
        -0xd5 -> :sswitch_3
        -0xca -> :sswitch_0
        -0xc9 -> :sswitch_0
        -0x66 -> :sswitch_0
        -0x65 -> :sswitch_0
    .end sparse-switch
.end method

.method private getPrinterNotFoundDialog(Z)Landroid/support/v4/app/DialogFragment;
    .locals 5

    const v0, 0x7f0e0473

    const/4 v1, 0x1

    const v2, 0x7f0e04af

    if-eqz p1, :cond_0

    const p1, 0x7f0e01a9

    .line 944
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 945
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 947
    :cond_0
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 949
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v3, "epson.scanner.SelectedScanner"

    const-string v4, "RE_SEARCH"

    invoke-static {p1, v3, v4}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    const p1, 0x7f0e04ae

    .line 953
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 954
    invoke-static {p0}, Lepson/common/Utils;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 955
    invoke-static {p0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    .line 956
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\nSSID: "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 959
    invoke-static {v1, p1, v2, v3, v0}, Lepson/print/CustomLayoutDialogFragment;->newInstance(ILjava/lang/String;Ljava/lang/String;II)Lepson/print/CustomLayoutDialogFragment;

    move-result-object p1

    return-object p1

    :cond_2
    const p1, 0x7f0e04ad

    .line 964
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 968
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n\n"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const p1, 0x7f0e0309

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const v3, 0x7f0e02ea

    .line 969
    invoke-static {v1, p1, v2, v3, v0}, Lepson/print/CustomLayoutDialogFragment;->newInstance(ILjava/lang/String;Ljava/lang/String;II)Lepson/print/CustomLayoutDialogFragment;

    move-result-object p1

    return-object p1
.end method

.method private getSimpleApSsid(Lepson/print/MyPrinter;)Ljava/lang/String;
    .locals 1

    .line 1226
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getScannerId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromScannerId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1228
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1230
    :cond_0
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const-string p1, ""

    :goto_0
    return-object p1
.end method

.method public static getStartIntent(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;
    .locals 2

    .line 1301
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/ScanSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "scanner-location"

    .line 1302
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "scanner-id"

    .line 1303
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "scanner-ssid"

    .line 1304
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private interruptSearch()V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    const/4 v0, 0x1

    .line 874
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mScannerFindCancelRequested:Z

    .line 877
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 878
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const-string v0, "epson.scan.activity.ScanSettingsActivity"

    const-string v1, "Cancel search scanner"

    .line 880
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 885
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 888
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz v0, :cond_0

    .line 889
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    const/4 v0, 0x0

    .line 890
    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    :cond_0
    const/4 v0, 0x0

    .line 892
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->isSearchSimpleAp:Z

    .line 894
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mFindScannerTask:Lepson/scan/activity/ScanSearchActivity$FindScannerTask;

    if-eqz v0, :cond_1

    .line 896
    invoke-virtual {v0}, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->cancelFind()V

    :cond_1
    return-void
.end method

.method private localFinish(Lepson/scan/activity/ScannerPropertyWrapper;)V
    .locals 1

    .line 1217
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mSimpleApSsid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lepson/scan/activity/ScannerPropertyWrapper;->setSimpleApSsid(Ljava/lang/String;)V

    .line 1219
    invoke-static {p1}, Lepson/scan/activity/SettingActivityParams;->getScanSearchReturnIntent(Lepson/scan/activity/ScannerPropertyWrapper;)Landroid/content/Intent;

    move-result-object p1

    const/4 v0, -0x1

    .line 1220
    invoke-virtual {p0, v0, p1}, Lepson/scan/activity/ScanSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 1222
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->finish()V

    return-void
.end method

.method private releaseAndFinish()V
    .locals 1

    .line 587
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    const/4 v0, 0x0

    .line 589
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->setResult(I)V

    .line 590
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->finish()V

    return-void
.end method

.method private search()V
    .locals 6
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .line 816
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->interruptSearch()V

    const/4 v0, 0x0

    .line 817
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mScannerFindCancelRequested:Z

    .line 819
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->resetEscanLib()V

    .line 821
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v1}, Lepson/scan/activity/ScannerListAdapter;->refresh()V

    .line 822
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v1}, Lepson/scan/activity/ScannerListAdapter;->destructor()V

    .line 823
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->clearPrinterInfoList()V

    .line 825
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xc

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    .line 828
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 829
    iput-boolean v3, p0, Lepson/scan/activity/ScanSearchActivity;->isSearchSimpleAp:Z

    goto :goto_0

    .line 832
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isWifiEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 834
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 835
    iput-boolean v3, p0, Lepson/scan/activity/ScanSearchActivity;->isSearchSimpleAp:Z

    goto :goto_0

    .line 836
    :cond_1
    iget-boolean v1, p0, Lepson/scan/activity/ScanSearchActivity;->bCheckWiFiStatus:Z

    if-nez v1, :cond_2

    if-nez v1, :cond_2

    .line 839
    iput-boolean v3, p0, Lepson/scan/activity/ScanSearchActivity;->bCheckWiFiStatus:Z

    const/4 v0, -0x1

    .line 840
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->enableWiFi(Landroid/app/Activity;I)V

    return-void

    .line 852
    :cond_2
    :goto_0
    invoke-static {p0, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 855
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->searchButtonSetEnabled(Z)V

    .line 856
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 857
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->tvResult:Landroid/widget/TextView;

    const v2, 0x7f0e039e

    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 858
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->isFinishSearchScanner:Z

    .line 860
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mEscanLib:Lepson/scan/lib/escanLib;

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lepson/scan/lib/escanLib;->setScanHandler(Landroid/os/Handler;)V

    .line 862
    new-instance v1, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-direct {v1, p0, v2}, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;-><init>(Lepson/scan/activity/ScanSearchActivity;Lepson/scan/lib/escanLib;)V

    iput-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mFindScannerTask:Lepson/scan/activity/ScanSearchActivity$FindScannerTask;

    .line 863
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mFindScannerTask:Lepson/scan/activity/ScanSearchActivity$FindScannerTask;

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private setNotFoundPrinterButton()V
    .locals 2

    .line 234
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f08022b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$1;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$1;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private showPrinterNotFoundDialog(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .line 975
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 978
    :cond_0
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "printer_not_found_dialog"

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showProgress()V
    .locals 1

    const/4 v0, 0x1

    .line 1257
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgress:Z

    .line 1258
    new-instance v0, Lepson/print/screen/WorkingDialog;

    invoke-direct {v0, p0}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressDialog:Lepson/print/screen/WorkingDialog;

    .line 1259
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressDialog:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    return-void
.end method

.method private startGetCapabilityAndFinishTask(Lepson/print/MyPrinter;)V
    .locals 2

    .line 1275
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mMyPrinterForSimpleApDisconnect:Ljava/lang/String;

    .line 1278
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->interruptSearch()V

    .line 1281
    new-instance v0, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;

    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-direct {v0, p0, v1, p1}, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;-><init>(Lepson/scan/activity/ScanSearchActivity;Lepson/scan/lib/escanLib;Lepson/print/MyPrinter;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mGetScannerCapabilityAndFinishTask:Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;

    .line 1283
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mGetScannerCapabilityAndFinishTask:Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private startWifiPrinterSelect()V
    .locals 2

    .line 268
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/ScanSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updateFindScannerResult()V
    .locals 5

    .line 905
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 909
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/4 v0, 0x1

    .line 911
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->searchButtonSetEnabled(Z)V

    .line 913
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v1}, Lepson/scan/activity/ScannerListAdapter;->getData()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 915
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    const v3, 0x7f0e039f

    if-nez v2, :cond_2

    .line 916
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 919
    :cond_1
    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity;->tvResult:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 917
    :cond_2
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->tvResult:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 922
    :cond_3
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->tvResult:Landroid/widget/TextView;

    const v1, 0x7f0e03a0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 924
    iget-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mActivityForegroundLifetime:Z

    if-eqz v0, :cond_4

    .line 925
    iget-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->isSearchSimpleAp:Z

    invoke-direct {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->getPrinterNotFoundDialog(Z)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->showPrinterNotFoundDialog(Landroid/support/v4/app/DialogFragment;)V

    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method addWiFiSetupButton()V
    .locals 2

    .line 248
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0802b4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    .line 251
    iget-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->isJapaneseLocale:Z

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0e04c4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 254
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0e0524

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 258
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$2;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$2;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 770
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x7

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eq p1, v0, :cond_1

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    if-ne p2, v2, :cond_3

    .line 776
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {p1}, Lepson/scan/activity/ScannerListAdapter;->refresh()V

    .line 778
    new-instance p1, Lepson/print/MyPrinter;

    const-string p2, "PRINTER_NAME"

    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "PRINTER_IP"

    .line 779
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "SCAN_REFS_SCANNER_ID"

    .line 780
    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, v0, p3, v1}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x3

    .line 781
    invoke-virtual {p1, p2}, Lepson/print/MyPrinter;->setLocation(I)V

    .line 782
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->startGetCapabilityAndFinishTask(Lepson/print/MyPrinter;)V

    goto :goto_0

    :cond_1
    if-ne p2, v2, :cond_2

    const/4 p1, 0x1

    .line 789
    iput-boolean p1, p0, Lepson/scan/activity/ScanSearchActivity;->isSelected:Z

    const-string p1, "easysetup"

    const/4 p2, 0x0

    .line 793
    invoke-virtual {p3, p1, p2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 797
    new-instance p1, Lepson/print/MyPrinter;

    const-string p2, "name"

    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "ip"

    .line 798
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "id"

    .line 799
    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, v0, p3, v1}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    iget p2, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    invoke-virtual {p1, p2}, Lepson/print/MyPrinter;->setLocation(I)V

    .line 802
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->getSimpleApSsid(Lepson/print/MyPrinter;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lepson/scan/activity/ScanSearchActivity;->mSimpleApSsid:Ljava/lang/String;

    .line 804
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->startGetCapabilityAndFinishTask(Lepson/print/MyPrinter;)V

    return-void

    .line 807
    :cond_2
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->forceSetPrinterSearchEnd()V

    :cond_3
    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .line 569
    iget-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mProgress:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 573
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mActivityBackRequested:Z

    .line 574
    sget-object v0, Lepson/scan/activity/ScanSearchActivity;->sEscanLibSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    if-gtz v0, :cond_1

    .line 576
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->interruptSearch()V

    .line 579
    new-instance v0, Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    .line 583
    :cond_1
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->releaseAndFinish()V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .line 516
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iput-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 517
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 520
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 545
    :pswitch_0
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 547
    iget v0, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const/16 v0, 0xa

    .line 549
    iput v0, p1, Landroid/os/Message;->what:I

    .line 553
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerListAdapter;->getData()Ljava/util/Vector;

    move-result-object v0

    iget v2, p0, Lepson/scan/activity/ScanSearchActivity;->mDeletePos:I

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 554
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 523
    :pswitch_1
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mContext:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 524
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0316

    .line 525
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 526
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070099

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    .line 527
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lepson/scan/activity/ScanSearchActivity$10;

    invoke-direct {v2, p0}, Lepson/scan/activity/ScanSearchActivity$10;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0476

    .line 536
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lepson/scan/activity/ScanSearchActivity$9;

    invoke-direct {v2, p0}, Lepson/scan/activity/ScanSearchActivity$9;-><init>(Lepson/scan/activity/ScanSearchActivity;)V

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 541
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_1
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 175
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 177
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x1

    if-nez p1, :cond_1

    .line 178
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 181
    iput-boolean p1, p0, Lepson/scan/activity/ScanSearchActivity;->isJapaneseLocale:Z

    goto :goto_1

    .line 179
    :cond_1
    :goto_0
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->isJapaneseLocale:Z

    .line 184
    :goto_1
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v1, 0x7f0a00b8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    .line 188
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->addWiFiSetupButton()V

    .line 190
    new-instance p1, Lepson/scan/lib/escanLib;

    invoke-direct {p1}, Lepson/scan/lib/escanLib;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 192
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {p1, p0}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    .line 194
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "scanner-location"

    .line 196
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    .line 197
    iget v1, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    if-nez v1, :cond_2

    .line 198
    iput v0, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    .line 201
    :cond_2
    iget v1, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    const-string v1, "scanner-id"

    .line 206
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->scannerId:Ljava/lang/String;

    const-string v1, "scanner-ssid"

    .line 207
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->connectionInfo:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const-string v1, "scanner-id"

    .line 203
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->scannerKey:Ljava/lang/String;

    .line 211
    :goto_2
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->setContentView(Landroid/view/View;)V

    .line 212
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080123

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->tvResult:Landroid/widget/TextView;

    .line 213
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->tvResult:Landroid/widget/TextView;

    const v1, 0x7f0e039e

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0801e1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    .line 216
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x102000a

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mListView:Landroid/widget/ListView;

    const-string p1, "epson.scan.activity.ScanSearchActivity"

    const-string v1, "builElements"

    .line 218
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->buildElements()V

    .line 221
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->setNotFoundPrinterButton()V

    const p1, 0x7f0e0536

    .line 224
    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/ScanSearchActivity;->setActionBar(IZ)V

    .line 226
    iput-object p0, p0, Lepson/scan/activity/ScanSearchActivity;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    .line 507
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    const p2, 0x7f0e049d

    .line 509
    invoke-interface {p1, p2}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    const/4 p3, 0x0

    const/4 v0, 0x1

    const v1, 0x7f0e0485

    .line 510
    invoke-interface {p1, p3, v0, p3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x2

    .line 511
    invoke-interface {p1, p3, v0, p3, p2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    .line 983
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0e04f2

    const v2, 0x7f0e04af

    const v3, 0x7f0e01a9

    if-eq p1, v3, :cond_1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, ""

    .line 990
    invoke-static {p0}, Lepson/common/Utils;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 991
    invoke-static {p0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    .line 992
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nSSID: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 994
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 995
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 994
    invoke-static {p0, v0, p1, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 1000
    :cond_1
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1001
    invoke-virtual {p0, v3}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1000
    invoke-static {p0, p1, v0, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0e04ad
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "epson.scan.activity.ScanSearchActivity"

    const-string v1, "Destroy search screen"

    .line 561
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->interruptSearch()V

    .line 563
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerListAdapter;->destructor()V

    .line 564
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    return-void
.end method

.method public onLocalNegativeCallback(I)V
    .locals 0

    return-void
.end method

.method public onLocalPositiveCallback(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    .line 1242
    :cond_0
    invoke-static {p0}, Lepson/maintain/activity/PrinterNotFoundDialogCreator;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "SearchPrinterScr"

    const-string v1, "onPause"

    .line 1082
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    const/4 v0, 0x0

    .line 1085
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mActivityForegroundLifetime:Z

    .line 1086
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->interruptSearch()V

    .line 1089
    iget-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->isSelected:Z

    if-nez v0, :cond_0

    .line 1091
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->removeAllDialog()V

    .line 1094
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mGetScannerCapabilityAndFinishTask:Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;

    if-eqz v0, :cond_1

    .line 1096
    invoke-virtual {v0}, Lepson/scan/activity/ScanSearchActivity$GetScannerCapabilityAndFinishTask;->cancelTask()V

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 4

    .line 1055
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "Epson"

    const-string v1, "ScanSearchActivity.java call onResume()"

    .line 1056
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1058
    iput-boolean v0, p0, Lepson/scan/activity/ScanSearchActivity;->mActivityForegroundLifetime:Z

    .line 1061
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->dismissPrinterNotFoundDialog()V

    .line 1063
    iget v1, p0, Lepson/scan/activity/ScanSearchActivity;->scanner_location:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 1069
    iget-boolean v1, p0, Lepson/scan/activity/ScanSearchActivity;->isSelected:Z

    if-nez v1, :cond_0

    .line 1071
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 1072
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    const-string v0, "ScanSearchActivity"

    const-string v1, "Send CHECK_PRINTER Message."

    .line 1074
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method removeAllDialog()V
    .locals 1

    const v0, 0x7f0e04ad

    .line 1014
    :try_start_0
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const v0, 0x7f0e04ae

    .line 1019
    :try_start_1
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->removeDialog(I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const v0, 0x7f0e01a9

    .line 1024
    :try_start_2
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->removeDialog(I)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1030
    :catch_2
    :try_start_3
    invoke-direct {p0}, Lepson/scan/activity/ScanSearchActivity;->dismissPrinterNotFoundDialog()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method

.method public searchButtonSetEnabled(Z)V
    .locals 2

    .line 1044
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity;->mSearchButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1047
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 1049
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method showErrorDialog(Lepson/scan/lib/EscanLibException;)V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .line 1105
    invoke-virtual {p0}, Lepson/scan/activity/ScanSearchActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1110
    :cond_0
    invoke-virtual {p1}, Lepson/scan/lib/EscanLibException;->getEscIVersion()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1111
    invoke-virtual {p1}, Lepson/scan/lib/EscanLibException;->getEscanLibErrorCode()I

    move-result p1

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->getEscanI1ErrorTitleAndMessageId(I)[I

    move-result-object p1

    goto :goto_0

    .line 1113
    :cond_1
    invoke-virtual {p1}, Lepson/scan/lib/EscanLibException;->getEscanLibErrorCode()I

    move-result p1

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->getEscanI2ErrorTitleAndMessageId(I)[I

    move-result-object p1

    :goto_0
    const/4 v0, 0x0

    .line 1116
    aget v0, p1, v0

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aget p1, p1, v1

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f0e04f2

    .line 1117
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1116
    invoke-static {p0, v0, p1, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->errorDialog:Landroid/app/AlertDialog;

    .line 1119
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity;->errorDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

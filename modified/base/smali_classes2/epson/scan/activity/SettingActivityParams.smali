.class Lepson/scan/activity/SettingActivityParams;
.super Ljava/lang/Object;
.source "SettingActivityParams.java"


# static fields
.field private static final ESC_I1_SETTING_ACTIVITY:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static final ESC_I2_SETTING_ACTIVITY:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field static final PARAM_KEY_EXTERNAL_SCAN_PARAMS:Ljava/lang/String; = "external-scan-params"

.field static final PARAM_KEY_HIDE_COLOR_MODE:Ljava/lang/String; = "hide-color-mode"

.field static final PARAM_KEY_HIDE_RESOLUTION:Ljava/lang/String; = "hide-resolution"

.field private static final PARAM_RETURN_IF_ESC_I_VERSION_CHANGED:Ljava/lang/String; = "return-if-esc-i-version-changed"

.field private static final PARAM_SCANNER_PROPERTY_WRAPPER:Ljava/lang/String; = "scanner-property-wrapper"

.field static final RESULT_ESC_I_VERSION_CHANGED:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    const-class v0, Lepson/scan/activity/ScanSettingsActivity;

    sput-object v0, Lepson/scan/activity/SettingActivityParams;->ESC_I1_SETTING_ACTIVITY:Ljava/lang/Class;

    .line 27
    const-class v0, Lepson/scan/activity/I2ScanSettingActivity;

    sput-object v0, Lepson/scan/activity/SettingActivityParams;->ESC_I2_SETTING_ACTIVITY:Ljava/lang/Class;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getChangeSettingActivityStartIntent(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;ZZZLcom/epson/iprint/shared/SharedParamScan;)Landroid/content/Intent;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 51
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result v1

    invoke-static {v1}, Lepson/scan/activity/SettingActivityParams;->getSettingActivityClass(I)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "return-if-esc-i-version-changed"

    .line 52
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "scanner-property-wrapper"

    .line 53
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p0, "hide-color-mode"

    .line 54
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "hide-resolution"

    .line 55
    invoke-virtual {v0, p0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "external-scan-params"

    .line 56
    invoke-virtual {v0, p0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method static getScanSearchReturnIntent(Lepson/scan/activity/ScannerPropertyWrapper;)Landroid/content/Intent;
    .locals 2

    .line 101
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "scanner-property-wrapper"

    .line 102
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method static getScannerPropertyWrapper(Landroid/content/Intent;)Lepson/scan/activity/ScannerPropertyWrapper;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "scanner-property-wrapper"

    .line 80
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p0

    check-cast p0, Lepson/scan/activity/ScannerPropertyWrapper;

    return-object p0
.end method

.method private static getSettingActivityClass(I)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 66
    sget-object p0, Lepson/scan/activity/SettingActivityParams;->ESC_I1_SETTING_ACTIVITY:Ljava/lang/Class;

    return-object p0

    .line 64
    :cond_0
    sget-object p0, Lepson/scan/activity/SettingActivityParams;->ESC_I2_SETTING_ACTIVITY:Ljava/lang/Class;

    return-object p0
.end method

.method static getSettingReturnIntent(Lepson/scan/activity/ScannerPropertyWrapper;)Landroid/content/Intent;
    .locals 2
    .param p0    # Lepson/scan/activity/ScannerPropertyWrapper;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 89
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "scanner-property-wrapper"

    .line 90
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method static returnIfEscIVersionChanged(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "return-if-esc-i-version-changed"

    const/4 v1, 0x0

    .line 38
    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

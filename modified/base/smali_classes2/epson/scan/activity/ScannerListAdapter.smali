.class Lepson/scan/activity/ScannerListAdapter;
.super Ljava/lang/Object;
.source "ScannerListAdapter.java"


# static fields
.field public static final TYPE_IP_ADDRESS:I = 0x2

.field public static final TYPE_LOCAL:I = 0x1


# instance fields
.field private mBuilder:Lepson/print/widgets/AbstractListBuilder;

.field private mEscIVersion:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V
    .locals 1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mEscIVersion:Ljava/util/ArrayList;

    const/4 v0, 0x1

    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 57
    :pswitch_0
    new-instance p3, Lepson/print/widgets/PrinterInfoBuilder;

    invoke-direct {p3, p1, p2, v0}, Lepson/print/widgets/PrinterInfoBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object p3, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    goto :goto_0

    .line 54
    :pswitch_1
    new-instance p3, Lepson/print/widgets/PrinterInfoIpBuilder;

    invoke-direct {p3, p1, p2, v0}, Lepson/print/widgets/PrinterInfoIpBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object p3, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 64
    :goto_0
    iget-object p1, p0, Lepson/scan/activity/ScannerListAdapter;->mEscIVersion:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addPrinter(Lepson/print/MyPrinter;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    instance-of v1, v0, Lepson/print/widgets/PrinterInfoBuilder;

    if-eqz v1, :cond_0

    .line 69
    check-cast v0, Lepson/print/widgets/PrinterInfoBuilder;

    invoke-virtual {v0, p1}, Lepson/print/widgets/PrinterInfoBuilder;->addPrinter(Lepson/print/MyPrinter;)V

    :cond_0
    return-void
.end method

.method public addPrinter([Ljava/lang/String;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0, p1}, Lepson/print/widgets/AbstractListBuilder;->addPrinter([Ljava/lang/String;)V

    return-void
.end method

.method public build()V
    .locals 1

    .line 99
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->build()V

    return-void
.end method

.method public destructor()V
    .locals 1

    .line 95
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->destructor()V

    return-void
.end method

.method public getAdapter()Landroid/widget/BaseAdapter;
    .locals 1

    .line 87
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getBuilder()Lepson/print/widgets/AbstractListBuilder;
    .locals 1

    .line 107
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    return-object v0
.end method

.method public getData()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    return-object v0
.end method

.method public refresh()V
    .locals 1

    .line 91
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    return-void
.end method

.method public setResource(Ljava/lang/Object;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lepson/scan/activity/ScannerListAdapter;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0, p1}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    return-void
.end method

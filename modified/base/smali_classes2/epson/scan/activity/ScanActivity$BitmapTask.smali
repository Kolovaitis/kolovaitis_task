.class Lepson/scan/activity/ScanActivity$BitmapTask;
.super Landroid/os/AsyncTask;
.source "ScanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BitmapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method public constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 2287
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$BitmapTask;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    .line 2292
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$BitmapTask;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->isJobDone()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 2293
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$BitmapTask;->this$0:Lepson/scan/activity/ScanActivity;

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-static {v0, p1}, Lepson/scan/activity/ScanActivity;->access$5500(Lepson/scan/activity/ScanActivity;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2286
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanActivity$BitmapTask;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 2298
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$BitmapTask;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0, p1}, Lepson/scan/activity/ScanActivity;->access$5600(Lepson/scan/activity/ScanActivity;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 2286
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanActivity$BitmapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.class Lepson/scan/activity/ScanSettingsActivity$1$1;
.super Landroid/os/AsyncTask;
.source "ScanSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/scan/activity/ScanSettingsActivity$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/scan/activity/ScanSettingsActivity$1;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSettingsActivity$1;)V
    .locals 0

    .line 376
    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 376
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity$1$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 386
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object p1, p1, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "scanner"

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object v1, v1, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    .line 387
    invoke-static {v1}, Lepson/scan/activity/ScanSettingsActivity;->access$100(Lepson/scan/activity/ScanSettingsActivity;)Lepson/scan/lib/ScannerInfo;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v1

    .line 386
    invoke-static {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 389
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object p1, p1, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object v0, v0, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    .line 390
    invoke-static {v0}, Lepson/scan/activity/ScanSettingsActivity;->access$200(Lepson/scan/activity/ScanSettingsActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object v1, v1, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanSettingsActivity;->access$100(Lepson/scan/activity/ScanSettingsActivity;)Lepson/scan/lib/ScannerInfo;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v1

    .line 389
    invoke-static {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnectSimpleAP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 376
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity$1$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3

    .line 396
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object p1, p1, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object v0, v0, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    .line 397
    invoke-static {v0}, Lepson/scan/activity/ScanSettingsActivity;->access$100(Lepson/scan/activity/ScanSettingsActivity;)Lepson/scan/lib/ScannerInfo;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object v1, v1, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanSettingsActivity;->access$100(Lepson/scan/activity/ScanSettingsActivity;)Lepson/scan/lib/ScannerInfo;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v1

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object v2, v2, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-static {v2}, Lepson/scan/activity/ScanSettingsActivity;->access$200(Lepson/scan/activity/ScanSettingsActivity;)Ljava/lang/String;

    move-result-object v2

    .line 396
    invoke-static {p1, v0, v1, v2}, Lepson/scan/activity/ScanSearchActivity;->getStartIntent(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 398
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object v0, v0, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lepson/scan/activity/ScanSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 401
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity$1$1;->this$1:Lepson/scan/activity/ScanSettingsActivity$1;

    iget-object p1, p1, Lepson/scan/activity/ScanSettingsActivity$1;->this$0:Lepson/scan/activity/ScanSettingsActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsActivity;->access$000(Lepson/scan/activity/ScanSettingsActivity;)Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method

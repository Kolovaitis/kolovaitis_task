.class Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;
.super Landroid/os/AsyncTask;
.source "ScanSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CancelWaitTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSearchActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 1483
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1483
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    .line 1489
    :try_start_0
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 1490
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$2100(Lepson/scan/activity/ScanSearchActivity;)V

    .line 1491
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1483
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0

    .line 1501
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$CancelWaitTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$2600(Lepson/scan/activity/ScanSearchActivity;)V

    return-void
.end method

.class public Lepson/scan/activity/DisplayIdConverter;
.super Ljava/lang/Object;
.source "DisplayIdConverter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getColorModeItemIdArray([Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;)[I
    .locals 5

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 39
    :cond_0
    array-length v0, p0

    new-array v0, v0, [I

    .line 41
    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v4, p0, v2

    .line 42
    invoke-static {v4}, Lepson/scan/activity/DisplayIdConverter;->getColorStringId(Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;)I

    move-result v4

    if-nez v4, :cond_1

    const v4, 0x7f0e0471

    .line 49
    :cond_1
    aput v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static getColorStringId(Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;)I
    .locals 1

    .line 21
    sget-object v0, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$ColorMode:[I

    invoke-virtual {p0}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return p0

    :pswitch_0
    const p0, 0x7f0e0508

    return p0

    :pswitch_1
    const p0, 0x7f0e0507

    return p0

    :pswitch_2
    const p0, 0x7f0e0506

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getEpsMediaId(Lcom/epson/lib/escani2/ScanSize$PaperSize;)I
    .locals 1

    .line 128
    sget-object v0, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    invoke-virtual {p0}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, -0x1

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 167
    :pswitch_0
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 159
    :pswitch_1
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_BUZCARD_55X91:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 155
    :pswitch_2
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 148
    :pswitch_3
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 145
    :pswitch_4
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 142
    :pswitch_5
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 139
    :pswitch_6
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 136
    :pswitch_7
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 133
    :pswitch_8
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 130
    :pswitch_9
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    :goto_0
    :pswitch_a
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_a
        :pswitch_2
        :pswitch_1
        :pswitch_a
        :pswitch_0
    .end packed-switch
.end method

.method public static getGammaStringId(Lcom/epson/lib/escani2/EscanI2Lib$Gamma;)I
    .locals 1

    .line 252
    sget-object v0, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$Gamma:[I

    invoke-virtual {p0}, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return p0

    :pswitch_0
    const p0, 0x7f0e050b

    return p0

    :pswitch_1
    const p0, 0x7f0e050a

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getGammaStringIdArray(Landroid/content/Context;[Lcom/epson/lib/escani2/EscanI2Lib$Gamma;)[I
    .locals 4

    .line 241
    array-length p0, p1

    new-array p0, p0, [I

    .line 243
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v3, p1, v1

    .line 244
    invoke-static {v3}, Lepson/scan/activity/DisplayIdConverter;->getGammaStringId(Lcom/epson/lib/escani2/EscanI2Lib$Gamma;)I

    move-result v3

    aput v3, p0, v2

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public static getInputUnitItemIdArray([Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I
    .locals 5

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 74
    :cond_0
    array-length v0, p0

    new-array v0, v0, [I

    .line 76
    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v4, p0, v2

    .line 77
    invoke-static {v4}, Lepson/scan/activity/DisplayIdConverter;->getInputUnitStringId(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)I

    move-result v4

    if-nez v4, :cond_1

    const v4, 0x7f0e0471

    .line 84
    :cond_1
    aput v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static getInputUnitStringId(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)I
    .locals 1

    .line 60
    sget-object v0, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

    invoke-virtual {p0}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return p0

    :pswitch_0
    const p0, 0x7f0e0511

    return p0

    :pswitch_1
    const p0, 0x7f0e0512

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getScanResolutionStrings(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3

    if-gtz p1, :cond_0

    const p1, 0x7f0e0471

    .line 215
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const v0, 0x7f0e050e

    const/4 v1, 0x1

    .line 218
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getScanResolutionStrings(Landroid/content/Context;[Ljava/lang/Integer;)[Ljava/lang/String;
    .locals 5

    .line 200
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    .line 202
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v4, p1, v2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 203
    invoke-static {p0, v4}, Lepson/scan/activity/DisplayIdConverter;->getScanResolutionStrings(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static getScanSizeIdArray([Lcom/epson/lib/escani2/ScanSize;)[I
    .locals 5

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 178
    :cond_0
    array-length v0, p0

    new-array v0, v0, [I

    .line 180
    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v4, p0, v2

    .line 181
    invoke-static {v4}, Lepson/scan/activity/DisplayIdConverter;->getScanSizeStringId(Lcom/epson/lib/escani2/ScanSize;)I

    move-result v4

    if-nez v4, :cond_1

    const v4, 0x7f0e0471

    .line 188
    :cond_1
    aput v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static getScanSizeStringId(Lcom/epson/lib/escani2/ScanSize$PaperSize;)I
    .locals 1

    if-eqz p0, :cond_1

    .line 112
    sget-object v0, Lcom/epson/lib/escani2/ScanSize$PaperSize;->PIXEL:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    if-ne p0, v0, :cond_0

    goto :goto_0

    .line 116
    :cond_0
    invoke-static {p0}, Lepson/scan/activity/DisplayIdConverter;->getEpsMediaId(Lcom/epson/lib/escani2/ScanSize$PaperSize;)I

    move-result p0

    .line 117
    invoke-static {p0}, Lepson/scan/lib/ScanSizeHelper;->getScanSizeName(I)I

    move-result p0

    return p0

    :cond_1
    :goto_0
    const p0, 0x7f0e0515

    return p0
.end method

.method public static getScanSizeStringId(Lcom/epson/lib/escani2/ScanSize;)I
    .locals 0

    if-nez p0, :cond_0

    const p0, 0x7f0e0515

    return p0

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/epson/lib/escani2/ScanSize;->getSize()Lcom/epson/lib/escani2/ScanSize$PaperSize;

    move-result-object p0

    .line 102
    invoke-static {p0}, Lepson/scan/activity/DisplayIdConverter;->getScanSizeStringId(Lcom/epson/lib/escani2/ScanSize$PaperSize;)I

    move-result p0

    return p0
.end method

.method public static getYesNoStringId(Landroid/content/Context;Z)I
    .locals 0

    if-eqz p1, :cond_0

    const p0, 0x7f0e052b

    goto :goto_0

    :cond_0
    const p0, 0x7f0e04e6

    :goto_0
    return p0
.end method

.method public static getYesNoStringIdArray(Landroid/content/Context;[Ljava/lang/Boolean;)[I
    .locals 5

    .line 227
    array-length v0, p1

    new-array v0, v0, [I

    .line 229
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v4, p1, v2

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 230
    invoke-static {p0, v4}, Lepson/scan/activity/DisplayIdConverter;->getYesNoStringId(Landroid/content/Context;Z)I

    move-result v4

    aput v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

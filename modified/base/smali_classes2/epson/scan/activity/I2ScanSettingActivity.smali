.class public Lepson/scan/activity/I2ScanSettingActivity;
.super Lepson/print/ActivityIACommon;
.source "I2ScanSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final PARAM_KEY_EXTERNAL_SCAN_PARAMS:Ljava/lang/String; = "external-scan-params"

.field private static final PARAM_KEY_HIDE_COLOR_MODE:Ljava/lang/String; = "hide-color-mode"

.field private static final PARAM_KEY_HIDE_RESOLUTION:Ljava/lang/String; = "hide-resolution"

.field private static final REQUEST_CODE_CHANGE_ESC_I_VERSION:I = 0x64

.field private static final REQUEST_CODE_SELECT_BRIGHTNESS:I = 0x8

.field private static final REQUEST_CODE_SELECT_COLOR_MODE:I = 0x3

.field private static final REQUEST_CODE_SELECT_DUPLEX:I = 0x5

.field private static final REQUEST_CODE_SELECT_GAMMA:I = 0x6

.field private static final REQUEST_CODE_SELECT_INPUT_UNIT:I = 0x1

.field private static final REQUEST_CODE_SELECT_RESOLUTION:I = 0x4

.field private static final REQUEST_CODE_SELECT_SCANNER:I = 0x7

.field private static final REQUEST_CODE_SELECT_SCAN_SIZE:I = 0x2

.field private static final RETURN_KEY_OLD_PREVIEW_VALID:Ljava/lang/String; = "old-preview-valid"


# instance fields
.field private mBrightnessSeparator:Landroid/view/View;

.field private mBrightnessText:Landroid/widget/TextView;

.field private mBrightnessViewGroup:Landroid/view/View;

.field private mColorModeSeparator:Landroid/view/View;

.field private mColorModeText:Landroid/widget/TextView;

.field private mColorModeViewGroup:Landroid/view/View;

.field private mDuplexSeparator:Landroid/view/View;

.field private mDuplexText:Landroid/widget/TextView;

.field private mDuplexViewGroup:Landroid/view/View;

.field private mExternalScanParams:Lcom/epson/iprint/shared/SharedParamScan;

.field private mGammaText:Landroid/widget/TextView;

.field private mGammaViewGroup:Landroid/view/View;

.field private mHideColorMode:Z

.field private mHideResolutionMode:Z

.field private mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

.field private mInputUnitSeparator:Landroid/view/View;

.field private mInputUnitText:Landroid/widget/TextView;

.field private mInputUnitViewGroup:Landroid/view/View;

.field private mPrinterLocationIcon:Landroid/widget/ImageView;

.field private mProgressBarLayout:Landroid/view/View;

.field private mScanResolutionText:Landroid/widget/TextView;

.field private mScanResolutionViewGroup:Landroid/view/View;

.field private mScanSizeSelected:Z

.field private mScanSizeSeparator:Landroid/view/View;

.field private mScanSizeText:Landroid/widget/TextView;

.field private mScanSizeViewGroup:Landroid/view/View;

.field private mScannerChanged:Z

.field private mScannerNameText:Landroid/widget/TextView;

.field private mScannerNameViewGroup:Landroid/view/View;

.field private mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method

.method private displayScannerLocationIcon()V
    .locals 3

    .line 272
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 273
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 276
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mPrinterLocationIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f070110

    .line 279
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {v1}, Lepson/scan/activity/ScannerPropertyWrapper;->getLocation()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const v0, 0x7f07010f

    .line 286
    :goto_0
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mPrinterLocationIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 274
    :cond_2
    :goto_1
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mPrinterLocationIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method public static getOldDataValidation(Landroid/content/Intent;)Z
    .locals 2
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v1, "old-preview-valid"

    .line 781
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static getStartIntent(Landroid/content/Context;ZZLcom/epson/iprint/shared/SharedParamScan;)Landroid/content/Intent;
    .locals 2
    .param p3    # Lcom/epson/iprint/shared/SharedParamScan;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 792
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/I2ScanSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "hide-color-mode"

    .line 793
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "hide-resolution"

    .line 794
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "external-scan-params"

    .line 795
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method private goBrightnessSelection()V
    .locals 2

    .line 587
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 588
    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getDensity()I

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v1}, Lepson/scan/i2lib/I2ScanParamArbiter;->getDensityChangeable()Z

    move-result v1

    .line 587
    invoke-static {p0, v0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->getStartIntent(Landroid/content/Context;IZ)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x8

    .line 589
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private goDuplicateSelection()V
    .locals 2

    .line 581
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 582
    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getDuplex()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v1}, Lepson/scan/i2lib/I2ScanParamArbiter;->getDuplexTurnDirection()I

    move-result v1

    .line 581
    invoke-static {p0, v0, v1}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->getStartIntent(Landroid/content/Context;ZI)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x5

    .line 583
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private goGammaSelection()V
    .locals 3

    .line 593
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getGammaCandidates()[Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    move-result-object v0

    .line 595
    new-instance v1, Lepson/scan/activity/ModeListAdapter$Builder;

    .line 596
    invoke-static {p0, v0}, Lepson/scan/activity/DisplayIdConverter;->getGammaStringIdArray(Landroid/content/Context;[Lcom/epson/lib/escani2/EscanI2Lib$Gamma;)[I

    move-result-object v0

    iget-object v2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 597
    invoke-virtual {v2}, Lepson/scan/i2lib/I2ScanParamArbiter;->getGammaPosition()I

    move-result v2

    invoke-direct {v1, v0, v2}, Lepson/scan/activity/ModeListAdapter$Builder;-><init>([II)V

    const v0, 0x7f0e050c

    .line 599
    invoke-static {p0, v1, v0}, Lepson/scan/activity/I2SettingDetailActivity;->getStartIntent(Landroid/content/Context;Lepson/scan/activity/LocalListAdapterBuilder;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x6

    .line 602
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private goImageTypeSelection()V
    .locals 3

    .line 533
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getColorModeCandidates()[Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    move-result-object v0

    .line 534
    new-instance v1, Lepson/scan/activity/ModeListAdapter$Builder;

    .line 535
    invoke-static {v0}, Lepson/scan/activity/DisplayIdConverter;->getColorModeItemIdArray([Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;)[I

    move-result-object v0

    iget-object v2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 536
    invoke-virtual {v2}, Lepson/scan/i2lib/I2ScanParamArbiter;->getColorModePosition()I

    move-result v2

    invoke-direct {v1, v0, v2}, Lepson/scan/activity/ModeListAdapter$Builder;-><init>([II)V

    const v0, 0x7f0e0509

    .line 538
    invoke-static {p0, v1, v0}, Lepson/scan/activity/I2SettingDetailActivity;->getStartIntent(Landroid/content/Context;Lepson/scan/activity/LocalListAdapterBuilder;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    .line 541
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private goInputUnitSelection()V
    .locals 3

    .line 545
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 546
    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getInputUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object v0

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 547
    invoke-virtual {v1}, Lepson/scan/i2lib/I2ScanParamArbiter;->isAdfAlignmentMenuValid()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 548
    invoke-virtual {v1}, Lepson/scan/i2lib/I2ScanParamArbiter;->getManualAdfAlignmentValue()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, -0x1

    .line 545
    :goto_1
    invoke-static {p0, v0, v1}, Lepson/scan/activity/InputUnitSettingActivity;->getStartIntent(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    .line 551
    invoke-virtual {p0, v0, v2}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private goScanResolutionSelection()V
    .locals 3

    .line 568
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getResolutionCandidates()[Ljava/lang/Integer;

    move-result-object v0

    .line 570
    new-instance v1, Lepson/scan/activity/StringListAdapter$Builder;

    .line 571
    invoke-static {p0, v0}, Lepson/scan/activity/DisplayIdConverter;->getScanResolutionStrings(Landroid/content/Context;[Ljava/lang/Integer;)[Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 572
    invoke-virtual {v2}, Lepson/scan/i2lib/I2ScanParamArbiter;->getResolutionPosition()I

    move-result v2

    invoke-direct {v1, v0, v2}, Lepson/scan/activity/StringListAdapter$Builder;-><init>([Ljava/lang/String;I)V

    const v0, 0x7f0e050f

    .line 574
    invoke-static {p0, v1, v0}, Lepson/scan/activity/I2SettingDetailActivity;->getStartIntent(Landroid/content/Context;Lepson/scan/activity/LocalListAdapterBuilder;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    .line 577
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private goScanSizeSelection()V
    .locals 3

    .line 555
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getScanSizeCandidates()[Lcom/epson/lib/escani2/ScanSize;

    move-result-object v0

    .line 557
    new-instance v1, Lepson/scan/activity/ModeListAdapter$Builder;

    .line 558
    invoke-static {v0}, Lepson/scan/activity/DisplayIdConverter;->getScanSizeIdArray([Lcom/epson/lib/escani2/ScanSize;)[I

    move-result-object v0

    iget-boolean v2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeSelected:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 559
    invoke-virtual {v2}, Lepson/scan/i2lib/I2ScanParamArbiter;->getScanSizePosition()I

    move-result v2

    goto :goto_0

    :cond_0
    const/4 v2, -0x1

    :goto_0
    invoke-direct {v1, v0, v2}, Lepson/scan/activity/ModeListAdapter$Builder;-><init>([II)V

    const v0, 0x7f0e04ff

    .line 561
    invoke-static {p0, v1, v0}, Lepson/scan/activity/I2SettingDetailActivity;->getStartIntent(Landroid/content/Context;Lepson/scan/activity/LocalListAdapterBuilder;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    .line 564
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private hideProgress()V
    .locals 2

    .line 230
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mProgressBarLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private isOldPreviewValid(Lepson/scan/activity/ScannerPropertyWrapper;Lcom/epson/lib/escani2/ScanI2Params;)Z
    .locals 4

    .line 451
    invoke-static {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->load(Landroid/content/Context;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object v0

    .line 452
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result v1

    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result v2

    const/4 v3, 0x0

    if-ne v1, v2, :cond_3

    .line 453
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getModelName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getModelName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_1

    .line 457
    :cond_0
    invoke-static {p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object p1

    .line 458
    iget-object v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    iget-object v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne v0, v1, :cond_2

    iget-object v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    iget-object v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    if-ne v0, v1, :cond_2

    iget v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    iget v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    if-ne v0, v1, :cond_2

    iget v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    iget v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    if-ne v0, v1, :cond_2

    iget v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->duplexTurnDirection:I

    iget v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->duplexTurnDirection:I

    if-ne v0, v1, :cond_2

    iget-boolean v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    iget-boolean v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    if-ne v0, v1, :cond_2

    iget v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->manualAdfAlignment:I

    iget v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->manualAdfAlignment:I

    if-ne v0, v1, :cond_2

    iget v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->density:I

    iget v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->density:I

    if-ne v0, v1, :cond_2

    iget-object p1, p1, Lcom/epson/lib/escani2/ScanI2Params;->userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    iget-object p2, p2, Lcom/epson/lib/escani2/ScanI2Params;->userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    if-eq p1, p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_0
    return v3

    :cond_3
    :goto_1
    return v3
.end method

.method private loadData()V
    .locals 3

    .line 221
    invoke-static {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->load(Landroid/content/Context;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 222
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 223
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object v0

    .line 224
    invoke-static {p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v1

    .line 226
    iget-object v2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v2, p0, v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter;->makeSelectionList(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;Lcom/epson/lib/escani2/ScanI2Params;)V

    return-void
.end method

.method private onScannerSelectResult(ILandroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 698
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateIfIpAddressPrinter()V

    .line 699
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->isPrinterSet()Z

    move-result p1

    if-nez p1, :cond_0

    .line 701
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->transitI1SettingActivity()V

    :cond_0
    return-void

    :cond_1
    const/4 p1, 0x1

    .line 707
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerChanged:Z

    const/4 p1, 0x0

    .line 711
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeSelected:Z

    .line 713
    invoke-static {p2}, Lepson/scan/activity/SettingActivityParams;->getScannerPropertyWrapper(Landroid/content/Intent;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 714
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    if-nez p1, :cond_2

    .line 719
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->transitI1SettingActivity()V

    return-void

    .line 723
    :cond_2
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result p1

    const/4 p2, 0x2

    if-eq p1, p2, :cond_3

    .line 726
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->transitI1SettingActivity()V

    return-void

    .line 730
    :cond_3
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    iget-object p2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {p2}, Lepson/scan/activity/ScannerPropertyWrapper;->getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object p2

    invoke-virtual {p1, p0, p2}, Lepson/scan/i2lib/I2ScanParamArbiter;->notifyScannerChanged(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    .line 732
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    return-void
.end method

.method private readEscIVersionChangeIntent(Landroid/content/Intent;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 198
    :cond_0
    invoke-static {p1}, Lepson/scan/activity/SettingActivityParams;->getScannerPropertyWrapper(Landroid/content/Intent;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 199
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    if-nez p1, :cond_1

    return v0

    .line 205
    :cond_1
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result p1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    return v0

    :cond_2
    const/4 p1, 0x1

    .line 210
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerChanged:Z

    .line 212
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 213
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v1, p0, v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->notifyScannerChanged(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    return p1
.end method

.method private saveSettingsAndFinish()V
    .locals 3

    .line 423
    :try_start_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getScanParam()Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v0

    .line 425
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-direct {p0, v1, v0}, Lepson/scan/activity/I2ScanSettingActivity;->isOldPreviewValid(Lepson/scan/activity/ScannerPropertyWrapper;Lcom/epson/lib/escani2/ScanI2Params;)Z

    move-result v1

    .line 427
    invoke-static {p0, v0}, Lepson/scan/i2lib/I2ScanParamManager;->saveParams(Landroid/content/Context;Lcom/epson/lib/escani2/ScanI2Params;)V

    .line 429
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    iget-boolean v2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerChanged:Z

    invoke-virtual {v0, p0, v2}, Lepson/scan/activity/ScannerPropertyWrapper;->saveData(Landroid/content/Context;Z)V

    .line 433
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 434
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "old-preview-valid"

    .line 444
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, -0x1

    .line 445
    invoke-virtual {p0, v1, v0}, Lepson/scan/activity/I2ScanSettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 447
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanSettingActivity;->finish()V

    return-void

    :catch_0
    return-void
.end method

.method private setFieldFromIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 178
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mHideColorMode:Z

    .line 179
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mHideResolutionMode:Z

    const/4 v1, 0x0

    .line 180
    iput-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mExternalScanParams:Lcom/epson/iprint/shared/SharedParamScan;

    :cond_0
    const-string v1, "hide-color-mode"

    .line 183
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mHideColorMode:Z

    const-string v1, "hide-resolution"

    .line 184
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mHideResolutionMode:Z

    const-string v0, "external-scan-params"

    .line 185
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/shared/SharedParamScan;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mExternalScanParams:Lcom/epson/iprint/shared/SharedParamScan;

    return-void
.end method

.method private startSelectScannerActivity()V
    .locals 3

    .line 524
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 525
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getScannerId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 526
    invoke-virtual {v1}, Lepson/scan/activity/ScannerPropertyWrapper;->getLocation()I

    move-result v1

    iget-object v2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 527
    invoke-virtual {v2}, Lepson/scan/activity/ScannerPropertyWrapper;->getSimpleApSsid()Ljava/lang/String;

    move-result-object v2

    .line 524
    invoke-static {p0, v0, v1, v2}, Lepson/scan/activity/ScanSearchActivity;->getStartIntent(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x7

    .line 528
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private transitI1SettingActivity()V
    .locals 8

    .line 739
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lepson/scan/activity/SettingActivityParams;->returnIfEscIVersionChanged(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 741
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 742
    invoke-static {v1}, Lepson/scan/activity/SettingActivityParams;->getSettingReturnIntent(Lepson/scan/activity/ScannerPropertyWrapper;)Landroid/content/Intent;

    move-result-object v1

    .line 741
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 744
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanSettingActivity;->finish()V

    return-void

    .line 749
    :cond_0
    iget-object v3, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    const/4 v4, 0x1

    iget-boolean v5, p0, Lepson/scan/activity/I2ScanSettingActivity;->mHideColorMode:Z

    iget-boolean v6, p0, Lepson/scan/activity/I2ScanSettingActivity;->mHideResolutionMode:Z

    iget-object v7, p0, Lepson/scan/activity/I2ScanSettingActivity;->mExternalScanParams:Lcom/epson/iprint/shared/SharedParamScan;

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lepson/scan/activity/SettingActivityParams;->getChangeSettingActivityStartIntent(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;ZZZLcom/epson/iprint/shared/SharedParamScan;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x64

    .line 752
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updateBrightnessDisplay()V
    .locals 6

    .line 379
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getColorMode()Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    move-result-object v0

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    .line 381
    :goto_0
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mExternalScanParams:Lcom/epson/iprint/shared/SharedParamScan;

    if-eqz v1, :cond_2

    .line 383
    invoke-virtual {v1}, Lcom/epson/iprint/shared/SharedParamScan;->getScan_type()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    .line 387
    :cond_2
    :goto_1
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mBrightnessViewGroup:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 388
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mBrightnessSeparator:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    if-nez v0, :cond_3

    .line 391
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getDensity()I

    move-result v0

    .line 392
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mBrightnessText:Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    rsub-int v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v3

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method private updateDuplexDisplay()V
    .locals 2

    .line 369
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->isDuplexSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    .line 370
    :goto_0
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mDuplexSeparator:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 371
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mDuplexViewGroup:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getDuplex()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 374
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mDuplexText:Landroid/widget/TextView;

    invoke-static {p0, v0}, Lepson/scan/activity/DisplayIdConverter;->getYesNoStringId(Landroid/content/Context;Z)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateGammaDisplay()V
    .locals 2

    .line 397
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getGamma()Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    move-result-object v0

    .line 398
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mGammaText:Landroid/widget/TextView;

    invoke-static {v0}, Lepson/scan/activity/DisplayIdConverter;->getGammaStringId(Lcom/epson/lib/escani2/EscanI2Lib$Gamma;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateIfIpAddressPrinter()V
    .locals 2

    .line 759
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getLocation()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    return-void

    .line 765
    :cond_0
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {v1}, Lepson/scan/activity/ScannerPropertyWrapper;->getScannerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/EPPrinterManager;->doseContainScannerIdInIpPrinterList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 767
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->resetScanner()V

    .line 769
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->tmpReset()V

    .line 771
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    :cond_1
    return-void
.end method

.method private updateImageType()V
    .locals 2

    .line 336
    iget-boolean v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mHideColorMode:Z

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeViewGroup:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeSeparator:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    .line 342
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeViewGroup:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeSeparator:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getColorMode()Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    move-result-object v0

    .line 346
    invoke-static {v0}, Lepson/scan/activity/DisplayIdConverter;->getColorStringId(Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;)I

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0e0471

    .line 351
    :cond_1
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateInputUnitDisplay()V
    .locals 2

    .line 313
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getInputUnitCandidates()[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 315
    :goto_0
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mInputUnitViewGroup:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 316
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mInputUnitSeparator:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getInputUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object v0

    .line 319
    invoke-static {v0}, Lepson/scan/activity/DisplayIdConverter;->getInputUnitStringId(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)I

    move-result v0

    .line 320
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mInputUnitText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateResolution()V
    .locals 2

    .line 355
    iget-boolean v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mHideResolutionMode:Z

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanResolutionViewGroup:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    .line 361
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanResolutionViewGroup:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 363
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getResolution()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 364
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanResolutionText:Landroid/widget/TextView;

    invoke-static {p0, v0}, Lepson/scan/activity/DisplayIdConverter;->getScanResolutionStrings(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateScanParamDisplay()V
    .locals 0

    .line 238
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScannerName()V

    .line 239
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->displayScannerLocationIcon()V

    .line 241
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateInputUnitDisplay()V

    .line 242
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanSize()V

    .line 243
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateImageType()V

    .line 244
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateResolution()V

    .line 246
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateDuplexDisplay()V

    .line 247
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateGammaDisplay()V

    .line 248
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateBrightnessDisplay()V

    .line 250
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanSizeSeparator()V

    return-void
.end method

.method private updateScanSize()V
    .locals 2

    .line 324
    iget-boolean v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeSelected:Z

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getScanSize()Lcom/epson/lib/escani2/ScanSize;

    move-result-object v0

    .line 326
    invoke-static {v0}, Lepson/scan/activity/DisplayIdConverter;->getScanSizeStringId(Lcom/epson/lib/escani2/ScanSize;)I

    move-result v0

    .line 327
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 330
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeText:Landroid/widget/TextView;

    const v1, 0x7f0e0515

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void
.end method

.method private updateScanSizeSeparator()V
    .locals 2

    .line 260
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeViewGroup:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanResolutionViewGroup:Landroid/view/View;

    .line 261
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 265
    :goto_1
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeSeparator:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private updateScannerName()V
    .locals 2

    .line 294
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getLocation()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 295
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 296
    invoke-virtual {v1}, Lepson/scan/activity/ScannerPropertyWrapper;->getScannerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/EPPrinterManager;->getIpPrinterUserName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 298
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerNameText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 304
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 307
    :cond_1
    iget-object v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerNameText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 305
    :cond_2
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerNameText:Landroid/widget/TextView;

    const v1, 0x7f0e04d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/16 v0, 0x64

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq p1, v0, :cond_1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    if-ne p2, v2, :cond_3

    if-eqz p3, :cond_3

    .line 656
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 657
    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->getDensityFromReturnIntent(Landroid/content/Intent;)I

    move-result p2

    .line 658
    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->getChangeAble(Landroid/content/Intent;)Z

    move-result p3

    .line 656
    invoke-virtual {p1, p2, p3}, Lepson/scan/i2lib/I2ScanParamArbiter;->setDensity(IZ)V

    .line 660
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    goto/16 :goto_0

    .line 690
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lepson/scan/activity/I2ScanSettingActivity;->onScannerSelectResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_2
    if-ne p2, v2, :cond_3

    if-eqz p3, :cond_3

    .line 666
    invoke-static {p3}, Lepson/scan/activity/I2SettingDetailActivity;->getResultPosition(Landroid/content/Intent;)I

    move-result p1

    .line 667
    iget-object p2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->setGammaPosition(I)V

    .line 668
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    goto/16 :goto_0

    :pswitch_3
    if-ne p2, v2, :cond_3

    if-eqz p3, :cond_3

    .line 646
    invoke-static {p3}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->getDoubleSideFromReturnIntent(Landroid/content/Intent;)Z

    move-result p1

    .line 647
    iget-object p2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->setDuplexValue(Z)V

    .line 648
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    .line 649
    invoke-static {p3}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->getTurnDirectionFromReturnIntent(Landroid/content/Intent;)I

    move-result p2

    .line 648
    invoke-virtual {p1, p2}, Lepson/scan/i2lib/I2ScanParamArbiter;->setDuplexTurnDirection(I)V

    .line 650
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    goto :goto_0

    :pswitch_4
    if-ne p2, v2, :cond_3

    if-eqz p3, :cond_3

    .line 638
    invoke-static {p3}, Lepson/scan/activity/I2SettingDetailActivity;->getResultPosition(Landroid/content/Intent;)I

    move-result p1

    .line 639
    iget-object p2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->setResolutionPosition(I)V

    .line 640
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    goto :goto_0

    :pswitch_5
    if-ne p2, v2, :cond_3

    if-eqz p3, :cond_3

    .line 611
    invoke-static {p3}, Lepson/scan/activity/I2SettingDetailActivity;->getResultPosition(Landroid/content/Intent;)I

    move-result p1

    .line 612
    iget-object p2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->setColorModePosition(I)V

    .line 613
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    goto :goto_0

    :pswitch_6
    if-ne p2, v2, :cond_3

    if-eqz p3, :cond_3

    .line 618
    iput-boolean v1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeSelected:Z

    .line 619
    invoke-static {p3}, Lepson/scan/activity/I2SettingDetailActivity;->getResultPosition(Landroid/content/Intent;)I

    move-result p1

    .line 620
    iget-object p2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->setScanSizePosition(I)V

    .line 621
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    goto :goto_0

    .line 626
    :pswitch_7
    invoke-static {p3}, Lepson/scan/activity/InputUnitSettingActivity;->getInputUnit(Landroid/content/Intent;)I

    move-result p1

    .line 627
    iget-object p2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->setInputUnitPosition(I)V

    .line 628
    invoke-static {p3}, Lepson/scan/activity/InputUnitSettingActivity;->getAdfAlignment(Landroid/content/Intent;)I

    move-result p1

    if-eq p1, v2, :cond_0

    .line 630
    iget-object p2, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->setManualAdfAlignmentValue(I)V

    .line 632
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    goto :goto_0

    :cond_1
    if-ne p2, v1, :cond_2

    .line 677
    invoke-direct {p0, v2, p3}, Lepson/scan/activity/I2ScanSettingActivity;->onScannerSelectResult(ILandroid/content/Intent;)V

    return-void

    .line 683
    :cond_2
    invoke-virtual {p0, p2, p3}, Lepson/scan/activity/I2ScanSettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 684
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanSettingActivity;->finish()V

    :cond_3
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .line 476
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 478
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackIPPrinterInfo()V

    .line 480
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 487
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0801ee

    if-ne p1, v0, :cond_0

    .line 490
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->startSelectScannerActivity()V

    return-void

    :cond_0
    const v0, 0x7f0801ed

    if-ne p1, v0, :cond_1

    .line 494
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->goInputUnitSelection()V

    return-void

    :cond_1
    const v0, 0x7f0801ec

    if-ne p1, v0, :cond_2

    .line 497
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->goScanSizeSelection()V

    return-void

    :cond_2
    const v0, 0x7f0801e8

    if-ne p1, v0, :cond_3

    .line 501
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->goImageTypeSelection()V

    return-void

    :cond_3
    const v0, 0x7f0801eb

    if-ne p1, v0, :cond_4

    .line 504
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->goScanResolutionSelection()V

    return-void

    :cond_4
    const v0, 0x7f0801e7

    if-ne p1, v0, :cond_5

    .line 508
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->goDuplicateSelection()V

    return-void

    :cond_5
    const v0, 0x7f0801e9

    if-ne p1, v0, :cond_6

    .line 512
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->goBrightnessSelection()V

    return-void

    :cond_6
    const v0, 0x7f0801ea

    if-ne p1, v0, :cond_7

    .line 516
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->goGammaSelection()V

    return-void

    :cond_7
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 111
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 113
    new-instance p1, Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-direct {p1}, Lepson/scan/i2lib/I2ScanParamArbiter;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mI2ScanParamArbiter:Lepson/scan/i2lib/I2ScanParamArbiter;

    const/4 p1, 0x0

    .line 114
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeSelected:Z

    const p1, 0x7f0a002d

    .line 116
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->setContentView(I)V

    const p1, 0x7f0e04d8

    const/4 v0, 0x1

    .line 117
    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/I2ScanSettingActivity;->setActionBar(IZ)V

    const p1, 0x7f0801ee

    .line 119
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerNameViewGroup:Landroid/view/View;

    .line 120
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerNameViewGroup:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080358

    .line 121
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScannerNameText:Landroid/widget/TextView;

    const p1, 0x7f080198

    .line 122
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mPrinterLocationIcon:Landroid/widget/ImageView;

    const p1, 0x7f0801ed

    .line 124
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mInputUnitViewGroup:Landroid/view/View;

    .line 125
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mInputUnitViewGroup:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08035c

    .line 126
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mInputUnitText:Landroid/widget/TextView;

    const p1, 0x7f0801ec

    .line 128
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeViewGroup:Landroid/view/View;

    .line 129
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeViewGroup:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08035b

    .line 130
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeText:Landroid/widget/TextView;

    const p1, 0x7f0802db

    .line 131
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanSizeSeparator:Landroid/view/View;

    const p1, 0x7f0801e8

    .line 133
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeViewGroup:Landroid/view/View;

    .line 134
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeViewGroup:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080355

    .line 135
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeText:Landroid/widget/TextView;

    const p1, 0x7f0800d3

    .line 136
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mColorModeSeparator:Landroid/view/View;

    const p1, 0x7f0801eb

    .line 138
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanResolutionViewGroup:Landroid/view/View;

    .line 139
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanResolutionViewGroup:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08035a

    .line 140
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mScanResolutionText:Landroid/widget/TextView;

    const p1, 0x7f0801e7

    .line 142
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mDuplexViewGroup:Landroid/view/View;

    .line 143
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mDuplexViewGroup:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080354

    .line 144
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mDuplexText:Landroid/widget/TextView;

    const p1, 0x7f0801e9

    .line 146
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mBrightnessViewGroup:Landroid/view/View;

    .line 147
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mBrightnessViewGroup:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 148
    iget-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mBrightnessViewGroup:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080356

    .line 149
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mBrightnessText:Landroid/widget/TextView;

    const p1, 0x7f08007f

    .line 150
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0801ea

    .line 152
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mGammaViewGroup:Landroid/view/View;

    .line 153
    iget-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mGammaViewGroup:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080357

    .line 154
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mGammaText:Landroid/widget/TextView;

    const v0, 0x7f080389

    .line 157
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mInputUnitSeparator:Landroid/view/View;

    const v0, 0x7f080388

    .line 158
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2ScanSettingActivity;->mDuplexSeparator:Landroid/view/View;

    .line 159
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mBrightnessSeparator:Landroid/view/View;

    const p1, 0x7f080297

    .line 163
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanSettingActivity;->mProgressBarLayout:Landroid/view/View;

    .line 165
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->setFieldFromIntent(Landroid/content/Intent;)V

    .line 169
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanSettingActivity;->readEscIVersionChangeIntent(Landroid/content/Intent;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 170
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->loadData()V

    .line 173
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->updateScanParamDisplay()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 403
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanSettingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 410
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-ne v0, v1, :cond_0

    .line 412
    invoke-direct {p0}, Lepson/scan/activity/I2ScanSettingActivity;->saveSettingsAndFinish()V

    const/4 p1, 0x1

    return p1

    .line 416
    :cond_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

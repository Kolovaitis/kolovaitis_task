.class Lepson/scan/activity/ScanBaseView$PointInfo;
.super Ljava/lang/Object;
.source "ScanBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PointInfo"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanBaseView;

.field private x:D

.field private y:D


# direct methods
.method public constructor <init>(Lepson/scan/activity/ScanBaseView;DD)V
    .locals 2

    .line 67
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->this$0:Lepson/scan/activity/ScanBaseView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 65
    iput-wide v0, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->x:D

    .line 66
    iput-wide v0, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->y:D

    .line 68
    iput-wide p2, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->x:D

    .line 69
    iput-wide p4, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->y:D

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D
    .locals 2

    .line 64
    iget-wide v0, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->x:D

    return-wide v0
.end method

.method static synthetic access$002(Lepson/scan/activity/ScanBaseView$PointInfo;D)D
    .locals 0

    .line 64
    iput-wide p1, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->x:D

    return-wide p1
.end method

.method static synthetic access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D
    .locals 2

    .line 64
    iget-wide v0, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->y:D

    return-wide v0
.end method

.method static synthetic access$102(Lepson/scan/activity/ScanBaseView$PointInfo;D)D
    .locals 0

    .line 64
    iput-wide p1, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->y:D

    return-wide p1
.end method


# virtual methods
.method public getX()D
    .locals 2

    .line 72
    iget-wide v0, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .line 78
    iget-wide v0, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->y:D

    return-wide v0
.end method

.method public setX(D)V
    .locals 0

    .line 75
    iput-wide p1, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->x:D

    return-void
.end method

.method public setY(D)V
    .locals 0

    .line 81
    iput-wide p1, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->y:D

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 86
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "(%.2f,%.2f)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-wide v3, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->x:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-wide v3, p0, Lepson/scan/activity/ScanBaseView$PointInfo;->y:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

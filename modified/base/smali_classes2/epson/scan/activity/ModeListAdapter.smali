.class public Lepson/scan/activity/ModeListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ModeListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/ModeListAdapter$Builder;
    }
.end annotation


# instance fields
.field private mCurrentPosition:I

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLeftImageId:[I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private mStringIdArray:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;[II)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .line 33
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 34
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ModeListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 35
    iput-object p2, p0, Lepson/scan/activity/ModeListAdapter;->mStringIdArray:[I

    .line 36
    iput p3, p0, Lepson/scan/activity/ModeListAdapter;->mCurrentPosition:I

    const/4 p1, 0x0

    .line 37
    iput-object p1, p0, Lepson/scan/activity/ModeListAdapter;->mLeftImageId:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[I[II)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .line 45
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ModeListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    if-eqz p2, :cond_0

    goto :goto_0

    .line 47
    :cond_0
    array-length p1, p3

    new-array p2, p1, [I

    :goto_0
    iput-object p2, p0, Lepson/scan/activity/ModeListAdapter;->mStringIdArray:[I

    .line 48
    iput p4, p0, Lepson/scan/activity/ModeListAdapter;->mCurrentPosition:I

    .line 49
    iput-object p3, p0, Lepson/scan/activity/ModeListAdapter;->mLeftImageId:[I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 54
    iget-object v0, p0, Lepson/scan/activity/ModeListAdapter;->mStringIdArray:[I

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    .line 59
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getPosition()I
    .locals 1

    .line 80
    iget v0, p0, Lepson/scan/activity/ModeListAdapter;->mCurrentPosition:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 87
    iget-object p2, p0, Lepson/scan/activity/ModeListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a0076

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 90
    :cond_0
    iget-object p3, p0, Lepson/scan/activity/ModeListAdapter;->mLeftImageId:[I

    if-eqz p3, :cond_1

    const p3, 0x7f0801be

    .line 91
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 92
    iget-object v1, p0, Lepson/scan/activity/ModeListAdapter;->mLeftImageId:[I

    aget v1, v1, p1

    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 93
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 96
    :cond_1
    iget-object p3, p0, Lepson/scan/activity/ModeListAdapter;->mStringIdArray:[I

    aget p3, p3, v0

    if-eqz p3, :cond_2

    const p3, 0x7f08035d

    .line 97
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 98
    iget-object v1, p0, Lepson/scan/activity/ModeListAdapter;->mStringIdArray:[I

    aget v1, v1, p1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    const p3, 0x7f0801b4

    .line 101
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 102
    iget v1, p0, Lepson/scan/activity/ModeListAdapter;->mCurrentPosition:I

    if-ne p1, v1, :cond_3

    goto :goto_0

    :cond_3
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-object p2
.end method

.method public setPosition(I)V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .line 69
    iput p1, p0, Lepson/scan/activity/ModeListAdapter;->mCurrentPosition:I

    .line 70
    iget p1, p0, Lepson/scan/activity/ModeListAdapter;->mCurrentPosition:I

    if-gez p1, :cond_0

    const/4 p1, 0x0

    .line 71
    iput p1, p0, Lepson/scan/activity/ModeListAdapter;->mCurrentPosition:I

    goto :goto_0

    .line 72
    :cond_0
    invoke-virtual {p0}, Lepson/scan/activity/ModeListAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 73
    invoke-virtual {p0}, Lepson/scan/activity/ModeListAdapter;->getCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lepson/scan/activity/ModeListAdapter;->mCurrentPosition:I

    .line 76
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lepson/scan/activity/ModeListAdapter;->notifyDataSetChanged()V

    return-void
.end method

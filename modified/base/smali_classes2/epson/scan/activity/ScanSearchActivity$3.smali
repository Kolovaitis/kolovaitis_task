.class Lepson/scan/activity/ScanSearchActivity$3;
.super Ljava/lang/Object;
.source "ScanSearchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/scan/activity/ScanSearchActivity;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSearchActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 384
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 389
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$100(Lepson/scan/activity/ScanSearchActivity;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 392
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$200(Lepson/scan/activity/ScanSearchActivity;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_2

    .line 393
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    const/4 p4, 0x0

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-static {p1, p4}, Lepson/scan/activity/ScanSearchActivity;->access$202(Lepson/scan/activity/ScanSearchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 398
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1, p2}, Lepson/scan/activity/ScanSearchActivity;->access$102(Lepson/scan/activity/ScanSearchActivity;Z)Z

    .line 401
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget-object p1, p1, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {p1}, Lepson/scan/activity/ScannerListAdapter;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    .line 404
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p2

    .line 405
    sget-object p3, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    iget-object p4, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p4, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectType(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_1

    .line 407
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$300(Lepson/scan/activity/ScanSearchActivity;)V

    .line 408
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    const/4 p3, 0x7

    invoke-static {p1, p2, p3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->connect(Landroid/app/Activity;Ljava/lang/String;I)Z

    return-void

    .line 413
    :cond_1
    iget-object p2, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p2}, Lepson/scan/activity/ScanSearchActivity;->access$400(Lepson/scan/activity/ScanSearchActivity;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/print/MyPrinter;->setLocation(I)V

    .line 414
    iget-object p2, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p2, p1}, Lepson/scan/activity/ScanSearchActivity;->access$600(Lepson/scan/activity/ScanSearchActivity;Lepson/print/MyPrinter;)Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lepson/scan/activity/ScanSearchActivity;->access$502(Lepson/scan/activity/ScanSearchActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 416
    iget-object p2, p0, Lepson/scan/activity/ScanSearchActivity$3;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p2, p1}, Lepson/scan/activity/ScanSearchActivity;->access$700(Lepson/scan/activity/ScanSearchActivity;Lepson/print/MyPrinter;)V

    return-void

    :cond_2
    return-void
.end method

.class Lepson/scan/activity/ScanSearchActivity$FindScannerTask;
.super Landroid/os/AsyncTask;
.source "ScanSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FindScannerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mCancelableFind:Lepson/scan/lib/CancelableFind;

.field final synthetic this$0:Lepson/scan/activity/ScanSearchActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSearchActivity;Lepson/scan/lib/escanLib;)V
    .locals 0

    .line 1321
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1322
    new-instance p1, Lepson/scan/lib/CancelableFind;

    invoke-direct {p1}, Lepson/scan/lib/CancelableFind;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->mCancelableFind:Lepson/scan/lib/CancelableFind;

    .line 1323
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->mCancelableFind:Lepson/scan/lib/CancelableFind;

    invoke-virtual {p1, p2}, Lepson/scan/lib/CancelableFind;->setEscanLib(Lepson/scan/lib/escanLib;)V

    return-void
.end method


# virtual methods
.method public cancelFind()V
    .locals 1

    .line 1365
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->mCancelableFind:Lepson/scan/lib/CancelableFind;

    invoke-virtual {v0}, Lepson/scan/lib/CancelableFind;->cancel()V

    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .line 1328
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1800(Lepson/scan/activity/ScanSearchActivity;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 1329
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 1334
    :cond_0
    :try_start_0
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 1336
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->mCancelableFind:Lepson/scan/lib/CancelableFind;

    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanSearchActivity;->access$2000(Lepson/scan/activity/ScanSearchActivity;)Lepson/scan/lib/escanLib;

    move-result-object v1

    invoke-virtual {p1, v1}, Lepson/scan/lib/CancelableFind;->setEscanLib(Lepson/scan/lib/escanLib;)V

    .line 1337
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->mCancelableFind:Lepson/scan/lib/CancelableFind;

    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v2}, Lepson/scan/activity/ScanSearchActivity;->access$1000(Lepson/scan/activity/ScanSearchActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lepson/scan/lib/CancelableFind;->findScanner(Landroid/content/Context;Landroid/os/Handler;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 1343
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    throw p1

    :catch_0
    :goto_0
    invoke-static {}, Lepson/scan/activity/ScanSearchActivity;->access$1900()Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1346
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1318
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled(Ljava/lang/Boolean;)V
    .locals 0

    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .line 1318
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->onCancelled(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    .line 1351
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lepson/scan/activity/ScanSearchActivity;->access$1702(Lepson/scan/activity/ScanSearchActivity;Z)Z

    .line 1352
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->mCancelableFind:Lepson/scan/lib/CancelableFind;

    invoke-virtual {p1}, Lepson/scan/lib/CancelableFind;->isCanceled()Z

    move-result p1

    if-nez p1, :cond_0

    .line 1353
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1500(Lepson/scan/activity/ScanSearchActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1354
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1600(Lepson/scan/activity/ScanSearchActivity;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1318
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSearchActivity$FindScannerTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

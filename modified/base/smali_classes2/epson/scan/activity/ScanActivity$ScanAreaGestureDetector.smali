.class Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ScanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScanAreaGestureDetector"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 2216
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .line 2230
    iget-object p3, p0, Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p3}, Lepson/scan/activity/ScanActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p3

    .line 2231
    invoke-interface {p3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p3

    .line 2233
    new-instance p4, Landroid/graphics/Point;

    invoke-direct {p4}, Landroid/graphics/Point;-><init>()V

    .line 2234
    invoke-virtual {p3, p4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2236
    iget p3, p4, Landroid/graphics/Point;->y:I

    int-to-float p3, p3

    .line 2237
    iget p4, p4, Landroid/graphics/Point;->x:I

    int-to-float p4, p4

    cmpl-float v0, p3, p4

    if-lez v0, :cond_0

    move p3, p4

    .line 2246
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result p1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result p2

    sub-float/2addr p1, p2

    float-to-int p1, p1

    int-to-double p1, p1

    neg-float p4, p3

    float-to-double v0, p4

    const-wide v2, 0x3fb999999999999aL    # 0.1

    mul-double v0, v0, v2

    cmpg-double p4, p1, v0

    if-gez p4, :cond_1

    .line 2250
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$5300(Lepson/scan/activity/ScanActivity;)V

    goto :goto_0

    :cond_1
    float-to-double p3, p3

    mul-double p3, p3, v2

    cmpl-double v0, p1, p3

    if-lez v0, :cond_2

    .line 2253
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$5400(Lepson/scan/activity/ScanActivity;)V

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    .line 2220
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;->this$0:Lepson/scan/activity/ScanActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lepson/scan/activity/ScanActivity;->bLongTouch:Z

    .line 2221
    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$100(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    .line 2222
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.class Lepson/scan/activity/ScanActivity$6;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 891
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$6;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 893
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$6;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2000(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 897
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$6;->this$0:Lepson/scan/activity/ScanActivity;

    iget v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    .line 898
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$6;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    if-ge p1, v1, :cond_1

    .line 899
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$6;->this$0:Lepson/scan/activity/ScanActivity;

    iget v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    add-int/2addr v0, v1

    iput v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    return-void

    .line 903
    :cond_1
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$6;->this$0:Lepson/scan/activity/ScanActivity;

    iget v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$2100(Lepson/scan/activity/ScanActivity;I)V

    .line 904
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$6;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2200(Lepson/scan/activity/ScanActivity;)V

    .line 905
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$6;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$100(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    return-void
.end method

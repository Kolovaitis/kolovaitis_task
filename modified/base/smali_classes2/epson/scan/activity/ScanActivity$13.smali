.class Lepson/scan/activity/ScanActivity$13;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 1402
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 1406
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-lez p1, :cond_3

    .line 1407
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2000(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 1411
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$3700(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 1412
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 1414
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$3100(Lepson/scan/activity/ScanActivity;)Lcom/epson/iprint/shared/SharedParamScan;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const-string v0, "scan-file"

    .line 1418
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$3100(Lepson/scan/activity/ScanActivity;)Lcom/epson/iprint/shared/SharedParamScan;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/iprint/shared/SharedParamScan;->getArrayOutFilePath()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1419
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1}, Lepson/scan/activity/ScanActivity;->setResult(ILandroid/content/Intent;)V

    .line 1421
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->finish()V

    return-void

    .line 1426
    :cond_2
    new-instance p1, Landroid/content/Intent;

    iget-object v1, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    const-class v2, Lepson/server/screens/StorageServer;

    invoke-direct {p1, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "PICK_SERVER_FOR"

    const/4 v2, 0x2

    .line 1428
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "SAVING_FILE_PATH"

    .line 1429
    iget-object v2, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v2}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "forLoggerCallFrom"

    .line 1431
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1433
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$13;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1, p1, v0}, Lepson/scan/activity/ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_3
    return-void
.end method

.class Lepson/scan/activity/I2ScanActivity$LocalHandler;
.super Landroid/os/Handler;
.source "I2ScanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/I2ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocalHandler"
.end annotation


# static fields
.field static final MESSAGE_UPDATE_PREVIEW:I = 0x1


# instance fields
.field private final mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lepson/scan/activity/I2ScanActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lepson/scan/activity/I2ScanActivity;)V
    .locals 1

    .line 1073
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1074
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity$LocalHandler;->mActivity:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .line 1079
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    return-void

    .line 1081
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity$LocalHandler;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/scan/activity/I2ScanActivity;

    if-eqz v0, :cond_2

    .line 1082
    invoke-virtual {v0}, Lepson/scan/activity/I2ScanActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 1085
    :cond_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    iget p1, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1, p1}, Lepson/scan/activity/I2ScanActivity;->access$1000(Lepson/scan/activity/I2ScanActivity;II)V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method sendUpdateScanProgressPreviewMessage(II)V
    .locals 1

    const/4 v0, 0x1

    .line 1099
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity$LocalHandler;->removeMessages(I)V

    .line 1100
    invoke-virtual {p0, v0, p1, p2}, Lepson/scan/activity/I2ScanActivity$LocalHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    .line 1101
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanActivity$LocalHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

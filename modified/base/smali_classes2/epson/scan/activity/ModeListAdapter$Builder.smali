.class public Lepson/scan/activity/ModeListAdapter$Builder;
.super Ljava/lang/Object;
.source "ModeListAdapter.java"

# interfaces
.implements Lepson/scan/activity/LocalListAdapterBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ModeListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCurrentPosition:I

.field private mStringIdArray:[I


# direct methods
.method public constructor <init>([II)V
    .locals 0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p1, p0, Lepson/scan/activity/ModeListAdapter$Builder;->mStringIdArray:[I

    .line 122
    iput p2, p0, Lepson/scan/activity/ModeListAdapter$Builder;->mCurrentPosition:I

    return-void
.end method


# virtual methods
.method public build(Landroid/content/Context;)Landroid/widget/ListAdapter;
    .locals 3

    .line 132
    new-instance v0, Lepson/scan/activity/ModeListAdapter;

    iget-object v1, p0, Lepson/scan/activity/ModeListAdapter$Builder;->mStringIdArray:[I

    iget v2, p0, Lepson/scan/activity/ModeListAdapter$Builder;->mCurrentPosition:I

    invoke-direct {v0, p1, v1, v2}, Lepson/scan/activity/ModeListAdapter;-><init>(Landroid/content/Context;[II)V

    return-object v0
.end method

.class public Lepson/scan/activity/ScanBaseView;
.super Lepson/print/ActivityIACommon;
.source "ScanBaseView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/ScanBaseView$ScanAreaBackground;,
        Lepson/scan/activity/ScanBaseView$ScanAreaSet;,
        Lepson/scan/activity/ScanBaseView$PointInfo;
    }
.end annotation


# static fields
.field public static SCAN_AREA_HOTSPOT_SIZE:I = 0x28

.field private static final SCAN_REDRAWBITMAP:I = -0x64

.field private static final TAG:Ljava/lang/String; = "ScanBaseView"


# instance fields
.field public final LINE_AREA_BORDER:I

.field public final LINE_CORNER_BIG:I

.field public final LINE_CORNER_BIG_WIDTH:I

.field public final LINE_CORNER_SMALL:I

.field public final LINE_CORNER_SMALL_WIDTH:I

.field public final MARGIN_LEFT_RIGHT:I

.field public MARGIN_TOP_BOT:I

.field final MAX_DASHEFFECT_PHASE:I

.field public final SCREEN_STATUS_BOTLEFT_TOPRIGHT:I

.field public final SCREEN_STATUS_BOTRIGHT_TOPLEFT:I

.field public final SCREEN_STATUS_TOPLEFT_BOTRIGHT:I

.field public final SCREEN_STATUS_TOPRIGHT_BOTLEFT:I

.field private _bmOption:Landroid/graphics/BitmapFactory$Options;

.field public bLongTouch:Z

.field public bmCenter:Landroid/graphics/Bitmap;

.field private bmRectF:Landroid/graphics/RectF;

.field context:Landroid/content/Context;

.field curentViewingFile:I

.field escan:Lepson/scan/lib/escanLib;

.field private first:Lepson/scan/activity/ScanBaseView$PointInfo;

.field private inSampleSize:I

.field private isDisplayResult:Z

.field public isDoctable:Z

.field private isSetSize:Z

.field private isValidArea:Z

.field private listSavedJPGFilePath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBaseBot:Landroid/graphics/Point;

.field private mBaseTop:Landroid/graphics/Point;

.field private mBitmap:Landroid/graphics/Bitmap;

.field mBot:Landroid/graphics/Point;

.field mCenter:Landroid/graphics/Point;

.field mFirstTouch:Landroid/graphics/Point;

.field private mPaint:Landroid/graphics/Paint;

.field mTop:Landroid/graphics/Point;

.field mTouch:Landroid/graphics/Point;

.field mWhere:I

.field private maxScanHeightOnScreen:D

.field private maxScanWidthOnScreen:D

.field private maxheight:I

.field private maxwidth:I

.field phase:I

.field private piBaseBot:Lepson/scan/activity/ScanBaseView$PointInfo;

.field private piBaseTop:Lepson/scan/activity/ScanBaseView$PointInfo;

.field private rectScanArea:Landroid/graphics/RectF;

.field private scale:D

.field private screenStatus:I

.field private second:Lepson/scan/activity/ScanBaseView$PointInfo;

.field totalScanned:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .line 313
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v6, 0x1

    .line 35
    iput v6, p0, Lepson/scan/activity/ScanBaseView;->SCREEN_STATUS_TOPLEFT_BOTRIGHT:I

    const/4 v7, 0x2

    .line 36
    iput v7, p0, Lepson/scan/activity/ScanBaseView;->SCREEN_STATUS_BOTLEFT_TOPRIGHT:I

    const/4 v0, 0x3

    .line 37
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->SCREEN_STATUS_BOTRIGHT_TOPLEFT:I

    const/4 v0, 0x4

    .line 38
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->SCREEN_STATUS_TOPRIGHT_BOTLEFT:I

    const/16 v0, 0x3c

    .line 41
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->MARGIN_TOP_BOT:I

    const/16 v1, 0xa

    .line 42
    iput v1, p0, Lepson/scan/activity/ScanBaseView;->MARGIN_LEFT_RIGHT:I

    const/4 v1, 0x6

    .line 44
    iput v1, p0, Lepson/scan/activity/ScanBaseView;->LINE_CORNER_SMALL:I

    .line 45
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->LINE_CORNER_SMALL_WIDTH:I

    const/16 v0, 0x12

    .line 46
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->LINE_CORNER_BIG:I

    const/16 v0, 0x96

    .line 47
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->LINE_CORNER_BIG_WIDTH:I

    .line 49
    iput v7, p0, Lepson/scan/activity/ScanBaseView;->LINE_AREA_BORDER:I

    const/4 v8, 0x0

    .line 56
    iput-boolean v8, p0, Lepson/scan/activity/ScanBaseView;->isDoctable:Z

    .line 57
    iput v8, p0, Lepson/scan/activity/ScanBaseView;->maxwidth:I

    .line 58
    iput v8, p0, Lepson/scan/activity/ScanBaseView;->maxheight:I

    .line 60
    iput-boolean v8, p0, Lepson/scan/activity/ScanBaseView;->isSetSize:Z

    .line 62
    iput-boolean v8, p0, Lepson/scan/activity/ScanBaseView;->bLongTouch:Z

    .line 96
    iput v6, p0, Lepson/scan/activity/ScanBaseView;->screenStatus:I

    const-wide/16 v0, 0x0

    .line 104
    iput-wide v0, p0, Lepson/scan/activity/ScanBaseView;->maxScanHeightOnScreen:D

    .line 105
    iput-wide v0, p0, Lepson/scan/activity/ScanBaseView;->maxScanWidthOnScreen:D

    .line 109
    new-instance v9, Lepson/scan/activity/ScanBaseView$PointInfo;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    move-object v0, v9

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    iput-object v9, p0, Lepson/scan/activity/ScanBaseView;->piBaseTop:Lepson/scan/activity/ScanBaseView$PointInfo;

    .line 110
    new-instance v9, Lepson/scan/activity/ScanBaseView$PointInfo;

    move-object v0, v9

    invoke-direct/range {v0 .. v5}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    iput-object v9, p0, Lepson/scan/activity/ScanBaseView;->piBaseBot:Lepson/scan/activity/ScanBaseView$PointInfo;

    .line 130
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBaseTop:Landroid/graphics/Point;

    .line 131
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBaseBot:Landroid/graphics/Point;

    .line 133
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    .line 134
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    .line 135
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->mCenter:Landroid/graphics/Point;

    .line 136
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    .line 137
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->mFirstTouch:Landroid/graphics/Point;

    .line 143
    iput v6, p0, Lepson/scan/activity/ScanBaseView;->curentViewingFile:I

    .line 144
    iput v8, p0, Lepson/scan/activity/ScanBaseView;->totalScanned:I

    .line 153
    iput v7, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    .line 329
    new-instance v6, Lepson/scan/activity/ScanBaseView$PointInfo;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    iput-object v6, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    .line 330
    new-instance v6, Lepson/scan/activity/ScanBaseView$PointInfo;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    iput-object v6, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    .line 331
    iput-boolean v8, p0, Lepson/scan/activity/ScanBaseView;->isValidArea:Z

    .line 415
    iput-boolean v8, p0, Lepson/scan/activity/ScanBaseView;->isDisplayResult:Z

    .line 416
    iput v8, p0, Lepson/scan/activity/ScanBaseView;->phase:I

    const/16 v0, 0x3e8

    .line 417
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->MAX_DASHEFFECT_PHASE:I

    .line 315
    new-instance v0, Lepson/scan/lib/escanLib;

    invoke-direct {v0}, Lepson/scan/lib/escanLib;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    .line 316
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0, v8}, Lepson/scan/lib/escanLib;->setJobDone(Z)V

    .line 318
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->_bmOption:Landroid/graphics/BitmapFactory$Options;

    .line 319
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 320
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->mPaint:Landroid/graphics/Paint;

    .line 321
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 322
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 325
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->listSavedJPGFilePath:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$200(Lepson/scan/activity/ScanBaseView;)Z
    .locals 0

    .line 33
    iget-boolean p0, p0, Lepson/scan/activity/ScanBaseView;->isDisplayResult:Z

    return p0
.end method

.method static synthetic access$300(Lepson/scan/activity/ScanBaseView;)Z
    .locals 0

    .line 33
    iget-boolean p0, p0, Lepson/scan/activity/ScanBaseView;->isSetSize:Z

    return p0
.end method

.method static synthetic access$400(Lepson/scan/activity/ScanBaseView;)I
    .locals 0

    .line 33
    iget p0, p0, Lepson/scan/activity/ScanBaseView;->screenStatus:I

    return p0
.end method

.method static synthetic access$500(Lepson/scan/activity/ScanBaseView;)Landroid/graphics/BitmapFactory$Options;
    .locals 0

    .line 33
    iget-object p0, p0, Lepson/scan/activity/ScanBaseView;->_bmOption:Landroid/graphics/BitmapFactory$Options;

    return-object p0
.end method

.method static synthetic access$600(Lepson/scan/activity/ScanBaseView;)I
    .locals 0

    .line 33
    iget p0, p0, Lepson/scan/activity/ScanBaseView;->inSampleSize:I

    return p0
.end method


# virtual methods
.method public calculateTheCenterPoint(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 4

    .line 394
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 395
    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p2, Landroid/graphics/Point;->x:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p2, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 396
    iget v1, p1, Landroid/graphics/Point;->y:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget p1, p1, Landroid/graphics/Point;->y:I

    iget p2, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    add-int/2addr v1, p1

    iput v1, v0, Landroid/graphics/Point;->y:I

    return-object v0
.end method

.method public detectTheScreenStatus()V
    .locals 2

    .line 939
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    .line 941
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->screenStatus:I

    goto :goto_0

    .line 942
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x2

    .line 944
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->screenStatus:I

    goto :goto_0

    .line 945
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_2

    const/4 v0, 0x3

    .line 947
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->screenStatus:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    .line 950
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->screenStatus:I

    :goto_0
    return-void
.end method

.method public detectTheTouchPoint()V
    .locals 6

    .line 806
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    if-gt v0, v1, :cond_b

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lt v0, v1, :cond_b

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    .line 807
    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    if-gt v0, v1, :cond_b

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lt v0, v1, :cond_b

    .line 808
    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "touche in"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_2

    .line 813
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_0

    .line 815
    iput v4, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 816
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_1

    .line 818
    iput v2, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 821
    :cond_1
    iput v3, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 824
    :cond_2
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_5

    .line 826
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_3

    .line 828
    iput v4, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 829
    :cond_3
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_4

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_4

    .line 831
    iput v2, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 834
    :cond_4
    iput v3, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 836
    :cond_5
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_8

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_8

    .line 838
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_6

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_6

    .line 840
    iput v4, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 841
    :cond_6
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_7

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_7

    .line 843
    iput v2, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 846
    :cond_7
    iput v3, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 850
    :cond_8
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_9

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_9

    .line 852
    iput v4, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 853
    :cond_9
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_a

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_a

    .line 855
    iput v2, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 858
    :cond_a
    iput v3, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 863
    :cond_b
    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "touche out"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_c

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_d

    :cond_c
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 865
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_e

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_d

    goto :goto_0

    :cond_d
    const/4 v0, 0x2

    .line 929
    iput v0, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 867
    :cond_e
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_15

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_15

    .line 869
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_f

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_10

    :cond_f
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 870
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_11

    .line 872
    :cond_10
    iput v4, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 873
    :cond_11
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_12

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_13

    :cond_12
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 874
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_14

    .line 876
    :cond_13
    iput v2, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 879
    :cond_14
    iput v3, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 882
    :cond_15
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_1c

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_1c

    .line 884
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_16

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_17

    :cond_16
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 885
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_18

    .line 887
    :cond_17
    iput v4, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 888
    :cond_18
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_19

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_1a

    :cond_19
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 889
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_1b

    .line 891
    :cond_1a
    iput v2, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 894
    :cond_1b
    iput v3, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 897
    :cond_1c
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_23

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_23

    .line 899
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_1d

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_1e

    :cond_1d
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 900
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_1f

    .line 902
    :cond_1e
    iput v4, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto/16 :goto_1

    .line 903
    :cond_1f
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_20

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_21

    :cond_20
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 904
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_22

    .line 906
    :cond_21
    iput v2, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto :goto_1

    .line 909
    :cond_22
    iput v3, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto :goto_1

    .line 913
    :cond_23
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_24

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_25

    :cond_24
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 914
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_26

    .line 916
    :cond_25
    iput v4, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto :goto_1

    .line 917
    :cond_26
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_27

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-lt v0, v1, :cond_28

    :cond_27
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 918
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lepson/scan/activity/ScanBaseView;->SCAN_AREA_HOTSPOT_SIZE:I

    if-ge v0, v1, :cond_29

    .line 920
    :cond_28
    iput v2, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    goto :goto_1

    .line 923
    :cond_29
    iput v3, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    :goto_1
    return-void
.end method

.method public getBm()Landroid/graphics/Bitmap;
    .locals 1

    .line 274
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBmCenter()Landroid/graphics/Bitmap;
    .locals 1

    .line 238
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBmRectF()Landroid/graphics/RectF;
    .locals 1

    .line 268
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->bmRectF:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .line 160
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getEscan()Lepson/scan/lib/escanLib;
    .locals 1

    .line 184
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    return-object v0
.end method

.method public getInSampleSize()I
    .locals 1

    .line 256
    iget v0, p0, Lepson/scan/activity/ScanBaseView;->inSampleSize:I

    return v0
.end method

.method public getListSavedJPGFilePath()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->listSavedJPGFilePath:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMaxScanHeightOnScreen()D
    .locals 2

    .line 298
    iget-wide v0, p0, Lepson/scan/activity/ScanBaseView;->maxScanHeightOnScreen:D

    return-wide v0
.end method

.method public getMaxScanWidthOnScreen()D
    .locals 2

    .line 304
    iget-wide v0, p0, Lepson/scan/activity/ScanBaseView;->maxScanWidthOnScreen:D

    return-wide v0
.end method

.method public getMaxheight()I
    .locals 1

    .line 202
    iget v0, p0, Lepson/scan/activity/ScanBaseView;->maxheight:I

    return v0
.end method

.method public getMaxwidth()I
    .locals 1

    .line 196
    iget v0, p0, Lepson/scan/activity/ScanBaseView;->maxwidth:I

    return v0
.end method

.method public getPiBaseBot()Lepson/scan/activity/ScanBaseView$PointInfo;
    .locals 1

    .line 119
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->piBaseBot:Lepson/scan/activity/ScanBaseView$PointInfo;

    return-object v0
.end method

.method public getPiBaseTop()Lepson/scan/activity/ScanBaseView$PointInfo;
    .locals 1

    .line 112
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->piBaseTop:Lepson/scan/activity/ScanBaseView$PointInfo;

    return-object v0
.end method

.method public getRectScanArea()Landroid/graphics/RectF;
    .locals 1

    .line 178
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->rectScanArea:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getScale()D
    .locals 2

    .line 190
    iget-wide v0, p0, Lepson/scan/activity/ScanBaseView;->scale:D

    return-wide v0
.end method

.method public getScreenStatus()I
    .locals 1

    .line 244
    iget v0, p0, Lepson/scan/activity/ScanBaseView;->screenStatus:I

    return v0
.end method

.method public get_bmOption()Landroid/graphics/BitmapFactory$Options;
    .locals 1

    .line 250
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->_bmOption:Landroid/graphics/BitmapFactory$Options;

    return-object v0
.end method

.method public getmBaseBot()Landroid/graphics/Point;
    .locals 1

    .line 172
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBaseBot:Landroid/graphics/Point;

    return-object v0
.end method

.method public getmBaseTop()Landroid/graphics/Point;
    .locals 1

    .line 166
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBaseTop:Landroid/graphics/Point;

    return-object v0
.end method

.method public getmBot()Landroid/graphics/Point;
    .locals 1

    .line 214
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    return-object v0
.end method

.method public getmCenter()Landroid/graphics/Point;
    .locals 1

    .line 220
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mCenter:Landroid/graphics/Point;

    return-object v0
.end method

.method public getmFirstTouch()Landroid/graphics/Point;
    .locals 1

    .line 232
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mFirstTouch:Landroid/graphics/Point;

    return-object v0
.end method

.method public getmPaint()Landroid/graphics/Paint;
    .locals 1

    .line 286
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getmTop()Landroid/graphics/Point;
    .locals 1

    .line 208
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    return-object v0
.end method

.method public getmTouch()Landroid/graphics/Point;
    .locals 1

    .line 226
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    return-object v0
.end method

.method public getmWhere()I
    .locals 1

    .line 280
    iget v0, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    return v0
.end method

.method public isSetSize()Z
    .locals 1

    .line 262
    iget-boolean v0, p0, Lepson/scan/activity/ScanBaseView;->isSetSize:Z

    return v0
.end method

.method public isValidArea()Z
    .locals 1

    .line 401
    iget-boolean v0, p0, Lepson/scan/activity/ScanBaseView;->isValidArea:Z

    return v0
.end method

.method public setBm(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 277
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setBmCenter(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 241
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->bmCenter:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setBmRectF(Landroid/graphics/RectF;)V
    .locals 0

    .line 271
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->bmRectF:Landroid/graphics/RectF;

    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    .line 163
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->context:Landroid/content/Context;

    return-void
.end method

.method public setEscan(Lepson/scan/lib/escanLib;)V
    .locals 0

    .line 187
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    return-void
.end method

.method public setInSampleSize(I)V
    .locals 0

    .line 259
    iput p1, p0, Lepson/scan/activity/ScanBaseView;->inSampleSize:I

    return-void
.end method

.method public setListSavedJPGFilePath(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 292
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->listSavedJPGFilePath:Ljava/util/ArrayList;

    return-void
.end method

.method public setMaxScanHeightOnScreen(D)V
    .locals 0

    .line 301
    iput-wide p1, p0, Lepson/scan/activity/ScanBaseView;->maxScanHeightOnScreen:D

    return-void
.end method

.method public setMaxScanWidthOnScreen(D)V
    .locals 0

    .line 308
    iput-wide p1, p0, Lepson/scan/activity/ScanBaseView;->maxScanWidthOnScreen:D

    return-void
.end method

.method public setMaxheight(I)V
    .locals 0

    .line 205
    iput p1, p0, Lepson/scan/activity/ScanBaseView;->maxheight:I

    return-void
.end method

.method public setMaxwidth(I)V
    .locals 0

    .line 199
    iput p1, p0, Lepson/scan/activity/ScanBaseView;->maxwidth:I

    return-void
.end method

.method public setPiBaseBot(Lepson/scan/activity/ScanBaseView$PointInfo;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->piBaseBot:Lepson/scan/activity/ScanBaseView$PointInfo;

    return-void
.end method

.method public setPiBaseTop(Lepson/scan/activity/ScanBaseView$PointInfo;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->piBaseTop:Lepson/scan/activity/ScanBaseView$PointInfo;

    return-void
.end method

.method public setRectScanArea(Landroid/graphics/RectF;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->rectScanArea:Landroid/graphics/RectF;

    return-void
.end method

.method public setScale(D)V
    .locals 0

    .line 193
    iput-wide p1, p0, Lepson/scan/activity/ScanBaseView;->scale:D

    return-void
.end method

.method public setScreenStatus(I)V
    .locals 0

    .line 247
    iput p1, p0, Lepson/scan/activity/ScanBaseView;->screenStatus:I

    return-void
.end method

.method public setSetSize(Z)V
    .locals 0

    .line 265
    iput-boolean p1, p0, Lepson/scan/activity/ScanBaseView;->isSetSize:Z

    return-void
.end method

.method public setValidArea(Z)V
    .locals 0

    .line 404
    iput-boolean p1, p0, Lepson/scan/activity/ScanBaseView;->isValidArea:Z

    return-void
.end method

.method public set_bmOption(Landroid/graphics/BitmapFactory$Options;)V
    .locals 0

    .line 253
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->_bmOption:Landroid/graphics/BitmapFactory$Options;

    return-void
.end method

.method public setmBaseBot(Landroid/graphics/Point;)V
    .locals 0

    .line 175
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mBaseBot:Landroid/graphics/Point;

    return-void
.end method

.method public setmBaseTop(Landroid/graphics/Point;)V
    .locals 0

    .line 169
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mBaseTop:Landroid/graphics/Point;

    return-void
.end method

.method public setmBot(Landroid/graphics/Point;)V
    .locals 0

    .line 217
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mBot:Landroid/graphics/Point;

    return-void
.end method

.method public setmCenter(Landroid/graphics/Point;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mCenter:Landroid/graphics/Point;

    return-void
.end method

.method public setmFirstTouch(Landroid/graphics/Point;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mFirstTouch:Landroid/graphics/Point;

    return-void
.end method

.method public setmPaint(Landroid/graphics/Paint;)V
    .locals 0

    .line 289
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public setmTop(Landroid/graphics/Point;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mTop:Landroid/graphics/Point;

    return-void
.end method

.method public setmTouch(Landroid/graphics/Point;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lepson/scan/activity/ScanBaseView;->mTouch:Landroid/graphics/Point;

    return-void
.end method

.method public setmWhere(I)V
    .locals 0

    .line 283
    iput p1, p0, Lepson/scan/activity/ScanBaseView;->mWhere:I

    return-void
.end method

.method public setupScanArea()V
    .locals 9

    .line 339
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->piBaseTop:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v1}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v1

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView;->piBaseBot:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v3}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$002(Lepson/scan/activity/ScanBaseView$PointInfo;D)D

    .line 340
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->piBaseTop:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v1}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v1

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView;->piBaseBot:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v3}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$102(Lepson/scan/activity/ScanBaseView$PointInfo;D)D

    .line 341
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->piBaseTop:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v1}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v1

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView;->piBaseBot:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v3}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$002(Lepson/scan/activity/ScanBaseView$PointInfo;D)D

    .line 342
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->piBaseTop:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v1}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v1

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView;->piBaseBot:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v3}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$102(Lepson/scan/activity/ScanBaseView$PointInfo;D)D

    .line 345
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    cmpg-double v4, v0, v2

    if-gez v4, :cond_0

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-double v1, v1

    invoke-static {v0, v1, v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$002(Lepson/scan/activity/ScanBaseView$PointInfo;D)D

    .line 346
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    cmpg-double v4, v0, v2

    if-gez v4, :cond_1

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-double v1, v1

    invoke-static {v0, v1, v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$102(Lepson/scan/activity/ScanBaseView$PointInfo;D)D

    .line 348
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    cmpl-double v4, v0, v2

    if-lez v4, :cond_2

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-double v1, v1

    invoke-static {v0, v1, v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$002(Lepson/scan/activity/ScanBaseView$PointInfo;D)D

    .line 349
    :cond_2
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    cmpl-double v4, v0, v2

    if-lez v4, :cond_3

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-double v1, v1

    invoke-static {v0, v1, v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$102(Lepson/scan/activity/ScanBaseView$PointInfo;D)D

    .line 351
    :cond_3
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    const/4 v4, 0x1

    cmpg-double v5, v0, v2

    if-lez v5, :cond_5

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    cmpl-double v5, v0, v2

    if-gez v5, :cond_5

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    .line 352
    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    cmpg-double v5, v0, v2

    if-lez v5, :cond_5

    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    cmpl-double v5, v0, v2

    if-ltz v5, :cond_4

    goto :goto_0

    .line 356
    :cond_4
    iput-boolean v4, p0, Lepson/scan/activity/ScanBaseView;->isValidArea:Z

    goto :goto_1

    :cond_5
    :goto_0
    const/4 v0, 0x0

    .line 354
    iput-boolean v0, p0, Lepson/scan/activity/ScanBaseView;->isValidArea:Z

    .line 359
    :goto_1
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v1}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v1

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-double v5, v3

    sub-double/2addr v1, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxwidth()I

    move-result v3

    int-to-double v5, v3

    mul-double v1, v1, v5

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxScanWidthOnScreen()D

    move-result-wide v5

    div-double/2addr v1, v5

    const-wide/16 v5, 0x0

    invoke-static {v1, v2, v5, v6}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Lepson/scan/lib/escanLib;->setOffsetX(I)V

    .line 360
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v1}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v1

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-double v7, v3

    sub-double/2addr v1, v7

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxheight()I

    move-result v3

    int-to-double v7, v3

    mul-double v1, v1, v7

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxScanHeightOnScreen()D

    move-result-wide v7

    div-double/2addr v1, v7

    invoke-static {v1, v2, v5, v6}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Lepson/scan/lib/escanLib;->setOffsetY(I)V

    .line 365
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v0}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v0

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxwidth()I

    move-result v2

    int-to-double v2, v2

    mul-double v0, v0, v2

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxScanWidthOnScreen()D

    move-result-wide v2

    div-double/2addr v0, v2

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxwidth()I

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v3}, Lepson/scan/lib/escanLib;->getOffsetX()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 366
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v1}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v1

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v3}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v5

    sub-double/2addr v1, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxheight()I

    move-result v3

    int-to-double v5, v3

    mul-double v1, v1, v5

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxScanHeightOnScreen()D

    move-result-wide v5

    div-double/2addr v1, v5

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getMaxheight()I

    move-result v3

    iget-object v5, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v5}, Lepson/scan/lib/escanLib;->getOffsetY()I

    move-result v5

    sub-int/2addr v3, v5

    int-to-double v5, v3

    invoke-static {v1, v2, v5, v6}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    double-to-int v1, v1

    if-nez v0, :cond_6

    .line 370
    iget-object v0, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getOffsetX()I

    move-result v2

    sub-int/2addr v2, v4

    invoke-virtual {v0, v2}, Lepson/scan/lib/escanLib;->setOffsetX(I)V

    const/4 v0, 0x1

    :cond_6
    if-nez v1, :cond_7

    .line 374
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->getOffsetY()I

    move-result v2

    sub-int/2addr v2, v4

    invoke-virtual {v1, v2}, Lepson/scan/lib/escanLib;->setOffsetY(I)V

    const/4 v1, 0x1

    .line 377
    :cond_7
    iget-object v2, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v2, v0}, Lepson/scan/lib/escanLib;->setScanAreaWidth(I)V

    .line 378
    iget-object v2, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v2, v1}, Lepson/scan/lib/escanLib;->setScanAreaHeight(I)V

    .line 380
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->context:Landroid/content/Context;

    const-string v2, "epson.scanner.SelectedScanner"

    const-string v3, "SCAN_REFS_SETTINGS_2SIDED"

    invoke-static {v1, v2, v3}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eq v1, v4, :cond_8

    iget-boolean v1, p0, Lepson/scan/activity/ScanBaseView;->isDoctable:Z

    if-nez v1, :cond_9

    .line 384
    :cond_8
    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "epson.scanner.supported.Options"

    const-string v4, "SCAN_REFS_MAX_WIDTH"

    invoke-static {v2, v3, v4}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lepson/scan/lib/escanLib;->setOffsetX(I)V

    .line 388
    :cond_9
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v1}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v1

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-double v3, v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    double-to-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lepson/scan/activity/ScanBaseView;->first:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v2}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v2

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v3}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$000(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v3

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-double v5, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(DD)D

    move-result-wide v3

    double-to-int v3, v3

    int-to-float v3, v3

    iget-object v4, p0, Lepson/scan/activity/ScanBaseView;->second:Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-static {v4}, Lepson/scan/activity/ScanBaseView$PointInfo;->access$100(Lepson/scan/activity/ScanBaseView$PointInfo;)D

    move-result-wide v4

    invoke-virtual {p0}, Lepson/scan/activity/ScanBaseView;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    double-to-int v4, v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lepson/scan/activity/ScanBaseView;->rectScanArea:Landroid/graphics/RectF;

    return-void
.end method

.class public Lepson/scan/activity/StringListAdapter$Builder;
.super Ljava/lang/Object;
.source "StringListAdapter.java"

# interfaces
.implements Lepson/scan/activity/LocalListAdapterBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/StringListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCurrentPosition:I

.field private mStringArray:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;I)V
    .locals 0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lepson/scan/activity/StringListAdapter$Builder;->mStringArray:[Ljava/lang/String;

    .line 74
    iput p2, p0, Lepson/scan/activity/StringListAdapter$Builder;->mCurrentPosition:I

    return-void
.end method


# virtual methods
.method public build(Landroid/content/Context;)Landroid/widget/ListAdapter;
    .locals 3

    .line 83
    new-instance v0, Lepson/scan/activity/StringListAdapter;

    iget-object v1, p0, Lepson/scan/activity/StringListAdapter$Builder;->mStringArray:[Ljava/lang/String;

    iget v2, p0, Lepson/scan/activity/StringListAdapter$Builder;->mCurrentPosition:I

    invoke-direct {v0, p1, v1, v2}, Lepson/scan/activity/StringListAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;I)V

    return-object v0
.end method

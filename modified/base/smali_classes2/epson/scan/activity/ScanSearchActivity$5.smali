.class Lepson/scan/activity/ScanSearchActivity$5;
.super Ljava/lang/Object;
.source "ScanSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/scan/activity/ScanSearchActivity;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSearchActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 435
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$5;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 438
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$5;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$400(Lepson/scan/activity/ScanSearchActivity;)I

    move-result p1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 443
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$5;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget-object p1, p1, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {p1}, Lepson/scan/activity/ScannerListAdapter;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    const/16 v0, 0x20

    if-lt p1, v0, :cond_1

    .line 444
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$5;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$900(Lepson/scan/activity/ScanSearchActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 445
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$5;->this$0:Lepson/scan/activity/ScanSearchActivity;

    const v1, 0x7f0e0341

    .line 446
    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$5;->this$0:Lepson/scan/activity/ScanSearchActivity;

    const v1, 0x7f0e0340

    .line 447
    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$5;->this$0:Lepson/scan/activity/ScanSearchActivity;

    const v1, 0x7f0e052b

    .line 448
    invoke-virtual {v0, v1}, Lepson/scan/activity/ScanSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/scan/activity/ScanSearchActivity$5$1;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanSearchActivity$5$1;-><init>(Lepson/scan/activity/ScanSearchActivity$5;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 453
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 458
    :cond_1
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/16 v0, 0xa

    .line 459
    iput v0, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x0

    .line 460
    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 461
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$5;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$1000(Lepson/scan/activity/ScanSearchActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void
.end method

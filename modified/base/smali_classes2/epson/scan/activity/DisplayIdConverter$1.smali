.class synthetic Lepson/scan/activity/DisplayIdConverter$1;
.super Ljava/lang/Object;
.source "DisplayIdConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/DisplayIdConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$epson$lib$escani2$EscanI2Lib$ColorMode:[I

.field static final synthetic $SwitchMap$com$epson$lib$escani2$EscanI2Lib$Gamma:[I

.field static final synthetic $SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

.field static final synthetic $SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 252
    invoke-static {}, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->values()[Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$Gamma:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$Gamma:[I

    sget-object v2, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->GAMMA_100:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$Gamma:[I

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->GAMMA_180:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    invoke-virtual {v3}, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    .line 128
    :catch_1
    invoke-static {}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->values()[Lcom/epson/lib/escani2/ScanSize$PaperSize;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    :try_start_2
    sget-object v2, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v3, Lcom/epson/lib/escani2/ScanSize$PaperSize;->A4:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v3}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v2, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v3, Lcom/epson/lib/escani2/ScanSize$PaperSize;->LETTER:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v3}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    const/4 v2, 0x3

    :try_start_4
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->LEGAL:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->B5:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/4 v5, 0x4

    aput v5, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->B6:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/4 v5, 0x5

    aput v5, v3, v4
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->A5:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/4 v5, 0x6

    aput v5, v3, v4
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->A6:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/4 v5, 0x7

    aput v5, v3, v4
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->A8:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/16 v5, 0x8

    aput v5, v3, v4
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->A3:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/16 v5, 0x9

    aput v5, v3, v4
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MEISHI:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/16 v5, 0xa

    aput v5, v3, v4
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->PCARD:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/16 v5, 0xb

    aput v5, v3, v4
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v4

    const/16 v5, 0xc

    aput v5, v3, v4
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    .line 60
    :catch_d
    invoke-static {}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->values()[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

    :try_start_e
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

    sget-object v4, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

    sget-object v4, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    .line 21
    :catch_f
    invoke-static {}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->values()[Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$ColorMode:[I

    :try_start_10
    sget-object v3, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$ColorMode:[I

    sget-object v4, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->COLOR_24BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    :try_start_11
    sget-object v0, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$ColorMode:[I

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->GRAYSCALE_8BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    invoke-virtual {v3}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    :try_start_12
    sget-object v0, Lepson/scan/activity/DisplayIdConverter$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$ColorMode:[I

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    invoke-virtual {v1}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_12

    :catch_12
    return-void
.end method

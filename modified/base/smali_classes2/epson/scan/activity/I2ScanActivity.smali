.class public Lepson/scan/activity/I2ScanActivity;
.super Lepson/print/ActivityIACommon;
.source "I2ScanActivity.java"

# interfaces
.implements Lepson/scan/activity/I2ScanAreaView$FlickCallback;
.implements Lepson/print/CustomTitleDialogFragment$Callback;
.implements Lepson/scan/activity/ScanContinueParam$ScanContinueDialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;,
        Lepson/scan/activity/I2ScanActivity$LocalHandler;,
        Lepson/scan/activity/I2ScanActivity$ProgressCallback;
    }
.end annotation


# static fields
.field private static final DIALOG_ID_CONFIRM_CANCEL:I = 0x1

.field private static final DIALOG_ID_SCAN_ERROR:I = 0x2

.field private static final DIALOG_TAG_CONFIRM_CANCEL:Ljava/lang/String; = "cancel-dialog"

.field private static final DIALOG_TAG_SCAN_END:Ljava/lang/String; = "scan-end"

.field private static final DIALOG_TAG_SCAN_ERROR:Ljava/lang/String; = "scan-error"

.field private static final PARAM_KEY_SCAN_AUTO_START:Ljava/lang/String; = "scan-auto-start"

.field private static final REQUEST_CODE_I2SETTING:I = 0x1

.field private static final REQUEST_CODE_NFC_SCANNER_CHANGE:I = 0x5

.field private static final REQUEST_CODE_PRINT:I = 0x2

.field private static final REQUEST_CODE_START_MAIL_ACTIVITY:I = 0x3

.field private static final REQUEST_CODE_WIFI_DIRECT_SIMPLE_AP_CONNECT:I = 0x4

.field public static final RESULT_ESC_I_VERSION_CHANGE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "I2ScanActivity"


# instance fields
.field private mActivityIsVisible:Z

.field private mCurrentPage:I

.field private mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

.field private mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

.field private mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

.field private final mLocalHandler:Lepson/scan/activity/I2ScanActivity$LocalHandler;

.field private mMailButton:Lepson/scan/activity/CustomButtonWrapper;

.field private mNextPageButton:Landroid/widget/Button;

.field private mPageDisplayText:Landroid/widget/TextView;

.field private mPreviousPageButton:Landroid/widget/Button;

.field private mPrintButton:Lepson/scan/activity/CustomButtonWrapper;

.field private mProgressCallback:Lepson/scan/activity/I2ScanActivity$ProgressCallback;

.field private mProgressViewGroup:Landroid/view/View;

.field private mSaveButton:Lepson/scan/activity/CustomButtonWrapper;

.field private mScanButton:Landroid/widget/Button;

.field private mScanCancelButton:Landroid/widget/Button;

.field private mScanFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mScanFileSizeAndOffset:[I

.field private mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

.field private mScanProgressText:Landroid/widget/TextView;

.field private mStartScanAtScannerChanged:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 56
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 131
    new-instance v0, Lepson/scan/activity/I2ScanActivity$LocalHandler;

    invoke-direct {v0, p0}, Lepson/scan/activity/I2ScanActivity$LocalHandler;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mLocalHandler:Lepson/scan/activity/I2ScanActivity$LocalHandler;

    return-void
.end method

.method static synthetic access$100(Lepson/scan/activity/I2ScanActivity;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->onSaveButtonClicked()V

    return-void
.end method

.method static synthetic access$1000(Lepson/scan/activity/I2ScanActivity;II)V
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2}, Lepson/scan/activity/I2ScanActivity;->updateScanProgressPreview(II)V

    return-void
.end method

.method static synthetic access$1100(Lepson/scan/activity/I2ScanActivity;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->updateScanEnd()V

    return-void
.end method

.method static synthetic access$1200(Lepson/scan/activity/I2ScanActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->finishWithScanParam(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$1300(Lepson/scan/activity/I2ScanActivity;Lepson/scan/i2lib/I2ScanTask$TaskError;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->displayScanError(Lepson/scan/i2lib/I2ScanTask$TaskError;)V

    return-void
.end method

.method static synthetic access$200(Lepson/scan/activity/I2ScanActivity;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->startMailActivity()V

    return-void
.end method

.method static synthetic access$300(Lepson/scan/activity/I2ScanActivity;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->startPrintActivity()V

    return-void
.end method

.method static synthetic access$400(Lepson/scan/activity/I2ScanActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$500(Lepson/scan/activity/I2ScanActivity;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->confirmCancelScan()V

    return-void
.end method

.method static synthetic access$600(Lepson/scan/activity/I2ScanActivity;I)V
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->changePage(I)V

    return-void
.end method

.method static synthetic access$700(Lepson/scan/activity/I2ScanActivity;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->startScan()V

    return-void
.end method

.method static synthetic access$800(Lepson/scan/activity/I2ScanActivity;)Lepson/scan/activity/I2ScanActivity$LocalHandler;
    .locals 0

    .line 56
    iget-object p0, p0, Lepson/scan/activity/I2ScanActivity;->mLocalHandler:Lepson/scan/activity/I2ScanActivity$LocalHandler;

    return-object p0
.end method

.method static synthetic access$900(Lepson/scan/activity/I2ScanActivity;Lepson/scan/i2lib/I2ScanTask$TaskError;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->localScanEnd(Lepson/scan/i2lib/I2ScanTask$TaskError;)V

    return-void
.end method

.method private cancelScan()V
    .locals 2

    .line 449
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 451
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    if-eqz v0, :cond_0

    .line 452
    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanTask;->cancelScan()V

    .line 453
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private changePage(I)V
    .locals 1

    .line 932
    iget v0, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    add-int/2addr v0, p1

    iput v0, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    .line 933
    iget p1, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    if-gez p1, :cond_0

    const/4 p1, 0x0

    .line 934
    iput p1, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    goto :goto_0

    .line 935
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 936
    iget-object p1, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    .line 939
    :cond_1
    :goto_0
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->updateScanPreviewPage()V

    return-void
.end method

.method private checkNfcTagAndStartProcess(Landroid/content/Intent;)V
    .locals 2

    .line 469
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTag(Landroid/content/Context;Landroid/content/Intent;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 477
    :cond_0
    invoke-static {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->load(Landroid/content/Context;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object v0

    .line 478
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getScannerId()Ljava/lang/String;

    .line 483
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 484
    const-class v1, Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "connectInfo"

    .line 485
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "changeMode"

    const/4 v1, 0x2

    .line 486
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p1, 0x5

    .line 487
    invoke-virtual {p0, v0, p1}, Lepson/scan/activity/I2ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 p1, 0x1

    .line 489
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanActivity;->mStartScanAtScannerChanged:Z

    return-void
.end method

.method private confirmCancelScan()V
    .locals 5

    .line 369
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    if-nez v0, :cond_0

    return-void

    .line 372
    :cond_0
    iget-boolean v0, p0, Lepson/scan/activity/I2ScanActivity;->mActivityIsVisible:Z

    if-nez v0, :cond_1

    return-void

    .line 379
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    const v1, 0x7f0e04de

    .line 382
    invoke-virtual {p0, v1}, Lepson/scan/activity/I2ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0e052b

    const v4, 0x7f0e04e6

    .line 381
    invoke-static {v0, v1, v2, v3, v4}, Lepson/print/CustomTitleDialogFragment;->newInstance(ILjava/lang/String;III)Lepson/print/CustomTitleDialogFragment;

    move-result-object v0

    .line 384
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "cancel-dialog"

    invoke-virtual {v0, v1, v2}, Lepson/print/CustomTitleDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private dismissCancelConfirmDialog()V
    .locals 2

    .line 439
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "cancel-dialog"

    .line 441
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lepson/print/CustomTitleDialogFragment;

    if-eqz v0, :cond_0

    .line 443
    invoke-virtual {v0}, Lepson/print/CustomTitleDialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private displayScanError(Lepson/scan/i2lib/I2ScanTask$TaskError;)V
    .locals 4
    .param p1    # Lepson/scan/i2lib/I2ScanTask$TaskError;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 765
    iget-boolean v0, p0, Lepson/scan/activity/I2ScanActivity;->mActivityIsVisible:Z

    if-nez v0, :cond_0

    return-void

    .line 771
    :cond_0
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->getI2ScanErrorTitleAndMessage(Lepson/scan/i2lib/I2ScanTask$TaskError;)[I

    move-result-object p1

    const/4 v0, 0x1

    .line 772
    aget v1, p1, v0

    if-nez v1, :cond_1

    const-string v0, ""

    goto :goto_0

    :cond_1
    aget v0, p1, v0

    .line 773
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 774
    aget p1, p1, v2

    const v3, 0x7f0e04f2

    invoke-static {v1, v0, p1, v3, v2}, Lepson/print/CustomTitleDialogFragment;->newInstance(ILjava/lang/String;III)Lepson/print/CustomTitleDialogFragment;

    move-result-object p1

    .line 778
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "scan-error"

    invoke-virtual {p1, v0, v1}, Lepson/print/CustomTitleDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private displayScanFileList()V
    .locals 4

    .line 901
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 906
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    .line 908
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, v1

    iput v0, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    .line 909
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v1, :cond_1

    .line 910
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPageDisplayText:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 912
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPageDisplayText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 915
    :cond_2
    iput v2, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    const/4 v1, 0x0

    .line 918
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mSaveButton:Lepson/scan/activity/CustomButtonWrapper;

    invoke-virtual {v0, v1}, Lepson/scan/activity/CustomButtonWrapper;->setEnabled(Z)V

    .line 919
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mMailButton:Lepson/scan/activity/CustomButtonWrapper;

    invoke-virtual {v0, v1}, Lepson/scan/activity/CustomButtonWrapper;->setEnabled(Z)V

    .line 920
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPrintButton:Lepson/scan/activity/CustomButtonWrapper;

    invoke-virtual {v0, v1}, Lepson/scan/activity/CustomButtonWrapper;->setEnabled(Z)V

    .line 923
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    if-eqz v1, :cond_3

    const/4 v2, 0x4

    :cond_3
    invoke-virtual {v0, v2}, Lepson/scan/activity/I2ScanAreaView;->setPromptStringVisibility(I)V

    .line 925
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->updateScanPreviewPage()V

    return-void
.end method

.method private finishWithScanParam(Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1024
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "scan-file"

    .line 1025
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 1026
    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/I2ScanActivity;->setResult(ILandroid/content/Intent;)V

    .line 1028
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->finish()V

    return-void
.end method

.method static getAutoStartRequestedFromReturnIntent(Landroid/content/Intent;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "scan-auto-start"

    .line 1058
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    return v0
.end method

.method private getI2ScanErrorTitleAndMessage(Lepson/scan/i2lib/I2ScanTask$TaskError;)[I
    .locals 7
    .param p1    # Lepson/scan/i2lib/I2ScanTask$TaskError;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x2

    .line 785
    new-array v1, v0, [I

    .line 787
    iget v2, p1, Lepson/scan/i2lib/I2ScanTask$TaskError;->error1:I

    const v3, 0x7f0e04ab

    const v4, 0x7f0e04ac

    const/4 v5, 0x1

    const/4 v6, 0x0

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_0

    .line 862
    :pswitch_0
    iget p1, p1, Lepson/scan/i2lib/I2ScanTask$TaskError;->error2:I

    if-eq p1, v0, :cond_0

    aput v4, v1, v6

    aput v3, v1, v5

    goto/16 :goto_0

    :cond_0
    const p1, 0x7f0e053c

    aput p1, v1, v6

    const p1, 0x7f0e04ea

    aput p1, v1, v5

    goto/16 :goto_0

    .line 790
    :pswitch_1
    iget p1, p1, Lepson/scan/i2lib/I2ScanTask$TaskError;->error2:I

    const v0, -0x7a121

    if-eq p1, v0, :cond_5

    const/16 v0, -0x138f

    if-eq p1, v0, :cond_4

    const/16 v0, -0x5dd

    if-eq p1, v0, :cond_3

    const/16 v0, -0x514

    if-eq p1, v0, :cond_2

    const/16 v0, -0x45a

    if-eq p1, v0, :cond_1

    const/16 v0, -0x44c

    if-eq p1, v0, :cond_1

    packed-switch p1, :pswitch_data_1

    packed-switch p1, :pswitch_data_2

    packed-switch p1, :pswitch_data_3

    packed-switch p1, :pswitch_data_4

    aput v4, v1, v6

    aput v3, v1, v5

    goto :goto_0

    :pswitch_2
    const p1, 0x7f0e0471

    aput p1, v1, v6

    const p1, 0x7f0e04a0

    aput p1, v1, v5

    goto :goto_0

    :pswitch_3
    const p1, 0x7f0e04a8

    aput p1, v1, v6

    const p1, 0x7f0e04a7

    aput p1, v1, v5

    goto :goto_0

    :pswitch_4
    const p1, 0x7f0e04b1

    aput p1, v1, v6

    const p1, 0x7f0e04b0

    aput p1, v1, v5

    goto :goto_0

    :pswitch_5
    const p1, 0x7f0e04b3

    aput p1, v1, v6

    const p1, 0x7f0e04b2

    aput p1, v1, v5

    goto :goto_0

    :cond_1
    :pswitch_6
    const p1, 0x7f0e04a6

    aput p1, v1, v6

    const p1, 0x7f0e04a5

    aput p1, v1, v5

    goto :goto_0

    :cond_2
    const p1, 0x7f0e04af

    aput p1, v1, v6

    const p1, 0x7f0e04ae

    aput p1, v1, v5

    goto :goto_0

    :cond_3
    :pswitch_7
    const p1, 0x7f0e04a4

    aput p1, v1, v6

    const p1, 0x7f0e04a3

    aput p1, v1, v5

    goto :goto_0

    :cond_4
    :pswitch_8
    const p1, 0x7f0e04aa

    aput p1, v1, v6

    const p1, 0x7f0e04a9

    aput p1, v1, v5

    goto :goto_0

    :cond_5
    const p1, 0x7f0e01b3

    aput p1, v1, v6

    const p1, 0x7f0e04c8

    aput p1, v1, v5

    :goto_0
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x138b
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0xd7
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_8
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0xca
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_4
    .packed-switch -0x66
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private goSettingActivity()V
    .locals 3

    .line 629
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->saveScanSizeIfChanged()V

    .line 631
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 632
    :goto_0
    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-static {p0, v0, v0, v2}, Lepson/scan/activity/I2ScanSettingActivity;->getStartIntent(Landroid/content/Context;ZZLcom/epson/iprint/shared/SharedParamScan;)Landroid/content/Intent;

    move-result-object v0

    .line 635
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private localScanEnd(Lepson/scan/i2lib/I2ScanTask$TaskError;)V
    .locals 0

    .line 388
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->dismissCancelConfirmDialog()V

    .line 389
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->updateScanEnd()V

    .line 390
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->updateScanFileList(Lepson/scan/i2lib/I2ScanTask$TaskError;)V

    .line 391
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->displayScanFileList()V

    return-void
.end method

.method private onI2SettingEnd(IZ)V
    .locals 1

    .line 670
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->returnToI1ActivityIfEscIVersionMismatch()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    const/4 p1, 0x0

    if-nez p2, :cond_2

    .line 680
    iget-object p2, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 681
    iput p1, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    .line 682
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->resetParameter()V

    .line 685
    invoke-static {p0}, Lepson/scan/activity/ScanActivity;->removeTempContents(Landroid/content/Context;)V

    .line 688
    :cond_2
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->updateUi()V

    .line 690
    iget-boolean p2, p0, Lepson/scan/activity/I2ScanActivity;->mStartScanAtScannerChanged:Z

    if-eqz p2, :cond_3

    .line 691
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanActivity;->mStartScanAtScannerChanged:Z

    .line 692
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->requestStartScan()V

    :cond_3
    return-void
.end method

.method private onSaveButtonClicked()V
    .locals 4

    .line 515
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    if-eqz v0, :cond_1

    .line 516
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 521
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->showProgressWithoutTextViewCancelButton()V

    .line 524
    new-instance v0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;

    iget-object v1, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-direct {v0, p0, v1}, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;-><init>(Lepson/scan/activity/I2ScanActivity;Lcom/epson/iprint/shared/SharedParamScan;)V

    const/4 v1, 0x1

    .line 526
    new-array v1, v1, [Ljava/util/ArrayList;

    const/4 v2, 0x0

    iget-object v3, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 529
    :cond_1
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->startSaveActivity()V

    :goto_0
    return-void
.end method

.method private requestStartScan()V
    .locals 1

    .line 700
    new-instance v0, Lepson/scan/activity/I2ScanActivity$8;

    invoke-direct {v0, p0}, Lepson/scan/activity/I2ScanActivity$8;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private resetUserDefineSize(Z)V
    .locals 1

    .line 245
    invoke-static {p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v0

    if-nez p1, :cond_0

    .line 246
    iget-object p1, v0, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    invoke-virtual {p1}, Lcom/epson/lib/escani2/ScanSize;->isPixelSize()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 248
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/epson/lib/escani2/ScanSize;->getLocaleDefaultSize()Lcom/epson/lib/escani2/ScanSize;

    move-result-object p1

    invoke-static {p0, p1}, Lepson/scan/i2lib/I2ScanParamManager;->updateScanSize(Landroid/content/Context;Lcom/epson/lib/escani2/ScanSize;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    return-void
.end method

.method private returnToI1ActivityIfEscIVersionMismatch()Z
    .locals 4

    .line 313
    invoke-static {p0}, Lepson/scan/lib/ScanInfoStorage;->loadEscIVersion(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    return v1

    .line 318
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "scan-auto-start"

    .line 319
    iget-boolean v3, p0, Lepson/scan/activity/I2ScanActivity;->mStartScanAtScannerChanged:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v2, 0x1

    .line 320
    invoke-virtual {p0, v2, v0}, Lepson/scan/activity/I2ScanActivity;->setResult(ILandroid/content/Intent;)V

    .line 321
    iput-boolean v1, p0, Lepson/scan/activity/I2ScanActivity;->mStartScanAtScannerChanged:Z

    .line 323
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->finish()V

    return v2
.end method

.method private saveScanSizeIfChanged()V
    .locals 5

    .line 611
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    invoke-virtual {v0}, Lepson/scan/activity/I2ScanAreaView;->isScanAreaModified()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 615
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    invoke-virtual {v0}, Lepson/scan/activity/I2ScanAreaView;->getMovableScanArea()Landroid/graphics/Rect;

    move-result-object v0

    .line 616
    new-instance v1, Lcom/epson/lib/escani2/ScanSize;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/epson/lib/escani2/ScanSize;-><init>(IIII)V

    .line 620
    :try_start_0
    invoke-static {p0, v1}, Lepson/scan/i2lib/I2ScanParamManager;->updateScanSize(Landroid/content/Context;Lcom/epson/lib/escani2/ScanSize;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private showProgressWithoutTextViewCancelButton()V
    .locals 2

    .line 403
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 404
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanCancelButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanProgressText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private startMailActivity()V
    .locals 3

    .line 556
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 561
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/MailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "SAVING_FILE_PATH"

    .line 562
    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/4 v1, 0x3

    .line 564
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startPrintActivity()V
    .locals 3

    .line 572
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 577
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/ActivityPrintWeb;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "from"

    const/4 v2, 0x1

    .line 579
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "print_web"

    .line 582
    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "print_log"

    .line 583
    invoke-static {}, Lepson/scan/activity/ScanActivity;->getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, 0x2

    .line 585
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startSaveActivity()V
    .locals 4

    .line 534
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 536
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    goto :goto_0

    .line 542
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lepson/server/screens/StorageServer;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "PICK_SERVER_FOR"

    const/4 v3, 0x2

    .line 544
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "SAVING_FILE_PATH"

    .line 545
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "forLoggerCallFrom"

    const/4 v2, 0x1

    .line 547
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 549
    invoke-virtual {p0, v1}, Lepson/scan/activity/I2ScanActivity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private startScan()V
    .locals 5

    .line 329
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    if-eqz v0, :cond_0

    .line 330
    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    return-void

    .line 335
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->saveScanSizeIfChanged()V

    .line 337
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanCancelButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 338
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanCancelButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 340
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 341
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPageDisplayText:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mNextPageButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPreviousPageButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lepson/scan/activity/I2ScanAreaView;->setPromptStringVisibility(I)V

    const-string v0, "scanner"

    .line 347
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "scanner"

    .line 350
    invoke-static {p0, v0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 358
    :cond_1
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result v0

    if-nez v0, :cond_2

    .line 360
    invoke-static {p0}, Lepson/scan/activity/ScanCount;->countUpScanPage(Landroid/content/Context;)V

    .line 363
    :cond_2
    new-instance v0, Lepson/scan/i2lib/I2ScanTask;

    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity;->mProgressCallback:Lepson/scan/activity/I2ScanActivity$ProgressCallback;

    iget-object v3, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    iget-object v4, p0, Lepson/scan/activity/I2ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {v0, p0, v2, v3, v4}, Lepson/scan/i2lib/I2ScanTask;-><init>(Landroid/content/Context;Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;Lcom/epson/iprint/shared/SharedParamScan;Lcom/epson/iprint/prtlogger/CommonLog;)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    .line 364
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private startScanIfNeeded(Landroid/content/Intent;)V
    .locals 0

    .line 233
    invoke-static {p1}, Lepson/scan/activity/ScanActivityCommonParams;->startScanAtActivityChange(Landroid/content/Intent;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 234
    iput-boolean p1, p0, Lepson/scan/activity/I2ScanActivity;->mStartScanAtScannerChanged:Z

    .line 235
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->requestStartScan()V

    :cond_0
    return-void
.end method

.method private updateScanEnd()V
    .locals 2

    .line 395
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private updateScanFileList(Lepson/scan/i2lib/I2ScanTask$TaskError;)V
    .locals 4

    .line 713
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 716
    :cond_0
    invoke-static {p1}, Lepson/scan/i2lib/I2ScanTask$TaskError;->isInvalidScanArea(Lepson/scan/i2lib/I2ScanTask$TaskError;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 720
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    if-eqz v0, :cond_5

    .line 721
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    iget-object v1, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    invoke-virtual {v1}, Lepson/scan/i2lib/I2ScanTask;->getScanFileList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 725
    :cond_2
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 726
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    iget-object v1, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    invoke-virtual {v1}, Lepson/scan/i2lib/I2ScanTask;->getScanFileList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 728
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 732
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lepson/scan/activity/ScanCount;->saveScanCount(Landroid/content/Context;I)V

    .line 733
    invoke-static {v0}, Lepson/scan/activity/ScanContinueParam;->setScannedFileCount(I)V

    .line 734
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isReachMaximumScan()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 735
    invoke-static {p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne v0, v1, :cond_3

    .line 736
    invoke-static {p0}, Lepson/scan/activity/ScanContinueParam;->showScanErrorLimit(Landroid/content/Context;)V

    .line 740
    :cond_3
    invoke-static {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->load(Landroid/content/Context;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object v0

    .line 741
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    .line 742
    invoke-static {p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v1

    .line 744
    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    if-nez v2, :cond_4

    .line 745
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    invoke-virtual {v2, v0, v1}, Lcom/epson/lib/escani2/ScanSize;->getValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileSizeAndOffset:[I

    goto :goto_1

    :cond_4
    const/4 v0, 0x4

    .line 747
    new-array v0, v0, [I

    invoke-virtual {v2}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_main()I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    const/4 v1, 0x1

    iget-object v3, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v3}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_sub()I

    move-result v3

    aput v3, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v2, v0, v1

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileSizeAndOffset:[I

    goto :goto_1

    .line 753
    :cond_5
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_6
    :goto_1
    if-eqz p1, :cond_7

    .line 756
    invoke-virtual {p1}, Lepson/scan/i2lib/I2ScanTask$TaskError;->isCancel()Z

    move-result v0

    if-nez v0, :cond_7

    .line 757
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->displayScanError(Lepson/scan/i2lib/I2ScanTask$TaskError;)V

    :cond_7
    return-void
.end method

.method private updateScanPreviewPage()V
    .locals 7

    .line 947
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    const/4 v3, 0x1

    if-le v0, v3, :cond_0

    .line 949
    iget-object v4, p0, Lepson/scan/activity/I2ScanActivity;->mPageDisplayText:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 950
    iget-object v4, p0, Lepson/scan/activity/I2ScanActivity;->mPageDisplayText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    add-int/2addr v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 952
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPageDisplayText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 958
    :goto_0
    iget v0, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    if-lez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    .line 961
    :goto_1
    iget v4, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    iget-object v5, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v5, v3

    if-ge v4, v5, :cond_2

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    .line 964
    :goto_2
    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity;->mPreviousPageButton:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 965
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mNextPageButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 968
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 969
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Lepson/scan/activity/I2ScanAreaView;->setPreviewImage(Ljava/lang/String;[I)V

    goto :goto_3

    .line 971
    :cond_3
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    iget-object v1, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    iget v2, p0, Lepson/scan/activity/I2ScanActivity;->mCurrentPage:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileSizeAndOffset:[I

    invoke-virtual {v0, v1, v2}, Lepson/scan/activity/I2ScanAreaView;->setPreviewImage(Ljava/lang/String;[I)V

    .line 973
    :goto_3
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    invoke-virtual {v0}, Lepson/scan/activity/I2ScanAreaView;->invalidate()V

    return-void
.end method

.method private updateScanProgressPreview(II)V
    .locals 1

    .line 982
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    invoke-virtual {v0, p1, p2}, Lepson/scan/i2lib/I2ScanTask;->getScanFile(II)Ljava/io/File;

    move-result-object p1

    if-nez p1, :cond_0

    .line 984
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 985
    :cond_0
    iget-object p2, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lepson/scan/activity/I2ScanAreaView;->setPreviewImage(Ljava/lang/String;)V

    .line 986
    iget-object p1, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    invoke-virtual {p1}, Lepson/scan/activity/I2ScanAreaView;->invalidate()V

    :cond_1
    return-void
.end method

.method private updateUi()V
    .locals 11

    .line 259
    invoke-static {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->load(Landroid/content/Context;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object v0

    .line 261
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 262
    iget-object v2, v0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    if-nez v2, :cond_0

    goto/16 :goto_2

    .line 274
    :cond_0
    iget-object v0, v0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    .line 276
    invoke-static {p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v2

    .line 280
    iget-object v3, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eqz v3, :cond_1

    const/4 v7, 0x4

    .line 281
    new-array v7, v7, [I

    invoke-virtual {v3}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_main()I

    move-result v3

    aput v3, v7, v1

    iget-object v3, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v3}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_sub()I

    move-result v3

    aput v3, v7, v6

    aput v1, v7, v5

    aput v1, v7, v4

    .line 283
    iget-object v3, v2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    iget-object v8, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    .line 284
    invoke-virtual {v8}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_main()I

    move-result v8

    iget-object v9, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v9}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_sub()I

    move-result v9

    .line 283
    invoke-virtual {v0, v3, v8, v9}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorPixelSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object v0

    goto :goto_0

    .line 286
    :cond_1
    iget-object v3, v2, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    invoke-virtual {v3, v0, v2}, Lcom/epson/lib/escani2/ScanSize;->getSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object v7

    .line 287
    iget-object v3, v2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    iget v8, v2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    iget v9, v2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    invoke-virtual {v0, v3, v8, v9}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorPixelSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object v0

    .line 291
    :goto_0
    iget-object v2, v2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne v2, v3, :cond_2

    .line 292
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    aget v1, v7, v1

    aget v2, v7, v6

    invoke-virtual {v0, v1, v2}, Lepson/scan/activity/I2ScanAreaView;->setArea(II)V

    goto :goto_1

    .line 295
    :cond_2
    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    aget v3, v0, v1

    aget v0, v0, v6

    new-instance v8, Landroid/graphics/Rect;

    aget v9, v7, v5

    aget v10, v7, v4

    aget v1, v7, v1

    aget v5, v7, v5

    add-int/2addr v1, v5

    aget v5, v7, v6

    aget v4, v7, v4

    add-int/2addr v5, v4

    invoke-direct {v8, v9, v10, v1, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v2, v3, v0, v8}, Lepson/scan/activity/I2ScanAreaView;->setMovableAreaModeSize(IILandroid/graphics/Rect;)V

    .line 299
    :goto_1
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->displayScanFileList()V

    .line 301
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    if-eqz v0, :cond_3

    .line 302
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mMailButton:Lepson/scan/activity/CustomButtonWrapper;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lepson/scan/activity/CustomButtonWrapper;->setVisibility(I)V

    .line 303
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPrintButton:Lepson/scan/activity/CustomButtonWrapper;

    invoke-virtual {v0, v1}, Lepson/scan/activity/CustomButtonWrapper;->setVisibility(I)V

    :cond_3
    return-void

    .line 266
    :cond_4
    :goto_2
    invoke-static {p0, v1}, Lepson/scan/lib/ScanInfoStorage;->saveEscIVersion(Landroid/content/Context;I)V

    .line 267
    invoke-static {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->clearSavedData(Landroid/content/Context;)V

    .line 270
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->returnToI1ActivityIfEscIVersionMismatch()Z

    return-void
.end method


# virtual methods
.method public flick(I)V
    .locals 0

    .line 992
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->changePage(I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-eq p1, v2, :cond_1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 648
    :pswitch_0
    invoke-direct {p0, p2, v0}, Lepson/scan/activity/I2ScanActivity;->onI2SettingEnd(IZ)V

    goto :goto_0

    :pswitch_1
    if-ne p2, v1, :cond_0

    .line 655
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->requestStartScan()V

    goto :goto_0

    .line 659
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->updateScanEnd()V

    goto :goto_0

    :cond_1
    if-ne p2, v1, :cond_2

    .line 643
    invoke-static {p3}, Lepson/scan/activity/I2ScanSettingActivity;->getOldDataValidation(Landroid/content/Intent;)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 642
    :cond_3
    invoke-direct {p0, p2, v0}, Lepson/scan/activity/I2ScanActivity;->onI2SettingEnd(IZ)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .line 504
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 508
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    invoke-virtual {v0}, Lepson/scan/activity/I2ScanAreaView;->isScanAreaModified()Z

    move-result v0

    invoke-direct {p0, v0}, Lepson/scan/activity/I2ScanActivity;->resetUserDefineSize(Z)V

    .line 509
    invoke-static {p0}, Lepson/scan/activity/ScanActivity;->removeTempContents(Landroid/content/Context;)V

    .line 511
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 135
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a002c

    .line 136
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->setContentView(I)V

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    .line 139
    new-instance v0, Lepson/scan/activity/I2ScanActivity$ProgressCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lepson/scan/activity/I2ScanActivity$ProgressCallback;-><init>(Lepson/scan/activity/I2ScanActivity;Lepson/scan/activity/I2ScanActivity$1;)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressCallback:Lepson/scan/activity/I2ScanActivity$ProgressCallback;

    const v0, 0x7f0e04fe

    const/4 v1, 0x1

    .line 142
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2ScanActivity;->setActionBar(IZ)V

    const v0, 0x7f080066

    .line 144
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lepson/scan/activity/I2ScanAreaView;

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    .line 145
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanAreaView:Lepson/scan/activity/I2ScanAreaView;

    invoke-virtual {v0, p0}, Lepson/scan/activity/I2ScanAreaView;->setFlickCallback(Lepson/scan/activity/I2ScanAreaView$FlickCallback;)V

    const v0, 0x7f0801ef

    .line 147
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    const v0, 0x7f0802d9

    .line 148
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanProgressText:Landroid/widget/TextView;

    const v0, 0x7f080353

    .line 150
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPageDisplayText:Landroid/widget/TextView;

    .line 152
    new-instance v0, Lepson/scan/activity/CustomButtonWrapper;

    const v1, 0x7f080091

    invoke-virtual {p0, v1}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f07013d

    invoke-direct {v0, v1, v2}, Lepson/scan/activity/CustomButtonWrapper;-><init>(Landroid/widget/Button;I)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mSaveButton:Lepson/scan/activity/CustomButtonWrapper;

    .line 153
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mSaveButton:Lepson/scan/activity/CustomButtonWrapper;

    new-instance v1, Lepson/scan/activity/I2ScanActivity$1;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2ScanActivity$1;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    invoke-virtual {v0, v1}, Lepson/scan/activity/CustomButtonWrapper;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    new-instance v0, Lepson/scan/activity/CustomButtonWrapper;

    const v1, 0x7f08008d

    invoke-virtual {p0, v1}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f070139

    invoke-direct {v0, v1, v2}, Lepson/scan/activity/CustomButtonWrapper;-><init>(Landroid/widget/Button;I)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mMailButton:Lepson/scan/activity/CustomButtonWrapper;

    .line 161
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mMailButton:Lepson/scan/activity/CustomButtonWrapper;

    new-instance v1, Lepson/scan/activity/I2ScanActivity$2;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2ScanActivity$2;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    invoke-virtual {v0, v1}, Lepson/scan/activity/CustomButtonWrapper;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    new-instance v0, Lepson/scan/activity/CustomButtonWrapper;

    const v1, 0x7f080090

    invoke-virtual {p0, v1}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f07013a

    invoke-direct {v0, v1, v2}, Lepson/scan/activity/CustomButtonWrapper;-><init>(Landroid/widget/Button;I)V

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPrintButton:Lepson/scan/activity/CustomButtonWrapper;

    .line 169
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPrintButton:Lepson/scan/activity/CustomButtonWrapper;

    new-instance v1, Lepson/scan/activity/I2ScanActivity$3;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2ScanActivity$3;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    invoke-virtual {v0, v1}, Lepson/scan/activity/CustomButtonWrapper;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080092

    .line 176
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanButton:Landroid/widget/Button;

    .line 177
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanButton:Landroid/widget/Button;

    new-instance v1, Lepson/scan/activity/I2ScanActivity$4;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2ScanActivity$4;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08008c

    .line 184
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanCancelButton:Landroid/widget/Button;

    .line 185
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mScanCancelButton:Landroid/widget/Button;

    new-instance v1, Lepson/scan/activity/I2ScanActivity$5;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2ScanActivity$5;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08008e

    .line 192
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mNextPageButton:Landroid/widget/Button;

    .line 193
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mNextPageButton:Landroid/widget/Button;

    new-instance v1, Lepson/scan/activity/I2ScanActivity$6;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2ScanActivity$6;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08008f

    .line 200
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPreviousPageButton:Landroid/widget/Button;

    .line 201
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mPreviousPageButton:Landroid/widget/Button;

    new-instance v1, Lepson/scan/activity/I2ScanActivity$7;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2ScanActivity$7;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lepson/scan/activity/ScanActivityCommonParams;->getExternalIntentData(Landroid/content/Intent;)Lcom/epson/iprint/shared/SharedParamScan;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    .line 210
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->returnToI1ActivityIfEscIVersionMismatch()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-nez p1, :cond_1

    const/4 p1, 0x0

    .line 219
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->resetUserDefineSize(Z)V

    .line 222
    :cond_1
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->updateUi()V

    .line 224
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-static {p1}, Lepson/scan/activity/ScanActivityCommonParams;->getCommonLog(Landroid/content/Intent;)Lcom/epson/iprint/prtlogger/CommonLog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/I2ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    .line 226
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->startScanIfNeeded(Landroid/content/Intent;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 591
    invoke-virtual {p0}, Lepson/scan/activity/I2ScanActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b000a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onLocalNegativeCallback(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    .line 1000
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/I2ScanActivity;->mScanCancelButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1001
    iget-object p1, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onLocalPositiveCallback(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    .line 1014
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->cancelScan()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .line 459
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onNewIntent(Landroid/content/Intent;)V

    .line 461
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "android.nfc.action.NDEF_DISCOVERED"

    .line 462
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity;->checkNfcTagAndStartProcess(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 598
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f08009f

    if-ne v0, v1, :cond_0

    .line 602
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->goSettingActivity()V

    const/4 p1, 0x1

    return p1

    .line 607
    :cond_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 1

    .line 419
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    const/4 v0, 0x0

    .line 420
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanActivity;->mActivityIsVisible:Z

    .line 421
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    return-void
.end method

.method public onRequestScanAction()V
    .locals 0

    .line 1033
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->startScan()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 410
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x0

    .line 412
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 414
    iput-boolean v0, p0, Lepson/scan/activity/I2ScanActivity;->mActivityIsVisible:Z

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .line 498
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 426
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    .line 428
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity;->mProgressViewGroup:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 429
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->cancelScan()V

    .line 432
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->dismissCancelConfirmDialog()V

    return-void
.end method

.method setScanFileList(Ljava/util/ArrayList;[I)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;[I)V"
        }
    .end annotation

    .line 885
    iput-object p1, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    .line 886
    iget-object p1, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    if-nez p1, :cond_0

    .line 887
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileList:Ljava/util/ArrayList;

    .line 889
    :cond_0
    iput-object p2, p0, Lepson/scan/activity/I2ScanActivity;->mScanFileSizeAndOffset:[I

    .line 891
    invoke-direct {p0}, Lepson/scan/activity/I2ScanActivity;->displayScanFileList()V

    return-void
.end method

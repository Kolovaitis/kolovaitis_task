.class public Lepson/scan/activity/InputUnitSettingActivity;
.super Lepson/print/ActivityIACommon;
.source "InputUnitSettingActivity.java"


# static fields
.field public static final IGNORE_ADF_ALIGNMENT:I = -0x1

.field private static final SCAN_SETTINGS_ADF_ALIGNMENT:Ljava/lang/String; = "adf-alignment"

.field private static final SCAN_SETTINGS_LAST_VALUE:Ljava/lang/String; = "SCAN_SETTINGS_LAST_VALUE"


# instance fields
.field private displayAdfAlignment:Z

.field private lastAdfGuide:I

.field private lastValue:I

.field private listSelect:Landroid/widget/ListView;

.field private listSelect2:Landroid/widget/ListView;

.field private final mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

.field private final mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

.field private mlAdapter:Lepson/common/CustomListRowAdapter;

.field private ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 116
    new-instance v0, Lepson/scan/activity/InputUnitSettingActivity$1;

    invoke-direct {v0, p0}, Lepson/scan/activity/InputUnitSettingActivity$1;-><init>(Lepson/scan/activity/InputUnitSettingActivity;)V

    iput-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

    .line 134
    new-instance v0, Lepson/scan/activity/InputUnitSettingActivity$2;

    invoke-direct {v0, p0}, Lepson/scan/activity/InputUnitSettingActivity$2;-><init>(Lepson/scan/activity/InputUnitSettingActivity;)V

    iput-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/InputUnitSettingActivity;)I
    .locals 0

    .line 26
    iget p0, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastValue:I

    return p0
.end method

.method static synthetic access$002(Lepson/scan/activity/InputUnitSettingActivity;I)I
    .locals 0

    .line 26
    iput p1, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastValue:I

    return p1
.end method

.method static synthetic access$100(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;
    .locals 0

    .line 26
    iget-object p0, p0, Lepson/scan/activity/InputUnitSettingActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    return-object p0
.end method

.method static synthetic access$200(Lepson/scan/activity/InputUnitSettingActivity;)Landroid/widget/ListView;
    .locals 0

    .line 26
    iget-object p0, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect2:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$302(Lepson/scan/activity/InputUnitSettingActivity;I)I
    .locals 0

    .line 26
    iput p1, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastAdfGuide:I

    return p1
.end method

.method static synthetic access$400(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/common/CustomListRowAdapter;
    .locals 0

    .line 26
    iget-object p0, p0, Lepson/scan/activity/InputUnitSettingActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    return-object p0
.end method

.method public static getAdfAlignment(Landroid/content/Intent;)I
    .locals 2

    const/4 v0, -0x1

    if-nez p0, :cond_0

    return v0

    :cond_0
    const-string v1, "adf-alignment"

    .line 206
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static getInputUnit(Landroid/content/Intent;)I
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    const-string v1, "SCAN_REFS_SETTINGS_SOURCE"

    .line 194
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static getStartIntent(Landroid/content/Context;II)Landroid/content/Intent;
    .locals 2

    .line 181
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/InputUnitSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "SCAN_SETTINGS_LAST_VALUE"

    .line 182
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "adf-alignment"

    .line 183
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private makeListCorrespondenceOptions()V
    .locals 7

    const v0, 0x7f0e0513

    .line 78
    invoke-virtual {p0, v0}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/scan/activity/InputUnitSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 82
    iget v1, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastAdfGuide:I

    const v2, 0x7f070056

    const v3, 0x7f0e0505

    const v4, 0x7f070145

    const v5, 0x7f070057

    const v6, 0x7f0e0510

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 88
    :pswitch_0
    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-virtual {p0, v6}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-virtual {p0, v3}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    :pswitch_1
    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-virtual {p0, v6}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-virtual {p0, v3}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    :goto_0
    new-instance v1, Lepson/common/CustomListRowAdapter;

    const v2, 0x7f0a004b

    invoke-direct {v1, p0, v2, v0}, Lepson/common/CustomListRowAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lepson/scan/activity/InputUnitSettingActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    .line 96
    iget-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect2:Landroid/widget/ListView;

    iget-object v1, p0, Lepson/scan/activity/InputUnitSettingActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 99
    iget-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect2:Landroid/widget/ListView;

    iget-object v1, p0, Lepson/scan/activity/InputUnitSettingActivity;->mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 101
    iget v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastValue:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 102
    iget-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect2:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1

    .line 104
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect2:Landroid/widget/ListView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 107
    :goto_1
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const v3, 0x7f0e0512

    .line 108
    invoke-virtual {p0, v3}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f0e0511

    .line 109
    invoke-virtual {p0, v1}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    new-instance v1, Lepson/scan/activity/ScanSettingsDetailAdapter;

    invoke-virtual {p0}, Lepson/scan/activity/InputUnitSettingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastValue:I

    invoke-direct {v1, v2, v0, v3}, Lepson/scan/activity/ScanSettingsDetailAdapter;-><init>(Landroid/content/Context;Ljava/util/LinkedHashMap;I)V

    iput-object v1, p0, Lepson/scan/activity/InputUnitSettingActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    .line 111
    iget-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect:Landroid/widget/ListView;

    iget-object v1, p0, Lepson/scan/activity/InputUnitSettingActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 113
    iget-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect:Landroid/widget/ListView;

    iget-object v1, p0, Lepson/scan/activity/InputUnitSettingActivity;->mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onBackPressed()V
    .locals 4

    .line 164
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "SCAN_REFS_SETTINGS_SOURCE"

    .line 166
    iget v2, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastValue:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "adf-alignment"

    .line 167
    iget-boolean v2, p0, Lepson/scan/activity/InputUnitSettingActivity;->displayAdfAlignment:Z

    const/4 v3, -0x1

    if-eqz v2, :cond_0

    iget v2, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastAdfGuide:I

    goto :goto_0

    :cond_0
    const/4 v2, -0x1

    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 170
    invoke-virtual {p0, v3, v0}, Lepson/scan/activity/InputUnitSettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 172
    invoke-virtual {p0}, Lepson/scan/activity/InputUnitSettingActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 53
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00b5

    .line 55
    invoke-virtual {p0, p1}, Lepson/scan/activity/InputUnitSettingActivity;->setContentView(I)V

    .line 58
    invoke-virtual {p0}, Lepson/scan/activity/InputUnitSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "SCAN_SETTINGS_LAST_VALUE"

    .line 60
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastValue:I

    const-string v0, "adf-alignment"

    .line 61
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastAdfGuide:I

    .line 62
    iget p1, p0, Lepson/scan/activity/InputUnitSettingActivity;->lastAdfGuide:I

    const/4 v0, 0x1

    if-ltz p1, :cond_0

    .line 63
    iput-boolean v0, p0, Lepson/scan/activity/InputUnitSettingActivity;->displayAdfAlignment:Z

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 65
    iput-boolean p1, p0, Lepson/scan/activity/InputUnitSettingActivity;->displayAdfAlignment:Z

    :goto_0
    const p1, 0x7f080203

    .line 68
    invoke-virtual {p0, p1}, Lepson/scan/activity/InputUnitSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect:Landroid/widget/ListView;

    const p1, 0x7f080204

    .line 69
    invoke-virtual {p0, p1}, Lepson/scan/activity/InputUnitSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity;->listSelect2:Landroid/widget/ListView;

    .line 71
    invoke-direct {p0}, Lepson/scan/activity/InputUnitSettingActivity;->makeListCorrespondenceOptions()V

    .line 73
    invoke-virtual {p0}, Lepson/scan/activity/InputUnitSettingActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/InputUnitSettingActivity;->setActionBar(Ljava/lang/String;Z)V

    return-void
.end method

.class public Lepson/scan/activity/ScanContinueParam;
.super Ljava/lang/Object;
.source "ScanContinueParam.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/ScanContinueParam$ScanContinueDialogButtonClick;
    }
.end annotation


# static fields
.field public static final SCAN_ADF_MAX_PAGES_LIMIT:I = 0x32

.field public static final SCAN_CONTINUE_MAX_PAGES_LIMIT:I = 0x12c

.field private static mIsContinueScanning:Z

.field private static mIsNeedShowErrorLimit:Z

.field private static mScannedFileCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAvailableScanPageCount()I
    .locals 3

    .line 151
    sget v0, Lepson/scan/activity/ScanContinueParam;->mScannedFileCount:I

    rsub-int v0, v0, 0x12c

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    .line 157
    :cond_0
    sget-boolean v1, Lepson/scan/activity/ScanContinueParam;->mIsContinueScanning:Z

    const/16 v2, 0x32

    if-nez v1, :cond_1

    const/16 v0, 0x32

    .line 161
    :cond_1
    invoke-static {v0}, Lepson/scan/activity/ScanContinueParam;->setIsNeedShowErrorLimitFlag(I)V

    if-lt v0, v2, :cond_2

    const/16 v0, 0x32

    :cond_2
    return v0
.end method

.method public static isContinueScanning()Z
    .locals 1

    .line 134
    sget-boolean v0, Lepson/scan/activity/ScanContinueParam;->mIsContinueScanning:Z

    return v0
.end method

.method public static isReachMaximumScan()Z
    .locals 2

    .line 171
    sget v0, Lepson/scan/activity/ScanContinueParam;->mScannedFileCount:I

    const/16 v1, 0x12c

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static judgeScanContinue(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 74
    move-object v0, p0

    check-cast v0, Lepson/scan/activity/ScanContinueParam$ScanContinueDialogButtonClick;

    .line 75
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    .line 77
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const v2, 0x7f0a0058

    const/4 v3, 0x0

    .line 78
    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 79
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object p0

    const v2, 0x7f0802d6

    .line 80
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0802d5

    .line 81
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 83
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isReachMaximumScan()Z

    move-result v3

    if-nez v3, :cond_0

    .line 85
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const v3, 0x7f0e045b

    .line 86
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const v2, 0x7f0e045a

    .line 87
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(I)V

    const p1, 0x7f0e0458

    .line 88
    new-instance v2, Lepson/scan/activity/ScanContinueParam$2;

    invoke-direct {v2, v0}, Lepson/scan/activity/ScanContinueParam$2;-><init>(Lepson/scan/activity/ScanContinueParam$ScanContinueDialogButtonClick;)V

    invoke-virtual {p0, p1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    .line 98
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v2, 0x7f0e0455

    .line 99
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const p1, 0x7f0e0457

    .line 102
    new-instance v2, Lepson/scan/activity/ScanContinueParam$4;

    invoke-direct {v2}, Lepson/scan/activity/ScanContinueParam$4;-><init>()V

    invoke-virtual {p0, p1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v2, 0x7f0e0459

    new-instance v3, Lepson/scan/activity/ScanContinueParam$3;

    invoke-direct {v3, v0}, Lepson/scan/activity/ScanContinueParam$3;-><init>(Lepson/scan/activity/ScanContinueParam$ScanContinueDialogButtonClick;)V

    .line 107
    invoke-virtual {p1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 115
    invoke-virtual {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    .line 116
    invoke-virtual {p0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 117
    sget-object p1, Lepson/scan/activity/-$$Lambda$ScanContinueParam$O5-_W7A8WVuIPQG0HgQ75RE2hUc;->INSTANCE:Lepson/scan/activity/-$$Lambda$ScanContinueParam$O5-_W7A8WVuIPQG0HgQ75RE2hUc;

    invoke-virtual {p0, p1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 122
    invoke-virtual {p0}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 124
    :cond_1
    invoke-static {v1}, Lepson/scan/activity/ScanContinueParam;->setContinueScanningFlag(Z)V

    .line 125
    invoke-interface {v0}, Lepson/scan/activity/ScanContinueParam$ScanContinueDialogButtonClick;->onRequestScanAction()V

    :goto_1
    return-void
.end method

.method static synthetic lambda$judgeScanContinue$0(Landroid/content/DialogInterface;)V
    .locals 2

    .line 118
    check-cast p0, Landroid/app/AlertDialog;

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAllCaps(Z)V

    const/4 v0, -0x2

    .line 119
    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAllCaps(Z)V

    const/4 v0, -0x3

    .line 120
    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/widget/Button;->setAllCaps(Z)V

    return-void
.end method

.method public static resetParameter()V
    .locals 1

    const/4 v0, 0x0

    .line 138
    sput-boolean v0, Lepson/scan/activity/ScanContinueParam;->mIsContinueScanning:Z

    .line 139
    sput v0, Lepson/scan/activity/ScanContinueParam;->mScannedFileCount:I

    return-void
.end method

.method public static setContinueScanningFlag(Z)V
    .locals 0

    .line 130
    sput-boolean p0, Lepson/scan/activity/ScanContinueParam;->mIsContinueScanning:Z

    return-void
.end method

.method public static setIsNeedShowErrorLimitFlag(I)V
    .locals 1

    const/16 v0, 0x32

    if-ge p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 167
    :goto_0
    sput-boolean p0, Lepson/scan/activity/ScanContinueParam;->mIsNeedShowErrorLimit:Z

    return-void
.end method

.method public static setScannedFileCount(I)V
    .locals 0

    .line 143
    sput p0, Lepson/scan/activity/ScanContinueParam;->mScannedFileCount:I

    return-void
.end method

.method public static showScanErrorLimit(Landroid/content/Context;)V
    .locals 2

    .line 52
    sget-boolean v0, Lepson/scan/activity/ScanContinueParam;->mIsNeedShowErrorLimit:Z

    if-eqz v0, :cond_0

    .line 53
    move-object v0, p0

    check-cast v0, Lepson/scan/activity/ScanContinueParam$ScanContinueDialogButtonClick;

    .line 54
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const p0, 0x7f0e0456

    .line 55
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object p0

    const v0, 0x7f0e04f2

    .line 57
    new-instance v1, Lepson/scan/activity/ScanContinueParam$1;

    invoke-direct {v1}, Lepson/scan/activity/ScanContinueParam$1;-><init>()V

    invoke-virtual {p0, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 64
    invoke-virtual {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    const/4 v0, 0x0

    .line 65
    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 66
    invoke-virtual {p0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    return-void
.end method

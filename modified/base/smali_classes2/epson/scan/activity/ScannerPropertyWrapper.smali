.class public Lepson/scan/activity/ScannerPropertyWrapper;
.super Ljava/lang/Object;
.source "ScannerPropertyWrapper.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mEscIVersion:I

.field private mI2LibScannerInfoAndCapability:Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

.field private mScannerInfo:Lepson/scan/lib/ScannerInfo;

.field private mSimpleApSsid:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 49
    iput v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mEscIVersion:I

    const/4 v0, 0x0

    .line 50
    iput-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    .line 51
    iput-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mI2LibScannerInfoAndCapability:Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    return-void
.end method

.method public constructor <init>(Lepson/scan/lib/ScannerInfo;)V
    .locals 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 55
    iput v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mEscIVersion:I

    .line 56
    iput-object p1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    const/4 p1, 0x0

    .line 57
    iput-object p1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mI2LibScannerInfoAndCapability:Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    return-void
.end method

.method public constructor <init>(Lepson/scan/lib/ScannerInfo;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 61
    iput v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mEscIVersion:I

    .line 62
    iput-object p1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    .line 63
    iput-object p2, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mI2LibScannerInfoAndCapability:Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    return-void
.end method

.method public static clearSavedData(Landroid/content/Context;)V
    .locals 1

    .line 172
    new-instance v0, Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-direct {v0}, Lepson/scan/activity/ScannerPropertyWrapper;-><init>()V

    .line 173
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->resetScanner()V

    .line 175
    invoke-direct {v0, p0}, Lepson/scan/activity/ScannerPropertyWrapper;->i2ScannerInfoSaveSettings(Landroid/content/Context;)V

    const/4 v0, -0x1

    .line 177
    invoke-static {p0, v0}, Lepson/scan/lib/ScanInfoStorage;->saveEscIVersion(Landroid/content/Context;I)V

    .line 179
    invoke-static {p0}, Lepson/scan/i2lib/I2ScanParamManager;->deleteAllInfoFile(Landroid/content/Context;)V

    return-void
.end method

.method private i2ScannerInfoSaveSettings(Landroid/content/Context;)V
    .locals 4

    .line 215
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    .line 216
    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    .line 217
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v3

    .line 215
    invoke-static {p1, v0, v1, v2, v3}, Lepson/scan/lib/ScanInfoStorage;->saveScannerConnectivityInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public static load(Landroid/content/Context;)Lepson/scan/activity/ScannerPropertyWrapper;
    .locals 3

    .line 135
    new-instance v0, Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-direct {v0}, Lepson/scan/activity/ScannerPropertyWrapper;-><init>()V

    .line 137
    invoke-static {p0}, Lepson/scan/lib/ScanInfoStorage;->loadEscIVersion(Landroid/content/Context;)I

    move-result v1

    iput v1, v0, Lepson/scan/activity/ScannerPropertyWrapper;->mEscIVersion:I

    .line 141
    invoke-direct {v0, p0}, Lepson/scan/activity/ScannerPropertyWrapper;->loadScannerInfoConnectivitySettings(Landroid/content/Context;)V

    .line 144
    iget v1, v0, Lepson/scan/activity/ScannerPropertyWrapper;->mEscIVersion:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 145
    invoke-direct {v0, p0}, Lepson/scan/activity/ScannerPropertyWrapper;->loadI2Data(Landroid/content/Context;)V

    :cond_0
    return-object v0
.end method

.method private loadI2Data(Landroid/content/Context;)V
    .locals 0

    .line 156
    invoke-static {p1}, Lepson/scan/i2lib/I2ScanParamManager;->loadI2AllInfo(Landroid/content/Context;)Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mI2LibScannerInfoAndCapability:Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    return-void
.end method

.method private loadScannerInfoConnectivitySettings(Landroid/content/Context;)V
    .locals 2

    .line 183
    invoke-static {}, Lepson/scan/lib/ScannerInfo;->getInstance()Lepson/scan/lib/ScannerInfo;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    .line 185
    invoke-static {}, Lepson/scan/lib/ScanInfoStorage;->getInstance()Lepson/scan/lib/ScanInfoStorage;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, p1, v1}, Lepson/scan/lib/ScanInfoStorage;->loadScannerConnectivityInfo(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    const-string v0, "scanner"

    .line 188
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mSimpleApSsid:Ljava/lang/String;

    return-void
.end method

.method private saveSimpleApSsid(Landroid/content/Context;)V
    .locals 3

    .line 118
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mSimpleApSsid:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 119
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mSimpleApSsid:Ljava/lang/String;

    const-string v1, "scanner"

    iget-object v2, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    .line 120
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-static {p1, v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setConnectInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "scanner"

    .line 122
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getEscIVersion()I
    .locals 1

    .line 76
    iget v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mEscIVersion:I

    return v0
.end method

.method public getI1ScannerInfo()Lepson/scan/lib/ScannerInfo;
    .locals 1

    .line 84
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    return-object v0
.end method

.method public getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;
    .locals 1

    .line 80
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mI2LibScannerInfoAndCapability:Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    return-object v0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    .line 199
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()I
    .locals 1

    .line 207
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v0

    return v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .line 203
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScannerId()Ljava/lang/String;
    .locals 1

    .line 211
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSimpleApSsid()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mSimpleApSsid:Ljava/lang/String;

    return-object v0
.end method

.method public isPrinterSet()Z
    .locals 1

    .line 193
    invoke-virtual {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->getModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public resetScanner()V
    .locals 1

    .line 161
    invoke-static {}, Lepson/scan/lib/ScannerInfo;->getInstance()Lepson/scan/lib/ScannerInfo;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    const/4 v0, -0x1

    .line 163
    iput v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mEscIVersion:I

    return-void
.end method

.method public saveData(Landroid/content/Context;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 95
    invoke-virtual {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->isPrinterSet()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 100
    :cond_0
    invoke-virtual {p0}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result v0

    invoke-static {p1, v0}, Lepson/scan/lib/ScanInfoStorage;->saveEscIVersion(Landroid/content/Context;I)V

    .line 102
    iget v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mEscIVersion:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 104
    invoke-direct {p0, p1}, Lepson/scan/activity/ScannerPropertyWrapper;->i2ScannerInfoSaveSettings(Landroid/content/Context;)V

    .line 105
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mI2LibScannerInfoAndCapability:Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    invoke-static {p1, v0}, Lepson/scan/i2lib/I2ScanParamManager;->saveI2AllInfo(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    if-eqz p2, :cond_1

    .line 108
    invoke-direct {p0, p1}, Lepson/scan/activity/ScannerPropertyWrapper;->saveSimpleApSsid(Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method public setSimpleApSsid(Ljava/lang/String;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mSimpleApSsid:Ljava/lang/String;

    return-void
.end method

.method public updateSetSimpleApSsid(Landroid/content/Context;)V
    .locals 2

    .line 226
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    .line 228
    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromScannerId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 230
    iget-object v0, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 232
    iput-object p1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mSimpleApSsid:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 234
    iput-object p1, p0, Lepson/scan/activity/ScannerPropertyWrapper;->mSimpleApSsid:Ljava/lang/String;

    :goto_0
    return-void
.end method

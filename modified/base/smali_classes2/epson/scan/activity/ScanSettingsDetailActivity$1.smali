.class Lepson/scan/activity/ScanSettingsDetailActivity$1;
.super Ljava/lang/Object;
.source "ScanSettingsDetailActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSettingsDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSettingsDetailActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSettingsDetailActivity;)V
    .locals 0

    .line 207
    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 213
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;

    if-eqz p1, :cond_6

    .line 214
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;

    .line 216
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    iget p2, p2, Lepson/scan/activity/ScanSettingsDetailActivity;->screenId:I

    const p3, 0x7f0e0513

    if-ne p2, p3, :cond_1

    .line 222
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    iget p3, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->value:I

    iput p3, p2, Lepson/scan/activity/ScanSettingsDetailActivity;->lastValue:I

    .line 223
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    iget-object p1, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->text:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->access$002(Lepson/scan/activity/ScanSettingsDetailActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 225
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->access$100(Lepson/scan/activity/ScanSettingsDetailActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;

    move-result-object p1

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    iget p2, p2, Lepson/scan/activity/ScanSettingsDetailActivity;->lastValue:I

    invoke-virtual {p1, p2}, Lepson/scan/activity/ScanSettingsDetailAdapter;->setSelected(I)V

    .line 226
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->access$100(Lepson/scan/activity/ScanSettingsDetailActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/activity/ScanSettingsDetailAdapter;->notifyDataSetChanged()V

    .line 228
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    iget p1, p1, Lepson/scan/activity/ScanSettingsDetailActivity;->lastValue:I

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 229
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->access$200(Lepson/scan/activity/ScanSettingsDetailActivity;)Landroid/widget/ListView;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1

    .line 231
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->access$200(Lepson/scan/activity/ScanSettingsDetailActivity;)Landroid/widget/ListView;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1

    .line 236
    :cond_1
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    invoke-virtual {p2}, Lepson/scan/activity/ScanSettingsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object p2

    .line 237
    iget-object p3, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    iget p3, p3, Lepson/scan/activity/ScanSettingsDetailActivity;->screenId:I

    const p4, 0x7f0e046e

    if-eq p3, p4, :cond_5

    const p4, 0x7f0e04ff

    if-eq p3, p4, :cond_3

    const p4, 0x7f0e0509

    if-eq p3, p4, :cond_4

    const p4, 0x7f0e050f

    if-eq p3, p4, :cond_2

    goto :goto_0

    :cond_2
    const-string p3, "SCAN_REFS_SETTINGS_RESOLUTION"

    .line 250
    iget p4, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->value:I

    invoke-virtual {p2, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p3, "SCAN_REFS_SETTINGS_RESOLUTION_NAME"

    .line 251
    iget-object p1, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->text:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_3
    const-string p3, "SCAN_REFS_SCANNER_CHOSEN_SIZE"

    .line 240
    iget p4, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->value:I

    invoke-virtual {p2, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_4
    const-string p3, "SCAN_REFS_SETTINGS_COLOR"

    .line 244
    iget p4, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->value:I

    invoke-virtual {p2, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p3, "SCAN_REFS_SETTINGS_COLOR_NAME"

    .line 245
    iget-object p1, p1, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->text:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    :cond_5
    :goto_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    const/4 p3, -0x1

    invoke-virtual {p1, p3, p2}, Lepson/scan/activity/ScanSettingsDetailActivity;->setResult(ILandroid/content/Intent;)V

    .line 263
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity$1;->this$0:Lepson/scan/activity/ScanSettingsDetailActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->finish()V

    :cond_6
    :goto_1
    return-void
.end method

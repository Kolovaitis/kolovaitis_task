.class public Lepson/scan/activity/ScanSettingsDetailAdapter;
.super Landroid/widget/BaseAdapter;
.source "ScanSettingsDetailAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;
    }
.end annotation


# instance fields
.field private lhmListSettingsSupport:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private selectedValue:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/LinkedHashMap;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 25
    iput-object p2, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->lhmListSettingsSupport:Ljava/util/LinkedHashMap;

    .line 26
    iput p3, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->selectedValue:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 30
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->lhmListSettingsSupport:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 34
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->lhmListSettingsSupport:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    .line 50
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->mInflater:Landroid/view/LayoutInflater;

    const p3, 0x7f0a00b6

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 51
    new-instance p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;

    invoke-direct {p3}, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;-><init>()V

    const v0, 0x7f08035d

    .line 52
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->text:Landroid/widget/TextView;

    const v0, 0x7f0801b4

    .line 53
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->img:Landroid/widget/ImageView;

    .line 54
    iget-object v0, p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->img:Landroid/widget/ImageView;

    const v1, 0x7f070145

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 55
    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;

    .line 61
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->lhmListSettingsSupport:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 62
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 64
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 65
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-ne v2, p1, :cond_2

    .line 67
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->lhmListSettingsSupport:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 69
    iget-object v0, p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->text:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget v0, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->selectedValue:I

    if-ne p1, v0, :cond_1

    .line 71
    iget-object v0, p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->img:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 75
    :cond_1
    iget-object v0, p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->img:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    :goto_2
    iput p1, p3, Lepson/scan/activity/ScanSettingsDetailAdapter$DetailSettingHolder;->value:I

    goto :goto_3

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    :goto_3
    return-object p2
.end method

.method public setSelected(I)V
    .locals 0

    .line 42
    iput p1, p0, Lepson/scan/activity/ScanSettingsDetailAdapter;->selectedValue:I

    return-void
.end method

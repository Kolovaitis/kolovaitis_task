.class public Lepson/scan/activity/CustomButtonWrapper;
.super Ljava/lang/Object;
.source "CustomButtonWrapper.java"


# instance fields
.field private final mButton:Landroid/widget/Button;

.field private final mDrawableId:I


# direct methods
.method public constructor <init>(Landroid/widget/Button;I)V
    .locals 1
    .param p1    # Landroid/widget/Button;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lepson/scan/activity/CustomButtonWrapper;->mButton:Landroid/widget/Button;

    .line 30
    iget-object p1, p0, Lepson/scan/activity/CustomButtonWrapper;->mButton:Landroid/widget/Button;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 34
    iput p2, p0, Lepson/scan/activity/CustomButtonWrapper;->mDrawableId:I

    .line 37
    iget-object p1, p0, Lepson/scan/activity/CustomButtonWrapper;->mButton:Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/Button;->isEnabled()Z

    move-result p1

    invoke-virtual {p0, p1}, Lepson/scan/activity/CustomButtonWrapper;->setEnabled(Z)V

    return-void
.end method

.method public static localGetDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 87
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 88
    invoke-virtual {p0, p1, v0}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0

    .line 91
    :cond_1
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static setScanButtonState(Landroid/widget/Button;IIZ)V
    .locals 1
    .param p0    # Landroid/widget/Button;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .line 60
    invoke-virtual {p0}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-static {p2, p1}, Lepson/scan/activity/CustomButtonWrapper;->localGetDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-eqz p3, :cond_1

    const/4 p2, -0x1

    .line 66
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    :cond_1
    const-string p2, "#cccccc"

    .line 68
    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p2

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :goto_0
    const/4 p2, 0x0

    .line 70
    invoke-virtual {p0, p2, p2, p2, p1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    const-string p1, ""

    .line 72
    invoke-virtual {p0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {p0, p3}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 3

    .line 41
    iget-object v0, p0, Lepson/scan/activity/CustomButtonWrapper;->mButton:Landroid/widget/Button;

    iget v1, p0, Lepson/scan/activity/CustomButtonWrapper;->mDrawableId:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lepson/scan/activity/CustomButtonWrapper;->setScanButtonState(Landroid/widget/Button;IIZ)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lepson/scan/activity/CustomButtonWrapper;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .line 49
    iget-object v0, p0, Lepson/scan/activity/CustomButtonWrapper;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.class public Lepson/scan/activity/ScanSettingsDetailActivity;
.super Lepson/print/ActivityIACommon;
.source "ScanSettingsDetailActivity.java"


# static fields
.field private static final SCAN_SETTINGS_LAST_VALUE:Ljava/lang/String; = "SCAN_SETTINGS_LAST_VALUE"

.field private static final SCAN_SUPPORTED_RESOLUTION_LIST:Ljava/lang/String; = "SCAN_SUPPORTED_RESOLUTION_LIST"

.field private static final SCAN_SUPPORTED_SIZE_LIST:Ljava/lang/String; = "SCAN_SUPPORTED_SIZE_LIST"


# instance fields
.field adfHeight:I

.field adfWidth:I

.field context:Landroid/content/Context;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/common/CustomListRow;",
            ">;"
        }
    .end annotation
.end field

.field private lastAdfGuid:I

.field lastValue:I

.field lhmListOptionValue:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field listResolution:[I

.field private listSelect:Landroid/widget/ListView;

.field private listSelect2:Landroid/widget/ListView;

.field listSize:[I

.field private mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

.field private mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

.field private mlAdapter:Lepson/common/CustomListRowAdapter;

.field screenId:I

.field screenTitle:Ljava/lang/String;

.field private ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

.field private strSelText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 207
    new-instance v0, Lepson/scan/activity/ScanSettingsDetailActivity$1;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsDetailActivity$1;-><init>(Lepson/scan/activity/ScanSettingsDetailActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

    .line 270
    new-instance v0, Lepson/scan/activity/ScanSettingsDetailActivity$2;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsDetailActivity$2;-><init>(Lepson/scan/activity/ScanSettingsDetailActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$002(Lepson/scan/activity/ScanSettingsDetailActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 32
    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->strSelText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lepson/scan/activity/ScanSettingsDetailActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;
    .locals 0

    .line 32
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    return-object p0
.end method

.method static synthetic access$200(Lepson/scan/activity/ScanSettingsDetailActivity;)Landroid/widget/ListView;
    .locals 0

    .line 32
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect2:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$302(Lepson/scan/activity/ScanSettingsDetailActivity;I)I
    .locals 0

    .line 32
    iput p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastAdfGuid:I

    return p1
.end method

.method static synthetic access$400(Lepson/scan/activity/ScanSettingsDetailActivity;)Lepson/common/CustomListRowAdapter;
    .locals 0

    .line 32
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    return-object p0
.end method

.method private makeListCorespondenceOptions(I)V
    .locals 6

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->items:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const/4 v0, 0x0

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_3

    :sswitch_0
    const p1, 0x7f0e0513

    .line 97
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenTitle:Ljava/lang/String;

    .line 98
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v2, 0x7f0e0512

    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->adfWidth:I

    if-lez p1, :cond_0

    iget p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->adfHeight:I

    if-lez p1, :cond_0

    .line 102
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v0, 0x7f0e0511

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    :cond_0
    iget p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastAdfGuid:I

    const v0, 0x7f070056

    const v1, 0x7f0e0505

    const v2, 0x7f070145

    const v3, 0x7f070057

    const v4, 0x7f0e0510

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_3

    .line 112
    :pswitch_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->items:Ljava/util/List;

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v5

    invoke-virtual {p0, v4}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v3}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->items:Ljava/util/List;

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v3

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 108
    :pswitch_1
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->items:Ljava/util/List;

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v5

    invoke-virtual {p0, v4}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v3}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->items:Ljava/util/List;

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v2

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :sswitch_1
    const p1, 0x7f0e050f

    .line 141
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenTitle:Ljava/lang/String;

    const/4 p1, 0x0

    .line 146
    :goto_0
    iget-object v2, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listResolution:[I

    array-length v3, v2

    if-ge p1, v3, :cond_2

    .line 147
    aget v2, v2, p1

    const/16 v3, 0x4b

    if-eq v2, v3, :cond_1

    const/16 v3, 0x96

    if-eq v2, v3, :cond_1

    const/16 v3, 0x12c

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 151
    :cond_1
    iget-object v2, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v3, 0x7f0e050e

    invoke-virtual {p0, v3}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listResolution:[I

    aget v5, v5, p1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listResolution:[I

    aget v4, v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :sswitch_2
    const p1, 0x7f0e0509

    .line 134
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenTitle:Ljava/lang/String;

    .line 135
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v0, 0x7f0e0506

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v0, 0x7f0e0507

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v0, 0x7f0e0508

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :sswitch_3
    const p1, 0x7f0e04ff

    .line 123
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenTitle:Ljava/lang/String;

    .line 126
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSize:[I

    array-length v1, p1

    :goto_2
    if-ge v0, v1, :cond_2

    aget v2, p1, v0

    .line 127
    iget-object v3, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    invoke-static {v2}, Lepson/scan/lib/ScanSizeHelper;->getScanSizeName(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :sswitch_4
    const p1, 0x7f0e046e

    .line 162
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenTitle:Ljava/lang/String;

    :cond_2
    :goto_3
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e046e -> :sswitch_4
        0x7f0e04ff -> :sswitch_3
        0x7f0e0509 -> :sswitch_2
        0x7f0e050f -> :sswitch_1
        0x7f0e0513 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setListCorespondenceOptions(I)V
    .locals 3

    .line 171
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenTitle:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsDetailActivity;->setTitle(Ljava/lang/CharSequence;)V

    sparse-switch p1, :sswitch_data_0

    goto :goto_1

    .line 175
    :sswitch_0
    new-instance p1, Lepson/common/CustomListRowAdapter;

    const v0, 0x7f0a004b

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->items:Ljava/util/List;

    invoke-direct {p1, p0, v0, v1}, Lepson/common/CustomListRowAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    .line 176
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect2:Landroid/widget/ListView;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 178
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect2:Landroid/widget/ListView;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 180
    iget p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastValue:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 181
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect2:Landroid/widget/ListView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 183
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect2:Landroid/widget/ListView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 192
    :goto_0
    :sswitch_1
    new-instance p1, Lepson/scan/activity/ScanSettingsDetailAdapter;

    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsDetailActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    iget v2, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastValue:I

    invoke-direct {p1, v0, v1, v2}, Lepson/scan/activity/ScanSettingsDetailAdapter;-><init>(Landroid/content/Context;Ljava/util/LinkedHashMap;I)V

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    .line 193
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect:Landroid/widget/ListView;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 195
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect:Landroid/widget/ListView;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :goto_1
    :sswitch_2
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f0e046e -> :sswitch_2
        0x7f0e04ff -> :sswitch_1
        0x7f0e0509 -> :sswitch_1
        0x7f0e050f -> :sswitch_1
        0x7f0e0513 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .line 306
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 307
    iget v1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenId:I

    const v2, 0x7f0e0513

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    .line 325
    invoke-virtual {p0, v1, v0}, Lepson/scan/activity/ScanSettingsDetailActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    .line 311
    :cond_0
    iget v1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastValue:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const v1, 0x7f0e0511

    .line 312
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->strSelText:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const v1, 0x7f0e0512

    .line 314
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->strSelText:Ljava/lang/String;

    :goto_0
    const-string v1, "SCAN_REFS_SETTINGS_SOURCE"

    .line 317
    iget v2, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastValue:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "SCAN_REFS_SETTINGS_SOURCE_NAME"

    .line 318
    iget-object v2, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->strSelText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "SCAN_REFS_SETTINGS_ADF_PAPER_GUIDE"

    .line 319
    iget v2, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastAdfGuid:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, -0x1

    .line 321
    invoke-virtual {p0, v1, v0}, Lepson/scan/activity/ScanSettingsDetailActivity;->setResult(ILandroid/content/Intent;)V

    .line 330
    :goto_1
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsDetailActivity;->finish()V

    .line 331
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 63
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00b5

    .line 65
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->setContentView(I)V

    .line 68
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "SCAN_SETTINGS_DETAIL_TITLE"

    .line 70
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenId:I

    const-string v0, "SCAN_SETTINGS_LAST_VALUE"

    .line 71
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastValue:I

    const-string v0, "SCAN_REFS_OPTIONS_ADF_WIDTH"

    .line 72
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->adfWidth:I

    const-string v0, "SCAN_REFS_OPTIONS_ADF_HEIGHT"

    .line 73
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->adfHeight:I

    const-string v0, "SCAN_SUPPORTED_RESOLUTION_LIST"

    .line 74
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listResolution:[I

    const-string v0, "SCAN_SUPPORTED_SIZE_LIST"

    .line 75
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSize:[I

    const-string v0, "SCAN_REFS_SETTINGS_ADF_PAPER_GUIDE"

    .line 77
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->lastAdfGuid:I

    const p1, 0x7f080203

    .line 79
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect:Landroid/widget/ListView;

    const p1, 0x7f080204

    .line 80
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->listSelect2:Landroid/widget/ListView;

    .line 82
    iput-object p0, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->context:Landroid/content/Context;

    .line 84
    iget p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenId:I

    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->makeListCorespondenceOptions(I)V

    .line 85
    iget p1, p0, Lepson/scan/activity/ScanSettingsDetailActivity;->screenId:I

    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSettingsDetailActivity;->setListCorespondenceOptions(I)V

    .line 87
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsDetailActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/ScanSettingsDetailActivity;->setActionBar(Ljava/lang/String;Z)V

    return-void
.end method

.class public Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;
.super Lepson/print/ActivityIACommon;
.source "ScanSettingsAdvanceDetailActivity.java"


# static fields
.field private static final SCAN_SETTINGS_LAST_VALUE:Ljava/lang/String; = "SCAN_SETTINGS_LAST_VALUE"


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/common/CustomListRow;",
            ">;"
        }
    .end annotation
.end field

.field private lastAdfRotate:I

.field private lastValue:I

.field private lhmListOptionValue:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private listSelect:Landroid/widget/ListView;

.field private listSelect2:Landroid/widget/ListView;

.field private mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

.field private mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

.field private mlAdapter:Lepson/common/CustomListRowAdapter;

.field private screenId:I

.field private screenTitle:Ljava/lang/String;

.field private ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

.field private strSelText:Ljava/lang/String;

.field private supportedAdfDuplex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 148
    new-instance v0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$1;-><init>(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

    .line 191
    new-instance v0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity$2;-><init>(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)I
    .locals 0

    .line 35
    iget p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->screenId:I

    return p0
.end method

.method static synthetic access$100(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)I
    .locals 0

    .line 35
    iget p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastValue:I

    return p0
.end method

.method static synthetic access$102(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;I)I
    .locals 0

    .line 35
    iput p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastValue:I

    return p1
.end method

.method static synthetic access$202(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 35
    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->strSelText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/scan/activity/ScanSettingsDetailAdapter;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    return-object p0
.end method

.method static synthetic access$400(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Landroid/widget/ListView;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect2:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$502(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;I)I
    .locals 0

    .line 35
    iput p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastAdfRotate:I

    return p1
.end method

.method static synthetic access$600(Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;)Lepson/common/CustomListRowAdapter;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    return-object p0
.end method

.method private getListCorespondenceOptions(I)V
    .locals 5

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->items:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const/4 v0, 0x0

    const v1, 0x7f0e0504

    const/4 v2, 0x1

    if-eq p1, v1, :cond_1

    const v1, 0x7f0e050c

    if-eq p1, v1, :cond_0

    goto/16 :goto_0

    .line 109
    :cond_0
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->screenTitle:Ljava/lang/String;

    .line 110
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v1, 0x7f0e050a

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v0, 0x7f0e050b

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 85
    :cond_1
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->screenTitle:Ljava/lang/String;

    .line 87
    iget p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->supportedAdfDuplex:I

    if-ne p1, v2, :cond_2

    .line 88
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v1, 0x7f0e052b

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    :cond_2
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    const v1, 0x7f0e04e6

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastAdfRotate:I

    const v0, 0x7f07013c

    const v1, 0x7f070145

    const v2, 0x7f070138

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 99
    :pswitch_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->items:Ljava/util/List;

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->items:Ljava/util/List;

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    :pswitch_1
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->items:Ljava/util/List;

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->items:Ljava/util/List;

    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setListCorespondenceOptions(I)V
    .locals 3

    .line 121
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->screenTitle:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f0e0504

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 125
    :cond_0
    new-instance p1, Lepson/common/CustomListRowAdapter;

    const v0, 0x7f0a004b

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->items:Ljava/util/List;

    invoke-direct {p1, p0, v0, v1}, Lepson/common/CustomListRowAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    .line 126
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect2:Landroid/widget/ListView;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->mlAdapter:Lepson/common/CustomListRowAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 128
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect2:Landroid/widget/ListView;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->mListOptionClick2:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 129
    iget p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastValue:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 130
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect2:Landroid/widget/ListView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 132
    :cond_1
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect2:Landroid/widget/ListView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 139
    :goto_0
    new-instance p1, Lepson/scan/activity/ScanSettingsDetailAdapter;

    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lhmListOptionValue:Ljava/util/LinkedHashMap;

    iget v2, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastValue:I

    invoke-direct {p1, v0, v1, v2}, Lepson/scan/activity/ScanSettingsDetailAdapter;-><init>(Landroid/content/Context;Ljava/util/LinkedHashMap;I)V

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    .line 140
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect:Landroid/widget/ListView;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->ssdAdapter:Lepson/scan/activity/ScanSettingsDetailAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 141
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect:Landroid/widget/ListView;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->mListOptionClick:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .line 229
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 231
    iget v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->screenId:I

    const v2, 0x7f0e0504

    if-eq v1, v2, :cond_1

    const v2, 0x7f0e050c

    if-eq v1, v2, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    .line 250
    invoke-virtual {p0, v1, v0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    .line 234
    :cond_1
    iget v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastValue:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const v1, 0x7f0e052b

    .line 235
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->strSelText:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const v1, 0x7f0e04e6

    .line 237
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->strSelText:Ljava/lang/String;

    :goto_0
    const-string v1, "SCAN_REFS_SETTINGS_2SIDED"

    .line 240
    iget v2, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastValue:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "SCAN_REFS_SETTINGS_2SIDED_NAME"

    .line 241
    iget-object v2, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->strSelText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "SCAN_REFS_SETTINGS_ROTATE"

    .line 242
    iget v2, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastAdfRotate:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, -0x1

    .line 243
    invoke-virtual {p0, v1, v0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->setResult(ILandroid/content/Intent;)V

    .line 258
    :goto_1
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->finish()V

    .line 259
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 56
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00b5

    .line 58
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->setContentView(I)V

    .line 61
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "SCAN_SETTINGS_DETAIL_TITLE"

    .line 62
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->screenId:I

    const-string v0, "SCAN_SETTINGS_LAST_VALUE"

    .line 63
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastValue:I

    const-string v0, "SCAN_REFS_OPTIONS_SUPPORTED_ADF_DUPLEX"

    .line 64
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->supportedAdfDuplex:I

    const-string v0, "SCAN_REFS_SETTINGS_ROTATE"

    .line 66
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->lastAdfRotate:I

    const p1, 0x7f080203

    .line 68
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect:Landroid/widget/ListView;

    const p1, 0x7f080204

    .line 69
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->listSelect2:Landroid/widget/ListView;

    .line 71
    iget p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->screenId:I

    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getListCorespondenceOptions(I)V

    .line 72
    iget p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->screenId:I

    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->setListCorespondenceOptions(I)V

    .line 74
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;->setActionBar(Ljava/lang/String;Z)V

    return-void
.end method

.class Lepson/scan/activity/ScanSearchActivity$11;
.super Ljava/lang/Object;
.source "ScanSearchActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSearchActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSearchActivity;)V
    .locals 0

    .line 612
    iput-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    .line 614
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanSearchActivity;->isFinishing()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$1200(Lepson/scan/activity/ScanSearchActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v1, :cond_9

    const/16 v3, 0x8

    if-eq v0, v3, :cond_9

    packed-switch v0, :pswitch_data_0

    .line 628
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v3, -0x1

    packed-switch v0, :pswitch_data_1

    :pswitch_0
    goto/16 :goto_2

    .line 635
    :pswitch_1
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    .line 636
    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1000(Lepson/scan/activity/ScanSearchActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    const/16 v3, 0x3c

    .line 635
    invoke-static {p1, v0, v1, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->searchWiFiDirectPrinter(Landroid/content/Context;Landroid/os/Handler;II)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/scan/activity/ScanSearchActivity;->access$1402(Lepson/scan/activity/ScanSearchActivity;Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 637
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1400(Lepson/scan/activity/ScanSearchActivity;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    move-result-object p1

    if-nez p1, :cond_8

    .line 638
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1, v2}, Lepson/scan/activity/ScanSearchActivity;->access$1502(Lepson/scan/activity/ScanSearchActivity;Z)Z

    goto/16 :goto_2

    .line 702
    :pswitch_2
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$300(Lepson/scan/activity/ScanSearchActivity;)V

    const/4 v0, 0x0

    .line 705
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 706
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lepson/print/MyPrinter;

    .line 710
    :cond_2
    new-instance p1, Landroid/content/Intent;

    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    const-class v4, Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-direct {p1, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 712
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    if-eqz v0, :cond_3

    .line 713
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    const-string v4, "PRINTER_KEY"

    .line 714
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "PRINTER_KEY"

    const-string v4, ""

    .line 717
    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iput v3, v0, Lepson/scan/activity/ScanSearchActivity;->mDeletePos:I

    .line 722
    invoke-virtual {p1, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 723
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    const/16 v1, 0xa

    invoke-virtual {v0, p1, v1}, Lepson/scan/activity/ScanSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_2

    .line 729
    :pswitch_3
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget p1, p1, Lepson/scan/activity/ScanSearchActivity;->mDeletePos:I

    if-ltz p1, :cond_8

    .line 731
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$400(Lepson/scan/activity/ScanSearchActivity;)I

    move-result p1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_4

    goto :goto_1

    .line 743
    :cond_4
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$900(Lepson/scan/activity/ScanSearchActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 745
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget-object v0, v0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    .line 746
    invoke-virtual {v0}, Lepson/scan/activity/ScannerListAdapter;->getData()Ljava/util/Vector;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget v1, v1, Lepson/scan/activity/ScanSearchActivity;->mDeletePos:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    .line 745
    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->deleteIpPrinterInfo(Ljava/lang/String;)Z

    .line 747
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget-object p1, p1, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {p1}, Lepson/scan/activity/ScannerListAdapter;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object p1

    .line 748
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget-object v0, v0, Lepson/scan/activity/ScanSearchActivity;->mBuilder:Lepson/scan/activity/ScannerListAdapter;

    invoke-virtual {v0}, Lepson/scan/activity/ScannerListAdapter;->getData()Ljava/util/Vector;

    move-result-object v0

    .line 749
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget v1, v1, Lepson/scan/activity/ScanSearchActivity;->mDeletePos:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 750
    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 755
    :goto_1
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iput v3, p1, Lepson/scan/activity/ScanSearchActivity;->mDeletePos:I

    goto/16 :goto_2

    .line 649
    :pswitch_4
    iget-object v0, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanSearchActivity;->access$400(Lepson/scan/activity/ScanSearchActivity;)I

    move-result v0

    if-eq v0, v1, :cond_5

    goto/16 :goto_2

    .line 652
    :cond_5
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "name"

    .line 654
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 657
    new-instance v0, Lepson/print/MyPrinter;

    const-string v1, "name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "ip"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v0, v1, v3, v4, v5}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "id"

    .line 658
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/MyPrinter;->setScannerId(Ljava/lang/String;)V

    const-string v1, "commonDeviceName"

    .line 659
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/print/MyPrinter;->setCommonDeviceName(Ljava/lang/String;)V

    .line 662
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget-object p1, p1, Lepson/scan/activity/ScanSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    .line 663
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getScannerId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromScannerId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 662
    invoke-virtual {p1, v0, v2, v1, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->addPrinter(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 671
    :pswitch_5
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 672
    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ssid"

    .line 675
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "name"

    .line 676
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "addr_infra"

    .line 677
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez v1, :cond_6

    move-object v1, v0

    .line 684
    :cond_6
    new-instance v3, Lepson/print/MyPrinter;

    const-string v4, ""

    invoke-direct {v3, v1, v0, p1, v4}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    iget-object v1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    iget-object v1, v1, Lepson/scan/activity/ScanSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    invoke-virtual {v1, v3, v2, v0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->addPrinter(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 692
    :cond_7
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1, v2}, Lepson/scan/activity/ScanSearchActivity;->access$1502(Lepson/scan/activity/ScanSearchActivity;Z)Z

    .line 693
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1700(Lepson/scan/activity/ScanSearchActivity;)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 694
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1600(Lepson/scan/activity/ScanSearchActivity;)V

    goto :goto_2

    .line 643
    :pswitch_6
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$300(Lepson/scan/activity/ScanSearchActivity;)V

    .line 644
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1600(Lepson/scan/activity/ScanSearchActivity;)V

    goto :goto_2

    .line 631
    :pswitch_7
    iget-object p1, p0, Lepson/scan/activity/ScanSearchActivity$11;->this$0:Lepson/scan/activity/ScanSearchActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSearchActivity;->access$1300(Lepson/scan/activity/ScanSearchActivity;)V

    :cond_8
    :goto_2
    return v2

    :cond_9
    :pswitch_8
    return v2

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

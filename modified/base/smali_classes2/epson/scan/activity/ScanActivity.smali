.class public Lepson/scan/activity/ScanActivity;
.super Lepson/scan/activity/ScanBaseView;
.source "ScanActivity.java"

# interfaces
.implements Lepson/scan/activity/ScanContinueParam$ScanContinueDialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/ScanActivity$BitmapTask;,
        Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;
    }
.end annotation


# static fields
.field private static final MESSAGE_CHECK_STATUS_AND_START_SCAN:I = 0x14

.field public static final PARAM_KEY_SCAN_LOG:Ljava/lang/String; = "scan-log"

.field private static final REQEST_RUNTIMEPERMMISSION:I = 0x4

.field private static final REQUEST_CODE_ESC_I_VERSION_CHANGE:I = 0x14

.field static final RETURN_KEY_SCAN_FILE:Ljava/lang/String; = "scan-file"

.field private static final SCAN_CANCELED:I = -0x2

.field private static final SCAN_DISMISS:I = -0x3

.field public static final SCAN_ENDPAGE_CB:I = 0xb

.field private static final SCAN_ERROR:I = -0x1

.field private static final SCAN_FINISHED:I = 0x2

.field public static final SCAN_RECV_IMGBLOCK_CB:I = 0xa

.field private static final SCAN_REDRAWBITMAP:I = -0x64

.field private static final START_SCAN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ScanActivity"


# instance fields
.field private area:Landroid/graphics/RectF;

.field private areaHeightOnScreen:I

.field private areaWidthOnScreen:I

.field private bAutoStartScan:Z

.field private btnCancelScan:Landroid/widget/Button;

.field private btnMail:Landroid/widget/Button;

.field private btnNext:Landroid/widget/Button;

.field private btnPrev:Landroid/widget/Button;

.field private btnPrint:Landroid/widget/Button;

.field private btnSave:Landroid/widget/Button;

.field private btnScan:Landroid/widget/Button;

.field private confirmAlertDialog:Landroid/app/AlertDialog;

.field private convertCount:I

.field private doscan:Ljava/lang/Thread;

.field private error:I

.field private errorDialog:Landroid/app/AlertDialog;

.field private escan:Lepson/scan/lib/escanLib;

.field private extParam:Lcom/epson/iprint/shared/SharedParamScan;

.field private flScanResult:Landroid/widget/FrameLayout;

.field private gestureDetector:Landroid/view/GestureDetector;

.field private gestureListener:Landroid/view/View$OnTouchListener;

.field private isConfirmCancel:Z

.field private isCustomAction:Z

.field private isDecoding:Z

.field private isNeedClearBm:Z

.field private isNeedDeleteTempFile:Z

.field private isNeedGetScanSize:Z

.field private isNeedRedrawBitmap:Z

.field private isNeedScale:Z

.field private isNeedUpdateScanningArea:Z

.field private isStopScan:Z

.field private llScanArea:Landroid/widget/LinearLayout;

.field private llScanAreaBackground:Landroid/widget/LinearLayout;

.field private llScanningProgress:Landroid/widget/LinearLayout;

.field private mActivityPaused:Z

.field private mButtonCancelScanListener:Landroid/view/View$OnClickListener;

.field private mButtonMailListener:Landroid/view/View$OnClickListener;

.field private mButtonNextListener:Landroid/view/View$OnClickListener;

.field private mButtonPrevListener:Landroid/view/View$OnClickListener;

.field private mButtonPrintListener:Landroid/view/View$OnClickListener;

.field private mButtonSaveListener:Landroid/view/View$OnClickListener;

.field private mButtonScanListener:Landroid/view/View$OnClickListener;

.field private mI1ScanParams:Lepson/scan/lib/I1ScanParams;

.field private mOnPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field private mScanAreaBackgroundViewSizeChanged:Z

.field private mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

.field private oldScannerId:Ljava/lang/String;

.field private previousTotalScanned:I

.field private resolution:I

.field private scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

.field private scanAreaBackground:Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

.field private scanOrigin:I

.field private scanningProgressHandler:Landroid/os/Handler;

.field private tvScanResult:Landroid/widget/TextView;

.field private usingScannerId:Ljava/lang/String;

.field private usingScannerIp:Ljava/lang/String;

.field private usingScannerLocation:I

.field private usingScannerModel:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanBaseView;-><init>()V

    const/4 v0, 0x1

    .line 127
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isNeedGetScanSize:Z

    const/4 v1, 0x0

    .line 128
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isStopScan:Z

    .line 129
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isNeedRedrawBitmap:Z

    .line 130
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isNeedScale:Z

    .line 132
    iput v1, p0, Lepson/scan/activity/ScanActivity;->error:I

    .line 140
    iput v1, p0, Lepson/scan/activity/ScanActivity;->resolution:I

    const/4 v2, 0x2

    .line 141
    iput v2, p0, Lepson/scan/activity/ScanActivity;->scanOrigin:I

    .line 146
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    .line 154
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->bAutoStartScan:Z

    .line 164
    iput v1, p0, Lepson/scan/activity/ScanActivity;->previousTotalScanned:I

    .line 364
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isNeedDeleteTempFile:Z

    .line 506
    iput v1, p0, Lepson/scan/activity/ScanActivity;->areaWidthOnScreen:I

    .line 507
    iput v1, p0, Lepson/scan/activity/ScanActivity;->areaHeightOnScreen:I

    .line 537
    new-instance v0, Lepson/scan/activity/ScanActivity$4;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$4;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->mOnPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 868
    new-instance v0, Lepson/scan/activity/ScanActivity$5;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$5;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->mButtonNextListener:Landroid/view/View$OnClickListener;

    .line 891
    new-instance v0, Lepson/scan/activity/ScanActivity$6;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$6;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->mButtonPrevListener:Landroid/view/View$OnClickListener;

    .line 1144
    new-instance v0, Lepson/scan/activity/ScanActivity$9;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$9;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->mButtonScanListener:Landroid/view/View$OnClickListener;

    .line 1159
    new-instance v0, Lepson/scan/activity/ScanActivity$10;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$10;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->mButtonCancelScanListener:Landroid/view/View$OnClickListener;

    .line 1353
    new-instance v0, Lepson/scan/activity/ScanActivity$11;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$11;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->mButtonMailListener:Landroid/view/View$OnClickListener;

    .line 1375
    new-instance v0, Lepson/scan/activity/ScanActivity$12;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$12;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->mButtonPrintListener:Landroid/view/View$OnClickListener;

    .line 1402
    new-instance v0, Lepson/scan/activity/ScanActivity$13;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$13;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->mButtonSaveListener:Landroid/view/View$OnClickListener;

    .line 1440
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    .line 1497
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isNeedClearBm:Z

    .line 1498
    iput v1, p0, Lepson/scan/activity/ScanActivity;->convertCount:I

    .line 1499
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isDecoding:Z

    .line 1501
    new-instance v0, Landroid/os/Handler;

    new-instance v2, Lepson/scan/activity/ScanActivity$14;

    invoke-direct {v2, p0}, Lepson/scan/activity/ScanActivity$14;-><init>(Lepson/scan/activity/ScanActivity;)V

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    .line 1996
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isConfirmCancel:Z

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/ScanActivity;)Landroid/view/GestureDetector;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->gestureDetector:Landroid/view/GestureDetector;

    return-object p0
.end method

.method static synthetic access$100(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaSet;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaBackground;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->scanAreaBackground:Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

    return-object p0
.end method

.method static synthetic access$1100(Lepson/scan/activity/ScanActivity;)Landroid/widget/FrameLayout;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->flScanResult:Landroid/widget/FrameLayout;

    return-object p0
.end method

.method static synthetic access$1200(Lepson/scan/activity/ScanActivity;)I
    .locals 0

    .line 84
    iget p0, p0, Lepson/scan/activity/ScanActivity;->resolution:I

    return p0
.end method

.method static synthetic access$1202(Lepson/scan/activity/ScanActivity;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/scan/activity/ScanActivity;->resolution:I

    return p1
.end method

.method static synthetic access$1300(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    return p0
.end method

.method static synthetic access$1302(Lepson/scan/activity/ScanActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    return p1
.end method

.method static synthetic access$1400(Lepson/scan/activity/ScanActivity;)I
    .locals 0

    .line 84
    iget p0, p0, Lepson/scan/activity/ScanActivity;->areaHeightOnScreen:I

    return p0
.end method

.method static synthetic access$1402(Lepson/scan/activity/ScanActivity;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/scan/activity/ScanActivity;->areaHeightOnScreen:I

    return p1
.end method

.method static synthetic access$1500(Lepson/scan/activity/ScanActivity;)I
    .locals 0

    .line 84
    iget p0, p0, Lepson/scan/activity/ScanActivity;->areaWidthOnScreen:I

    return p0
.end method

.method static synthetic access$1502(Lepson/scan/activity/ScanActivity;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/scan/activity/ScanActivity;->areaWidthOnScreen:I

    return p1
.end method

.method static synthetic access$1600(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->initFrameWithScreensize()V

    return-void
.end method

.method static synthetic access$1700(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->isNeedRedrawBitmap:Z

    return p0
.end method

.method static synthetic access$1702(Lepson/scan/activity/ScanActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedRedrawBitmap:Z

    return p1
.end method

.method static synthetic access$1800(Lepson/scan/activity/ScanActivity;I)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->chooseScanSize(I)V

    return-void
.end method

.method static synthetic access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$200(Lepson/scan/activity/ScanActivity;)Landroid/widget/Button;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$2000(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->isCompletedScanProcess()Z

    move-result p0

    return p0
.end method

.method static synthetic access$2100(Lepson/scan/activity/ScanActivity;I)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->updateThumbResult(I)V

    return-void
.end method

.method static synthetic access$2200(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->showDisplayingResult()V

    return-void
.end method

.method static synthetic access$2300(Lepson/scan/activity/ScanActivity;)I
    .locals 0

    .line 84
    iget p0, p0, Lepson/scan/activity/ScanActivity;->error:I

    return p0
.end method

.method static synthetic access$2302(Lepson/scan/activity/ScanActivity;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/scan/activity/ScanActivity;->error:I

    return p1
.end method

.method static synthetic access$2400(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->canStartScan()Z

    move-result p0

    return p0
.end method

.method static synthetic access$2500(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->usingScannerId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2600(Lepson/scan/activity/ScanActivity;)I
    .locals 0

    .line 84
    iget p0, p0, Lepson/scan/activity/ScanActivity;->usingScannerLocation:I

    return p0
.end method

.method static synthetic access$2700(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->usingScannerIp:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2702(Lepson/scan/activity/ScanActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 84
    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->usingScannerIp:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    return-object p0
.end method

.method static synthetic access$2900(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->isStopScan:Z

    return p0
.end method

.method static synthetic access$2902(Lepson/scan/activity/ScanActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isStopScan:Z

    return p1
.end method

.method static synthetic access$300(Lepson/scan/activity/ScanActivity;)Landroid/widget/Button;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$3002(Lepson/scan/activity/ScanActivity;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/scan/activity/ScanActivity;->previousTotalScanned:I

    return p1
.end method

.method static synthetic access$3100(Lepson/scan/activity/ScanActivity;)Lcom/epson/iprint/shared/SharedParamScan;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    return-object p0
.end method

.method static synthetic access$3202(Lepson/scan/activity/ScanActivity;Lepson/scan/lib/I1ScanParams;)Lepson/scan/lib/I1ScanParams;
    .locals 0

    .line 84
    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->mI1ScanParams:Lepson/scan/lib/I1ScanParams;

    return-object p1
.end method

.method static synthetic access$3300(Lepson/scan/activity/ScanActivity;Lcom/epson/iprint/shared/SharedParamScan;)Ljava/lang/String;
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->makeImageName(Lcom/epson/iprint/shared/SharedParamScan;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$3400(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->check3GAndStartPrint()V

    return-void
.end method

.method static synthetic access$3500(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->hideScanningPop()V

    return-void
.end method

.method static synthetic access$3600(Lepson/scan/activity/ScanActivity;Ljava/lang/String;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->confirmationDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3700(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    return p0
.end method

.method static synthetic access$3800(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->isConfirmCancel:Z

    return p0
.end method

.method static synthetic access$3802(Lepson/scan/activity/ScanActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isConfirmCancel:Z

    return p1
.end method

.method static synthetic access$3900(Lepson/scan/activity/ScanActivity;)I
    .locals 0

    .line 84
    iget p0, p0, Lepson/scan/activity/ScanActivity;->convertCount:I

    return p0
.end method

.method static synthetic access$3902(Lepson/scan/activity/ScanActivity;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/scan/activity/ScanActivity;->convertCount:I

    return p1
.end method

.method static synthetic access$400(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->mActivityPaused:Z

    return p0
.end method

.method static synthetic access$4000(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->isDecoding:Z

    return p0
.end method

.method static synthetic access$4002(Lepson/scan/activity/ScanActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isDecoding:Z

    return p1
.end method

.method static synthetic access$4100(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->showScanningPop()V

    return-void
.end method

.method static synthetic access$4200(Lepson/scan/activity/ScanActivity;)Ljava/lang/Thread;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->doscan:Ljava/lang/Thread;

    return-object p0
.end method

.method static synthetic access$4300(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->scanStart()V

    return-void
.end method

.method static synthetic access$4400(Lepson/scan/activity/ScanActivity;Ljava/lang/String;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->convert(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4500(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    return-object p0
.end method

.method static synthetic access$4600(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->updateDoneScanJob()V

    return-void
.end method

.method static synthetic access$4700(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->loggerScanCountUp()V

    return-void
.end method

.method static synthetic access$4800(Lepson/scan/activity/ScanActivity;II)V
    .locals 0

    .line 84
    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanActivity;->checkLoggerServer(II)V

    return-void
.end method

.method static synthetic access$4900(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->updateScreenPreviewToLastData()V

    return-void
.end method

.method static synthetic access$500(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->mScanAreaBackgroundViewSizeChanged:Z

    return p0
.end method

.method static synthetic access$5000(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    return-object p0
.end method

.method static synthetic access$502(Lepson/scan/activity/ScanActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->mScanAreaBackgroundViewSizeChanged:Z

    return p1
.end method

.method static synthetic access$5100(Lepson/scan/activity/ScanActivity;I)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->setupErrorMessage(I)V

    return-void
.end method

.method static synthetic access$5200(Lepson/scan/activity/ScanActivity;)Landroid/graphics/RectF;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    return-object p0
.end method

.method static synthetic access$5202(Lepson/scan/activity/ScanActivity;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 0

    .line 84
    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    return-object p1
.end method

.method static synthetic access$5300(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->prevPage()V

    return-void
.end method

.method static synthetic access$5400(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->nextPage()V

    return-void
.end method

.method static synthetic access$5500(Lepson/scan/activity/ScanActivity;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->showBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$5600(Lepson/scan/activity/ScanActivity;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->displayPreview(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$600(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->isNeedGetScanSize:Z

    return p0
.end method

.method static synthetic access$602(Lepson/scan/activity/ScanActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedGetScanSize:Z

    return p1
.end method

.method static synthetic access$700(Lepson/scan/activity/ScanActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/scan/activity/ScanActivity;->isNeedScale:Z

    return p0
.end method

.method static synthetic access$702(Lepson/scan/activity/ScanActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedScale:Z

    return p1
.end method

.method static synthetic access$800(Lepson/scan/activity/ScanActivity;)Landroid/widget/LinearLayout;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->llScanArea:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic access$900(Lepson/scan/activity/ScanActivity;)Landroid/widget/LinearLayout;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/scan/activity/ScanActivity;->llScanAreaBackground:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method private callScanSetting()V
    .locals 3

    .line 2262
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 2265
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->usingScannerId:Ljava/lang/String;

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->oldScannerId:Ljava/lang/String;

    .line 2267
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/ScanSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2271
    iget-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    if-eqz v1, :cond_1

    .line 2272
    iget-object v1, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v1}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_main()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_0

    iget-object v1, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v1}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_sub()I

    move-result v1

    if-lez v1, :cond_0

    const-string v1, "hide-resolution"

    .line 2273
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const-string v1, "hide-color-mode"

    .line 2277
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "external-scan-params"

    .line 2279
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_1
    const-string v1, "android.intent.action.VIEW"

    .line 2281
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x2

    .line 2282
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private canStartScan()Z
    .locals 8

    .line 1171
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lepson/print/CommonDefine;->DEFAULT_DIR:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1172
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v1

    long-to-double v1, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v3

    long-to-double v3, v3

    mul-double v1, v1, v3

    .line 1173
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxwidth()I

    move-result v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxheight()I

    move-result v3

    mul-int v0, v0, v3

    mul-int/lit8 v0, v0, 0x3

    int-to-double v3, v0

    .line 1174
    iget-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    const/4 v5, 0x1

    if-nez v0, :cond_0

    .line 1176
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->getAvailableScanPageCount()I

    move-result v0

    add-int/2addr v0, v5

    int-to-double v6, v0

    mul-double v3, v3, v6

    goto :goto_0

    :cond_0
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double v3, v3, v6

    :goto_0
    cmpl-double v0, v1, v3

    if-lez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    return v5
.end method

.method private check3GAndStartPrint()V
    .locals 1

    .line 1156
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0, v0}, Lepson/scan/activity/ScanContinueParam;->judgeScanContinue(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method private checkLoggerServer(II)V
    .locals 2

    .line 2371
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    iget v0, v0, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    if-nez v0, :cond_0

    .line 2372
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    const/16 v1, 0x2005

    iput v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 2374
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    iget v1, p0, Lepson/scan/activity/ScanActivity;->previousTotalScanned:I

    sub-int/2addr p1, v1

    iput p1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    .line 2375
    iput p2, v0, Lcom/epson/iprint/prtlogger/CommonLog;->connectionType:I

    .line 2378
    new-instance p1, Lepson/scan/lib/ScannerInfo;

    invoke-direct {p1}, Lepson/scan/lib/ScannerInfo;-><init>()V

    .line 2379
    invoke-static {}, Lepson/scan/lib/ScanInfoStorage;->getInstance()Lepson/scan/lib/ScanInfoStorage;

    move-result-object p2

    .line 2380
    invoke-virtual {p2, p0, p1}, Lepson/scan/lib/ScanInfoStorage;->loadScannerConnectivityInfo(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 2381
    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p2, Lcom/epson/iprint/prtlogger/CommonLog;->printerName:Ljava/lang/String;

    .line 2384
    monitor-enter p0

    .line 2385
    :try_start_0
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->mI1ScanParams:Lepson/scan/lib/I1ScanParams;

    .line 2386
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2387
    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    iget p2, p2, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    if-lez p2, :cond_1

    if-eqz p1, :cond_1

    .line 2388
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result p2

    .line 2389
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-static {p0, p1, v0, p2}, Lcom/epson/iprint/prtlogger/Analytics;->sendScanI1Log(Landroid/content/Context;Lepson/scan/lib/I1ScanParams;Lcom/epson/iprint/prtlogger/CommonLog;Z)V

    :cond_1
    return-void

    :catchall_0
    move-exception p1

    .line 2386
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private chooseScanSize(I)V
    .locals 15

    move-object v6, p0

    move/from16 v7, p1

    .line 763
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    if-ne v7, v0, :cond_3

    .line 765
    iget v0, v6, Lepson/scan/activity/ScanActivity;->resolution:I

    const/16 v3, 0x4b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x12c

    if-eq v0, v3, :cond_0

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    :cond_0
    move-wide v3, v1

    goto :goto_0

    :cond_1
    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    .line 782
    :goto_0
    iget-object v0, v6, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    const-string v5, "epson.scanner.supported.Options"

    const-string v8, "SCAN_REFS_MAX_WIDTH"

    invoke-static {v0, v5, v8}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 783
    iget-object v5, v6, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    const-string v8, "epson.scanner.supported.Options"

    const-string v9, "SCAN_REFS_MAX_HEIGHT"

    invoke-static {v5, v8, v9}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-nez v0, :cond_2

    const-wide v8, 0x4093ec0000000000L    # 1275.0

    mul-double v8, v8, v3

    double-to-int v0, v8

    :cond_2
    if-nez v5, :cond_4

    const-wide v8, 0x409b6c0000000000L    # 1755.0

    mul-double v3, v3, v8

    double-to-int v5, v3

    goto :goto_1

    .line 794
    :cond_3
    iget v0, v6, Lepson/scan/activity/ScanActivity;->resolution:I

    invoke-static {v7, v0}, Lepson/scan/lib/ScanSizeHelper;->getPaperSize(II)Landroid/graphics/Point;

    move-result-object v0

    .line 795
    iget v3, v0, Landroid/graphics/Point;->x:I

    .line 796
    iget v5, v0, Landroid/graphics/Point;->y:I

    move v0, v3

    .line 801
    :cond_4
    :goto_1
    iget-boolean v3, v6, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    const/4 v8, 0x1

    const/4 v4, 0x2

    if-eqz v3, :cond_6

    .line 803
    iget v1, v6, Lepson/scan/activity/ScanActivity;->scanOrigin:I

    if-ne v1, v4, :cond_5

    .line 804
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->setmTop(Landroid/graphics/Point;)V

    .line 806
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    int-to-double v9, v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanWidthOnScreen()D

    move-result-wide v11

    mul-double v11, v11, v9

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxwidth()I

    move-result v0

    int-to-double v13, v0

    div-double/2addr v11, v13

    add-double/2addr v2, v11

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 807
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    int-to-double v11, v5

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanHeightOnScreen()D

    move-result-wide v4

    mul-double v4, v4, v11

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxheight()I

    move-result v13

    int-to-double v13, v13

    div-double/2addr v4, v13

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v3, v2

    invoke-direct {v1, v0, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 806
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->setmBot(Landroid/graphics/Point;)V

    .line 809
    new-instance v13, Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-double v2, v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-double v4, v0

    move-object v0, v13

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {p0, v13}, Lepson/scan/activity/ScanActivity;->setPiBaseTop(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 810
    new-instance v13, Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-double v0, v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanWidthOnScreen()D

    move-result-wide v2

    mul-double v9, v9, v2

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxwidth()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v9, v2

    add-double v2, v0, v9

    .line 811
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-double v0, v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanHeightOnScreen()D

    move-result-wide v4

    mul-double v11, v11, v4

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxheight()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v11, v4

    add-double v4, v0, v11

    move-object v0, v13

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    .line 810
    invoke-virtual {p0, v13}, Lepson/scan/activity/ScanActivity;->setPiBaseBot(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 813
    invoke-virtual {p0, v8}, Lepson/scan/activity/ScanActivity;->setScreenStatus(I)V

    .line 816
    :cond_5
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->hideTotalScanned()V

    goto/16 :goto_2

    .line 821
    :cond_6
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setMaxwidth(I)V

    .line 822
    invoke-virtual {p0, v5}, Lepson/scan/activity/ScanActivity;->setMaxheight(I)V

    .line 824
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxheight()I

    move-result v0

    int-to-double v9, v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanHeightOnScreen()D

    move-result-wide v11

    div-double/2addr v9, v11

    invoke-virtual {p0, v9, v10}, Lepson/scan/activity/ScanActivity;->setScale(D)V

    .line 825
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxwidth()I

    move-result v0

    int-to-double v9, v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getScale()D

    move-result-wide v11

    div-double/2addr v9, v11

    double-to-int v0, v9

    int-to-double v9, v0

    invoke-virtual {p0, v9, v10}, Lepson/scan/activity/ScanActivity;->setMaxScanWidthOnScreen(D)V

    .line 829
    new-instance v0, Landroid/graphics/Point;

    iget v3, v6, Lepson/scan/activity/ScanActivity;->areaWidthOnScreen:I

    int-to-double v9, v3

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanWidthOnScreen()D

    move-result-wide v11

    sub-double/2addr v9, v11

    double-to-int v3, v9

    div-int/2addr v3, v4

    iget v4, v6, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    invoke-direct {v0, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setmBaseTop(Landroid/graphics/Point;)V

    .line 831
    new-instance v0, Landroid/graphics/Point;

    iget v3, v6, Lepson/scan/activity/ScanActivity;->areaWidthOnScreen:I

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    iget v4, v6, Lepson/scan/activity/ScanActivity;->areaHeightOnScreen:I

    iget v5, v6, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    sub-int/2addr v4, v5

    invoke-direct {v0, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setmBaseBot(Landroid/graphics/Point;)V

    .line 833
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setmTop(Landroid/graphics/Point;)V

    .line 834
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setmBot(Landroid/graphics/Point;)V

    .line 837
    new-instance v9, Lepson/scan/activity/ScanBaseView$PointInfo;

    iget v0, v6, Lepson/scan/activity/ScanActivity;->areaWidthOnScreen:I

    int-to-double v3, v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanWidthOnScreen()D

    move-result-wide v10

    sub-double/2addr v3, v10

    div-double v2, v3, v1

    iget v0, v6, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    int-to-double v4, v0

    move-object v0, v9

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {p0, v9}, Lepson/scan/activity/ScanActivity;->setPiBaseTop(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 838
    new-instance v9, Lepson/scan/activity/ScanBaseView$PointInfo;

    iget v0, v6, Lepson/scan/activity/ScanActivity;->areaWidthOnScreen:I

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    int-to-double v2, v0

    iget v0, v6, Lepson/scan/activity/ScanActivity;->areaHeightOnScreen:I

    iget v1, v6, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    sub-int/2addr v0, v1

    int-to-double v4, v0

    move-object v0, v9

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {p0, v9}, Lepson/scan/activity/ScanActivity;->setPiBaseBot(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 840
    iget-boolean v0, v6, Lepson/scan/activity/ScanActivity;->isNeedClearBm:Z

    if-eqz v0, :cond_7

    .line 842
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->hideTotalScanned()V

    const/4 v0, 0x0

    .line 843
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setBm(Landroid/graphics/Bitmap;)V

    .line 847
    :cond_7
    :goto_2
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmTop()Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getmBot()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/ScanActivity;->calculateTheCenterPoint(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setmCenter(Landroid/graphics/Point;)V

    .line 849
    invoke-virtual {p0, v8}, Lepson/scan/activity/ScanActivity;->setSetSize(Z)V

    .line 851
    iget-object v0, v6, Lepson/scan/activity/ScanActivity;->scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    .line 852
    iget-object v0, v6, Lepson/scan/activity/ScanActivity;->scanAreaBackground:Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->postInvalidate()V

    .line 855
    iget-boolean v0, v6, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    if-eqz v0, :cond_8

    .line 856
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v2, "SCAN_REFS_SCANNER_CHOSEN_SIZE_DOC"

    invoke-static {v0, v1, v2, v7}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    .line 859
    :cond_8
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v2, "SCAN_REFS_SCANNER_CHOSEN_SIZE_ADF"

    invoke-static {v0, v1, v2, v7}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    :goto_3
    return-void
.end method

.method private confirmationDialog(Ljava/lang/String;)V
    .locals 3

    .line 1999
    iget-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isConfirmCancel:Z

    if-nez v0, :cond_0

    .line 2001
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    .line 2002
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    const v1, 0x7f0700ad

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setIcon(I)V

    .line 2004
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 2005
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    new-instance v1, Lepson/scan/activity/ScanActivity$15;

    invoke-direct {v1, p0}, Lepson/scan/activity/ScanActivity$15;-><init>(Lepson/scan/activity/ScanActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 2015
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2016
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    const/4 v0, -0x2

    const v1, 0x7f0e04e6

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lepson/scan/activity/ScanActivity$16;

    invoke-direct {v2, p0}, Lepson/scan/activity/ScanActivity$16;-><init>(Lepson/scan/activity/ScanActivity;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2024
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    const/4 v0, -0x1

    const v1, 0x7f0e052b

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lepson/scan/activity/ScanActivity$17;

    invoke-direct {v2, p0}, Lepson/scan/activity/ScanActivity$17;-><init>(Lepson/scan/activity/ScanActivity;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2034
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->confirmAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    :cond_0
    return-void
.end method

.method private convert(Ljava/lang/String;)V
    .locals 2

    .line 2338
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2339
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2341
    iget-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2342
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/shared/SharedParamScan;->setArrayOutFilePath(Ljava/lang/String;)V

    goto :goto_0

    .line 2344
    :cond_0
    invoke-static {p1}, Lepson/common/Utils;->deleteTempFile(Ljava/lang/String;)V

    .line 2347
    :goto_0
    iget p1, p0, Lepson/scan/activity/ScanActivity;->convertCount:I

    add-int/2addr p1, v1

    iput p1, p0, Lepson/scan/activity/ScanActivity;->convertCount:I

    .line 2349
    iget p1, p0, Lepson/scan/activity/ScanActivity;->convertCount:I

    invoke-direct {p0, p1}, Lepson/scan/activity/ScanActivity;->updateThumbResult(I)V

    return-void
.end method

.method private disableFunctionButtons()V
    .locals 4

    .line 1980
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnSave:Landroid/widget/Button;

    const/4 v1, 0x0

    const v2, 0x7f07013d

    const v3, 0x7f0e04fc

    invoke-static {v0, v2, v3, v1}, Lepson/scan/activity/CustomButtonWrapper;->setScanButtonState(Landroid/widget/Button;IIZ)V

    .line 1981
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnMail:Landroid/widget/Button;

    const v2, 0x7f070139

    const v3, 0x7f0e0474

    invoke-static {v0, v2, v3, v1}, Lepson/scan/activity/CustomButtonWrapper;->setScanButtonState(Landroid/widget/Button;IIZ)V

    .line 1982
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnPrint:Landroid/widget/Button;

    const v2, 0x7f07013a

    const v3, 0x7f0e04f4

    invoke-static {v0, v2, v3, v1}, Lepson/scan/activity/CustomButtonWrapper;->setScanButtonState(Landroid/widget/Button;IIZ)V

    return-void
.end method

.method private displayPreview(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 2326
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->isJobDone()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "epson.scan.activity.ScanActivity"

    const-string v1, "update UI"

    .line 2327
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2328
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanActivity;->setBm(Landroid/graphics/Bitmap;)V

    .line 2329
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanActivity;->setBmRectF(Landroid/graphics/RectF;)V

    .line 2330
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    invoke-virtual {p1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    :cond_0
    const/4 p1, 0x0

    .line 2333
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isDecoding:Z

    return-void
.end method

.method private enableFunctionButtons()V
    .locals 4

    .line 1987
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnSave:Landroid/widget/Button;

    const/4 v1, 0x1

    const v2, 0x7f07013d

    const v3, 0x7f0e04fc

    invoke-static {v0, v2, v3, v1}, Lepson/scan/activity/CustomButtonWrapper;->setScanButtonState(Landroid/widget/Button;IIZ)V

    .line 1988
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnMail:Landroid/widget/Button;

    const v2, 0x7f070139

    const v3, 0x7f0e0474

    invoke-static {v0, v2, v3, v1}, Lepson/scan/activity/CustomButtonWrapper;->setScanButtonState(Landroid/widget/Button;IIZ)V

    .line 1989
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnPrint:Landroid/widget/Button;

    const v2, 0x7f07013a

    const v3, 0x7f0e04f4

    invoke-static {v0, v2, v3, v1}, Lepson/scan/activity/CustomButtonWrapper;->setScanButtonState(Landroid/widget/Button;IIZ)V

    return-void
.end method

.method static getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 2

    .line 1394
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    const/4 v1, 0x4

    .line 1395
    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    return-object v0
.end method

.method public static getScanFileListFromReturnIntent(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const-string v0, "scan-file"

    .line 2399
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private getSelectedScannerInfo()V
    .locals 6

    .line 1444
    iget-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isNeedGetScanSize:Z

    if-eqz v0, :cond_6

    .line 1447
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v2, "SCAN_REFS_SCANNER_ID"

    invoke-static {v0, v1, v2}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1452
    iget-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v1, :cond_1

    .line 1453
    iget-object v1, p0, Lepson/scan/activity/ScanActivity;->usingScannerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1454
    iput-boolean v4, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    goto :goto_0

    .line 1456
    :cond_0
    iput-boolean v3, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    .line 1457
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->setBm(Landroid/graphics/Bitmap;)V

    .line 1461
    :cond_1
    :goto_0
    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->usingScannerId:Ljava/lang/String;

    .line 1463
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SCANNER_MODEL"

    invoke-static {v0, v1, v5}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->usingScannerModel:Ljava/lang/String;

    .line 1465
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SCANNER_LOCATION"

    invoke-static {v0, v1, v5}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->usingScannerIp:Ljava/lang/String;

    .line 1467
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SCANNER_ACCESSPATH"

    invoke-static {v0, v1, v5}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanActivity;->usingScannerLocation:I

    .line 1470
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SETTINGS_SOURCE"

    invoke-static {v0, v1, v5}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 1475
    :goto_1
    iget-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    if-eq v0, v1, :cond_3

    .line 1476
    iput-boolean v3, p0, Lepson/scan/activity/ScanActivity;->isNeedClearBm:Z

    .line 1477
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->setBm(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 1479
    :cond_3
    iput-boolean v4, p0, Lepson/scan/activity/ScanActivity;->isNeedClearBm:Z

    :goto_2
    if-nez v0, :cond_4

    .line 1482
    iget-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    if-ne v0, v1, :cond_4

    iget-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    if-nez v1, :cond_4

    .line 1483
    iput-boolean v4, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    :cond_4
    if-ne v0, v3, :cond_5

    .line 1486
    iget-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    if-ne v0, v1, :cond_5

    iget-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    if-nez v1, :cond_5

    .line 1487
    iput-boolean v4, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    goto :goto_3

    .line 1489
    :cond_5
    iput-boolean v3, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    .line 1492
    :goto_3
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    :cond_6
    return-void
.end method

.method private hideScanningPop()V
    .locals 2

    .line 1338
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->llScanningProgress:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 1340
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f0801e6

    .line 1341
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_0
    const-string v0, "ScanActivity"

    const-string v1, "clearFlags : FLAG_KEEP_SCREEN_ON "

    .line 1344
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1345
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method private hideTotalScanned()V
    .locals 2

    .line 1859
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->tvScanResult:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1861
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1862
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method private initFrameWithScreensize()V
    .locals 7

    const/16 v0, 0x14

    const/16 v1, 0x14

    .line 731
    :goto_0
    iget v2, p0, Lepson/scan/activity/ScanActivity;->areaHeightOnScreen:I

    if-ge v1, v2, :cond_0

    .line 732
    iput v1, p0, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    .line 735
    iget v3, p0, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-double v2, v2

    invoke-virtual {p0, v2, v3}, Lepson/scan/activity/ScanActivity;->setMaxScanHeightOnScreen(D)V

    .line 740
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxheight()I

    move-result v2

    int-to-double v2, v2

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanHeightOnScreen()D

    move-result-wide v4

    div-double/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lepson/scan/activity/ScanActivity;->setScale(D)V

    .line 747
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxwidth()I

    move-result v2

    int-to-double v2, v2

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getScale()D

    move-result-wide v4

    div-double/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lepson/scan/activity/ScanActivity;->setMaxScanWidthOnScreen(D)V

    .line 749
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxScanWidthOnScreen()D

    move-result-wide v2

    iget v4, p0, Lepson/scan/activity/ScanActivity;->areaWidthOnScreen:I

    sub-int/2addr v4, v0

    int-to-double v4, v4

    cmpl-double v6, v2, v4

    if-lez v6, :cond_0

    add-int/lit8 v1, v1, 0xa

    goto :goto_0

    :cond_0
    return-void
.end method

.method private isCompletedScanProcess()Z
    .locals 2

    .line 1819
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static localSetAllCaps(Landroid/widget/TextView;Z)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 2364
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2365
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setAllCaps(Z)V

    :cond_0
    return-void
.end method

.method private loggerScanCountUp()V
    .locals 1

    .line 1808
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-gtz v0, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method private makeImageName(Lcom/epson/iprint/shared/SharedParamScan;)Ljava/lang/String;
    .locals 3

    .line 1193
    invoke-virtual {p1}, Lcom/epson/iprint/shared/SharedParamScan;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1194
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/epson/iprint/shared/SharedParamScan;->getFolder_name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1195
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 1197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    .line 1198
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1199
    invoke-virtual {p1}, Lcom/epson/iprint/shared/SharedParamScan;->getFile_name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1203
    :cond_0
    new-instance p1, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1204
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    .line 1206
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1208
    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->removeTempScannedImage(Ljava/io/File;)V

    .line 1209
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->resetParameter()V

    .line 1212
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string p1, "/"

    .line 1214
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p1, 0x1

    .line 1215
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    .line 1216
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/2addr v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 p1, 0x5

    .line 1217
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p1, 0xb

    .line 1218
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p1, 0xc

    .line 1219
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p1, 0xd

    .line 1220
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1224
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private nextPage()V
    .locals 2

    .line 910
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    .line 911
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    const/16 v1, 0x12c

    if-gt v0, v1, :cond_1

    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    iget v1, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-le v0, v1, :cond_0

    goto :goto_0

    .line 915
    :cond_0
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    invoke-direct {p0, v0}, Lepson/scan/activity/ScanActivity;->updateThumbResult(I)V

    .line 916
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->showDisplayingResult()V

    .line 917
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    return-void

    .line 912
    :cond_1
    :goto_0
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    return-void
.end method

.method private onScanSettingEnd(ILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_a

    .line 2132
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->startI2ActivityIfEscIVersionMismatch()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_c

    .line 2137
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "ACT_RESULT"

    .line 2138
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_1

    const-string v1, "ACT_RESULT_BACK"

    .line 2140
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    return-void

    :cond_1
    const-string p2, "IS_NEW_SAVE"

    .line 2145
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p2

    iput-boolean p2, p0, Lepson/scan/activity/ScanActivity;->isNeedGetScanSize:Z

    const-string p2, "NO_CLEAR_RESULT"

    .line 2146
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    const/4 p2, 0x0

    .line 2148
    invoke-virtual {p0, p2}, Lepson/scan/activity/ScanActivity;->setBm(Landroid/graphics/Bitmap;)V

    .line 2149
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->hideTotalScanned()V

    .line 2150
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->disableFunctionButtons()V

    .line 2152
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lepson/scan/activity/ScanActivity;->removeTempContents(Landroid/content/Context;)V

    .line 2155
    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {p2}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 2156
    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {p2}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 2157
    iput v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 2158
    iput v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    .line 2160
    :cond_2
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 2161
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 2164
    :cond_3
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->resetParameter()V

    .line 2166
    :cond_4
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->getSelectedScannerInfo()V

    .line 2169
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_CHOSEN_SIZE"

    invoke-static {p2, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    .line 2170
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    if-eq p2, v0, :cond_5

    .line 2171
    invoke-direct {p0, p2}, Lepson/scan/activity/ScanActivity;->chooseScanSize(I)V

    goto :goto_0

    .line 2173
    :cond_5
    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->oldScannerId:Ljava/lang/String;

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    if-eqz p2, :cond_6

    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->usingScannerId:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    if-eqz p2, :cond_6

    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->oldScannerId:Ljava/lang/String;

    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->usingScannerId:Ljava/lang/String;

    .line 2174
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_6

    .line 2176
    invoke-static {}, Lepson/scan/lib/ScanSizeHelper;->getDefaultScanSize()I

    move-result p2

    invoke-direct {p0, p2}, Lepson/scan/activity/ScanActivity;->chooseScanSize(I)V

    goto :goto_0

    .line 2180
    :cond_6
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_MAX_WIDTH"

    invoke-static {p2, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    .line 2181
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.supported.Options"

    const-string v2, "SCAN_REFS_MAX_HEIGHT"

    invoke-static {v0, v1, v2}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2182
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxwidth()I

    move-result v1

    if-lt p2, v1, :cond_7

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMaxheight()I

    move-result p2

    if-ge v0, p2, :cond_8

    .line 2183
    :cond_7
    invoke-static {}, Lepson/scan/lib/ScanSizeHelper;->getDefaultScanSize()I

    move-result p2

    invoke-direct {p0, p2}, Lepson/scan/activity/ScanActivity;->chooseScanSize(I)V

    :cond_8
    :goto_0
    if-nez p1, :cond_9

    const/4 p1, 0x1

    .line 2188
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedGetScanSize:Z

    .line 2189
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedUpdateScanningArea:Z

    .line 2193
    :cond_9
    iget-boolean p1, p0, Lepson/scan/activity/ScanActivity;->bAutoStartScan:Z

    if-eqz p1, :cond_c

    .line 2195
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->mButtonScanListener:Landroid/view/View$OnClickListener;

    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->btnScan:Landroid/widget/Button;

    invoke-interface {p1, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    const-string p1, "ScanActivity"

    const-string p2, "Send CHECK_PRINTER Message."

    .line 2196
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    if-nez p1, :cond_c

    .line 2200
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->startI2ActivityIfEscIVersionMismatch()Z

    move-result p1

    if-eqz p1, :cond_b

    return-void

    .line 2203
    :cond_b
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isNeedGetScanSize:Z

    :cond_c
    :goto_1
    return-void
.end method

.method private preparingForScan()V
    .locals 2

    .line 1823
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/scan/lib/escanLib;->setJobDone(Z)V

    .line 1824
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0, v1}, Lepson/scan/lib/escanLib;->setScanning(Z)V

    .line 1826
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isConfirmCancel:Z

    .line 1828
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanActivity;->previousTotalScanned:I

    .line 1830
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getRectScanArea()Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    .line 1834
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1837
    iput v1, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 1838
    iput v1, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    .line 1841
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->hideTotalScanned()V

    .line 1842
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->disableFunctionButtons()V

    .line 1845
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method private prevPage()V
    .locals 2

    .line 921
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    .line 922
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    if-ge v0, v1, :cond_0

    .line 923
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    add-int/2addr v0, v1

    iput v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    return-void

    .line 926
    :cond_0
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    invoke-direct {p0, v0}, Lepson/scan/activity/ScanActivity;->updateThumbResult(I)V

    .line 927
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->showDisplayingResult()V

    .line 928
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    return-void
.end method

.method public static removeTempContents(Landroid/content/Context;)V
    .locals 5

    .line 467
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p0

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 468
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 469
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 470
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p0

    .line 471
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p0, v2

    .line 472
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 473
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 478
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 479
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_2
    return-void
.end method

.method private static removeTempScannedImage(Ljava/io/File;)V
    .locals 4

    .line 1232
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1233
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1234
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p0

    .line 1235
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p0, v1

    .line 1236
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1237
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1241
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1242
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    :cond_2
    return-void
.end method

.method private scanStart()V
    .locals 3

    .line 934
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->usingScannerModel:Ljava/lang/String;

    const v1, 0x7f0e04d6

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->usingScannerModel:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 949
    :cond_0
    iget-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 951
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v0}, Lcom/epson/iprint/shared/SharedParamScan;->clearArrayOutFilePath()V

    .line 957
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->doscan:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 962
    :cond_2
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->isValidArea()Z

    move-result v0

    if-nez v0, :cond_3

    return-void

    .line 966
    :cond_3
    iput v1, p0, Lepson/scan/activity/ScanActivity;->error:I

    const/4 v0, 0x0

    .line 969
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setBm(Landroid/graphics/Bitmap;)V

    .line 970
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->isNeedClearBm:Z

    .line 971
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanAreaBackground:Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

    invoke-virtual {v0, v2}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->hideText(Z)V

    .line 972
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanAreaBackground:Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

    invoke-virtual {v0}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->invalidate()V

    .line 974
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->preparingForScan()V

    .line 976
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 978
    new-instance v0, Lepson/scan/activity/ScanActivity$8;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanActivity$8;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanActivity;->doscan:Ljava/lang/Thread;

    .line 1137
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->doscan:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    .line 936
    :cond_4
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 937
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e045c

    .line 938
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e045d

    .line 939
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e04f2

    .line 940
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/scan/activity/ScanActivity$7;

    invoke-direct {v2, p0}, Lepson/scan/activity/ScanActivity$7;-><init>(Lepson/scan/activity/ScanActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 945
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private setupErrorMessage(I)V
    .locals 3

    const v0, -0x7a121

    const v1, 0x7f0e04f2

    if-eq p1, v0, :cond_5

    const/16 v0, -0x5dd

    if-eq p1, v0, :cond_4

    const/16 v0, -0x514

    if-eq p1, v0, :cond_3

    const/16 v0, -0x44c

    if-eq p1, v0, :cond_2

    const/16 v0, -0x3e9

    if-eq p1, v0, :cond_1

    const v0, 0x7f0e04d6

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    .line 1318
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04ac

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04ab

    .line 1319
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1320
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1318
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1268
    :pswitch_0
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e053c

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04ea

    .line 1269
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1270
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1268
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1301
    :pswitch_1
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04a2

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04a1

    .line 1302
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1303
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1301
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1296
    :pswitch_2
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04b3

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04b2

    .line 1297
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1298
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1296
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1291
    :pswitch_3
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04b1

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04b0

    .line 1292
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1293
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1291
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1286
    :pswitch_4
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04a8

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04a7

    .line 1287
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1288
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1286
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1256
    :cond_0
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 1257
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1258
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1256
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1280
    :cond_1
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e0471

    .line 1281
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04a0

    .line 1282
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1283
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1280
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto :goto_0

    .line 1275
    :cond_2
    :pswitch_5
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04a6

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04a5

    .line 1276
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1277
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1275
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto :goto_0

    .line 1262
    :cond_3
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04af

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04ae

    .line 1263
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1264
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1262
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto :goto_0

    .line 1307
    :cond_4
    :pswitch_6
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04a4

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04a3

    .line 1308
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1309
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1307
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    goto :goto_0

    .line 1312
    :cond_5
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    const v0, 0x7f0e01b3

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0e04c8

    .line 1313
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1314
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1312
    invoke-static {p1, v0, v2, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    const/4 v0, 0x1

    .line 2306
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isDecoding:Z

    .line 2307
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "start decode for preview "

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2309
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2310
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2312
    :try_start_0
    iget p1, p0, Lepson/scan/activity/ScanActivity;->areaHeightOnScreen:I

    iget v1, p0, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr p1, v1

    invoke-static {v0, p1}, Lepson/common/Utils;->decodeFile(Ljava/io/File;I)Landroid/graphics/Bitmap;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 2316
    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    .line 2314
    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :cond_0
    :goto_0
    const/4 p1, 0x0

    .line 2320
    :goto_1
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "end decode for preview "

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method private showDisplayingResult()V
    .locals 3

    .line 1866
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->tvScanResult:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showScanningPop()V
    .locals 2

    .line 1327
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->llScanningProgress:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 1329
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f0801e6

    .line 1330
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_0
    const-string v0, "ScanActivity"

    const-string v1, "addFlags : FLAG_KEEP_SCREEN_ON "

    .line 1333
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.method private showTotalScanned()V
    .locals 3

    .line 1854
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->tvScanResult:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1855
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->tvScanResult:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private startI2ActivityIfEscIVersionMismatch()Z
    .locals 4

    .line 335
    invoke-static {p0}, Lepson/scan/lib/ScanInfoStorage;->loadEscIVersion(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 342
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setBm(Landroid/graphics/Bitmap;)V

    .line 345
    iget-boolean v2, p0, Lepson/scan/activity/ScanActivity;->bAutoStartScan:Z

    iget-boolean v3, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    if-eqz v3, :cond_1

    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    :cond_1
    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-static {p0, v1, v2, v0, v3}, Lepson/scan/activity/ScanActivityCommonParams;->getEscIVersionChangeIntent(Landroid/content/Context;IZLcom/epson/iprint/shared/SharedParamScan;Lcom/epson/iprint/prtlogger/CommonLog;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 v0, 0x1

    return v0
.end method

.method private updateDoneScanJob()V
    .locals 4

    .line 1870
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 1873
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    iget v1, p0, Lepson/scan/activity/ScanActivity;->previousTotalScanned:I

    const/4 v2, 0x1

    if-ne v0, v1, :cond_1

    .line 1874
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v3, "SCAN_REFS_SETTINGS_SOURCE"

    invoke-static {v0, v1, v3}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v2, :cond_0

    const/16 v0, 0x64

    .line 1875
    iput v0, p0, Lepson/scan/activity/ScanActivity;->error:I

    .line 1876
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 1884
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 1885
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    iget v3, p0, Lepson/scan/activity/ScanActivity;->previousTotalScanned:I

    if-ne v0, v3, :cond_1

    const/16 v0, -0x44c

    .line 1889
    iput v0, p0, Lepson/scan/activity/ScanActivity;->error:I

    .line 1890
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 1896
    :cond_1
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-lez v0, :cond_2

    .line 1898
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->enableFunctionButtons()V

    .line 1900
    :cond_2
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-le v0, v2, :cond_3

    .line 1901
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    iput v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    .line 1902
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1903
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1904
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->showTotalScanned()V

    .line 1908
    :cond_3
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->isCompletedScanProcess()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1909
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    .line 1913
    :cond_4
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1916
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    invoke-direct {p0, v0}, Lepson/scan/activity/ScanActivity;->updateThumbResult(I)V

    return-void
.end method

.method private updateScreenPreviewToLastData()V
    .locals 2

    .line 1920
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1922
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    iput v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    .line 1924
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->showTotalScanned()V

    .line 1926
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    invoke-direct {p0, v0}, Lepson/scan/activity/ScanActivity;->updateThumbResult(I)V

    :cond_0
    return-void
.end method

.method private updateThumbResult(I)V
    .locals 5

    .line 1932
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    .line 1936
    :cond_0
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    const/4 v2, 0x0

    if-le v0, v1, :cond_3

    .line 1937
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    iget v3, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    const/16 v4, 0x8

    if-ne v0, v3, :cond_1

    .line 1938
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 1940
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1943
    :goto_0
    iget v0, p0, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    if-ne v0, v1, :cond_2

    .line 1944
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 1946
    :cond_2
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    :cond_3
    :goto_1
    if-ge p1, v1, :cond_4

    const/4 p1, 0x1

    .line 1951
    :cond_4
    iget v0, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-le p1, v0, :cond_5

    iget p1, p0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 1953
    :cond_5
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v0

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 1959
    :try_start_0
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    .line 1960
    iget-object v1, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v1, v1

    mul-int v3, v0, v1

    const v4, 0xc8000

    if-ge v3, v4, :cond_6

    const/16 v0, 0x400

    const/16 v1, 0x320

    .line 1965
    :cond_6
    invoke-static {p1, v0, v1, v2}, Lepson/print/Util/Photo;->createBitmapWithUri(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1966
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setBmRectF(Landroid/graphics/RectF;)V

    .line 1967
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanActivity;->setBm(Landroid/graphics/Bitmap;)V

    .line 1970
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    invoke-virtual {p1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 1974
    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception p1

    .line 1972
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_2
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 2041
    invoke-super {p0, p1, p2, p3}, Lepson/scan/activity/ScanBaseView;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0xa

    const/4 v1, -0x1

    if-eq p1, v0, :cond_4

    const/16 v0, 0x14

    const/4 v2, 0x0

    if-eq p1, v0, :cond_1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    if-eq p2, v1, :cond_0

    .line 2085
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->onBackPressed()V

    goto :goto_0

    .line 2079
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->startI2ActivityIfEscIVersionMismatch()Z

    move-result p1

    if-eqz p1, :cond_6

    return-void

    .line 2045
    :pswitch_1
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->resetEscanLib()V

    .line 2047
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {p1, p0}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    .line 2048
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Lepson/scan/lib/escanLib;->setScanHandler(Landroid/os/Handler;)V

    .line 2050
    invoke-direct {p0, p2, p3}, Lepson/scan/activity/ScanActivity;->onScanSettingEnd(ILandroid/content/Intent;)V

    .line 2051
    iput-boolean v2, p0, Lepson/scan/activity/ScanActivity;->bAutoStartScan:Z

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    if-ne p2, p1, :cond_2

    .line 2094
    invoke-static {p3}, Lepson/scan/activity/I2ScanActivity;->getAutoStartRequestedFromReturnIntent(Landroid/content/Intent;)Z

    move-result p2

    iput-boolean p2, p0, Lepson/scan/activity/ScanActivity;->bAutoStartScan:Z

    .line 2097
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    const-string p3, "IS_NEW_SAVE"

    .line 2099
    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "NO_CLEAR_RESULT"

    .line 2101
    invoke-virtual {p2, p1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2103
    invoke-direct {p0, v1, p2}, Lepson/scan/activity/ScanActivity;->onScanSettingEnd(ILandroid/content/Intent;)V

    .line 2104
    iput-boolean v2, p0, Lepson/scan/activity/ScanActivity;->bAutoStartScan:Z

    return-void

    .line 2109
    :cond_2
    iget-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    if-eqz p1, :cond_3

    .line 2111
    invoke-virtual {p0, p2, p3}, Lepson/scan/activity/ScanActivity;->setResult(ILandroid/content/Intent;)V

    .line 2112
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->finish()V

    return-void

    .line 2118
    :cond_3
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->finish()V

    return-void

    :cond_4
    if-eq p2, v1, :cond_5

    .line 2070
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->hideScanningPop()V

    goto :goto_0

    .line 2065
    :cond_5
    iget-object p1, p0, Lepson/scan/activity/ScanActivity;->mButtonScanListener:Landroid/view/View$OnClickListener;

    iget-object p2, p0, Lepson/scan/activity/ScanActivity;->btnScan:Landroid/widget/Button;

    invoke-interface {p1, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    const-string p1, "ScanActivity"

    const-string p2, "Send CHECK_PRINTER Message."

    .line 2066
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_0
    :pswitch_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .line 382
    invoke-super {p0}, Lepson/scan/activity/ScanBaseView;->onBackPressed()V

    .line 383
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 384
    invoke-static {p0}, Lepson/scan/activity/ScanActivity;->removeTempContents(Landroid/content/Context;)V

    .line 386
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .line 511
    invoke-super {p0, p1}, Lepson/scan/activity/ScanBaseView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const/4 p1, 0x1

    .line 512
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedGetScanSize:Z

    .line 513
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedScale:Z

    .line 515
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    .line 517
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getRectScanArea()Landroid/graphics/RectF;

    move-result-object v1

    iput-object v1, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    .line 518
    iget v1, v0, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    if-ne v1, v2, :cond_0

    iget v1, v0, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    if-ne v1, v2, :cond_0

    iget v1, v0, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    float-to-int v2, v2

    if-ne v1, v2, :cond_0

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget-object v1, p0, Lepson/scan/activity/ScanActivity;->area:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_0

    .line 524
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->isNeedRedrawBitmap:Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 529
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isNeedRedrawBitmap:Z

    .line 532
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lepson/scan/activity/ScanActivity;->mActivityPaused:Z

    if-eqz v0, :cond_2

    .line 533
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->mScanAreaBackgroundViewSizeChanged:Z

    :cond_2
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 168
    invoke-super {p0, p1}, Lepson/scan/activity/ScanBaseView;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a00b2

    .line 169
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanActivity;->setContentView(I)V

    const/4 v0, 0x1

    const v1, 0x7f0e04fe

    .line 171
    invoke-virtual {p0, v1, v0}, Lepson/scan/activity/ScanActivity;->setActionBar(IZ)V

    .line 173
    invoke-virtual {p0, p0}, Lepson/scan/activity/ScanActivity;->setContext(Landroid/content/Context;)V

    .line 174
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->mActivityPaused:Z

    const/4 v1, 0x0

    .line 175
    iput-boolean v1, p0, Lepson/scan/activity/ScanActivity;->mScanAreaBackgroundViewSizeChanged:Z

    .line 177
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 178
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 180
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 182
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.epson.iprint.scan"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 183
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    const-string v3, "extParam"

    .line 184
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/epson/iprint/shared/SharedParamScan;

    iput-object v3, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    goto :goto_0

    .line 186
    :cond_0
    new-instance v3, Lcom/epson/iprint/shared/SharedParamScan;

    invoke-direct {v3}, Lcom/epson/iprint/shared/SharedParamScan;-><init>()V

    iput-object v3, p0, Lepson/scan/activity/ScanActivity;->extParam:Lcom/epson/iprint/shared/SharedParamScan;

    :goto_0
    :try_start_0
    const-string v3, "scan-log"

    .line 191
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/epson/iprint/prtlogger/CommonLog;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :catch_0
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    if-nez v2, :cond_1

    .line 196
    new-instance v2, Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {v2}, Lcom/epson/iprint/prtlogger/CommonLog;-><init>()V

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->mScanLog:Lcom/epson/iprint/prtlogger/CommonLog;

    :cond_1
    const v2, 0x7f080092

    .line 199
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->btnScan:Landroid/widget/Button;

    .line 200
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnScan:Landroid/widget/Button;

    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->mButtonScanListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f080091

    .line 202
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->btnSave:Landroid/widget/Button;

    .line 203
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnSave:Landroid/widget/Button;

    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->mButtonSaveListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f08008d

    .line 205
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->btnMail:Landroid/widget/Button;

    .line 206
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnMail:Landroid/widget/Button;

    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->mButtonMailListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f080090

    .line 208
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->btnPrint:Landroid/widget/Button;

    .line 209
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnPrint:Landroid/widget/Button;

    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->mButtonPrintListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->getSelectedScannerInfo()V

    .line 213
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->disableFunctionButtons()V

    .line 215
    iget-boolean v2, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    const/16 v3, 0x8

    if-ne v2, v0, :cond_2

    .line 216
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnMail:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 217
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnPrint:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_2
    const v2, 0x7f0801e5

    .line 220
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->llScanAreaBackground:Landroid/widget/LinearLayout;

    .line 221
    new-instance v2, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;-><init>(Lepson/scan/activity/ScanBaseView;Landroid/content/Context;)V

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->scanAreaBackground:Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

    .line 222
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->llScanAreaBackground:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lepson/scan/activity/ScanActivity;->scanAreaBackground:Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const v2, 0x7f080353

    .line 224
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->tvScanResult:Landroid/widget/TextView;

    .line 226
    new-instance v2, Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;-><init>(Lepson/scan/activity/ScanBaseView;Landroid/content/Context;)V

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    const v2, 0x7f08014d

    .line 227
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->flScanResult:Landroid/widget/FrameLayout;

    .line 228
    new-instance v2, Landroid/view/GestureDetector;

    new-instance v4, Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;

    invoke-direct {v4, p0}, Lepson/scan/activity/ScanActivity$ScanAreaGestureDetector;-><init>(Lepson/scan/activity/ScanActivity;)V

    invoke-direct {v2, p0, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->gestureDetector:Landroid/view/GestureDetector;

    .line 229
    new-instance v2, Lepson/scan/activity/ScanActivity$1;

    invoke-direct {v2, p0}, Lepson/scan/activity/ScanActivity$1;-><init>(Lepson/scan/activity/ScanActivity;)V

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->gestureListener:Landroid/view/View$OnTouchListener;

    .line 236
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->flScanResult:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lepson/scan/activity/ScanActivity;->gestureListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v2, 0x7f08008e

    .line 238
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    .line 239
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    iget-object v4, p0, Lepson/scan/activity/ScanActivity;->mButtonNextListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    new-instance v4, Lepson/scan/activity/ScanActivity$2;

    invoke-direct {v4, p0}, Lepson/scan/activity/ScanActivity$2;-><init>(Lepson/scan/activity/ScanActivity;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v2, 0x7f08008f

    .line 259
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    .line 260
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    iget-object v4, p0, Lepson/scan/activity/ScanActivity;->mButtonPrevListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    new-instance v4, Lepson/scan/activity/ScanActivity$3;

    invoke-direct {v4, p0}, Lepson/scan/activity/ScanActivity$3;-><init>(Lepson/scan/activity/ScanActivity;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 278
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnNext:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 279
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnPrev:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 281
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    .line 285
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getEscan()Lepson/scan/lib/escanLib;

    move-result-object v2

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    .line 286
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v2, p0}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    .line 287
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Lepson/scan/lib/escanLib;->setScanHandler(Landroid/os/Handler;)V

    const v2, 0x7f0801e4

    .line 289
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->llScanArea:Landroid/widget/LinearLayout;

    .line 291
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->llScanArea:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->scanArea:Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const v2, 0x7f0801ef

    .line 293
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->llScanningProgress:Landroid/widget/LinearLayout;

    const v2, 0x7f08008c

    .line 294
    invoke-virtual {p0, v2}, Lepson/scan/activity/ScanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lepson/scan/activity/ScanActivity;->btnCancelScan:Landroid/widget/Button;

    .line 295
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->btnCancelScan:Landroid/widget/Button;

    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->mButtonCancelScanListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v2, p0, Lepson/scan/activity/ScanActivity;->llScanArea:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 298
    iget-object v3, p0, Lepson/scan/activity/ScanActivity;->mOnPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 301
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 302
    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    .line 303
    new-array v4, v3, [Ljava/lang/String;

    const v5, 0x7f0e0422

    invoke-virtual {p0, v5}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v1

    invoke-virtual {p0, v5}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 304
    new-array v3, v3, [Ljava/lang/String;

    const v5, 0x7f0e0420

    .line 305
    invoke-virtual {p0, v5}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v1

    .line 306
    invoke-virtual {p0, v5}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0e0424

    invoke-virtual {p0, v6}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v5, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v0

    .line 312
    new-instance v0, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v1, v2, v1

    invoke-direct {v0, v1, v4, v3}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 314
    invoke-static {p0, v2}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 p1, 0x4

    .line 315
    invoke-static {p0, v0, p1}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    :cond_3
    if-nez p1, :cond_4

    .line 322
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->startI2ActivityIfEscIVersionMismatch()Z

    move-result p1

    if-eqz p1, :cond_4

    return-void

    :cond_4
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 706
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b000a

    .line 707
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 708
    invoke-super {p0, p1}, Lepson/scan/activity/ScanBaseView;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const/4 p1, 0x1

    return p1
.end method

.method protected onDestroy()V
    .locals 3

    .line 395
    invoke-static {p0}, Lepson/scan/activity/ScanActivity;->removeTempContents(Landroid/content/Context;)V

    .line 396
    iget-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isCustomAction:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 397
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    .line 398
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 399
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lepson/common/Utils;->deleteTempFile(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 402
    :cond_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 403
    invoke-super {p0}, Lepson/scan/activity/ScanBaseView;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 358
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->llScanningProgress:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 361
    :cond_0
    invoke-super {p0, p1, p2}, Lepson/scan/activity/ScanBaseView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .line 430
    invoke-super {p0, p1}, Lepson/scan/activity/ScanBaseView;->onNewIntent(Landroid/content/Intent;)V

    .line 432
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 434
    iget-object v1, p0, Lepson/scan/activity/ScanActivity;->doscan:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    .line 436
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTag(Landroid/content/Context;Landroid/content/Intent;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 445
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 446
    const-class v1, Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "connectInfo"

    .line 447
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "changeMode"

    const/4 v1, 0x2

    .line 448
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 449
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 p1, 0x1

    .line 450
    iput-boolean p1, p0, Lepson/scan/activity/ScanActivity;->bAutoStartScan:Z

    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 717
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f08009f

    if-eq v0, v1, :cond_0

    .line 724
    invoke-super {p0, p1}, Lepson/scan/activity/ScanBaseView;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 719
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->callScanSetting()V

    const/4 p1, 0x1

    return p1
.end method

.method protected onPause()V
    .locals 1

    .line 418
    invoke-super {p0}, Lepson/scan/activity/ScanBaseView;->onPause()V

    .line 420
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    const/4 v0, 0x1

    .line 421
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->mActivityPaused:Z

    .line 423
    invoke-virtual {p0}, Lepson/scan/activity/ScanActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :cond_0
    return-void
.end method

.method public onRequestScanAction()V
    .locals 0

    .line 2213
    invoke-direct {p0}, Lepson/scan/activity/ScanActivity;->scanStart()V

    return-void
.end method

.method protected onRestart()V
    .locals 1

    const/4 v0, 0x0

    .line 367
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isNeedDeleteTempFile:Z

    .line 368
    invoke-super {p0}, Lepson/scan/activity/ScanBaseView;->onRestart()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 408
    invoke-super {p0}, Lepson/scan/activity/ScanBaseView;->onResume()V

    const/4 v0, 0x0

    .line 412
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 413
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->mActivityPaused:Z

    return-void
.end method

.method public onScanAreaSizeChange()V
    .locals 1

    const/4 v0, 0x1

    .line 353
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->mScanAreaBackgroundViewSizeChanged:Z

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 373
    iget-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isNeedDeleteTempFile:Z

    if-eqz v0, :cond_0

    .line 374
    invoke-static {p0}, Lepson/scan/activity/ScanActivity;->removeTempContents(Landroid/content/Context;)V

    .line 376
    :cond_0
    invoke-super {p0}, Lepson/scan/activity/ScanBaseView;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const/4 v0, 0x1

    .line 488
    iput-boolean v0, p0, Lepson/scan/activity/ScanActivity;->isStopScan:Z

    .line 490
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->escan:Lepson/scan/lib/escanLib;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/scan/lib/escanLib;->setJobDone(Z)V

    .line 492
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->doscan:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->scanningProgressHandler:Landroid/os/Handler;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 495
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 496
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->errorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 499
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->llScanningProgress:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 500
    iget-object v0, p0, Lepson/scan/activity/ScanActivity;->llScanningProgress:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 503
    :cond_2
    invoke-super {p0}, Lepson/scan/activity/ScanBaseView;->onStop()V

    return-void
.end method

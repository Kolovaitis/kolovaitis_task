.class public Lepson/scan/activity/I2DoubleSideScanSettingActivity;
.super Lepson/print/ActivityIACommon;
.source "I2DoubleSideScanSettingActivity.java"


# static fields
.field private static final DOUBLE_SIDE_VALUE_LIST:[Ljava/lang/Boolean;

.field private static final PARAM_KEY_DOUBLE_SIDE:Ljava/lang/String; = "double-side"

.field private static final PARAM_KEY_TURN_DIRECTION:Ljava/lang/String; = "turn-direction"


# instance fields
.field private mDoubleSideAdapter:Lepson/scan/activity/ModeListAdapter;

.field private mDoubleSideListView:Landroid/widget/ListView;

.field private mTurnDirectionAdapter:Lepson/scan/activity/ModeListAdapter;

.field private mTurnDirectionListView:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    .line 24
    new-array v0, v0, [Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->DOUBLE_SIDE_VALUE_LIST:[Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/I2DoubleSideScanSettingActivity;I)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->changeDoubleSide(I)V

    return-void
.end method

.method static synthetic access$100(Lepson/scan/activity/I2DoubleSideScanSettingActivity;I)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->changeTurnDirection(I)V

    return-void
.end method

.method private changeDoubleSide(I)V
    .locals 1

    .line 101
    iget-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v0, p1}, Lepson/scan/activity/ModeListAdapter;->setPosition(I)V

    .line 102
    invoke-direct {p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->updateTurnDirectionVisibilityFromDoubleSide()V

    return-void
.end method

.method private changeTurnDirection(I)V
    .locals 1

    .line 97
    iget-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v0, p1}, Lepson/scan/activity/ModeListAdapter;->setPosition(I)V

    return-void
.end method

.method private getDoubleSideAdapter()Lepson/scan/activity/ModeListAdapter;
    .locals 3

    .line 114
    sget-object v0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->DOUBLE_SIDE_VALUE_LIST:[Ljava/lang/Boolean;

    invoke-static {p0, v0}, Lepson/scan/activity/DisplayIdConverter;->getYesNoStringIdArray(Landroid/content/Context;[Ljava/lang/Boolean;)[I

    move-result-object v0

    .line 115
    new-instance v1, Lepson/scan/activity/ModeListAdapter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lepson/scan/activity/ModeListAdapter;-><init>(Landroid/content/Context;[II)V

    return-object v1
.end method

.method public static getDoubleSideFromReturnIntent(Landroid/content/Intent;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v1, "double-side"

    .line 154
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private getDoubleSidePosition(Z)I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 86
    :goto_0
    sget-object v2, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->DOUBLE_SIDE_VALUE_LIST:[Ljava/lang/Boolean;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 87
    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, p1, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static getStartIntent(Landroid/content/Context;ZI)Landroid/content/Intent;
    .locals 2

    .line 147
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/I2DoubleSideScanSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "double-side"

    .line 148
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "turn-direction"

    .line 149
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private getTurnDirectionAdapter()Lepson/scan/activity/ModeListAdapter;
    .locals 4

    const/4 v0, 0x2

    .line 120
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 121
    new-instance v1, Lepson/scan/activity/ModeListAdapter;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lepson/scan/activity/ModeListAdapter;-><init>(Landroid/content/Context;[I[II)V

    return-object v1

    :array_0
    .array-data 4
        0x7f070138
        0x7f07013c
    .end array-data
.end method

.method public static getTurnDirectionFromReturnIntent(Landroid/content/Intent;)I
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "turn-direction"

    .line 158
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0
.end method

.method private updateTurnDirectionVisibilityFromDoubleSide()V
    .locals 3

    .line 109
    iget-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v0}, Lepson/scan/activity/ModeListAdapter;->getPosition()I

    move-result v0

    .line 110
    iget-object v1, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionListView:Landroid/widget/ListView;

    sget-object v2, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->DOUBLE_SIDE_VALUE_LIST:[Ljava/lang/Boolean;

    aget-object v0, v2, v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    return-void
.end method

.method private updateUi(ZI)V
    .locals 1

    .line 79
    iget-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v0, p2}, Lepson/scan/activity/ModeListAdapter;->setPosition(I)V

    .line 80
    iget-object p2, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-direct {p0, p1}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->getDoubleSidePosition(Z)I

    move-result p1

    invoke-virtual {p2, p1}, Lepson/scan/activity/ModeListAdapter;->setPosition(I)V

    .line 82
    invoke-direct {p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->updateTurnDirectionVisibilityFromDoubleSide()V

    return-void
.end method

.method private updateUiFromBundle(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "double-side"

    .line 68
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "turn-direction"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-direct {p0, v0, p1}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->updateUi(ZI)V

    return-void
.end method

.method private updateUiFromIntent(Landroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "double-side"

    .line 72
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    const-string v2, "turn-direction"

    .line 73
    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 75
    :goto_1
    invoke-direct {p0, v1, v0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->updateUi(ZI)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 4

    .line 128
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "double-side"

    .line 129
    sget-object v2, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->DOUBLE_SIDE_VALUE_LIST:[Ljava/lang/Boolean;

    iget-object v3, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v3}, Lepson/scan/activity/ModeListAdapter;->getPosition()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "turn-direction"

    .line 130
    iget-object v2, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v2}, Lepson/scan/activity/ModeListAdapter;->getPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, -0x1

    .line 131
    invoke-virtual {p0, v1, v0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 132
    invoke-virtual {p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 34
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a002b

    .line 35
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->setContentView(I)V

    const v0, 0x7f0e0504

    .line 37
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->setTitle(I)V

    .line 38
    invoke-virtual {p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->setActionBar(Ljava/lang/String;Z)V

    const v0, 0x7f08037f

    .line 40
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideListView:Landroid/widget/ListView;

    .line 41
    invoke-direct {p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->getDoubleSideAdapter()Lepson/scan/activity/ModeListAdapter;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideAdapter:Lepson/scan/activity/ModeListAdapter;

    .line 42
    iget-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideListView:Landroid/widget/ListView;

    iget-object v1, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 43
    iget-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideListView:Landroid/widget/ListView;

    new-instance v1, Lepson/scan/activity/I2DoubleSideScanSettingActivity$1;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity$1;-><init>(Lepson/scan/activity/I2DoubleSideScanSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f080201

    .line 50
    invoke-virtual {p0, v0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionListView:Landroid/widget/ListView;

    .line 51
    invoke-direct {p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->getTurnDirectionAdapter()Lepson/scan/activity/ModeListAdapter;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionAdapter:Lepson/scan/activity/ModeListAdapter;

    .line 52
    iget-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionListView:Landroid/widget/ListView;

    iget-object v1, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 53
    iget-object v0, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionListView:Landroid/widget/ListView;

    new-instance v1, Lepson/scan/activity/I2DoubleSideScanSettingActivity$2;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity$2;-><init>(Lepson/scan/activity/I2DoubleSideScanSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    if-eqz p1, :cond_0

    .line 61
    invoke-direct {p0, p1}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->updateUiFromBundle(Landroid/os/Bundle;)V

    goto :goto_0

    .line 63
    :cond_0
    invoke-virtual {p0}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->updateUiFromIntent(Landroid/content/Intent;)V

    :goto_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .line 137
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "double-side"

    .line 138
    sget-object v1, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->DOUBLE_SIDE_VALUE_LIST:[Ljava/lang/Boolean;

    iget-object v2, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mDoubleSideAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v2}, Lepson/scan/activity/ModeListAdapter;->getPosition()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "turn-direction"

    .line 139
    iget-object v1, p0, Lepson/scan/activity/I2DoubleSideScanSettingActivity;->mTurnDirectionAdapter:Lepson/scan/activity/ModeListAdapter;

    invoke-virtual {v1}, Lepson/scan/activity/ModeListAdapter;->getPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

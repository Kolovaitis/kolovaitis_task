.class public Lepson/scan/activity/I2SettingDetailActivity;
.super Lepson/print/ActivityIACommon;
.source "I2SettingDetailActivity.java"


# static fields
.field private static final KEY_ADAPTOR:Ljava/lang/String; = "adaptor"

.field private static final KEY_TITLE:Ljava/lang/String; = "title"

.field private static final RESULT_KEY_SELECTED:Ljava/lang/String; = "selected-position"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/I2SettingDetailActivity;I)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lepson/scan/activity/I2SettingDetailActivity;->localItemSelected(I)V

    return-void
.end method

.method public static getResultPosition(Landroid/content/Intent;)I
    .locals 2

    const/4 v0, -0x1

    if-nez p0, :cond_0

    return v0

    :cond_0
    const-string v1, "selected-position"

    .line 90
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static getStartIntent(Landroid/content/Context;Lepson/scan/activity/LocalListAdapterBuilder;I)Landroid/content/Intent;
    .locals 2

    .line 73
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/I2SettingDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "adaptor"

    .line 74
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p0, "title"

    .line 75
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private localItemSelected(I)V
    .locals 2

    .line 61
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "selected-position"

    .line 62
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 63
    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/I2SettingDetailActivity;->setResult(ILandroid/content/Intent;)V

    .line 64
    invoke-virtual {p0}, Lepson/scan/activity/I2SettingDetailActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 27
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a002e

    .line 28
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2SettingDetailActivity;->setContentView(I)V

    .line 29
    invoke-virtual {p0}, Lepson/scan/activity/I2SettingDetailActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/I2SettingDetailActivity;->setActionBar(Ljava/lang/String;Z)V

    const p1, 0x7f0801e0

    .line 31
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2SettingDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    .line 33
    invoke-virtual {p0}, Lepson/scan/activity/I2SettingDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "adaptor"

    .line 34
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lepson/scan/activity/LocalListAdapterBuilder;

    if-eqz v1, :cond_2

    .line 39
    invoke-interface {v1, p0}, Lepson/scan/activity/LocalListAdapterBuilder;->build(Landroid/content/Context;)Landroid/widget/ListAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 44
    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 46
    new-instance v1, Lepson/scan/activity/I2SettingDetailActivity$1;

    invoke-direct {v1, p0}, Lepson/scan/activity/I2SettingDetailActivity$1;-><init>(Lepson/scan/activity/I2SettingDetailActivity;)V

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const-string p1, "title"

    const/4 v1, 0x0

    .line 53
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    if-eqz p1, :cond_0

    .line 55
    invoke-virtual {p0, p1}, Lepson/scan/activity/I2SettingDetailActivity;->setTitle(I)V

    :cond_0
    return-void

    .line 42
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    .line 36
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "intent extra \'adaptor\' == null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

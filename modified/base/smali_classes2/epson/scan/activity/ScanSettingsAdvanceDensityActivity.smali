.class public Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;
.super Lepson/print/ActivityIACommon;
.source "ScanSettingsAdvanceDensityActivity.java"


# static fields
.field private static final DEFAULT_DENSITY:I = 0x80

.field private static final PARAM_KEY_DENSITY:Ljava/lang/String; = "SCAN_SETTINGS_LAST_DENSITY"

.field private static final PARAM_KEY_DENSITY_STATUS:Ljava/lang/String; = "SCAN_SETTINGS_LAST_DENSITY_STATUS"


# instance fields
.field private densityStatus:Z

.field private densityValue:I

.field private mSeekDensityChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mToogleDensityListener:Landroid/view/View$OnClickListener;

.field private seekDensity:Landroid/widget/SeekBar;

.field private toggleDensity:Landroid/widget/Switch;

.field private tvDensityValue:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 19
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 64
    new-instance v0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$1;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$1;-><init>(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->mSeekDensityChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 83
    new-instance v0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;-><init>(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->mToogleDensityListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)I
    .locals 0

    .line 19
    iget p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityValue:I

    return p0
.end method

.method static synthetic access$002(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;I)I
    .locals 0

    .line 19
    iput p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityValue:I

    return p1
.end method

.method static synthetic access$100(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Landroid/widget/TextView;
    .locals 0

    .line 19
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->tvDensityValue:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$200(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Z
    .locals 0

    .line 19
    iget-boolean p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityStatus:Z

    return p0
.end method

.method static synthetic access$202(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;Z)Z
    .locals 0

    .line 19
    iput-boolean p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityStatus:Z

    return p1
.end method

.method static synthetic access$300(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Landroid/widget/Switch;
    .locals 0

    .line 19
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->toggleDensity:Landroid/widget/Switch;

    return-object p0
.end method

.method static synthetic access$400(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Landroid/widget/SeekBar;
    .locals 0

    .line 19
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->seekDensity:Landroid/widget/SeekBar;

    return-object p0
.end method

.method public static getChangeAble(Landroid/content/Intent;)Z
    .locals 2
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    const-string v1, "SCAN_SETTINGS_LAST_DENSITY_STATUS"

    .line 142
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static getDensityFromReturnIntent(Landroid/content/Intent;)I
    .locals 2
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/16 v0, 0x80

    if-nez p0, :cond_0

    return v0

    :cond_0
    const-string v1, "SCAN_SETTINGS_LAST_DENSITY"

    .line 134
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static getStartIntent(Landroid/content/Context;IZ)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 118
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "SCAN_SETTINGS_LAST_DENSITY"

    .line 119
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "SCAN_SETTINGS_LAST_DENSITY_STATUS"

    .line 120
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .line 101
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SCAN_SETTINGS_LAST_DENSITY"

    .line 102
    iget v2, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityValue:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "SCAN_SETTINGS_LAST_DENSITY_STATUS"

    .line 103
    iget-boolean v2, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityStatus:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, -0x1

    .line 105
    invoke-virtual {p0, v1, v0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->setResult(ILandroid/content/Intent;)V

    .line 106
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->finish()V

    .line 107
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 33
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00b4

    .line 34
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->setContentView(I)V

    const/4 p1, 0x1

    const v0, 0x7f0e02e9

    .line 36
    invoke-virtual {p0, v0, p1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->setActionBar(IZ)V

    .line 38
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SCAN_SETTINGS_DETAIL_TITLE"

    .line 39
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    const-string v1, "SCAN_SETTINGS_LAST_DENSITY"

    .line 40
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityValue:I

    const-string v1, "SCAN_SETTINGS_LAST_DENSITY_STATUS"

    .line 41
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityStatus:Z

    const v0, 0x7f0802ee

    .line 43
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->seekDensity:Landroid/widget/SeekBar;

    .line 44
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->seekDensity:Landroid/widget/SeekBar;

    iget v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityValue:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 45
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->seekDensity:Landroid/widget/SeekBar;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->mSeekDensityChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v0, 0x7f080352

    .line 47
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->tvDensityValue:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->tvDensityValue:Landroid/widget/TextView;

    const-string v1, "[%s]"

    new-array v2, p1, [Ljava/lang/Object;

    iget v3, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityValue:I

    rsub-int v3, v3, 0xff

    .line 50
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 49
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->seekDensity:Landroid/widget/SeekBar;

    iget v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityValue:I

    rsub-int v1, v1, 0xff

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    const v0, 0x7f08034a

    .line 54
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->toggleDensity:Landroid/widget/Switch;

    .line 55
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->toggleDensity:Landroid/widget/Switch;

    iget-boolean v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->densityStatus:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 56
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->toggleDensity:Landroid/widget/Switch;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->mToogleDensityListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->toggleDensity:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->seekDensity:Landroid/widget/SeekBar;

    invoke-virtual {p1, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    goto :goto_0

    .line 60
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->seekDensity:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_0
    return-void
.end method

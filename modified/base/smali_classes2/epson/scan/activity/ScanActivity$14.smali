.class Lepson/scan/activity/ScanActivity$14;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 1501
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 8

    .line 1503
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->isFinishing()Z

    move-result v0

    const/4 v1, -0x3

    const/16 v2, -0x64

    const/16 v3, 0x14

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eqz v0, :cond_1

    .line 1504
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v2, :cond_0

    if-eq v0, v1, :cond_0

    if-eq v0, v4, :cond_0

    if-eq v0, v3, :cond_0

    packed-switch v0, :pswitch_data_0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :cond_0
    :pswitch_0
    return v5

    .line 1518
    :cond_1
    :goto_0
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x0

    if-eq p1, v2, :cond_1f

    const/4 v2, -0x1

    if-eq p1, v4, :cond_1c

    if-eq p1, v3, :cond_18

    const-wide/16 v6, 0x3e8

    packed-switch p1, :pswitch_data_2

    packed-switch p1, :pswitch_data_3

    goto/16 :goto_6

    .line 1618
    :pswitch_1
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 1621
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-ge p1, v5, :cond_2

    return v0

    .line 1623
    :cond_2
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string v1, "end page, waiting..."

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1625
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$3900(Lepson/scan/activity/ScanActivity;)I

    move-result p1

    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_3

    .line 1626
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v2}, Lepson/scan/activity/ScanActivity;->access$3900(Lepson/scan/activity/ScanActivity;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p1, v1}, Lepson/scan/activity/ScanActivity;->access$4400(Lepson/scan/activity/ScanActivity;Ljava/lang/String;)V

    .line 1629
    :cond_3
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string v1, "done add to list"

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1604
    :pswitch_2
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 1605
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-ge p1, v5, :cond_4

    return v0

    .line 1606
    :cond_4
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->isJobDone()Z

    move-result p1

    if-eqz p1, :cond_5

    return v5

    .line 1610
    :cond_5
    :try_start_0
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4000(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-nez p1, :cond_21

    .line 1611
    new-instance p1, Lepson/scan/activity/ScanActivity$BitmapTask;

    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p1, v1}, Lepson/scan/activity/ScanActivity$BitmapTask;-><init>(Lepson/scan/activity/ScanActivity;)V

    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v2}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v2

    invoke-virtual {v2}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    iget v3, v3, Lepson/scan/activity/ScanActivity;->totalScanned:I

    sub-int/2addr v3, v5

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-virtual {p1, v1}, Lepson/scan/activity/ScanActivity$BitmapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_6

    :catch_0
    move-exception p1

    .line 1614
    invoke-virtual {p1}, Ljava/util/concurrent/RejectedExecutionException;->printStackTrace()V

    goto/16 :goto_6

    .line 1520
    :pswitch_3
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object p1

    const-string v1, "[EPSON SCAN] START SCAN"

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$2902(Lepson/scan/activity/ScanActivity;Z)Z

    .line 1523
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$3802(Lepson/scan/activity/ScanActivity;Z)Z

    .line 1525
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result p1

    if-nez p1, :cond_6

    .line 1527
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$3902(Lepson/scan/activity/ScanActivity;I)I

    .line 1529
    :cond_6
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$4002(Lepson/scan/activity/ScanActivity;Z)Z

    .line 1534
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    .line 1535
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    const-string v2, "display;"

    .line 1536
    iget v3, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "display;"

    .line 1537
    invoke-virtual {p1}, Landroid/view/Display;->getRotation()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1538
    invoke-virtual {p1}, Landroid/view/Display;->getRotation()I

    move-result p1

    const/16 v2, 0x9

    const/16 v3, 0x8

    packed-switch p1, :pswitch_data_4

    goto :goto_1

    .line 1570
    :pswitch_4
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le p1, v3, :cond_8

    .line 1572
    iget p1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne p1, v5, :cond_7

    .line 1573
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v5}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1575
    :cond_7
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v3}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1580
    :cond_8
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v5}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1556
    :pswitch_5
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le p1, v3, :cond_a

    .line 1558
    iget p1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne p1, v4, :cond_9

    .line 1559
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v3}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1561
    :cond_9
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v2}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1565
    :cond_a
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v5}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1548
    :pswitch_6
    iget p1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne p1, v5, :cond_b

    .line 1549
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v2}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1551
    :cond_b
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v0}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1540
    :pswitch_7
    iget p1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne p1, v4, :cond_c

    .line 1541
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v0}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1543
    :cond_c
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1, v5}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    .line 1586
    :goto_1
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4100(Lepson/scan/activity/ScanActivity;)V

    goto/16 :goto_6

    .line 1722
    :pswitch_8
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4500(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object p1

    if-eqz p1, :cond_d

    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4500(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_d

    .line 1723
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4500(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1726
    :cond_d
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v5}, Lepson/scan/activity/ScanActivity;->access$2902(Lepson/scan/activity/ScanActivity;Z)Z

    .line 1727
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1730
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2000(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-nez p1, :cond_e

    .line 1731
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object p1

    invoke-virtual {p1, v2, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_6

    .line 1734
    :cond_e
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4900(Lepson/scan/activity/ScanActivity;)V

    .line 1737
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$3800(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-eqz p1, :cond_f

    goto/16 :goto_6

    .line 1741
    :cond_f
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$5000(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object p1

    if-eqz p1, :cond_17

    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$5000(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result p1

    if-nez p1, :cond_17

    .line 1742
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->getStatus()[I

    move-result-object p1

    .line 1748
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v1

    const/16 v2, -0x44c

    if-ne v1, v2, :cond_10

    .line 1749
    aget v1, p1, v5

    const/4 v2, 0x5

    if-ne v1, v2, :cond_11

    aget v1, p1, v4

    if-eqz v1, :cond_11

    .line 1750
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    aget p1, p1, v4

    invoke-static {v1, p1}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    goto :goto_2

    .line 1755
    :cond_10
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v1

    const v3, 0x7f0e04d6

    if-eq v1, v3, :cond_11

    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    .line 1756
    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v1

    const/16 v3, -0x514

    if-eq v1, v3, :cond_11

    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    .line 1757
    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v1

    if-eq v1, v2, :cond_11

    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    .line 1758
    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v1

    if-eq v1, v5, :cond_11

    .line 1760
    aget v1, p1, v4

    const/16 v2, 0x64

    if-eq v1, v2, :cond_11

    aget v1, p1, v4

    if-eqz v1, :cond_11

    .line 1762
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    aget p1, p1, v4

    invoke-static {v1, p1}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1766
    :cond_11
    :goto_2
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v2}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1768
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v1

    invoke-static {p1, v1}, Lepson/scan/activity/ScanActivity;->access$5100(Lepson/scan/activity/ScanActivity;I)V

    .line 1770
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$5000(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    .line 1773
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    iget-object p1, p1, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    const-string v1, "scanner"

    iget-object v2, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v2}, Lepson/scan/activity/ScanActivity;->access$2700(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_5

    .line 1672
    :pswitch_9
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v5}, Lepson/scan/activity/ScanActivity;->access$2902(Lepson/scan/activity/ScanActivity;Z)Z

    .line 1674
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4200(Lepson/scan/activity/ScanActivity;)Ljava/lang/Thread;

    move-result-object p1

    if-eqz p1, :cond_12

    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4200(Lepson/scan/activity/ScanActivity;)Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-eqz p1, :cond_12

    .line 1675
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4200(Lepson/scan/activity/ScanActivity;)Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 1679
    :cond_12
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->isScanning()Z

    move-result p1

    if-nez p1, :cond_13

    .line 1680
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string v1, "scan, cancel find"

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1682
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperCancelFindScanner()I

    .line 1684
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$3500(Lepson/scan/activity/ScanActivity;)V

    .line 1688
    :cond_13
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->isScanning()Z

    move-result p1

    const/4 v1, -0x2

    if-eqz p1, :cond_15

    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->isJobDone()Z

    move-result p1

    if-nez p1, :cond_15

    .line 1689
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->isCanselOK()Z

    move-result p1

    if-ne p1, v5, :cond_14

    .line 1690
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string v2, "isCanselOK OK"

    invoke-static {p1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1691
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string v2, "scan, cancel scan job"

    invoke-static {p1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1692
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->cancelScanJob()I

    goto :goto_4

    .line 1694
    :cond_14
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string v2, "isCanselOK NO"

    invoke-static {p1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697
    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception p1

    .line 1699
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1702
    :goto_3
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1704
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$3802(Lepson/scan/activity/ScanActivity;Z)Z

    goto/16 :goto_6

    .line 1709
    :cond_15
    :goto_4
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$3802(Lepson/scan/activity/ScanActivity;Z)Z

    .line 1711
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2000(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-nez p1, :cond_16

    .line 1712
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object p1

    invoke-virtual {p1, v1, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_6

    .line 1715
    :cond_16
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4900(Lepson/scan/activity/ScanActivity;)V

    .line 1718
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    iget-object p1, p1, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2700(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1778
    :cond_17
    :goto_5
    :pswitch_a
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$3500(Lepson/scan/activity/ScanActivity;)V

    goto/16 :goto_6

    .line 1595
    :cond_18
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4200(Lepson/scan/activity/ScanActivity;)Ljava/lang/Thread;

    move-result-object p1

    if-eqz p1, :cond_19

    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4200(Lepson/scan/activity/ScanActivity;)Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-nez p1, :cond_1a

    :cond_19
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->isValidArea()Z

    move-result p1

    if-nez p1, :cond_1b

    .line 1596
    :cond_1a
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object p1

    const-wide/16 v1, 0x12c

    invoke-virtual {p1, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_6

    .line 1600
    :cond_1b
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$4300(Lepson/scan/activity/ScanActivity;)V

    goto/16 :goto_6

    .line 1632
    :cond_1c
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string v1, "MESSAGE SCAN FINISHED!!!"

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1635
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 1637
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    .line 1638
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1, p1}, Lepson/scan/activity/ScanCount;->saveScanCount(Landroid/content/Context;I)V

    .line 1641
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v1

    invoke-virtual {v1, v5}, Lepson/scan/lib/escanLib;->setJobDone(Z)V

    .line 1643
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v1

    .line 1644
    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v3}, Lepson/scan/activity/ScanActivity;->access$4500(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object v3

    if-eqz v3, :cond_1d

    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v3}, Lepson/scan/activity/ScanActivity;->access$4500(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 1645
    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v3}, Lepson/scan/activity/ScanActivity;->access$4500(Lepson/scan/activity/ScanActivity;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 1647
    :cond_1d
    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v3}, Lepson/scan/activity/ScanActivity;->access$4600(Lepson/scan/activity/ScanActivity;)V

    .line 1649
    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v3}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Lepson/scan/activity/ScanContinueParam;->setScannedFileCount(I)V

    .line 1651
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isReachMaximumScan()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 1652
    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    iget-object v3, v3, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    const-string v4, "epson.scanner.SelectedScanner"

    const-string v6, "SCAN_REFS_SETTINGS_SOURCE"

    invoke-static {v3, v4, v6}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-ne v3, v5, :cond_1e

    .line 1653
    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v3}, Lepson/scan/activity/ScanContinueParam;->showScanErrorLimit(Landroid/content/Context;)V

    .line 1660
    :cond_1e
    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v3, v2}, Lepson/scan/activity/ScanActivity;->setRequestedOrientation(I)V

    .line 1662
    iget-object v2, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    iget-object v2, v2, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    .line 1663
    iget-object v3, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v3}, Lepson/scan/activity/ScanActivity;->access$2700(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v3

    .line 1665
    invoke-static {v2, v3}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 1667
    iget-object v2, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v2}, Lepson/scan/activity/ScanActivity;->access$4700(Lepson/scan/activity/ScanActivity;)V

    .line 1668
    iget-object v2, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v2, p1, v1}, Lepson/scan/activity/ScanActivity;->access$4800(Lepson/scan/activity/ScanActivity;II)V

    goto :goto_6

    .line 1786
    :cond_1f
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$100(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    move-result-object p1

    const/4 v1, 0x0

    iput-object v1, p1, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->redrawCBHandler:Landroid/os/Handler;

    .line 1787
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$1700(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-ne p1, v5, :cond_20

    .line 1788
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$1702(Lepson/scan/activity/ScanActivity;Z)Z

    .line 1789
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getRectScanArea()Landroid/graphics/RectF;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/scan/activity/ScanActivity;->access$5202(Lepson/scan/activity/ScanActivity;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 1790
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$5200(Lepson/scan/activity/ScanActivity;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p1, v1}, Lepson/scan/activity/ScanActivity;->setBmRectF(Landroid/graphics/RectF;)V

    .line 1792
    :cond_20
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$14;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$100(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    :cond_21
    :goto_6
    return v0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x3
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

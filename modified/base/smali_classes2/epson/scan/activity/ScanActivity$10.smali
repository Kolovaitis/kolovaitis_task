.class Lepson/scan/activity/ScanActivity$10;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 1159
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$10;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1161
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$10;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {p1}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isStopScan = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/scan/activity/ScanActivity$10;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$2900(Lepson/scan/activity/ScanActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$10;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->isJobDone()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 1163
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$10;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2900(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1164
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$10;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$3500(Lepson/scan/activity/ScanActivity;)V

    .line 1165
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$10;->this$0:Lepson/scan/activity/ScanActivity;

    const v0, 0x7f0e04de

    invoke-virtual {p1, v0}, Lepson/scan/activity/ScanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$3600(Lepson/scan/activity/ScanActivity;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

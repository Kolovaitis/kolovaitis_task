.class Lepson/scan/activity/ScanActivity$11;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 1353
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$11;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1357
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$11;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-lez p1, :cond_1

    .line 1358
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$11;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2000(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 1362
    :cond_0
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lepson/scan/activity/ScanActivity$11;->this$0:Lepson/scan/activity/ScanActivity;

    const-class v1, Lepson/scan/activity/MailActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "SAVING_FILE_PATH"

    .line 1364
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$11;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1366
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$11;->this$0:Lepson/scan/activity/ScanActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lepson/scan/activity/ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_1
    return-void
.end method

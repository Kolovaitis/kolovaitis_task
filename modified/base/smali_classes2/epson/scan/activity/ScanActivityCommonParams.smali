.class Lepson/scan/activity/ScanActivityCommonParams;
.super Ljava/lang/Object;
.source "ScanActivityCommonParams.java"


# static fields
.field private static final ESC_I1_SCAN_ACTIVITY:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static final ESC_I2_SCAN_ACTIVITY:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static final PARAM_KEY_EXTERNAL_INTENT_DATA:Ljava/lang/String; = "external-intent-data"

.field private static final PARAM_KEY_START_SCAN_AT_ACTIVITY_CHANGE:Ljava/lang/String; = "start-scan-at-activity-change"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    const-class v0, Lepson/scan/activity/ScanActivity;

    sput-object v0, Lepson/scan/activity/ScanActivityCommonParams;->ESC_I1_SCAN_ACTIVITY:Ljava/lang/Class;

    .line 22
    const-class v0, Lepson/scan/activity/I2ScanActivity;

    sput-object v0, Lepson/scan/activity/ScanActivityCommonParams;->ESC_I2_SCAN_ACTIVITY:Ljava/lang/Class;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getCommonLog(Landroid/content/Intent;)Lcom/epson/iprint/prtlogger/CommonLog;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const-string v0, "scan-log"

    .line 72
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p0

    check-cast p0, Lcom/epson/iprint/prtlogger/CommonLog;

    :goto_0
    return-object p0
.end method

.method static getEscIVersionChangeIntent(Landroid/content/Context;IZLcom/epson/iprint/shared/SharedParamScan;Lcom/epson/iprint/prtlogger/CommonLog;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/shared/SharedParamScan;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/epson/iprint/prtlogger/CommonLog;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 34
    new-instance v0, Landroid/content/Intent;

    invoke-static {p1}, Lepson/scan/activity/ScanActivityCommonParams;->getScanActivityClass(I)Ljava/lang/Class;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "start-scan-at-activity-change"

    .line 35
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "external-intent-data"

    .line 36
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p0, "scan-log"

    .line 37
    invoke-virtual {v0, p0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method static getExternalIntentData(Landroid/content/Intent;)Lcom/epson/iprint/shared/SharedParamScan;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const-string v0, "external-intent-data"

    .line 67
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p0

    check-cast p0, Lcom/epson/iprint/shared/SharedParamScan;

    :goto_0
    return-object p0
.end method

.method private static getScanActivityClass(I)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 48
    sget-object p0, Lepson/scan/activity/ScanActivityCommonParams;->ESC_I1_SCAN_ACTIVITY:Ljava/lang/Class;

    return-object p0

    .line 45
    :cond_0
    sget-object p0, Lepson/scan/activity/ScanActivityCommonParams;->ESC_I2_SCAN_ACTIVITY:Ljava/lang/Class;

    return-object p0
.end method

.method static startScanAtActivityChange(Landroid/content/Intent;)Z
    .locals 2
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v1, "start-scan-at-activity-change"

    .line 58
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.class Lepson/scan/activity/ScanActivity$4;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 537
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 22

    move-object/from16 v0, p0

    .line 544
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$400(Lepson/scan/activity/ScanActivity;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_0

    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$500(Lepson/scan/activity/ScanActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1, v2}, Lepson/scan/activity/ScanActivity;->access$502(Lepson/scan/activity/ScanActivity;Z)Z

    .line 548
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1, v3}, Lepson/scan/activity/ScanActivity;->access$602(Lepson/scan/activity/ScanActivity;Z)Z

    .line 549
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1, v3}, Lepson/scan/activity/ScanActivity;->access$702(Lepson/scan/activity/ScanActivity;Z)Z

    .line 550
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$800(Lepson/scan/activity/ScanActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->invalidate()V

    .line 551
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$900(Lepson/scan/activity/ScanActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->invalidate()V

    .line 552
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$100(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->invalidate()V

    .line 553
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$1000(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaBackground;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/activity/ScanBaseView$ScanAreaBackground;->invalidate()V

    .line 554
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$1100(Lepson/scan/activity/ScanActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->invalidate()V

    .line 558
    :cond_0
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$600(Lepson/scan/activity/ScanActivity;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 560
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    iget-boolean v1, v1, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    if-eqz v1, :cond_1

    .line 561
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "epson.scanner.supported.Options"

    const-string v6, "SCAN_REFS_MAX_WIDTH"

    invoke-static {v4, v5, v6}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Lepson/scan/activity/ScanActivity;->setMaxwidth(I)V

    .line 562
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "epson.scanner.supported.Options"

    const-string v6, "SCAN_REFS_MAX_HEIGHT"

    invoke-static {v4, v5, v6}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Lepson/scan/activity/ScanActivity;->setMaxheight(I)V

    .line 565
    :cond_1
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getMaxwidth()I

    move-result v1

    if-nez v1, :cond_2

    .line 566
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    const/16 v4, 0x4fb

    invoke-virtual {v1, v4}, Lepson/scan/activity/ScanActivity;->setMaxwidth(I)V

    .line 568
    :cond_2
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getMaxheight()I

    move-result v1

    if-nez v1, :cond_3

    .line 569
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    const/16 v4, 0x6db

    invoke-virtual {v1, v4}, Lepson/scan/activity/ScanActivity;->setMaxheight(I)V

    :cond_3
    const-string v1, ""

    .line 573
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v4}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "epson.scanner.SelectedScanner"

    const-string v6, "SCAN_REFS_SCANNER_MODEL"

    invoke-static {v4, v5, v6}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 574
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SETTINGS_RESOLUTION"

    invoke-static {v1, v4, v5}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_4
    const/16 v1, 0x96

    .line 580
    :goto_0
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4}, Lepson/scan/activity/ScanActivity;->access$1200(Lepson/scan/activity/ScanActivity;)I

    move-result v4

    if-ne v1, v4, :cond_5

    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    iget-boolean v4, v4, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    if-nez v4, :cond_5

    .line 581
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4, v3}, Lepson/scan/activity/ScanActivity;->access$1302(Lepson/scan/activity/ScanActivity;Z)Z

    .line 583
    :cond_5
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4, v1}, Lepson/scan/activity/ScanActivity;->access$1202(Lepson/scan/activity/ScanActivity;I)I

    .line 586
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$900(Lepson/scan/activity/ScanActivity;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v4

    invoke-static {v1, v4}, Lepson/scan/activity/ScanActivity;->access$1402(Lepson/scan/activity/ScanActivity;I)I

    .line 587
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$900(Lepson/scan/activity/ScanActivity;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v4

    invoke-static {v1, v4}, Lepson/scan/activity/ScanActivity;->access$1502(Lepson/scan/activity/ScanActivity;I)I

    .line 591
    :cond_6
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$1600(Lepson/scan/activity/ScanActivity;)V

    .line 595
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v1

    .line 596
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v4}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v4

    .line 597
    iget v5, v4, Landroid/graphics/Point;->x:I

    iget v6, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v5, v6

    .line 598
    iget v6, v4, Landroid/graphics/Point;->y:I

    iget v7, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v6, v7

    .line 601
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    new-instance v8, Landroid/graphics/Point;

    invoke-static {v7}, Lepson/scan/activity/ScanActivity;->access$1500(Lepson/scan/activity/ScanActivity;)I

    move-result v9

    int-to-double v9, v9

    iget-object v11, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v11}, Lepson/scan/activity/ScanActivity;->getMaxScanWidthOnScreen()D

    move-result-wide v11

    sub-double/2addr v9, v11

    double-to-int v9, v9

    div-int/lit8 v9, v9, 0x2

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    iget v10, v10, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    invoke-direct {v8, v9, v10}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v7, v8}, Lepson/scan/activity/ScanActivity;->setmBaseTop(Landroid/graphics/Point;)V

    .line 603
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    new-instance v8, Landroid/graphics/Point;

    invoke-static {v7}, Lepson/scan/activity/ScanActivity;->access$1500(Lepson/scan/activity/ScanActivity;)I

    move-result v9

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->x:I

    sub-int/2addr v9, v10

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v10}, Lepson/scan/activity/ScanActivity;->access$1400(Lepson/scan/activity/ScanActivity;)I

    move-result v10

    iget-object v11, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    iget v11, v11, Lepson/scan/activity/ScanActivity;->MARGIN_TOP_BOT:I

    sub-int/2addr v10, v11

    invoke-direct {v8, v9, v10}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v7, v8}, Lepson/scan/activity/ScanActivity;->setmBaseBot(Landroid/graphics/Point;)V

    .line 605
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    iget-boolean v7, v7, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    if-nez v7, :cond_7

    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v7}, Lepson/scan/activity/ScanActivity;->access$600(Lepson/scan/activity/ScanActivity;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 606
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    new-instance v14, Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-double v10, v8

    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-double v12, v8

    move-object v8, v14

    move-object v9, v7

    invoke-direct/range {v8 .. v13}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {v7, v14}, Lepson/scan/activity/ScanActivity;->setPiBaseTop(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 607
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    new-instance v8, Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    int-to-double v9, v9

    iget-object v11, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v11}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Point;->y:I

    int-to-double v11, v11

    move-object v15, v8

    move-object/from16 v16, v7

    move-wide/from16 v17, v9

    move-wide/from16 v19, v11

    invoke-direct/range {v15 .. v20}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {v7, v8}, Lepson/scan/activity/ScanActivity;->setPiBaseBot(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 609
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    invoke-virtual {v7, v8}, Lepson/scan/activity/ScanActivity;->setmTop(Landroid/graphics/Point;)V

    .line 610
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v8

    invoke-virtual {v7, v8}, Lepson/scan/activity/ScanActivity;->setmBot(Landroid/graphics/Point;)V

    .line 613
    :cond_7
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v7}, Lepson/scan/activity/ScanActivity;->access$600(Lepson/scan/activity/ScanActivity;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 617
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v7}, Lepson/scan/activity/ScanActivity;->access$1300(Lepson/scan/activity/ScanActivity;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 619
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    new-instance v14, Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-double v10, v8

    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-double v12, v8

    move-object v8, v14

    move-object v9, v7

    invoke-direct/range {v8 .. v13}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {v7, v14}, Lepson/scan/activity/ScanActivity;->setPiBaseTop(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 620
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    new-instance v8, Lepson/scan/activity/ScanBaseView$PointInfo;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    int-to-double v9, v9

    iget-object v11, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v11}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Point;->y:I

    int-to-double v11, v11

    move-object v15, v8

    move-object/from16 v16, v7

    move-wide/from16 v17, v9

    move-wide/from16 v19, v11

    invoke-direct/range {v15 .. v20}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {v7, v8}, Lepson/scan/activity/ScanActivity;->setPiBaseBot(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 623
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    invoke-virtual {v7, v8}, Lepson/scan/activity/ScanActivity;->setmTop(Landroid/graphics/Point;)V

    .line 624
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v8

    invoke-virtual {v7, v8}, Lepson/scan/activity/ScanActivity;->setmBot(Landroid/graphics/Point;)V

    .line 626
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7, v3}, Lepson/scan/activity/ScanActivity;->setScreenStatus(I)V

    .line 628
    :cond_8
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v7}, Lepson/scan/activity/ScanActivity;->access$700(Lepson/scan/activity/ScanActivity;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 632
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v7, v2}, Lepson/scan/activity/ScanActivity;->access$702(Lepson/scan/activity/ScanActivity;Z)Z

    .line 634
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 635
    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    iget-object v9, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v9}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->x:I

    sub-int/2addr v9, v10

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmTop()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->x:I

    iget v11, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v10, v11

    mul-int v9, v9, v10

    int-to-float v9, v9

    int-to-float v5, v5

    div-float/2addr v9, v5

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Point;->x:I

    .line 636
    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    iget-object v9, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v9}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->y:I

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v10

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmTop()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget v11, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v10, v11

    mul-int v9, v9, v10

    int-to-float v9, v9

    int-to-float v6, v6

    div-float/2addr v9, v6

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Point;->y:I

    .line 637
    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    iget-object v9, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v9}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->x:I

    sub-int/2addr v9, v10

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmBot()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->x:I

    iget v11, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v10, v11

    mul-int v9, v9, v10

    int-to-float v9, v9

    div-float/2addr v9, v5

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v7, Landroid/graphics/Point;->x:I

    .line 638
    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    iget-object v9, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v9}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->y:I

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v10

    iget-object v10, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v10}, Lepson/scan/activity/ScanActivity;->getmBot()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget v11, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v10, v11

    mul-int v9, v9, v10

    int-to-float v9, v9

    div-float/2addr v9, v6

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v7, Landroid/graphics/Point;->y:I

    .line 640
    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    new-instance v15, Lepson/scan/activity/ScanBaseView$PointInfo;

    iget v9, v4, Landroid/graphics/Point;->x:I

    int-to-double v11, v9

    iget v9, v4, Landroid/graphics/Point;->y:I

    int-to-double v13, v9

    move-object v9, v15

    move-object v10, v8

    invoke-direct/range {v9 .. v14}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {v8, v15}, Lepson/scan/activity/ScanActivity;->setPiBaseTop(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 641
    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    new-instance v9, Lepson/scan/activity/ScanBaseView$PointInfo;

    iget v10, v7, Landroid/graphics/Point;->x:I

    int-to-double v10, v10

    iget v12, v7, Landroid/graphics/Point;->y:I

    int-to-double v12, v12

    move-object/from16 v16, v9

    move-object/from16 v17, v8

    move-wide/from16 v18, v10

    move-wide/from16 v20, v12

    invoke-direct/range {v16 .. v21}, Lepson/scan/activity/ScanBaseView$PointInfo;-><init>(Lepson/scan/activity/ScanBaseView;DD)V

    invoke-virtual {v8, v9}, Lepson/scan/activity/ScanActivity;->setPiBaseBot(Lepson/scan/activity/ScanBaseView$PointInfo;)V

    .line 643
    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8, v4}, Lepson/scan/activity/ScanActivity;->setmTop(Landroid/graphics/Point;)V

    .line 644
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v4, v7}, Lepson/scan/activity/ScanActivity;->setmBot(Landroid/graphics/Point;)V

    .line 646
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4}, Lepson/scan/activity/ScanActivity;->access$1700(Lepson/scan/activity/ScanActivity;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 648
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v4}, Lepson/scan/activity/ScanActivity;->getBmRectF()Landroid/graphics/RectF;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 650
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v9}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->left:F

    iget v10, v1, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    mul-float v8, v8, v9

    div-float/2addr v8, v5

    add-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->left:F

    .line 651
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    iget-object v9, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v9}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->y:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v1, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    mul-float v8, v8, v9

    div-float/2addr v8, v6

    add-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->top:F

    .line 652
    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v9}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->right:F

    iget v10, v1, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    mul-float v8, v8, v9

    div-float/2addr v8, v5

    add-float/2addr v7, v8

    iput v7, v4, Landroid/graphics/RectF;->right:F

    .line 653
    iget-object v5, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v5}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    iget-object v7, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v7}, Lepson/scan/activity/ScanActivity;->getmBaseBot()Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v8}, Lepson/scan/activity/ScanActivity;->getmBaseTop()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    sub-float/2addr v8, v1

    mul-float v7, v7, v8

    div-float/2addr v7, v6

    add-float/2addr v5, v7

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 654
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1, v4}, Lepson/scan/activity/ScanActivity;->setBmRectF(Landroid/graphics/RectF;)V

    .line 660
    :cond_9
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getmTop()Landroid/graphics/Point;

    move-result-object v4

    iget-object v5, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v5}, Lepson/scan/activity/ScanActivity;->getmBot()Landroid/graphics/Point;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lepson/scan/activity/ScanActivity;->calculateTheCenterPoint(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v4

    invoke-virtual {v1, v4}, Lepson/scan/activity/ScanActivity;->setmCenter(Landroid/graphics/Point;)V

    .line 663
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$1400(Lepson/scan/activity/ScanActivity;)I

    move-result v4

    const/16 v5, 0x64

    mul-int/lit8 v4, v4, 0x64

    div-int/lit16 v4, v4, 0x28a

    div-int/2addr v5, v4

    int-to-float v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v1, v4}, Lepson/scan/activity/ScanActivity;->setInSampleSize(I)V

    .line 664
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->get_bmOption()Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v4}, Lepson/scan/activity/ScanActivity;->getInSampleSize()I

    move-result v4

    iput v4, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 666
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->setupScanArea()V

    .line 668
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1, v2}, Lepson/scan/activity/ScanActivity;->access$602(Lepson/scan/activity/ScanActivity;Z)Z

    .line 669
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1, v3}, Lepson/scan/activity/ScanActivity;->setSetSize(Z)V

    .line 674
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    iget-object v1, v1, Lepson/scan/activity/ScanActivity;->context:Landroid/content/Context;

    const-string v4, "epson.scanner.SelectedScanner"

    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 675
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    iget-boolean v4, v4, Lepson/scan/activity/ScanActivity;->isDoctable:Z

    if-eqz v4, :cond_a

    const-string v4, "SCAN_REFS_SCANNER_CHOSEN_SIZE_DOC"

    .line 676
    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_1

    :cond_a
    const-string v4, "SCAN_REFS_SCANNER_CHOSEN_SIZE_ADF"

    .line 678
    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 680
    :goto_1
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4}, Lepson/scan/activity/ScanActivity;->access$1300(Lepson/scan/activity/ScanActivity;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 681
    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    if-ne v1, v4, :cond_b

    .line 682
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {}, Lepson/scan/lib/ScanSizeHelper;->getDefaultScanSize()I

    move-result v4

    invoke-static {v1, v4}, Lepson/scan/activity/ScanActivity;->access$1800(Lepson/scan/activity/ScanActivity;I)V

    goto :goto_2

    .line 684
    :cond_b
    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4, v1}, Lepson/scan/activity/ScanActivity;->access$1800(Lepson/scan/activity/ScanActivity;I)V

    .line 690
    :cond_c
    :goto_2
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1}, Lepson/scan/activity/ScanActivity;->access$100(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    move-result-object v1

    iget-object v4, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v4

    iput-object v4, v1, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->redrawCBHandler:Landroid/os/Handler;

    .line 692
    iget-object v1, v0, Lepson/scan/activity/ScanActivity$4;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v1, v2}, Lepson/scan/activity/ScanActivity;->access$1302(Lepson/scan/activity/ScanActivity;Z)Z

    :cond_d
    return v3
.end method

.class public Lepson/scan/activity/StringListAdapter;
.super Landroid/widget/BaseAdapter;
.source "StringListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/activity/StringListAdapter$Builder;
    }
.end annotation


# instance fields
.field private mCurrentPosition:I

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mStringArray:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;I)V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/StringListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 25
    iput-object p2, p0, Lepson/scan/activity/StringListAdapter;->mStringArray:[Ljava/lang/String;

    .line 26
    iput p3, p0, Lepson/scan/activity/StringListAdapter;->mCurrentPosition:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 31
    iget-object v0, p0, Lepson/scan/activity/StringListAdapter;->mStringArray:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    .line 36
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 48
    iget-object p2, p0, Lepson/scan/activity/StringListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a0076

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const p3, 0x7f08035d

    .line 51
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 52
    iget-object v1, p0, Lepson/scan/activity/StringListAdapter;->mStringArray:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f0801b4

    .line 54
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 55
    iget v1, p0, Lepson/scan/activity/StringListAdapter;->mCurrentPosition:I

    if-ne p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-object p2
.end method

.class Lepson/scan/activity/InputUnitSettingActivity$2;
.super Ljava/lang/Object;
.source "InputUnitSettingActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/InputUnitSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/InputUnitSettingActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/InputUnitSettingActivity;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 137
    iget-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p1, p3}, Lepson/scan/activity/InputUnitSettingActivity;->access$302(Lepson/scan/activity/InputUnitSettingActivity;I)I

    const p1, 0x7f070056

    const p2, 0x7f0e0505

    const/4 p4, 0x1

    const p5, 0x7f070145

    const v0, 0x7f070057

    const v1, 0x7f0e0510

    const/4 v2, 0x0

    packed-switch p3, :pswitch_data_0

    goto/16 :goto_0

    .line 147
    :pswitch_0
    iget-object p3, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p3}, Lepson/scan/activity/InputUnitSettingActivity;->access$400(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p3

    .line 148
    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v3

    iget-object v4, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-virtual {v4, v1}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v0

    .line 147
    invoke-virtual {p3, v2, v0}, Lepson/common/CustomListRowAdapter;->setList(ILepson/common/CustomListRow;)V

    .line 149
    iget-object p3, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p3}, Lepson/scan/activity/InputUnitSettingActivity;->access$400(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p3

    .line 150
    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v0

    iget-object v1, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-virtual {v1, p2}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p1

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p1

    .line 149
    invoke-virtual {p3, p4, p1}, Lepson/common/CustomListRowAdapter;->setList(ILepson/common/CustomListRow;)V

    goto :goto_0

    .line 141
    :pswitch_1
    iget-object p3, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p3}, Lepson/scan/activity/InputUnitSettingActivity;->access$400(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p3

    .line 142
    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object v3

    iget-object v4, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-virtual {v4, v1}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-virtual {v0, p5}, Lepson/common/CustomListRowImpl;->suffixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p5

    .line 141
    invoke-virtual {p3, v2, p5}, Lepson/common/CustomListRowAdapter;->setList(ILepson/common/CustomListRow;)V

    .line 143
    iget-object p3, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p3}, Lepson/scan/activity/InputUnitSettingActivity;->access$400(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p3

    .line 144
    invoke-static {}, Lepson/common/CustomListRowImpl;->create()Lepson/common/CustomListRowImpl;

    move-result-object p5

    iget-object v0, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-virtual {v0, p2}, Lepson/scan/activity/InputUnitSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p5, p2}, Lepson/common/CustomListRowImpl;->addText(Ljava/lang/String;)Lepson/common/CustomListRowImpl;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Lepson/common/CustomListRowImpl;->prefixImage(Ljava/lang/Integer;)Lepson/common/CustomListRowImpl;

    move-result-object p1

    .line 143
    invoke-virtual {p3, p4, p1}, Lepson/common/CustomListRowAdapter;->setList(ILepson/common/CustomListRow;)V

    .line 157
    :goto_0
    iget-object p1, p0, Lepson/scan/activity/InputUnitSettingActivity$2;->this$0:Lepson/scan/activity/InputUnitSettingActivity;

    invoke-static {p1}, Lepson/scan/activity/InputUnitSettingActivity;->access$400(Lepson/scan/activity/InputUnitSettingActivity;)Lepson/common/CustomListRowAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/CustomListRowAdapter;->notifyDataSetChanged()V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

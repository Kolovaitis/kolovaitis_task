.class Lepson/scan/activity/ScanActivity$8;
.super Ljava/lang/Thread;
.source "ScanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/scan/activity/ScanActivity;->scanStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 978
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 985
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    const-string v1, "scanner"

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 989
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    const-string v1, "scanner"

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 997
    :cond_1
    :goto_0
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result v0

    if-nez v0, :cond_2

    .line 999
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/scan/activity/ScanCount;->countUpScanPage(Landroid/content/Context;)V

    .line 1003
    :cond_2
    invoke-static {}, Lepson/common/Utils;->isMediaMounted()Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_3

    .line 1004
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    const/16 v2, 0x65

    invoke-static {v0, v2}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1005
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 1010
    :cond_3
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2400(Lepson/scan/activity/ScanActivity;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1011
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    const/16 v2, -0x3e9

    invoke-static {v0, v2}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1012
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 1017
    :cond_4
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2500(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v0

    const/16 v2, -0x514

    if-eqz v0, :cond_16

    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2500(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x1

    if-ge v0, v3, :cond_5

    goto/16 :goto_5

    .line 1027
    :cond_5
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2600(Lepson/scan/activity/ScanActivity;)I

    move-result v0

    const/4 v4, 0x3

    const/16 v5, -0x41b

    if-eq v0, v4, :cond_6

    .line 1039
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v4

    iget-object v6, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v6}, Lepson/scan/activity/ScanActivity;->access$2500(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v7}, Lepson/scan/activity/ScanActivity;->access$2700(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lepson/scan/lib/escanLib;->probeScannerById(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-static {v0, v4}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1040
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v0

    if-ne v0, v5, :cond_7

    .line 1041
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "scan, ESCAN_ERR_LIB_NOT_INITIALIZED"

    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    iget-object v4, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0, v4}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    .line 1043
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v4

    iget-object v5, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v5}, Lepson/scan/activity/ScanActivity;->access$2500(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v6}, Lepson/scan/activity/ScanActivity;->access$2700(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lepson/scan/lib/escanLib;->probeScannerById(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-static {v0, v4}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    goto :goto_1

    .line 1029
    :cond_6
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v4

    iget-object v6, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v6}, Lepson/scan/activity/ScanActivity;->access$2500(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v7}, Lepson/scan/activity/ScanActivity;->access$2700(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lepson/scan/lib/escanLib;->probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-static {v0, v4}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1030
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v0

    if-ne v0, v5, :cond_7

    .line 1031
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "scan, ESCAN_ERR_LIB_NOT_INITIALIZED"

    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    iget-object v4, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0, v4}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    .line 1033
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v4

    iget-object v5, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v5}, Lepson/scan/activity/ScanActivity;->access$2500(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v6}, Lepson/scan/activity/ScanActivity;->access$2700(Lepson/scan/activity/ScanActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lepson/scan/lib/escanLib;->probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-static {v0, v4}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1048
    :cond_7
    :goto_1
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v0

    if-eqz v0, :cond_9

    .line 1049
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2900(Lepson/scan/activity/ScanActivity;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1050
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_8
    return-void

    .line 1055
    :cond_9
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "scan, done probe"

    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getListFoundScanner()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getListFoundScanner()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v3, :cond_a

    goto/16 :goto_4

    .line 1066
    :cond_a
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lepson/scan/lib/escanLib;->escanWrapperSetScanner(I)I

    move-result v4

    invoke-static {v0, v4}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1067
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v0

    if-eqz v0, :cond_b

    .line 1068
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0, v2}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1069
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 1074
    :cond_b
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v2

    invoke-virtual {v2}, Lepson/scan/lib/escanLib;->getListFoundScanner()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lepson/scan/activity/ScanActivity;->access$2702(Lepson/scan/activity/ScanActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1077
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->resetEscanLib()V

    .line 1079
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_c

    .line 1080
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "scan, getListScannedFile == NULL"

    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1081
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v2}, Lepson/scan/lib/escanLib;->setListScannedFile(Ljava/util/List;)V

    goto :goto_2

    .line 1083
    :cond_c
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1084
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "scan, getListScannedFile != NULL"

    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1087
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0, v5}, Lepson/scan/activity/ScanActivity;->access$3002(Lepson/scan/activity/ScanActivity;I)I

    .line 1093
    :cond_d
    :goto_2
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v2

    invoke-virtual {v2}, Lepson/scan/lib/escanLib;->GetSupportedOption()I

    move-result v2

    invoke-static {v0, v2}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1097
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    iget-object v2, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v2}, Lepson/scan/activity/ScanActivity;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4}, Lepson/scan/activity/ScanActivity;->access$3100(Lepson/scan/activity/ScanActivity;)Lcom/epson/iprint/shared/SharedParamScan;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lepson/scan/lib/escanLib;->makeJobAttribute(Landroid/content/Context;Lcom/epson/iprint/shared/SharedParamScan;)V

    .line 1098
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    monitor-enter v0

    .line 1099
    :try_start_0
    iget-object v2, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    iget-object v4, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v4

    invoke-virtual {v4}, Lepson/scan/lib/escanLib;->getScanParams()Lepson/scan/lib/I1ScanParams;

    move-result-object v4

    invoke-static {v2, v4}, Lepson/scan/activity/ScanActivity;->access$3202(Lepson/scan/activity/ScanActivity;Lepson/scan/lib/I1ScanParams;)Lepson/scan/lib/I1ScanParams;

    .line 1100
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1102
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2900(Lepson/scan/activity/ScanActivity;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->isScanning()Z

    move-result v0

    if-eqz v0, :cond_e

    goto/16 :goto_3

    .line 1108
    :cond_e
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$3100(Lepson/scan/activity/ScanActivity;)Lcom/epson/iprint/shared/SharedParamScan;

    move-result-object v2

    invoke-static {v0, v2}, Lepson/scan/activity/ScanActivity;->access$3300(Lepson/scan/activity/ScanActivity;Lcom/epson/iprint/shared/SharedParamScan;)Ljava/lang/String;

    move-result-object v0

    .line 1109
    iget-object v2, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v2}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "scan, fname = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    iget-object v2, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v2}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v4

    invoke-virtual {v4, v0}, Lepson/scan/lib/escanLib;->startScanJob(Ljava/lang/String;)I

    move-result v0

    invoke-static {v2, v0}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1112
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[ESCAN][DONE] escanLib().startScanJob - return - "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v4}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1115
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v0

    const/16 v2, 0x28

    if-ne v0, v2, :cond_f

    .line 1116
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0, v3}, Lepson/scan/activity/ScanActivity;->access$2902(Lepson/scan/activity/ScanActivity;Z)Z

    .line 1117
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0, v5}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1119
    :cond_f
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2900(Lepson/scan/activity/ScanActivity;)Z

    move-result v0

    const/4 v2, 0x2

    if-nez v0, :cond_12

    .line 1120
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "IN-> isStopScan."

    invoke-static {v0, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getStatus()[I

    move-result-object v0

    aget v0, v0, v2

    const/4 v3, 0x4

    if-eq v0, v3, :cond_12

    .line 1122
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2300(Lepson/scan/activity/ScanActivity;)I

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getStatus()[I

    move-result-object v0

    aget v0, v0, v2

    if-eqz v0, :cond_12

    .line 1123
    :cond_10
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1124
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2800(Lepson/scan/activity/ScanActivity;)Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->getListScannedFile()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_11

    .line 1125
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "END SCAN because error."

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1126
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v3, 0x1f4

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1128
    :cond_11
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SCAN error."

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 1133
    :cond_12
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "END SCAN NORMALY."

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 1103
    :cond_13
    :goto_3
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scan, after makeJobAttribute"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v1

    .line 1100
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1057
    :cond_14
    :goto_4
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v0}, Lepson/scan/activity/ScanActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "scan, ESCAN_ERR_SCANNER_NOT_FOUND"

    invoke-static {v0, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0, v2}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1059
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$2900(Lepson/scan/activity/ScanActivity;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 1060
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_15
    return-void

    .line 1019
    :cond_16
    :goto_5
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0, v2}, Lepson/scan/activity/ScanActivity;->access$2302(Lepson/scan/activity/ScanActivity;I)I

    .line 1020
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$8;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/ScanActivity;->access$1900(Lepson/scan/activity/ScanActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

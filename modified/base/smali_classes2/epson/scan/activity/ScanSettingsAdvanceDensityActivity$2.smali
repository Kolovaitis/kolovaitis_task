.class Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;
.super Ljava/lang/Object;
.source "ScanSettingsAdvanceDensityActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 85
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->access$300(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    invoke-static {p1, v0}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->access$202(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;Z)Z

    .line 86
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->access$200(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 89
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->access$400(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Landroid/widget/SeekBar;

    move-result-object p1

    const/16 v0, 0x7f

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 90
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->access$400(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Landroid/widget/SeekBar;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    goto :goto_0

    .line 93
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity$2;->this$0:Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->access$400(Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;)Landroid/widget/SeekBar;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_0
    return-void
.end method

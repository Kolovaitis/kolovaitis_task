.class Lepson/scan/activity/ScanActivity$12;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 1375
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$12;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1379
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$12;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-lez p1, :cond_0

    .line 1380
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lepson/scan/activity/ScanActivity$12;->this$0:Lepson/scan/activity/ScanActivity;

    const-class v1, Lepson/print/ActivityPrintWeb;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "from"

    const/4 v1, 0x1

    .line 1382
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "print_web"

    .line 1385
    iget-object v1, p0, Lepson/scan/activity/ScanActivity$12;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-virtual {v1}, Lepson/scan/activity/ScanActivity;->getListSavedJPGFilePath()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "print_log"

    .line 1386
    invoke-static {}, Lepson/scan/activity/ScanActivity;->getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1388
    iget-object v0, p0, Lepson/scan/activity/ScanActivity$12;->this$0:Lepson/scan/activity/ScanActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Lepson/scan/activity/ScanActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.class Lepson/scan/activity/I2ScanActivity$ProgressCallback;
.super Ljava/lang/Object;
.source "I2ScanActivity.java"

# interfaces
.implements Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/I2ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/I2ScanActivity;


# direct methods
.method private constructor <init>(Lepson/scan/activity/I2ScanActivity;)V
    .locals 0

    .line 1039
    iput-object p1, p0, Lepson/scan/activity/I2ScanActivity$ProgressCallback;->this$0:Lepson/scan/activity/I2ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/scan/activity/I2ScanActivity;Lepson/scan/activity/I2ScanActivity$1;)V
    .locals 0

    .line 1039
    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity$ProgressCallback;-><init>(Lepson/scan/activity/I2ScanActivity;)V

    return-void
.end method


# virtual methods
.method public onProgressUpdate(II)V
    .locals 1

    .line 1043
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity$ProgressCallback;->this$0:Lepson/scan/activity/I2ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/I2ScanActivity;->access$800(Lepson/scan/activity/I2ScanActivity;)Lepson/scan/activity/I2ScanActivity$LocalHandler;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lepson/scan/activity/I2ScanActivity$LocalHandler;->sendUpdateScanProgressPreviewMessage(II)V

    return-void
.end method

.method public onScanEnd(Lepson/scan/i2lib/I2ScanTask$TaskError;)V
    .locals 1

    .line 1048
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity$ProgressCallback;->this$0:Lepson/scan/activity/I2ScanActivity;

    invoke-static {v0, p1}, Lepson/scan/activity/I2ScanActivity;->access$900(Lepson/scan/activity/I2ScanActivity;Lepson/scan/i2lib/I2ScanTask$TaskError;)V

    return-void
.end method

.class public Lepson/scan/activity/ScanSettingsActivity;
.super Lepson/print/ActivityIACommon;
.source "ScanSettingsActivity.java"


# static fields
.field public static final HIDE_COLOR_MODE:Ljava/lang/String; = "hide-color-mode"

.field public static final HIDE_RESOLUTION:Ljava/lang/String; = "hide-resolution"

.field public static final IS_NEW_SAVE:Ljava/lang/String; = "IS_NEW_SAVE"

.field public static final NO_CLEAR_RESULT:Ljava/lang/String; = "NO_CLEAR_RESULT"

.field public static final PARAM_KEY_EXTERNAL_DATA:Ljava/lang/String; = "external-scan-params"

.field private static final REQUEST_CODE_BRIGHTNESS:I = 0x8

.field private static final REQUEST_CODE_CHANGE_ESC_I_VERSION:I = 0x64

.field private static final REQUEST_CODE_SELECT_SCANNER:I = 0x1

.field private static final SCAN_SETTINGS_LAST_DENSITY:Ljava/lang/String; = "SCAN_SETTINGS_LAST_DENSITY"

.field private static final SCAN_SETTINGS_LAST_DENSITY_STATUS:Ljava/lang/String; = "SCAN_SETTINGS_LAST_DENSITY_STATUS"

.field private static final SCAN_SETTINGS_LAST_VALUE:Ljava/lang/String; = "SCAN_SETTINGS_LAST_VALUE"

.field private static final SCAN_SUPPORTED_RESOLUTION_LIST:Ljava/lang/String; = "SCAN_SUPPORTED_RESOLUTION_LIST"

.field private static final SCAN_SUPPORTED_SIZE_LIST:Ljava/lang/String; = "SCAN_SUPPORTED_SIZE_LIST"


# instance fields
.field private autoConnectSsid:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private errorDialog:Landroid/app/AlertDialog;

.field private isNeedSaveSupportedOptions:Z

.field private llScanName:Landroid/widget/LinearLayout;

.field private llScanSettingsAdvance2Sided:Landroid/widget/LinearLayout;

.field private llScanSettingsColor:Landroid/widget/LinearLayout;

.field private llScanSettingsDensity:Landroid/widget/LinearLayout;

.field private llScanSettingsGamma:Landroid/widget/LinearLayout;

.field private llScanSettingsResolution:Landroid/widget/LinearLayout;

.field private llScanSettingsSize:Landroid/widget/LinearLayout;

.field private llScanSettingsSource:Landroid/widget/LinearLayout;

.field private mBrightnessSeparator:Landroid/view/View;

.field private mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

.field private mHideColorMode:Z

.field private mHideResolution:Z

.field private mLLScanNameClick:Landroid/view/View$OnClickListener;

.field private mLLScanSettings2Sided:Landroid/view/View$OnClickListener;

.field private mLLScanSettingsColor:Landroid/view/View$OnClickListener;

.field private mLLScanSettingsDensity:Landroid/view/View$OnClickListener;

.field private mLLScanSettingsGamma:Landroid/view/View$OnClickListener;

.field private mLLScanSettingsResolution:Landroid/view/View$OnClickListener;

.field private mLLScanSettingsSize:Landroid/view/View$OnClickListener;

.field private mLLScanSettingsSource:Landroid/view/View$OnClickListener;

.field private mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

.field private progressGetOption:Landroid/view/View;

.field private scanInfo:Lepson/scan/lib/ScannerInfo;

.field private siOrigin:Lepson/scan/lib/ScannerInfo;

.field private tvScanner2Sided:Landroid/widget/TextView;

.field private tvScannerColor:Landroid/widget/TextView;

.field private tvScannerDensity:Landroid/widget/TextView;

.field private tvScannerGamma:Landroid/widget/TextView;

.field private tvScannerName:Landroid/widget/TextView;

.field private tvScannerResolution:Landroid/widget/TextView;

.field private tvScannerSize:Landroid/widget/TextView;

.field private tvScannerSource:Landroid/widget/TextView;

.field private viewScanSettingsAdvance2Sided:Landroid/view/View;

.field private viewScanSettingsSource:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 85
    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->autoConnectSsid:Ljava/lang/String;

    const/4 v0, 0x0

    .line 91
    iput-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->isNeedSaveSupportedOptions:Z

    .line 371
    new-instance v0, Lepson/scan/activity/ScanSettingsActivity$1;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsActivity$1;-><init>(Lepson/scan/activity/ScanSettingsActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanNameClick:Landroid/view/View$OnClickListener;

    .line 410
    new-instance v0, Lepson/scan/activity/ScanSettingsActivity$2;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsActivity$2;-><init>(Lepson/scan/activity/ScanSettingsActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsSource:Landroid/view/View$OnClickListener;

    .line 423
    new-instance v0, Lepson/scan/activity/ScanSettingsActivity$3;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsActivity$3;-><init>(Lepson/scan/activity/ScanSettingsActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsSize:Landroid/view/View$OnClickListener;

    .line 434
    new-instance v0, Lepson/scan/activity/ScanSettingsActivity$4;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsActivity$4;-><init>(Lepson/scan/activity/ScanSettingsActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsColor:Landroid/view/View$OnClickListener;

    .line 446
    new-instance v0, Lepson/scan/activity/ScanSettingsActivity$5;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsActivity$5;-><init>(Lepson/scan/activity/ScanSettingsActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsResolution:Landroid/view/View$OnClickListener;

    .line 458
    new-instance v0, Lepson/scan/activity/ScanSettingsActivity$6;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsActivity$6;-><init>(Lepson/scan/activity/ScanSettingsActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettings2Sided:Landroid/view/View$OnClickListener;

    .line 468
    new-instance v0, Lepson/scan/activity/ScanSettingsActivity$7;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsActivity$7;-><init>(Lepson/scan/activity/ScanSettingsActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsDensity:Landroid/view/View$OnClickListener;

    .line 477
    new-instance v0, Lepson/scan/activity/ScanSettingsActivity$8;

    invoke-direct {v0, p0}, Lepson/scan/activity/ScanSettingsActivity$8;-><init>(Lepson/scan/activity/ScanSettingsActivity;)V

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsGamma:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lepson/scan/activity/ScanSettingsActivity;)Landroid/widget/LinearLayout;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanName:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic access$100(Lepson/scan/activity/ScanSettingsActivity;)Lepson/scan/lib/ScannerInfo;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    return-object p0
.end method

.method static synthetic access$200(Lepson/scan/activity/ScanSettingsActivity;)Ljava/lang/String;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsActivity;->autoConnectSsid:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lepson/scan/activity/ScanSettingsActivity;)Landroid/view/View;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/scan/activity/ScanSettingsActivity;->progressGetOption:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$400(Lepson/scan/activity/ScanSettingsActivity;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->startInputUnitSettingActivity()V

    return-void
.end method

.method static synthetic access$500(Lepson/scan/activity/ScanSettingsActivity;Ljava/lang/String;III)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lepson/scan/activity/ScanSettingsActivity;->startDetailSettingScreenForResult(Ljava/lang/String;III)V

    return-void
.end method

.method static synthetic access$600(Lepson/scan/activity/ScanSettingsActivity;Ljava/lang/String;III)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lepson/scan/activity/ScanSettingsActivity;->goToSettingAdvanceDetailsScreen(Ljava/lang/String;III)V

    return-void
.end method

.method static synthetic access$700(Lepson/scan/activity/ScanSettingsActivity;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->goToSettingAdvanceDensityScreen()V

    return-void
.end method

.method private backToScanScreen(Z)V
    .locals 3

    .line 1012
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1013
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V

    .line 1016
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ACT_RESULT"

    const-string v2, "ACT_RESULT_SAVE"

    .line 1018
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "IS_NEW_SAVE"

    const/4 v2, 0x1

    .line 1019
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "NO_CLEAR_RESULT"

    .line 1020
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 1022
    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/ScanSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 1023
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->finish()V

    return-void
.end method

.method private displayNewDocumentSourceSetting()V
    .locals 2

    .line 896
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result v0

    if-nez v0, :cond_0

    .line 898
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsSource:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 899
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->viewScanSettingsSource:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 901
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsSource:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 902
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->viewScanSettingsSource:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 907
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSource:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getSourceName()I

    move-result v1

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 910
    :goto_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningName()I

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfDuplex()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lepson/scan/activity/ScanSettingsActivity;->updateScanner2SidedSetting(II)V

    return-void
.end method

.method private displayScannerName()V
    .locals 2

    .line 291
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 292
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 294
    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/EPPrinterManager;->getIpPrinterUserName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 296
    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 301
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 302
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 303
    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 305
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerName:Landroid/widget/TextView;

    const v1, 0x7f0e04d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void
.end method

.method private getScannerSavedSettings()V
    .locals 7

    const-string v0, "epson.scan.activity.ScanSettingsActivity"

    const-string v1, "Start get saved option"

    .line 239
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->loadRefScannerName()V

    .line 241
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->loadRefScannerSourceSetting()V

    .line 242
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->loadRefScannerColorSetting()V

    .line 243
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->loadRefScannerResolutionSetting()V

    .line 244
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->loadRefScannerBasicOptions()V

    .line 249
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setSizeValue(I)V

    .line 251
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, p0, v1}, Lepson/scan/lib/ScanInfoStorage;->loadScanSetting(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    const/16 v0, 0x40

    .line 253
    new-array v1, v0, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 255
    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->context:Landroid/content/Context;

    const-string v4, "epson.scanner.supported.Options"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SCAN_REFS_OPTIONS_RESOLUTION_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 257
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedResolutionList([I)V

    const-string v0, "scanner"

    .line 260
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->autoConnectSsid:Ljava/lang/String;

    .line 263
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningName()I

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfDuplex()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lepson/scan/activity/ScanSettingsActivity;->updateScanner2SidedSetting(II)V

    .line 264
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerDensity:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getDensityValue()I

    move-result v1

    rsub-int v1, v1, 0xff

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerGamma:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getGammaName()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private goToSettingAdvanceDensityScreen()V
    .locals 2

    .line 543
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getDensityValue()I

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->isDensityStatus()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->getStartIntent(Landroid/content/Context;IZ)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x8

    .line 544
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/ScanSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private goToSettingAdvanceDetailsScreen(Ljava/lang/String;III)V
    .locals 2

    .line 556
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/ScanSettingsAdvanceDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 558
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "SCAN_SETTINGS_LAST_VALUE"

    .line 559
    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "SCAN_REFS_OPTIONS_SUPPORTED_ADF_DUPLEX"

    .line 560
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfDuplex()I

    move-result p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "SCAN_REFS_SETTINGS_ROTATE"

    .line 563
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes()Z

    move-result p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    .line 565
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 567
    invoke-virtual {p0, v0, p4}, Lepson/scan/activity/ScanSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private loadRefScannerBasicOptions()V
    .locals 2

    .line 368
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, p0, v1}, Lepson/scan/lib/ScanInfoStorage;->loadSupportedValue(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    return-void
.end method

.method private loadRefScannerColorSetting()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 344
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, p0, v1}, Lepson/scan/lib/ScanInfoStorage;->loadColorValue(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 346
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerColor:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getColorName()I

    move-result v1

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private loadRefScannerName()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 276
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, p0, v1}, Lepson/scan/lib/ScanInfoStorage;->loadScannerConnectivityInfo(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 279
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->displayScannerName()V

    .line 281
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->updatePrinterIcon()V

    return-void
.end method

.method private loadRefScannerResolutionSetting()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 357
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, p0, v1}, Lepson/scan/lib/ScanInfoStorage;->loadResolution(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 359
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerResolution:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1, p0}, Lepson/scan/lib/ScannerInfo;->getResolutionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private loadRefScannerSourceSetting()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 317
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, p0, v1}, Lepson/scan/lib/ScanInfoStorage;->loadSupportedAdfSize(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 320
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, p0, v1}, Lepson/scan/lib/ScanInfoStorage;->loadReadUnitValue(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 323
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSource:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getSourceName()I

    move-result v1

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result v0

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsSource:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->viewScanSettingsSource:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 330
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsSource:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->viewScanSettingsSource:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private onScannerSelectResult(ILandroid/content/Intent;)V
    .locals 7

    const/4 v0, -0x1

    if-ne p1, v0, :cond_4

    .line 708
    invoke-static {p2}, Lepson/scan/activity/SettingActivityParams;->getScannerPropertyWrapper(Landroid/content/Intent;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    .line 719
    :cond_0
    invoke-virtual {v2}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result p1

    const/4 p2, 0x2

    if-ne p1, p2, :cond_2

    .line 722
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-static {p1}, Lepson/scan/activity/SettingActivityParams;->returnIfEscIVersionChanged(Landroid/content/Intent;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    .line 725
    invoke-static {v2}, Lepson/scan/activity/SettingActivityParams;->getSettingReturnIntent(Lepson/scan/activity/ScannerPropertyWrapper;)Landroid/content/Intent;

    move-result-object p2

    .line 724
    invoke-virtual {p0, p1, p2}, Lepson/scan/activity/ScanSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 726
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->finish()V

    return-void

    :cond_1
    const/4 v3, 0x1

    .line 731
    iget-boolean v4, p0, Lepson/scan/activity/ScanSettingsActivity;->mHideColorMode:Z

    iget-boolean v5, p0, Lepson/scan/activity/ScanSettingsActivity;->mHideResolution:Z

    iget-object v6, p0, Lepson/scan/activity/ScanSettingsActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lepson/scan/activity/SettingActivityParams;->getChangeSettingActivityStartIntent(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;ZZZLcom/epson/iprint/shared/SharedParamScan;)Landroid/content/Intent;

    move-result-object p1

    const/16 p2, 0x64

    .line 734
    invoke-virtual {p0, p1, p2}, Lepson/scan/activity/ScanSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    .line 738
    :cond_2
    invoke-virtual {v2}, Lepson/scan/activity/ScannerPropertyWrapper;->getI1ScannerInfo()Lepson/scan/lib/ScannerInfo;

    move-result-object p1

    if-nez p1, :cond_3

    return-void

    .line 747
    :cond_3
    invoke-virtual {v2}, Lepson/scan/activity/ScannerPropertyWrapper;->getSimpleApSsid()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanSettingsActivity;->updateSelectedScannerInfo(Lepson/scan/lib/ScannerInfo;Ljava/lang/String;)V

    goto :goto_0

    .line 751
    :cond_4
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->updateIpAddressPrinter()V

    :goto_0
    return-void
.end method

.method private onSettingDone()V
    .locals 4

    .line 958
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->progressGetOption:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    return-void

    .line 963
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->isReSearch()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 964
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 965
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setReSearch(Z)V

    .line 969
    :cond_1
    iget-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->isNeedSaveSupportedOptions:Z

    if-eqz v0, :cond_3

    .line 970
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->context:Landroid/content/Context;

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-static {v0, v2}, Lepson/scan/lib/ScanSettingHelper;->saveSupportedOptionToRef(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 973
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->autoConnectSsid:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 974
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->autoConnectSsid:Ljava/lang/String;

    const-string v2, "scanner"

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 975
    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v3

    .line 974
    invoke-static {p0, v0, v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setConnectInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "scanner"

    .line 977
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V

    :cond_3
    :goto_0
    const/4 v0, 0x0

    .line 980
    iput-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->isNeedSaveSupportedOptions:Z

    .line 985
    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 986
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 987
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getColorValue()I

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getColorValue()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 988
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getResolutionValue()I

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getResolutionValue()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 989
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 990
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes()Z

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes()Z

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 991
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getAdfPaperGuide()I

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getAdfPaperGuide()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 992
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getDensityValue()I

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getDensityValue()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 993
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getGammaValue()I

    move-result v2

    iget-object v3, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v3}, Lepson/scan/lib/ScannerInfo;->getGammaValue()I

    move-result v3

    if-ne v2, v3, :cond_4

    const/4 v0, 0x1

    .line 999
    :cond_4
    invoke-static {p0, v1}, Lepson/scan/lib/ScanInfoStorage;->saveEscIVersion(Landroid/content/Context;I)V

    .line 1002
    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->context:Landroid/content/Context;

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-static {v1, v2}, Lepson/scan/lib/ScanSettingHelper;->saveAllUserSettingsToRef(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 1007
    invoke-direct {p0, v0}, Lepson/scan/activity/ScanSettingsActivity;->backToScanScreen(Z)V

    return-void
.end method

.method private resetSettings()V
    .locals 2

    .line 1044
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setScannerId(Ljava/lang/String;)V

    .line 1045
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setIp(Ljava/lang/String;)V

    .line 1046
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setModelName(Ljava/lang/String;)V

    .line 1047
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setLocation(I)V

    const-string v0, ""

    .line 1049
    iput-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->autoConnectSsid:Ljava/lang/String;

    .line 1051
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->setBlankSettings()V

    .line 1053
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->showNewSelectedScannerInfo()V

    .line 1055
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->displayScannerName()V

    .line 1057
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->updatePrinterIcon()V

    return-void
.end method

.method private setBlankSettings()V
    .locals 4

    .line 835
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 837
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfHeight(I)V

    .line 838
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfWidth(I)V

    .line 839
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfDuplex(I)V

    .line 842
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    .line 845
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const/16 v2, 0x96

    invoke-virtual {v0, v2}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    .line 848
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v2

    invoke-virtual {v0, v2}, Lepson/scan/lib/ScannerInfo;->setSizeValue(I)V

    .line 851
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lepson/scan/lib/ScannerInfo;->setColorValue(I)V

    .line 854
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setDensityStatus(Z)V

    .line 855
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Lepson/scan/lib/ScannerInfo;->setDensityValue(I)V

    .line 858
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0, v2}, Lepson/scan/lib/ScannerInfo;->setGammaValue(I)V

    .line 860
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsResolution:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 861
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsColor:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 863
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsDensity:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 864
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsGamma:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    return-void
.end method

.method private setColorModeClickable()V
    .locals 2

    .line 941
    iget-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mHideColorMode:Z

    if-nez v0, :cond_0

    .line 942
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsColor:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    :cond_0
    return-void
.end method

.method private setResolutionClickable()V
    .locals 2

    .line 934
    iget-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mHideResolution:Z

    if-nez v0, :cond_0

    .line 935
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsResolution:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    :cond_0
    return-void
.end method

.method private showNewSelectedScannerInfo()V
    .locals 2

    .line 873
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->displayNewDocumentSourceSetting()V

    .line 876
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSize:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getSizeValue()I

    move-result v1

    invoke-static {v1}, Lepson/scan/lib/ScanSizeHelper;->getScanSizeName(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 879
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerColor:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getColorName()I

    move-result v1

    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 882
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerResolution:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1, p0}, Lepson/scan/lib/ScannerInfo;->getResolutionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 885
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerDensity:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getDensityValue()I

    move-result v1

    rsub-int v1, v1, 0xff

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 888
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerGamma:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getGammaName()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private startDetailSettingScreenForResult(Ljava/lang/String;III)V
    .locals 3

    .line 504
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/scan/activity/ScanSettingsDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 506
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "SCAN_SETTINGS_LAST_VALUE"

    .line 507
    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "SCAN_REFS_OPTIONS_ADF_WIDTH"

    .line 508
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "SCAN_REFS_OPTIONS_ADF_HEIGHT"

    .line 509
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "SCAN_SUPPORTED_RESOLUTION_LIST"

    .line 510
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedResolutionList()[I

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string p1, "SCAN_REFS_SETTINGS_ADF_PAPER_GUIDE"

    .line 511
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getAdfPaperGuide()I

    move-result p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p1, 0x0

    .line 514
    new-array p2, p1, [I

    .line 515
    iget-object p3, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p3}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result p3

    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 524
    :pswitch_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result p1

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 525
    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result p2

    iget-object p3, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 526
    invoke-virtual {p3}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result p3

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 527
    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result v1

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 528
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicResolution()I

    move-result v2

    .line 524
    invoke-static {p1, p2, p3, v1, v2}, Lepson/scan/lib/ScanSizeHelper;->getSupportedScanSizeList(IIIII)[I

    move-result-object p2

    goto :goto_0

    .line 517
    :pswitch_1
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result p2

    iget-object p3, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 519
    invoke-virtual {p3}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicWidth()I

    move-result p3

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 520
    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicHeight()I

    move-result v1

    iget-object v2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 521
    invoke-virtual {v2}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicResolution()I

    move-result v2

    .line 517
    invoke-static {p2, p1, p3, v1, v2}, Lepson/scan/lib/ScanSizeHelper;->getSupportedScanSizeList(IIIII)[I

    move-result-object p2

    :goto_0
    const-string p1, "SCAN_SUPPORTED_SIZE_LIST"

    .line 531
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    .line 533
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 534
    invoke-virtual {p0, v0, p4}, Lepson/scan/activity/ScanSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private startInputUnitSettingActivity()V
    .locals 2

    .line 488
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 489
    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 494
    :cond_0
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 495
    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v0

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getAdfPaperGuide()I

    move-result v1

    .line 494
    invoke-static {p0, v0, v1}, Lepson/scan/activity/InputUnitSettingActivity;->getStartIntent(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    .line 497
    invoke-virtual {p0, v0, v1}, Lepson/scan/activity/ScanSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private updateIpAddressPrinter()V
    .locals 2

    .line 759
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    return-void

    .line 765
    :cond_0
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/EPPrinterManager;->doseContainScannerIdInIpPrinterList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 767
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->resetSettings()V

    :cond_1
    return-void
.end method

.method private updatePrinterIcon()V
    .locals 3

    .line 213
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->isModelNameEmpty()Z

    move-result v0

    const v1, 0x7f080198

    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 216
    :cond_0
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 224
    :pswitch_0
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f07010f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 221
    :pswitch_1
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f070111

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 227
    :pswitch_2
    invoke-virtual {p0, v1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f070110

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateScanner2SidedSetting(II)V
    .locals 1

    .line 915
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScanner2Sided:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    const/4 p1, 0x1

    if-eq p2, p1, :cond_0

    .line 919
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsAdvance2Sided:Landroid/widget/LinearLayout;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 920
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->viewScanSettingsAdvance2Sided:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 923
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsAdvance2Sided:Landroid/widget/LinearLayout;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 924
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->viewScanSettingsAdvance2Sided:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private updateScannerInfoIfEscIVersionChanged(Landroid/content/Intent;)V
    .locals 2

    .line 186
    invoke-static {p1}, Lepson/scan/activity/SettingActivityParams;->getScannerPropertyWrapper(Landroid/content/Intent;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 190
    :cond_0
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->isPrinterSet()Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->resetSettings()V

    return-void

    .line 197
    :cond_1
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getI1ScannerInfo()Lepson/scan/lib/ScannerInfo;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    const/4 v1, 0x1

    .line 205
    iput-boolean v1, p0, Lepson/scan/activity/ScanSettingsActivity;->isNeedSaveSupportedOptions:Z

    .line 208
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getSimpleApSsid()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lepson/scan/activity/ScanSettingsActivity;->updateSelectedScannerInfo(Lepson/scan/lib/ScannerInfo;Ljava/lang/String;)V

    return-void
.end method

.method private updateSelectedScannerInfo(Lepson/scan/lib/ScannerInfo;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    .line 778
    iput-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->isNeedSaveSupportedOptions:Z

    .line 781
    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/scan/lib/ScannerInfo;->setScannerId(Ljava/lang/String;)V

    .line 782
    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/scan/lib/ScannerInfo;->setIp(Ljava/lang/String;)V

    .line 783
    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v2

    invoke-virtual {v1, v2}, Lepson/scan/lib/ScannerInfo;->setLocation(I)V

    .line 784
    iget-object v1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/scan/lib/ScannerInfo;->setModelName(Ljava/lang/String;)V

    .line 787
    iput-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->autoConnectSsid:Ljava/lang/String;

    .line 790
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->displayScannerName()V

    .line 792
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->updatePrinterIcon()V

    .line 796
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const/4 v1, 0x7

    new-array v1, v1, [I

    invoke-virtual {p2, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedOptions([I)V

    .line 798
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const/16 v1, 0x40

    new-array v1, v1, [I

    invoke-virtual {p2, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedResolutionList([I)V

    .line 801
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-static {p2}, Lepson/scan/lib/ScanSettingHelper;->updateNewSelectedScannerSupportedOptions(Lepson/scan/lib/ScannerInfo;)V

    .line 804
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->setBlankSettings()V

    .line 810
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v1

    invoke-virtual {p2, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedOptions([I)V

    .line 811
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedResolutionList()[I

    move-result-object p1

    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setSupportedResolutionList([I)V

    .line 814
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-static {p1}, Lepson/scan/lib/ScanSettingHelper;->updateNewSelectedScannerSupportedOptions(Lepson/scan/lib/ScannerInfo;)V

    .line 816
    invoke-static {p0}, Lepson/scan/lib/ScanCommonParams;->load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams;

    move-result-object p1

    .line 817
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-static {p1, p2}, Lepson/scan/lib/ScanSettingHelper;->setDefaultSettings3(Lepson/scan/lib/ScanCommonParams;Lepson/scan/lib/ScannerInfo;)V

    .line 819
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->showNewSelectedScannerInfo()V

    .line 821
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object p1

    const-string p2, ""

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 822
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->setResolutionClickable()V

    .line 823
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->setColorModeClickable()V

    .line 824
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsDensity:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 825
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsGamma:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setClickable(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 577
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-eq p1, v0, :cond_9

    const/16 v1, 0x64

    const/4 v2, -0x1

    if-eq p1, v1, :cond_7

    if-ne p2, v2, :cond_6

    .line 604
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_2

    .line 682
    :pswitch_1
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string p3, "SCAN_REFS_SCANNER_CHOSEN_SIZE"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/lib/ScannerInfo;->setSizeValue(I)V

    .line 683
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSize:Landroid/widget/TextView;

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSizeValue()I

    move-result p2

    invoke-static {p2}, Lepson/scan/lib/ScanSizeHelper;->getScanSizeName(I)I

    move-result p2

    invoke-virtual {p0, p2}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 641
    :pswitch_2
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string p3, "SCAN_REFS_SETTINGS_GAMMA"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p3

    invoke-virtual {p1, p3}, Lepson/scan/lib/ScannerInfo;->setGammaValue(I)V

    const-string p1, "SCAN_REFS_SETTINGS_GAMMA_NAME"

    .line 642
    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const p2, 0x7f0e050a

    invoke-virtual {p0, p2}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const p2, 0x7f0e050b

    .line 648
    :goto_0
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerGamma:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 635
    :pswitch_3
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->getDensityFromReturnIntent(Landroid/content/Intent;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/lib/ScannerInfo;->setDensityValue(I)V

    .line 636
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-static {p3}, Lepson/scan/activity/ScanSettingsAdvanceDensityActivity;->getChangeAble(Landroid/content/Intent;)Z

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/lib/ScannerInfo;->setDensityStatus(Z)V

    .line 638
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerDensity:Landroid/widget/TextView;

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getDensityValue()I

    move-result p2

    rsub-int p2, p2, 0xff

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 608
    :pswitch_4
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string p3, "SCAN_REFS_SETTINGS_2SIDED"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p3

    invoke-virtual {p1, p3}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    const-string p1, "SCAN_REFS_SETTINGS_2SIDED_NAME"

    .line 609
    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const p3, 0x7f0e052b

    invoke-virtual {p0, p3}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const p3, 0x7f0e04e6

    .line 615
    :goto_1
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScanner2Sided:Landroid/widget/TextView;

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(I)V

    .line 617
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string p3, "SCAN_REFS_SETTINGS_ROTATE"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    if-ne p2, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    invoke-virtual {p1, v1}, Lepson/scan/lib/ScannerInfo;->setAdfDuplexRotaitonYes(Z)V

    .line 622
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result p1

    if-ne p1, v0, :cond_3

    .line 623
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1, v0}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 624
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSource:Landroid/widget/TextView;

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSourceName()I

    move-result p2

    invoke-virtual {p0, p2}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 630
    :cond_3
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    sget-object p2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/lib/ScannerInfo;->setSizeValue(I)V

    .line 631
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSize:Landroid/widget/TextView;

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSizeValue()I

    move-result p2

    invoke-static {p2}, Lepson/scan/lib/ScanSizeHelper;->getScanSizeName(I)I

    move-result p2

    invoke-virtual {p0, p2}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 692
    :pswitch_5
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string p3, "SCAN_REFS_SETTINGS_RESOLUTION"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    .line 693
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerResolution:Landroid/widget/TextView;

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2, p0}, Lepson/scan/lib/ScannerInfo;->getResolutionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 687
    :pswitch_6
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    const-string p3, "SCAN_REFS_SETTINGS_COLOR"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/lib/ScannerInfo;->setColorValue(I)V

    .line 688
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerColor:Landroid/widget/TextView;

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getColorName()I

    move-result p2

    invoke-virtual {p0, p2}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 653
    :pswitch_7
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-static {p3}, Lepson/scan/activity/InputUnitSettingActivity;->getInputUnit(Landroid/content/Intent;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 657
    invoke-static {p3}, Lepson/scan/activity/InputUnitSettingActivity;->getAdfAlignment(Landroid/content/Intent;)I

    move-result p1

    if-ltz p1, :cond_4

    .line 659
    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setAdfPaperGuide(I)V

    .line 663
    :cond_4
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSource:Landroid/widget/TextView;

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSourceName()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 666
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result p1

    if-nez p1, :cond_5

    .line 667
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1, v1}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    .line 671
    :cond_5
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningName()I

    move-result p1

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfDuplex()I

    move-result p2

    invoke-direct {p0, p1, p2}, Lepson/scan/activity/ScanSettingsActivity;->updateScanner2SidedSetting(II)V

    .line 676
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    sget-object p2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/scan/lib/ScannerInfo;->setSizeValue(I)V

    .line 677
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSize:Landroid/widget/TextView;

    iget-object p2, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSizeValue()I

    move-result p2

    invoke-static {p2}, Lepson/scan/lib/ScanSizeHelper;->getScanSizeName(I)I

    move-result p2

    invoke-virtual {p0, p2}, Lepson/scan/activity/ScanSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 702
    :cond_6
    :goto_2
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "back to settings"

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_7
    if-ne p2, v0, :cond_8

    .line 591
    invoke-direct {p0, v2, p3}, Lepson/scan/activity/ScanSettingsActivity;->onScannerSelectResult(ILandroid/content/Intent;)V

    return-void

    .line 597
    :cond_8
    invoke-virtual {p0, p2, p3}, Lepson/scan/activity/ScanSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 598
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->finish()V

    return-void

    .line 583
    :cond_9
    invoke-direct {p0, p2, p3}, Lepson/scan/activity/ScanSettingsActivity;->onScannerSelectResult(ILandroid/content/Intent;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .line 949
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 951
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackIPPrinterInfo()V

    const/4 v0, 0x0

    .line 953
    invoke-virtual {p0, v0}, Lepson/scan/activity/ScanSettingsActivity;->setResult(I)V

    .line 954
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 98
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 99
    invoke-static {}, Lepson/scan/lib/ScanInfoStorage;->getInstance()Lepson/scan/lib/ScanInfoStorage;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->mScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    const p1, 0x7f0a00b7

    .line 101
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->setContentView(I)V

    const p1, 0x7f0e04d8

    const/4 v0, 0x1

    .line 103
    invoke-virtual {p0, p1, v0}, Lepson/scan/activity/ScanSettingsActivity;->setActionBar(IZ)V

    .line 105
    iput-object p0, p0, Lepson/scan/activity/ScanSettingsActivity;->context:Landroid/content/Context;

    const p1, 0x7f0801ee

    .line 107
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanName:Landroid/widget/LinearLayout;

    .line 108
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanName:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanNameClick:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801ed

    .line 110
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsSource:Landroid/widget/LinearLayout;

    .line 111
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsSource:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsSource:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801ec

    .line 113
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsSize:Landroid/widget/LinearLayout;

    .line 114
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsSize:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsSize:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080389

    .line 116
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->viewScanSettingsSource:Landroid/view/View;

    const p1, 0x7f0801e8

    .line 118
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsColor:Landroid/widget/LinearLayout;

    .line 119
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsColor:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsColor:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801eb

    .line 121
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsResolution:Landroid/widget/LinearLayout;

    .line 122
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsResolution:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsResolution:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080358

    .line 124
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerName:Landroid/widget/TextView;

    const p1, 0x7f08035c

    .line 125
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSource:Landroid/widget/TextView;

    const p1, 0x7f08035b

    .line 126
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerSize:Landroid/widget/TextView;

    const p1, 0x7f080355

    .line 127
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerColor:Landroid/widget/TextView;

    const p1, 0x7f08035a

    .line 128
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerResolution:Landroid/widget/TextView;

    const p1, 0x7f080297

    .line 129
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->progressGetOption:Landroid/view/View;

    const p1, 0x7f0801e7

    .line 131
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsAdvance2Sided:Landroid/widget/LinearLayout;

    .line 132
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsAdvance2Sided:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettings2Sided:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080388

    .line 134
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->viewScanSettingsAdvance2Sided:Landroid/view/View;

    const p1, 0x7f0801e9

    .line 136
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsDensity:Landroid/widget/LinearLayout;

    .line 137
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsDensity:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsDensity:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08007f

    .line 138
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->mBrightnessSeparator:Landroid/view/View;

    const p1, 0x7f0801ea

    .line 140
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsGamma:Landroid/widget/LinearLayout;

    .line 141
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsGamma:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mLLScanSettingsGamma:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080354

    .line 143
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScanner2Sided:Landroid/widget/TextView;

    const p1, 0x7f080356

    .line 144
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerDensity:Landroid/widget/TextView;

    const p1, 0x7f080357

    .line 145
    invoke-virtual {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->tvScannerGamma:Landroid/widget/TextView;

    .line 150
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "hide-resolution"

    const/4 v1, 0x0

    .line 151
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mHideResolution:Z

    .line 152
    iget-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mHideResolution:Z

    const/16 v2, 0x8

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsResolution:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    const-string v0, "hide-color-mode"

    .line 155
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mHideColorMode:Z

    .line 156
    iget-boolean v0, p0, Lepson/scan/activity/ScanSettingsActivity;->mHideColorMode:Z

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->llScanSettingsColor:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    const-string v0, "external-scan-params"

    .line 159
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/shared/SharedParamScan;

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->mExternalIntentData:Lcom/epson/iprint/shared/SharedParamScan;

    .line 162
    new-instance p1, Lepson/scan/lib/ScannerInfo;

    invoke-direct {p1}, Lepson/scan/lib/ScannerInfo;-><init>()V

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    .line 163
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->getScannerSavedSettings()V

    .line 164
    iget-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 167
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->setBlankSettings()V

    .line 168
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->showNewSelectedScannerInfo()V

    goto :goto_0

    .line 170
    :cond_2
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->setResolutionClickable()V

    .line 171
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->setColorModeClickable()V

    .line 174
    :goto_0
    new-instance p1, Lepson/scan/lib/ScannerInfo;

    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->scanInfo:Lepson/scan/lib/ScannerInfo;

    invoke-direct {p1, v0}, Lepson/scan/lib/ScannerInfo;-><init>(Lepson/scan/lib/ScannerInfo;)V

    iput-object p1, p0, Lepson/scan/activity/ScanSettingsActivity;->siOrigin:Lepson/scan/lib/ScannerInfo;

    .line 177
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/scan/activity/ScanSettingsActivity;->updateScannerInfoIfEscIVersionChanged(Landroid/content/Intent;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 1063
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    .line 1064
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1066
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 1072
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-eq v0, v1, :cond_0

    .line 1078
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 1074
    :cond_0
    invoke-direct {p0}, Lepson/scan/activity/ScanSettingsActivity;->onSettingDone()V

    const/4 p1, 0x1

    return p1
.end method

.method protected onStop()V
    .locals 2

    .line 1029
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    .line 1030
    invoke-virtual {p0}, Lepson/scan/activity/ScanSettingsActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scannsetting screen, onStop"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->progressGetOption:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1033
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->errorDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1034
    iget-object v0, p0, Lepson/scan/activity/ScanSettingsActivity;->errorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    return-void
.end method

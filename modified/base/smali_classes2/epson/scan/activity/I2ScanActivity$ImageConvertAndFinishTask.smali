.class Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;
.super Landroid/os/AsyncTask;
.source "I2ScanActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/I2ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageConvertAndFinishTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/util/ArrayList<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final ERROR_CODE_FILE_WRITE_ERROR:I

.field private final ERROR_CODE_NO_ERROR:I

.field private final ERROR_CODE_NO_FILE:I

.field private mScanFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mScanParams:Lcom/epson/iprint/shared/SharedParamScan;

.field final synthetic this$0:Lepson/scan/activity/I2ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/I2ScanActivity;Lcom/epson/iprint/shared/SharedParamScan;)V
    .locals 0
    .param p1    # Lepson/scan/activity/I2ScanActivity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 1120
    iput-object p1, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->this$0:Lepson/scan/activity/I2ScanActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 p1, 0x0

    .line 1109
    iput p1, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->ERROR_CODE_NO_ERROR:I

    const/4 p1, 0x1

    .line 1110
    iput p1, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->ERROR_CODE_NO_FILE:I

    const/4 p1, 0x2

    .line 1111
    iput p1, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->ERROR_CODE_FILE_WRITE_ERROR:I

    .line 1121
    iput-object p2, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->mScanParams:Lcom/epson/iprint/shared/SharedParamScan;

    return-void
.end method

.method private convertError(I)Lepson/scan/i2lib/I2ScanTask$TaskError;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/16 p1, -0xc8

    goto :goto_0

    :cond_0
    const/16 p1, -0xca

    .line 1209
    :goto_0
    new-instance v0, Lepson/scan/i2lib/I2ScanTask$TaskError;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/util/ArrayList;)Ljava/lang/Integer;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .line 1130
    array-length v0, p1

    const/4 v1, 0x1

    if-gtz v0, :cond_0

    .line 1131
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v0, 0x0

    .line 1133
    aget-object p1, p1, v0

    .line 1134
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 1135
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 1138
    :cond_1
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->mScanParams:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v3}, Lcom/epson/iprint/shared/SharedParamScan;->getFolder_name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1139
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    const/4 v4, 0x2

    if-nez v3, :cond_2

    .line 1140
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1141
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 1145
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->mScanFileList:Ljava/util/ArrayList;

    .line 1147
    new-instance v3, Lepson/print/EPImageUtil;

    invoke-direct {v3}, Lepson/print/EPImageUtil;-><init>()V

    .line 1148
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->mScanParams:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v2}, Lcom/epson/iprint/shared/SharedParamScan;->getFile_name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_%03d.bmp"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1150
    iget-object v5, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->mScanParams:Lcom/epson/iprint/shared/SharedParamScan;

    invoke-virtual {v5}, Lcom/epson/iprint/shared/SharedParamScan;->getScan_type()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    const/4 v5, 0x1

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    .line 1155
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v6, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    add-int/2addr v6, v1

    .line 1157
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v9, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v0

    invoke-static {v8, v2, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    if-eqz v5, :cond_4

    const/16 v9, 0x7f

    .line 1161
    invoke-static {v7, v8, v9}, Lepson/print/EPImageUtil;->jpegTo1BitBmpPixelRoundDown(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    goto :goto_2

    .line 1164
    :cond_4
    invoke-virtual {v3, v7, v8, v1}, Lepson/print/EPImageUtil;->jpg2bmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    :goto_2
    if-eqz v7, :cond_5

    .line 1167
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 1170
    :cond_5
    iget-object v7, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->mScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1173
    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1108
    check-cast p1, [Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->doInBackground([Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    .line 1178
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->this$0:Lepson/scan/activity/I2ScanActivity;

    invoke-static {v0}, Lepson/scan/activity/I2ScanActivity;->access$1100(Lepson/scan/activity/I2ScanActivity;)V

    if-eqz p1, :cond_3

    .line 1180
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 1185
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    .line 1186
    iget-object p1, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->mScanFileList:Ljava/util/ArrayList;

    if-nez p1, :cond_1

    return-void

    .line 1190
    :cond_1
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->this$0:Lepson/scan/activity/I2ScanActivity;

    invoke-static {v0, p1}, Lepson/scan/activity/I2ScanActivity;->access$1200(Lepson/scan/activity/I2ScanActivity;Ljava/util/ArrayList;)V

    return-void

    .line 1195
    :cond_2
    iget-object v0, p0, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->this$0:Lepson/scan/activity/I2ScanActivity;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->convertError(I)Lepson/scan/i2lib/I2ScanTask$TaskError;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/scan/activity/I2ScanActivity;->access$1300(Lepson/scan/activity/I2ScanActivity;Lepson/scan/i2lib/I2ScanTask$TaskError;)V

    return-void

    :cond_3
    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1108
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/scan/activity/I2ScanActivity$ImageConvertAndFinishTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

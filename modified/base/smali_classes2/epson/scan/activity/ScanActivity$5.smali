.class Lepson/scan/activity/ScanActivity$5;
.super Ljava/lang/Object;
.source "ScanActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/activity/ScanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/activity/ScanActivity;


# direct methods
.method constructor <init>(Lepson/scan/activity/ScanActivity;)V
    .locals 0

    .line 868
    iput-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 871
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2000(Lepson/scan/activity/ScanActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 875
    :cond_0
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    iget v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    .line 877
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    const/16 v0, 0x12c

    if-gt p1, v0, :cond_2

    iget-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    iget p1, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    iget-object v0, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    iget v0, v0, Lepson/scan/activity/ScanActivity;->totalScanned:I

    if-le p1, v0, :cond_1

    goto :goto_0

    .line 882
    :cond_1
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    iget v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    invoke-static {p1, v0}, Lepson/scan/activity/ScanActivity;->access$2100(Lepson/scan/activity/ScanActivity;I)V

    .line 883
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$2200(Lepson/scan/activity/ScanActivity;)V

    .line 884
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    invoke-static {p1}, Lepson/scan/activity/ScanActivity;->access$100(Lepson/scan/activity/ScanActivity;)Lepson/scan/activity/ScanBaseView$ScanAreaSet;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/activity/ScanBaseView$ScanAreaSet;->postInvalidate()V

    return-void

    .line 878
    :cond_2
    :goto_0
    iget-object p1, p0, Lepson/scan/activity/ScanActivity$5;->this$0:Lepson/scan/activity/ScanActivity;

    iget v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Lepson/scan/activity/ScanActivity;->curentViewingFile:I

    return-void
.end method

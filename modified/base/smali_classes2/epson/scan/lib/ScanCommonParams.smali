.class public abstract Lepson/scan/lib/ScanCommonParams;
.super Ljava/lang/Object;
.source "ScanCommonParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;,
        Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertInputUnit(I)Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    .line 61
    sget-object p0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    return-object p0

    .line 59
    :cond_0
    sget-object p0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    return-object p0
.end method

.method public static convertYesNo(I)Z
    .locals 1

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams;
    .locals 2

    .line 21
    invoke-static {p0}, Lepson/scan/lib/ScanInfoStorage;->loadEscIVersion(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 27
    invoke-static {p0}, Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;->load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;

    move-result-object p0

    return-object p0

    .line 25
    :cond_0
    invoke-static {p0}, Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;->load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getI2Duplex()Z
    .locals 1

    .line 48
    invoke-virtual {p0}, Lepson/scan/lib/ScanCommonParams;->getTwoSideValue()I

    move-result v0

    invoke-static {v0}, Lepson/scan/lib/ScanCommonParams;->convertYesNo(I)Z

    move-result v0

    return v0
.end method

.method public getI2ScanSourceUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lepson/scan/lib/ScanCommonParams;->getScanSourceUnit()I

    move-result v0

    invoke-static {v0}, Lepson/scan/lib/ScanCommonParams;->convertInputUnit(I)Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object v0

    return-object v0
.end method

.method public abstract getScanSourceUnit()I
.end method

.method public abstract getTwoSideValue()I
.end method

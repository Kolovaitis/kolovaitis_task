.class public Lepson/scan/lib/escanLib;
.super Ljava/lang/Object;
.source "escanLib.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lepson/scan/lib/EscanLibInterface;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/scan/lib/escanLib;",
            ">;"
        }
    .end annotation
.end field

.field private static escanJobAttrib:[I

.field private static escanStatus:[I


# instance fields
.field volatile bInvalidId:Z

.field private cachePath:Ljava/lang/String;

.field private debugString:Ljava/lang/String;

.field error:I

.field private escanSupportOption:[I

.field private escanSupportResolution:[I

.field private isAdfRotate:Z

.field private isCanselOK:Z

.field private isJobDone:Z

.field private isScanning:Z

.field private listFoundScanner:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/scan/lib/ScannerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private listScannedFile:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEscIVersionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsSearch:Z

.field private offsetX:I

.field private offsetY:I

.field private printerList:Ljava/lang/String;

.field private scanAreaHeight:I

.field private scanAreaWidth:I

.field private scanHandler:Landroid/os/Handler;

.field private scanner:Ljava/lang/String;

.field volatile scannerId:Ljava/lang/String;

.field volatile scannerIp:Ljava/lang/String;

.field si:Lepson/scan/lib/ScannerInfo;

.field status:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "escan"

    .line 76
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 548
    new-instance v0, Lepson/scan/lib/escanLib$1;

    invoke-direct {v0}, Lepson/scan/lib/escanLib$1;-><init>()V

    sput-object v0, Lepson/scan/lib/escanLib;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 28
    iput-object v0, p0, Lepson/scan/lib/escanLib;->scanner:Ljava/lang/String;

    const/4 v0, 0x3

    .line 34
    new-array v0, v0, [I

    iput-object v0, p0, Lepson/scan/lib/escanLib;->status:[I

    const/4 v0, 0x0

    .line 36
    iput-boolean v0, p0, Lepson/scan/lib/escanLib;->isAdfRotate:Z

    .line 47
    iput-boolean v0, p0, Lepson/scan/lib/escanLib;->isCanselOK:Z

    .line 58
    iput-boolean v0, p0, Lepson/scan/lib/escanLib;->bInvalidId:Z

    const-string v1, ""

    .line 78
    iput-object v1, p0, Lepson/scan/lib/escanLib;->debugString:Ljava/lang/String;

    const-string v1, ""

    .line 79
    iput-object v1, p0, Lepson/scan/lib/escanLib;->printerList:Ljava/lang/String;

    const/4 v1, 0x0

    .line 81
    iput-object v1, p0, Lepson/scan/lib/escanLib;->cachePath:Ljava/lang/String;

    .line 64
    iput v0, p0, Lepson/scan/lib/escanLib;->error:I

    const/4 v0, 0x1

    .line 65
    iput-boolean v0, p0, Lepson/scan/lib/escanLib;->isJobDone:Z

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/lib/escanLib;->listScannedFile:Ljava/util/List;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/lib/escanLib;->mEscIVersionList:Ljava/util/ArrayList;

    return-void
.end method

.method private endPageCB()V
    .locals 2

    .line 259
    iget-object v0, p0, Lepson/scan/lib/escanLib;->scanHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/16 v1, 0xb

    .line 260
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const/4 v0, 0x0

    .line 262
    iput-boolean v0, p0, Lepson/scan/lib/escanLib;->isCanselOK:Z

    return-void
.end method

.method private native escanWrapperInitDriver()I
.end method

.method private extractEscVersion(Ljava/lang/String;)I
    .locals 2

    const/4 v0, -0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    .line 237
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    packed-switch p1, :pswitch_data_0

    return v0

    :pswitch_0
    const/4 p1, 0x2

    return p1

    :pswitch_1
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private findScannerCB()V
    .locals 8

    .line 152
    iget-object v0, p0, Lepson/scan/lib/escanLib;->scanner:Ljava/lang/String;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 153
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EXTRACT Scanner information: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->scanner:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lepson/scan/lib/escanLib;->scanner:Ljava/lang/String;

    const-string v1, "\\|\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 155
    new-instance v1, Lepson/scan/lib/ScannerInfo;

    invoke-direct {v1}, Lepson/scan/lib/ScannerInfo;-><init>()V

    iput-object v1, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    .line 157
    iget-object v1, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    const/4 v2, 0x1

    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Lepson/scan/lib/ScannerInfo;->setModelName(Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    const/4 v3, 0x2

    aget-object v4, v0, v3

    invoke-virtual {v1, v4}, Lepson/scan/lib/ScannerInfo;->setIp(Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    const/4 v4, 0x3

    aget-object v4, v0, v4

    invoke-virtual {v1, v4}, Lepson/scan/lib/ScannerInfo;->setFriendlyName(Ljava/lang/String;)V

    .line 163
    iget-object v1, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    const/4 v4, 0x4

    aget-object v5, v0, v4

    invoke-virtual {v1, v5}, Lepson/scan/lib/ScannerInfo;->setScannerId(Ljava/lang/String;)V

    .line 165
    array-length v1, v0

    const/4 v5, 0x6

    if-le v1, v5, :cond_0

    .line 166
    iget-object v1, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    aget-object v6, v0, v5

    invoke-virtual {v1, v6}, Lepson/scan/lib/ScannerInfo;->setCommonDeviceName(Ljava/lang/String;)V

    goto :goto_0

    .line 168
    :cond_0
    iget-object v1, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lepson/scan/lib/ScannerInfo;->setCommonDeviceName(Ljava/lang/String;)V

    .line 171
    :goto_0
    iget-object v1, p0, Lepson/scan/lib/escanLib;->scannerIp:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lepson/scan/lib/escanLib;->scannerId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 173
    iget-object v1, p0, Lepson/scan/lib/escanLib;->scannerIp:Ljava/lang/String;

    iget-object v6, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v6}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lepson/scan/lib/escanLib;->scannerId:Ljava/lang/String;

    iget-object v6, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v6}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 174
    iput-boolean v2, p0, Lepson/scan/lib/escanLib;->bInvalidId:Z

    .line 175
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid IP Printer: Expected = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->scannerId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 180
    :cond_1
    iget-object v1, p0, Lepson/scan/lib/escanLib;->listFoundScanner:Ljava/util/List;

    if-nez v1, :cond_2

    .line 181
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepson/scan/lib/escanLib;->listFoundScanner:Ljava/util/List;

    .line 182
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepson/scan/lib/escanLib;->mEscIVersionList:Ljava/util/ArrayList;

    .line 186
    :cond_2
    iget-object v1, p0, Lepson/scan/lib/escanLib;->listFoundScanner:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 187
    iget-object v1, p0, Lepson/scan/lib/escanLib;->listFoundScanner:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lepson/scan/lib/ScannerInfo;

    .line 188
    invoke-virtual {v6}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v7}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    return-void

    .line 194
    :cond_4
    iget-boolean v1, p0, Lepson/scan/lib/escanLib;->mIsSearch:Z

    if-eqz v1, :cond_6

    .line 201
    iget-object v1, p0, Lepson/scan/lib/escanLib;->scanHandler:Landroid/os/Handler;

    if-eqz v1, :cond_6

    .line 202
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const/16 v6, 0x8

    .line 203
    iput v6, v1, Landroid/os/Message;->what:I

    .line 204
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v7, "name"

    .line 205
    aget-object v2, v0, v2

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ip"

    .line 206
    aget-object v3, v0, v3

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "id"

    .line 207
    aget-object v3, v0, v4

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    array-length v2, v0

    if-le v2, v5, :cond_5

    const-string v2, "commonDeviceName"

    .line 209
    aget-object v3, v0, v5

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_5
    invoke-virtual {v1, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 215
    iget-object v2, p0, Lepson/scan/lib/escanLib;->scanHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 219
    :cond_6
    iget-object v1, p0, Lepson/scan/lib/escanLib;->listFoundScanner:Ljava/util/List;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->si:Lepson/scan/lib/ScannerInfo;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    iget-object v1, p0, Lepson/scan/lib/escanLib;->mEscIVersionList:Ljava/util/ArrayList;

    const/4 v2, 0x5

    aget-object v0, v0, v2

    invoke-direct {p0, v0}, Lepson/scan/lib/escanLib;->extractEscVersion(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_1
    const-string v0, ""

    .line 223
    iput-object v0, p0, Lepson/scan/lib/escanLib;->scanner:Ljava/lang/String;

    return-void
.end method

.method private javaDebugCB(Ljava/lang/String;)V
    .locals 1

    .line 266
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private newFileNotifyCB(Ljava/lang/String;)V
    .locals 3

    .line 271
    iget-object v0, p0, Lepson/scan/lib/escanLib;->listScannedFile:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    iget-object v0, p0, Lepson/scan/lib/escanLib;->mEscIVersionList:Ljava/util/ArrayList;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "newFileNotifyCB() call, Scan new file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", totals: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lepson/scan/lib/escanLib;->listScannedFile:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 274
    iput-boolean p1, p0, Lepson/scan/lib/escanLib;->isCanselOK:Z

    return-void
.end method

.method private recvScanImageBlockCB()V
    .locals 2

    .line 253
    iget-object v0, p0, Lepson/scan/lib/escanLib;->scanHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/16 v1, 0xa

    .line 254
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method private updateStatusCB([I)V
    .locals 3

    .line 142
    iput-object p1, p0, Lepson/scan/lib/escanLib;->status:[I

    const-string p1, "escanLib.java"

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ESCAN SCAN status = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/scan/lib/escanLib;->status:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/scan/lib/escanLib;->status:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/scan/lib/escanLib;->status:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public native BMPAppendRaster([C[CI)I
.end method

.method public native BMPClose([C)I
.end method

.method public native BMPCreate([CIII)I
.end method

.method public GetSupportedOption()I
    .locals 4

    const/4 v0, 0x7

    .line 390
    new-array v0, v0, [I

    iput-object v0, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    const/16 v0, 0x40

    .line 391
    new-array v0, v0, [I

    iput-object v0, p0, Lepson/scan/lib/escanLib;->escanSupportResolution:[I

    .line 392
    iget-object v0, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    iget-object v1, p0, Lepson/scan/lib/escanLib;->escanSupportResolution:[I

    invoke-virtual {p0, v0, v1}, Lepson/scan/lib/escanLib;->escanWrapperGetSupportedOption([I[I)I

    move-result v0

    iput v0, p0, Lepson/scan/lib/escanLib;->error:I

    const-string v0, "epson.print.escanLib"

    .line 394
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSupportedOption: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    const/4 v3, 0x3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    const/4 v3, 0x4

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    const/4 v3, 0x5

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    const/4 v3, 0x6

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    iget v0, p0, Lepson/scan/lib/escanLib;->error:I

    return v0
.end method

.method public cancelScanJob()I
    .locals 1

    .line 384
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->escanWrapperCancelJob()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/escanLib;->error:I

    .line 386
    iget v0, p0, Lepson/scan/lib/escanLib;->error:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public native doScan(IIIZI)I
.end method

.method public native escanProbeScanner(II[CI)I
.end method

.method public native escanProbeScannerById(I[C[C)I
.end method

.method public native escanWrapperCancelFindScanner()I
.end method

.method public native escanWrapperCancelJob()I
.end method

.method public native escanWrapperEndJob()I
.end method

.method public native escanWrapperEndPage(I)I
.end method

.method public native escanWrapperFindScanner(I)I
.end method

.method public native escanWrapperGetScanningImageInfo([I)I
.end method

.method public native escanWrapperGetStatus([I)I
.end method

.method public native escanWrapperGetSupportedOption([I[I)I
.end method

.method public escanWrapperInitDriver(Landroid/content/Context;)I
    .locals 2

    .line 95
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object p1

    const-string v1, "escan.cache"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/lib/escanLib;->cachePath:Ljava/lang/String;

    .line 97
    invoke-direct {p0}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver()I

    move-result p1

    return p1
.end method

.method public native escanWrapperRecvScanImageBlock([I)I
.end method

.method public native escanWrapperReleaseDriver()I
.end method

.method public native escanWrapperSetImageName(Ljava/lang/String;)I
.end method

.method public native escanWrapperSetScanner(I)I
.end method

.method public native escanWrapperSetWaitingConfirmFlag(Z)I
.end method

.method public native escanWrapperStartJob([I)I
.end method

.method public native escanWrapperStartPage()I
.end method

.method public getError()I
    .locals 1

    .line 485
    iget v0, p0, Lepson/scan/lib/escanLib;->error:I

    return v0
.end method

.method public getEscIVersion(I)I
    .locals 1

    if-ltz p1, :cond_1

    .line 582
    iget-object v0, p0, Lepson/scan/lib/escanLib;->mEscIVersionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 585
    :cond_0
    iget-object v0, p0, Lepson/scan/lib/escanLib;->mEscIVersionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, -0x1

    return p1
.end method

.method public getEscanSupportOption()[I
    .locals 1

    .line 501
    iget-object v0, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    return-object v0
.end method

.method public getEscanSupportResolution()[I
    .locals 1

    .line 509
    iget-object v0, p0, Lepson/scan/lib/escanLib;->escanSupportResolution:[I

    return-object v0
.end method

.method public getListFoundScanner()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lepson/scan/lib/ScannerInfo;",
            ">;"
        }
    .end annotation

    .line 564
    iget-object v0, p0, Lepson/scan/lib/escanLib;->listFoundScanner:Ljava/util/List;

    return-object v0
.end method

.method public getListScannedFile()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 570
    iget-object v0, p0, Lepson/scan/lib/escanLib;->listScannedFile:Ljava/util/List;

    return-object v0
.end method

.method public getOffsetX()I
    .locals 1

    .line 517
    iget v0, p0, Lepson/scan/lib/escanLib;->offsetX:I

    return v0
.end method

.method public getOffsetY()I
    .locals 1

    .line 525
    iget v0, p0, Lepson/scan/lib/escanLib;->offsetY:I

    return v0
.end method

.method public getScanAreaHeight()I
    .locals 1

    .line 541
    iget v0, p0, Lepson/scan/lib/escanLib;->scanAreaHeight:I

    return v0
.end method

.method public getScanAreaWidth()I
    .locals 1

    .line 533
    iget v0, p0, Lepson/scan/lib/escanLib;->scanAreaWidth:I

    return v0
.end method

.method public getScanHandler()Landroid/os/Handler;
    .locals 1

    .line 608
    iget-object v0, p0, Lepson/scan/lib/escanLib;->scanHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getScanParams()Lepson/scan/lib/I1ScanParams;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 661
    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    array-length v0, v0

    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    goto :goto_1

    .line 666
    :cond_0
    new-instance v0, Lepson/scan/lib/I1ScanParams;

    invoke-direct {v0}, Lepson/scan/lib/I1ScanParams;-><init>()V

    .line 668
    sget-object v2, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/16 v3, 0x8

    aget v2, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x1

    packed-switch v2, :pswitch_data_0

    return-object v1

    .line 670
    :pswitch_0
    iput v4, v0, Lepson/scan/lib/I1ScanParams;->inputDevice:I

    .line 671
    iput v4, v0, Lepson/scan/lib/I1ScanParams;->twoSide:I

    goto :goto_0

    .line 674
    :pswitch_1
    iput v4, v0, Lepson/scan/lib/I1ScanParams;->inputDevice:I

    .line 675
    iput v3, v0, Lepson/scan/lib/I1ScanParams;->twoSide:I

    goto :goto_0

    .line 678
    :pswitch_2
    iput v3, v0, Lepson/scan/lib/I1ScanParams;->inputDevice:I

    .line 679
    iput v3, v0, Lepson/scan/lib/I1ScanParams;->twoSide:I

    .line 686
    :goto_0
    sget-object v1, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v2, 0x7

    aget v2, v1, v2

    iput v2, v0, Lepson/scan/lib/I1ScanParams;->colorType:I

    .line 687
    aget v2, v1, v4

    iput v2, v0, Lepson/scan/lib/I1ScanParams;->resolution:I

    const/16 v2, 0xa

    .line 688
    aget v2, v1, v2

    iput v2, v0, Lepson/scan/lib/I1ScanParams;->gamma:I

    const/16 v2, 0xb

    .line 689
    aget v1, v1, v2

    rsub-int v1, v1, 0xff

    iput v1, v0, Lepson/scan/lib/I1ScanParams;->brightness:I

    return-object v0

    :cond_1
    :goto_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSearchStt()Z
    .locals 1

    .line 619
    iget-boolean v0, p0, Lepson/scan/lib/escanLib;->mIsSearch:Z

    return v0
.end method

.method public getStatus()[I
    .locals 1

    .line 596
    iget-object v0, p0, Lepson/scan/lib/escanLib;->status:[I

    return-object v0
.end method

.method public isCanselOK()Z
    .locals 1

    .line 603
    iget-boolean v0, p0, Lepson/scan/lib/escanLib;->isCanselOK:Z

    return v0
.end method

.method public isJobDone()Z
    .locals 1

    .line 497
    iget-boolean v0, p0, Lepson/scan/lib/escanLib;->isJobDone:Z

    return v0
.end method

.method public isScanning()Z
    .locals 1

    .line 589
    iget-boolean v0, p0, Lepson/scan/lib/escanLib;->isScanning:Z

    return v0
.end method

.method public makeJobAttribute(Landroid/content/Context;Lcom/epson/iprint/shared/SharedParamScan;)V
    .locals 6

    const/16 v0, 0xd

    .line 405
    new-array v0, v0, [I

    sput-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    .line 408
    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const-string v1, "epson.scanner.supported.Options"

    const-string v2, "SCAN_REFS_OPTIONS_VERSION"

    invoke-static {p1, v1, v2}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 410
    invoke-virtual {p0, p1, p2}, Lepson/scan/lib/escanLib;->setScanArea(Landroid/content/Context;Lcom/epson/iprint/shared/SharedParamScan;)V

    .line 413
    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getScan_type()I

    move-result p2

    goto :goto_0

    :cond_0
    const-string p2, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_COLOR"

    invoke-static {p1, p2, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    :goto_0
    const/4 v1, 0x7

    aput p2, v0, v1

    const-string p2, "epson.scanner.SelectedScanner"

    const-string v0, "SCAN_REFS_SETTINGS_2SIDED"

    .line 417
    invoke-static {p1, p2, v0}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    const/4 v0, 0x2

    const/16 v1, 0x8

    const/4 v3, 0x1

    if-ne p2, v3, :cond_1

    const-string p2, "epson.scanner.SelectedScanner"

    const-string v4, "SCAN_REFS_SETTINGS_SOURCE"

    .line 418
    invoke-static {p1, p2, v4}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    if-ne p2, v3, :cond_1

    .line 420
    sget-object p2, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    aput v0, p2, v1

    goto :goto_1

    :cond_1
    const-string p2, "epson.scanner.SelectedScanner"

    const-string v4, "SCAN_REFS_SETTINGS_2SIDED"

    .line 422
    invoke-static {p1, p2, v4}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    if-nez p2, :cond_2

    const-string p2, "epson.scanner.SelectedScanner"

    const-string v4, "SCAN_REFS_SETTINGS_SOURCE"

    .line 423
    invoke-static {p1, p2, v4}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    if-ne p2, v3, :cond_2

    .line 425
    sget-object p2, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    aput v3, p2, v1

    goto :goto_1

    .line 428
    :cond_2
    sget-object p2, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    aput v2, p2, v1

    .line 432
    :goto_1
    sget-object p2, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/16 v4, 0x9

    aput v3, p2, v4

    const/16 v3, 0xa

    const-string v4, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SETTINGS_GAMMA"

    .line 435
    invoke-static {p1, v4, v5}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    aput v4, p2, v3

    .line 438
    sget-object p2, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/16 v3, 0xb

    const-string v4, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SETTINGS_DENSITY_NAME"

    invoke-static {p1, v4, v5}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    aput v4, p2, v3

    .line 441
    sget-object p2, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    aget p2, p2, v1

    if-ne p2, v0, :cond_3

    const-string p2, "epson.scanner.SelectedScanner"

    const-string v0, "SCAN_REFS_SETTINGS_ROTATE"

    .line 442
    invoke-static {p1, p2, v0}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p2

    iput-boolean p2, p0, Lepson/scan/lib/escanLib;->isAdfRotate:Z

    goto :goto_2

    .line 444
    :cond_3
    iput-boolean v2, p0, Lepson/scan/lib/escanLib;->isAdfRotate:Z

    .line 448
    :goto_2
    sget-object p2, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_ADF_PAPER_GUIDE"

    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    const/16 v0, 0xc

    aput p1, p2, v0

    const-string p1, "makeJobAttribute"

    .line 450
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "**** PAPER_GUIDE =  "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    aget v0, v1, v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public probeByIpAndGetScannerInfo(Ljava/lang/String;Ljava/lang/String;)Lepson/scan/lib/ScannerInfo;
    .locals 2

    .line 633
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->resetEscanLib()V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 636
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, v0

    .line 638
    :goto_0
    invoke-virtual {p0, p1, p2}, Lepson/scan/lib/escanLib;->probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_1

    return-object v0

    .line 643
    :cond_1
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->getListFoundScanner()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_2

    return-object v0

    :cond_2
    const/4 p2, 0x0

    .line 648
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/scan/lib/ScannerInfo;

    return-object p1
.end method

.method public probeScannerById(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .line 306
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->resetEscanLib()V

    .line 307
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->resetIPAdressCheck()V

    .line 309
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    const/16 v0, 0x78

    invoke-virtual {p0, v0, p1, p2}, Lepson/scan/lib/escanLib;->escanProbeScannerById(I[C[C)I

    move-result p1

    const-string p2, "EPSON escanLib"

    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "scanners.probeScannerById() error = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return p1
.end method

.method public probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .line 320
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->resetEscanLib()V

    .line 323
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->resetIPAdressCheck()V

    .line 324
    iput-object p1, p0, Lepson/scan/lib/escanLib;->scannerId:Ljava/lang/String;

    .line 325
    iput-object p2, p0, Lepson/scan/lib/escanLib;->scannerIp:Ljava/lang/String;

    .line 327
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    array-length p2, p2

    const/16 v0, 0x78

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p1, p2}, Lepson/scan/lib/escanLib;->escanProbeScanner(II[CI)I

    move-result p1

    if-nez p1, :cond_0

    .line 330
    iget-boolean p2, p0, Lepson/scan/lib/escanLib;->bInvalidId:Z

    if-eqz p2, :cond_0

    const p1, -0x7a121

    :cond_0
    return p1
.end method

.method public resetEscanLib()V
    .locals 1

    const/4 v0, 0x0

    .line 287
    iput-boolean v0, p0, Lepson/scan/lib/escanLib;->isJobDone:Z

    .line 288
    iput v0, p0, Lepson/scan/lib/escanLib;->error:I

    const/4 v0, 0x3

    .line 289
    new-array v0, v0, [I

    iput-object v0, p0, Lepson/scan/lib/escanLib;->status:[I

    .line 290
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/scan/lib/escanLib;->listFoundScanner:Ljava/util/List;

    .line 295
    iget-object v0, p0, Lepson/scan/lib/escanLib;->mEscIVersionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public resetIPAdressCheck()V
    .locals 1

    const/4 v0, 0x0

    .line 339
    iput-object v0, p0, Lepson/scan/lib/escanLib;->scannerId:Ljava/lang/String;

    .line 340
    iput-object v0, p0, Lepson/scan/lib/escanLib;->scannerId:Ljava/lang/String;

    const/4 v0, 0x0

    .line 341
    iput-boolean v0, p0, Lepson/scan/lib/escanLib;->bInvalidId:Z

    return-void
.end method

.method public setError(I)V
    .locals 0

    .line 489
    iput p1, p0, Lepson/scan/lib/escanLib;->error:I

    return-void
.end method

.method public setEscanSupportOption([I)V
    .locals 0

    .line 505
    iput-object p1, p0, Lepson/scan/lib/escanLib;->escanSupportOption:[I

    return-void
.end method

.method public setEscanSupportResolution([I)V
    .locals 0

    .line 513
    iput-object p1, p0, Lepson/scan/lib/escanLib;->escanSupportResolution:[I

    return-void
.end method

.method public setJobDone(Z)V
    .locals 0

    .line 493
    iput-boolean p1, p0, Lepson/scan/lib/escanLib;->isJobDone:Z

    return-void
.end method

.method public setListFoundScanner(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lepson/scan/lib/ScannerInfo;",
            ">;)V"
        }
    .end annotation

    .line 567
    iput-object p1, p0, Lepson/scan/lib/escanLib;->listFoundScanner:Ljava/util/List;

    return-void
.end method

.method public setListScannedFile(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 573
    iput-object p1, p0, Lepson/scan/lib/escanLib;->listScannedFile:Ljava/util/List;

    return-void
.end method

.method public setOffsetX(I)V
    .locals 0

    .line 521
    iput p1, p0, Lepson/scan/lib/escanLib;->offsetX:I

    return-void
.end method

.method public setOffsetY(I)V
    .locals 0

    .line 529
    iput p1, p0, Lepson/scan/lib/escanLib;->offsetY:I

    return-void
.end method

.method public setScanArea(Landroid/content/Context;Lcom/epson/iprint/shared/SharedParamScan;)V
    .locals 4

    .line 455
    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_main()I

    move-result p1

    goto :goto_0

    :cond_0
    const-string v1, "epson.scanner.SelectedScanner"

    const-string v2, "SCAN_REFS_SETTINGS_RESOLUTION"

    .line 456
    invoke-static {p1, v1, v2}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    :goto_0
    const/4 v1, 0x1

    aput p1, v0, v1

    .line 461
    sget-object p1, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v0, 0x2

    iget v2, p0, Lepson/scan/lib/escanLib;->offsetX:I

    aput v2, p1, v0

    const/4 v0, 0x3

    .line 462
    iget v2, p0, Lepson/scan/lib/escanLib;->offsetY:I

    aput v2, p1, v0

    .line 464
    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_main()I

    move-result v0

    goto :goto_1

    :cond_1
    iget v0, p0, Lepson/scan/lib/escanLib;->scanAreaWidth:I

    :goto_1
    const/4 v2, 0x4

    aput v0, p1, v2

    .line 465
    sget-object p1, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v0, 0x5

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_sub()I

    move-result p2

    goto :goto_2

    :cond_2
    iget p2, p0, Lepson/scan/lib/escanLib;->scanAreaHeight:I

    :goto_2
    aput p2, p1, v0

    .line 468
    sget-object p1, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    aget p2, p1, v1

    const/16 v0, 0x4b

    const/4 v1, 0x6

    if-eq p2, v0, :cond_4

    const/16 v0, 0x12c

    if-eq p2, v0, :cond_3

    const/16 p2, 0x8

    .line 477
    aput p2, p1, v1

    goto :goto_3

    .line 473
    :cond_3
    aput v2, p1, v1

    goto :goto_3

    :cond_4
    const/16 p2, 0x10

    .line 470
    aput p2, p1, v1

    :goto_3
    return-void
.end method

.method public setScanAreaHeight(I)V
    .locals 0

    .line 545
    iput p1, p0, Lepson/scan/lib/escanLib;->scanAreaHeight:I

    return-void
.end method

.method public setScanAreaWidth(I)V
    .locals 0

    .line 537
    iput p1, p0, Lepson/scan/lib/escanLib;->scanAreaWidth:I

    return-void
.end method

.method public setScanHandler(Landroid/os/Handler;)V
    .locals 0

    .line 612
    iput-object p1, p0, Lepson/scan/lib/escanLib;->scanHandler:Landroid/os/Handler;

    return-void
.end method

.method public setScanning(Z)V
    .locals 0

    .line 593
    iput-boolean p1, p0, Lepson/scan/lib/escanLib;->isScanning:Z

    return-void
.end method

.method public setSearchStt(Z)V
    .locals 0

    .line 616
    iput-boolean p1, p0, Lepson/scan/lib/escanLib;->mIsSearch:Z

    return-void
.end method

.method public setStatus([I)V
    .locals 0

    .line 599
    iput-object p1, p0, Lepson/scan/lib/escanLib;->status:[I

    return-void
.end method

.method public startScanJob(Ljava/lang/String;)I
    .locals 8

    const/4 v0, 0x1

    .line 350
    iput-boolean v0, p0, Lepson/scan/lib/escanLib;->isScanning:Z

    const/4 v1, 0x0

    .line 351
    iput-boolean v1, p0, Lepson/scan/lib/escanLib;->isCanselOK:Z

    .line 354
    invoke-virtual {p0, p1}, Lepson/scan/lib/escanLib;->escanWrapperSetImageName(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/scan/lib/escanLib;->error:I

    .line 355
    iget p1, p0, Lepson/scan/lib/escanLib;->error:I

    if-eqz p1, :cond_0

    const-string p1, "EPSON escanLib"

    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "scanners.escanWrapperSetImageName(scannedImageNamePath) error = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lepson/scan/lib/escanLib;->error:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget p1, p0, Lepson/scan/lib/escanLib;->error:I

    return p1

    :cond_0
    const-string p1, "EPSON escanLib"

    .line 360
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    aget v0, v3, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v3, 0x2

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v3, 0x3

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v3, 0x4

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v3, 0x5

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v3, 0x6

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/4 v3, 0x7

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/16 v3, 0x8

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/16 v3, 0x9

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/16 v3, 0xa

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/16 v3, 0xb

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    const/16 v3, 0xc

    aget v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    sget-object p1, Lepson/scan/lib/escanLib;->escanJobAttrib:[I

    invoke-virtual {p0, p1}, Lepson/scan/lib/escanLib;->escanWrapperStartJob([I)I

    move-result p1

    iput p1, p0, Lepson/scan/lib/escanLib;->error:I

    .line 366
    iget p1, p0, Lepson/scan/lib/escanLib;->error:I

    if-eqz p1, :cond_1

    const-string p1, "EPSON escanLib"

    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "scanners.escanWrapperStartJob(escanJobAttrib) error = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lepson/scan/lib/escanLib;->error:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    iget p1, p0, Lepson/scan/lib/escanLib;->error:I

    return p1

    :cond_1
    const-string p1, "**** isAdfRotate = "

    .line 371
    iget-boolean v0, p0, Lepson/scan/lib/escanLib;->isAdfRotate:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 374
    iget-boolean v6, p0, Lepson/scan/lib/escanLib;->isAdfRotate:Z

    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->getAvailableScanPageCount()I

    move-result v7

    move-object v2, p0

    invoke-virtual/range {v2 .. v7}, Lepson/scan/lib/escanLib;->doScan(IIIZI)I

    move-result p1

    iput p1, p0, Lepson/scan/lib/escanLib;->error:I

    .line 376
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->escanWrapperEndJob()I

    .line 378
    iput-boolean v1, p0, Lepson/scan/lib/escanLib;->isScanning:Z

    .line 379
    iget p1, p0, Lepson/scan/lib/escanLib;->error:I

    return p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method

.class public Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;
.super Lepson/scan/lib/ScanCommonParams;
.source "ScanCommonParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/lib/ScanCommonParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "I1ParamWrapper"
.end annotation


# instance fields
.field private mI1ScannerInfo:Lepson/scan/lib/ScannerInfo;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 94
    invoke-direct {p0}, Lepson/scan/lib/ScanCommonParams;-><init>()V

    .line 95
    invoke-static {}, Lepson/scan/lib/ScannerInfo;->getInstance()Lepson/scan/lib/ScannerInfo;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;->mI1ScannerInfo:Lepson/scan/lib/ScannerInfo;

    return-void
.end method

.method public static load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;
    .locals 4

    .line 83
    new-instance v0, Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;

    invoke-direct {v0}, Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;-><init>()V

    .line 84
    invoke-static {}, Lepson/scan/lib/ScanInfoStorage;->getInstance()Lepson/scan/lib/ScanInfoStorage;

    move-result-object v1

    .line 87
    iget-object v2, v0, Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;->mI1ScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v1, p0}, Lepson/scan/lib/ScanInfoStorage;->getScanSourceUnit(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 88
    iget-object v2, v0, Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;->mI1ScannerInfo:Lepson/scan/lib/ScannerInfo;

    .line 89
    invoke-virtual {v1, p0}, Lepson/scan/lib/ScanInfoStorage;->getTwoSideValue(Landroid/content/Context;)I

    move-result p0

    .line 88
    invoke-virtual {v2, p0}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    return-object v0
.end method


# virtual methods
.method public getScanSourceUnit()I
    .locals 1

    .line 99
    iget-object v0, p0, Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;->mI1ScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v0

    return v0
.end method

.method public getTwoSideValue()I
    .locals 1

    .line 103
    iget-object v0, p0, Lepson/scan/lib/ScanCommonParams$I1ParamWrapper;->mI1ScannerInfo:Lepson/scan/lib/ScannerInfo;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result v0

    return v0
.end method

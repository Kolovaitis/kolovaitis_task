.class public Lepson/scan/lib/CancelableFind;
.super Ljava/lang/Object;
.source "CancelableFind.java"


# static fields
.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private volatile isFinishSearchScanner:Z

.field private volatile mCanceled:Z

.field private mEscanLib:Lepson/scan/lib/escanLib;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/scan/lib/CancelableFind;->sLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CFind"

    .line 16
    iput-object v0, p0, Lepson/scan/lib/CancelableFind;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lepson/scan/lib/CancelableFind;->isFinishSearchScanner:Z

    .line 22
    iput-boolean v0, p0, Lepson/scan/lib/CancelableFind;->mCanceled:Z

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .line 63
    iget-boolean v0, p0, Lepson/scan/lib/CancelableFind;->isFinishSearchScanner:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 66
    iput-boolean v0, p0, Lepson/scan/lib/CancelableFind;->isFinishSearchScanner:Z

    .line 67
    iput-boolean v0, p0, Lepson/scan/lib/CancelableFind;->mCanceled:Z

    .line 69
    iget-object v0, p0, Lepson/scan/lib/CancelableFind;->mEscanLib:Lepson/scan/lib/escanLib;

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x0

    .line 73
    invoke-virtual {v0, v1}, Lepson/scan/lib/escanLib;->setSearchStt(Z)V

    .line 74
    iget-object v0, p0, Lepson/scan/lib/CancelableFind;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperCancelFindScanner()I

    return-void
.end method

.method public findScanner(Landroid/content/Context;Landroid/os/Handler;)I
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Handler;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 40
    iget-boolean p1, p0, Lepson/scan/lib/CancelableFind;->mCanceled:Z

    if-eqz p1, :cond_0

    const/16 p1, 0x28

    return p1

    .line 43
    :cond_0
    sget-object p1, Lepson/scan/lib/CancelableFind;->sLock:Ljava/lang/Object;

    monitor-enter p1

    const/4 p2, 0x0

    .line 44
    :try_start_0
    iput-boolean p2, p0, Lepson/scan/lib/CancelableFind;->isFinishSearchScanner:Z

    .line 46
    iget-object v0, p0, Lepson/scan/lib/CancelableFind;->mEscanLib:Lepson/scan/lib/escanLib;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/scan/lib/escanLib;->setSearchStt(Z)V

    .line 47
    iget-object v0, p0, Lepson/scan/lib/CancelableFind;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->resetIPAdressCheck()V

    .line 48
    iget-object v0, p0, Lepson/scan/lib/CancelableFind;->mEscanLib:Lepson/scan/lib/escanLib;

    const/16 v2, 0x3c

    invoke-virtual {v0, v2}, Lepson/scan/lib/escanLib;->escanWrapperFindScanner(I)I

    move-result v0

    .line 49
    iget-object v2, p0, Lepson/scan/lib/CancelableFind;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v2, p2}, Lepson/scan/lib/escanLib;->setSearchStt(Z)V

    .line 50
    iget-object p2, p0, Lepson/scan/lib/CancelableFind;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {p2, v1}, Lepson/scan/lib/escanLib;->setJobDone(Z)V

    .line 51
    iput-boolean v1, p0, Lepson/scan/lib/CancelableFind;->isFinishSearchScanner:Z

    const/16 p2, -0x41b

    if-eq v0, p2, :cond_1

    .line 58
    monitor-exit p1

    return v0

    .line 55
    :cond_1
    new-instance p2, Ljava/lang/IllegalStateException;

    const-string v0, "escanWrapperFindScanner() returns ESCAN_ERR_LIB_NOT_INITIALIZED"

    invoke-direct {p2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    :catchall_0
    move-exception p2

    .line 59
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p2
.end method

.method public isCanceled()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lepson/scan/lib/CancelableFind;->mCanceled:Z

    return v0
.end method

.method public setEscanLib(Lepson/scan/lib/escanLib;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lepson/scan/lib/CancelableFind;->mEscanLib:Lepson/scan/lib/escanLib;

    return-void
.end method

.class public Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;
.super Lepson/scan/lib/ScanCommonParams;
.source "ScanCommonParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/lib/ScanCommonParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "I2ParamWrapper"
.end annotation


# instance fields
.field private mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 119
    invoke-direct {p0}, Lepson/scan/lib/ScanCommonParams;-><init>()V

    .line 120
    new-instance v0, Lcom/epson/lib/escani2/ScanI2Params;

    invoke-direct {v0}, Lcom/epson/lib/escani2/ScanI2Params;-><init>()V

    iput-object v0, p0, Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;->mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

    return-void
.end method

.method public static load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;
    .locals 1

    .line 114
    new-instance v0, Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;

    invoke-direct {v0}, Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;-><init>()V

    .line 115
    invoke-static {p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object p0

    iput-object p0, v0, Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;->mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

    return-object v0
.end method


# virtual methods
.method public getScanSourceUnit()I
    .locals 2

    .line 124
    sget-object v0, Lepson/scan/lib/ScanCommonParams$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

    iget-object v1, p0, Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;->mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

    iget-object v1, v1, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v1}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getTwoSideValue()I
    .locals 1

    .line 132
    iget-object v0, p0, Lepson/scan/lib/ScanCommonParams$I2ParamWrapper;->mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

    iget-boolean v0, v0, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    return v0
.end method

.class public Lepson/scan/lib/CancelableScannerProbe;
.super Ljava/lang/Object;
.source "CancelableScannerProbe.java"


# instance fields
.field private mEscanLib:Lepson/scan/lib/escanLib;

.field private volatile mProbingScanner:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static localProbeById(Lepson/scan/lib/escanLib;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 171
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->resetEscanLib()V

    .line 172
    invoke-virtual {p0, p1, p2}, Lepson/scan/lib/escanLib;->probeScannerById(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    return-object p2

    .line 177
    :cond_0
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->getListFoundScanner()Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 178
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p1

    if-gtz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 182
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    :goto_0
    return-object p2
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 189
    monitor-enter p0

    .line 190
    :try_start_0
    iget-boolean v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperCancelFindScanner()I

    .line 195
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public probeScanner(Lepson/scan/activity/ScannerPropertyWrapper;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/scan/lib/EscanLibException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 105
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    const/4 v1, 0x0

    .line 107
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_9

    .line 108
    :try_start_1
    new-instance v2, Lepson/scan/lib/escanLib;

    invoke-direct {v2}, Lepson/scan/lib/escanLib;-><init>()V

    iput-object v2, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 109
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_8

    const/4 v2, 0x0

    .line 113
    :cond_0
    :try_start_2
    iget-object v3, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v3, p2}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    move-result v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    const/16 v4, -0x41a

    const/4 v5, 0x1

    if-eq v3, v4, :cond_3

    if-eqz v3, :cond_2

    .line 148
    monitor-enter p0

    .line 149
    :try_start_3
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 150
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 152
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p1, :cond_1

    if-eqz v2, :cond_1

    .line 153
    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 155
    :cond_1
    monitor-enter p0

    .line 156
    :try_start_4
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 157
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    .line 150
    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw p1

    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 120
    :cond_3
    :try_start_6
    iget-object v3, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v3}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :goto_0
    if-eqz v2, :cond_0

    .line 127
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getIp()Ljava/lang/String;

    move-result-object p2

    .line 128
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getScannerId()Ljava/lang/String;

    move-result-object v3

    .line 130
    monitor-enter p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    .line 131
    :try_start_7
    iput-boolean v5, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 132
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 135
    :try_start_8
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getLocation()I

    move-result p1

    const/4 v4, 0x3

    if-ne p1, v4, :cond_6

    .line 137
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {p1, v3, p2}, Lepson/scan/lib/escanLib;->probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_7

    if-nez p1, :cond_5

    .line 148
    monitor-enter p0

    .line 149
    :try_start_9
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 150
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 152
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p1, :cond_4

    if-eqz v2, :cond_4

    .line 153
    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 155
    :cond_4
    monitor-enter p0

    .line 156
    :try_start_a
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 157
    monitor-exit p0

    return-object p2

    :catchall_2
    move-exception p1

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw p1

    :catchall_3
    move-exception p1

    .line 150
    :try_start_b
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw p1

    .line 139
    :cond_5
    :try_start_c
    new-instance p2, Lepson/scan/lib/EscanLibException;

    invoke-direct {p2, p1}, Lepson/scan/lib/EscanLibException;-><init>(I)V

    throw p2

    .line 144
    :cond_6
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-static {p1, v3, p2}, Lepson/scan/lib/CancelableScannerProbe;->localProbeById(Lepson/scan/lib/escanLib;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_7

    .line 148
    monitor-enter p0

    .line 149
    :try_start_d
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 150
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 152
    iget-object p2, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p2, :cond_7

    if-eqz v2, :cond_7

    .line 153
    invoke-virtual {p2}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 155
    :cond_7
    monitor-enter p0

    .line 156
    :try_start_e
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 157
    monitor-exit p0

    return-object p1

    :catchall_4
    move-exception p1

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    throw p1

    :catchall_5
    move-exception p1

    .line 150
    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    throw p1

    :catchall_6
    move-exception p1

    .line 132
    :try_start_10
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    :try_start_11
    throw p1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    :catchall_7
    move-exception p1

    goto :goto_1

    :catchall_8
    move-exception p1

    .line 109
    :try_start_12
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    :try_start_13
    throw p1
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    :catchall_9
    move-exception p1

    const/4 v2, 0x0

    .line 148
    :goto_1
    monitor-enter p0

    .line 149
    :try_start_14
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 150
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_b

    .line 152
    iget-object p2, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p2, :cond_8

    if-eqz v2, :cond_8

    .line 153
    invoke-virtual {p2}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 155
    :cond_8
    monitor-enter p0

    .line 156
    :try_start_15
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 157
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_a

    throw p1

    :catchall_a
    move-exception p1

    :try_start_16
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    throw p1

    :catchall_b
    move-exception p1

    .line 150
    :try_start_17
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_b

    throw p1
.end method

.method public probeScanner_test(Lepson/scan/activity/ScannerPropertyWrapper;Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    .line 34
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    const/4 v1, 0x0

    .line 36
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_d

    .line 37
    :try_start_1
    new-instance v2, Lepson/scan/lib/escanLib;

    invoke-direct {v2}, Lepson/scan/lib/escanLib;-><init>()V

    iput-object v2, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 38
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_c

    const/4 v2, 0x0

    .line 42
    :cond_0
    :try_start_2
    iget-object v3, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v3, p2}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    move-result v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_b

    const/16 v4, -0x41a

    const/4 v5, 0x1

    if-eq v3, v4, :cond_3

    if-eqz v3, :cond_2

    .line 83
    monitor-enter p0

    .line 84
    :try_start_3
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 85
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 87
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p1, :cond_1

    if-eqz v2, :cond_1

    .line 88
    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 90
    :cond_1
    monitor-enter p0

    .line 91
    :try_start_4
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 92
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    .line 85
    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw p1

    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 49
    :cond_3
    :try_start_6
    iget-object v3, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v3}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :goto_0
    if-eqz v2, :cond_0

    .line 56
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getIp()Ljava/lang/String;

    move-result-object p2

    .line 57
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getScannerId()Ljava/lang/String;

    move-result-object v3

    .line 59
    monitor-enter p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_b

    .line 60
    :try_start_7
    iput-boolean v5, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 61
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_a

    if-eqz p2, :cond_9

    .line 63
    :try_start_8
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    .line 64
    iget-object v4, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v4, v3, p2}, Lepson/scan/lib/escanLib;->probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_b

    if-nez v4, :cond_5

    .line 83
    monitor-enter p0

    .line 84
    :try_start_9
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 85
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 87
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p1, :cond_4

    if-eqz v2, :cond_4

    .line 88
    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 90
    :cond_4
    monitor-enter p0

    .line 91
    :try_start_a
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 92
    monitor-exit p0

    return-object p2

    :catchall_2
    move-exception p1

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw p1

    :catchall_3
    move-exception p1

    .line 85
    :try_start_b
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw p1

    .line 69
    :cond_5
    :try_start_c
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getLocation()I

    move-result p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_b

    if-eq p1, v5, :cond_7

    .line 83
    monitor-enter p0

    .line 84
    :try_start_d
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 85
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 87
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p1, :cond_6

    if-eqz v2, :cond_6

    .line 88
    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 90
    :cond_6
    monitor-enter p0

    .line 91
    :try_start_e
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 92
    monitor-exit p0

    return-object v0

    :catchall_4
    move-exception p1

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    throw p1

    :catchall_5
    move-exception p1

    .line 85
    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    throw p1

    .line 77
    :cond_7
    :try_start_10
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-static {p1, v3, p2}, Lepson/scan/lib/CancelableScannerProbe;->localProbeById(Lepson/scan/lib/escanLib;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_b

    .line 83
    monitor-enter p0

    .line 84
    :try_start_11
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 85
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    .line 87
    iget-object p2, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p2, :cond_8

    if-eqz v2, :cond_8

    .line 88
    invoke-virtual {p2}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 90
    :cond_8
    monitor-enter p0

    .line 91
    :try_start_12
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 92
    monitor-exit p0

    return-object p1

    :catchall_6
    move-exception p1

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    throw p1

    :catchall_7
    move-exception p1

    .line 85
    :try_start_13
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_7

    throw p1

    .line 79
    :cond_9
    :try_start_14
    iget-object p1, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-static {p1, v3, p2}, Lepson/scan/lib/CancelableScannerProbe;->localProbeById(Lepson/scan/lib/escanLib;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_b

    .line 83
    monitor-enter p0

    .line 84
    :try_start_15
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 85
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_9

    .line 87
    iget-object p2, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p2, :cond_a

    if-eqz v2, :cond_a

    .line 88
    invoke-virtual {p2}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 90
    :cond_a
    monitor-enter p0

    .line 91
    :try_start_16
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 92
    monitor-exit p0

    return-object p1

    :catchall_8
    move-exception p1

    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_8

    throw p1

    :catchall_9
    move-exception p1

    .line 85
    :try_start_17
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_9

    throw p1

    :catchall_a
    move-exception p1

    .line 61
    :try_start_18
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_a

    :try_start_19
    throw p1
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_b

    :catchall_b
    move-exception p1

    goto :goto_1

    :catchall_c
    move-exception p1

    .line 38
    :try_start_1a
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_c

    :try_start_1b
    throw p1
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_d

    :catchall_d
    move-exception p1

    const/4 v2, 0x0

    .line 83
    :goto_1
    monitor-enter p0

    .line 84
    :try_start_1c
    iput-boolean v1, p0, Lepson/scan/lib/CancelableScannerProbe;->mProbingScanner:Z

    .line 85
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_f

    .line 87
    iget-object p2, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    if-eqz p2, :cond_b

    if-eqz v2, :cond_b

    .line 88
    invoke-virtual {p2}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 90
    :cond_b
    monitor-enter p0

    .line 91
    :try_start_1d
    iput-object v0, p0, Lepson/scan/lib/CancelableScannerProbe;->mEscanLib:Lepson/scan/lib/escanLib;

    .line 92
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_e

    throw p1

    :catchall_e
    move-exception p1

    :try_start_1e
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_e

    throw p1

    :catchall_f
    move-exception p1

    .line 85
    :try_start_1f
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_f

    throw p1
.end method

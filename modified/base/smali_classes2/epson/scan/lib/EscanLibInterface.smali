.class public interface abstract Lepson/scan/lib/EscanLibInterface;
.super Ljava/lang/Object;
.source "EscanLibInterface.java"

# interfaces
.implements Landroid/os/Parcelable;


# virtual methods
.method public abstract GetSupportedOption()I
.end method

.method public abstract escanProbeScannerById(I[C[C)I
.end method

.method public abstract escanWrapperCancelFindScanner()I
.end method

.method public abstract escanWrapperInitDriver(Landroid/content/Context;)I
.end method

.method public abstract escanWrapperReleaseDriver()I
.end method

.method public abstract escanWrapperSetScanner(I)I
.end method

.method public abstract getEscanSupportOption()[I
.end method

.method public abstract getEscanSupportResolution()[I
.end method

.method public abstract getListFoundScanner()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lepson/scan/lib/ScannerInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSearchStt()Z
.end method

.method public abstract getStatus()[I
.end method

.method public abstract probeScannerById(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract setEscanSupportOption([I)V
.end method

.method public abstract setSearchStt(Z)V
.end method

.class public Lepson/scan/lib/CancelablePropertyTaker;
.super Ljava/lang/Object;
.source "CancelablePropertyTaker.java"


# instance fields
.field private volatile mCancelRequested:Z

.field private mEscanLib:Lepson/scan/lib/escanLib;


# direct methods
.method public constructor <init>(Lepson/scan/lib/escanLib;)V
    .locals 0
    .param p1    # Lepson/scan/lib/escanLib;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lepson/scan/lib/CancelablePropertyTaker;->mEscanLib:Lepson/scan/lib/escanLib;

    return-void
.end method

.method public static getScannerProperty(Lepson/scan/lib/escanLib;Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/scan/lib/EscanLibException;
        }
    .end annotation

    .line 119
    new-instance v0, Lepson/scan/lib/CancelablePropertyTaker;

    invoke-direct {v0, p0}, Lepson/scan/lib/CancelablePropertyTaker;-><init>(Lepson/scan/lib/escanLib;)V

    .line 120
    invoke-virtual {v0, p1}, Lepson/scan/lib/CancelablePropertyTaker;->getScannerProperty(Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 113
    iput-boolean v0, p0, Lepson/scan/lib/CancelablePropertyTaker;->mCancelRequested:Z

    .line 114
    iget-object v0, p0, Lepson/scan/lib/CancelablePropertyTaker;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperCancelFindScanner()I

    return-void
.end method

.method public getScannerProperty(Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/scan/lib/EscanLibException;
        }
    .end annotation

    .line 38
    invoke-static {}, Lepson/scan/lib/ScannerInfo;->getInstance()Lepson/scan/lib/ScannerInfo;

    move-result-object v0

    .line 39
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setLocation(I)V

    .line 40
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setModelName(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getScannerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setScannerId(Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setIp(Ljava/lang/String;)V

    .line 44
    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v1, v2

    .line 54
    :cond_1
    iget-object v3, p0, Lepson/scan/lib/CancelablePropertyTaker;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v3}, Lepson/scan/lib/escanLib;->resetEscanLib()V

    .line 55
    iget-boolean v3, p0, Lepson/scan/lib/CancelablePropertyTaker;->mCancelRequested:Z

    if-eqz v3, :cond_2

    return-object v2

    .line 60
    :cond_2
    iget-object v3, p0, Lepson/scan/lib/CancelablePropertyTaker;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lepson/scan/lib/escanLib;->probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_b

    .line 65
    iget-boolean v1, p0, Lepson/scan/lib/CancelablePropertyTaker;->mCancelRequested:Z

    if-eqz v1, :cond_3

    return-object v2

    .line 69
    :cond_3
    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_5

    .line 70
    :cond_4
    iget-object v1, p0, Lepson/scan/lib/CancelablePropertyTaker;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->getListFoundScanner()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/scan/lib/ScannerInfo;

    .line 71
    invoke-virtual {v1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setScannerId(Ljava/lang/String;)V

    .line 76
    :cond_5
    iget-object v1, p0, Lepson/scan/lib/CancelablePropertyTaker;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-virtual {v1, v3}, Lepson/scan/lib/escanLib;->getEscIVersion(I)I

    move-result v1

    .line 79
    iget-boolean v3, p0, Lepson/scan/lib/CancelablePropertyTaker;->mCancelRequested:Z

    if-eqz v3, :cond_6

    return-object v2

    :cond_6
    const/4 v3, 0x2

    if-eq v1, v3, :cond_8

    .line 97
    iget-object p1, p0, Lepson/scan/lib/CancelablePropertyTaker;->mEscanLib:Lepson/scan/lib/escanLib;

    invoke-static {p1, v0}, Lepson/scan/lib/ScanSettingHelper;->getI1SupportedOptions(Lepson/scan/lib/escanLib;Lepson/scan/lib/ScannerInfo;)I

    move-result p1

    if-nez p1, :cond_7

    .line 102
    new-instance p1, Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-direct {p1, v0}, Lepson/scan/activity/ScannerPropertyWrapper;-><init>(Lepson/scan/lib/ScannerInfo;)V

    goto :goto_0

    .line 99
    :cond_7
    new-instance v0, Lepson/scan/lib/EscanLibException;

    invoke-direct {v0, p1}, Lepson/scan/lib/EscanLibException;-><init>(I)V

    throw v0

    .line 85
    :cond_8
    new-instance v1, Lcom/epson/lib/escani2/ScanI2PropertyTaker;

    invoke-direct {v1}, Lcom/epson/lib/escani2/ScanI2PropertyTaker;-><init>()V

    .line 88
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/epson/lib/escani2/ScanI2PropertyTaker;->getScannerInfoAndCapability(Ljava/lang/String;)Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object p1

    if-eqz p1, :cond_a

    .line 92
    new-instance v1, Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-direct {v1, v0, p1}, Lepson/scan/activity/ScannerPropertyWrapper;-><init>(Lepson/scan/lib/ScannerInfo;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    move-object p1, v1

    .line 105
    :goto_0
    iget-boolean v0, p0, Lepson/scan/lib/CancelablePropertyTaker;->mCancelRequested:Z

    if-eqz v0, :cond_9

    return-object v2

    :cond_9
    return-object p1

    .line 90
    :cond_a
    new-instance p1, Lepson/scan/lib/EscanLibException;

    invoke-virtual {v1}, Lcom/epson/lib/escani2/ScanI2PropertyTaker;->getEscanI2LibError()I

    move-result v0

    invoke-direct {p1, v3, v0}, Lepson/scan/lib/EscanLibException;-><init>(II)V

    throw p1

    .line 62
    :cond_b
    new-instance p1, Lepson/scan/lib/EscanLibException;

    invoke-direct {p1, v1}, Lepson/scan/lib/EscanLibException;-><init>(I)V

    throw p1
.end method

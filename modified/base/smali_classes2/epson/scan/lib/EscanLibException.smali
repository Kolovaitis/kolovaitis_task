.class public Lepson/scan/lib/EscanLibException;
.super Ljava/lang/Exception;
.source "EscanLibException.java"


# instance fields
.field private mEscIVersion:I

.field private mEscanLibErrorCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/4 v0, 0x1

    .line 15
    iput v0, p0, Lepson/scan/lib/EscanLibException;->mEscIVersion:I

    .line 16
    iput p1, p0, Lepson/scan/lib/EscanLibException;->mEscanLibErrorCode:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 20
    iput p1, p0, Lepson/scan/lib/EscanLibException;->mEscIVersion:I

    .line 21
    iput p2, p0, Lepson/scan/lib/EscanLibException;->mEscanLibErrorCode:I

    return-void
.end method


# virtual methods
.method public getEscIVersion()I
    .locals 1

    .line 29
    iget v0, p0, Lepson/scan/lib/EscanLibException;->mEscIVersion:I

    return v0
.end method

.method public getEscanLibErrorCode()I
    .locals 1

    .line 25
    iget v0, p0, Lepson/scan/lib/EscanLibException;->mEscanLibErrorCode:I

    return v0
.end method

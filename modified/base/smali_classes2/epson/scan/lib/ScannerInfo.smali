.class public Lepson/scan/lib/ScannerInfo;
.super Ljava/lang/Object;
.source "ScannerInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private adfPaperGuide:I

.field private colorValue:I

.field private commonDeviceName:Ljava/lang/String;

.field private densityStatus:Z

.field private densityValue:I

.field private friendlyName:Ljava/lang/String;

.field private gammaValue:I

.field private ip:Ljava/lang/String;

.field private isAdfDuplexRotaitonYes:Z

.field private isAdfDuplexYes:Z

.field private isReSearch:Z

.field private location:I

.field private maxHeight:I

.field private maxWidth:I

.field private modelName:Ljava/lang/String;

.field private resolutionValue:I

.field private scannerId:Ljava/lang/String;

.field private sizeValue:I

.field private sourceValue:I

.field private supportedAdfDuplex:I

.field private supportedAdfHeight:I

.field private supportedAdfWidth:I

.field private supportedBasicHeight:I

.field private supportedBasicResolution:I

.field private supportedBasicWidth:I

.field private supportedOptions:[I

.field private supportedResolutionList:[I

.field private supportedVersion:I

.field private twoSidedScanningValue:I

.field private version:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexYes:Z

    .line 58
    iput-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes:Z

    .line 60
    iput v0, p0, Lepson/scan/lib/ScannerInfo;->adfPaperGuide:I

    return-void
.end method

.method public constructor <init>(Lepson/scan/lib/ScannerInfo;)V
    .locals 1

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexYes:Z

    .line 58
    iput-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes:Z

    .line 60
    iput v0, p0, Lepson/scan/lib/ScannerInfo;->adfPaperGuide:I

    .line 69
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/lib/ScannerInfo;->modelName:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/lib/ScannerInfo;->ip:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getFriendlyName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/lib/ScannerInfo;->friendlyName:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/lib/ScannerInfo;->scannerId:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getColorValue()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->colorValue:I

    .line 78
    iget-boolean v0, p1, Lepson/scan/lib/ScannerInfo;->densityStatus:Z

    iput-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->densityStatus:Z

    .line 79
    iget v0, p1, Lepson/scan/lib/ScannerInfo;->densityValue:I

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->densityValue:I

    .line 82
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getGammaValue()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->gammaValue:I

    .line 84
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSizeValue()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->sizeValue:I

    .line 86
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->isReSearch()Z

    move-result v0

    iput-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isReSearch:Z

    .line 88
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getMaxHeight()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->maxHeight:I

    .line 89
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getMaxWidth()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->maxWidth:I

    .line 92
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getResolutionValue()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->resolutionValue:I

    .line 95
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->sourceValue:I

    .line 97
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfDuplex()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfDuplex:I

    .line 98
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfHeight:I

    .line 99
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfWidth:I

    .line 100
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicHeight()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicHeight:I

    .line 101
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicResolution()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicResolution:I

    .line 102
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicWidth()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicWidth:I

    .line 103
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    iput-object v0, p0, Lepson/scan/lib/ScannerInfo;->supportedOptions:[I

    .line 104
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedResolutionList()[I

    move-result-object v0

    iput-object v0, p0, Lepson/scan/lib/ScannerInfo;->supportedResolutionList:[I

    .line 105
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedVersion()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->supportedVersion:I

    .line 108
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result v0

    iput v0, p0, Lepson/scan/lib/ScannerInfo;->twoSidedScanningValue:I

    .line 110
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->isAdfDuplexYes()Z

    move-result v0

    iput-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexYes:Z

    .line 111
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes()Z

    move-result v0

    iput-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes:Z

    .line 113
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getAdfPaperGuide()I

    move-result p1

    iput p1, p0, Lepson/scan/lib/ScannerInfo;->adfPaperGuide:I

    return-void
.end method

.method public static getInstance()Lepson/scan/lib/ScannerInfo;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 440
    new-instance v0, Lepson/scan/lib/ScannerInfo;

    invoke-direct {v0}, Lepson/scan/lib/ScannerInfo;-><init>()V

    const-string v1, ""

    .line 441
    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setScannerId(Ljava/lang/String;)V

    const-string v1, ""

    .line 442
    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setIp(Ljava/lang/String;)V

    const-string v1, ""

    .line 443
    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setModelName(Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 444
    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setLocation(I)V

    const-string v1, ""

    .line 445
    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setCommonDeviceName(Ljava/lang/String;)V

    const/4 v1, 0x7

    .line 448
    new-array v1, v1, [I

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedOptions([I)V

    const/16 v1, 0x40

    .line 449
    new-array v1, v1, [I

    invoke-virtual {v0, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedResolutionList([I)V

    return-object v0
.end method


# virtual methods
.method public getAdfPaperGuide()I
    .locals 1

    .line 427
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->adfPaperGuide:I

    return v0
.end method

.method public getColorName()I
    .locals 1

    .line 204
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getColorValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0e0471

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0e0508

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0e0507

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0e0506

    :goto_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getColorValue()I
    .locals 1

    .line 228
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->colorValue:I

    return v0
.end method

.method public getCommonDeviceName()Ljava/lang/String;
    .locals 1

    .line 149
    iget-object v0, p0, Lepson/scan/lib/ScannerInfo;->commonDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDensityValue()I
    .locals 1

    .line 293
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->densityValue:I

    return v0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lepson/scan/lib/ScannerInfo;->friendlyName:Ljava/lang/String;

    return-object v0
.end method

.method public getGammaName()I
    .locals 1

    .line 309
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getGammaValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0e0471

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0e050b

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0e050a

    :goto_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getGammaValue()I
    .locals 1

    .line 329
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->gammaValue:I

    return v0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lepson/scan/lib/ScannerInfo;->ip:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()I
    .locals 1

    .line 159
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->location:I

    return v0
.end method

.method public getMaxHeight()I
    .locals 1

    .line 405
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->maxHeight:I

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    .line 399
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->maxWidth:I

    return v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lepson/scan/lib/ScannerInfo;->modelName:Ljava/lang/String;

    return-object v0
.end method

.method public getResolutionName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .line 183
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getResolutionValue()I

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0e0471

    .line 185
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const v1, 0x7f0e050e

    .line 188
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getResolutionValue()I
    .locals 1

    .line 194
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->resolutionValue:I

    return v0
.end method

.method public getScannerId()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lepson/scan/lib/ScannerInfo;->scannerId:Ljava/lang/String;

    return-object v0
.end method

.method public getSizeValue()I
    .locals 1

    .line 335
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->sizeValue:I

    return v0
.end method

.method public getSourceName()I
    .locals 1

    .line 238
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0e0471

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0e0511

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0e0512

    :goto_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSourceValue()I
    .locals 1

    .line 258
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->sourceValue:I

    return v0
.end method

.method public getSupportedAdfDuplex()I
    .locals 1

    .line 375
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfDuplex:I

    return v0
.end method

.method public getSupportedAdfHeight()I
    .locals 1

    .line 363
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfHeight:I

    return v0
.end method

.method public getSupportedAdfWidth()I
    .locals 1

    .line 369
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfWidth:I

    return v0
.end method

.method public getSupportedBasicHeight()I
    .locals 1

    .line 351
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicHeight:I

    return v0
.end method

.method public getSupportedBasicResolution()I
    .locals 1

    .line 345
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicResolution:I

    return v0
.end method

.method public getSupportedBasicWidth()I
    .locals 1

    .line 357
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicWidth:I

    return v0
.end method

.method public getSupportedOptions()[I
    .locals 1

    .line 168
    iget-object v0, p0, Lepson/scan/lib/ScannerInfo;->supportedOptions:[I

    return-object v0
.end method

.method public getSupportedResolutionList()[I
    .locals 1

    .line 383
    iget-object v0, p0, Lepson/scan/lib/ScannerInfo;->supportedResolutionList:[I

    return-object v0
.end method

.method public getSupportedVersion()I
    .locals 1

    .line 339
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->supportedVersion:I

    return v0
.end method

.method public getTwoSidedScanningName()I
    .locals 1

    .line 268
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0e0471

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0e052b

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0e04e6

    :goto_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getTwoSidedScanningValue()I
    .locals 1

    .line 287
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->twoSidedScanningValue:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    .line 175
    iget v0, p0, Lepson/scan/lib/ScannerInfo;->version:I

    return v0
.end method

.method public isAdfDuplexRotaitonYes()Z
    .locals 1

    .line 420
    iget-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes:Z

    return v0
.end method

.method public isAdfDuplexYes()Z
    .locals 1

    .line 412
    iget-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexYes:Z

    return v0
.end method

.method public isDensityStatus()Z
    .locals 1

    .line 299
    iget-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->densityStatus:Z

    return v0
.end method

.method public isModelNameEmpty()Z
    .locals 1

    .line 125
    iget-object v0, p0, Lepson/scan/lib/ScannerInfo;->modelName:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isReSearch()Z
    .locals 1

    .line 391
    iget-boolean v0, p0, Lepson/scan/lib/ScannerInfo;->isReSearch:Z

    return v0
.end method

.method public setAdfDuplexRotaitonYes(Z)V
    .locals 0

    .line 423
    iput-boolean p1, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes:Z

    return-void
.end method

.method public setAdfDuplexYes(Z)V
    .locals 0

    .line 415
    iput-boolean p1, p0, Lepson/scan/lib/ScannerInfo;->isAdfDuplexYes:Z

    return-void
.end method

.method public setAdfPaperGuide(I)V
    .locals 0

    .line 430
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->adfPaperGuide:I

    return-void
.end method

.method public setColorValue(I)V
    .locals 0

    .line 231
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->colorValue:I

    return-void
.end method

.method public setCommonDeviceName(Ljava/lang/String;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lepson/scan/lib/ScannerInfo;->commonDeviceName:Ljava/lang/String;

    return-void
.end method

.method public setDensityStatus(Z)V
    .locals 0

    .line 302
    iput-boolean p1, p0, Lepson/scan/lib/ScannerInfo;->densityStatus:Z

    return-void
.end method

.method public setDensityValue(I)V
    .locals 0

    .line 296
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->densityValue:I

    return-void
.end method

.method public setFriendlyName(Ljava/lang/String;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lepson/scan/lib/ScannerInfo;->friendlyName:Ljava/lang/String;

    return-void
.end method

.method public setGammaValue(I)V
    .locals 0

    .line 332
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->gammaValue:I

    return-void
.end method

.method public setIp(Ljava/lang/String;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lepson/scan/lib/ScannerInfo;->ip:Ljava/lang/String;

    return-void
.end method

.method public setLocation(I)V
    .locals 0

    .line 165
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->location:I

    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .line 408
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->maxHeight:I

    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0

    .line 402
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->maxWidth:I

    return-void
.end method

.method public setModelName(Ljava/lang/String;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lepson/scan/lib/ScannerInfo;->modelName:Ljava/lang/String;

    return-void
.end method

.method public setReSearch(Z)V
    .locals 0

    .line 394
    iput-boolean p1, p0, Lepson/scan/lib/ScannerInfo;->isReSearch:Z

    return-void
.end method

.method public setResolutionValue(I)V
    .locals 0

    .line 197
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->resolutionValue:I

    return-void
.end method

.method public setScannerId(Ljava/lang/String;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lepson/scan/lib/ScannerInfo;->scannerId:Ljava/lang/String;

    return-void
.end method

.method public setSizeValue(I)V
    .locals 0

    .line 336
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->sizeValue:I

    return-void
.end method

.method public setSourceValue(I)V
    .locals 0

    .line 261
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->sourceValue:I

    return-void
.end method

.method public setSupportedAdfDuplex(I)V
    .locals 0

    .line 378
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfDuplex:I

    return-void
.end method

.method public setSupportedAdfHeight(I)V
    .locals 0

    .line 366
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfHeight:I

    return-void
.end method

.method public setSupportedAdfWidth(I)V
    .locals 0

    .line 372
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->supportedAdfWidth:I

    return-void
.end method

.method public setSupportedBasicHeight(I)V
    .locals 0

    .line 354
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicHeight:I

    return-void
.end method

.method public setSupportedBasicResolution(I)V
    .locals 0

    .line 348
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicResolution:I

    return-void
.end method

.method public setSupportedBasicWidth(I)V
    .locals 0

    .line 360
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->supportedBasicWidth:I

    return-void
.end method

.method public setSupportedOptions([I)V
    .locals 0

    .line 171
    iput-object p1, p0, Lepson/scan/lib/ScannerInfo;->supportedOptions:[I

    return-void
.end method

.method public setSupportedResolutionList([I)V
    .locals 0

    .line 386
    iput-object p1, p0, Lepson/scan/lib/ScannerInfo;->supportedResolutionList:[I

    return-void
.end method

.method public setSupportedVersion(I)V
    .locals 0

    .line 342
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->supportedVersion:I

    return-void
.end method

.method public setTwoSidedScanningValue(I)V
    .locals 0

    .line 290
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->twoSidedScanningValue:I

    return-void
.end method

.method public setVersion(I)V
    .locals 0

    .line 178
    iput p1, p0, Lepson/scan/lib/ScannerInfo;->version:I

    return-void
.end method

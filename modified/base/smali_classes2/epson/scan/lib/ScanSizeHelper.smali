.class public Lepson/scan/lib/ScanSizeHelper;
.super Ljava/lang/Object;
.source "ScanSizeHelper.java"


# static fields
.field public static final SUPPORT_A3:I = 0x3

.field public static final SUPPORT_A4:I = 0x1

.field public static final SUPPORT_LEGAL:I = 0x2

.field public static final SUPPORT_NONE:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultScanSize()I
    .locals 2

    .line 235
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 236
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Ljava/util/Locale;->CANADA:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 240
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    return v0

    .line 237
    :cond_1
    :goto_0
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    return v0
.end method

.method private static getMaxScanSize(III)I
    .locals 3

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return v0

    .line 86
    :cond_0
    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1, p2}, Lepson/scan/lib/ScanSizeHelper;->getPaperSize(II)Landroid/graphics/Point;

    move-result-object v1

    .line 87
    iget v2, v1, Landroid/graphics/Point;->x:I

    if-lt p0, v2, :cond_1

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-lt p1, v1, :cond_1

    const/4 p0, 0x3

    return p0

    .line 92
    :cond_1
    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1, p2}, Lepson/scan/lib/ScanSizeHelper;->getPaperSize(II)Landroid/graphics/Point;

    move-result-object v1

    .line 93
    iget v2, v1, Landroid/graphics/Point;->x:I

    if-lt p0, v2, :cond_2

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-lt p1, v1, :cond_2

    const/4 p0, 0x2

    return p0

    .line 98
    :cond_2
    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1, p2}, Lepson/scan/lib/ScanSizeHelper;->getPaperSize(II)Landroid/graphics/Point;

    move-result-object p2

    .line 99
    iget v1, p2, Landroid/graphics/Point;->x:I

    if-lt p0, v1, :cond_3

    iget p0, p2, Landroid/graphics/Point;->y:I

    if-lt p1, p0, :cond_3

    const/4 p0, 0x1

    return p0

    :cond_3
    return v0
.end method

.method public static getMaxScanSize(IIIII)Landroid/graphics/Point;
    .locals 2

    .line 208
    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 217
    :pswitch_0
    invoke-static {p0, p1, p2, p3, p4}, Lepson/scan/lib/ScanSizeHelper;->getSupportedScanSizeList(IIIII)[I

    move-result-object p0

    .line 220
    array-length p1, p0

    if-lez p1, :cond_0

    .line 221
    array-length p1, p0

    add-int/lit8 p1, p1, -0x1

    aget p0, p0, p1

    invoke-static {p0, p4}, Lepson/scan/lib/ScanSizeHelper;->getPaperSize(II)Landroid/graphics/Point;

    move-result-object v0

    goto :goto_0

    .line 212
    :pswitch_1
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p2, p3}, Landroid/graphics/Point;-><init>(II)V

    :cond_0
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getPaperSize(II)Landroid/graphics/Point;
    .locals 5

    .line 31
    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x5

    if-eq p0, v0, :cond_3

    const/16 v0, 0x3e

    const/16 v1, 0xb9a

    if-eq p0, v0, :cond_2

    const/16 v0, 0xaea

    const/16 v2, 0x834

    const/16 v3, 0x86f

    packed-switch p0, :pswitch_data_0

    .line 57
    invoke-static {}, Lepson/scan/lib/ScanSizeHelper;->getDefaultScanSize()I

    move-result p0

    const/4 v4, 0x1

    if-eq p0, v4, :cond_1

    .line 63
    new-instance p0, Landroid/graphics/Point;

    invoke-direct {p0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 59
    :cond_1
    new-instance p0, Landroid/graphics/Point;

    invoke-direct {p0, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 51
    :pswitch_0
    new-instance p0, Landroid/graphics/Point;

    const/16 v0, 0xde4

    invoke-direct {p0, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 48
    :pswitch_1
    new-instance p0, Landroid/graphics/Point;

    invoke-direct {p0, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 42
    :pswitch_2
    new-instance p0, Landroid/graphics/Point;

    invoke-direct {p0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 45
    :cond_2
    new-instance p0, Landroid/graphics/Point;

    const/16 v0, 0x1068

    invoke-direct {p0, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 39
    :cond_3
    new-instance p0, Landroid/graphics/Point;

    const/16 v0, 0x71c

    const/16 v1, 0xa0a

    invoke-direct {p0, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 69
    :goto_0
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Landroid/graphics/Point;->x:I

    mul-int v1, v1, p1

    int-to-float v1, v1

    const v2, 0x3b810204

    mul-float v1, v1, v2

    float-to-int v1, v1

    iget p0, p0, Landroid/graphics/Point;->y:I

    mul-int p0, p0, p1

    int-to-float p0, p0

    mul-float p0, p0, v2

    float-to-int p0, p0

    invoke-direct {v0, v1, p0}, Landroid/graphics/Point;-><init>(II)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getScanSizeName(I)I
    .locals 1

    .line 251
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    if-ne p0, v0, :cond_0

    const p0, 0x7f0e0515

    return p0

    .line 253
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    if-ne p0, v0, :cond_1

    const p0, 0x7f0e0500

    return p0

    .line 257
    :cond_1
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    .line 258
    invoke-virtual {v0, p0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;->getStringId(I)I

    move-result p0

    return p0
.end method

.method public static getSupportedScanSizeList(IIIII)[I
    .locals 2

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    invoke-static {p2, p3, p4}, Lepson/scan/lib/ScanSizeHelper;->getMaxScanSize(III)I

    move-result p2

    const/4 p3, 0x3

    const/4 p4, 0x2

    const/4 v1, 0x1

    packed-switch p0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    packed-switch p1, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_1
    if-lt p2, v1, :cond_5

    .line 175
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_2
    if-lt p2, v1, :cond_0

    .line 154
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-lt p2, p4, :cond_1

    .line 162
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-lt p2, p3, :cond_5

    .line 167
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_3
    if-lt p2, v1, :cond_2

    .line 125
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    if-lt p2, p4, :cond_3

    .line 136
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    if-lt p2, p3, :cond_4

    .line 141
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_4
    sget-object p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_5
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array p0, p0, [I

    const/4 p1, 0x0

    .line 190
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-ge p1, p2, :cond_6

    .line 191
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    aput p2, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_6
    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.class public Lepson/scan/lib/libHaru;
.super Ljava/lang/Object;
.source "libHaru.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "haru"

    .line 7
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native EndDoc(Ljava/lang/String;)I
.end method

.method public native addPage(Ljava/lang/String;)I
.end method

.method public createPDF(Ljava/util/ArrayList;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .line 22
    invoke-virtual {p0}, Lepson/scan/lib/libHaru;->initDoc()I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 27
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 29
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lepson/scan/lib/libHaru;->addPage(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_2
    invoke-virtual {p0, p2}, Lepson/scan/lib/libHaru;->EndDoc(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public native initDoc()I
.end method

.class public Lepson/scan/lib/ScanSettingHelper;
.super Ljava/lang/Object;
.source "ScanSettingHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ScanSettingHelper"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static adjustAndSaveScanParams(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;)V
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lepson/scan/activity/ScannerPropertyWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 447
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 450
    invoke-static {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->getAdjustedParams(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v0

    .line 451
    invoke-static {p0, v0}, Lepson/scan/i2lib/I2ScanParamManager;->saveParams(Landroid/content/Context;Lcom/epson/lib/escani2/ScanI2Params;)V

    .line 454
    invoke-virtual {p1, p0}, Lepson/scan/activity/ScannerPropertyWrapper;->updateSetSimpleApSsid(Landroid/content/Context;)V

    .line 456
    invoke-virtual {p1, p0, v1}, Lepson/scan/activity/ScannerPropertyWrapper;->saveData(Landroid/content/Context;Z)V

    goto :goto_0

    .line 459
    :cond_0
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getI1ScannerInfo()Lepson/scan/lib/ScannerInfo;

    move-result-object v0

    .line 461
    invoke-static {p0}, Lepson/scan/lib/ScanCommonParams;->load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams;

    move-result-object v2

    .line 462
    invoke-static {v2, v0}, Lepson/scan/lib/ScanSettingHelper;->setDefaultSettings3(Lepson/scan/lib/ScanCommonParams;Lepson/scan/lib/ScannerInfo;)V

    .line 465
    invoke-virtual {p1, p0, v1}, Lepson/scan/activity/ScannerPropertyWrapper;->saveData(Landroid/content/Context;Z)V

    .line 468
    invoke-static {p0, v0}, Lepson/scan/lib/ScanSettingHelper;->changeScanner(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    :goto_0
    return-void
.end method

.method private static changeScanner(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 3

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_ID"

    .line 386
    invoke-static {p0, v0, v1}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v2, "SCAN_REFS_SCANNER_ACCESSPATH"

    .line 388
    invoke-static {p0, v1, v2}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v0, :cond_0

    .line 391
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v0

    if-eq v1, v0, :cond_2

    :cond_0
    const-string v0, "ScanSettingHelper"

    .line 392
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeScanner scannerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    invoke-static {p0, p1}, Lepson/scan/lib/ScanSettingHelper;->saveAllUserSettingsToRef(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 396
    invoke-static {p0, p1}, Lepson/scan/lib/ScanSettingHelper;->saveSupportedOptionToRef(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 401
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromScannerId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 399
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 403
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    const-string v1, "scanner"

    .line 407
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object p1

    .line 405
    invoke-static {p0, v0, v1, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setConnectInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string p1, "scanner"

    .line 410
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public static getI1SupportedOptions(Lepson/scan/lib/escanLib;Lepson/scan/lib/ScannerInfo;)I
    .locals 2

    const/4 v0, 0x0

    .line 178
    invoke-virtual {p0, v0}, Lepson/scan/lib/escanLib;->escanWrapperSetScanner(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/16 p0, -0x514

    return p0

    .line 186
    :cond_0
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->GetSupportedOption()I

    move-result v0

    .line 192
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->getEscanSupportOption()[I

    move-result-object v1

    invoke-virtual {p1, v1}, Lepson/scan/lib/ScannerInfo;->setSupportedOptions([I)V

    .line 194
    invoke-virtual {p0}, Lepson/scan/lib/escanLib;->getEscanSupportResolution()[I

    move-result-object p0

    invoke-virtual {p1, p0}, Lepson/scan/lib/ScannerInfo;->setSupportedResolutionList([I)V

    .line 197
    invoke-static {p1}, Lepson/scan/lib/ScanSettingHelper;->updateNewSelectedScannerSupportedOptions(Lepson/scan/lib/ScannerInfo;)V

    return v0
.end method

.method public static recodeScannerInfo(Lepson/scan/lib/escanLib;Landroid/content/Context;Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/scan/lib/EscanLibException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 428
    invoke-static {p0, p2}, Lepson/scan/lib/CancelablePropertyTaker;->getScannerProperty(Lepson/scan/lib/escanLib;Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 433
    invoke-static {p1, p0}, Lepson/scan/lib/ScanSettingHelper;->adjustAndSaveScanParams(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;)V

    return-object p0

    .line 430
    :cond_0
    new-instance p0, Lepson/scan/lib/EscanLibException;

    const/16 p1, -0x3e9

    invoke-direct {p0, p1}, Lepson/scan/lib/EscanLibException;-><init>(I)V

    throw p0
.end method

.method public static saveAllUserSettingsToRef(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 4

    .line 36
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getModelName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getIp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getLocation()I

    move-result v3

    invoke-static {p0, v0, v1, v2, v3}, Lepson/scan/lib/ScanInfoStorage;->saveScannerConnectivityInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 38
    invoke-static {p0, p1}, Lepson/scan/lib/ScanInfoStorage;->saveSettings(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    .line 40
    invoke-static {p0, p1}, Lepson/scan/lib/ScanSettingHelper;->saveScanDefaultMaxSize(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V

    return-void
.end method

.method public static saveErroredBlankSettings(Landroid/content/Context;)V
    .locals 4

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_VALIDATION"

    const/4 v2, 0x0

    .line 132
    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    const v0, 0x7f0e04d6

    .line 135
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const-string v3, ""

    invoke-static {p0, v0, v1, v3, v2}, Lepson/scan/lib/ScanInfoStorage;->saveScannerConnectivityInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_SOURCE"

    .line 139
    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_ADF_HEIGHT"

    .line 141
    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_ADF_WIDTH"

    .line 142
    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_CHOSEN_SIZE_ADF"

    .line 145
    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_CHOSEN_SIZE_DOC"

    .line 146
    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_CHOSEN_SIZE"

    .line 147
    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_COLOR"

    const/4 v2, 0x1

    .line 150
    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_RESOLUTION"

    const/16 v2, 0x96

    .line 154
    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_MAX_WIDTH"

    const/16 v2, 0x4fb

    .line 158
    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_MAX_HEIGHT"

    const/16 v2, 0x6db

    .line 159
    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public static saveScanDefaultMaxSize(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 8

    .line 72
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicResolution()I

    move-result v0

    .line 73
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getResolutionValue()I

    move-result v1

    const/16 v2, 0x6db

    const/16 v3, 0x4fb

    if-lez v0, :cond_5

    .line 82
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 85
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v4

    .line 86
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result v5

    .line 87
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result v6

    .line 88
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result v7

    .line 84
    invoke-static {v4, v5, v6, v7, v0}, Lepson/scan/lib/ScanSizeHelper;->getMaxScanSize(IIIII)Landroid/graphics/Point;

    move-result-object v4

    .line 91
    iget v5, v4, Landroid/graphics/Point;->x:I

    .line 92
    iget v4, v4, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 95
    :cond_0
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicWidth()I

    move-result v5

    .line 97
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedBasicHeight()I

    move-result v4

    :goto_0
    if-le v1, v0, :cond_1

    .line 101
    div-int/2addr v1, v0

    mul-int v5, v5, v1

    mul-int v4, v4, v1

    goto :goto_1

    :cond_1
    if-ge v1, v0, :cond_2

    .line 104
    div-int/2addr v0, v1

    div-int/2addr v5, v0

    .line 105
    div-int/2addr v4, v0

    :cond_2
    :goto_1
    if-nez v5, :cond_3

    goto :goto_2

    :cond_3
    move v3, v5

    :goto_2
    if-nez v4, :cond_4

    goto :goto_3

    :cond_4
    move v2, v4

    .line 118
    :cond_5
    :goto_3
    invoke-virtual {p1, v3}, Lepson/scan/lib/ScannerInfo;->setMaxWidth(I)V

    .line 119
    invoke-virtual {p1, v2}, Lepson/scan/lib/ScannerInfo;->setMaxHeight(I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_MAX_WIDTH"

    .line 121
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getMaxWidth()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_MAX_HEIGHT"

    .line 122
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getMaxHeight()I

    move-result p1

    invoke-static {p0, v0, v1, p1}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private static saveSelectedScannerSupportedResolutionList(Landroid/content/Context;[I)V
    .locals 4

    const/4 v0, 0x0

    .line 47
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 49
    aget v1, p1, v0

    const/16 v2, 0x4b

    if-eq v1, v2, :cond_0

    const/16 v2, 0x96

    if-eq v1, v2, :cond_0

    const/16 v2, 0x12c

    if-eq v1, v2, :cond_0

    goto :goto_1

    :cond_0
    const-string v1, "epson.scanner.supported.Options"

    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SCAN_REFS_OPTIONS_RESOLUTION_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aget v3, p1, v0

    invoke-static {p0, v1, v2, v3}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static saveSupportedOptionToRef(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 1

    .line 30
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    invoke-static {p0, v0}, Lepson/scan/lib/ScanInfoStorage;->saveSelectedScannerSupportedOptions(Landroid/content/Context;[I)V

    .line 31
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedResolutionList()[I

    move-result-object p1

    invoke-static {p0, p1}, Lepson/scan/lib/ScanSettingHelper;->saveSelectedScannerSupportedResolutionList(Landroid/content/Context;[I)V

    return-void
.end method

.method public static setDefaultSettings2(Landroid/content/Context;Lepson/scan/lib/ScanInfoStorage;Lepson/scan/lib/ScannerInfo;)V
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lepson/scan/lib/ScanInfoStorage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lepson/scan/lib/ScannerInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 240
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 243
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    .line 246
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setAdfDuplexRotaitonYes(Z)V

    .line 249
    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_1

    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result v1

    if-lez v1, :cond_1

    .line 253
    invoke-virtual {p2, v2}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 257
    invoke-virtual {p1, p0, v0}, Lepson/scan/lib/ScanInfoStorage;->getScanSourceUnit(Landroid/content/Context;I)I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 260
    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfDuplex()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 263
    invoke-virtual {p1, p0}, Lepson/scan/lib/ScanInfoStorage;->getTwoSideValue(Landroid/content/Context;)I

    move-result p0

    invoke-virtual {p2, p0}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    .line 269
    invoke-virtual {p1, p0, v1}, Lepson/scan/lib/ScanInfoStorage;->getScanSourceUnit(Landroid/content/Context;I)I

    move-result p0

    if-eq p0, v1, :cond_1

    .line 272
    invoke-virtual {p2, p0}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 277
    :cond_1
    :goto_0
    invoke-virtual {p2}, Lepson/scan/lib/ScannerInfo;->getSupportedResolutionList()[I

    move-result-object p0

    invoke-virtual {p0}, [I->clone()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [I

    .line 278
    invoke-static {p0}, Ljava/util/Arrays;->sort([I)V

    const/16 p1, 0x96

    .line 280
    invoke-static {p0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v1

    if-lez v1, :cond_2

    .line 281
    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    goto :goto_1

    :cond_2
    const/16 p1, 0x4b

    .line 282
    invoke-static {p0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v1

    if-lez v1, :cond_3

    .line 283
    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    goto :goto_1

    :cond_3
    const/16 p1, 0x12c

    .line 284
    invoke-static {p0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result p0

    if-lez p0, :cond_4

    .line 285
    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    goto :goto_1

    .line 287
    :cond_4
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    .line 291
    :goto_1
    invoke-virtual {p2, v2}, Lepson/scan/lib/ScannerInfo;->setColorValue(I)V

    .line 294
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setDensityStatus(Z)V

    const/16 p0, 0x80

    .line 295
    invoke-virtual {p2, p0}, Lepson/scan/lib/ScannerInfo;->setDensityValue(I)V

    .line 298
    invoke-virtual {p2, v2}, Lepson/scan/lib/ScannerInfo;->setGammaValue(I)V

    return-void
.end method

.method public static setDefaultSettings3(Lepson/scan/lib/ScanCommonParams;Lepson/scan/lib/ScannerInfo;)V
    .locals 4
    .param p0    # Lepson/scan/lib/ScanCommonParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lepson/scan/lib/ScannerInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 323
    invoke-virtual {p1, v0}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 326
    invoke-virtual {p1, v0}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    .line 329
    invoke-virtual {p1, v0}, Lepson/scan/lib/ScannerInfo;->setAdfDuplexRotaitonYes(Z)V

    .line 332
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfHeight()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfWidth()I

    move-result v1

    if-lez v1, :cond_1

    .line 336
    invoke-virtual {p1, v2}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 340
    invoke-virtual {p0}, Lepson/scan/lib/ScanCommonParams;->getScanSourceUnit()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 343
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedAdfDuplex()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 346
    invoke-virtual {p0}, Lepson/scan/lib/ScanCommonParams;->getTwoSideValue()I

    move-result p0

    invoke-virtual {p1, p0}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    goto :goto_0

    .line 350
    :cond_0
    invoke-virtual {p0}, Lepson/scan/lib/ScanCommonParams;->getScanSourceUnit()I

    move-result p0

    invoke-virtual {p1, p0}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    .line 354
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSupportedResolutionList()[I

    move-result-object p0

    invoke-virtual {p0}, [I->clone()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [I

    .line 355
    invoke-static {p0}, Ljava/util/Arrays;->sort([I)V

    const/16 v1, 0x96

    .line 357
    invoke-static {p0, v1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v3

    if-lez v3, :cond_2

    .line 358
    invoke-virtual {p1, v1}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    goto :goto_1

    :cond_2
    const/16 v1, 0x4b

    .line 359
    invoke-static {p0, v1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v3

    if-lez v3, :cond_3

    .line 360
    invoke-virtual {p1, v1}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    goto :goto_1

    :cond_3
    const/16 v1, 0x12c

    .line 361
    invoke-static {p0, v1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result p0

    if-lez p0, :cond_4

    .line 362
    invoke-virtual {p1, v1}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    goto :goto_1

    .line 364
    :cond_4
    invoke-virtual {p1, v0}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    .line 368
    :goto_1
    invoke-virtual {p1, v2}, Lepson/scan/lib/ScannerInfo;->setColorValue(I)V

    .line 371
    invoke-virtual {p1, v0}, Lepson/scan/lib/ScannerInfo;->setDensityStatus(Z)V

    const/16 p0, 0x80

    .line 372
    invoke-virtual {p1, p0}, Lepson/scan/lib/ScannerInfo;->setDensityValue(I)V

    .line 375
    invoke-virtual {p1, v2}, Lepson/scan/lib/ScannerInfo;->setGammaValue(I)V

    return-void
.end method

.method public static updateNewSelectedScannerSupportedOptions(Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    .line 208
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lepson/scan/lib/ScannerInfo;->setVersion(I)V

    .line 209
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedBasicResolution(I)V

    .line 210
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedBasicWidth(I)V

    .line 211
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    const/4 v1, 0x3

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedBasicHeight(I)V

    .line 212
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    const/4 v1, 0x4

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfWidth(I)V

    .line 213
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    const/4 v1, 0x5

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfHeight(I)V

    .line 214
    invoke-virtual {p0}, Lepson/scan/lib/ScannerInfo;->getSupportedOptions()[I

    move-result-object v0

    const/4 v1, 0x6

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfDuplex(I)V

    return-void
.end method

.class public Lepson/scan/lib/ScanInfoStorage;
.super Ljava/lang/Object;
.source "ScanInfoStorage.java"


# static fields
.field private static final PREFS_KEY_ESC_I_VERSION:Ljava/lang/String; = "esc/i-version"

.field private static sScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lepson/scan/lib/ScanInfoStorage;
    .locals 1

    .line 30
    sget-object v0, Lepson/scan/lib/ScanInfoStorage;->sScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lepson/scan/lib/ScanInfoStorage;

    invoke-direct {v0}, Lepson/scan/lib/ScanInfoStorage;-><init>()V

    sput-object v0, Lepson/scan/lib/ScanInfoStorage;->sScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    .line 33
    :cond_0
    sget-object v0, Lepson/scan/lib/ScanInfoStorage;->sScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    return-object v0
.end method

.method public static loadEscIVersion(Landroid/content/Context;)I
    .locals 1

    .line 294
    invoke-static {}, Lepson/scan/lib/ScanInfoStorage;->getInstance()Lepson/scan/lib/ScanInfoStorage;

    move-result-object v0

    .line 295
    invoke-virtual {v0, p0}, Lepson/scan/lib/ScanInfoStorage;->loadEscIVersionNonStatic(Landroid/content/Context;)I

    move-result p0

    return p0
.end method

.method public static saveEscIVersion(Landroid/content/Context;I)V
    .locals 1

    .line 284
    invoke-static {}, Lepson/scan/lib/ScanInfoStorage;->getInstance()Lepson/scan/lib/ScanInfoStorage;

    move-result-object v0

    .line 285
    invoke-virtual {v0, p0, p1}, Lepson/scan/lib/ScanInfoStorage;->saveEscIVersionNonStatic(Landroid/content/Context;I)V

    return-void
.end method

.method public static saveScannerConnectivityInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .line 129
    invoke-static {}, Lepson/scan/lib/ScanInfoStorage;->getInstance()Lepson/scan/lib/ScanInfoStorage;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lepson/scan/lib/ScanInfoStorage;->saveScannerConnectivityInfoNonStatic(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static saveSelectedScannerSupportedOptions(Landroid/content/Context;[I)V
    .locals 3

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_VERSION"

    const/4 v2, 0x0

    .line 99
    aget v2, p1, v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_BASIC_RESOLUTION"

    const/4 v2, 0x1

    .line 102
    aget v2, p1, v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_WIDTH"

    const/4 v2, 0x2

    .line 105
    aget v2, p1, v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_HEIGHT"

    const/4 v2, 0x3

    .line 108
    aget v2, p1, v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_ADF_WIDTH"

    const/4 v2, 0x4

    .line 111
    aget v2, p1, v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_ADF_HEIGHT"

    const/4 v2, 0x5

    .line 114
    aget v2, p1, v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_SUPPORTED_ADF_DUPLEX"

    const/4 v2, 0x6

    .line 117
    aget p1, p1, v2

    invoke-static {p0, v0, v1, p1}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static saveSettings(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 3

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_SOURCE"

    .line 54
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSourceValue()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_CHOSEN_SIZE"

    .line 58
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getSizeValue()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_COLOR"

    .line 61
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getColorValue()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_RESOLUTION"

    .line 65
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getResolutionValue()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_2SIDED"

    .line 69
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getTwoSidedScanningValue()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_DENSITY_NAME"

    .line 74
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getDensityValue()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_DENSITY_STATUS"

    .line 75
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->isDensityStatus()Z

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_GAMMA"

    .line 78
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getGammaValue()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_ROTATE"

    .line 82
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->isAdfDuplexRotaitonYes()Z

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_ADF_PAPER_GUIDE"

    .line 85
    invoke-virtual {p1}, Lepson/scan/lib/ScannerInfo;->getAdfPaperGuide()I

    move-result p1

    invoke-static {p0, v0, v1, p1}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string p1, "epson.scanner.SelectedScanner"

    const-string v0, "RE_SEARCH"

    const/4 v1, 0x1

    .line 88
    invoke-static {p0, p1, v0, v1}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method protected static setInstance(Lepson/scan/lib/ScanInfoStorage;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 41
    sput-object p0, Lepson/scan/lib/ScanInfoStorage;->sScanInfoStorage:Lepson/scan/lib/ScanInfoStorage;

    return-void
.end method


# virtual methods
.method public getScanSourceUnit(Landroid/content/Context;)I
    .locals 1

    const/4 v0, 0x1

    .line 240
    invoke-virtual {p0, p1, v0}, Lepson/scan/lib/ScanInfoStorage;->getScanSourceUnit(Landroid/content/Context;I)I

    move-result p1

    return p1
.end method

.method public getScanSourceUnit(Landroid/content/Context;I)I
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const/4 v1, 0x0

    .line 235
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "SCAN_REFS_SETTINGS_SOURCE"

    .line 236
    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method public getTwoSideValue(Landroid/content/Context;)I
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_2SIDED"

    .line 247
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public loadColorValue(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_COLOR"

    .line 207
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setColorValue(I)V

    return-void
.end method

.method public loadEscIVersionNonStatic(Landroid/content/Context;)I
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const/4 v1, 0x0

    .line 268
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "esc/i-version"

    const/4 v1, 0x1

    .line 270
    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method public loadReadUnitValue(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_SOURCE"

    .line 222
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setSourceValue(I)V

    return-void
.end method

.method public loadResolution(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_RESOLUTION"

    .line 212
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setResolutionValue(I)V

    return-void
.end method

.method public loadScanSetting(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_SUPPORTED_ADF_DUPLEX"

    .line 151
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfDuplex(I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_2SIDED"

    .line 155
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setTwoSidedScanningValue(I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_DENSITY_NAME"

    .line 159
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setDensityValue(I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_DENSITY_STATUS"

    .line 162
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setDensityStatus(Z)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_GAMMA"

    .line 166
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setGammaValue(I)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_ROTATE"

    .line 171
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setAdfDuplexRotaitonYes(Z)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SETTINGS_ADF_PAPER_GUIDE"

    .line 176
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setAdfPaperGuide(I)V

    return-void
.end method

.method public loadScannerConnectivityInfo(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_ID"

    .line 140
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setScannerId(Ljava/lang/String;)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_MODEL"

    .line 142
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setModelName(Ljava/lang/String;)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_LOCATION"

    .line 144
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setIp(Ljava/lang/String;)V

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_ACCESSPATH"

    .line 146
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setLocation(I)V

    return-void
.end method

.method public loadSupportedAdfSize(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_ADF_WIDTH"

    .line 199
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 198
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfWidth(I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_ADF_HEIGHT"

    .line 202
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    .line 201
    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfHeight(I)V

    return-void
.end method

.method public loadSupportedValue(Landroid/content/Context;Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_BASIC_RESOLUTION"

    .line 183
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 182
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedBasicResolution(I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_WIDTH"

    .line 185
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 184
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedBasicWidth(I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_HEIGHT"

    .line 187
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 186
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedBasicHeight(I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_ADF_WIDTH"

    .line 189
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 188
    invoke-virtual {p2, v0}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfWidth(I)V

    const-string v0, "epson.scanner.supported.Options"

    const-string v1, "SCAN_REFS_OPTIONS_ADF_HEIGHT"

    .line 191
    invoke-static {p1, v0, v1}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    .line 190
    invoke-virtual {p2, p1}, Lepson/scan/lib/ScannerInfo;->setSupportedAdfHeight(I)V

    return-void
.end method

.method public saveEscIVersionNonStatic(Landroid/content/Context;I)V
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "esc/i-version"

    .line 259
    invoke-static {p1, v0, v1, p2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method protected saveScannerConnectivityInfoNonStatic(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    const-string v0, "epson.scanner.SelectedScanner"

    const-string v1, "SCAN_REFS_SCANNER_MODEL"

    .line 133
    invoke-static {p1, v0, v1, p2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "epson.scanner.SelectedScanner"

    const-string v0, "SCAN_REFS_SCANNER_LOCATION"

    .line 134
    invoke-static {p1, p2, v0, p3}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "epson.scanner.SelectedScanner"

    const-string p3, "SCAN_REFS_SCANNER_ID"

    .line 135
    invoke-static {p1, p2, p3, p4}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "epson.scanner.SelectedScanner"

    const-string p3, "SCAN_REFS_SCANNER_ACCESSPATH"

    .line 136
    invoke-static {p1, p2, p3, p5}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

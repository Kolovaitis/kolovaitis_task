.class Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
.super Ljava/lang/Object;
.source "I2ScanParamArbiter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/i2lib/I2ScanParamArbiter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Selector"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final UNKNOWN_POSITION:I = -0x1


# instance fields
.field private mCandidates:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TE;"
        }
    .end annotation
.end field

.field private mSelectedPosition:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 813
    iput v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->mSelectedPosition:I

    return-void
.end method


# virtual methods
.method public getCandidateArray()[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[TE;"
        }
    .end annotation

    .line 859
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->mCandidates:[Ljava/lang/Object;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .line 821
    iget v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->mSelectedPosition:I

    return v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .line 841
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    .line 846
    :cond_0
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->mCandidates:[Ljava/lang/Object;

    aget-object v0, v1, v0

    return-object v0
.end method

.method public setCandidateArray([Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)V"
        }
    .end annotation

    .line 850
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->mCandidates:[Ljava/lang/Object;

    .line 851
    array-length p1, p1

    if-lez p1, :cond_0

    const/4 p1, 0x0

    .line 852
    invoke-virtual {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    .line 854
    invoke-virtual {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    :goto_0
    return-void
.end method

.method public setPosition(I)V
    .locals 0

    .line 825
    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->mSelectedPosition:I

    return-void
.end method

.method public setSelectedValue(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .line 833
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->mCandidates:[Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    .line 834
    invoke-virtual {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    return-void
.end method

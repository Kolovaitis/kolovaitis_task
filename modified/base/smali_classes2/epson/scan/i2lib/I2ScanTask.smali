.class public Lepson/scan/i2lib/I2ScanTask;
.super Landroid/os/AsyncTask;
.source "I2ScanTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/i2lib/I2ScanTask$LocalCancel;,
        Lepson/scan/i2lib/I2ScanTask$TaskError;,
        Lepson/scan/i2lib/I2ScanTask$TaskObserver;,
        Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/scan/i2lib/I2ScanTask$TaskError;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "I2ScanTask"


# instance fields
.field private mApplicationContext:Landroid/content/Context;

.field private volatile mCancelRequested:Z

.field private mCancelableScannerProbe:Lepson/scan/lib/CancelableScannerProbe;

.field private mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

.field private mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

.field private mResScanFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

.field private mScanI2Runner:Lcom/epson/lib/escani2/ScanI2Runner;

.field private final mScanResultReceiver:Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;

.field private mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;Lcom/epson/iprint/shared/SharedParamScan;Lcom/epson/iprint/prtlogger/CommonLog;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/shared/SharedParamScan;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 135
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 136
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    .line 138
    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanTask;->loadAndAdjustScanParams(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v0

    .line 139
    invoke-direct {p0, v0, p3}, Lepson/scan/i2lib/I2ScanTask;->margeScanI2Params(Lcom/epson/lib/escani2/ScanI2Params;Lcom/epson/iprint/shared/SharedParamScan;)V

    .line 140
    iput-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

    .line 142
    new-instance p3, Lcom/epson/lib/escani2/ScanI2Runner;

    invoke-direct {p3}, Lcom/epson/lib/escani2/ScanI2Runner;-><init>()V

    iput-object p3, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Runner:Lcom/epson/lib/escani2/ScanI2Runner;

    .line 143
    invoke-static {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->load(Landroid/content/Context;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object p3

    iput-object p3, p0, Lepson/scan/i2lib/I2ScanTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 144
    iget-object p3, p0, Lepson/scan/i2lib/I2ScanTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    if-eqz p3, :cond_3

    .line 147
    invoke-virtual {p3}, Lepson/scan/activity/ScannerPropertyWrapper;->getEscIVersion()I

    move-result p3

    const/4 v1, 0x2

    if-ne p3, v1, :cond_2

    .line 151
    iget-object p3, p0, Lepson/scan/i2lib/I2ScanTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-virtual {p3}, Lepson/scan/activity/ScannerPropertyWrapper;->getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object p3

    .line 153
    iget-object p3, p3, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    iget v5, p3, Lcom/epson/lib/escani2/ScannerI2Info;->adfDuplexType:I

    .line 155
    new-instance p3, Lepson/scan/i2lib/ImageProcessController;

    iget-boolean v3, v0, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    iget v4, v0, Lcom/epson/lib/escani2/ScanI2Params;->duplexTurnDirection:I

    iget-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    sget-object v2, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v6, 0x0

    :goto_0
    iget v7, v0, Lcom/epson/lib/escani2/ScanI2Params;->density:I

    move-object v1, p3

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lepson/scan/i2lib/ImageProcessController;-><init>(Lepson/scan/i2lib/I2ScanTask;ZIIZI)V

    iput-object p3, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    .line 159
    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanTask;->getSaveDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object p1

    .line 160
    iget-object p3, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p3, p1, v0}, Lepson/scan/i2lib/ImageProcessController;->init(Ljava/io/File;Ljava/util/Calendar;)V

    .line 162
    iput-object p2, p0, Lepson/scan/i2lib/I2ScanTask;->mScanResultReceiver:Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;

    .line 163
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mResScanFileList:Ljava/util/ArrayList;

    .line 165
    iput-object p4, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    .line 166
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    if-nez p1, :cond_1

    .line 167
    new-instance p1, Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {p1}, Lcom/epson/iprint/prtlogger/CommonLog;-><init>()V

    iput-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    :cond_1
    return-void

    .line 148
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "ScannerPropertyWrapper.load() error! ESC/I version != 2"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 145
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "ScannerPropertyWrapper.load() error!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private checkCancel()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/scan/i2lib/I2ScanTask$LocalCancel;
        }
    .end annotation

    .line 539
    iget-boolean v0, p0, Lepson/scan/i2lib/I2ScanTask;->mCancelRequested:Z

    if-nez v0, :cond_0

    return-void

    .line 540
    :cond_0
    new-instance v0, Lepson/scan/i2lib/I2ScanTask$LocalCancel;

    invoke-direct {v0}, Lepson/scan/i2lib/I2ScanTask$LocalCancel;-><init>()V

    throw v0
.end method

.method private checkScanParams(Ljava/io/File;Lcom/epson/lib/escani2/ScanI2Params;Lcom/epson/lib/escani2/ScannerI2Info;)I
    .locals 4
    .param p1    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/lib/escani2/ScanI2Params;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/lib/escani2/ScannerI2Info;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 449
    iget-object v0, p2, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 450
    invoke-virtual {v0, p3, p2}, Lcom/epson/lib/escani2/ScanSize;->getValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object p3

    const/4 v0, 0x1

    if-nez p3, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    .line 455
    aget v2, p3, v1

    aget p3, p3, v0

    mul-int v2, v2, p3

    mul-int/lit8 v2, v2, 0x3

    int-to-long v2, v2

    .line 457
    iget-object p2, p2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object p3, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne p2, p3, :cond_1

    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->getAvailableScanPageCount()I

    move-result p2

    goto :goto_0

    :cond_1
    const/4 p2, 0x1

    :goto_0
    add-int/2addr p2, v0

    int-to-long p2, p2

    mul-long p2, p2, v2

    .line 460
    invoke-virtual {p1}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v2

    cmp-long p1, v2, p2

    if-gez p1, :cond_2

    const/4 p1, 0x2

    return p1

    :cond_2
    return v1
.end method

.method private convertFile()V
    .locals 2

    .line 489
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Runner:Lcom/epson/lib/escani2/ScanI2Runner;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/ScanI2Runner;->getScanPages()[I

    move-result-object v0

    .line 491
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v1, v0}, Lepson/scan/i2lib/ImageProcessController;->getScanFiles([I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mResScanFileList:Ljava/util/ArrayList;

    return-void
.end method

.method private convertScanParams(Lcom/epson/lib/escani2/ScanI2Params;)Lepson/scan/lib/I1ScanParams;
    .locals 5
    .param p1    # Lcom/epson/lib/escani2/ScanI2Params;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 394
    new-instance v0, Lepson/scan/lib/I1ScanParams;

    invoke-direct {v0}, Lepson/scan/lib/I1ScanParams;-><init>()V

    .line 396
    sget-object v1, Lepson/scan/i2lib/I2ScanTask$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

    iget-object v2, p1, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    .line 402
    iput v2, v0, Lepson/scan/lib/I1ScanParams;->inputDevice:I

    goto :goto_0

    .line 398
    :cond_0
    iput v3, v0, Lepson/scan/lib/I1ScanParams;->inputDevice:I

    .line 406
    :goto_0
    sget-object v1, Lepson/scan/i2lib/I2ScanTask$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$ColorMode:[I

    iget-object v4, p1, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 415
    iput v3, v0, Lepson/scan/lib/I1ScanParams;->colorType:I

    goto :goto_1

    :pswitch_0
    const/4 v1, 0x2

    .line 411
    iput v1, v0, Lepson/scan/lib/I1ScanParams;->colorType:I

    goto :goto_1

    :pswitch_1
    const/4 v1, 0x3

    .line 408
    iput v1, v0, Lepson/scan/lib/I1ScanParams;->colorType:I

    .line 418
    :goto_1
    iget v1, p1, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    iput v1, v0, Lepson/scan/lib/I1ScanParams;->resolution:I

    .line 419
    iget-boolean v1, p1, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    iput v1, v0, Lepson/scan/lib/I1ScanParams;->twoSide:I

    .line 421
    sget-object v1, Lepson/scan/i2lib/I2ScanTask$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$Gamma:[I

    iget-object v4, p1, Lcom/epson/lib/escani2/ScanI2Params;->userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    invoke-virtual {v4}, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->ordinal()I

    move-result v4

    aget v1, v1, v4

    if-eq v1, v3, :cond_1

    .line 427
    iput v3, v0, Lepson/scan/lib/I1ScanParams;->gamma:I

    goto :goto_2

    .line 423
    :cond_1
    iput v2, v0, Lepson/scan/lib/I1ScanParams;->gamma:I

    .line 430
    :goto_2
    iget v1, p1, Lcom/epson/lib/escani2/ScanI2Params;->density:I

    rsub-int v1, v1, 0xff

    iput v1, v0, Lepson/scan/lib/I1ScanParams;->brightness:I

    .line 431
    iget-object p1, p1, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    if-eq p1, v1, :cond_2

    const/16 p1, 0x7f

    .line 433
    iput p1, v0, Lepson/scan/lib/I1ScanParams;->brightness:I

    :cond_2
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getSaveDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .line 204
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 205
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_1

    .line 207
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 209
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mkdirs error \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-object v0
.end method

.method private loadAndAdjustScanParams(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;
    .locals 2

    .line 223
    invoke-static {p1}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object p1

    .line 225
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;->FALSE:Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;

    iput-object v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->autoCrop:Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;

    .line 226
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;->FALSE:Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;

    iput-object v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->autoSkew:Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;

    const/4 v0, 0x0

    .line 227
    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->lookupTableNo:I

    .line 228
    iput-boolean v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->overScan:Z

    .line 229
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_NONE:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    iput-object v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->doubleFeedLevel:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    .line 230
    iget-object v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne v0, v1, :cond_0

    .line 232
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->getAvailableScanPageCount()I

    move-result v0

    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->maxScanSheets:I

    .line 233
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->getAvailableScanPageCount()I

    move-result v0

    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->maxWriteSheets:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 235
    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->maxScanSheets:I

    .line 236
    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->maxWriteSheets:I

    :goto_0
    const/16 v0, 0x5a

    .line 238
    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->qualityHW:I

    .line 239
    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->qualitySW:I

    return-object p1
.end method

.method private margeScanI2Params(Lcom/epson/lib/escani2/ScanI2Params;Lcom/epson/iprint/shared/SharedParamScan;)V
    .locals 4
    .param p1    # Lcom/epson/lib/escani2/ScanI2Params;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/shared/SharedParamScan;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p2, :cond_0

    return-void

    .line 185
    :cond_0
    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_main()I

    move-result v0

    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    .line 186
    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_sub()I

    move-result v0

    iput v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    .line 188
    new-instance v0, Lcom/epson/lib/escani2/ScanSize;

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_main()I

    move-result v1

    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_sub()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/epson/lib/escani2/ScanSize;-><init>(IIII)V

    .line 190
    iput-object v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 192
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->COLOR_24BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    .line 193
    invoke-virtual {p2}, Lcom/epson/iprint/shared/SharedParamScan;->getScan_type()I

    move-result p2

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 198
    :pswitch_0
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    goto :goto_0

    .line 195
    :pswitch_1
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->GRAYSCALE_8BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    .line 200
    :goto_0
    iput-object v0, p1, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private probeScanner(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/scan/lib/EscanLibException;
        }
    .end annotation

    .line 474
    monitor-enter p0

    .line 475
    :try_start_0
    new-instance v0, Lepson/scan/lib/CancelableScannerProbe;

    invoke-direct {v0}, Lepson/scan/lib/CancelableScannerProbe;-><init>()V

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mCancelableScannerProbe:Lepson/scan/lib/CancelableScannerProbe;

    .line 476
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 477
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mCancelableScannerProbe:Lepson/scan/lib/CancelableScannerProbe;

    invoke-virtual {v0, p2, p1}, Lepson/scan/lib/CancelableScannerProbe;->probeScanner(Lepson/scan/activity/ScannerPropertyWrapper;Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 478
    monitor-enter p0

    const/4 p2, 0x0

    .line 479
    :try_start_1
    iput-object p2, p0, Lepson/scan/i2lib/I2ScanTask;->mCancelableScannerProbe:Lepson/scan/lib/CancelableScannerProbe;

    .line 480
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    .line 476
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p1
.end method

.method private sendLog(I)V
    .locals 3

    .line 368
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    if-nez v0, :cond_0

    return-void

    .line 376
    :cond_0
    iput p1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->connectionType:I

    .line 377
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanTask;->getScanFileList()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    iput p1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    .line 378
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    iget p1, p1, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    if-nez p1, :cond_1

    .line 379
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    const/16 v0, 0x2005

    iput v0, p1, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 381
    :cond_1
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    if-eqz p1, :cond_2

    .line 382
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getModelName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->printerName:Ljava/lang/String;

    .line 385
    :cond_2
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    iget p1, p1, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    if-lez p1, :cond_3

    .line 386
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanTask;->convertScanParams(Lcom/epson/lib/escani2/ScanI2Params;)Lepson/scan/lib/I1ScanParams;

    move-result-object p1

    .line 387
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result v0

    .line 388
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    iget-object v2, p0, Lepson/scan/i2lib/I2ScanTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-static {v1, p1, v2, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendScanI1Log(Landroid/content/Context;Lepson/scan/lib/I1ScanParams;Lcom/epson/iprint/prtlogger/CommonLog;Z)V

    :cond_3
    return-void
.end method


# virtual methods
.method public cancelScan()V
    .locals 1

    const/4 v0, 0x1

    .line 523
    iput-boolean v0, p0, Lepson/scan/i2lib/I2ScanTask;->mCancelRequested:Z

    .line 524
    monitor-enter p0

    .line 525
    :try_start_0
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mCancelableScannerProbe:Lepson/scan/lib/CancelableScannerProbe;

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mCancelableScannerProbe:Lepson/scan/lib/CancelableScannerProbe;

    invoke-virtual {v0}, Lepson/scan/lib/CancelableScannerProbe;->cancel()V

    .line 528
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Runner:Lcom/epson/lib/escani2/ScanI2Runner;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/ScanI2Runner;->cancelScan()V

    .line 531
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v0}, Lepson/scan/i2lib/ImageProcessController;->cancelThread()V

    return-void

    :catchall_0
    move-exception v0

    .line 528
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/scan/i2lib/I2ScanTask$TaskError;
    .locals 8

    const-string p1, ""

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 260
    :try_start_0
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanTask;->checkCancel()V

    .line 261
    invoke-static {}, Lepson/common/Utils;->isMediaMounted()Z

    move-result v2

    const/4 v3, 0x2

    if-nez v2, :cond_0

    .line 262
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanTask;->checkCancel()V

    .line 263
    new-instance v2, Lepson/scan/i2lib/I2ScanTask$TaskError;

    invoke-direct {v2, v3, v3}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V
    :try_end_0
    .catch Lepson/scan/i2lib/I2ScanTask$LocalCancel; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lepson/scan/lib/EscanLibException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 357
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v2

    .line 266
    :cond_0
    :try_start_1
    iget-object v2, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lepson/scan/i2lib/I2ScanTask;->getSaveDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    .line 268
    iget-object v4, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

    iget-object v5, p0, Lepson/scan/i2lib/I2ScanTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    .line 269
    invoke-virtual {v5}, Lepson/scan/activity/ScannerPropertyWrapper;->getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object v5

    iget-object v5, v5, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    .line 268
    invoke-direct {p0, v2, v4, v5}, Lepson/scan/i2lib/I2ScanTask;->checkScanParams(Ljava/io/File;Lcom/epson/lib/escani2/ScanI2Params;Lcom/epson/lib/escani2/ScannerI2Info;)I

    move-result v4

    const/16 v5, -0xca

    packed-switch v4, :pswitch_data_0

    .line 278
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanTask;->checkCancel()V

    goto/16 :goto_2

    .line 276
    :pswitch_0
    new-instance v2, Lepson/scan/i2lib/I2ScanTask$TaskError;

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V
    :try_end_1
    .catch Lepson/scan/i2lib/I2ScanTask$LocalCancel; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lepson/scan/lib/EscanLibException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 357
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v2

    .line 284
    :pswitch_1
    :try_start_2
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->isContinueScanning()Z

    move-result v3

    if-nez v3, :cond_1

    .line 286
    iget-object v3, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v3}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lepson/common/ExternalFileUtils;->clearTempFoler(Ljava/lang/String;)V

    .line 287
    invoke-static {}, Lepson/scan/activity/ScanContinueParam;->resetParameter()V

    .line 290
    :cond_1
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanTask;->checkCancel()V

    .line 291
    iget-object v2, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    iget-object v3, p0, Lepson/scan/i2lib/I2ScanTask;->mScannerPropertyWrapper:Lepson/scan/activity/ScannerPropertyWrapper;

    invoke-direct {p0, v2, v3}, Lepson/scan/i2lib/I2ScanTask;->probeScanner(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;)Ljava/lang/String;

    move-result-object p1

    .line 292
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanTask;->checkCancel()V

    if-nez p1, :cond_2

    .line 294
    new-instance v2, Lepson/scan/i2lib/I2ScanTask$TaskError;

    const/16 v3, -0xd6

    invoke-direct {v2, v1, v3}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V
    :try_end_2
    .catch Lepson/scan/i2lib/I2ScanTask$LocalCancel; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lepson/scan/lib/EscanLibException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 357
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v2

    .line 298
    :cond_2
    :try_start_3
    iget-object v2, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v2}, Lepson/scan/i2lib/ImageProcessController;->startThread()V

    .line 303
    iget-object v2, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Params:Lcom/epson/lib/escani2/ScanI2Params;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/ScanI2Params;->clone()Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v2

    .line 304
    iget-object v3, v2, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    sget-object v4, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    if-ne v3, v4, :cond_3

    .line 305
    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->GRAYSCALE_8BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    iput-object v3, v2, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    .line 307
    :cond_3
    iget-object v3, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v3}, Lepson/scan/i2lib/ImageProcessController;->getScanOutFilenameFormat()[Ljava/lang/String;

    move-result-object v3

    .line 308
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanTask;->checkCancel()V

    .line 309
    iget-object v4, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Runner:Lcom/epson/lib/escani2/ScanI2Runner;

    new-instance v6, Lepson/scan/i2lib/I2ScanTask$TaskObserver;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lepson/scan/i2lib/I2ScanTask$TaskObserver;-><init>(Lepson/scan/i2lib/I2ScanTask;Lepson/scan/i2lib/I2ScanTask$1;)V

    invoke-virtual {v4, p1, v2, v3, v6}, Lcom/epson/lib/escani2/ScanI2Runner;->doScan(Ljava/lang/String;Lcom/epson/lib/escani2/ScanI2Params;[Ljava/lang/String;Lcom/epson/lib/escani2/EscanI2Lib$Observer;)I

    move-result v2

    .line 312
    iget-boolean v3, p0, Lepson/scan/i2lib/I2ScanTask;->mCancelRequested:Z

    if-eqz v3, :cond_4

    .line 314
    iget-object v3, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v3}, Lepson/scan/i2lib/ImageProcessController;->cancelThread()V

    const/4 v3, 0x0

    goto :goto_0

    .line 317
    :cond_4
    iget-object v3, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v3}, Lepson/scan/i2lib/ImageProcessController;->requestFinishThread()V
    :try_end_3
    .catch Lepson/scan/i2lib/I2ScanTask$LocalCancel; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lepson/scan/lib/EscanLibException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v3, 0x1

    :goto_0
    if-eqz v2, :cond_5

    if-eq v2, v1, :cond_5

    .line 322
    :try_start_4
    new-instance v4, Lepson/scan/i2lib/I2ScanTask$TaskError;

    invoke-direct {v4, v1, v2}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V

    goto :goto_1

    :cond_5
    move-object v4, v7

    .line 326
    :goto_1
    iget-object v6, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v6}, Lepson/scan/i2lib/ImageProcessController;->waitUntilThreadEnd()Z

    move-result v6

    if-nez v6, :cond_6

    if-nez v4, :cond_6

    .line 330
    new-instance v4, Lepson/scan/i2lib/I2ScanTask$TaskError;

    invoke-direct {v4, v1, v5}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V
    :try_end_4
    .catch Lepson/scan/i2lib/I2ScanTask$LocalCancel; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lepson/scan/lib/EscanLibException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 335
    :cond_6
    :try_start_5
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanTask;->convertFile()V
    :try_end_5
    .catch Lepson/scan/i2lib/I2ScanTask$LocalCancel; {:try_start_5 .. :try_end_5} :catch_5
    .catch Lepson/scan/lib/EscanLibException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v4, :cond_7

    .line 357
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v4

    .line 340
    :cond_7
    :try_start_6
    iget-object v3, p0, Lepson/scan/i2lib/I2ScanTask;->mResScanFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 342
    new-instance v3, Lepson/scan/i2lib/I2ScanTask$TaskError;

    invoke-direct {v3, v1, v2}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V
    :try_end_6
    .catch Lepson/scan/i2lib/I2ScanTask$LocalCancel; {:try_start_6 .. :try_end_6} :catch_5
    .catch Lepson/scan/lib/EscanLibException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 357
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v3

    .line 357
    :cond_8
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v7

    :catchall_0
    move-exception v1

    move v0, v3

    goto :goto_5

    :catch_0
    move-exception v2

    move v0, v3

    goto :goto_3

    :catch_1
    move v0, v3

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v1, v0

    const/4 v0, 0x1

    goto :goto_5

    :catch_2
    move-exception v2

    const/4 v0, 0x1

    goto :goto_3

    :catch_3
    const/4 v0, 0x1

    goto :goto_4

    .line 280
    :goto_2
    :try_start_7
    new-instance v2, Lepson/scan/i2lib/I2ScanTask$TaskError;

    invoke-direct {v2, v1, v5}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V
    :try_end_7
    .catch Lepson/scan/i2lib/I2ScanTask$LocalCancel; {:try_start_7 .. :try_end_7} :catch_5
    .catch Lepson/scan/lib/EscanLibException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 357
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v2

    :catchall_2
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v2

    .line 349
    :goto_3
    :try_start_8
    iget-object v3, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v3}, Lepson/scan/i2lib/ImageProcessController;->cancelThread()V

    .line 350
    new-instance v3, Lepson/scan/i2lib/I2ScanTask$TaskError;

    invoke-virtual {v2}, Lepson/scan/lib/EscanLibException;->getEscanLibErrorCode()I

    move-result v2

    invoke-direct {v3, v1, v2}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    if-eqz v0, :cond_9

    .line 353
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v0}, Lepson/scan/i2lib/ImageProcessController;->cancelThread()V

    .line 357
    :cond_9
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v3

    .line 346
    :catch_5
    :goto_4
    :try_start_9
    new-instance v2, Lepson/scan/i2lib/I2ScanTask$TaskError;

    invoke-direct {v2, v1, v1}, Lepson/scan/i2lib/I2ScanTask$TaskError;-><init>(II)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    if-eqz v0, :cond_a

    .line 353
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v0}, Lepson/scan/i2lib/ImageProcessController;->cancelThread()V

    .line 357
    :cond_a
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    return-object v2

    :goto_5
    if-eqz v0, :cond_b

    .line 353
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v0}, Lepson/scan/i2lib/ImageProcessController;->cancelThread()V

    .line 357
    :cond_b
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result v0

    .line 358
    iget-object v2, p0, Lepson/scan/i2lib/I2ScanTask;->mApplicationContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lepson/scan/lib/ScannerConnection;->disconnectIfSimpleAp(Landroid/content/Context;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanTask;->sendLog(I)V

    .line 361
    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/scan/i2lib/I2ScanTask;->doInBackground([Ljava/lang/Void;)Lepson/scan/i2lib/I2ScanTask$TaskError;

    move-result-object p1

    return-object p1
.end method

.method public getScanFile(II)Ljava/io/File;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 119
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Runner:Lcom/epson/lib/escani2/ScanI2Runner;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 123
    :cond_0
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v0, p1, p2}, Lepson/scan/i2lib/ImageProcessController;->getAllProcessFile(II)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method public getScanFileList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mResScanFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getScanPages()[I
    .locals 1

    .line 250
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mScanI2Runner:Lcom/epson/lib/escani2/ScanI2Runner;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/ScanI2Runner;->getScanPages()[I

    move-result-object v0

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method

.method onImageProcessEnd(II)V
    .locals 1

    .line 557
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mScanResultReceiver:Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;

    if-eqz v0, :cond_0

    .line 558
    invoke-interface {v0, p1, p2}, Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;->onProgressUpdate(II)V

    :cond_0
    return-void
.end method

.method protected onPostExecute(Lepson/scan/i2lib/I2ScanTask$TaskError;)V
    .locals 1

    .line 508
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mScanResultReceiver:Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;

    if-eqz v0, :cond_0

    .line 509
    invoke-interface {v0, p1}, Lepson/scan/i2lib/I2ScanTask$ScanResultReceiver;->onScanEnd(Lepson/scan/i2lib/I2ScanTask$TaskError;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 53
    check-cast p1, Lepson/scan/i2lib/I2ScanTask$TaskError;

    invoke-virtual {p0, p1}, Lepson/scan/i2lib/I2ScanTask;->onPostExecute(Lepson/scan/i2lib/I2ScanTask$TaskError;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method

.method scanProgressCallback(II)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 502
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v0, p1, p2}, Lepson/scan/i2lib/ImageProcessController;->putScanResult(II)Z

    return-void
.end method

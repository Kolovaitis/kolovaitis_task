.class public Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;
.super Ljava/lang/Object;
.source "ImageProcessController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/i2lib/ImageProcessController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageProcessInfo"
.end annotation


# static fields
.field public static PROCESS_TYPE_BLACK_AND_WHITE:I = 0x20

.field public static PROCESS_TYPE_EMPTY:I = 0x0

.field public static PROCESS_TYPE_END:I = 0x1

.field public static PROCESS_TYPE_ROTATE_180:I = 0x10


# instance fields
.field private mInputFilename:Ljava/lang/String;

.field private mOutputFilename:Ljava/lang/String;

.field private mPage:I

.field private mProcessType:I

.field private mSide:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .line 396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397
    iput p1, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mProcessType:I

    .line 398
    iput-object p2, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mInputFilename:Ljava/lang/String;

    .line 399
    iput-object p3, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mOutputFilename:Ljava/lang/String;

    .line 400
    iput p4, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mSide:I

    .line 401
    iput p5, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mPage:I

    return-void
.end method

.method public static getEndRequestInfo()Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 392
    new-instance v6, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;

    sget v1, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_END:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    return-object v6
.end method


# virtual methods
.method public blackAndWhite()Z
    .locals 2

    .line 421
    iget v0, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mProcessType:I

    sget v1, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_BLACK_AND_WHITE:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public endRequested()Z
    .locals 2

    .line 425
    iget v0, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mProcessType:I

    sget v1, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_END:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getInputFilename()Ljava/lang/String;
    .locals 1

    .line 405
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mInputFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputFilename()Ljava/lang/String;
    .locals 1

    .line 409
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mOutputFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getPage()I
    .locals 1

    .line 433
    iget v0, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mPage:I

    return v0
.end method

.method public getProcessType()I
    .locals 1

    .line 413
    iget v0, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mProcessType:I

    return v0
.end method

.method public getSide()I
    .locals 1

    .line 429
    iget v0, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mSide:I

    return v0
.end method

.method public rotate180()Z
    .locals 2

    .line 417
    iget v0, p0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->mProcessType:I

    sget v1, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_ROTATE_180:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

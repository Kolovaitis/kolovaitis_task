.class public Lepson/scan/i2lib/ImageProcessController;
.super Ljava/lang/Object;
.source "ImageProcessController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ImageProcessController"


# instance fields
.field private mBackSideRotate180:Z

.field private mBlackAndWhiteThresh:I

.field private mBlockingDeque:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque<",
            "Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mCancelRequested:Z

.field private mConvertBlackAndWhite:Z

.field private mDuplex:Z

.field private mError:Z

.field private mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

.field private mImageProcessOutFilenameFormat:[Ljava/lang/String;

.field private mImageProcessThread:Lepson/scan/i2lib/ImageProcessThread;

.field private final mProcessEndLatch:Ljava/util/concurrent/CountDownLatch;

.field private mScanOutFilenameFormat:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lepson/scan/i2lib/I2ScanTask;ZIIZI)V
    .locals 2
    .param p1    # Lepson/scan/i2lib/I2ScanTask;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mBlockingDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 76
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mProcessEndLatch:Ljava/util/concurrent/CountDownLatch;

    .line 78
    iput-object p1, p0, Lepson/scan/i2lib/ImageProcessController;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    .line 79
    iput-boolean p2, p0, Lepson/scan/i2lib/ImageProcessController;->mDuplex:Z

    .line 80
    iput-boolean p5, p0, Lepson/scan/i2lib/ImageProcessController;->mConvertBlackAndWhite:Z

    .line 81
    iput p6, p0, Lepson/scan/i2lib/ImageProcessController;->mBlackAndWhiteThresh:I

    .line 83
    invoke-direct {p0, p3, p4}, Lepson/scan/i2lib/ImageProcessController;->setBackSideRotation(II)V

    return-void
.end method

.method private checkStatus()V
    .locals 2

    .line 229
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mScanOutFilenameFormat:[Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    .line 230
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init() is not called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private checkThreadStatus()V
    .locals 2

    .line 235
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mImageProcessThread:Lepson/scan/i2lib/ImageProcessThread;

    if-eqz v0, :cond_0

    return-void

    .line 236
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "startThread() is not called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static geDateFormat(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/util/Calendar;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 265
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMdd_HHmmss_SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 266
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getFile(Ljava/lang/String;I)Ljava/io/File;
    .locals 4

    .line 224
    new-instance v0, Ljava/io/File;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, p0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getProcessType(II)I
    .locals 1

    .line 350
    sget p2, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_EMPTY:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 352
    iget-boolean p1, p0, Lepson/scan/i2lib/ImageProcessController;->mBackSideRotate180:Z

    if-eqz p1, :cond_0

    .line 353
    sget p1, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_ROTATE_180:I

    or-int/2addr p2, p1

    .line 356
    :cond_0
    iget-boolean p1, p0, Lepson/scan/i2lib/ImageProcessController;->mConvertBlackAndWhite:Z

    if-eqz p1, :cond_1

    .line 357
    sget p1, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_BLACK_AND_WHITE:I

    or-int/2addr p2, p1

    :cond_1
    return p2
.end method

.method private makeScanFilenameFormat(Ljava/io/File;Ljava/util/Calendar;)V
    .locals 6
    .param p1    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Calendar;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 245
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lepson/scan/i2lib/ImageProcessController;->geDateFormat(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "a_%d.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 246
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lepson/scan/i2lib/ImageProcessController;->geDateFormat(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "b_%d.jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x2

    .line 247
    new-array v3, v2, [Ljava/lang/String;

    .line 248
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v3, v1

    iput-object v3, p0, Lepson/scan/i2lib/ImageProcessController;->mScanOutFilenameFormat:[Ljava/lang/String;

    .line 251
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lepson/scan/i2lib/ImageProcessController;->geDateFormat(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "a2_%d.jpg"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 252
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lepson/scan/i2lib/ImageProcessController;->geDateFormat(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "b2_%d.jpg"

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v3, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 253
    new-array p1, v2, [Ljava/lang/String;

    .line 254
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v1

    iput-object p1, p0, Lepson/scan/i2lib/ImageProcessController;->mImageProcessOutFilenameFormat:[Ljava/lang/String;

    return-void
.end method

.method private setBackSideRotation(II)V
    .locals 2

    .line 90
    iget-boolean v0, p0, Lepson/scan/i2lib/ImageProcessController;->mDuplex:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 98
    :goto_0
    iput-boolean v0, p0, Lepson/scan/i2lib/ImageProcessController;->mBackSideRotate180:Z

    goto :goto_2

    :cond_1
    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 93
    :goto_1
    iput-boolean v0, p0, Lepson/scan/i2lib/ImageProcessController;->mBackSideRotate180:Z

    goto :goto_2

    .line 101
    :cond_3
    iput-boolean v1, p0, Lepson/scan/i2lib/ImageProcessController;->mBackSideRotate180:Z

    :goto_2
    return-void
.end method


# virtual methods
.method public cancelThread()V
    .locals 2

    const/4 v0, 0x1

    .line 149
    iput-boolean v0, p0, Lepson/scan/i2lib/ImageProcessController;->mCancelRequested:Z

    .line 151
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mImageProcessThread:Lepson/scan/i2lib/ImageProcessThread;

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mProcessEndLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mBlockingDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->clear()V

    .line 159
    :try_start_0
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mBlockingDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-static {}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->getEndRequestInfo()Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->putFirst(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 164
    :catch_0
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mProcessEndLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :goto_0
    return-void
.end method

.method public checkNexImageProcessRequest()Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 320
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mBlockingDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->takeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;

    return-object v0
.end method

.method public getAllProcessFile(II)Ljava/io/File;
    .locals 2

    .line 216
    invoke-direct {p0}, Lepson/scan/i2lib/ImageProcessController;->checkStatus()V

    .line 218
    invoke-direct {p0, p1, p2}, Lepson/scan/i2lib/ImageProcessController;->getProcessType(II)I

    move-result v0

    sget v1, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_EMPTY:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mScanOutFilenameFormat:[Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mImageProcessOutFilenameFormat:[Ljava/lang/String;

    .line 220
    :goto_0
    aget-object p1, v0, p1

    invoke-static {p1, p2}, Lepson/scan/i2lib/ImageProcessController;->getFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method getScanFiles([I)Ljava/util/ArrayList;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 113
    iget-boolean v0, p0, Lepson/scan/i2lib/ImageProcessController;->mDuplex:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 115
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v0, :cond_3

    .line 120
    aget v5, p1, v4

    if-lt v3, v5, :cond_1

    return-object v1

    .line 124
    :cond_1
    invoke-virtual {p0, v4, v3}, Lepson/scan/i2lib/ImageProcessController;->getAllProcessFile(II)Ljava/io/File;

    move-result-object v5

    .line 125
    invoke-virtual {v5}, Ljava/io/File;->canRead()Z

    move-result v6

    if-nez v6, :cond_2

    return-object v1

    .line 128
    :cond_2
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getScanOutFilenameFormat()[Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 205
    invoke-direct {p0}, Lepson/scan/i2lib/ImageProcessController;->checkStatus()V

    .line 206
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mScanOutFilenameFormat:[Ljava/lang/String;

    return-object v0
.end method

.method public init(Ljava/io/File;Ljava/util/Calendar;)V
    .locals 0

    .line 135
    invoke-direct {p0, p1, p2}, Lepson/scan/i2lib/ImageProcessController;->makeScanFilenameFormat(Ljava/io/File;Ljava/util/Calendar;)V

    return-void
.end method

.method public putImageProcessEnd(Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;Z)V
    .locals 1
    .param p1    # Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 328
    invoke-virtual {p1}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->endRequested()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 338
    :cond_0
    iget-object p2, p0, Lepson/scan/i2lib/ImageProcessController;->mI2ScanTask:Lepson/scan/i2lib/I2ScanTask;

    .line 339
    invoke-virtual {p1}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->getSide()I

    move-result v0

    invoke-virtual {p1}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->getPage()I

    move-result p1

    .line 338
    invoke-virtual {p2, v0, p1}, Lepson/scan/i2lib/I2ScanTask;->onImageProcessEnd(II)V

    return-void

    :cond_1
    :goto_0
    xor-int/lit8 p1, p2, 0x1

    .line 330
    iput-boolean p1, p0, Lepson/scan/i2lib/ImageProcessController;->mError:Z

    .line 333
    iget-object p1, p0, Lepson/scan/i2lib/ImageProcessController;->mProcessEndLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public putScanResult(II)Z
    .locals 8

    .line 278
    invoke-direct {p0}, Lepson/scan/i2lib/ImageProcessController;->checkThreadStatus()V

    .line 283
    iget-boolean v0, p0, Lepson/scan/i2lib/ImageProcessController;->mCancelRequested:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 289
    :cond_0
    invoke-direct {p0, p1, p2}, Lepson/scan/i2lib/ImageProcessController;->getProcessType(II)I

    move-result v3

    .line 291
    new-instance v0, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;

    iget-object v2, p0, Lepson/scan/i2lib/ImageProcessController;->mScanOutFilenameFormat:[Ljava/lang/String;

    aget-object v2, v2, p1

    .line 292
    invoke-static {v2, p2}, Lepson/scan/i2lib/ImageProcessController;->getFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    sget v2, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->PROCESS_TYPE_EMPTY:I

    if-ne v3, v2, :cond_1

    const/4 v2, 0x0

    move-object v5, v2

    goto :goto_0

    .line 295
    :cond_1
    invoke-virtual {p0, p1, p2}, Lepson/scan/i2lib/ImageProcessController;->getAllProcessFile(II)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    move-object v5, v2

    :goto_0
    move-object v2, v0

    move v6, p1

    move v7, p2

    invoke-direct/range {v2 .. v7}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    .line 301
    :try_start_0
    iget-object p1, p0, Lepson/scan/i2lib/ImageProcessController;->mBlockingDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->putLast(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 307
    :catch_0
    iget-object p1, p0, Lepson/scan/i2lib/ImageProcessController;->mProcessEndLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public requestFinishThread()V
    .locals 2

    .line 173
    invoke-direct {p0}, Lepson/scan/i2lib/ImageProcessController;->checkThreadStatus()V

    .line 175
    :try_start_0
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mBlockingDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-static {}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->getEndRequestInfo()Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->putLast(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 180
    :catch_0
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mProcessEndLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :goto_0
    return-void
.end method

.method public startThread()V
    .locals 2

    .line 139
    invoke-direct {p0}, Lepson/scan/i2lib/ImageProcessController;->checkStatus()V

    .line 140
    new-instance v0, Lepson/scan/i2lib/ImageProcessThread;

    iget v1, p0, Lepson/scan/i2lib/ImageProcessController;->mBlackAndWhiteThresh:I

    invoke-direct {v0, p0, v1}, Lepson/scan/i2lib/ImageProcessThread;-><init>(Lepson/scan/i2lib/ImageProcessController;I)V

    iput-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mImageProcessThread:Lepson/scan/i2lib/ImageProcessThread;

    .line 141
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mImageProcessThread:Lepson/scan/i2lib/ImageProcessThread;

    invoke-virtual {v0}, Lepson/scan/i2lib/ImageProcessThread;->start()V

    return-void
.end method

.method public waitUntilThreadEnd()Z
    .locals 1

    .line 191
    :try_start_0
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessController;->mProcessEndLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    iget-boolean v0, p0, Lepson/scan/i2lib/ImageProcessController;->mError:Z

    xor-int/lit8 v0, v0, 0x1

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

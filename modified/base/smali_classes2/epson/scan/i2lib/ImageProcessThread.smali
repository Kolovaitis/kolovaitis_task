.class public Lepson/scan/i2lib/ImageProcessThread;
.super Ljava/lang/Thread;
.source "ImageProcessThread.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ImageProcessThread"


# instance fields
.field private final mBlackAndWhiteThreshold:I

.field private final mImageProcessController:Lepson/scan/i2lib/ImageProcessController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "opencv_java3"

    .line 20
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lepson/scan/i2lib/ImageProcessController;I)V
    .locals 1
    .param p1    # Lepson/scan/i2lib/ImageProcessController;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "ImageProcessThread"

    .line 25
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lepson/scan/i2lib/ImageProcessThread;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    .line 27
    iput p2, p0, Lepson/scan/i2lib/ImageProcessThread;->mBlackAndWhiteThreshold:I

    return-void
.end method

.method private static blackAndWhite(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 8
    .param p0    # Lorg/opencv/core/Mat;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lorg/opencv/core/Mat;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 133
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->width()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->height()I

    move-result v0

    if-lez v0, :cond_0

    int-to-double v3, p2

    const-wide v5, 0x406fe00000000000L    # 255.0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    .line 137
    invoke-static/range {v1 .. v7}, Lorg/opencv/imgproc/Imgproc;->threshold(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDI)D

    return-void

    .line 134
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "orgImage size error"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static openImageFile(Ljava/lang/String;)Lorg/opencv/core/Mat;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, -0x1

    .line 110
    invoke-static {p0, v0}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;I)Lorg/opencv/core/Mat;

    move-result-object p0

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 115
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->width()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->height()I

    move-result v1

    if-gtz v1, :cond_1

    goto :goto_0

    :cond_1
    return-object p0

    :cond_2
    :goto_0
    return-object v0
.end method

.method private processImage(Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;I)Z
    .locals 5
    .param p1    # Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 66
    invoke-virtual {p1}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->rotate180()Z

    move-result v0

    .line 67
    invoke-virtual {p1}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->blackAndWhite()Z

    move-result v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_4

    .line 70
    :cond_0
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    .line 71
    invoke-virtual {p1}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->getInputFilename()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lepson/scan/i2lib/ImageProcessThread;->openImageFile(Ljava/lang/String;)Lorg/opencv/core/Mat;

    move-result-object v3

    const/4 v4, 0x0

    if-nez v3, :cond_1

    return v4

    :cond_1
    if-eqz v1, :cond_2

    .line 78
    invoke-static {v3, v2, p2}, Lepson/scan/i2lib/ImageProcessThread;->blackAndWhite(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    if-eqz v0, :cond_2

    .line 81
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    .line 83
    new-instance p2, Lorg/opencv/core/Mat;

    invoke-direct {p2}, Lorg/opencv/core/Mat;-><init>()V

    goto :goto_0

    :cond_2
    move-object p2, v2

    move-object v2, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 88
    invoke-static {v2, p2}, Lepson/scan/i2lib/ImageProcessThread;->rotate180(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V

    .line 91
    :cond_3
    invoke-virtual {p1}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->getOutputFilename()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p2}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    move-result p1

    .line 92
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->release()V

    .line 93
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    if-nez p1, :cond_4

    return v4

    :cond_4
    const/4 p1, 0x1

    return p1
.end method

.method private static rotate180(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 1
    .param p0    # Lorg/opencv/core/Mat;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lorg/opencv/core/Mat;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 125
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->width()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->height()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, -0x1

    .line 129
    invoke-static {p0, p1, v0}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    return-void

    .line 126
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "orgImage size error"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 32
    :goto_0
    invoke-virtual {p0}, Lepson/scan/i2lib/ImageProcessThread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 35
    :try_start_0
    iget-object v0, p0, Lepson/scan/i2lib/ImageProcessThread;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v0}, Lepson/scan/i2lib/ImageProcessController;->checkNexImageProcessRequest()Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    invoke-virtual {v0}, Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;->endRequested()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, p0, Lepson/scan/i2lib/ImageProcessThread;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v1, v0, v2}, Lepson/scan/i2lib/ImageProcessController;->putImageProcessEnd(Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;Z)V

    return-void

    .line 49
    :cond_0
    iget v1, p0, Lepson/scan/i2lib/ImageProcessThread;->mBlackAndWhiteThreshold:I

    invoke-direct {p0, v0, v1}, Lepson/scan/i2lib/ImageProcessThread;->processImage(Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 50
    iget-object v1, p0, Lepson/scan/i2lib/ImageProcessThread;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lepson/scan/i2lib/ImageProcessController;->putImageProcessEnd(Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;Z)V

    return-void

    .line 55
    :cond_1
    iget-object v1, p0, Lepson/scan/i2lib/ImageProcessThread;->mImageProcessController:Lepson/scan/i2lib/ImageProcessController;

    invoke-virtual {v1, v0, v2}, Lepson/scan/i2lib/ImageProcessController;->putImageProcessEnd(Lepson/scan/i2lib/ImageProcessController$ImageProcessInfo;Z)V

    goto :goto_0

    :catch_0
    return-void

    :cond_2
    return-void
.end method

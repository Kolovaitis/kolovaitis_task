.class public Lepson/scan/i2lib/I2ScanParamManager;
.super Ljava/lang/Object;
.source "I2ScanParamManager.java"


# static fields
.field static final DEFAULT_RESOLUTION:I = 0x96

.field private static final I2SCAN_ALL_INFO_FILE_NAME:Ljava/lang/String; = "i2_all_info"

.field private static final I2SCAN_PARAM_FILE_NAME:Ljava/lang/String; = "i2_scan_params.bin"

.field private static final TAG:Ljava/lang/String; = "I2ScanParamManager"

.field private static sI2ScanParamManager:Lepson/scan/i2lib/I2ScanParamManager;


# direct methods
.method protected constructor <init>()V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteAllInfoFile(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 267
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getInstance()Lepson/scan/i2lib/I2ScanParamManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lepson/scan/i2lib/I2ScanParamManager;->deleteAllInfoFileNonStatic(Landroid/content/Context;)V

    return-void
.end method

.method public static deleteScanI2Params(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 133
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getInstance()Lepson/scan/i2lib/I2ScanParamManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lepson/scan/i2lib/I2ScanParamManager;->deleteAllInfoFileNonStatic(Landroid/content/Context;)V

    return-void
.end method

.method public static getDefaultScanI2Params()Lcom/epson/lib/escani2/ScanI2Params;
    .locals 3

    .line 148
    new-instance v0, Lcom/epson/lib/escani2/ScanI2Params;

    invoke-direct {v0}, Lcom/epson/lib/escani2/ScanI2Params;-><init>()V

    const/16 v1, 0x96

    .line 150
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    .line 151
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    .line 153
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    .line 154
    invoke-static {}, Lcom/epson/lib/escani2/ScanSize;->getLocaleDefaultSize()Lcom/epson/lib/escani2/ScanSize;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 156
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->COLOR_24BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    .line 157
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->GAMMA_180:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    .line 158
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;->FALSE:Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->autoCrop:Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;

    .line 159
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;->FALSE:Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->autoSkew:Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;

    const/4 v1, 0x0

    .line 160
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->lookupTableNo:I

    .line 162
    iput-boolean v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    .line 163
    iput-boolean v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->overScan:Z

    const/16 v2, 0x80

    .line 164
    iput v2, v0, Lcom/epson/lib/escani2/ScanI2Params;->density:I

    .line 165
    iput-boolean v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->densityChangeable:Z

    .line 167
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_NONE:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->doubleFeedLevel:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    const/16 v1, 0x1e

    .line 170
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->maxScanSheets:I

    .line 173
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->maxWriteSheets:I

    const/16 v1, 0x5a

    .line 175
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->qualityHW:I

    .line 176
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->qualitySW:I

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lepson/scan/i2lib/I2ScanParamManager;
    .locals 2

    const-class v0, Lepson/scan/i2lib/I2ScanParamManager;

    monitor-enter v0

    .line 41
    :try_start_0
    sget-object v1, Lepson/scan/i2lib/I2ScanParamManager;->sI2ScanParamManager:Lepson/scan/i2lib/I2ScanParamManager;

    if-nez v1, :cond_0

    .line 42
    new-instance v1, Lepson/scan/i2lib/I2ScanParamManager;

    invoke-direct {v1}, Lepson/scan/i2lib/I2ScanParamManager;-><init>()V

    sput-object v1, Lepson/scan/i2lib/I2ScanParamManager;->sI2ScanParamManager:Lepson/scan/i2lib/I2ScanParamManager;

    .line 44
    :cond_0
    sget-object v1, Lepson/scan/i2lib/I2ScanParamManager;->sI2ScanParamManager:Lepson/scan/i2lib/I2ScanParamManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static loadI2AllInfo(Landroid/content/Context;)Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 234
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getInstance()Lepson/scan/i2lib/I2ScanParamManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadI2AllInfoNonStatic(Landroid/content/Context;)Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object p0

    return-object p0
.end method

.method public static loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 99
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getInstance()Lepson/scan/i2lib/I2ScanParamManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2ParamsNonStatic(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object p0

    return-object p0
.end method

.method protected static replaceInstance(Lepson/scan/i2lib/I2ScanParamManager;)V
    .locals 0
    .param p0    # Lepson/scan/i2lib/I2ScanParamManager;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 52
    sput-object p0, Lepson/scan/i2lib/I2ScanParamManager;->sI2ScanParamManager:Lepson/scan/i2lib/I2ScanParamManager;

    return-void
.end method

.method public static saveI2AllInfo(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 201
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getInstance()Lepson/scan/i2lib/I2ScanParamManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lepson/scan/i2lib/I2ScanParamManager;->saveI2AllInfoNonStatic(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    return-void
.end method

.method public static saveParams(Landroid/content/Context;Lcom/epson/lib/escani2/ScanI2Params;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/epson/lib/escani2/ScanI2Params;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 66
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getInstance()Lepson/scan/i2lib/I2ScanParamManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lepson/scan/i2lib/I2ScanParamManager;->saveParamsNonStatic(Landroid/content/Context;Lcom/epson/lib/escani2/ScanI2Params;)V

    return-void
.end method

.method public static updateScanSize(Landroid/content/Context;Lcom/epson/lib/escani2/ScanSize;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getInstance()Lepson/scan/i2lib/I2ScanParamManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lepson/scan/i2lib/I2ScanParamManager;->updateScanSizeNonStatic(Landroid/content/Context;Lcom/epson/lib/escani2/ScanSize;)V

    return-void
.end method


# virtual methods
.method protected deleteAllInfoFileNonStatic(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const-string v0, "i2_all_info"

    .line 272
    invoke-virtual {p1, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    .line 273
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_0
    return-void
.end method

.method protected deleteScanI2ParamsNonStatic(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const-string v0, "i2_scan_params.bin"

    .line 138
    invoke-virtual {p1, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_0
    return-void
.end method

.method protected loadI2AllInfoNonStatic(Landroid/content/Context;)Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    .line 243
    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    const-string v2, "i2_all_info"

    invoke-virtual {p1, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 244
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-object v0, p1

    goto :goto_4

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_0

    :catch_1
    nop

    goto :goto_1

    :catch_2
    nop

    goto :goto_3

    :catchall_1
    move-exception p1

    :goto_0
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 259
    :catch_3
    :cond_0
    throw p1

    :catch_4
    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_1

    .line 256
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_4

    :catch_5
    move-object v1, v0

    :goto_3
    if-eqz v1, :cond_1

    goto :goto_2

    :catch_6
    :cond_1
    :goto_4
    return-object v0
.end method

.method protected loadScanI2ParamsNonStatic(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    .line 107
    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    const-string v2, "i2_scan_params.bin"

    invoke-virtual {p1, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 108
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/lib/escani2/ScanI2Params;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return-object p1

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_2

    :catch_1
    move-object v0, v1

    goto :goto_0

    :catch_2
    move-object v0, v1

    goto :goto_1

    :catchall_1
    move-exception p1

    goto :goto_2

    .line 117
    :catch_3
    :goto_0
    :try_start_3
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getDefaultScanI2Params()Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v0, :cond_0

    .line 121
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :cond_0
    return-object p1

    .line 111
    :catch_5
    :goto_1
    :try_start_5
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getDefaultScanI2Params()Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v0, :cond_1

    .line 121
    :try_start_6
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :cond_1
    return-object p1

    :goto_2
    if-eqz v0, :cond_2

    :try_start_7
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    .line 124
    :catch_7
    :cond_2
    throw p1
.end method

.method protected saveI2AllInfoNonStatic(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 211
    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    const-string v2, "i2_all_info"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 212
    :try_start_1
    invoke-virtual {v1, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    if-nez v0, :cond_0

    return-void

    .line 224
    :cond_0
    throw v0

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_1

    :catchall_1
    move-exception p1

    :goto_1
    if-eqz v0, :cond_1

    .line 216
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 219
    :catch_1
    :cond_1
    throw p1
.end method

.method protected saveParamsNonStatic(Landroid/content/Context;Lcom/epson/lib/escani2/ScanI2Params;)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/lib/escani2/ScanI2Params;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 75
    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    const-string v2, "i2_scan_params.bin"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 76
    :try_start_1
    invoke-virtual {v1, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    if-nez v0, :cond_0

    return-void

    .line 88
    :cond_0
    throw v0

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_1

    :catchall_1
    move-exception p1

    :goto_1
    if-eqz v0, :cond_1

    .line 80
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 83
    :catch_1
    :cond_1
    throw p1
.end method

.method protected updateScanSizeNonStatic(Landroid/content/Context;Lcom/epson/lib/escani2/ScanSize;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    invoke-static {p1}, Lepson/scan/i2lib/I2ScanParamManager;->loadScanI2Params(Landroid/content/Context;)Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v0

    .line 191
    iput-object p2, v0, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 192
    invoke-static {p1, v0}, Lepson/scan/i2lib/I2ScanParamManager;->saveParams(Landroid/content/Context;Lcom/epson/lib/escani2/ScanI2Params;)V

    return-void
.end method

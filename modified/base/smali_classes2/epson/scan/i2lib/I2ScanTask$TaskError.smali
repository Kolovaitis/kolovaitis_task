.class public Lepson/scan/i2lib/I2ScanTask$TaskError;
.super Ljava/lang/Object;
.source "I2ScanTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/i2lib/I2ScanTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TaskError"
.end annotation


# static fields
.field public static final ERROR_SD_CARD_NOT_MOUNTED:I = 0x2

.field public static final ESC_I2_ERROR:I = 0x1

.field public static final OTHER_ERROR:I = 0x2

.field public static final OTHER_ERROR_INVALID_SCAN_AREA:I = 0x3


# instance fields
.field public error1:I

.field public error2:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 597
    iput p1, p0, Lepson/scan/i2lib/I2ScanTask$TaskError;->error1:I

    .line 598
    iput p2, p0, Lepson/scan/i2lib/I2ScanTask$TaskError;->error2:I

    return-void
.end method

.method public static isInvalidScanArea(Lepson/scan/i2lib/I2ScanTask$TaskError;)Z
    .locals 3
    .param p0    # Lepson/scan/i2lib/I2ScanTask$TaskError;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 610
    :cond_0
    iget v1, p0, Lepson/scan/i2lib/I2ScanTask$TaskError;->error1:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget p0, p0, Lepson/scan/i2lib/I2ScanTask$TaskError;->error2:I

    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public isCancel()Z
    .locals 2

    .line 602
    iget v0, p0, Lepson/scan/i2lib/I2ScanTask$TaskError;->error1:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lepson/scan/i2lib/I2ScanTask$TaskError;->error2:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.class Lepson/scan/i2lib/I2ScanTask$TaskObserver;
.super Ljava/lang/Object;
.source "I2ScanTask.java"

# interfaces
.implements Lcom/epson/lib/escani2/EscanI2Lib$Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/scan/i2lib/I2ScanTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaskObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/scan/i2lib/I2ScanTask;


# direct methods
.method private constructor <init>(Lepson/scan/i2lib/I2ScanTask;)V
    .locals 0

    .line 563
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanTask$TaskObserver;->this$0:Lepson/scan/i2lib/I2ScanTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/scan/i2lib/I2ScanTask;Lepson/scan/i2lib/I2ScanTask$1;)V
    .locals 0

    .line 563
    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanTask$TaskObserver;-><init>(Lepson/scan/i2lib/I2ScanTask;)V

    return-void
.end method


# virtual methods
.method public update(II)V
    .locals 1

    if-gtz p2, :cond_0

    return-void

    .line 570
    :cond_0
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanTask$TaskObserver;->this$0:Lepson/scan/i2lib/I2ScanTask;

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {v0, p1, p2}, Lepson/scan/i2lib/I2ScanTask;->scanProgressCallback(II)V

    return-void
.end method

.class public Lepson/scan/i2lib/I2ScanParamArbiter;
.super Ljava/lang/Object;
.source "I2ScanParamArbiter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    }
.end annotation


# static fields
.field private static final ALL_INPUT_UNITS:[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;


# instance fields
.field private mAdfDuplexScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Lcom/epson/lib/escani2/ScanSize;",
            ">;"
        }
    .end annotation
.end field

.field private mAdfNonDuplexScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Lcom/epson/lib/escani2/ScanSize;",
            ">;"
        }
    .end annotation
.end field

.field private mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

.field private mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;",
            ">;"
        }
    .end annotation
.end field

.field private mDensity:I

.field private mDensityChangeable:Z

.field private mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mDuplexTurnDirection:I

.field private mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Lcom/epson/lib/escani2/ScanSize;",
            ">;"
        }
    .end annotation
.end field

.field private mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Lcom/epson/lib/escani2/EscanI2Lib$Gamma;",
            ">;"
        }
    .end annotation
.end field

.field private mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;",
            ">;"
        }
    .end annotation
.end field

.field private mManualAdfAlignmentI1Value:I

.field private mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Lcom/epson/lib/escani2/ScanSize;",
            ">;"
        }
    .end annotation
.end field

.field private mScannerAdfAlignment:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    .line 30
    new-array v0, v0, [Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lepson/scan/i2lib/I2ScanParamArbiter;->ALL_INPUT_UNITS:[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->init()V

    return-void
.end method

.method private adjustDuplexAndInputUnit(Z)V
    .locals 3

    .line 584
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getCandidateArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Boolean;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    .line 586
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    goto :goto_0

    .line 589
    :cond_0
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 592
    :goto_0
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result p1

    if-gez p1, :cond_1

    .line 593
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {p1, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    :cond_1
    return-void
.end method

.method private adjustDuplexSelector(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 572
    invoke-virtual {p1}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->canDoubleSideScan()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 573
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-virtual {p1, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    goto :goto_0

    .line 575
    :cond_0
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    new-array v0, v0, [Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private adjustInputUnitSelector(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 2

    .line 550
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    .line 552
    sget-object p1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 554
    :cond_0
    invoke-virtual {p1}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->isFlatbedAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 555
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 557
    :cond_1
    invoke-virtual {p1}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->isAdfAvailable()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 558
    sget-object p1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    :cond_2
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_3

    .line 565
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    return-void

    .line 563
    :cond_3
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1}, Ljava/lang/RuntimeException;-><init>()V

    throw p1
.end method

.method private changeInputUnitFromDuplexIfNeeded()V
    .locals 2

    .line 347
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 348
    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne v0, v1, :cond_0

    .line 351
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->setInputUnit(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)V

    goto :goto_0

    .line 353
    :cond_0
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getInputUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->selectSizeSelectorFromInputUnitAndDuplex(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)V

    :goto_0
    return-void
.end method

.method private checkAndAdjustScanSize()V
    .locals 4

    .line 207
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getScanSizePosition()I

    move-result v0

    if-gez v0, :cond_0

    .line 209
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-static {}, Lcom/epson/lib/escani2/ScanSize;->getLocaleDefaultSize()Lcom/epson/lib/escani2/ScanSize;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 210
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    if-gez v0, :cond_0

    .line 211
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    new-instance v1, Lcom/epson/lib/escani2/ScanSize;

    sget-object v2, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 212
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    if-gez v0, :cond_0

    .line 213
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    :cond_0
    return-void
.end method

.method public static getAdjustedParams(Landroid/content/Context;Lepson/scan/activity/ScannerPropertyWrapper;)Lcom/epson/lib/escani2/ScanI2Params;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lepson/scan/activity/ScannerPropertyWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 790
    invoke-virtual {p1}, Lepson/scan/activity/ScannerPropertyWrapper;->getI2ScannerAllInfo()Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    move-result-object p1

    .line 791
    new-instance v0, Lepson/scan/i2lib/I2ScanParamArbiter;

    invoke-direct {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;-><init>()V

    .line 792
    invoke-virtual {v0, p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->adjust(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    .line 793
    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getScanParam()Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object p0

    return-object p0
.end method

.method public static getI2ScanSize(I)Lcom/epson/lib/escani2/ScanSize;
    .locals 2

    .line 750
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    const/4 v1, 0x1

    if-ne p0, v0, :cond_0

    .line 751
    new-instance p0, Lcom/epson/lib/escani2/ScanSize;

    sget-object v0, Lcom/epson/lib/escani2/ScanSize$PaperSize;->B5:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {p0, v0, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    return-object p0

    .line 752
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 753
    new-instance p0, Lcom/epson/lib/escani2/ScanSize;

    sget-object v0, Lcom/epson/lib/escani2/ScanSize$PaperSize;->A4:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {p0, v0, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    return-object p0

    .line 754
    :cond_1
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    if-ne p0, v0, :cond_2

    .line 755
    new-instance p0, Lcom/epson/lib/escani2/ScanSize;

    sget-object v0, Lcom/epson/lib/escani2/ScanSize$PaperSize;->LETTER:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {p0, v0, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    return-object p0

    .line 756
    :cond_2
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    if-ne p0, v0, :cond_3

    .line 757
    new-instance p0, Lcom/epson/lib/escani2/ScanSize;

    sget-object v0, Lcom/epson/lib/escani2/ScanSize$PaperSize;->LEGAL:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {p0, v0, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    return-object p0

    .line 758
    :cond_3
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    if-ne p0, v0, :cond_4

    .line 759
    new-instance p0, Lcom/epson/lib/escani2/ScanSize;

    sget-object v0, Lcom/epson/lib/escani2/ScanSize$PaperSize;->A3:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {p0, v0, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    return-object p0

    :cond_4
    const/4 p0, 0x0

    return-object p0
.end method

.method private init()V
    .locals 5

    .line 92
    new-instance v0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 93
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    sget-object v1, Lepson/scan/i2lib/I2ScanParamArbiter;->ALL_INPUT_UNITS:[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    .line 95
    new-instance v0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 96
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/4 v1, 0x1

    new-array v2, v1, [Lcom/epson/lib/escani2/ScanSize;

    new-instance v3, Lcom/epson/lib/escani2/ScanSize;

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {v3, v4, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/4 v0, 0x0

    .line 100
    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 102
    new-instance v0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 103
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    new-array v2, v1, [Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->COLOR_24BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    .line 107
    new-instance v0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 108
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->setDefaultResolutionCandidate()V

    .line 110
    new-instance v0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 111
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    new-array v2, v1, [Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    .line 113
    new-instance v0, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 114
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    new-array v1, v1, [Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    sget-object v2, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->GAMMA_180:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    .line 118
    iput v4, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mManualAdfAlignmentI1Value:I

    return-void
.end method

.method private makeAllScanSizeSelector(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 649
    new-instance p1, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 650
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    new-array v2, v0, [Lcom/epson/lib/escani2/ScanSize;

    new-instance v3, Lcom/epson/lib/escani2/ScanSize;

    sget-object v4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {v3, v4, v0}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    aput-object v3, v2, v1

    invoke-virtual {p1, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    .line 652
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    return-void

    .line 657
    :cond_0
    iget-object v2, p1, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorSizeBaseResolution()I

    move-result v2

    .line 659
    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v4, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    .line 660
    invoke-virtual {p1, v4}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->getSensorSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I

    move-result-object v4

    .line 659
    invoke-direct {p0, v3, v1, v4, v2}, Lepson/scan/i2lib/I2ScanParamArbiter;->makeScanSizeSelector(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;Z[II)Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    move-result-object v3

    iput-object v3, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 664
    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v4, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    .line 665
    invoke-virtual {p1, v4}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->getSensorSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I

    move-result-object v4

    .line 664
    invoke-direct {p0, v3, v0, v4, v2}, Lepson/scan/i2lib/I2ScanParamArbiter;->makeScanSizeSelector(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;Z[II)Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    move-result-object v0

    iput-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAdfDuplexScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 668
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    .line 669
    invoke-virtual {p1, v3}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->getSensorSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I

    move-result-object p1

    .line 668
    invoke-direct {p0, v0, v1, p1, v2}, Lepson/scan/i2lib/I2ScanParamArbiter;->makeScanSizeSelector(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;Z[II)Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    move-result-object p1

    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAdfNonDuplexScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    return-void
.end method

.method private static makeList([I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 770
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 771
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget v3, p0, v2

    .line 772
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private makeScanSizeSelector(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;Z[II)Lepson/scan/i2lib/I2ScanParamArbiter$Selector;
    .locals 4
    .param p3    # [I
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;",
            "Z[II)",
            "Lepson/scan/i2lib/I2ScanParamArbiter$Selector<",
            "Lcom/epson/lib/escani2/ScanSize;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p3, :cond_0

    .line 680
    new-instance p1, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    .line 681
    new-array p2, v1, [Lcom/epson/lib/escani2/ScanSize;

    new-instance p3, Lcom/epson/lib/escani2/ScanSize;

    sget-object p4, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {p3, p4, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    aput-object p3, p2, v0

    invoke-virtual {p1, p2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    return-object p1

    .line 685
    :cond_0
    sget-object v2, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne p1, v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    aget v3, p3, v0

    aget p3, p3, v1

    invoke-static {v2, p2, v3, p3, p4}, Lepson/scan/lib/ScanSizeHelper;->getSupportedScanSizeList(IIIII)[I

    move-result-object p2

    .line 691
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 692
    array-length p4, p2

    :goto_1
    if-ge v0, p4, :cond_3

    aget v2, p2, v0

    .line 693
    invoke-static {v2}, Lepson/scan/i2lib/I2ScanParamArbiter;->getI2ScanSize(I)Lcom/epson/lib/escani2/ScanSize;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 695
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 699
    :cond_3
    sget-object p2, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne p1, p2, :cond_4

    .line 700
    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 702
    new-instance p1, Lcom/epson/lib/escani2/ScanSize;

    sget-object p2, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {p1, p2, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 707
    :cond_4
    new-instance p1, Lcom/epson/lib/escani2/ScanSize;

    sget-object p2, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-direct {p1, p2, v1}, Lcom/epson/lib/escani2/ScanSize;-><init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    :cond_5
    :goto_2
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p1

    new-array p1, p1, [Lcom/epson/lib/escani2/ScanSize;

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/epson/lib/escani2/ScanSize;

    .line 711
    new-instance p2, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {p2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    .line 712
    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    return-object p2
.end method

.method private selectSizeSelectorFromInputUnitAndDuplex(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)V
    .locals 2

    .line 184
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getScanSize()Lcom/epson/lib/escani2/ScanSize;

    move-result-object v0

    .line 185
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne p1, v1, :cond_1

    .line 186
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getDuplex()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAdfDuplexScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAdfNonDuplexScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    :goto_0
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    goto :goto_1

    .line 189
    :cond_1
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mFlatbedScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    :goto_1
    const/4 p1, 0x0

    .line 193
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 196
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {p1, v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 198
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->checkAndAdjustScanSize()V

    return-void
.end method

.method private setDefaultResolutionCandidate()V
    .locals 4

    .line 633
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x96

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    return-void
.end method

.method private setInputUnit(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)V
    .locals 1

    .line 154
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 160
    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->selectSizeSelectorFromInputUnitAndDuplex(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)V

    return-void
.end method

.method private setResolutionInitValue()V
    .locals 2

    .line 637
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/16 v1, 0x96

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 638
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    const/4 v1, -0x1

    if-ne v1, v0, :cond_0

    .line 639
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public adjust(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 0

    .line 417
    invoke-static {p1}, Lepson/scan/lib/ScanCommonParams;->load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams;

    move-result-object p1

    .line 418
    invoke-virtual {p0, p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->internalAdjust(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;Lepson/scan/lib/ScanCommonParams;)V

    return-void
.end method

.method adjustResolution(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 7

    if-nez p1, :cond_0

    .line 604
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->setDefaultResolutionCandidate()V

    return-void

    .line 608
    :cond_0
    iget-object v0, p1, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Capability:Lcom/epson/lib/escani2/ScannerI2Capability;

    iget-object v0, v0, Lcom/epson/lib/escani2/ScannerI2Capability;->mainScanResolutions:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 609
    iget-object p1, p1, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Capability:Lcom/epson/lib/escani2/ScannerI2Capability;

    iget-object p1, p1, Lcom/epson/lib/escani2/ScannerI2Capability;->subScanResolutions:[I

    if-eqz v0, :cond_4

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x3

    .line 615
    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lepson/scan/i2lib/I2ScanParamArbiter;->makeList([I)Ljava/util/List;

    move-result-object v1

    .line 616
    invoke-static {p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->makeList([I)Ljava/util/List;

    move-result-object p1

    .line 617
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 619
    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    .line 620
    array-length v3, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget v5, v0, v4

    .line 621
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 622
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 623
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 627
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/Integer;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Integer;

    .line 628
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    return-void

    .line 611
    :cond_4
    :goto_1
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->setDefaultResolutionCandidate()V

    return-void

    nop

    :array_0
    .array-data 4
        0x4b
        0x96
        0x12c
    .end array-data
.end method

.method public getColorMode()Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;
    .locals 1

    .line 254
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    return-object v0
.end method

.method public getColorModeCandidates()[Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;
    .locals 1

    .line 250
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getCandidateArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    return-object v0
.end method

.method public getColorModePosition()I
    .locals 1

    .line 258
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    return v0
.end method

.method public getDensity()I
    .locals 1

    .line 377
    iget v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensity:I

    return v0
.end method

.method public getDensityChangeable()Z
    .locals 1

    .line 381
    iget-boolean v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensityChangeable:Z

    return v0
.end method

.method public getDuplex()Ljava/lang/Boolean;
    .locals 1

    .line 299
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public getDuplexCandidates()[Ljava/lang/Boolean;
    .locals 1

    .line 295
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getCandidateArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Boolean;

    return-object v0
.end method

.method public getDuplexPosition()I
    .locals 1

    .line 303
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    return v0
.end method

.method public getDuplexTurnDirection()I
    .locals 1

    .line 365
    iget v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexTurnDirection:I

    return v0
.end method

.method public getGamma()Lcom/epson/lib/escani2/EscanI2Lib$Gamma;
    .locals 1

    .line 392
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    return-object v0
.end method

.method public getGammaCandidates()[Lcom/epson/lib/escani2/EscanI2Lib$Gamma;
    .locals 1

    .line 388
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getCandidateArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    return-object v0
.end method

.method public getGammaPosition()I
    .locals 1

    .line 396
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    return v0
.end method

.method public getInputUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;
    .locals 1

    .line 130
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    return-object v0
.end method

.method public getInputUnitCandidates()[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;
    .locals 1

    .line 126
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getCandidateArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    return-object v0
.end method

.method public getInputUnitPosition()I
    .locals 1

    .line 134
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    return v0
.end method

.method public getManualAdfAlignmentValue()I
    .locals 1

    .line 174
    iget v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mManualAdfAlignmentI1Value:I

    return v0
.end method

.method public getResolution()Ljava/lang/Integer;
    .locals 1

    .line 273
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getResolutionCandidates()[Ljava/lang/Integer;
    .locals 1

    .line 269
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getCandidateArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    return-object v0
.end method

.method public getResolutionPosition()I
    .locals 1

    .line 277
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    return v0
.end method

.method public getScanParam()Lcom/epson/lib/escani2/ScanI2Params;
    .locals 2

    .line 722
    new-instance v0, Lcom/epson/lib/escani2/ScanI2Params;

    invoke-direct {v0}, Lcom/epson/lib/escani2/ScanI2Params;-><init>()V

    .line 723
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getResolution()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 724
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    .line 725
    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    .line 727
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getInputUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    .line 730
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getScanSize()Lcom/epson/lib/escani2/ScanSize;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 732
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getColorMode()Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    .line 733
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getGamma()Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    .line 734
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getDuplex()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    .line 735
    iget v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexTurnDirection:I

    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->duplexTurnDirection:I

    .line 736
    iget v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensity:I

    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->density:I

    .line 737
    iget-boolean v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensityChangeable:Z

    iput-boolean v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->densityChangeable:Z

    .line 738
    iget v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mManualAdfAlignmentI1Value:I

    iput v1, v0, Lcom/epson/lib/escani2/ScanI2Params;->manualAdfAlignment:I

    return-object v0
.end method

.method public getScanSize()Lcom/epson/lib/escani2/ScanSize;
    .locals 1

    .line 231
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    if-gez v0, :cond_0

    .line 233
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/lib/escani2/ScanSize;

    :goto_0
    return-object v0
.end method

.method public getScanSizeCandidates()[Lcom/epson/lib/escani2/ScanSize;
    .locals 1

    .line 224
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getCandidateArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/ScanSize;

    return-object v0
.end method

.method public getScanSizePosition()I
    .locals 1

    .line 237
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    return v0
.end method

.method internalAdjust(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;Lepson/scan/lib/ScanCommonParams;)V
    .locals 7
    .param p2    # Lepson/scan/lib/ScanCommonParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 433
    invoke-static {}, Lepson/scan/i2lib/I2ScanParamManager;->getDefaultScanI2Params()Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v0

    .line 438
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {p1, v1}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->getAlignment(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)I

    move-result v1

    iput v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScannerAdfAlignment:I

    .line 441
    new-instance v1, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 442
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/4 v2, 0x3

    new-array v2, v2, [Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->COLOR_24BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->GRAYSCALE_8BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    const/4 v5, 0x1

    aput-object v3, v2, v5

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    const/4 v6, 0x2

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    .line 447
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iget-object v2, v0, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    invoke-virtual {v1, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 450
    new-instance v1, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-direct {v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;-><init>()V

    iput-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 451
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    new-array v2, v6, [Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->GAMMA_100:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    aput-object v3, v2, v4

    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->GAMMA_180:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setCandidateArray([Ljava/lang/Object;)V

    .line 455
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iget-object v2, v0, Lcom/epson/lib/escani2/ScanI2Params;->userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    invoke-virtual {v1, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 458
    invoke-virtual {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->adjustResolution(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    .line 459
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->setResolutionInitValue()V

    .line 462
    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->adjustInputUnitSelector(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    .line 464
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {p2}, Lepson/scan/lib/ScanCommonParams;->getI2ScanSourceUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 465
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v1

    if-gez v1, :cond_0

    .line 466
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v1, v4}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    .line 470
    :cond_0
    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->adjustDuplexSelector(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    .line 472
    invoke-virtual {p2}, Lepson/scan/lib/ScanCommonParams;->getI2Duplex()Z

    move-result p2

    invoke-direct {p0, p2}, Lepson/scan/i2lib/I2ScanParamArbiter;->adjustDuplexAndInputUnit(Z)V

    .line 475
    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->makeAllScanSizeSelector(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    const/4 p1, 0x0

    .line 476
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 478
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getInputUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->selectSizeSelectorFromInputUnitAndDuplex(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)V

    .line 480
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iget-object p2, v0, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    invoke-virtual {p1, p2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 481
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->checkAndAdjustScanSize()V

    .line 483
    iget p1, v0, Lcom/epson/lib/escani2/ScanI2Params;->density:I

    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensity:I

    .line 484
    iget-boolean p1, v0, Lcom/epson/lib/escani2/ScanI2Params;->densityChangeable:Z

    iput-boolean p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensityChangeable:Z

    return-void
.end method

.method public isAdfAlignmentMenuValid()Z
    .locals 2

    .line 167
    iget v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScannerAdfAlignment:I

    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isDuplexSupported()Z
    .locals 5

    .line 315
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getCandidateArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Boolean;

    .line 317
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 318
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public makeSelectionList(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;Lcom/epson/lib/escani2/ScanI2Params;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/lib/escani2/ScanI2Params;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 496
    invoke-virtual {p0, p1, p2}, Lepson/scan/i2lib/I2ScanParamArbiter;->adjust(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V

    .line 499
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iget-object p2, p3, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    invoke-virtual {p1, p2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 503
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iget-object p2, p3, Lcom/epson/lib/escani2/ScanI2Params;->userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    invoke-virtual {p1, p2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 507
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iget p2, p3, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 508
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result p1

    const/4 p2, -0x1

    if-ne p2, p1, :cond_0

    .line 510
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/16 v0, 0x96

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 515
    :cond_0
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iget-object v0, p3, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {p1, v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 518
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    iget-boolean v0, p3, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 519
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result p1

    .line 521
    invoke-virtual {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->setDuplexPosition(I)V

    .line 523
    iget p1, p3, Lcom/epson/lib/escani2/ScanI2Params;->duplexTurnDirection:I

    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexTurnDirection:I

    .line 525
    iget p1, p3, Lcom/epson/lib/escani2/ScanI2Params;->density:I

    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensity:I

    .line 526
    iget-boolean p1, p3, Lcom/epson/lib/escani2/ScanI2Params;->densityChangeable:Z

    iput-boolean p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensityChangeable:Z

    .line 530
    iget-object p1, p3, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 531
    invoke-virtual {p1}, Lcom/epson/lib/escani2/ScanSize;->isPixelSize()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {p1, p2}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    .line 535
    iget-object p1, p3, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

    goto :goto_0

    .line 537
    :cond_1
    iget-object p2, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 538
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->checkAndAdjustScanSize()V

    const/4 p1, 0x0

    .line 539
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 542
    :goto_0
    iget p1, p3, Lcom/epson/lib/escani2/ScanI2Params;->manualAdfAlignment:I

    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mManualAdfAlignmentI1Value:I

    return-void
.end method

.method public notifyScannerChanged(Landroid/content/Context;Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;)V
    .locals 0

    .line 409
    invoke-static {p1}, Lepson/scan/lib/ScanCommonParams;->load(Landroid/content/Context;)Lepson/scan/lib/ScanCommonParams;

    move-result-object p1

    .line 410
    invoke-virtual {p0, p2, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->internalAdjust(Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;Lepson/scan/lib/ScanCommonParams;)V

    const/4 p1, 0x0

    .line 413
    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mManualAdfAlignmentI1Value:I

    return-void
.end method

.method public setColorModePosition(I)V
    .locals 1

    .line 262
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mColorModeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    return-void
.end method

.method public setDensity(IZ)V
    .locals 0

    .line 372
    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensity:I

    .line 373
    iput-boolean p2, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDensityChangeable:Z

    return-void
.end method

.method public setDuplexPosition(I)V
    .locals 1

    .line 334
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    .line 335
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->changeInputUnitFromDuplexIfNeeded()V

    return-void
.end method

.method public setDuplexTurnDirection(I)V
    .locals 0

    .line 361
    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexTurnDirection:I

    return-void
.end method

.method public setDuplexValue(Z)V
    .locals 1

    .line 339
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 340
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->changeInputUnitFromDuplexIfNeeded()V

    return-void
.end method

.method public setGammaPosition(I)V
    .locals 1

    .line 400
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mGammaSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    return-void
.end method

.method public setInputUnitPosition(I)V
    .locals 1

    .line 138
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mInputUnitSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    .line 139
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getInputUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object p1

    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    .line 140
    invoke-virtual {p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 141
    iget-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mDuplexSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setSelectedValue(Ljava/lang/Object;)V

    .line 144
    :cond_0
    invoke-virtual {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getInputUnit()Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter;->selectSizeSelectorFromInputUnitAndDuplex(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)V

    return-void
.end method

.method public setManualAdfAlignmentValue(I)V
    .locals 0

    .line 178
    iput p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mManualAdfAlignmentI1Value:I

    return-void
.end method

.method public setResolutionPosition(I)V
    .locals 2

    .line 281
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->getPosition()I

    move-result v0

    .line 282
    iget-object v1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mResolutionSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v1, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    if-eq v0, p1, :cond_0

    const/4 p1, 0x0

    .line 286
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 287
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->checkAndAdjustScanSize()V

    :cond_0
    return-void
.end method

.method public setScanSizePosition(I)V
    .locals 1

    .line 241
    iget-object v0, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mScanSizeSelector:Lepson/scan/i2lib/I2ScanParamArbiter$Selector;

    invoke-virtual {v0, p1}, Lepson/scan/i2lib/I2ScanParamArbiter$Selector;->setPosition(I)V

    const/4 p1, 0x0

    .line 242
    iput-object p1, p0, Lepson/scan/i2lib/I2ScanParamArbiter;->mAltScanSize:Lcom/epson/lib/escani2/ScanSize;

    return-void
.end method

.method public tmpReset()V
    .locals 0

    .line 782
    invoke-direct {p0}, Lepson/scan/i2lib/I2ScanParamArbiter;->init()V

    return-void
.end method

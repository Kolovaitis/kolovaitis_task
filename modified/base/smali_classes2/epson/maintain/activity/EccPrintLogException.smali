.class Lepson/maintain/activity/EccPrintLogException;
.super Ljava/lang/Exception;
.source "EccPrintLog.java"


# static fields
.field public static final ECC_LIB_INITIAL_ERROR:I = 0xa

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public mErrorCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 386
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 387
    iput p1, p0, Lepson/maintain/activity/EccPrintLogException;->mErrorCode:I

    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .line 392
    iget v0, p0, Lepson/maintain/activity/EccPrintLogException;->mErrorCode:I

    return v0
.end method

.class public Lepson/maintain/activity/MaintainActivity;
.super Lepson/print/ActivityIACommon;
.source "MaintainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/epson/mobilephone/common/escpr/MediaInfo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;,
        Lepson/maintain/activity/MaintainActivity$CustomProDialog;
    }
.end annotation


# static fields
.field private static final CANCEL_LOADCONFIG:I = 0xe

.field private static final CLEANING_ERROR:I = 0xa

.field private static final DELAY_TIME_MAINTAIN:I = 0xa

.field private static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field private static final DIALOG_TAG_NOZZLE_GUIDANCE:Ljava/lang/String; = "dialog_nozzle_guidance"

.field private static final EC_BUTTON_HIDE:I = 0x2

.field private static final EC_BUTTON_INVALID:I = 0x1

.field private static final EC_BUTTON_VALID:I = 0x0

.field private static final EPS_COMM_BID:I = 0x2

.field private static final EPS_ERR_COMM_ERROR:I = -0x44c

.field private static final EPS_LANG_ESCPR:I = 0x1

.field private static final EPS_MNT_CLEANING:I = 0x2

.field private static final EPS_MNT_NOZZLE:I = 0x3

.field private static final EPS_PRNERR_CEMPTY:I = 0x67

.field private static final EPS_PRNERR_CFAIL:I = 0x68

.field private static final EPS_PRNERR_COMM:I = 0x66

.field private static final EPS_PRNERR_DISABEL_CLEANING:I = 0x6c

.field private static final EPS_PRNERR_INKOUT:I = 0x6

.field private static final GAGA_DEVICE_ID_ARRAY:[Ljava/lang/String;

.field private static final GET_PRINTER_NAME:I = 0x8

.field private static final HANDLE_ERROR:I = 0x4

.field public static final ITEM_HEIGHT:I = 0x28

.field private static final LOGIN_STATUS_COMM_ERROR:I = -0x1

.field private static final LOGIN_STATUS_LOGIN_FAILED:I = -0x2

.field private static final LOGIN_STATUS_LOGIN_SUCCEEDED:I = 0x0

.field private static final MAINTAIN_OK:I = 0x1

.field private static final MAINTAIN_START:I = 0x0

.field private static final MAX_WAIT_CONNECTING_3G:I = 0xa

.field private static final MESSAGE_MAINTENANCE_BOX_INFO:I = 0x10

.field private static final NO_PRINTER_NAME:I = 0x9

.field private static final PREFS_NAME:Ljava/lang/String; = "PrintSetting"

.field private static final PROBE_ERROR:I = 0x7

.field private static final PROBE_RESULT:Ljava/lang/String; = "PROBE_ERROR"

.field private static final REQUEST_CODE_INK_DS_DIALOG:I = 0x64

.field private static final REQUEST_CODE_SELECT_PRINTER:I = 0x65

.field private static final START_LOADCONFIG:I = 0x6

.field private static final TAG:Ljava/lang/String; = "MAINTAIN"

.field private static final UPDATE_BATTERY:I = 0xf

.field private static final UPDATE_INK:I = 0x3

.field private static final UPDATE_MAINTENANCE_LABEL:I = 0xb

.field private static final UPDATE_SELECTED_PRINTER:I = 0x5

.field private static final UPDATE_STT:I = 0x2

.field private static isMessageScreenShowing:Z


# instance fields
.field private btnToggleAlert:Landroid/widget/Switch;

.field private btnTogglePreview:Landroid/widget/Switch;

.field private customPro:Landroid/app/Dialog;

.field private escprSetPrinterSuccessed:I

.field private isGotError:Z

.field private isKeepSimpleAPConnection:Z

.field private isRemotePrinter:Z

.field isResearchScanner:Z

.field private isTryConnectSimpleAp:Z

.field private mAlertMesseageSeparater:Landroid/view/View;

.field private mAlertMesseageView:Landroid/widget/LinearLayout;

.field private mBackGround:Ljava/lang/Thread;

.field private mBuyInkGroup:Landroid/widget/LinearLayout;

.field private mBuyInkGroupVisibility:I

.field private mBuyInkSeparator:Landroid/view/View;

.field private mBuyInkView:Landroid/widget/LinearLayout;

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private mCleanCheckView:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mDisplay:Landroid/view/Display;

.field private mDoMaintainThread:Ljava/lang/Thread;

.field private mEpsonConnectView:Landroid/widget/LinearLayout;

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field private mFWUpdateText:Landroid/widget/TextView;

.field private mFWUpdateView:Landroid/widget/LinearLayout;

.field mHavePrinter:Z

.field private mHeadCleanText:Landroid/widget/TextView;

.field private mHeadCleanView:Landroid/widget/LinearLayout;

.field private mInkDsView:Landroid/widget/LinearLayout;

.field private mInkView:Landroid/widget/LinearLayout;

.field private volatile mIsCancelProbe:Z

.field private mIsNozzleCheckEnable:Ljava/lang/Boolean;

.field private volatile mIsStillUpdate:Z

.field mIsStop:Z

.field private mLLSerialNo:Landroid/widget/LinearLayout;

.field private mLoadConPro:Landroid/view/View;

.field private mLoadConfigThread:Ljava/lang/Thread;

.field private mMaintenanceBoxText:Landroid/widget/TextView;

.field private mMaintenanceBoxView:Landroid/widget/LinearLayout;

.field private mNozzleCheckText:Landroid/widget/TextView;

.field private mNozzleCheckView:Landroid/widget/LinearLayout;

.field private mOnlineRegistrationSeparator:Landroid/view/View;

.field private mOnlineRegistrationView:Landroid/widget/LinearLayout;

.field private mPrintHistorySeparator:Landroid/view/View;

.field private mPrintHistoryView:Landroid/widget/LinearLayout;

.field private mPrintPreviewView:Landroid/widget/LinearLayout;

.field private mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field private mPrinterSettingSeparator:Landroid/view/View;

.field private mPrinterSettingView:Landroid/widget/LinearLayout;

.field private mPrinterStatus:[I

.field mStartSearch:Z

.field private mTextMaintenance:Landroid/widget/TextView;

.field private mTvPrinterName:Landroid/widget/TextView;

.field private mTvPrinterStatus:Landroid/widget/TextView;

.field private mTvPrinterStatusDetail:Landroid/widget/TextView;

.field private mTvSerialNo:Landroid/widget/TextView;

.field private mUiHandler:Landroid/os/Handler;

.field private mVSerialSeparator:Landroid/view/View;

.field private mWaiteInkDsDialog:Z

.field private modelBuyInkOnlineRegTask:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

.field private printerId:Ljava/lang/String;

.field private printerIp:Ljava/lang/String;

.field private printerLocation:I

.field private printerName:Ljava/lang/String;

.field private serverLoginStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const-string v0, "LX-10000F"

    const-string v1, "LX-7000F"

    const-string v2, "WF-C20590 Series"

    const-string v3, "WF-C17590 Series"

    const-string v4, "LX-10000FK"

    const-string v5, "LX-10010MF"

    const-string v6, "WF-M20590 Series"

    const-string v7, "LX-10050KF"

    const-string v8, "LX-10050MF"

    const-string v9, "LX-6050MF"

    const-string v10, "LX-7550MF"

    const-string v11, "WF-C20600 Series"

    const-string v12, "WF-C20750 Series"

    const-string v13, "WF-C21000 Series"

    .line 128
    filled-new-array/range {v0 .. v13}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lepson/maintain/activity/MaintainActivity;->GAGA_DEVICE_ID_ARRAY:[Ljava/lang/String;

    const/4 v0, 0x0

    .line 160
    sput-boolean v0, Lepson/maintain/activity/MaintainActivity;->isMessageScreenShowing:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 83
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 145
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const-string v0, ""

    .line 149
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->printerName:Ljava/lang/String;

    const-string v0, ""

    .line 150
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->printerId:Ljava/lang/String;

    const-string v0, ""

    .line 151
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    const/4 v0, 0x1

    .line 152
    iput v0, p0, Lepson/maintain/activity/MaintainActivity;->printerLocation:I

    const/4 v1, 0x0

    .line 184
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->isGotError:Z

    .line 185
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->mIsCancelProbe:Z

    .line 186
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->mStartSearch:Z

    .line 187
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mIsStillUpdate:Z

    .line 188
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->mHavePrinter:Z

    .line 189
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->isResearchScanner:Z

    .line 190
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->mIsStop:Z

    .line 192
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mIsNozzleCheckEnable:Ljava/lang/Boolean;

    .line 216
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->isRemotePrinter:Z

    const/4 v0, -0x2

    .line 217
    iput v0, p0, Lepson/maintain/activity/MaintainActivity;->serverLoginStatus:I

    const/4 v0, -0x1

    .line 218
    iput v0, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    const/4 v0, 0x0

    .line 219
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 221
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->isKeepSimpleAPConnection:Z

    .line 222
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->isTryConnectSimpleAp:Z

    .line 487
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lepson/maintain/activity/MaintainActivity$2;

    invoke-direct {v2, p0}, Lepson/maintain/activity/MaintainActivity$2;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    .line 2092
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 2093
    new-instance v0, Lepson/maintain/activity/MaintainActivity$14;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainActivity$14;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 2120
    new-instance v0, Lepson/maintain/activity/MaintainActivity$15;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainActivity$15;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method private BuyInkTask(ILjava/lang/String;)V
    .locals 1

    .line 2164
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->modelBuyInkOnlineRegTask:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    invoke-virtual {v0}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->getReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2165
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->modelBuyInkOnlineRegTask:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    invoke-virtual {v0, p1, p2}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->loadData(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method private OnlineRegTask(Ljava/lang/String;)V
    .locals 1

    .line 2175
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->modelBuyInkOnlineRegTask:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    invoke-virtual {v0}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->getReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->modelBuyInkOnlineRegTask:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    invoke-virtual {v0, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->loadData(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$100(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->loadConfig()V

    return-void
.end method

.method static synthetic access$1000(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1002(Lepson/maintain/activity/MaintainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 83
    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-object p0
.end method

.method static synthetic access$1202(Lepson/maintain/activity/MaintainActivity;I)I
    .locals 0

    .line 83
    iput p1, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    return p1
.end method

.method static synthetic access$1300(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->interrupMaintainThread()V

    return-void
.end method

.method static synthetic access$1400(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->cancelProDia()V

    return-void
.end method

.method static synthetic access$1500(Lepson/maintain/activity/MaintainActivity;I)V
    .locals 0

    .line 83
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->updateStatus(I)V

    return-void
.end method

.method static synthetic access$1600(Lepson/maintain/activity/MaintainActivity;ILcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)V
    .locals 0

    .line 83
    invoke-direct {p0, p1, p2}, Lepson/maintain/activity/MaintainActivity;->updateBatteryInfoEx(ILcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)V

    return-void
.end method

.method static synthetic access$1700(Lepson/maintain/activity/MaintainActivity;[I)V
    .locals 0

    .line 83
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->updateMaintenanceBoxInfo([I)V

    return-void
.end method

.method static synthetic access$1800(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->showProDia()V

    return-void
.end method

.method static synthetic access$1900(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->updateSelectedPrinter()V

    return-void
.end method

.method static synthetic access$200(Lepson/maintain/activity/MaintainActivity;)Z
    .locals 0

    .line 83
    iget-boolean p0, p0, Lepson/maintain/activity/MaintainActivity;->mIsCancelProbe:Z

    return p0
.end method

.method static synthetic access$2000(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->showLoadProgressBar()V

    return-void
.end method

.method static synthetic access$2100(Lepson/maintain/activity/MaintainActivity;Z)V
    .locals 0

    .line 83
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->setButtonClickable(Z)V

    return-void
.end method

.method static synthetic access$2200(Lepson/maintain/activity/MaintainActivity;I)V
    .locals 0

    .line 83
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->setEpsonConnectButton(I)V

    return-void
.end method

.method static synthetic access$2300(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->hideLoadProgressBar()V

    return-void
.end method

.method static synthetic access$2400(Lepson/maintain/activity/MaintainActivity;IZ)V
    .locals 0

    .line 83
    invoke-direct {p0, p1, p2}, Lepson/maintain/activity/MaintainActivity;->handlerError(IZ)V

    return-void
.end method

.method static synthetic access$2500(Lepson/maintain/activity/MaintainActivity;I)V
    .locals 0

    .line 83
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->setupErrorMessage(I)V

    return-void
.end method

.method static synthetic access$2600(Lepson/maintain/activity/MaintainActivity;)Landroid/widget/TextView;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterName:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2700(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->stopLoadConfigThread()V

    return-void
.end method

.method static synthetic access$2800(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->updateButton()V

    return-void
.end method

.method static synthetic access$2900(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->endBackGroundThread()V

    return-void
.end method

.method static synthetic access$300(Lepson/maintain/activity/MaintainActivity;)Z
    .locals 0

    .line 83
    iget-boolean p0, p0, Lepson/maintain/activity/MaintainActivity;->isRemotePrinter:Z

    return p0
.end method

.method static synthetic access$3000(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->startSelectPrinterActivity()V

    return-void
.end method

.method static synthetic access$302(Lepson/maintain/activity/MaintainActivity;Z)Z
    .locals 0

    .line 83
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainActivity;->isRemotePrinter:Z

    return p1
.end method

.method static synthetic access$3200(Lepson/maintain/activity/MaintainActivity;I)V
    .locals 0

    .line 83
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->performMaintain(I)V

    return-void
.end method

.method static synthetic access$3302(Lepson/maintain/activity/MaintainActivity;Z)Z
    .locals 0

    .line 83
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainActivity;->isKeepSimpleAPConnection:Z

    return p1
.end method

.method static synthetic access$3400(Lepson/maintain/activity/MaintainActivity;)Landroid/widget/Switch;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->btnTogglePreview:Landroid/widget/Switch;

    return-object p0
.end method

.method static synthetic access$3500(Lepson/maintain/activity/MaintainActivity;Landroid/os/Handler;[I)V
    .locals 0

    .line 83
    invoke-direct {p0, p1, p2}, Lepson/maintain/activity/MaintainActivity;->sendMaintenanceBoxInfoMessage(Landroid/os/Handler;[I)V

    return-void
.end method

.method static synthetic access$3600(Lepson/maintain/activity/MaintainActivity;)Z
    .locals 0

    .line 83
    iget-boolean p0, p0, Lepson/maintain/activity/MaintainActivity;->mIsStillUpdate:Z

    return p0
.end method

.method static synthetic access$3700(Lepson/maintain/activity/MaintainActivity;)Z
    .locals 0

    .line 83
    iget-boolean p0, p0, Lepson/maintain/activity/MaintainActivity;->isGotError:Z

    return p0
.end method

.method static synthetic access$3702(Lepson/maintain/activity/MaintainActivity;Z)Z
    .locals 0

    .line 83
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainActivity;->isGotError:Z

    return p1
.end method

.method static synthetic access$3800(Lepson/maintain/activity/MaintainActivity;)[I
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    return-object p0
.end method

.method static synthetic access$3900(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$4000(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->showNozzleCheckGuidanceDialog()V

    return-void
.end method

.method static synthetic access$402(Lepson/maintain/activity/MaintainActivity;I)I
    .locals 0

    .line 83
    iput p1, p0, Lepson/maintain/activity/MaintainActivity;->serverLoginStatus:I

    return p1
.end method

.method static synthetic access$500(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$502(Lepson/maintain/activity/MaintainActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 83
    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$600(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->printerId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;
    .locals 0

    .line 83
    iget-object p0, p0, Lepson/maintain/activity/MaintainActivity;->printerName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$800(Lepson/maintain/activity/MaintainActivity;)I
    .locals 0

    .line 83
    iget p0, p0, Lepson/maintain/activity/MaintainActivity;->printerLocation:I

    return p0
.end method

.method static synthetic access$900(Lepson/maintain/activity/MaintainActivity;)Z
    .locals 0

    .line 83
    iget-boolean p0, p0, Lepson/maintain/activity/MaintainActivity;->isTryConnectSimpleAp:Z

    return p0
.end method

.method static synthetic access$902(Lepson/maintain/activity/MaintainActivity;Z)Z
    .locals 0

    .line 83
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainActivity;->isTryConnectSimpleAp:Z

    return p1
.end method

.method private static calcInkWidth(IF)I
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0x64

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    if-le p0, v1, :cond_1

    const/16 p0, 0x64

    :cond_1
    :goto_0
    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr p1, v1

    int-to-float p0, p0

    mul-float p1, p1, p0

    float-to-int p0, p1

    if-lez p0, :cond_2

    goto :goto_1

    :cond_2
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private cancelProDia()V
    .locals 1

    .line 1209
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->customPro:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1210
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->customPro:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    const/4 v0, 0x0

    .line 1211
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->customPro:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method private checkGaga()Z
    .locals 2

    const/4 v0, 0x0

    .line 1157
    invoke-static {p0, v0}, Lepson/print/MyPrinter;->getPrinterDeviceId(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    return v0

    .line 1161
    :cond_0
    sget-object v0, Lepson/maintain/activity/MaintainActivity;->GAGA_DEVICE_ID_ARRAY:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1163
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private checkIfWhite(I)Z
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 1971
    :try_start_0
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const v1, 0xffffff

    and-int/2addr p1, v1

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    return v0
.end method

.method public static disableSimpleApAndWait(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    const-string v0, "printer"

    .line 2189
    invoke-static {p0, v0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge p1, v1, :cond_1

    .line 2196
    :try_start_0
    invoke-static {p0}, Lepson/common/Utils;->isOnline(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    const-string v1, "MAINTAIN"

    const-string v2, "Wait Connection 3G"

    .line 2200
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 p1, p1, 0x1

    const-wide/16 v1, 0x3e8

    .line 2202
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 2205
    invoke-virtual {p0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_1
    :goto_1
    return v0
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 2225
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 2227
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private endBackGroundThread()V
    .locals 1

    const/4 v0, 0x0

    .line 1356
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mIsStillUpdate:Z

    .line 1357
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBackGround:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1358
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBackGround:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 1360
    :try_start_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBackGround:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1362
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_0
    const/4 v0, 0x0

    .line 1364
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBackGround:Ljava/lang/Thread;

    :cond_0
    return-void
.end method

.method private handlerError(IZ)V
    .locals 5

    .line 1643
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->hideLoadProgressBar()V

    const/16 v0, -0x514

    const/16 v1, -0x547

    if-eq p1, v1, :cond_0

    const/16 v1, -0x44c

    if-ne p1, v1, :cond_1

    :cond_0
    const/16 p1, -0x514

    .line 1647
    :cond_1
    iget-boolean v1, p0, Lepson/maintain/activity/MaintainActivity;->isRemotePrinter:Z

    const/4 v2, 0x1

    const/16 v3, 0xb

    const/4 v4, 0x0

    if-ne v1, v2, :cond_2

    if-ne p1, v0, :cond_2

    .line 1648
    invoke-direct {p0, v4}, Lepson/maintain/activity/MaintainActivity;->setButtonClickable(Z)V

    .line 1649
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    .line 1652
    invoke-direct {p0, v1, p1}, Lepson/maintain/activity/MaintainActivity;->setErrorStatus(II)V

    .line 1654
    invoke-direct {p0, v4}, Lepson/maintain/activity/MaintainActivity;->setButtonClickable(Z)V

    .line 1655
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    if-eqz p2, :cond_4

    if-eq p1, v0, :cond_3

    const p2, -0x7a121

    if-ne p1, p2, :cond_4

    .line 1658
    :cond_3
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->removeAllViews()V

    :cond_4
    :goto_0
    return-void
.end method

.method private hideLoadProgressBar()V
    .locals 2

    .line 1223
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConPro:Landroid/view/View;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 1224
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private interrupMaintainThread()V
    .locals 1

    .line 1712
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mDoMaintainThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1713
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mDoMaintainThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    .line 1714
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mDoMaintainThread:Ljava/lang/Thread;

    :cond_0
    return-void
.end method

.method private isMaintenanceBoxStatusDisplayX(I)Z
    .locals 0

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    :pswitch_0
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isPrinterReady()I
    .locals 5

    .line 1700
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    .line 1701
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    aget v2, v0, v1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    aget v2, v0, v1

    const/4 v4, 0x3

    if-ne v2, v4, :cond_0

    goto :goto_0

    .line 1704
    :cond_0
    aget v0, v0, v1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    return v3

    :cond_2
    :goto_0
    return v1
.end method

.method private isSimpleApOrP2p(Lepson/print/MyPrinter;)Z
    .locals 0

    .line 1436
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    .line 1437
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1438
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static synthetic lambda$onCreate$0(Lepson/maintain/activity/MaintainActivity;Lepson/common/EventWrapper;)V
    .locals 2

    .line 335
    invoke-virtual {p1}, Lepson/common/EventWrapper;->hasBeenHandled()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dialog_progress"

    .line 338
    invoke-direct {p0, v0}, Lepson/maintain/activity/MaintainActivity;->dismissDialog(Ljava/lang/String;)V

    .line 339
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lepson/common/EventWrapper;->getEventContent()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/Uri;

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 340
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private loadConfig()V
    .locals 2

    .line 1857
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 1859
    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->getUserDefName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerName:Ljava/lang/String;

    .line 1860
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerName:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, ""

    .line 1861
    iput-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerName:Ljava/lang/String;

    .line 1863
    :cond_0
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerId:Ljava/lang/String;

    .line 1864
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerId:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, ""

    .line 1865
    iput-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerId:Ljava/lang/String;

    .line 1867
    :cond_1
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    .line 1868
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v1, ""

    .line 1869
    iput-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    .line 1871
    :cond_2
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v0

    iput v0, p0, Lepson/maintain/activity/MaintainActivity;->printerLocation:I

    return-void
.end method

.method private localSetBuyInkGroupVisibility(I)V
    .locals 1

    .line 874
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroup:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 875
    iput p1, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroupVisibility:I

    return-void
.end method

.method private logColorInfo(I[Ljava/lang/Integer;)V
    .locals 8
    .param p2    # [Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x1

    .line 1980
    aget-object v1, p2, v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    .line 1981
    aget-object v3, p2, v2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    .line 1982
    aget-object v5, p2, v4

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1983
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "colorNo = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ":"

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v1, :cond_0

    const-string p1, "--"

    goto :goto_0

    :cond_0
    aget-object p1, p2, v0

    .line 1984
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ":"

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v3, :cond_1

    const-string p1, "--"

    goto :goto_1

    :cond_1
    const-string p1, "#%06X"

    new-array v0, v0, [Ljava/lang/Object;

    const v1, 0xffffff

    aget-object v3, p2, v2

    .line 1987
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {p0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    and-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ":"

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v5, :cond_2

    const-string p1, "--"

    goto :goto_2

    :cond_2
    aget-object p1, p2, v4

    .line 1989
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_2
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1983
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    return-void
.end method

.method private onInkReplenishDialogEnd()V
    .locals 1

    const/4 v0, 0x0

    .line 1443
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mWaiteInkDsDialog:Z

    return-void
.end method

.method private performMaintain(I)V
    .locals 2

    .line 1776
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->isPrinterReady()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1777
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1778
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lepson/maintain/activity/MaintainActivity$13;

    invoke-direct {v1, p0, p1}, Lepson/maintain/activity/MaintainActivity$13;-><init>(Lepson/maintain/activity/MaintainActivity;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mDoMaintainThread:Ljava/lang/Thread;

    .line 1829
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mDoMaintainThread:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method private sendMaintenanceBoxInfoMessage(Landroid/os/Handler;[I)V
    .locals 2

    .line 1350
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    .line 1351
    invoke-virtual {p1, v1, v0, v0, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p2

    .line 1352
    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setButtonClickable(Z)V
    .locals 3

    .line 2037
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2038
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2039
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    const v0, 0x7f05007b

    if-eqz p1, :cond_1

    .line 2041
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mIsNozzleCheckEnable:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const v1, 0x7f050029

    if-nez p1, :cond_0

    .line 2042
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckText:Landroid/widget/TextView;

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 2044
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckText:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2046
    :goto_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanText:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2047
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateText:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 2049
    :cond_1
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckText:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2050
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanText:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2051
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateText:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    return-void
.end method

.method private setEpsonConnectButton(I)V
    .locals 6

    const v0, 0x7f05007b

    const v1, 0x7f08012d

    const v2, 0x7f08012a

    const v3, 0x7f080332

    const/16 v4, 0x8

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 2079
    :pswitch_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2080
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 2081
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v3, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2082
    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2083
    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2084
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 2071
    :pswitch_1
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2072
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 2073
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v3, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2074
    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2075
    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2076
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 2063
    :pswitch_2
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2064
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 2065
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050029

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2066
    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2067
    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2068
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setErrorStatus(II)V
    .locals 3

    .line 1670
    invoke-static {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1676
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v0, p2, p1}, Lepson/common/Utils;->replaceMessage([Ljava/lang/Integer;ILandroid/content/Context;)[Ljava/lang/String;

    move-result-object p1

    .line 1678
    iget-object p2, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    const/4 v0, 0x1

    aget-object v1, p1, v0

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string p2, "MAINTAIN"

    .line 1679
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 1681
    aget-object p1, p1, p2

    .line 1684
    iget-object p2, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string p2, "MAINTAIN"

    .line 1685
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stt detail: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1687
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt Code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Err Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private setupErrorMessage(I)V
    .locals 2

    const p1, 0x7f0e01b9

    .line 917
    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e01b8

    .line 918
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e0476

    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 917
    invoke-static {p0, p1, v0, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 918
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 2

    const v0, 0x7f0e051b

    .line 2216
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 2217
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 2218
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showLoadProgressBar()V
    .locals 2

    .line 1216
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConPro:Landroid/view/View;

    if-nez v0, :cond_0

    const v0, 0x7f08029e

    .line 1217
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConPro:Landroid/view/View;

    .line 1219
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConPro:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showMessageDialog()V
    .locals 4

    const/4 v0, 0x1

    .line 1171
    sput-boolean v0, Lepson/maintain/activity/MaintainActivity;->isMessageScreenShowing:Z

    .line 1172
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1173
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0e034d

    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v2, 0x7f0e034e

    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1174
    new-instance v1, Lepson/maintain/activity/MaintainActivity$9;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainActivity$9;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    .line 1187
    new-instance v2, Lepson/maintain/activity/MaintainActivity$10;

    invoke-direct {v2, p0}, Lepson/maintain/activity/MaintainActivity$10;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    const v3, 0x7f0e052b

    .line 1194
    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e04e6

    .line 1195
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    .line 1196
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1197
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1198
    sput-boolean v1, Lepson/maintain/activity/MaintainActivity;->isMessageScreenShowing:Z

    return-void
.end method

.method private showNozzleCheckGuidanceDialog()V
    .locals 3

    .line 1148
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->checkGaga()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1152
    :cond_0
    new-instance v0, Lepson/maintain/activity/NozzleGuidanceDialog;

    invoke-direct {v0}, Lepson/maintain/activity/NozzleGuidanceDialog;-><init>()V

    .line 1153
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog_nozzle_guidance"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showProDia()V
    .locals 3

    .line 1202
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->customPro:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1203
    new-instance v0, Lepson/maintain/activity/MaintainActivity$CustomProDialog;

    const v1, 0x7f0f0009

    const v2, 0x7f0a008b

    invoke-direct {v0, p0, p0, v1, v2}, Lepson/maintain/activity/MaintainActivity$CustomProDialog;-><init>(Lepson/maintain/activity/MaintainActivity;Landroid/content/Context;II)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->customPro:Landroid/app/Dialog;

    .line 1205
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->customPro:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private startBackgroundThread()V
    .locals 3

    const/4 v0, 0x1

    .line 1267
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mIsStillUpdate:Z

    .line 1268
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lepson/maintain/activity/MaintainActivity$12;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainActivity$12;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    const-string v2, "status-update"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBackGround:Ljava/lang/Thread;

    .line 1344
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBackGround:Ljava/lang/Thread;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 1345
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBackGround:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private startBrowsReadyInkUrl()V
    .locals 2

    .line 1139
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/inkrpln/JumpUrlProgressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startInkDsDialog()V
    .locals 2

    .line 1447
    invoke-static {p0}, Lepson/print/inkrpln/InkRplnProgressDialog;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lepson/maintain/activity/MaintainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startLoadConThread()V
    .locals 3

    const/4 v0, 0x0

    .line 346
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mIsCancelProbe:Z

    .line 347
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lepson/maintain/activity/MaintainActivity$1;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainActivity$1;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    const-string v2, "load-config"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConfigThread:Ljava/lang/Thread;

    .line 484
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConfigThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private startSelectPrinterActivity()V
    .locals 3

    const/4 v0, 0x1

    .line 1127
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mWaiteInkDsDialog:Z

    .line 1130
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "PRINTER_ID"

    .line 1131
    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->printerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    .line 1132
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x65

    .line 1133
    invoke-virtual {p0, v0, v1}, Lepson/maintain/activity/MaintainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private stopLoadConfigThread()V
    .locals 2

    const/4 v0, 0x1

    .line 1229
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mIsCancelProbe:Z

    .line 1230
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConfigThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1233
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 1235
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 1237
    new-instance v0, Lepson/maintain/activity/MaintainActivity$11;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainActivity$11;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 1248
    invoke-virtual {v0, v1}, Lepson/maintain/activity/MaintainActivity$11;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1251
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConfigThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    .line 1262
    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLoadConfigThread:Ljava/lang/Thread;

    :cond_1
    return-void
.end method

.method private updateBatteryInfoEx(ILcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)V
    .locals 2

    .line 1553
    iget v0, p2, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->number:I

    .line 1554
    iget v1, p2, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->powerSourceType:I

    if-nez p1, :cond_0

    if-lez v0, :cond_0

    const/4 p1, -0x1

    if-eq v1, p1, :cond_0

    .line 1557
    iget-object p1, p2, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryState:[I

    .line 1558
    iget-object p2, p2, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryRemain:[I

    .line 1560
    invoke-static {p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->showPowerText(Landroid/app/Activity;)V

    .line 1561
    invoke-static {p0, v0, v1, p1, p2}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->showBatteryList(Landroid/app/Activity;II[I[I)V

    goto :goto_0

    .line 1563
    :cond_0
    invoke-static {p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->clearBatteryList(Landroid/app/Activity;)V

    :goto_0
    return-void
.end method

.method private updateButton()V
    .locals 10

    .line 606
    iget v0, p0, Lepson/maintain/activity/MaintainActivity;->printerLocation:I

    const v1, 0x7f050029

    const v2, 0x7f05007b

    const v3, 0x7f08028a

    const v4, 0x7f080338

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x8

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_5

    .line 674
    :pswitch_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetLang()I

    move-result v0

    if-eq v0, v5, :cond_0

    .line 675
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTextMaintenance:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 676
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mCleanCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 678
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTextMaintenance:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 679
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mCleanCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 684
    :goto_0
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->updateBuyInkGroupForLocalOrIPPrinter()V

    .line 686
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 691
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingSeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 692
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintPreviewView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 693
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageSeparater:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 694
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 695
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistorySeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 696
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 697
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationSeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 698
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 700
    iget v0, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    if-nez v0, :cond_1

    .line 702
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 703
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 704
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 705
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 706
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 707
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 708
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 709
    invoke-direct {p0, v6}, Lepson/maintain/activity/MaintainActivity;->setEpsonConnectButton(I)V

    goto/16 :goto_5

    .line 712
    :cond_1
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 713
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 714
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 715
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 716
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 717
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 718
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 719
    invoke-direct {p0, v5}, Lepson/maintain/activity/MaintainActivity;->setEpsonConnectButton(I)V

    goto/16 :goto_5

    .line 726
    :pswitch_1
    invoke-direct {p0, v6}, Lepson/maintain/activity/MaintainActivity;->localSetBuyInkGroupVisibility(I)V

    .line 727
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 729
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingSeparator:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 730
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 732
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintPreviewView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 733
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageSeparater:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 734
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 735
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistorySeparator:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 736
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x2

    .line 739
    invoke-direct {p0, v0}, Lepson/maintain/activity/MaintainActivity;->setEpsonConnectButton(I)V

    .line 742
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 743
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 745
    :cond_2
    new-instance v0, Landroid/widget/TextView;

    iget-object v8, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 746
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v8, 0x41800000    # 16.0f

    .line 747
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextSize(F)V

    const v8, 0x7f0e0365

    .line 748
    invoke-virtual {p0, v8}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 749
    iget-object v8, p0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "PREFS_EPSON_CONNECT"

    .line 752
    invoke-virtual {p0, v0, v6}, Lepson/maintain/activity/MaintainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v8, "ENABLE_SHOW_PREVIEW"

    .line 753
    invoke-interface {v0, v8, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    const-string v9, "ENABLE_SHOW_WARNING"

    .line 754
    invoke-interface {v0, v9, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v8, v5, :cond_3

    .line 757
    iget-object v8, p0, Lepson/maintain/activity/MaintainActivity;->btnTogglePreview:Landroid/widget/Switch;

    invoke-virtual {v8, v5}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_1

    .line 759
    :cond_3
    iget-object v8, p0, Lepson/maintain/activity/MaintainActivity;->btnTogglePreview:Landroid/widget/Switch;

    invoke-virtual {v8, v6}, Landroid/widget/Switch;->setChecked(Z)V

    :goto_1
    if-ne v0, v5, :cond_4

    .line 762
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->btnToggleAlert:Landroid/widget/Switch;

    invoke-virtual {v0, v5}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_2

    .line 764
    :cond_4
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->btnToggleAlert:Landroid/widget/Switch;

    invoke-virtual {v0, v6}, Landroid/widget/Switch;->setChecked(Z)V

    .line 768
    :goto_2
    invoke-static {p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->clearBatteryList(Landroid/app/Activity;)V

    .line 773
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTextMaintenance:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 774
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mCleanCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 775
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationSeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 776
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 777
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxText:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 778
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 780
    iget v0, p0, Lepson/maintain/activity/MaintainActivity;->serverLoginStatus:I

    const v8, 0x7f080275

    const v9, 0x7f080336

    if-nez v0, :cond_5

    .line 782
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    const v2, 0x7f0e021c

    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 783
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    const v2, 0x7f0e021b

    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 785
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 786
    invoke-virtual {p0, v9}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 787
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 788
    invoke-virtual {p0, v8}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 789
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 790
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 791
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 792
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 793
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 794
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_5
    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    .line 798
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    const v1, 0x7f0e0060

    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 799
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    const v1, 0x7f0e005f

    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 801
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 802
    invoke-virtual {p0, v9}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 803
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 804
    invoke-virtual {p0, v8}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 805
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 806
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 807
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 808
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 809
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 810
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 813
    :cond_6
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    const v1, 0x7f0e0367

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 814
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    const v1, 0x7f0e0366

    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 816
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 817
    invoke-virtual {p0, v9}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 818
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 819
    invoke-virtual {p0, v8}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 820
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 821
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 822
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 823
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 824
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 825
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 609
    :pswitch_2
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetLang()I

    move-result v0

    if-eq v0, v5, :cond_7

    .line 610
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTextMaintenance:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 611
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mCleanCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 612
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationSeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 613
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 615
    :cond_7
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTextMaintenance:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 616
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mCleanCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 617
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationSeparator:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 625
    :goto_3
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->updateBuyInkGroupForLocalOrIPPrinter()V

    .line 627
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 632
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingSeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 633
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintPreviewView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 634
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageSeparater:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 635
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 636
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistorySeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 637
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 639
    iget v0, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    if-nez v0, :cond_8

    .line 641
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 642
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 643
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 644
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 645
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 646
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 647
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 648
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 649
    invoke-direct {p0, v6}, Lepson/maintain/activity/MaintainActivity;->setEpsonConnectButton(I)V

    goto :goto_4

    .line 652
    :cond_8
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 653
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 654
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 655
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 656
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 657
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 658
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 659
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 660
    invoke-direct {p0, v5}, Lepson/maintain/activity/MaintainActivity;->setEpsonConnectButton(I)V

    :goto_4
    const-string v0, "USB"

    .line 664
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 665
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 666
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingSeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 667
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_5

    .line 831
    :pswitch_3
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterName:Landroid/widget/TextView;

    const v1, 0x7f0e04d6

    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f080198

    .line 832
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 833
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    const v1, 0x7f0e01e6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 834
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    const v1, 0x7f0e01e5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 835
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_9

    .line 836
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 841
    :cond_9
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTextMaintenance:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 842
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mCleanCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 843
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 845
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 846
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 847
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 848
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 849
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 850
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 851
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 852
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 857
    invoke-direct {p0, v7}, Lepson/maintain/activity/MaintainActivity;->localSetBuyInkGroupVisibility(I)V

    .line 858
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationSeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 859
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 860
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingSeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 861
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintPreviewView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 862
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageSeparater:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 863
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 864
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistorySeparator:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 865
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 866
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxText:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 867
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_a
    :goto_5
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateBuyInkGroupForLocalOrIPPrinter()V
    .locals 4

    .line 879
    new-instance v0, Lepson/print/inkrpln/InkRplnRepository;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lepson/print/inkrpln/InkRplnRepository;-><init>(Z)V

    .line 880
    invoke-virtual {v0, p0}, Lepson/print/inkrpln/InkRplnRepository;->getInfo(Landroid/content/Context;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 881
    invoke-virtual {v0}, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->getButtonType()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x8

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/16 v3, 0x8

    goto :goto_1

    :pswitch_0
    const/16 v0, 0x8

    const/16 v1, 0x8

    const/16 v3, 0x8

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x0

    const/16 v3, 0x8

    .line 907
    :goto_1
    invoke-direct {p0, v1}, Lepson/maintain/activity/MaintainActivity;->localSetBuyInkGroupVisibility(I)V

    .line 908
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 909
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mInkDsView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 910
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkSeparator:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateMaintenanceBoxInfo([I)V
    .locals 12

    .line 1572
    iget v0, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroupVisibility:I

    const/16 v1, 0x8

    if-eqz p1, :cond_3

    .line 1573
    array-length v2, p1

    if-gtz v2, :cond_0

    goto/16 :goto_2

    .line 1589
    :cond_0
    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxText:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1590
    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxView:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1591
    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroup:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1594
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1595
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 1596
    iget-object v4, p0, Lepson/maintain/activity/MaintainActivity;->mDisplay:Landroid/view/Display;

    invoke-virtual {v4, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1597
    iget v2, v2, Landroid/graphics/Point;->x:I

    add-int/lit8 v2, v2, -0x1e

    div-int/lit8 v2, v2, 0x2

    .line 1599
    iget-object v4, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxView:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1601
    array-length v4, p1

    div-int/lit8 v4, v4, 0x2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    mul-int/lit8 v6, v5, 0x2

    .line 1603
    aget v7, p1, v6

    add-int/lit8 v6, v6, 0x1

    .line 1604
    aget v6, p1, v6

    const v8, 0x7f0a007e

    .line 1606
    iget-object v9, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8, v9, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    const v9, 0x7f0801a6

    .line 1608
    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 1609
    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const v9, 0x7f0801a4

    .line 1611
    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 1612
    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iput v2, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    const v9, 0x7f0801a5

    .line 1614
    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    if-lez v6, :cond_1

    .line 1617
    invoke-direct {p0, v7}, Lepson/maintain/activity/MaintainActivity;->isMaintenanceBoxStatusDisplayX(I)Z

    move-result v7

    if-nez v7, :cond_1

    add-int/lit8 v7, v2, -0x4

    int-to-float v7, v7

    .line 1619
    invoke-virtual {v9}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    const/high16 v11, 0x42c80000    # 100.0f

    div-float/2addr v7, v11

    int-to-float v6, v6

    mul-float v7, v7, v6

    float-to-int v6, v7

    iput v6, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    const v6, 0x7f05007c

    .line 1621
    invoke-virtual {v9, v6}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 1622
    invoke-virtual {v9, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 1624
    :cond_1
    invoke-virtual {v9}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iput v3, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    const v6, 0x7f0801a3

    .line 1625
    invoke-virtual {v8, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v6, 0x4

    .line 1626
    invoke-virtual {v9, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1628
    :goto_1
    iget-object v6, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxView:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    return-void

    .line 1576
    :cond_3
    :goto_2
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxText:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1577
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1579
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result p1

    if-ne v1, p1, :cond_4

    goto :goto_3

    .line 1583
    :cond_4
    iget p1, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroupVisibility:I

    :goto_3
    return-void
.end method

.method private updateSelectedPrinter()V
    .locals 5

    .line 1451
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->hideLoadProgressBar()V

    .line 1452
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->stopLoadConfigThread()V

    const/4 v0, 0x1

    .line 1454
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mIsNozzleCheckEnable:Ljava/lang/Boolean;

    .line 1455
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result v1

    iput v1, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    .line 1456
    iget v1, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 1457
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->startBackgroundThread()V

    .line 1458
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mIsStillUpdate:Z

    .line 1459
    invoke-direct {p0, v0}, Lepson/maintain/activity/MaintainActivity;->setButtonClickable(Z)V

    goto :goto_0

    .line 1461
    :cond_0
    iput-boolean v2, p0, Lepson/maintain/activity/MaintainActivity;->mIsStillUpdate:Z

    .line 1462
    invoke-direct {p0, v1, v2}, Lepson/maintain/activity/MaintainActivity;->handlerError(IZ)V

    .line 1465
    :goto_0
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetLang()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v0, :cond_1

    .line 1466
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1467
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1468
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    .line 1470
    :cond_1
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1471
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1472
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    iget v4, p0, Lepson/maintain/activity/MaintainActivity;->printerLocation:I

    if-ne v4, v0, :cond_2

    goto :goto_1

    :cond_2
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1476
    :goto_2
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetIp()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    return-void
.end method

.method private declared-synchronized updateStatus(I)V
    .locals 7

    monitor-enter p0

    .line 1482
    :try_start_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    if-nez p1, :cond_7

    .line 1485
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->isPrinterReady()I

    move-result p1

    const/4 v2, 0x3

    const/4 v3, 0x1

    if-eq p1, v3, :cond_0

    .line 1486
    invoke-direct {p0, v0}, Lepson/maintain/activity/MaintainActivity;->setButtonClickable(Z)V

    goto :goto_0

    .line 1488
    :cond_0
    invoke-direct {p0, v3}, Lepson/maintain/activity/MaintainActivity;->setButtonClickable(Z)V

    .line 1491
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetInkInfo()I

    move-result p1

    .line 1492
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 1493
    iput v2, v4, Landroid/os/Message;->what:I

    .line 1494
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "GET_INK_RESULT"

    .line 1495
    invoke-virtual {v5, v6, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1496
    invoke-virtual {v4, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1497
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1500
    :goto_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v0

    const/4 v4, 0x4

    if-ne p1, v4, :cond_5

    .line 1502
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v3

    const/16 v4, 0x66

    if-ne p1, v4, :cond_1

    const/4 p1, -0x1

    .line 1503
    iput p1, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    goto :goto_1

    .line 1505
    :cond_1
    iput v0, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    .line 1508
    :goto_1
    iget-boolean p1, p0, Lepson/maintain/activity/MaintainActivity;->isRemotePrinter:Z

    if-ne p1, v3, :cond_2

    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v3

    if-ne p1, v4, :cond_2

    .line 1509
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    .line 1512
    :cond_2
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v0

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget v0, v0, v3

    invoke-direct {p0, p1, v0}, Lepson/maintain/activity/MaintainActivity;->setErrorStatus(II)V

    .line 1514
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v3

    if-ne p1, v4, :cond_3

    .line 1515
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1518
    :cond_3
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v3

    const/4 v0, 0x6

    if-eq p1, v0, :cond_4

    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v3

    const/16 v0, 0x67

    if-eq p1, v0, :cond_4

    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v3

    const/16 v0, 0x68

    if-ne p1, v0, :cond_8

    .line 1520
    :cond_4
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetInkInfo()I

    move-result p1

    .line 1521
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1522
    iput v2, v0, Landroid/os/Message;->what:I

    .line 1523
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "GET_INK_RESULT"

    .line 1524
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1525
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1526
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    .line 1531
    :cond_5
    iput v0, p0, Lepson/maintain/activity/MaintainActivity;->escprSetPrinterSuccessed:I

    .line 1533
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget p1, p1, v0

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$StatusTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 1535
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1536
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    aget-object p1, p1, v3

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1538
    :cond_6
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stt Code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1539
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Err Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterStatus:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1543
    :cond_7
    invoke-direct {p0, p1, v0}, Lepson/maintain/activity/MaintainActivity;->handlerError(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1545
    :cond_8
    :goto_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public createInkView(Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;I)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 1876
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 1877
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 1878
    iget-object v4, v0, Lepson/maintain/activity/MaintainActivity;->mDisplay:Landroid/view/Display;

    invoke-virtual {v4, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1879
    iget v3, v3, Landroid/graphics/Point;->x:I

    add-int/lit8 v3, v3, -0x1e

    const/4 v4, 0x2

    div-int/2addr v3, v4

    .line 1881
    iget-object v5, v0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_0

    .line 1882
    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1885
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkNum()I

    move-result v5

    const v6, 0x7f0801a7

    if-lez v5, :cond_9

    .line 1888
    invoke-virtual {v0, v6}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1889
    iget-object v6, v0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1890
    iget-object v6, v0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroup:Landroid/widget/LinearLayout;

    iget v8, v0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroupVisibility:I

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_a

    .line 1893
    invoke-virtual {v1, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkRemainingAmount(I)I

    move-result v8

    .line 1896
    invoke-virtual {v1, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkName(I)I

    move-result v9

    .line 1897
    invoke-static {v9}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->getColorName(I)[Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x1

    if-nez v10, :cond_2

    .line 1900
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "colorNo = "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1903
    sget-object v9, Lepson/common/Constants$ColorName;->EPS_COLOR_WHITE:Lepson/common/Constants$ColorName;

    invoke-virtual {v9}, Lepson/common/Constants$ColorName;->ordinal()I

    move-result v9

    invoke-static {v9}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->getColorName(I)[Ljava/lang/Integer;

    move-result-object v10

    if-nez v10, :cond_1

    return-void

    .line 1909
    :cond_1
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v10, v11

    goto :goto_1

    .line 1911
    :cond_2
    invoke-direct {v0, v9, v10}, Lepson/maintain/activity/MaintainActivity;->logColorInfo(I[Ljava/lang/Integer;)V

    :goto_1
    const v9, 0x7f0a007e

    const/4 v12, 0x0

    .line 1914
    invoke-virtual {v2, v9, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    const v12, 0x7f0801a6

    .line 1915
    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 1916
    aget-object v13, v10, v11

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    if-eqz v13, :cond_3

    .line 1917
    aget-object v13, v10, v11

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_3
    const-string v13, ""

    .line 1919
    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    const v12, 0x7f0801a4

    .line 1922
    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout;

    .line 1923
    invoke-virtual {v12}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    iput v3, v12, Landroid/view/ViewGroup$LayoutParams;->width:I

    const v12, 0x7f0801a5

    .line 1925
    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    const/4 v13, 0x4

    add-int/lit8 v14, v3, -0x4

    int-to-float v14, v14

    .line 1928
    invoke-virtual {v1, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkStatus(I)I

    move-result v15

    const/4 v13, -0x4

    if-eq v15, v13, :cond_7

    if-eqz p2, :cond_4

    goto :goto_4

    .line 1937
    :cond_4
    invoke-virtual {v12}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    invoke-static {v8, v14}, Lepson/maintain/activity/MaintainActivity;->calcInkWidth(IF)I

    move-result v14

    iput v14, v13, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1941
    invoke-virtual {v1, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkStatus(I)I

    move-result v13

    if-eqz v13, :cond_5

    .line 1942
    invoke-virtual {v1, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkStatus(I)I

    move-result v13

    if-eq v13, v11, :cond_5

    .line 1943
    invoke-virtual {v1, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkStatus(I)I

    move-result v11

    if-eq v11, v4, :cond_5

    .line 1944
    invoke-virtual {v12}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    iput v7, v11, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1946
    :cond_5
    aget-object v11, v10, v7

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-direct {v0, v11}, Lepson/maintain/activity/MaintainActivity;->checkIfWhite(I)Z

    move-result v11

    if-eqz v11, :cond_6

    const v11, 0x7f08038e

    .line 1947
    invoke-virtual {v9, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 1948
    aget-object v10, v10, v7

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v11, v10}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1949
    invoke-virtual {v11, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 1951
    :cond_6
    aget-object v10, v10, v7

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v12, v10}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 1953
    :goto_3
    invoke-virtual {v12, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-gtz v8, :cond_8

    const v8, 0x7f0801a3

    .line 1956
    invoke-virtual {v9, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    :cond_7
    :goto_4
    const v8, 0x7f0801a3

    .line 1930
    invoke-virtual {v12}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    iput v7, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v10, 0x4

    .line 1931
    invoke-virtual {v12, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1933
    invoke-virtual {v1, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkStatus(I)I

    move-result v10

    if-eq v10, v13, :cond_8

    .line 1934
    invoke-virtual {v9, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1959
    :cond_8
    :goto_5
    iget-object v8, v0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 1963
    :cond_9
    invoke-virtual {v0, v6}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1964
    iget-object v1, v0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_a
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 1390
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    return-void

    .line 1394
    :pswitch_0
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p1

    .line 1395
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p3

    if-nez p3, :cond_0

    .line 1396
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->finish()V

    return-void

    :cond_0
    const/4 p3, 0x1

    if-ne p2, p3, :cond_3

    .line 1403
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz p2, :cond_1

    .line 1406
    :try_start_0
    invoke-interface {p2}, Lepson/print/service/IEpsonService;->refreshRemotePrinterLogin()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 1415
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->isSimpleApOrP2p(Lepson/print/MyPrinter;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 1416
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->startInkDsDialog()V

    goto :goto_1

    .line 1418
    :cond_2
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->onInkReplenishDialogEnd()V

    goto :goto_1

    .line 1421
    :cond_3
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->onInkReplenishDialogEnd()V

    :goto_1
    return-void

    .line 1426
    :pswitch_1
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->onInkReplenishDialogEnd()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .line 926
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e04e6

    const v2, 0x7f0e052b

    const v3, 0x7f0e04f1

    const v4, 0x7f0e04c5

    const v5, 0x7f080230

    const v6, 0x7f0700ad

    const/4 v7, 0x1

    const/4 v8, 0x0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_2

    .line 1077
    :sswitch_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->btnTogglePreview:Landroid/widget/Switch;

    invoke-virtual {p1}, Landroid/widget/Switch;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1078
    sget-boolean p1, Lepson/maintain/activity/MaintainActivity;->isMessageScreenShowing:Z

    if-eq p1, v7, :cond_0

    .line 1079
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->showMessageDialog()V

    goto/16 :goto_2

    :cond_0
    const-string p1, "PREFS_EPSON_CONNECT"

    .line 1083
    invoke-virtual {p0, p1, v8}, Lepson/maintain/activity/MaintainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 1084
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "ENABLE_SHOW_PREVIEW"

    .line 1085
    invoke-interface {p1, v0, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1086
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_2

    :cond_1
    const-string p1, "PREFS_EPSON_CONNECT"

    .line 1091
    invoke-virtual {p0, p1, v8}, Lepson/maintain/activity/MaintainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 1092
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "ENABLE_SHOW_PREVIEW"

    .line 1093
    invoke-interface {p1, v0, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1094
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_2

    .line 1099
    :sswitch_1
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->btnToggleAlert:Landroid/widget/Switch;

    invoke-virtual {p1}, Landroid/widget/Switch;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "PREFS_EPSON_CONNECT"

    .line 1102
    invoke-virtual {p0, p1, v8}, Lepson/maintain/activity/MaintainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 1103
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "ENABLE_SHOW_WARNING"

    .line 1104
    invoke-interface {p1, v0, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1105
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_2

    :cond_2
    const-string p1, "PREFS_EPSON_CONNECT"

    .line 1109
    invoke-virtual {p0, p1, v8}, Lepson/maintain/activity/MaintainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 1110
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "ENABLE_SHOW_WARNING"

    .line 1111
    invoke-interface {p1, v0, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1112
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_2

    .line 1009
    :sswitch_2
    iget p1, p0, Lepson/maintain/activity/MaintainActivity;->printerLocation:I

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_2

    :pswitch_0
    const-string p1, "https://www.epsonconnect.com/user/"

    .line 1047
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 1048
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1049
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 1014
    :pswitch_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result p1

    if-ne p1, v7, :cond_3

    iget-boolean p1, p0, Lepson/maintain/activity/MaintainActivity;->isKeepSimpleAPConnection:Z

    if-nez p1, :cond_3

    .line 1015
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1016
    invoke-virtual {p1, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e051d

    .line 1017
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    new-instance v1, Lepson/maintain/activity/MaintainActivity$8;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainActivity$8;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    .line 1018
    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1031
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 1032
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void

    .line 1038
    :cond_3
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetIp()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    const-string p1, "http"

    .line 1040
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    const-string v1, "/"

    invoke-static {p1, v0, v1}, Lepson/common/IPAddressUtils;->buildURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 1041
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1042
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_3
    const-string p1, "MAINTAIN"

    const-string v0, "onClick() R.id.printer"

    .line 928
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    new-instance p1, Lepson/maintain/activity/MaintainActivity$3;

    invoke-direct {p1, p0}, Lepson/maintain/activity/MaintainActivity$3;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    new-array v0, v8, [Ljava/lang/Void;

    .line 966
    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainActivity$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_2

    .line 1061
    :sswitch_4
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lepson/maintain/activity/EccPrintLog;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1062
    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    :sswitch_5
    const-string p1, "dialog_progress"

    .line 1072
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->showDialog(Ljava/lang/String;)V

    .line 1073
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->OnlineRegTask(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 971
    :sswitch_6
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v7, "NozzlleCheck"

    invoke-static {v0, v7}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 972
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 973
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    if-ne p1, v5, :cond_4

    goto :goto_0

    :cond_4
    const v3, 0x7f0e04c5

    :goto_0
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lepson/maintain/activity/MaintainActivity$5;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainActivity$5;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    .line 974
    invoke-virtual {p1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lepson/maintain/activity/MaintainActivity$4;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainActivity$4;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    .line 979
    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 983
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_2

    :sswitch_7
    const-string p1, "Maintenance-BuyInk"

    .line 1066
    invoke-static {p0, p1}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    const-string p1, "dialog_progress"

    .line 1067
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity;->showDialog(Ljava/lang/String;)V

    .line 1068
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    invoke-direct {p0, v7, p1}, Lepson/maintain/activity/MaintainActivity;->BuyInkTask(ILjava/lang/String;)V

    goto :goto_2

    :sswitch_8
    const-string p1, "Maintenance-ReadyInk"

    .line 1117
    invoke-static {p0, p1}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 1118
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->startBrowsReadyInkUrl()V

    return-void

    .line 986
    :sswitch_9
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v7, "HeadCleaning"

    invoke-static {v0, v7}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 987
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 988
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    if-ne p1, v5, :cond_5

    goto :goto_1

    :cond_5
    const v3, 0x7f0e04c5

    :goto_1
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lepson/maintain/activity/MaintainActivity$7;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainActivity$7;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    .line 989
    invoke-virtual {p1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lepson/maintain/activity/MaintainActivity$6;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainActivity$6;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    .line 994
    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 998
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_2

    .line 1001
    :sswitch_a
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "Firmware Update"

    invoke-static {p1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 1002
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lepson/maintain/activity/FirmwareUpdateActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "PRINTER_IP"

    .line 1003
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "PRINTER_MODEL_NAME"

    .line 1004
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1005
    iput-boolean v7, p0, Lepson/maintain/activity/MaintainActivity;->isKeepSimpleAPConnection:Z

    .line 1006
    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 1055
    :sswitch_b
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lepson/epsonconnectregistration/ActivityECConfiguration;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "Epson-Connect-BLE-Content"

    .line 1056
    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1057
    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity;->startActivity(Landroid/content/Intent;)V

    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f08012c -> :sswitch_b
        0x7f08015f -> :sswitch_a
        0x7f080179 -> :sswitch_9
        0x7f080209 -> :sswitch_8
        0x7f08020f -> :sswitch_7
        0x7f080230 -> :sswitch_6
        0x7f080239 -> :sswitch_5
        0x7f080274 -> :sswitch_4
        0x7f08027e -> :sswitch_3
        0x7f080289 -> :sswitch_2
        0x7f080348 -> :sswitch_1
        0x7f08034b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 1373
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "MAINTAIN"

    const-string v1, "onCreate IN"

    .line 231
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0089

    .line 233
    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity;->setContentView(I)V

    .line 234
    iput-object p0, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    const/4 p1, 0x0

    .line 237
    iput p1, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroupVisibility:I

    const/4 v0, 0x1

    const v1, 0x7f0e04db

    .line 240
    invoke-virtual {p0, v1, v0}, Lepson/maintain/activity/MaintainActivity;->setActionBar(IZ)V

    .line 242
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v1, :cond_0

    .line 243
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lepson/print/service/EpsonService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1, v2, v0}, Lepson/maintain/activity/MaintainActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 247
    :cond_0
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mDisplay:Landroid/view/Display;

    const v0, 0x7f08027e

    .line 248
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080284

    .line 249
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterName:Landroid/widget/TextView;

    const v0, 0x7f0802f4

    .line 250
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mVSerialSeparator:Landroid/view/View;

    const v0, 0x7f0802f2

    .line 251
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLLSerialNo:Landroid/widget/LinearLayout;

    const v0, 0x7f0802f3

    .line 252
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvSerialNo:Landroid/widget/TextView;

    const v0, 0x7f08028c

    .line 253
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatus:Landroid/widget/TextView;

    const v0, 0x7f08028d

    .line 254
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTvPrinterStatusDetail:Landroid/widget/TextView;

    const v0, 0x7f080334

    .line 255
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mTextMaintenance:Landroid/widget/TextView;

    const v0, 0x7f0801a8

    .line 256
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mInkView:Landroid/widget/LinearLayout;

    const v0, 0x7f080208

    .line 257
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxText:Landroid/widget/TextView;

    const v0, 0x7f080207

    .line 258
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mMaintenanceBoxView:Landroid/widget/LinearLayout;

    const v0, 0x7f0800c0

    .line 260
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mCleanCheckView:Landroid/widget/LinearLayout;

    const v0, 0x7f080179

    .line 261
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    .line 262
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080178

    .line 263
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mHeadCleanText:Landroid/widget/TextView;

    const v0, 0x7f080230

    .line 264
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    .line 265
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08022f

    .line 266
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mNozzleCheckText:Landroid/widget/TextView;

    const v0, 0x7f08015f

    .line 267
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    .line 268
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08015e

    .line 269
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mFWUpdateText:Landroid/widget/TextView;

    const v0, 0x7f08034b

    .line 270
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->btnTogglePreview:Landroid/widget/Switch;

    .line 271
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->btnTogglePreview:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080348

    .line 272
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->btnToggleAlert:Landroid/widget/Switch;

    .line 273
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->btnToggleAlert:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080289

    .line 275
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    .line 276
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08012c

    .line 277
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    .line 278
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080274

    .line 279
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    .line 280
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistoryView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080239

    .line 281
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationView:Landroid/widget/LinearLayout;

    .line 282
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080210

    .line 284
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkGroup:Landroid/widget/LinearLayout;

    const v0, 0x7f08020f

    .line 285
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkView:Landroid/widget/LinearLayout;

    .line 286
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0800a8

    .line 287
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mBuyInkSeparator:Landroid/view/View;

    const v0, 0x7f080209

    .line 288
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mInkDsView:Landroid/widget/LinearLayout;

    .line 289
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mInkDsView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080279

    .line 291
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintPreviewView:Landroid/widget/LinearLayout;

    const v0, 0x7f08005c

    .line 292
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageView:Landroid/widget/LinearLayout;

    const v0, 0x7f08028b

    .line 294
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingSeparator:Landroid/view/View;

    const v0, 0x7f080276

    .line 295
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrintHistorySeparator:Landroid/view/View;

    const v0, 0x7f08023b

    .line 296
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mOnlineRegistrationSeparator:Landroid/view/View;

    const v0, 0x7f0800d7

    .line 297
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mAlertMesseageSeparater:Landroid/view/View;

    .line 300
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinterSettingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    const v0, 0x7f080338

    .line 301
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mContext:Landroid/content/Context;

    .line 302
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const v0, 0x7f08028a

    .line 303
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    .line 304
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mVSerialSeparator:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mLLSerialNo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const-string v0, "MAINTAIN"

    const-string v1, "onCreate :: doInitDriver"

    .line 310
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    .line 313
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v2, "SCAN_REFS_SETTINGS_RESEACH"

    invoke-static {v0, v1, v2}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->isResearchScanner:Z

    .line 317
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 321
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->startSelectPrinterActivity()V

    .line 324
    :cond_1
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainActivity;->mWaiteInkDsDialog:Z

    .line 327
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity;->modelBuyInkOnlineRegTask:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    .line 333
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity;->modelBuyInkOnlineRegTask:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    invoke-virtual {p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->getUriEvent()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lepson/maintain/activity/-$$Lambda$MaintainActivity$QSe8Ag60V_SghLRNasnBbcKjo9I;

    invoke-direct {v0, p0}, Lepson/maintain/activity/-$$Lambda$MaintainActivity$QSe8Ag60V_SghLRNasnBbcKjo9I;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 1720
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    const-string v0, "MAINTAIN"

    const-string v1, "onDestroy"

    .line 1721
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1723
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 1725
    :try_start_0
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 1726
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainActivity;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1728
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "MaintainActivity"

    const-string v1, "onPause"

    .line 1750
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1752
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mIsCancelProbe:Z

    .line 1753
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->hideLoadProgressBar()V

    .line 1754
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 1755
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->stopLoadConfigThread()V

    .line 1756
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->interrupMaintainThread()V

    .line 1757
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->endBackGroundThread()V

    .line 1759
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mUiHandler:Landroid/os/Handler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1762
    iget-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->isKeepSimpleAPConnection:Z

    if-nez v0, :cond_0

    const-string v0, "printer"

    .line 1763
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1766
    :cond_0
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "MaintainActivity"

    const-string v1, "onResume()"

    .line 1737
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x0

    .line 1741
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->isKeepSimpleAPConnection:Z

    .line 1743
    iget-boolean v0, p0, Lepson/maintain/activity/MaintainActivity;->mWaiteInkDsDialog:Z

    if-nez v0, :cond_0

    .line 1744
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity;->startLoadConThread()V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 0

    .line 1772
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    return-void
.end method

.method startBrowseNozzleCheckGuidance()V
    .locals 2

    const-string v0, "dialog_progress"

    .line 2154
    invoke-direct {p0, v0}, Lepson/maintain/activity/MaintainActivity;->showDialog(Ljava/lang/String;)V

    .line 2155
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->printerIp:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-direct {p0, v1, v0}, Lepson/maintain/activity/MaintainActivity;->BuyInkTask(ILjava/lang/String;)V

    return-void
.end method

.method public updateInkInfo(I)V
    .locals 1

    .line 1548
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object v0

    .line 1549
    invoke-virtual {p0, v0, p1}, Lepson/maintain/activity/MaintainActivity;->createInkView(Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;I)V

    return-void
.end method

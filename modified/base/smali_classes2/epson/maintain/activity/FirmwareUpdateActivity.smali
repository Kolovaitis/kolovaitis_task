.class public Lepson/maintain/activity/FirmwareUpdateActivity;
.super Landroid/app/Activity;
.source "FirmwareUpdateActivity.java"

# interfaces
.implements Lepson/maintain/FirmwareManager$FWUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;
    }
.end annotation


# static fields
.field static final PRINTER_IP:Ljava/lang/String; = "PRINTER_IP"

.field static final PRINTER_MODEL_NAME:Ljava/lang/String; = "PRINTER_MODEL_NAME"

.field static final RECONNECT_SIMPLE_AP:I = 0x64

.field private static final REQUEST_CODE_EULA:I = 0x65


# instance fields
.field mCancelButton:Landroid/widget/Button;

.field mDialog:Landroid/app/AlertDialog;

.field mFWManager:Lepson/maintain/FirmwareManager;

.field final mGoBackOnClickListenner:Landroid/content/DialogInterface$OnClickListener;

.field mInterrupted:Z

.field mMessageText:Landroid/widget/TextView;

.field mPrinterIP:Ljava/lang/String;

.field mProgress:Landroid/widget/ProgressBar;

.field mReconnectingSimpleAP:Z

.field mShouldReconnectSimpleAP:Z

.field mTitleText:Landroid/widget/TextView;

.field private mWaitEulaCheckActivityEnd:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    new-instance v0, Lepson/maintain/activity/FirmwareUpdateActivity$1;

    invoke-direct {v0, p0}, Lepson/maintain/activity/FirmwareUpdateActivity$1;-><init>(Lepson/maintain/activity/FirmwareUpdateActivity;)V

    iput-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mGoBackOnClickListenner:Landroid/content/DialogInterface$OnClickListener;

    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mDialog:Landroid/app/AlertDialog;

    const/4 v0, 0x0

    .line 41
    iput-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mShouldReconnectSimpleAP:Z

    iput-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mReconnectingSimpleAP:Z

    iput-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mInterrupted:Z

    return-void
.end method

.method static synthetic access$000(Lepson/maintain/activity/FirmwareUpdateActivity;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->startEulaActivity()V

    return-void
.end method

.method private callStartDownload()V
    .locals 2

    .line 282
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const/4 v0, 0x1

    .line 284
    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->setProgressVisibility(Z)V

    .line 285
    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->setProcCancelable(Z)V

    .line 286
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0379

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lepson/maintain/activity/FirmwareUpdateActivity;->setMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    invoke-virtual {v0, p0}, Lepson/maintain/FirmwareManager;->startFWDownload(Lepson/maintain/FirmwareManager$FWUpdateListener;)V

    return-void
.end method

.method private startEulaActivity()V
    .locals 2

    const/4 v0, 0x1

    .line 273
    iput-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mWaitEulaCheckActivityEnd:Z

    .line 274
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/maintain/activity/CheckEulaActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lepson/maintain/activity/FirmwareUpdateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method dismissAlertDialog()V
    .locals 1

    .line 323
    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .line 142
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 143
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    return p1

    .line 147
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public fwManagerDidEndProc(Lepson/maintain/FirmwareManager$FWUpdateProcResult;Lepson/maintain/FirmwareManager$FWUpdateStep;)V
    .locals 9

    .line 213
    invoke-virtual {p0, p1, p2}, Lepson/maintain/activity/FirmwareUpdateActivity;->getEndProcTitleAndMessage(Lepson/maintain/FirmwareManager$FWUpdateProcResult;Lepson/maintain/FirmwareManager$FWUpdateStep;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 214
    aget-object v3, v0, v1

    const/4 v2, 0x1

    aget-object v4, v0, v2

    .line 217
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v5, 0x80

    invoke-virtual {v0, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 219
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const/4 v6, 0x0

    if-ne p1, v0, :cond_3

    .line 220
    sget-object v0, Lepson/maintain/activity/FirmwareUpdateActivity$4;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    invoke-virtual {p2}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v7

    aget v0, v0, v7

    const/4 v7, 0x5

    if-eq v0, v7, :cond_3

    packed-switch v0, :pswitch_data_0

    .line 259
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not Reachable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 222
    :pswitch_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 223
    new-instance p1, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;

    invoke-direct {p1, p0}, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;-><init>(Lepson/maintain/activity/FirmwareUpdateActivity;)V

    new-array p2, v1, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 225
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    invoke-virtual {p1, p0}, Lepson/maintain/FirmwareManager;->startGetUpdateInf(Lepson/maintain/FirmwareManager$FWUpdateListener;)V

    :goto_0
    return-void

    .line 237
    :pswitch_1
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0e0389

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v6}, Lepson/maintain/activity/FirmwareUpdateActivity;->setMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/view/Window;->addFlags(I)V

    .line 242
    iget-boolean p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mShouldReconnectSimpleAP:Z

    if-eqz p1, :cond_1

    .line 244
    iput-boolean v2, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mReconnectingSimpleAP:Z

    const-string p1, "printer"

    const/16 p2, 0x64

    .line 245
    invoke-static {p0, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result p1

    if-nez p1, :cond_2

    .line 246
    iput-boolean v1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mReconnectingSimpleAP:Z

    .line 247
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    sget-object p2, Lepson/maintain/FirmwareManager$FWUpdateStep;->ReadyUpdate:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p0, p1, p2}, Lepson/maintain/activity/FirmwareUpdateActivity;->fwManagerDidEndProc(Lepson/maintain/FirmwareManager$FWUpdateProcResult;Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    goto :goto_1

    .line 250
    :cond_1
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    invoke-virtual {p1, p0}, Lepson/maintain/FirmwareManager;->startFWUpdate(Lepson/maintain/FirmwareManager$FWUpdateListener;)V

    :cond_2
    :goto_1
    return-void

    :pswitch_2
    const p1, 0x7f0e052b

    .line 229
    invoke-virtual {p0, p1}, Lepson/maintain/activity/FirmwareUpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lepson/maintain/activity/FirmwareUpdateActivity$3;

    invoke-direct {v6, p0}, Lepson/maintain/activity/FirmwareUpdateActivity$3;-><init>(Lepson/maintain/activity/FirmwareUpdateActivity;)V

    const p1, 0x7f0e04e6

    .line 234
    invoke-virtual {p0, p1}, Lepson/maintain/activity/FirmwareUpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mGoBackOnClickListenner:Landroid/content/DialogInterface$OnClickListener;

    move-object v2, p0

    .line 229
    invoke-virtual/range {v2 .. v8}, Lepson/maintain/activity/FirmwareUpdateActivity;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    return-void

    :cond_3
    :goto_2
    if-nez v3, :cond_5

    if-eqz v4, :cond_4

    goto :goto_3

    .line 264
    :cond_4
    sget-object p2, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Interrupted:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    if-eq p1, p2, :cond_6

    .line 265
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->goBack()V

    goto :goto_4

    .line 263
    :cond_5
    :goto_3
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mGoBackOnClickListenner:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v3, v4, p1, v6}, Lepson/maintain/activity/FirmwareUpdateActivity;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V

    :cond_6
    :goto_4
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public fwManagerProcDoing(I)V
    .locals 1

    .line 209
    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.method getEndProcTitleAndMessage(Lepson/maintain/FirmwareManager$FWUpdateProcResult;Lepson/maintain/FirmwareManager$FWUpdateStep;)[Ljava/lang/String;
    .locals 5

    .line 153
    sget-object v0, Lepson/maintain/activity/FirmwareUpdateActivity$4;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateProcResult:[I

    invoke-virtual {p1}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const v0, 0x7f0e038a

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 196
    :pswitch_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "MSG_CANCELED"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 193
    :pswitch_1
    new-array p1, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 194
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e037a

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto/16 :goto_1

    .line 189
    :pswitch_2
    new-array p1, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 190
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e037f

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto/16 :goto_1

    .line 186
    :pswitch_3
    new-array p1, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0380

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 187
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0381

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto/16 :goto_1

    .line 174
    :pswitch_4
    sget-object p1, Lepson/maintain/activity/FirmwareUpdateActivity$4;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    invoke-virtual {p2}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result p2

    aget p1, p1, p2

    if-eq p1, v4, :cond_1

    const/4 p2, 0x5

    if-eq p1, p2, :cond_0

    goto/16 :goto_0

    .line 180
    :cond_0
    new-array p1, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0387

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 181
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0388

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto/16 :goto_1

    .line 176
    :cond_1
    new-array p1, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0383

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 177
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0384

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto/16 :goto_1

    .line 160
    :pswitch_5
    sget-object p1, Lepson/maintain/activity/FirmwareUpdateActivity$4;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    invoke-virtual {p2}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 168
    :pswitch_6
    new-array p1, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v3, 0x7f0e037b

    invoke-virtual {p2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 169
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto :goto_1

    .line 163
    :pswitch_7
    new-array p1, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e037c

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 164
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e037d

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto :goto_1

    .line 157
    :pswitch_8
    new-array p1, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0385

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 158
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0386

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto :goto_1

    .line 155
    :pswitch_9
    new-array p1, v3, [Ljava/lang/String;

    aput-object v1, p1, v2

    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e0382

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v4

    goto :goto_1

    :goto_0
    move-object p1, v1

    :goto_1
    if-nez p1, :cond_2

    .line 201
    filled-new-array {v1, v1}, [Ljava/lang/String;

    move-result-object p1

    :cond_2
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method goBack()V
    .locals 0

    .line 327
    invoke-static {}, Lepson/maintain/FirmwareManager;->removeSavedFile()V

    .line 328
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->dismissAlertDialog()V

    .line 329
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->finish()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .line 113
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v0, "onAcitivityReuslt requestCode=%d resultCode=%d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p3, -0x1

    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 115
    iput-boolean v3, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mShouldReconnectSimpleAP:Z

    iput-boolean v3, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mReconnectingSimpleAP:Z

    if-ne p2, p3, :cond_0

    .line 116
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 117
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    invoke-virtual {p1, p0}, Lepson/maintain/FirmwareManager;->startFWUpdate(Lepson/maintain/FirmwareManager$FWUpdateListener;)V

    goto :goto_0

    .line 119
    :cond_0
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    sget-object p2, Lepson/maintain/FirmwareManager$FWUpdateStep;->ReadyUpdate:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p0, p1, p2}, Lepson/maintain/activity/FirmwareUpdateActivity;->fwManagerDidEndProc(Lepson/maintain/FirmwareManager$FWUpdateProcResult;Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x65

    if-eq p1, v0, :cond_2

    return-void

    .line 126
    :cond_2
    iput-boolean v3, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mWaitEulaCheckActivityEnd:Z

    if-ne p2, p3, :cond_4

    .line 128
    iget-boolean p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mInterrupted:Z

    if-eqz p1, :cond_3

    return-void

    .line 131
    :cond_3
    invoke-direct {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->callStartDownload()V

    goto :goto_1

    .line 134
    :cond_4
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->goBack()V

    :goto_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a0072

    .line 52
    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->setContentView(I)V

    .line 53
    new-instance v0, Lepson/maintain/FirmwareManager;

    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lepson/maintain/FirmwareManager;-><init>(Landroid/content/Context;Lepson/print/MyPrinter;)V

    iput-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    const v0, 0x7f080219

    .line 54
    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mMessageText:Landroid/widget/TextView;

    const v0, 0x7f08021a

    .line 55
    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mTitleText:Landroid/widget/TextView;

    const v0, 0x7f080187

    .line 56
    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mProgress:Landroid/widget/ProgressBar;

    const v0, 0x7f0800b0

    .line 57
    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mCancelButton:Landroid/widget/Button;

    const/4 v0, 0x0

    .line 61
    iput-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mWaitEulaCheckActivityEnd:Z

    .line 63
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v1

    .line 64
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "PRINTER_IP"

    .line 65
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mPrinterIP:Ljava/lang/String;

    .line 66
    iget-object v3, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mPrinterIP:Ljava/lang/String;

    if-nez v3, :cond_0

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mPrinterIP:Ljava/lang/String;

    .line 67
    :cond_0
    new-instance v3, Lepson/print/MyPrinter;

    const-string v4, "PRINTER_MODEL_NAME"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mPrinterIP:Ljava/lang/String;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v2, v4, v5, v1}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v1, Lepson/maintain/FirmwareManager;

    invoke-direct {v1, p0, v3}, Lepson/maintain/FirmwareManager;-><init>(Landroid/content/Context;Lepson/print/MyPrinter;)V

    iput-object v1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    .line 70
    iget-object v1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mCancelButton:Landroid/widget/Button;

    const v2, 0x7f0e0476

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 71
    iget-object v1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mCancelButton:Landroid/widget/Button;

    new-instance v2, Lepson/maintain/activity/FirmwareUpdateActivity$2;

    invoke-direct {v2, p0}, Lepson/maintain/activity/FirmwareUpdateActivity$2;-><init>(Lepson/maintain/activity/FirmwareUpdateActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0378

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lepson/maintain/activity/FirmwareUpdateActivity;->setMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->setProcCancelable(Z)V

    if-nez p1, :cond_1

    .line 78
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    invoke-virtual {p1, p0}, Lepson/maintain/FirmwareManager;->startVersionCheck(Lepson/maintain/FirmwareManager$FWUpdateListener;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    .line 84
    iput-boolean p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mInterrupted:Z

    :goto_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 98
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 100
    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 103
    :cond_0
    iget-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mWaitEulaCheckActivityEnd:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mReconnectingSimpleAP:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 104
    iput-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mInterrupted:Z

    .line 105
    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    invoke-virtual {v0}, Lepson/maintain/FirmwareManager;->interruptProc()V

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 5

    .line 89
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 90
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onResume mReconnectingSimpleAP=%b mInterrupted=%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mReconnectingSimpleAP:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-boolean v3, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mInterrupted:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-boolean v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mInterrupted:Z

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e037e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e038a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mGoBackOnClickListenner:Landroid/content/DialogInterface$OnClickListener;

    const/4 v3, 0x0

    .line 92
    invoke-virtual {p0, v0, v1, v2, v3}, Lepson/maintain/activity/FirmwareUpdateActivity;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V

    :cond_0
    return-void
.end method

.method setMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x8

    if-eqz p1, :cond_0

    .line 299
    iget-object v2, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 302
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    if-eqz p2, :cond_1

    .line 305
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mMessageText:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mMessageText:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 308
    :cond_1
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mMessageText:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method setProcCancelable(Z)V
    .locals 1

    .line 292
    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mCancelButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method setProgressVisibility(Z)V
    .locals 1

    .line 295
    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mProgress:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method showAlertDialog(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 9

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    const v1, 0x7f0e04f2

    .line 313
    invoke-virtual {p0, v1}, Lepson/maintain/activity/FirmwareUpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object v5, v0

    :goto_0
    if-eqz p4, :cond_1

    const v0, 0x7f0e0476

    invoke-virtual {p0, v0}, Lepson/maintain/activity/FirmwareUpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v7, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object v8, p4

    .line 314
    invoke-virtual/range {v2 .. v8}, Lepson/maintain/activity/FirmwareUpdateActivity;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method showAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1

    .line 317
    invoke-virtual {p0}, Lepson/maintain/activity/FirmwareUpdateActivity;->dismissAlertDialog()V

    .line 318
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 319
    invoke-virtual {p1, p3, p4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1, p5, p6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mDialog:Landroid/app/AlertDialog;

    .line 320
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

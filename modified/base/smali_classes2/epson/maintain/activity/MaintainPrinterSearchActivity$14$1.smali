.class Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;
.super Landroid/os/AsyncTask;
.source "MaintainPrinterSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

.field final synthetic val$printer:Lepson/print/MyPrinter;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainPrinterSearchActivity$14;Lepson/print/MyPrinter;)V
    .locals 0

    .line 1037
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iput-object p2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->val$printer:Lepson/print/MyPrinter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 5

    .line 1057
    invoke-static {}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1300()Lepson/scan/lib/escanLib;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1, v0}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    move-result p1

    const/16 v0, -0x41a

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq p1, v0, :cond_1

    if-eqz p1, :cond_0

    .line 1067
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    .line 1071
    :goto_0
    :try_start_0
    invoke-static {}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1300()Lepson/scan/lib/escanLib;

    move-result-object v0

    iget-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v3, v3, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {v3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->val$printer:Lepson/print/MyPrinter;

    invoke-static {v0, v3, v4}, Lepson/scan/lib/ScanSettingHelper;->recodeScannerInfo(Lepson/scan/lib/escanLib;Landroid/content/Context;Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;
    :try_end_0
    .catch Lepson/scan/lib/EscanLibException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v2, :cond_2

    .line 1078
    invoke-static {}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1300()Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 1082
    :cond_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception v0

    goto :goto_1

    .line 1074
    :catch_0
    :try_start_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq p1, v2, :cond_3

    .line 1078
    invoke-static {}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1300()Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :cond_3
    return-object v0

    :goto_1
    if-eq p1, v2, :cond_4

    invoke-static {}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1300()Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :cond_4
    throw v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1037
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    .line 1087
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1088
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 1092
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    const/4 v0, 0x4

    .line 1093
    iput v0, p1, Landroid/os/Message;->what:I

    .line 1094
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->val$printer:Lepson/print/MyPrinter;

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1095
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1037
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .line 1041
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    if-nez v0, :cond_0

    .line 1042
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    new-instance v1, Lepson/print/screen/WorkingDialog;

    iget-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v2, v2, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-direct {v1, v2}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    .line 1044
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1045
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    :cond_1
    return-void
.end method

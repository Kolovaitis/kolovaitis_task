.class Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;
.super Landroid/os/AsyncTask;
.source "MaintainBuyInkOnlineRegTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->loadData(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$printerIp:Ljava/lang/String;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 89
    iput-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    iput-object p2, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->val$printerIp:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/net/Uri;
    .locals 1

    .line 98
    iget-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->val$context:Landroid/content/Context;

    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->val$printerIp:Ljava/lang/String;

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainActivity;->disableSimpleApAndWait(Landroid/content/Context;Ljava/lang/String;)Z

    .line 100
    iget-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->val$context:Landroid/content/Context;

    const-string v0, "eRegistration"

    invoke-static {p1, v0}, Lepson/print/Util/BuyInkUrl;->urlSupport(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 101
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 89
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->doInBackground([Ljava/lang/Void;)Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/net/Uri;)V
    .locals 1

    .line 108
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    invoke-virtual {v0, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->setUriEvent(Landroid/net/Uri;)V

    .line 110
    iget-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->setReady(Z)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 89
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->onPostExecute(Landroid/net/Uri;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 92
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 93
    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->setReady(Z)V

    return-void
.end method

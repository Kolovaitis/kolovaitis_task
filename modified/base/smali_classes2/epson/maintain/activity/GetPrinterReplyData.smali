.class public Lepson/maintain/activity/GetPrinterReplyData;
.super Landroid/support/v7/app/AppCompatActivity;
.source "GetPrinterReplyData.java"

# interfaces
.implements Lcom/epson/mobilephone/common/escpr/MediaInfo;
.implements Lepson/common/DialogProgress$DialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/maintain/activity/GetPrinterReplyData$CustomProDialog;
    }
.end annotation


# static fields
.field private static final CLEANING_ERROR:I = 0xa

.field private static final DELAY_TIME_MAINTAIN:I = 0xa

.field private static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field private static final END_REPLYDATA:I = 0x63

.field private static final EPS_COMM_BID:I = 0x2

.field private static final EPS_LANG_ESCPR:I = 0x1

.field private static final EPS_MNT_CLEANING:I = 0x2

.field private static final EPS_MNT_NOZZLE:I = 0x3

.field private static final EPS_PRNERR_CEMPTY:I = 0x67

.field private static final EPS_PRNERR_CFAIL:I = 0x68

.field private static final EPS_PRNERR_DISABEL_CLEANING:I = 0x6c

.field private static final EPS_PRNERR_INKOUT:I = 0x6

.field private static final GET_PRINTER_NAME:I = 0x8

.field private static final HANDLE_ERROR:I = 0x4

.field public static final ITEM_HEIGHT:I = 0x28

.field private static final MAINTAIN_OK:I = 0x1

.field private static final MAINTAIN_START:I = 0x0

.field private static final NO_PRINTER_NAME:I = 0x9

.field public static final PREFS_NAME:Ljava/lang/String; = "PrintSetting"

.field private static final PROBE_ERROR:I = 0x7

.field private static final PROBE_RESULT:Ljava/lang/String; = "PROBE_ERROR"

.field private static final START_LOADCONFIG:I = 0x6

.field private static final TAG:Ljava/lang/String; = "MAINTAIN"

.field private static final UPDATE_INK:I = 0x3

.field private static final UPDATE_SELECTED_PRINTER:I = 0x5

.field private static final UPDATE_STT:I = 0x2

.field public static mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;


# instance fields
.field private customPro:Landroid/app/Dialog;

.field private extParam:Lcom/epson/iprint/shared/SharedParamStatus;

.field isGotError:Z

.field isResearchScanner:Z

.field private mBackGround:Ljava/lang/Thread;

.field private mDoMaintainThread:Ljava/lang/Thread;

.field mHavePrinter:Z

.field volatile mIsCancelProbe:Z

.field mIsStillUpdate:Z

.field mIsStop:Z

.field private mLoadConfigThread:Ljava/lang/Thread;

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mPrinterId:Ljava/lang/String;

.field private mPrinterIp:Ljava/lang/String;

.field private mPrinterLocation:I

.field private mPrinterName:Ljava/lang/String;

.field private mPrinterStatus:[I

.field mStartSearch:Z

.field mUiHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 41
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x0

    .line 95
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->isGotError:Z

    .line 96
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    .line 97
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mStartSearch:Z

    const/4 v1, 0x1

    .line 98
    iput-boolean v1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsStillUpdate:Z

    .line 99
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mHavePrinter:Z

    .line 100
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->isResearchScanner:Z

    .line 101
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsStop:Z

    .line 250
    new-instance v0, Lepson/maintain/activity/GetPrinterReplyData$2;

    invoke-direct {v0, p0}, Lepson/maintain/activity/GetPrinterReplyData$2;-><init>(Lepson/maintain/activity/GetPrinterReplyData;)V

    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/maintain/activity/GetPrinterReplyData;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lepson/maintain/activity/GetPrinterReplyData;->loadConfig()V

    return-void
.end method

.method static synthetic access$100(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterIp:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lepson/maintain/activity/GetPrinterReplyData;)I
    .locals 0

    .line 41
    iget p0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterLocation:I

    return p0
.end method

.method static synthetic access$500(Lepson/maintain/activity/GetPrinterReplyData;I)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lepson/maintain/activity/GetPrinterReplyData;->setupErrorMessage(I)V

    return-void
.end method

.method static synthetic access$600(Lepson/maintain/activity/GetPrinterReplyData;)Lcom/epson/iprint/shared/SharedParamStatus;
    .locals 0

    .line 41
    iget-object p0, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    return-object p0
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 378
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 380
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private inkCodeToString(I)Ljava/lang/String;
    .locals 0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string p1, "unKnown"

    goto :goto_0

    :pswitch_1
    const-string p1, "Clean"

    goto :goto_0

    :pswitch_2
    const-string p1, "White"

    goto :goto_0

    :pswitch_3
    const-string p1, "Green"

    goto :goto_0

    :pswitch_4
    const-string p1, "Orange"

    goto :goto_0

    :pswitch_5
    const-string p1, "Black"

    goto :goto_0

    :pswitch_6
    const-string p1, "Gloss Optimizer"

    goto :goto_0

    :pswitch_7
    const-string p1, "Light Gray"

    goto :goto_0

    :pswitch_8
    const-string p1, "Blue"

    goto :goto_0

    :pswitch_9
    const-string p1, "Red"

    goto :goto_0

    :pswitch_a
    const-string p1, "Light Black"

    goto :goto_0

    :pswitch_b
    const-string p1, "Dark Yellow"

    goto :goto_0

    :pswitch_c
    const-string p1, "Light Yellow"

    goto :goto_0

    :pswitch_d
    const-string p1, "Light Magenta"

    goto :goto_0

    :pswitch_e
    const-string p1, "Light Cyan"

    goto :goto_0

    :pswitch_f
    const-string p1, "Yellow"

    goto :goto_0

    :pswitch_10
    const-string p1, "Magenta"

    goto :goto_0

    :pswitch_11
    const-string p1, "Cyan"

    goto :goto_0

    :pswitch_12
    const-string p1, "Black"

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic lambda$onCreate$0(Lepson/maintain/activity/GetPrinterReplyData;Ljava/util/Deque;)V
    .locals 2

    .line 126
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 128
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 129
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 132
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    invoke-direct {p0, v0}, Lepson/maintain/activity/GetPrinterReplyData;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 136
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 137
    invoke-direct {p0, v0}, Lepson/maintain/activity/GetPrinterReplyData;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private loadConfig()V
    .locals 2

    .line 718
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 719
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterId:Ljava/lang/String;

    .line 720
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterName:Ljava/lang/String;

    .line 721
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterIp:Ljava/lang/String;

    .line 722
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v0

    iput v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterLocation:I

    return-void
.end method

.method private lockRotation()V
    .locals 1

    .line 166
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x0

    .line 169
    invoke-virtual {p0, v0}, Lepson/maintain/activity/GetPrinterReplyData;->setRequestedOrientation(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    .line 172
    invoke-virtual {p0, v0}, Lepson/maintain/activity/GetPrinterReplyData;->setRequestedOrientation(I)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private saveSelectedPrinter()V
    .locals 4

    .line 728
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PrintSetting"

    const-string v2, "PRINTER_NAME"

    iget-object v3, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterName:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PrintSetting"

    const-string v2, "PRINTER_IP"

    iget-object v3, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterIp:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PrintSetting"

    const-string v2, "PRINTER_ID"

    iget-object v3, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setupErrorMessage(I)V
    .locals 2

    const p1, 0x7f0e01b9

    .line 328
    invoke-virtual {p0, p1}, Lepson/maintain/activity/GetPrinterReplyData;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e01b8

    .line 329
    invoke-virtual {p0, v0}, Lepson/maintain/activity/GetPrinterReplyData;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e04f2

    invoke-virtual {p0, v1}, Lepson/maintain/activity/GetPrinterReplyData;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 328
    invoke-static {p0, p1, v0, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 329
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 3

    .line 361
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x0

    const v2, -0x14b98bc

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const v0, 0x7f0e051b

    .line 363
    invoke-virtual {p0, v0}, Lepson/maintain/activity/GetPrinterReplyData;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    .line 369
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 370
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_3
    return-void
.end method


# virtual methods
.method protected OLDonPause()V
    .locals 1

    const/4 v0, 0x1

    .line 689
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsStop:Z

    .line 690
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    .line 691
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->cancelLoadConPro()V

    .line 692
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 693
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->endLoadConThread()V

    .line 694
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->interrupMaintainThread()V

    .line 695
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->endBackGroundThread()V

    .line 696
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    return-void
.end method

.method public cancelLoadConPro()V
    .locals 2

    .line 386
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method public cancelProDia()V
    .locals 1

    .line 342
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->customPro:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->customPro:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    const/4 v0, 0x0

    .line 344
    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->customPro:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public endBackGroundThread()V
    .locals 1

    const/4 v0, 0x0

    .line 450
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsStillUpdate:Z

    .line 451
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mBackGround:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mBackGround:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 454
    :try_start_0
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mBackGround:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 458
    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mBackGround:Ljava/lang/Thread;

    :cond_0
    return-void
.end method

.method public endLoadConThread()V
    .locals 1

    const/4 v0, 0x1

    .line 390
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    .line 391
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mLoadConfigThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mLoadConfigThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 394
    :try_start_0
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mLoadConfigThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 396
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_0
    const/4 v0, 0x0

    .line 398
    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mLoadConfigThread:Ljava/lang/Thread;

    :cond_0
    return-void
.end method

.method public handlerError(IZ)V
    .locals 0

    .line 651
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->cancelLoadConPro()V

    return-void
.end method

.method public interrupMaintainThread()V
    .locals 1

    .line 667
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mDoMaintainThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mDoMaintainThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    .line 669
    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mDoMaintainThread:Ljava/lang/Thread;

    :cond_0
    return-void
.end method

.method public isPrinterReady()I
    .locals 5

    .line 655
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterStatus:[I

    .line 656
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterStatus:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    aget v2, v0, v1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    aget v2, v0, v1

    const/4 v4, 0x3

    if-ne v2, v4, :cond_0

    goto :goto_0

    .line 659
    :cond_0
    aget v0, v0, v1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    return v3

    :cond_2
    :goto_0
    return v1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 482
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    if-ne p2, v0, :cond_1

    const-string p1, "printer position"

    const/4 p2, 0x0

    .line 486
    invoke-virtual {p3, p1, p2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    .line 487
    sget-object p2, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p2, p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setMSearchPos(I)V

    const-string p1, "PRINTER_NAME"

    .line 488
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterName:Ljava/lang/String;

    const-string p1, "PRINTER_ID"

    .line 489
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterId:Ljava/lang/String;

    const-string p1, "PRINTER_IP"

    .line 490
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterIp:Ljava/lang/String;

    .line 491
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "PrintSetting"

    const-string p3, "RE_SEARCH"

    invoke-static {p1, p2, p3, v0}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 492
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 495
    :cond_1
    invoke-direct {p0}, Lepson/maintain/activity/GetPrinterReplyData;->saveSelectedPrinter()V

    :goto_0
    return-void
.end method

.method public onCancelDialog(Ljava/lang/String;)V
    .locals 2

    .line 750
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x14b98bc

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    .line 752
    :cond_2
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->stopAllThread()V

    .line 753
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->finish()V

    :goto_2
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .line 467
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 471
    sget-object p1, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetStatus()I

    move-result p1

    .line 472
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x2

    .line 473
    iput v1, v0, Landroid/os/Message;->what:I

    .line 474
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "GET_STT_RESULT"

    .line 475
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 476
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 477
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 117
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 120
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgressViewModel;

    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 121
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lepson/maintain/activity/-$$Lambda$GetPrinterReplyData$qHy6t-VO-Y5M7yWXDFHiN9DcheU;

    invoke-direct {v0, p0}, Lepson/maintain/activity/-$$Lambda$GetPrinterReplyData$qHy6t-VO-Y5M7yWXDFHiN9DcheU;-><init>(Lepson/maintain/activity/GetPrinterReplyData;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 142
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "extStatus"

    .line 144
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/shared/SharedParamStatus;

    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    .line 145
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    if-nez p1, :cond_0

    .line 146
    new-instance p1, Lcom/epson/iprint/shared/SharedParamStatus;

    invoke-direct {p1}, Lcom/epson/iprint/shared/SharedParamStatus;-><init>()V

    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    .line 149
    :cond_0
    sget-object p1, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    if-nez p1, :cond_1

    .line 150
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    sput-object p1, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    :cond_1
    const-string p1, "MAINTAIN"

    const-string v0, "onCreate"

    .line 153
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-direct {p0}, Lepson/maintain/activity/GetPrinterReplyData;->lockRotation()V

    .line 158
    sget-object p1, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/4 v0, 0x2

    invoke-virtual {p1, p0, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    .line 160
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->startLoadConThread()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 675
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->stopAllThread()V

    .line 676
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    const-string v0, "MAINTAIN"

    const-string v1, "onDestroy"

    .line 677
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onNegativeClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onNeutralClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 0

    .line 684
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    return-void
.end method

.method public onPositiveClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onStop()V
    .locals 0

    .line 702
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStop()V

    return-void
.end method

.method public showLoadConPro()V
    .locals 2

    .line 352
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method public showProDia()V
    .locals 3

    .line 335
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->customPro:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 336
    new-instance v0, Lepson/maintain/activity/GetPrinterReplyData$CustomProDialog;

    const v1, 0x7f0f0009

    const v2, 0x7f0a008b

    invoke-direct {v0, p0, p0, v1, v2}, Lepson/maintain/activity/GetPrinterReplyData$CustomProDialog;-><init>(Lepson/maintain/activity/GetPrinterReplyData;Landroid/content/Context;II)V

    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->customPro:Landroid/app/Dialog;

    .line 338
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->customPro:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public startBackgroundThread()V
    .locals 2

    const/4 v0, 0x1

    .line 403
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsStillUpdate:Z

    .line 404
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lepson/maintain/activity/GetPrinterReplyData$3;

    invoke-direct {v1, p0}, Lepson/maintain/activity/GetPrinterReplyData$3;-><init>(Lepson/maintain/activity/GetPrinterReplyData;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mBackGround:Ljava/lang/Thread;

    .line 445
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mBackGround:Ljava/lang/Thread;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 446
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mBackGround:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public startLoadConThread()V
    .locals 2

    const/4 v0, 0x0

    .line 180
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    .line 181
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lepson/maintain/activity/GetPrinterReplyData$1;

    invoke-direct {v1, p0}, Lepson/maintain/activity/GetPrinterReplyData$1;-><init>(Lepson/maintain/activity/GetPrinterReplyData;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mLoadConfigThread:Ljava/lang/Thread;

    .line 247
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mLoadConfigThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method stopAllThread()V
    .locals 1

    .line 796
    iget-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 800
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsStop:Z

    .line 801
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    .line 802
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    if-eqz v0, :cond_1

    .line 803
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 806
    :cond_1
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->cancelLoadConPro()V

    .line 807
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->endLoadConThread()V

    .line 808
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->interrupMaintainThread()V

    .line 810
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->endBackGroundThread()V

    return-void
.end method

.method public updateInkInfo(I)V
    .locals 8

    .line 617
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object v0

    if-nez p1, :cond_1

    .line 620
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkNum()I

    move-result p1

    .line 621
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 622
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 624
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, -0x1

    .line 628
    iget-object v5, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    iget-object v6, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/epson/iprint/shared/SharedParamStatus;->setPrinter_name(Ljava/lang/String;)V

    .line 629
    iget-object v5, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    iget-object v6, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterStatus:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Lcom/epson/iprint/shared/SharedParamStatus;->setPrinter_status(I)V

    :goto_0
    if-ge v7, p1, :cond_0

    .line 631
    invoke-virtual {v0, v7}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkCode(I)I

    move-result v5

    invoke-direct {p0, v5}, Lepson/maintain/activity/GetPrinterReplyData;->inkCodeToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    invoke-virtual {v0, v7}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkRemainingAmount(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 633
    invoke-virtual {v0, v7}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getInkStatus(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 635
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    invoke-virtual {p1, v1}, Lcom/epson/iprint/shared/SharedParamStatus;->setArrayOutInkName(Ljava/util/ArrayList;)V

    .line 636
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    invoke-virtual {p1, v2}, Lcom/epson/iprint/shared/SharedParamStatus;->setArrayOutInkRemain(Ljava/util/ArrayList;)V

    .line 637
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->extParam:Lcom/epson/iprint/shared/SharedParamStatus;

    invoke-virtual {p1, v3}, Lcom/epson/iprint/shared/SharedParamStatus;->setEscprlibInkStatus(Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_1
    const/4 v4, 0x1

    .line 639
    :goto_1
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/16 v0, 0x63

    .line 640
    iput v0, p1, Landroid/os/Message;->what:I

    .line 641
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "END_RELPY_RESULT"

    .line 642
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 643
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 644
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public updateSelectedPrinter()V
    .locals 2

    .line 500
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->cancelLoadConPro()V

    .line 501
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->endLoadConThread()V

    .line 503
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result v0

    if-nez v0, :cond_0

    .line 505
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->startBackgroundThread()V

    const/4 v0, 0x1

    .line 506
    iput-boolean v0, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsStillUpdate:Z

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 508
    iput-boolean v1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mIsStillUpdate:Z

    .line 509
    invoke-virtual {p0, v0, v1}, Lepson/maintain/activity/GetPrinterReplyData;->handlerError(IZ)V

    :goto_0
    return-void
.end method

.method public updateStatus(I)V
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_2

    .line 516
    invoke-virtual {p0}, Lepson/maintain/activity/GetPrinterReplyData;->isPrinterReady()I

    move-result p1

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    goto :goto_0

    .line 519
    :cond_0
    sget-object p1, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetInkInfo()I

    move-result p1

    .line 520
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 521
    iput v1, v3, Landroid/os/Message;->what:I

    .line 522
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "GET_INK_RESULT"

    .line 523
    invoke-virtual {v4, v5, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 524
    invoke-virtual {v3, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 525
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 528
    :goto_0
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinterStatus:[I

    aget v0, p1, v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    .line 529
    aget v0, p1, v2

    const/4 v3, 0x6

    if-eq v0, v3, :cond_1

    aget v0, p1, v2

    const/16 v3, 0x67

    if-eq v0, v3, :cond_1

    aget p1, p1, v2

    const/16 v0, 0x68

    if-ne p1, v0, :cond_3

    .line 531
    :cond_1
    sget-object p1, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetInkInfo()I

    move-result p1

    .line 532
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 533
    iput v1, v0, Landroid/os/Message;->what:I

    .line 534
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "GET_INK_RESULT"

    .line 535
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 536
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 537
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 541
    :cond_2
    invoke-virtual {p0, p1, v0}, Lepson/maintain/activity/GetPrinterReplyData;->handlerError(IZ)V

    :cond_3
    :goto_1
    return-void
.end method

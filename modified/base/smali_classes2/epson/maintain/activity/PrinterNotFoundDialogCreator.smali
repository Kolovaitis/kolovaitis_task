.class public Lepson/maintain/activity/PrinterNotFoundDialogCreator;
.super Ljava/lang/Object;
.source "PrinterNotFoundDialogCreator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPrinterNotFoundDialog(Landroid/content/Context;ZI)Lepson/print/CustomLayoutDialogFragment;
    .locals 5

    const v0, 0x7f0e0473

    const v1, 0x7f0e00bf

    if-eqz p1, :cond_0

    const p1, 0x7f0e01a9

    .line 27
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, "PrintSetting"

    const-string v2, "RE_SEARCH"

    .line 29
    invoke-static {p0, p1, v2}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 32
    invoke-static {p0}, Lepson/common/Utils;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 33
    invoke-static {p0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    const v2, 0x7f0e00be

    const/4 v4, 0x1

    .line 34
    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v3

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const p1, 0x7f0e01ae

    .line 39
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 38
    invoke-static {p2, p0, v1, v3, v0}, Lepson/print/CustomLayoutDialogFragment;->newInstance(ILjava/lang/String;III)Lepson/print/CustomLayoutDialogFragment;

    move-result-object p0

    return-object p0

    :cond_2
    const p1, 0x7f0e01a8

    .line 44
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 47
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n\n"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const p1, 0x7f0e0309

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const p1, 0x7f0e02ea

    .line 49
    invoke-static {p2, p0, v1, p1, v0}, Lepson/print/CustomLayoutDialogFragment;->newInstance(ILjava/lang/String;III)Lepson/print/CustomLayoutDialogFragment;

    move-result-object p0

    return-object p0
.end method

.method public static getStartIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    const v0, 0x7f0e0438

    .line 62
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 63
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.class Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;
.super Landroid/os/AsyncTask;
.source "FirmwareUpdateActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/activity/FirmwareUpdateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisconnectAndStartGetUpdateInfTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/FirmwareUpdateActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/FirmwareUpdateActivity;)V
    .locals 0

    .line 333
    iput-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;->this$0:Lepson/maintain/activity/FirmwareUpdateActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1

    .line 336
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;->this$0:Lepson/maintain/activity/FirmwareUpdateActivity;

    iget-object v0, p1, Lepson/maintain/activity/FirmwareUpdateActivity;->mPrinterIP:Ljava/lang/String;

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainActivity;->disableSimpleApAndWait(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 333
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    .line 340
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 341
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 342
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;->this$0:Lepson/maintain/activity/FirmwareUpdateActivity;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lepson/maintain/activity/FirmwareUpdateActivity;->mShouldReconnectSimpleAP:Z

    .line 343
    iget-object p1, p1, Lepson/maintain/activity/FirmwareUpdateActivity;->mFWManager:Lepson/maintain/FirmwareManager;

    iget-object v0, p0, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;->this$0:Lepson/maintain/activity/FirmwareUpdateActivity;

    invoke-virtual {p1, v0}, Lepson/maintain/FirmwareManager;->startGetUpdateInf(Lepson/maintain/FirmwareManager$FWUpdateListener;)V

    goto :goto_0

    .line 345
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;->this$0:Lepson/maintain/activity/FirmwareUpdateActivity;

    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetUpdateInf:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p1, v0, v1}, Lepson/maintain/activity/FirmwareUpdateActivity;->fwManagerDidEndProc(Lepson/maintain/FirmwareManager$FWUpdateProcResult;Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 333
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/FirmwareUpdateActivity$DisconnectAndStartGetUpdateInfTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

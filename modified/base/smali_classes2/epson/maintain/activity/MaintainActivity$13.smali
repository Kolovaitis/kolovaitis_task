.class Lepson/maintain/activity/MaintainActivity$13;
.super Ljava/lang/Object;
.source "MaintainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainActivity;->performMaintain(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainActivity;

.field final synthetic val$cmd:I


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainActivity;I)V
    .locals 0

    .line 1778
    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    iput p2, p0, Lepson/maintain/activity/MaintainActivity$13;->val$cmd:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 1781
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    iget v1, p0, Lepson/maintain/activity/MaintainActivity$13;->val$cmd:I

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doDoMainteCmd(I)I

    move-result v0

    const/16 v1, 0x6c

    const/16 v2, 0xa

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v0, v1, :cond_0

    .line 1788
    iget-object v5, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v5, v3}, Lepson/maintain/activity/MaintainActivity;->access$3702(Lepson/maintain/activity/MaintainActivity;Z)Z

    const/16 v5, 0xb

    .line 1790
    iget-object v6, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v6}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v5, 0x0

    const/16 v6, 0xb

    goto :goto_0

    :cond_0
    const/4 v5, 0x1

    const/4 v6, 0x0

    :cond_1
    :goto_0
    if-nez v5, :cond_2

    if-gt v6, v2, :cond_7

    :cond_2
    const-wide/16 v7, 0x3e8

    .line 1795
    :try_start_0
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    add-int/lit8 v6, v6, 0x1

    if-ne v0, v1, :cond_3

    .line 1800
    :try_start_1
    iget-object v5, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v5, v3}, Lepson/maintain/activity/MaintainActivity;->access$3702(Lepson/maintain/activity/MaintainActivity;Z)Z

    .line 1801
    iget-object v5, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v5}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception v5

    move-object v7, v5

    const/4 v5, 0x0

    goto :goto_5

    .line 1805
    :cond_3
    :try_start_2
    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v7

    invoke-virtual {v7}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetStatus()I

    .line 1806
    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7}, Lepson/maintain/activity/MaintainActivity;->access$3800(Lepson/maintain/activity/MaintainActivity;)[I

    move-result-object v7

    aget v7, v7, v4

    const/4 v8, 0x4

    if-ne v7, v8, :cond_4

    .line 1807
    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7, v3}, Lepson/maintain/activity/MaintainActivity;->access$3702(Lepson/maintain/activity/MaintainActivity;Z)Z

    .line 1808
    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 1810
    :cond_4
    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7, v4}, Lepson/maintain/activity/MaintainActivity;->access$3702(Lepson/maintain/activity/MaintainActivity;Z)Z

    .line 1813
    :goto_1
    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7}, Lepson/maintain/activity/MaintainActivity;->access$3800(Lepson/maintain/activity/MaintainActivity;)[I

    move-result-object v7

    aget v7, v7, v4

    if-eqz v7, :cond_6

    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7}, Lepson/maintain/activity/MaintainActivity;->access$3800(Lepson/maintain/activity/MaintainActivity;)[I

    move-result-object v7

    aget v5, v7, v4

    if-ne v5, v8, :cond_5

    goto :goto_2

    :cond_5
    const/4 v5, 0x1

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v5, 0x0

    .line 1819
    :goto_3
    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7}, Lepson/maintain/activity/MaintainActivity;->access$3700(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v7
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v7, :cond_1

    .line 1826
    :cond_7
    :goto_4
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$13;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :catch_1
    move-exception v7

    .line 1823
    :goto_5
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

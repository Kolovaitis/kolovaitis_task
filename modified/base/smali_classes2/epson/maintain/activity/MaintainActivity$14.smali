.class Lepson/maintain/activity/MaintainActivity$14;
.super Ljava/lang/Object;
.source "MaintainActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/activity/MaintainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 2093
    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity$14;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    .line 2107
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$14;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p2}, Lepson/print/service/IEpsonService$Stub;->asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/maintain/activity/MaintainActivity;->access$502(Lepson/maintain/activity/MaintainActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    .line 2109
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$14;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$500(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2111
    :try_start_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$14;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$500(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object p2, p0, Lepson/maintain/activity/MaintainActivity$14;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p2}, Lepson/maintain/activity/MaintainActivity;->access$3900(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object p2

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->registerCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2113
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .line 2098
    :try_start_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$14;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$500(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$14;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$3900(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2100
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2102
    :goto_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$14;->this$0:Lepson/maintain/activity/MaintainActivity;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainActivity;->access$502(Lepson/maintain/activity/MaintainActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    return-void
.end method

.class Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;
.super Landroid/os/AsyncTask;
.source "EccPrintLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/activity/EccPrintLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RemotePrintHistoryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lepson/maintain/activity/LogUrl;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field mEccPrintLog:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lepson/maintain/activity/EccPrintLog;",
            ">;"
        }
    .end annotation
.end field

.field mErrorCode:I

.field private mLogUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lepson/maintain/activity/EccPrintLog;)V
    .locals 1

    .line 275
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 276
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->mEccPrintLog:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 268
    check-cast p1, [Lepson/maintain/activity/LogUrl;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->doInBackground([Lepson/maintain/activity/LogUrl;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lepson/maintain/activity/LogUrl;)Ljava/lang/Void;
    .locals 2

    const/4 v0, 0x0

    .line 287
    aget-object p1, p1, v0

    const/4 v1, 0x0

    .line 289
    :try_start_0
    invoke-virtual {p1}, Lepson/maintain/activity/LogUrl;->getUrl()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->mLogUrl:Ljava/lang/String;

    .line 290
    iput v0, p0, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->mErrorCode:I
    :try_end_0
    .catch Lepson/maintain/activity/EccPrintLogException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception p1

    .line 292
    invoke-virtual {p1}, Lepson/maintain/activity/EccPrintLogException;->getErrorCode()I

    move-result p1

    iput p1, p0, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->mErrorCode:I

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 268
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    const-string p1, "EccPrintLog"

    const-string v0, "onPostExecute() begin"

    .line 302
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object p1, p0, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->mEccPrintLog:Ljava/lang/ref/WeakReference;

    if-nez p1, :cond_0

    return-void

    .line 307
    :cond_0
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/maintain/activity/EccPrintLog;

    if-nez p1, :cond_1

    return-void

    .line 312
    :cond_1
    iget-object v0, p0, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->mLogUrl:Ljava/lang/String;

    iget v1, p0, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->mErrorCode:I

    invoke-virtual {p1, v0, v1}, Lepson/maintain/activity/EccPrintLog;->setUrl(Ljava/lang/String;I)V

    const-string p1, "EccPrintLog"

    const-string v0, "onPostExecute() end"

    .line 314
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setActivity(Lepson/maintain/activity/EccPrintLog;)V
    .locals 1

    .line 281
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->mEccPrintLog:Ljava/lang/ref/WeakReference;

    return-void
.end method

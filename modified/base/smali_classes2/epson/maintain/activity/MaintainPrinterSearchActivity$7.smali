.class Lepson/maintain/activity/MaintainPrinterSearchActivity$7;
.super Ljava/lang/Object;
.source "MaintainPrinterSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainPrinterSearchActivity;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 549
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .line 552
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result p1

    const/4 v0, 0x0

    const v1, 0x7f0e0340

    const v2, 0x7f0e0341

    const/16 v3, 0x20

    const v4, 0x7f0e052b

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 610
    :pswitch_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    if-lt p1, v3, :cond_0

    .line 611
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$700(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 612
    invoke-virtual {p1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 613
    invoke-virtual {v0, v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 614
    invoke-virtual {v0, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 615
    invoke-virtual {v0, v4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$7$4;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$7$4;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity$7;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 620
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 625
    :cond_0
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/16 v1, 0xa

    .line 626
    iput v1, p1, Landroid/os/Message;->what:I

    .line 627
    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 628
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 557
    :pswitch_1
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    if-lt p1, v3, :cond_1

    .line 558
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$700(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 559
    invoke-virtual {p1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 560
    invoke-virtual {v0, v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 561
    invoke-virtual {v0, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 562
    invoke-virtual {v0, v4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$7$1;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$7$1;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity$7;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 567
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 573
    :cond_1
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const-string v1, "PREFS_EPSON_CONNECT"

    invoke-virtual {p1, v1, v5}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v1, "ENABLE_SHOW_WARNING"

    const/4 v2, 0x1

    .line 574
    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-ne p1, v2, :cond_2

    .line 577
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$700(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 578
    invoke-virtual {p1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const v2, 0x7f0e0362

    .line 579
    invoke-virtual {v1, v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const v2, 0x7f0e0363

    .line 580
    invoke-virtual {v1, v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 579
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 581
    invoke-virtual {v0, v4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$7$3;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$7$3;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity$7;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const v1, 0x7f0e04e6

    .line 591
    invoke-virtual {v0, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$7$2;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$7$2;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity$7;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 596
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 599
    :cond_2
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x7

    .line 600
    iput v1, p1, Landroid/os/Message;->what:I

    .line 601
    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 602
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

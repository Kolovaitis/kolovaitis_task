.class Lepson/maintain/activity/LogUrl;
.super Ljava/lang/Object;
.source "EccPrintLog.java"


# instance fields
.field mAccessKey:Ljava/lang/String;

.field mClientId:Ljava/lang/String;

.field mMailAddress:Ljava/lang/String;

.field mPrinterName:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getUrl()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/maintain/activity/EccPrintLogException;
        }
    .end annotation

    .line 354
    new-instance v0, Lepson/print/ecclient/EcClientLib;

    invoke-direct {v0}, Lepson/print/ecclient/EcClientLib;-><init>()V

    .line 355
    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->Initialize()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 359
    iget-object v1, p0, Lepson/maintain/activity/LogUrl;->mMailAddress:Ljava/lang/String;

    iget-object v2, p0, Lepson/maintain/activity/LogUrl;->mAccessKey:Ljava/lang/String;

    iget-object v3, p0, Lepson/maintain/activity/LogUrl;->mClientId:Ljava/lang/String;

    iget-object v4, p0, Lepson/maintain/activity/LogUrl;->mPrinterName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lepson/print/ecclient/EcClientLib;->GetPrintLogUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 363
    iget-object v1, v0, Lepson/print/ecclient/EcClientLib;->mLogUri:Ljava/lang/String;

    .line 364
    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->Terminate()V

    return-object v1

    .line 361
    :cond_0
    new-instance v0, Lepson/maintain/activity/EccPrintLogException;

    invoke-direct {v0, v1}, Lepson/maintain/activity/EccPrintLogException;-><init>(I)V

    throw v0

    .line 356
    :cond_1
    new-instance v0, Lepson/maintain/activity/EccPrintLogException;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lepson/maintain/activity/EccPrintLogException;-><init>(I)V

    throw v0
.end method

.method public setContextDependValue(Landroid/content/Context;)V
    .locals 3

    .line 337
    invoke-static {p1}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object v0

    const-string v1, "PRINTER_CLIENT_ID"

    const-string v2, ""

    .line 339
    invoke-virtual {v0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/LogUrl;->mClientId:Ljava/lang/String;

    .line 340
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u25b2clientId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/maintain/activity/LogUrl;->mClientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 342
    invoke-static {p1}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 344
    invoke-virtual {v0, p1}, Lepson/print/MyPrinter;->getRemotePrinterAccessKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 343
    invoke-static {v1}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/LogUrl;->mAccessKey:Ljava/lang/String;

    .line 346
    invoke-virtual {v0, p1}, Lepson/print/MyPrinter;->getUserDefName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 345
    invoke-static {p1}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/maintain/activity/LogUrl;->mPrinterName:Ljava/lang/String;

    .line 348
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    .line 347
    invoke-static {p1}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/maintain/activity/LogUrl;->mMailAddress:Ljava/lang/String;

    return-void
.end method

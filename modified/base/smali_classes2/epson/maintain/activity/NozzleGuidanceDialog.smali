.class public Lepson/maintain/activity/NozzleGuidanceDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "NozzleGuidanceDialog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/maintain/activity/NozzleGuidanceDialog;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lepson/maintain/activity/NozzleGuidanceDialog;->startBrowser()V

    return-void
.end method

.method private startBrowser()V
    .locals 1

    .line 64
    invoke-virtual {p0}, Lepson/maintain/activity/NozzleGuidanceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lepson/maintain/activity/MaintainActivity;

    if-nez v0, :cond_0

    return-void

    .line 69
    :cond_0
    invoke-virtual {v0}, Lepson/maintain/activity/MaintainActivity;->startBrowseNozzleCheckGuidance()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .line 22
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/maintain/activity/NozzleGuidanceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-virtual {p0}, Lepson/maintain/activity/NozzleGuidanceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0a0055

    const/4 v2, 0x0

    .line 25
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0800d0

    .line 27
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 28
    new-instance v2, Lepson/maintain/activity/NozzleGuidanceDialog$1;

    invoke-direct {v2, p0}, Lepson/maintain/activity/NozzleGuidanceDialog$1;-><init>(Lepson/maintain/activity/NozzleGuidanceDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f080083

    .line 36
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 37
    new-instance v2, Lepson/maintain/activity/NozzleGuidanceDialog$2;

    invoke-direct {v2, p0}, Lepson/maintain/activity/NozzleGuidanceDialog$2;-><init>(Lepson/maintain/activity/NozzleGuidanceDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 47
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 48
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 49
    new-instance v0, Lepson/maintain/activity/NozzleGuidanceDialog$3;

    invoke-direct {v0, p0}, Lepson/maintain/activity/NozzleGuidanceDialog$3;-><init>(Lepson/maintain/activity/NozzleGuidanceDialog;)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object p1
.end method

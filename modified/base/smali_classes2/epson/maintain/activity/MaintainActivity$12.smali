.class Lepson/maintain/activity/MaintainActivity$12;
.super Ljava/lang/Object;
.source "MaintainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainActivity;->startBackgroundThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 1268
    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 1276
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetStatus()I

    move-result v0

    .line 1277
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x2

    .line 1278
    iput v2, v1, Landroid/os/Message;->what:I

    .line 1279
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "GET_STT_RESULT"

    .line 1280
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1281
    invoke-virtual {v1, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1282
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1285
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetInkInfo()I

    move-result v0

    .line 1286
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v3, 0x3

    .line 1287
    iput v3, v1, Landroid/os/Message;->what:I

    .line 1288
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "GET_INK_RESULT"

    .line 1289
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1290
    invoke-virtual {v1, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1291
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1293
    new-instance v0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;-><init>()V

    .line 1294
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getBatteryInfo(Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)I

    move-result v1

    if-nez v1, :cond_0

    .line 1295
    iget v3, v0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->powerSourceType:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 1298
    :goto_0
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    const/16 v5, 0xf

    .line 1299
    iput v5, v4, Landroid/os/Message;->what:I

    .line 1300
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v7, "GET_BATTERY_RESULT"

    .line 1301
    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "GET_BATTERY_INFO"

    .line 1302
    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1303
    invoke-virtual {v4, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1304
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1306
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMaintenanceBoxInformation()[I

    move-result-object v1

    .line 1307
    iget-object v4, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v4}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v7

    invoke-static {v4, v7, v1}, Lepson/maintain/activity/MaintainActivity;->access$3500(Lepson/maintain/activity/MaintainActivity;Landroid/os/Handler;[I)V

    .line 1310
    :goto_1
    :try_start_0
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$3600(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "MAINTAIN"

    const-string v4, "thread update stt running"

    .line 1311
    invoke-static {v1, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetStatus()I

    move-result v1

    .line 1313
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 1314
    iput v2, v4, Landroid/os/Message;->what:I

    const-string v7, "GET_STT_RESULT"

    .line 1315
    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1316
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1317
    invoke-virtual {v4, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1318
    iget-object v6, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v6}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    if-eqz v3, :cond_1

    .line 1321
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getBatteryInfo(Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)I

    move-result v1

    .line 1322
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 1323
    iput v5, v4, Landroid/os/Message;->what:I

    .line 1324
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v7, "GET_BATTERY_RESULT"

    .line 1325
    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "GET_BATTERY_INFO"

    .line 1326
    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1327
    invoke-virtual {v4, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1328
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    :cond_1
    move-object v6, v1

    .line 1333
    :goto_2
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMaintenanceBoxInformation()[I

    move-result-object v1

    .line 1334
    iget-object v4, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    iget-object v7, p0, Lepson/maintain/activity/MaintainActivity$12;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v7}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v7

    invoke-static {v4, v7, v1}, Lepson/maintain/activity/MaintainActivity;->access$3500(Lepson/maintain/activity/MaintainActivity;Landroid/os/Handler;[I)V

    const-wide/16 v7, 0xfa0

    .line 1336
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v1, "startBackgroundThread"

    const-string v2, "InterruptedException"

    .line 1339
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_2
    return-void
.end method

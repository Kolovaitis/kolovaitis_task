.class Lepson/maintain/activity/MaintainActivity$2;
.super Ljava/lang/Object;
.source "MaintainActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/activity/MaintainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 487
    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    .line 491
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 511
    :pswitch_1
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, [I

    invoke-static {v0, p1}, Lepson/maintain/activity/MaintainActivity;->access$1700(Lepson/maintain/activity/MaintainActivity;[I)V

    goto/16 :goto_0

    .line 507
    :pswitch_2
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "GET_BATTERY_RESULT"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v3, "GET_BATTERY_INFO"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;

    invoke-static {v0, v1, p1}, Lepson/maintain/activity/MaintainActivity;->access$1600(Lepson/maintain/activity/MaintainActivity;ILcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)V

    goto/16 :goto_0

    .line 537
    :pswitch_3
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$2300(Lepson/maintain/activity/MaintainActivity;)V

    goto/16 :goto_0

    .line 595
    :pswitch_4
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$2800(Lepson/maintain/activity/MaintainActivity;)V

    goto/16 :goto_0

    .line 545
    :pswitch_5
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainActivity;->access$2500(Lepson/maintain/activity/MaintainActivity;I)V

    .line 546
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$1300(Lepson/maintain/activity/MaintainActivity;)V

    goto/16 :goto_0

    .line 589
    :pswitch_6
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object p1

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 590
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$2300(Lepson/maintain/activity/MaintainActivity;)V

    .line 591
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$2700(Lepson/maintain/activity/MaintainActivity;)V

    goto/16 :goto_0

    .line 550
    :pswitch_7
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$2600(Lepson/maintain/activity/MaintainActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$700(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 553
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$800(Lepson/maintain/activity/MaintainActivity;)I

    move-result p1

    const v0, 0x7f080198

    packed-switch p1, :pswitch_data_1

    .line 564
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$700(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 565
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 559
    :pswitch_8
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 560
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const v0, 0x7f07010f

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 555
    :pswitch_9
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 556
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const v0, 0x7f070111

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 567
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 568
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const v0, 0x7f070110

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 541
    :pswitch_a
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v3, "PROBE_ERROR"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {v0, p1, v1}, Lepson/maintain/activity/MaintainActivity;->access$2400(Lepson/maintain/activity/MaintainActivity;IZ)V

    goto :goto_0

    .line 527
    :pswitch_b
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$2000(Lepson/maintain/activity/MaintainActivity;)V

    .line 530
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1, v2}, Lepson/maintain/activity/MaintainActivity;->access$2100(Lepson/maintain/activity/MaintainActivity;Z)V

    .line 533
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1, v1}, Lepson/maintain/activity/MaintainActivity;->access$2200(Lepson/maintain/activity/MaintainActivity;I)V

    goto :goto_0

    .line 523
    :pswitch_c
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$1900(Lepson/maintain/activity/MaintainActivity;)V

    goto :goto_0

    .line 515
    :pswitch_d
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$1300(Lepson/maintain/activity/MaintainActivity;)V

    goto :goto_0

    .line 503
    :pswitch_e
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "GET_INK_RESULT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Lepson/maintain/activity/MaintainActivity;->updateInkInfo(I)V

    goto :goto_0

    .line 499
    :pswitch_f
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "GET_STT_RESULT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {v0, p1}, Lepson/maintain/activity/MaintainActivity;->access$1500(Lepson/maintain/activity/MaintainActivity;I)V

    goto :goto_0

    :pswitch_10
    const-string p1, "Maintain"

    const-string v0, "********Maintain finish********"

    .line 493
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$1300(Lepson/maintain/activity/MaintainActivity;)V

    .line 495
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$1400(Lepson/maintain/activity/MaintainActivity;)V

    goto :goto_0

    .line 519
    :pswitch_11
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$2;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$1800(Lepson/maintain/activity/MaintainActivity;)V

    :goto_0
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

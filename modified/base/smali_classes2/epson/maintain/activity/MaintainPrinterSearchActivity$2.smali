.class Lepson/maintain/activity/MaintainPrinterSearchActivity$2;
.super Ljava/lang/Object;
.source "MaintainPrinterSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainPrinterSearchActivity;->setNotFoundPrinterButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 289
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$2;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 293
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$2;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/maintain/activity/PrinterNotFoundDialogCreator;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.class public Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;
.super Landroid/arch/lifecycle/AndroidViewModel;
.source "MaintainBuyInkOnlineRegTask.java"


# static fields
.field public static final URL_BUY_INK:I = 0x1

.field public static final URL_NOZZLE_CHECK:I = 0x2


# instance fields
.field private mState:Z

.field private mUriEvent:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData<",
            "Lepson/common/EventWrapper<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Landroid/arch/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    const/4 p1, 0x1

    .line 21
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mState:Z

    return-void
.end method


# virtual methods
.method public getReady()Z
    .locals 1

    .line 32
    iget-boolean v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mState:Z

    return v0
.end method

.method public getUriEvent()Landroid/arch/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/MutableLiveData<",
            "Lepson/common/EventWrapper<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mUriEvent:Landroid/arch/lifecycle/MutableLiveData;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mUriEvent:Landroid/arch/lifecycle/MutableLiveData;

    .line 47
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mUriEvent:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method public loadData(ILjava/lang/String;)V
    .locals 2

    .line 54
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->getApplication()Landroid/app/Application;

    move-result-object v0

    .line 55
    new-instance v1, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;

    invoke-direct {v1, p0, v0, p2, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;-><init>(Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;Landroid/content/Context;Ljava/lang/String;I)V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Void;

    .line 83
    invoke-virtual {v1, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public loadData(Ljava/lang/String;)V
    .locals 2

    .line 88
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->getApplication()Landroid/app/Application;

    move-result-object v0

    .line 89
    new-instance v1, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;

    invoke-direct {v1, p0, v0, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;-><init>(Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;Landroid/content/Context;Ljava/lang/String;)V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Void;

    .line 112
    invoke-virtual {v1, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public setReady(Z)V
    .locals 0

    .line 28
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mState:Z

    return-void
.end method

.method public setUriEvent(Landroid/net/Uri;)V
    .locals 2

    .line 36
    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mUriEvent:Landroid/arch/lifecycle/MutableLiveData;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mUriEvent:Landroid/arch/lifecycle/MutableLiveData;

    .line 40
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->mUriEvent:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v1, Lepson/common/EventWrapper;

    invoke-direct {v1, p1}, Lepson/common/EventWrapper;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

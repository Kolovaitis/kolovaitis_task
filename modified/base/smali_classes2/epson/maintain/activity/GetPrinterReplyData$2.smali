.class Lepson/maintain/activity/GetPrinterReplyData$2;
.super Landroid/os/Handler;
.source "GetPrinterReplyData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/activity/GetPrinterReplyData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/GetPrinterReplyData;


# direct methods
.method constructor <init>(Lepson/maintain/activity/GetPrinterReplyData;)V
    .locals 0

    .line 250
    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .line 252
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x63

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 285
    :pswitch_0
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lepson/maintain/activity/GetPrinterReplyData;->access$500(Lepson/maintain/activity/GetPrinterReplyData;I)V

    .line 286
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->interrupMaintainThread()V

    goto/16 :goto_0

    .line 300
    :pswitch_1
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->cancelLoadConPro()V

    .line 302
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->endLoadConThread()V

    .line 305
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1, v1}, Lepson/maintain/activity/GetPrinterReplyData;->setResult(I)V

    .line 306
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->finish()V

    goto/16 :goto_0

    .line 294
    :pswitch_2
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1, v1}, Lepson/maintain/activity/GetPrinterReplyData;->setResult(I)V

    .line 295
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->finish()V

    goto/16 :goto_0

    .line 282
    :pswitch_3
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->showLoadConPro()V

    goto/16 :goto_0

    .line 278
    :pswitch_4
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->updateSelectedPrinter()V

    goto :goto_0

    .line 269
    :pswitch_5
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->cancelLoadConPro()V

    .line 270
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->interrupMaintainThread()V

    goto :goto_0

    .line 265
    :pswitch_6
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "GET_INK_RESULT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Lepson/maintain/activity/GetPrinterReplyData;->updateInkInfo(I)V

    goto :goto_0

    .line 260
    :pswitch_7
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "GET_STT_RESULT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Lepson/maintain/activity/GetPrinterReplyData;->updateStatus(I)V

    goto :goto_0

    :pswitch_8
    const-string p1, "Maintain"

    const-string v0, "********Maintain finish********"

    .line 254
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->interrupMaintainThread()V

    goto :goto_0

    .line 275
    :pswitch_9
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->showProDia()V

    goto :goto_0

    .line 309
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {v0}, Lepson/maintain/activity/GetPrinterReplyData;->endBackGroundThread()V

    .line 310
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {v0}, Lepson/maintain/activity/GetPrinterReplyData;->cancelLoadConPro()V

    .line 311
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {v0}, Lepson/maintain/activity/GetPrinterReplyData;->endLoadConThread()V

    .line 312
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "extStatus"

    .line 313
    iget-object v2, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v2}, Lepson/maintain/activity/GetPrinterReplyData;->access$600(Lepson/maintain/activity/GetPrinterReplyData;)Lcom/epson/iprint/shared/SharedParamStatus;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 314
    iget-object v1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "END_RELPY_RESULT"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v1, p1, v0}, Lepson/maintain/activity/GetPrinterReplyData;->setResult(ILandroid/content/Intent;)V

    .line 316
    iget-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$2;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-virtual {p1}, Lepson/maintain/activity/GetPrinterReplyData;->finish()V

    :goto_0
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_a
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class Lepson/maintain/activity/MaintainActivity$1;
.super Ljava/lang/Object;
.source "MaintainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainActivity;->startLoadConThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 347
    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private hideLoadProgress()V
    .locals 2

    .line 480
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method


# virtual methods
.method public declared-synchronized run()V
    .locals 8

    monitor-enter p0

    :try_start_0
    const-string v0, "MAINTAIN"

    const-string v1, "new load Config thread"

    .line 350
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v0, 0x1f4

    .line 354
    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 360
    :try_start_2
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$100(Lepson/maintain/activity/MaintainActivity;)V

    .line 361
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 364
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$200(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity$1;->hideLoadProgress()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 366
    monitor-exit p0

    return-void

    .line 370
    :cond_0
    :try_start_3
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v2}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v0, v2}, Lepson/maintain/activity/MaintainActivity;->access$302(Lepson/maintain/activity/MaintainActivity;Z)Z

    .line 373
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$300(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_6

    .line 375
    :try_start_4
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    const/4 v1, -0x2

    invoke-static {v0, v1}, Lepson/maintain/activity/MaintainActivity;->access$402(Lepson/maintain/activity/MaintainActivity;I)I

    const/4 v0, 0x0

    .line 379
    :cond_1
    iget-object v5, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v5}, Lepson/maintain/activity/MaintainActivity;->access$500(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonService;

    move-result-object v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v5}, Lepson/maintain/activity/MaintainActivity;->access$200(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v5

    if-nez v5, :cond_2

    const-wide/16 v5, 0x64

    .line 380
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    add-int/2addr v0, v4

    const/16 v5, 0xa

    if-lt v0, v5, :cond_1

    .line 389
    :cond_2
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$500(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonService;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 390
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$500(Lepson/maintain/activity/MaintainActivity;)Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-interface {v0, v4}, Lepson/print/service/IEpsonService;->ensureLogin(Z)I

    move-result v0

    if-nez v0, :cond_3

    .line 392
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0, v3}, Lepson/maintain/activity/MaintainActivity;->access$402(Lepson/maintain/activity/MaintainActivity;I)I

    goto :goto_0

    :cond_3
    const/16 v3, -0x44c

    if-ne v0, v3, :cond_4

    .line 394
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0, v2}, Lepson/maintain/activity/MaintainActivity;->access$402(Lepson/maintain/activity/MaintainActivity;I)I

    goto :goto_0

    .line 396
    :cond_4
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0, v1}, Lepson/maintain/activity/MaintainActivity;->access$402(Lepson/maintain/activity/MaintainActivity;I)I
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 404
    :try_start_5
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 401
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 406
    :cond_5
    :goto_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 409
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity$1;->hideLoadProgress()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 410
    monitor-exit p0

    return-void

    .line 414
    :cond_6
    :try_start_6
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$200(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 415
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity$1;->hideLoadProgress()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 416
    monitor-exit p0

    return-void

    .line 419
    :cond_7
    :try_start_7
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$600(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$700(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 421
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$800(Lepson/maintain/activity/MaintainActivity;)I

    move-result v0

    if-ne v0, v4, :cond_9

    .line 423
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    const-string v5, "printer"

    invoke-static {v0, v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 425
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0, v3}, Lepson/maintain/activity/MaintainActivity;->access$902(Lepson/maintain/activity/MaintainActivity;Z)Z

    goto :goto_1

    .line 426
    :cond_8
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$900(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 428
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0, v4}, Lepson/maintain/activity/MaintainActivity;->access$902(Lepson/maintain/activity/MaintainActivity;Z)Z

    .line 429
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    const-string v5, "printer"

    invoke-static {v0, v5, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v0, :cond_9

    .line 430
    monitor-exit p0

    return-void

    .line 435
    :cond_9
    :goto_1
    :try_start_8
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, "startLoadConThread"

    const-string v1, "mPrinter.doProbePrinter"

    .line 436
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$600(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v5}, Lepson/maintain/activity/MaintainActivity;->access$1000(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v6}, Lepson/maintain/activity/MaintainActivity;->access$800(Lepson/maintain/activity/MaintainActivity;)I

    move-result v6

    const/16 v7, 0x3c

    invoke-virtual {v0, v7, v1, v5, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 440
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1, v2}, Lepson/maintain/activity/MaintainActivity;->access$1202(Lepson/maintain/activity/MaintainActivity;I)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 442
    :cond_a
    :goto_2
    :try_start_9
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$200(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v1

    if-nez v1, :cond_c

    if-nez v0, :cond_b

    goto :goto_3

    .line 446
    :cond_b
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x7

    .line 447
    iput v2, v1, Landroid/os/Message;->what:I

    .line 448
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v5, "PROBE_ERROR"

    .line 449
    invoke-virtual {v2, v5, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 450
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 451
    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v2}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-wide/16 v1, 0xfa0

    .line 452
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 453
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$200(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 455
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    iget-object v2, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v2}, Lepson/maintain/activity/MaintainActivity;->access$600(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v5}, Lepson/maintain/activity/MaintainActivity;->access$1000(Lepson/maintain/activity/MaintainActivity;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v6}, Lepson/maintain/activity/MaintainActivity;->access$800(Lepson/maintain/activity/MaintainActivity;)I

    move-result v6

    invoke-virtual {v1, v7, v2, v5, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v0
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_2

    :catch_2
    move-exception v1

    .line 459
    :try_start_a
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_c
    :goto_3
    const-string v1, "MAINTAIN"

    .line 462
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Probe Printer result: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    iget-object v1, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v1}, Lepson/maintain/activity/MaintainActivity;->access$200(Lepson/maintain/activity/MaintainActivity;)Z

    move-result v1

    if-nez v1, :cond_e

    if-nez v0, :cond_e

    .line 464
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setMSearchPos(I)V

    .line 465
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0, v3}, Lepson/maintain/activity/MaintainActivity;->access$1202(Lepson/maintain/activity/MaintainActivity;I)I

    .line 466
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    iput-boolean v4, v0, Lepson/maintain/activity/MaintainActivity;->mHavePrinter:Z

    .line 467
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_4

    .line 470
    :cond_d
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    iput-boolean v3, v0, Lepson/maintain/activity/MaintainActivity;->mHavePrinter:Z

    .line 471
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$1;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$000(Lepson/maintain/activity/MaintainActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, "MAINTAIN"

    const-string v1, "not select printer"

    .line 472
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 474
    :cond_e
    :goto_4
    monitor-exit p0

    return-void

    :catch_3
    move-exception v0

    .line 356
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 357
    invoke-direct {p0}, Lepson/maintain/activity/MaintainActivity$1;->hideLoadProgress()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 358
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

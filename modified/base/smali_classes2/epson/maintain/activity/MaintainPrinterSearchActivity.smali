.class public Lepson/maintain/activity/MaintainPrinterSearchActivity;
.super Lepson/print/ActivityIACommon;
.source "MaintainPrinterSearchActivity.java"

# interfaces
.implements Lepson/print/CustomTitleDialogFragment$Callback;


# static fields
.field private static final DELAY:I = 0x64

.field private static final DIALOG_ID_PRINTER_NOT_FOUND_WITH_WEB_GUIDANCE:I = 0x1

.field private static final DIALOG_TAG_PRINTER_NOT_FOUND:Ljava/lang/String; = "printer_not_found_dialog"

.field private static final EPS_PROTOCOL_ALL:I = 0xd0

.field private static final EPS_PROTOCOL_LPR:I = 0x40

.field private static final EPS_PROTOCOL_NET:I = 0xc0

.field private static final EPS_PROTOCOL_RAW:I = 0x80

.field private static final EPS_PROTOCOL_USB:I = 0x10

.field private static final Menu_Delete:I = 0x1

.field private static final Menu_Edit:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MaintainPrinterSearchActivity"

.field private static final mLock:Ljava/lang/Object;

.field private static mScanner:Lepson/scan/lib/escanLib;


# instance fields
.field private final BLE_SET_PRINTER:I

.field private final CANCEL_FIND_PRINTER:I

.field private final CONNECT_SIMPLEAP:I

.field private final DELETE_PRINTER:I

.field private final EDIT_IPPRINTER:I

.field private final EDIT_PRINTER:I

.field private final FINISH_FIND_PRINTER:I

.field private final FOUND_SIMPLEAP:I

.field private final PRINTER_COMMON_DEVICENAME:Ljava/lang/String;

.field private final PRINTER_ID:Ljava/lang/String;

.field private final PRINTER_INDEX:Ljava/lang/String;

.field private final PRINTER_IP:Ljava/lang/String;

.field private final PRINTER_NAME:Ljava/lang/String;

.field private final PRINTER_SERIAL_NO:Ljava/lang/String;

.field private final PROBE_PRINTER:I

.field private final PROBE_SCANNER:I

.field private final REQUEST_CODE_LOCATION_PERMISSION:I

.field private final SEARCH_PRINTER:I

.field private final SEARCH_PRINTER_P2P:I

.field private final SELECT_PRINTER:I

.field private bCheckWiFiStatus:Z

.field private bRejectLocationPermission:Z

.field helper:Lepson/print/widgets/ListControlHelper;

.field private volatile isFinishSearchPrinter:Z

.field private isFocused:Ljava/lang/Boolean;

.field private isJapaneseLocale:Z

.field private volatile isSearchSimpleAp:Z

.field private listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

.field mAboutRemoteButton:Landroid/view/View;

.field private mActivityForegroundLifetime:Z

.field mAddButton:Landroid/widget/Button;

.field private mBleButton:Landroid/widget/RelativeLayout;

.field private mBleFind:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

.field mBuilder:Lepson/print/widgets/AbstractListBuilder;

.field private mContext:Landroid/content/Context;

.field private mDeletePos:I

.field mHandler:Landroid/os/Handler;

.field mIpButton:Landroid/widget/RadioButton;

.field mIsClickSelect:Z

.field mLayout:Landroid/view/ViewGroup;

.field private mListEmptyMessageTextView:Landroid/widget/TextView;

.field private mListView:Landroid/widget/ListView;

.field mLocalButton:Landroid/widget/RadioButton;

.field private mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field mProgressBar:Landroid/widget/ProgressBar;

.field mRemoteButton:Landroid/widget/RadioButton;

.field mSearchButton:Landroid/widget/Button;

.field private mSearchThread:Ljava/lang/Thread;

.field mTextDetail:Landroid/widget/TextView;

.field mWiFiSettingButton:Landroid/view/View;

.field private printerEmailAddress:Ljava/lang/String;

.field private printerId:Ljava/lang/String;

.field private printerKey:Ljava/lang/String;

.field private volatile printer_location:I

.field probePrinterThread:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field progress:Lepson/print/screen/WorkingDialog;

.field private searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

.field wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 131
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLock:Ljava/lang/Object;

    .line 157
    new-instance v0, Lepson/scan/lib/escanLib;

    invoke-direct {v0}, Lepson/scan/lib/escanLib;-><init>()V

    sput-object v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mScanner:Lepson/scan/lib/escanLib;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 84
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 103
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/4 v0, 0x0

    .line 127
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIsClickSelect:Z

    const/4 v1, 0x1

    .line 128
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFocused:Ljava/lang/Boolean;

    .line 129
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFinishSearchPrinter:Z

    const/4 v2, 0x0

    .line 133
    iput-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mContext:Landroid/content/Context;

    const/4 v3, -0x1

    .line 134
    iput v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mDeletePos:I

    const-string v3, ""

    .line 135
    iput-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerId:Ljava/lang/String;

    const-string v3, ""

    .line 136
    iput-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerEmailAddress:Ljava/lang/String;

    const-string v3, ""

    .line 137
    iput-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerKey:Ljava/lang/String;

    .line 139
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    .line 141
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isJapaneseLocale:Z

    .line 143
    iput-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 144
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->bCheckWiFiStatus:Z

    .line 145
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isSearchSimpleAp:Z

    .line 148
    iput-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    .line 149
    iput-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->helper:Lepson/print/widgets/ListControlHelper;

    .line 151
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->bRejectLocationPermission:Z

    .line 154
    iput-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->probePrinterThread:Landroid/os/AsyncTask;

    .line 162
    iput-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleFind:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    const-string v0, "index"

    .line 842
    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->PRINTER_INDEX:Ljava/lang/String;

    const-string v0, "name"

    .line 843
    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->PRINTER_NAME:Ljava/lang/String;

    const-string v0, "ip"

    .line 844
    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->PRINTER_IP:Ljava/lang/String;

    const-string v0, "id"

    .line 845
    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->PRINTER_ID:Ljava/lang/String;

    const-string v0, "serial_no"

    .line 846
    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->PRINTER_SERIAL_NO:Ljava/lang/String;

    const-string v0, "common_devicename"

    .line 847
    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->PRINTER_COMMON_DEVICENAME:Ljava/lang/String;

    .line 848
    iput v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->SEARCH_PRINTER:I

    const/4 v0, 0x2

    .line 849
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->CANCEL_FIND_PRINTER:I

    const/4 v0, 0x4

    .line 851
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->SELECT_PRINTER:I

    const/4 v0, 0x5

    .line 852
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->FINISH_FIND_PRINTER:I

    const/4 v0, 0x6

    .line 853
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->DELETE_PRINTER:I

    const/4 v0, 0x7

    .line 854
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->EDIT_PRINTER:I

    const/16 v0, 0x8

    .line 855
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->FOUND_SIMPLEAP:I

    const/16 v0, 0x9

    .line 856
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->CONNECT_SIMPLEAP:I

    const/16 v0, 0xa

    .line 857
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->EDIT_IPPRINTER:I

    const/16 v0, 0xb

    .line 858
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->PROBE_PRINTER:I

    const/16 v0, 0xc

    .line 859
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->PROBE_SCANNER:I

    const/16 v0, 0xd

    .line 860
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->SEARCH_PRINTER_P2P:I

    const/16 v0, 0xe

    .line 861
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->REQUEST_CODE_LOCATION_PERMISSION:I

    const/16 v0, 0xf

    .line 862
    iput v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->BLE_SET_PRINTER:I

    .line 866
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->interruptSearch()V

    return-void
.end method

.method static synthetic access$100(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    return-object p0
.end method

.method static synthetic access$1002(Lepson/maintain/activity/MaintainPrinterSearchActivity;Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
    .locals 0

    .line 84
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    return-object p1
.end method

.method static synthetic access$1100(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isSearchSimpleAp:Z

    return p0
.end method

.method static synthetic access$1102(Lepson/maintain/activity/MaintainPrinterSearchActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isSearchSimpleAp:Z

    return p1
.end method

.method static synthetic access$1200(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFinishSearchPrinter:Z

    return p0
.end method

.method static synthetic access$1202(Lepson/maintain/activity/MaintainPrinterSearchActivity;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFinishSearchPrinter:Z

    return p1
.end method

.method static synthetic access$1300()Lepson/scan/lib/escanLib;
    .locals 1

    .line 84
    sget-object v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mScanner:Lepson/scan/lib/escanLib;

    return-object v0
.end method

.method static synthetic access$1400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-object p0
.end method

.method static synthetic access$1500()Ljava/lang/Object;
    .locals 1

    .line 84
    sget-object v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->startWifiPrinterSelect()V

    return-void
.end method

.method static synthetic access$300(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Ljava/lang/Boolean;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFocused:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$302(Lepson/maintain/activity/MaintainPrinterSearchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 84
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFocused:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I
    .locals 0

    .line 84
    iget p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    return p0
.end method

.method static synthetic access$402(Lepson/maintain/activity/MaintainPrinterSearchActivity;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    return p1
.end method

.method static synthetic access$500(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I
    .locals 0

    .line 84
    iget p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mDeletePos:I

    return p0
.end method

.method static synthetic access$502(Lepson/maintain/activity/MaintainPrinterSearchActivity;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mDeletePos:I

    return p1
.end method

.method static synthetic access$600(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Landroid/widget/ListView;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListView:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$700(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Landroid/content/Context;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$800(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->buildElements()V

    return-void
.end method

.method static synthetic access$900(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->search()V

    return-void
.end method

.method private buildElements()V
    .locals 9

    .line 348
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f08015c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    .line 349
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080298

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 350
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080121

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAddButton:Landroid/widget/Button;

    .line 351
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080105

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mTextDetail:Landroid/widget/TextView;

    const/4 v0, 0x0

    .line 355
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchButtonSetEnabled(Z)V

    .line 357
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 358
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAddButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 359
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mTextDetail:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 362
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 364
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f080098

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLocalButton:Landroid/widget/RadioButton;

    .line 365
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f080097

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    .line 366
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f08009e

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mRemoteButton:Landroid/widget/RadioButton;

    .line 368
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLocalButton:Landroid/widget/RadioButton;

    const v3, 0x7f07011c

    invoke-static {p0, v1, v3}, Lepson/common/Utils;->setDrawble2TextView(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 369
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    const v3, 0x7f07011b

    invoke-static {p0, v1, v3}, Lepson/common/Utils;->setDrawble2TextView(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 370
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mRemoteButton:Landroid/widget/RadioButton;

    const v3, 0x7f07011d

    invoke-static {p0, v1, v3}, Lepson/common/Utils;->setDrawble2TextView(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 376
    iget v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    const v3, 0x7f0e0437

    const v4, 0x7f08022b

    const/4 v5, 0x1

    const/4 v6, 0x0

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 412
    :pswitch_0
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAddButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 414
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 415
    iget-object v6, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    .line 417
    new-instance v1, Lepson/print/widgets/PrinterInfoIpBuilder;

    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v7, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v1, v3, v7, v0}, Lepson/print/widgets/PrinterInfoIpBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 420
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    iget-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerKey:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    .line 423
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 425
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mTextDetail:Landroid/widget/TextView;

    const v3, 0x7f0e0487

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 428
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 431
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 434
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 437
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 438
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 380
    :pswitch_1
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAddButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 383
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 384
    iget-object v6, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    .line 385
    new-instance v1, Lepson/print/widgets/PrinterInfoECBuilder;

    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v7, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v1, v3, v7}, Lepson/print/widgets/PrinterInfoECBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 388
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    iget-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    .line 391
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mRemoteButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 393
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mTextDetail:Landroid/widget/TextView;

    const v3, 0x7f0e048b

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 396
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 399
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 402
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 405
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 406
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 446
    :pswitch_2
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 447
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 448
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450
    new-instance v1, Lepson/print/widgets/PrinterInfoBuilder;

    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v7, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v1, v3, v7, v0}, Lepson/print/widgets/PrinterInfoBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    const-string v1, "printer"

    .line 453
    invoke-static {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 454
    iget-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    iget-object v7, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerId:Ljava/lang/String;

    invoke-static {p0, v7, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->getCurPrinterString(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    .line 457
    new-instance v3, Lepson/print/widgets/ListControlHelper;

    iget-object v7, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    check-cast v7, Lepson/print/widgets/PrinterInfoBuilder;

    invoke-direct {v3, v7}, Lepson/print/widgets/ListControlHelper;-><init>(Lepson/print/widgets/PrinterInfoBuilder;)V

    iput-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->helper:Lepson/print/widgets/ListControlHelper;

    .line 458
    new-instance v3, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    iget-object v7, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v7}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v7

    iget-object v8, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->helper:Lepson/print/widgets/ListControlHelper;

    invoke-direct {v3, p0, v7, v8, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;-><init>(Landroid/content/Context;Ljava/util/AbstractList;Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;Ljava/lang/String;)V

    iput-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    .line 461
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLocalButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 463
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mTextDetail:Landroid/widget/TextView;

    const v3, 0x7f0e0489

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 466
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 469
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 470
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 475
    :cond_0
    :goto_0
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v1}, Lepson/print/widgets/AbstractListBuilder;->build()V

    .line 476
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v1}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    .line 477
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 480
    iget v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 481
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAboutRemoteButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 483
    :cond_1
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAboutRemoteButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 486
    :goto_1
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 533
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$6;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$6;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 549
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAddButton:Landroid/widget/Button;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$7;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 636
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$8;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$8;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 645
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLocalButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$9;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$9;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 667
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mRemoteButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$10;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$10;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 689
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIpButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private callBackFuncs()V
    .locals 1

    .line 1830
    new-instance v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$16;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$16;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleFind:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-void
.end method

.method private dismissPrinterNotFoundDialog()V
    .locals 2

    .line 1722
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "printer_not_found_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 1724
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private init()V
    .locals 2

    .line 221
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    iput v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    .line 223
    iget v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 224
    iput v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    .line 228
    :cond_0
    iget v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    packed-switch v1, :pswitch_data_0

    .line 236
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerId:Ljava/lang/String;

    goto :goto_0

    .line 233
    :pswitch_0
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerKey:Ljava/lang/String;

    goto :goto_0

    .line 230
    :pswitch_1
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printerEmailAddress:Ljava/lang/String;

    .line 240
    :goto_0
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->buildElements()V

    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private interruptSearch()V
    .locals 6

    const-string v0, "MaintainPrinterSearchActivity"

    const-string v1, "interruptSearch()"

    .line 1594
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1598
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1599
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1601
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1602
    sget-object v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1605
    :try_start_0
    iget-object v4, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz v4, :cond_0

    .line 1606
    iget-object v4, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-virtual {v4}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    const/4 v4, 0x0

    .line 1607
    iput-object v4, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 1609
    :cond_0
    iput-boolean v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isSearchSimpleAp:Z

    .line 1611
    iget-boolean v4, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFinishSearchPrinter:Z

    if-ne v4, v1, :cond_1

    .line 1612
    monitor-exit v0

    return-void

    .line 1614
    :cond_1
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFinishSearchPrinter:Z

    .line 1615
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1618
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 1619
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchThread:Ljava/lang/Thread;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1620
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :try_start_1
    const-string v0, "MaintainPrinterSearchActivity"

    const-string v1, "mSearchThread.join() enter"

    .line 1630
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1631
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchThread:Ljava/lang/Thread;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v4, v5}, Ljava/lang/Thread;->join(J)V

    .line 1632
    :goto_0
    sget-object v0, Ljava/lang/Thread$State;->RUNNABLE:Ljava/lang/Thread$State;

    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    add-int/lit8 v0, v2, 0x1

    const/16 v1, 0x3c

    if-le v2, v1, :cond_2

    const-string v0, "MaintainPrinterSearchActivity"

    const-string v1, "mSearchThread.join() timeout"

    .line 1634
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v1, "MaintainPrinterSearchActivity"

    const-string v2, "retry doCancelFindPrinter()"

    .line 1637
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1638
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 1639
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchThread:Ljava/lang/Thread;

    invoke-virtual {v1, v4, v5}, Ljava/lang/Thread;->join(J)V

    move v2, v0

    goto :goto_0

    :cond_3
    :goto_1
    const-string v0, "MaintainPrinterSearchActivity"

    const-string v1, "mSearchThread.join() leave"

    .line 1641
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 1644
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1649
    :cond_4
    :goto_2
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :catchall_0
    move-exception v1

    .line 1615
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private requestLocationPermission()V
    .locals 3

    .line 248
    iget-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->bRejectLocationPermission:Z

    if-nez v0, :cond_1

    const/16 v0, 0x14

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->canAccessWiFiInfo(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 250
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWork;->isStartBleProcess(Landroid/content/Context;Ljava/lang/Boolean;)Z

    move-result v0

    const/16 v1, 0xe

    if-eqz v0, :cond_0

    .line 251
    invoke-static {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestLocationPermission(Landroid/app/Activity;I)V

    goto :goto_0

    .line 252
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1c

    if-le v0, v2, :cond_1

    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->getInsetance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->isWiFiValidated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 254
    invoke-static {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestLocationPermissionForce(Landroid/app/Activity;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private search()V
    .locals 5

    .line 1516
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->interruptSearch()V

    .line 1519
    sget-object v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1520
    :try_start_0
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v1}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    .line 1521
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->clearPrinterInfoList()V

    .line 1522
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    .line 1525
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIsClickSelect:Z

    .line 1528
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFinishSearchPrinter:Z

    .line 1529
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchButtonSetEnabled(Z)V

    .line 1531
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1532
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080123

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0e045f

    .line 1533
    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 1536
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFocused:Ljava/lang/Boolean;

    .line 1538
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xd

    if-eqz v1, :cond_0

    .line 1541
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1542
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isSearchSimpleAp:Z

    goto :goto_0

    .line 1545
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isWifiEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1547
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1548
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isSearchSimpleAp:Z

    goto :goto_0

    .line 1549
    :cond_1
    iget-boolean v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->bCheckWiFiStatus:Z

    if-nez v1, :cond_2

    if-nez v1, :cond_2

    .line 1552
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->bCheckWiFiStatus:Z

    const/4 v0, -0x1

    .line 1553
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->enableWiFi(Landroid/app/Activity;I)V

    return-void

    .line 1565
    :cond_2
    :goto_0
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 1567
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setHanlder(Landroid/os/Handler;)V

    .line 1569
    new-instance v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$15;

    invoke-direct {v0, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$15;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchThread:Ljava/lang/Thread;

    .line 1586
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :catchall_0
    move-exception v1

    .line 1522
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private setBleWifiSetupButton()V
    .locals 2

    .line 265
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080071

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    .line 266
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$1;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$1;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setNotFoundPrinterButton()V
    .locals 2

    .line 289
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f08022b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$2;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$2;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private startWifiPrinterSelect()V
    .locals 2

    .line 323
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method addAboutRemoteButton()V
    .locals 2

    .line 332
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAboutRemoteButton:Landroid/view/View;

    .line 335
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mAboutRemoteButton:Landroid/view/View;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$4;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$4;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method addWiFiSetupButton()V
    .locals 2

    .line 303
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0802b4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    .line 306
    iget-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isJapaneseLocale:Z

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0e04c4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 309
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0e0524

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 313
    :goto_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mWiFiSettingButton:Landroid/view/View;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$3;

    invoke-direct {v1, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$3;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method commitEPPrinterInfo()V
    .locals 2

    .line 1804
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1806
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V

    .line 1807
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitRemotePrinterInfo()V

    return-void
.end method

.method public displaySearchResult()V
    .locals 6

    .line 1653
    iget-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIsClickSelect:Z

    const/4 v1, 0x1

    if-nez v0, :cond_4

    .line 1654
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    const/4 v2, 0x0

    const v3, 0x7f080123

    if-lez v0, :cond_2

    .line 1655
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const v4, 0x7f0e03fd

    if-nez v0, :cond_1

    .line 1656
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1660
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1661
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v5}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1657
    :cond_1
    :goto_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1658
    invoke-virtual {p0, v4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1665
    :cond_2
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0e04f7

    .line 1666
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1669
    iget v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    if-ne v0, v1, :cond_4

    .line 1670
    iget-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mActivityForegroundLifetime:Z

    if-eqz v0, :cond_4

    .line 1672
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v2, 0x1

    .line 1671
    :cond_3
    invoke-static {p0, v2, v1}, Lepson/maintain/activity/PrinterNotFoundDialogCreator;->getPrinterNotFoundDialog(Landroid/content/Context;ZI)Lepson/print/CustomLayoutDialogFragment;

    move-result-object v0

    .line 1674
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "printer_not_found_dialog"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1680
    :cond_4
    :goto_1
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1684
    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->searchButtonSetEnabled(Z)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    .line 1396
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    .line 1398
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->interruptSearch()V

    const/16 v0, 0xb

    const/4 v1, 0x1

    const/4 v2, -0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 1504
    :pswitch_1
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 1506
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->finish()V

    goto/16 :goto_0

    :pswitch_2
    if-eq p2, v2, :cond_1

    .line 1499
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->bRejectLocationPermission:Z

    goto/16 :goto_0

    :pswitch_3
    if-ne p2, v2, :cond_1

    .line 1435
    new-instance p1, Lepson/print/MyPrinter;

    const-string p2, "PRINTER_NAME"

    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string p2, "PRINTER_IP"

    .line 1436
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string p2, "PRINTER_ID"

    .line 1437
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string p2, "PRINTER_SERIAL_NO"

    .line 1438
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    const/4 v9, 0x3

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const-string p2, "SCAN_REFS_SCANNER_ID"

    .line 1443
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lepson/print/MyPrinter;->setScannerId(Ljava/lang/String;)V

    .line 1446
    iget-object p2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p2

    .line 1447
    iput v0, p2, Landroid/os/Message;->what:I

    .line 1448
    iput-object p1, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1449
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_4
    const/4 p1, 0x0

    if-ne p2, v2, :cond_0

    .line 1470
    new-instance p2, Lepson/print/MyPrinter;

    const-string v1, "name"

    .line 1471
    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ip"

    .line 1472
    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "id"

    .line 1473
    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "serial_no"

    .line 1474
    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p2, v1, v2, v3, v4}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1479
    iput v0, v1, Landroid/os/Message;->what:I

    .line 1480
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1481
    iget-object p2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-string p2, "easysetup"

    .line 1483
    invoke-virtual {p3, p2, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    goto :goto_0

    .line 1487
    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    iput-object p2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isFocused:Ljava/lang/Boolean;

    .line 1488
    iput-boolean p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIsClickSelect:Z

    goto :goto_0

    :pswitch_5
    if-ne p2, v2, :cond_1

    const-string p1, "my_printer"

    .line 1404
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    .line 1405
    invoke-virtual {p1, p0}, Lepson/print/MyPrinter;->setCurPrinter(Landroid/content/Context;)V

    .line 1406
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->commitEPPrinterInfo()V

    .line 1409
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->removeAreaInfo()V

    .line 1411
    invoke-virtual {p0, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->setResult(I)V

    .line 1412
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->finish()V

    :cond_1
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .line 777
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    .line 780
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 782
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackRemotePrinterInfo()V

    .line 783
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackIPPrinterInfo()V

    .line 786
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->finish()V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .line 722
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iput-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 723
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 726
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 751
    :pswitch_0
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 753
    iget v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xa

    .line 758
    iput v0, p1, Landroid/os/Message;->what:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x7

    .line 755
    iput v0, p1, Landroid/os/Message;->what:I

    .line 762
    :goto_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    iget v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mDeletePos:I

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 763
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 729
    :pswitch_3
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mContext:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 730
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0316

    .line 731
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 732
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070099

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    .line 733
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lepson/maintain/activity/MaintainPrinterSearchActivity$13;

    invoke-direct {v2, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$13;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0476

    .line 742
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lepson/maintain/activity/MaintainPrinterSearchActivity$12;

    invoke-direct {v2, p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity$12;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 747
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_1
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 170
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 171
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    .line 172
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 175
    :cond_0
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isJapaneseLocale:Z

    goto :goto_1

    .line 173
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->isJapaneseLocale:Z

    .line 178
    :goto_1
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v2, 0x7f0a00b8

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    .line 181
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->addWiFiSetupButton()V

    .line 184
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->addAboutRemoteButton()V

    .line 186
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f0801e1

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListEmptyMessageTextView:Landroid/widget/TextView;

    .line 187
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x102000a

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mListView:Landroid/widget/ListView;

    .line 189
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->init()V

    .line 192
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->setContentView(Landroid/view/View;)V

    .line 193
    iput-object p0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mContext:Landroid/content/Context;

    .line 195
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIsClickSelect:Z

    .line 197
    iget p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    if-eq p1, v1, :cond_2

    .line 199
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->displaySearchResult()V

    .line 200
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 201
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f080123

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    :cond_2
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->setNotFoundPrinterButton()V

    .line 206
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->setBleWifiSetupButton()V

    const p1, 0x7f0e0536

    .line 209
    invoke-virtual {p0, p1, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->setActionBar(IZ)V

    .line 212
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->requestLocationPermission()V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    .line 713
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    const p2, 0x7f0e049d

    .line 715
    invoke-interface {p1, p2}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    const/4 p3, 0x0

    const/4 v0, 0x1

    const v1, 0x7f0e0485

    .line 716
    invoke-interface {p1, p3, v0, p3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x2

    .line 717
    invoke-interface {p1, p3, v0, p3, p2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    .line 1690
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0e00bd

    const v2, 0x7f0e04f2

    const v3, 0x7f0e00bf

    if-eq p1, v1, :cond_0

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 1713
    :pswitch_0
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e01a9

    .line 1714
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1713
    invoke-static {p0, p1, v0, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_1

    .line 1694
    :pswitch_1
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1695
    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1694
    invoke-static {p0, v0, p1, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_1

    .line 1700
    :cond_0
    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1701
    invoke-static {p0}, Lepson/common/Utils;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1702
    invoke-static {p0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 1703
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v1, v4

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const p1, 0x7f0e01ae

    .line 1705
    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1707
    :goto_0
    invoke-virtual {p0, v3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1708
    invoke-virtual {p0, v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1707
    invoke-static {p0, v0, p1, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    :goto_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0e01a8
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .line 771
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->destructor()V

    .line 772
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    return-void
.end method

.method public onLocalNegativeCallback(I)V
    .locals 0

    return-void
.end method

.method public onLocalPositiveCallback(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    .line 1818
    :cond_0
    invoke-static {p0}, Lepson/maintain/activity/PrinterNotFoundDialogCreator;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 817
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 818
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    const/4 v0, 0x0

    .line 820
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mActivityForegroundLifetime:Z

    .line 822
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->interruptSearch()V

    .line 824
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->blePrinterCheckStop()V

    .line 827
    invoke-virtual {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->removeAllDialog()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v0, 0x1

    .line 791
    iput-boolean v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mActivityForegroundLifetime:Z

    .line 792
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v1, "MaintainPrinterSearchActivity"

    const-string v2, "onResume()"

    .line 793
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->dismissPrinterNotFoundDialog()V

    .line 799
    iget v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    if-ne v1, v0, :cond_0

    .line 800
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const-string v1, "MaintainPrinterSearchActivity"

    const-string v2, "Send CHECK_PRINTER Message."

    .line 801
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    :cond_0
    iget v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->printer_location:I

    if-ne v1, v0, :cond_1

    .line 806
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWork;->isStartBleProcess(Landroid/content/Context;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "blePrinterCheck"

    .line 807
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 809
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->callBackFuncs()V

    .line 810
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBleFind:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->blePrinterCheck(Landroid/content/Context;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V

    :cond_1
    return-void
.end method

.method protected onStart()V
    .locals 0

    .line 833
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    .line 838
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 839
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    return-void
.end method

.method removeAllDialog()V
    .locals 1

    const v0, 0x7f0e01a8

    .line 1734
    :try_start_0
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const v0, 0x7f0e00bd

    .line 1739
    :try_start_1
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->removeDialog(I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const v0, 0x7f0e01a9

    .line 1744
    :try_start_2
    invoke-virtual {p0, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->removeDialog(I)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1750
    :catch_2
    :try_start_3
    invoke-direct {p0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->dismissPrinterNotFoundDialog()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method

.method public searchButtonSetEnabled(Z)V
    .locals 2

    .line 1789
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1792
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 1794
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

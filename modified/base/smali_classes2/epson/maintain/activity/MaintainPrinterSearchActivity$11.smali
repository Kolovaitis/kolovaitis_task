.class Lepson/maintain/activity/MaintainPrinterSearchActivity$11;
.super Ljava/lang/Object;
.source "MaintainPrinterSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainPrinterSearchActivity;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 689
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 692
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result p1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    return-void

    .line 695
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$402(Lepson/maintain/activity/MaintainPrinterSearchActivity;I)I

    .line 696
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 698
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mLayout:Landroid/view/ViewGroup;

    const v0, 0x7f080123

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 702
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    .line 703
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$800(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 704
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->displaySearchResult()V

    .line 707
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$11;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

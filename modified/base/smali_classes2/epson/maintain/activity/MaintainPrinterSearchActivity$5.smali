.class Lepson/maintain/activity/MaintainPrinterSearchActivity$5;
.super Ljava/lang/Object;
.source "MaintainPrinterSearchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainPrinterSearchActivity;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 486
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 489
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$300(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_3

    .line 490
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const/4 p4, 0x0

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-static {p1, p4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$302(Lepson/maintain/activity/MaintainPrinterSearchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 495
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iput-boolean p2, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mIsClickSelect:Z

    .line 498
    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    .line 499
    iget-object p4, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p4, p4, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p4}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p4

    invoke-virtual {p4, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p3

    iput-object p3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 501
    iget-object p3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result p3

    const/16 p4, 0xb

    if-eq p3, p2, :cond_1

    const/4 p2, 0x3

    if-eq p3, p2, :cond_0

    const/4 p2, 0x4

    .line 523
    iput p2, p1, Landroid/os/Message;->what:I

    goto :goto_0

    .line 503
    :cond_0
    iput p4, p1, Landroid/os/Message;->what:I

    goto :goto_0

    .line 508
    :cond_1
    iget-object p2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p2, Lepson/print/MyPrinter;

    .line 509
    invoke-virtual {p2}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p2

    .line 510
    sget-object p3, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    iget-object p5, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p5, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectType(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object p5

    invoke-virtual {p3, p5}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_2

    .line 512
    iget-object p3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 513
    iget-object p3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const/16 p4, 0x9

    invoke-static {p3, p2, p4}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->connect(Landroid/app/Activity;Ljava/lang/String;I)Z

    goto :goto_0

    .line 519
    :cond_2
    iput p4, p1, Landroid/os/Message;->what:I

    .line 527
    :goto_0
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->blePrinterCheckStop()V

    .line 528
    iget-object p2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$5;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p2, p2, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 p3, 0x64

    invoke-virtual {p2, p1, p3, p4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void

    :cond_3
    return-void
.end method

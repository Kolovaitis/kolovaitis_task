.class Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;
.super Landroid/os/AsyncTask;
.source "MaintainPrinterSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

.field final synthetic val$selectedPrinter:Lepson/print/MyPrinter;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainPrinterSearchActivity$14;Lepson/print/MyPrinter;)V
    .locals 0

    .line 1124
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iput-object p2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4

    .line 1145
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    .line 1147
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1145
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1151
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_0

    .line 1154
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setMSearchPos(I)V

    goto :goto_0

    .line 1158
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    const/16 v0, 0x3c

    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    .line 1159
    invoke-virtual {v1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    .line 1158
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-eqz p1, :cond_1

    .line 1162
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 1166
    :cond_1
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setMSearchPos(I)V

    .line 1170
    :goto_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result p1

    if-eqz p1, :cond_2

    .line 1172
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 1176
    :cond_2
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetLang()I

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/MyPrinter;->setLang(I)V

    .line 1179
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    .line 1182
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 1183
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v1, v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 1184
    invoke-virtual {v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    .line 1183
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetSupportedMedia(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 1186
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 1190
    :cond_3
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v1

    .line 1191
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v2

    .line 1193
    :try_start_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    const-string v1, "MaintainPrinterSearchActivity"

    const-string v2, "Success epsWrapperGetSupportedMedia"

    .line 1194
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 1196
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1201
    :goto_1
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 1202
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSavedAreaInfo()Ljava/io/File;

    move-result-object p1

    .line 1204
    :try_start_1
    invoke-static {v1, p1}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    const-string p1, "MaintainPrinterSearchActivity"

    const-string v1, "Success copy AreaInfo"

    .line 1205
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    .line 1207
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 1211
    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1124
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5

    .line 1219
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-nez v0, :cond_3

    .line 1221
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    if-eq p1, v2, :cond_2

    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    .line 1222
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    if-ne p1, v1, :cond_0

    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getScannerId()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 1230
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1231
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 1235
    :cond_1
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    const/4 v0, 0x4

    .line 1236
    iput v0, p1, Landroid/os/Message;->what:I

    .line 1237
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1238
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 1224
    :cond_2
    :goto_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    const/16 v0, 0xc

    .line 1225
    iput v0, p1, Landroid/os/Message;->what:I

    .line 1226
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->val$selectedPrinter:Lepson/print/MyPrinter;

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1227
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    .line 1243
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    .line 1247
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1248
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    :cond_4
    const/4 v0, 0x0

    if-nez p1, :cond_5

    .line 1252
    new-array p1, v1, [Ljava/lang/Integer;

    const v1, 0x7f0e023a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v0

    const v1, 0x7f0e023b

    .line 1253
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v2

    const/4 v1, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v1

    .line 1258
    :cond_5
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v1, v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v3, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v3, v3, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    aget-object v4, p1, v2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v4, v4, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    aget-object p1, p1, v0

    .line 1259
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v4, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const v4, 0x7f0e04f2

    invoke-virtual {v0, v4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1258
    invoke-static {v1, v3, p1, v0}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 1260
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 1263
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$302(Lepson/maintain/activity/MaintainPrinterSearchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1124
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .line 1128
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    if-nez v0, :cond_0

    .line 1129
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    new-instance v1, Lepson/print/screen/WorkingDialog;

    iget-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v2, v2, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-direct {v1, v2}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    .line 1131
    :cond_0
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1132
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->this$1:Lepson/maintain/activity/MaintainPrinterSearchActivity$14;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    :cond_1
    return-void
.end method

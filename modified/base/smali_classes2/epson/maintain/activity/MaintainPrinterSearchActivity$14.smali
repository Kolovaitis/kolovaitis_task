.class Lepson/maintain/activity/MaintainPrinterSearchActivity$14;
.super Ljava/lang/Object;
.source "MaintainPrinterSearchActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/activity/MaintainPrinterSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V
    .locals 0

    .line 866
    iput-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 7

    .line 870
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_4

    .line 888
    :pswitch_1
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3c

    invoke-static {p1, v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->searchWiFiDirectPrinter(Landroid/content/Context;Landroid/os/Handler;II)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1002(Lepson/maintain/activity/MaintainPrinterSearchActivity;Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 890
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    move-result-object p1

    if-nez p1, :cond_10

    .line 891
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1, v5}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1102(Lepson/maintain/activity/MaintainPrinterSearchActivity;Z)Z

    goto/16 :goto_4

    .line 1018
    :pswitch_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    .line 1021
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1200(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MaintainPrinterSearchActivity"

    const-string v1, "cancelSearch for PROBE_SCANNER"

    .line 1022
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 1037
    :cond_0
    new-instance v0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;

    invoke-direct {v0, p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity$14;Lepson/print/MyPrinter;)V

    new-array p1, v5, [Ljava/lang/Void;

    .line 1098
    invoke-virtual {v0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_4

    .line 1106
    :pswitch_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    .line 1109
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1200(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "MaintainPrinterSearchActivity"

    const-string v1, "cancelSearch for PROBE_PRINTER"

    .line 1110
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1113
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 1124
    :cond_1
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    new-instance v1, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;

    invoke-direct {v1, p0, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;-><init>(Lepson/maintain/activity/MaintainPrinterSearchActivity$14;Lepson/print/MyPrinter;)V

    new-array p1, v5, [Ljava/lang/Void;

    .line 1269
    invoke-virtual {v1, p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity$14$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object p1

    iput-object p1, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->probePrinterThread:Landroid/os/AsyncTask;

    goto/16 :goto_4

    .line 1304
    :pswitch_4
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 1307
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 1308
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v2, p1

    check-cast v2, Lepson/print/MyPrinter;

    .line 1312
    :cond_2
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const-class v1, Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1314
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz v2, :cond_3

    .line 1315
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v1, "PRINTER_KEY"

    .line 1316
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v1, "PRINTER_KEY"

    const-string v2, ""

    .line 1319
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    :goto_0
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v1, v3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$502(Lepson/maintain/activity/MaintainPrinterSearchActivity;I)I

    .line 1324
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1325
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const/16 v1, 0xa

    invoke-virtual {v0, p1, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_4

    .line 931
    :pswitch_5
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 932
    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ssid"

    .line 935
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "name"

    .line 936
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "addr_infra"

    .line 937
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez v1, :cond_4

    move-object v1, v0

    .line 946
    :cond_4
    new-instance v2, Lepson/print/MyPrinter;

    const-string v3, ""

    invoke-direct {v2, v1, v0, p1, v3}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v1, v1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    invoke-virtual {v1, v2, v5, v0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->addPrinter(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 951
    :cond_5
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1, v5}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1102(Lepson/maintain/activity/MaintainPrinterSearchActivity;Z)Z

    .line 952
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1200(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Z

    move-result p1

    if-eqz p1, :cond_10

    .line 953
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_4

    .line 1276
    :pswitch_6
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 1279
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_6

    .line 1280
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v2, p1

    check-cast v2, Lepson/print/MyPrinter;

    .line 1286
    :cond_6
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const-class v1, Lepson/print/screen/ActivityPrinterSetting;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1287
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz v2, :cond_7

    .line 1288
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    const-string v1, "PRINTER_EMAIL_ADDRESS"

    .line 1289
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const-string v1, "PRINTER_EMAIL_ADDRESS"

    const-string v2, ""

    .line 1292
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    :goto_1
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v1, v3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$502(Lepson/maintain/activity/MaintainPrinterSearchActivity;I)I

    .line 1297
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1298
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const/4 v1, 0x7

    invoke-virtual {v0, p1, v1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_4

    .line 1331
    :pswitch_7
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$500(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result p1

    if-ltz p1, :cond_10

    .line 1333
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result p1

    packed-switch p1, :pswitch_data_1

    goto/16 :goto_2

    .line 1365
    :pswitch_8
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$700(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1367
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 1368
    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    iget-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$500(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    .line 1367
    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->deleteIpPrinterInfo(Ljava/lang/String;)Z

    .line 1369
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object p1

    .line 1370
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    .line 1371
    iget-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$500(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 1372
    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_2

    .line 1344
    :pswitch_9
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$700(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1346
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 1347
    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    iget-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$500(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v0

    .line 1346
    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->deleteRemotePrinterInfo(Ljava/lang/String;)Z

    .line 1348
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object p1

    .line 1349
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    .line 1350
    iget-object v2, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v2}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$500(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 1351
    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 1377
    :goto_2
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->displaySearchResult()V

    .line 1381
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result p1

    if-eq p1, v4, :cond_8

    .line 1382
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1385
    :cond_8
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1, v3}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$502(Lepson/maintain/activity/MaintainPrinterSearchActivity;I)I

    goto/16 :goto_4

    .line 900
    :pswitch_a
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 901
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->displaySearchResult()V

    goto/16 :goto_4

    .line 961
    :pswitch_b
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->blePrinterCheckStop()V

    .line 964
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1200(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$1100(Lepson/maintain/activity/MaintainPrinterSearchActivity;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    const-string v0, "MaintainPrinterSearchActivity"

    const-string v1, "cancelSearch for SELECT_PRINTER"

    .line 965
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    .line 980
    :cond_a
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    .line 981
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1, v0}, Lepson/print/MyPrinter;->setCurPrinter(Landroid/content/Context;)V

    .line 984
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PrintSetting"

    const-string v2, "RE_SEARCH"

    invoke-static {v0, v1, v2, v4}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 986
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result v0

    if-ne v0, v4, :cond_c

    .line 988
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    .line 990
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 988
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 993
    iget-object v1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const-string v2, "printer"

    .line 995
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p1

    .line 993
    invoke-static {v1, v0, v2, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setConnectInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 998
    :cond_b
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    const-string v0, "printer"

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V

    .line 1002
    :cond_c
    :goto_3
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_d

    .line 1004
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->removeAreaInfo()V

    .line 1008
    :cond_d
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->commitEPPrinterInfo()V

    .line 1009
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1, v4}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->setResult(I)V

    .line 1010
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-virtual {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->finish()V

    goto/16 :goto_4

    .line 896
    :pswitch_c
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$000(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    goto/16 :goto_4

    .line 872
    :pswitch_d
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$400(Lepson/maintain/activity/MaintainPrinterSearchActivity;)I

    move-result p1

    if-eq p1, v4, :cond_e

    goto/16 :goto_4

    .line 878
    :cond_e
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object p1, p1, Lepson/maintain/activity/MaintainPrinterSearchActivity;->probePrinterThread:Landroid/os/AsyncTask;

    if-eqz p1, :cond_f

    .line 879
    sget-object p1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->probePrinterThread:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/AsyncTask$Status;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_f

    const-string p1, "MaintainPrinterSearchActivity"

    const-string v0, "Cancel serch. probePrinterThread is running."

    .line 880
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 884
    :cond_f
    iget-object p1, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainPrinterSearchActivity;->access$900(Lepson/maintain/activity/MaintainPrinterSearchActivity;)V

    goto :goto_4

    .line 907
    :pswitch_e
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "name"

    .line 908
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    const-string v0, "ip"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    const-string v0, "id"

    .line 909
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    const-string v0, "common_devicename"

    .line 912
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 913
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "commonDeviceName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 915
    new-instance v1, Lepson/print/MyPrinter;

    const-string v2, "name"

    .line 916
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ip"

    .line 917
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "id"

    .line 918
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "serial_no"

    .line 919
    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v3, v4, v6}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    invoke-virtual {v1, v0}, Lepson/print/MyPrinter;->setCommonDeviceName(Ljava/lang/String;)V

    .line 924
    iget-object v0, p0, Lepson/maintain/activity/MaintainPrinterSearchActivity$14;->this$0:Lepson/maintain/activity/MaintainPrinterSearchActivity;

    iget-object v0, v0, Lepson/maintain/activity/MaintainPrinterSearchActivity;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    const-string v2, "index"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 925
    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 924
    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->addPrinter(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)V

    :cond_10
    :goto_4
    return v5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_a
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

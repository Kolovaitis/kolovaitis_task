.class Lepson/maintain/activity/GetPrinterReplyData$3;
.super Ljava/lang/Object;
.source "GetPrinterReplyData.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/GetPrinterReplyData;->startBackgroundThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/GetPrinterReplyData;


# direct methods
.method constructor <init>(Lepson/maintain/activity/GetPrinterReplyData;)V
    .locals 0

    .line 404
    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$3;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 409
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetStatus()I

    move-result v0

    .line 410
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x2

    .line 411
    iput v2, v1, Landroid/os/Message;->what:I

    .line 412
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "GET_STT_RESULT"

    .line 413
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 414
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 415
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$3;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iget-object v0, v0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 418
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetInkInfo()I

    move-result v0

    .line 419
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x3

    .line 420
    iput v2, v1, Landroid/os/Message;->what:I

    .line 421
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "GET_INK_RESULT"

    .line 422
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 423
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 424
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$3;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iget-object v0, v0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.class public Lepson/maintain/activity/CheckEulaActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "CheckEulaActivity.java"


# static fields
.field private static final KEY_SCROLL_Y:Ljava/lang/String; = "scroll_y"


# instance fields
.field private mScrollView:Lcom/epson/mobilephone/common/license/RestoreScrollView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/maintain/activity/CheckEulaActivity;I)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lepson/maintain/activity/CheckEulaActivity;->finishWithResult(I)V

    return-void
.end method

.method private finishWithResult(I)V
    .locals 0

    .line 66
    invoke-virtual {p0, p1}, Lepson/maintain/activity/CheckEulaActivity;->setResult(I)V

    .line 67
    invoke-virtual {p0}, Lepson/maintain/activity/CheckEulaActivity;->finish()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 29
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a001f

    .line 30
    invoke-virtual {p0, v0}, Lepson/maintain/activity/CheckEulaActivity;->setContentView(I)V

    const v0, 0x7f080132

    .line 32
    invoke-virtual {p0, v0}, Lepson/maintain/activity/CheckEulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 33
    new-instance v1, Lepson/maintain/activity/CheckEulaActivity$1;

    invoke-direct {v1, p0}, Lepson/maintain/activity/CheckEulaActivity$1;-><init>(Lepson/maintain/activity/CheckEulaActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080134

    .line 40
    invoke-virtual {p0, v0}, Lepson/maintain/activity/CheckEulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 41
    new-instance v1, Lepson/maintain/activity/CheckEulaActivity$2;

    invoke-direct {v1, p0}, Lepson/maintain/activity/CheckEulaActivity$2;-><init>(Lepson/maintain/activity/CheckEulaActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080133

    .line 48
    invoke-virtual {p0, v0}, Lepson/maintain/activity/CheckEulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/epson/mobilephone/common/license/RestoreScrollView;

    iput-object v0, p0, Lepson/maintain/activity/CheckEulaActivity;->mScrollView:Lcom/epson/mobilephone/common/license/RestoreScrollView;

    const v0, 0x7f080135

    .line 50
    invoke-virtual {p0, v0}, Lepson/maintain/activity/CheckEulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 51
    new-instance v1, Lepson/common/IprintLicenseInfo;

    invoke-direct {v1}, Lepson/common/IprintLicenseInfo;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v1, p0, v2}, Lepson/common/IprintLicenseInfo;->getDocumentString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 52
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    .line 55
    iget-object v0, p0, Lepson/maintain/activity/CheckEulaActivity;->mScrollView:Lcom/epson/mobilephone/common/license/RestoreScrollView;

    const-string v1, "scroll_y"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/license/RestoreScrollView;->setScrollY(I)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 61
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "scroll_y"

    .line 62
    iget-object v1, p0, Lepson/maintain/activity/CheckEulaActivity;->mScrollView:Lcom/epson/mobilephone/common/license/RestoreScrollView;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/license/RestoreScrollView;->getLastY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

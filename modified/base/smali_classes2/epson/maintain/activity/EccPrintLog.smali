.class public Lepson/maintain/activity/EccPrintLog;
.super Lepson/print/ActivityIACommon;
.source "EccPrintLog.java"

# interfaces
.implements Lepson/common/DialogProgress$DialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;
    }
.end annotation


# static fields
.field private static final DIALOG_ERROR:Ljava/lang/String; = "dialog_error"

.field private static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field static final LOG_TAG:Ljava/lang/String; = "EccPrintLog"

.field static mRemotePrintHistoryTask:Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;


# instance fields
.field private mEccPrintLogViewModel:Lepson/maintain/activity/EccPrintLogViewModel;

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/maintain/activity/EccPrintLog;I)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lepson/maintain/activity/EccPrintLog;->show_message_and_finish(I)V

    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 221
    invoke-virtual {p0}, Lepson/maintain/activity/EccPrintLog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 223
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method public static synthetic lambda$onCreate$0(Lepson/maintain/activity/EccPrintLog;Ljava/util/Deque;)V
    .locals 2

    .line 64
    iget-object p1, p0, Lepson/maintain/activity/EccPrintLog;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 66
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 67
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 70
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    invoke-direct {p0, v0}, Lepson/maintain/activity/EccPrintLog;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 74
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 75
    invoke-direct {p0, v0}, Lepson/maintain/activity/EccPrintLog;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 11

    .line 199
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x14b98bc

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    const v1, 0x2b0a1c51

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 204
    :pswitch_0
    iget-object v0, p0, Lepson/maintain/activity/EccPrintLog;->mEccPrintLogViewModel:Lepson/maintain/activity/EccPrintLogViewModel;

    invoke-virtual {v0}, Lepson/maintain/activity/EccPrintLogViewModel;->getTitleAndMessage()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v5, 0x2

    .line 206
    aget-object v6, v0, v2

    aget-object v7, v0, v3

    const v0, 0x7f0e03fe

    .line 207
    invoke-virtual {p0, v0}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v4, p1

    .line 206
    invoke-static/range {v4 .. v10}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_3

    :pswitch_1
    const v0, 0x7f0e051b

    .line 201
    invoke-virtual {p0, v0}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v3, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_4

    .line 212
    invoke-virtual {v0, v3}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 213
    invoke-virtual {p0}, Lepson/maintain/activity/EccPrintLog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_4
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private show_message_and_finish(I)V
    .locals 1

    .line 173
    iget-object v0, p0, Lepson/maintain/activity/EccPrintLog;->mEccPrintLogViewModel:Lepson/maintain/activity/EccPrintLogViewModel;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog;->getErrorTitleAndMessage(I)[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/maintain/activity/EccPrintLogViewModel;->setTitleAndMessage([Ljava/lang/String;)V

    .line 174
    iget-object p1, p0, Lepson/maintain/activity/EccPrintLog;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v0, "dialog_error"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized startRemotePrintHistoryTask()V
    .locals 4

    monitor-enter p0

    .line 113
    :try_start_0
    sget-object v0, Lepson/maintain/activity/EccPrintLog;->mRemotePrintHistoryTask:Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;

    if-nez v0, :cond_0

    const-string v0, "EccPrintLog"

    const-string v1, "RemotePrintHistoryTask creating."

    .line 114
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    new-instance v0, Lepson/maintain/activity/LogUrl;

    invoke-direct {v0}, Lepson/maintain/activity/LogUrl;-><init>()V

    .line 116
    invoke-virtual {v0, p0}, Lepson/maintain/activity/LogUrl;->setContextDependValue(Landroid/content/Context;)V

    .line 117
    new-instance v1, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;

    invoke-direct {v1, p0}, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;-><init>(Lepson/maintain/activity/EccPrintLog;)V

    sput-object v1, Lepson/maintain/activity/EccPrintLog;->mRemotePrintHistoryTask:Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;

    .line 118
    sget-object v1, Lepson/maintain/activity/EccPrintLog;->mRemotePrintHistoryTask:Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;

    const/4 v2, 0x1

    new-array v2, v2, [Lepson/maintain/activity/LogUrl;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    const-string v0, "EccPrintLog"

    const-string v1, "RemotePrintHistoryTask exists. not create"

    .line 120
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    sget-object v0, Lepson/maintain/activity/EccPrintLog;->mRemotePrintHistoryTask:Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;

    invoke-virtual {v0, p0}, Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;->setActivity(Lepson/maintain/activity/EccPrintLog;)V

    .line 123
    :goto_0
    invoke-virtual {p0}, Lepson/maintain/activity/EccPrintLog;->showProgressDialog()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method dissmissProgressDialog()V
    .locals 2

    .line 185
    iget-object v0, p0, Lepson/maintain/activity/EccPrintLog;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method getErrorTitleAndMessage(I)[Ljava/lang/String;
    .locals 5

    const/4 v0, 0x2

    .line 128
    new-array v0, v0, [Ljava/lang/String;

    const v1, 0x7f0e0055

    .line 130
    invoke-virtual {p0, v1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    const/16 v2, -0x44c

    if-eq p1, v2, :cond_1

    const/16 v2, 0xa

    if-eq p1, v2, :cond_0

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :pswitch_0
    const p1, 0x7f0e0059

    .line 142
    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f0e0058

    .line 143
    invoke-virtual {p0, v1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_1
    const p1, 0x7f0e005c

    .line 146
    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f0e005b

    .line 147
    invoke-virtual {p0, v1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_2
    const p1, 0x7f0e0057

    .line 138
    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f0e0056

    .line 139
    invoke-virtual {p0, v1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_3
    const p1, 0x7f0e0049

    .line 151
    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f0e0047

    .line 152
    invoke-virtual {p0, v1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 156
    :cond_0
    invoke-virtual {p0, v1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    .line 160
    :goto_0
    invoke-virtual {p0, v1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 161
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0e0052

    invoke-virtual {p0, v3}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "0X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const p1, 0x7f0e0053

    .line 163
    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v4, v1

    move-object v1, p1

    move-object p1, v4

    goto :goto_1

    :cond_1
    const p1, 0x7f0e0060

    .line 134
    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f0e005f

    .line 135
    invoke-virtual {p0, v1}, Lepson/maintain/activity/EccPrintLog;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    const/4 v2, 0x0

    aput-object p1, v0, v2

    const/4 p1, 0x1

    aput-object v1, v0, p1

    return-object v0

    :pswitch_data_0
    .packed-switch -0x4b9
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x4b3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCancelDialog(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "EccPrintLog"

    const-string v1, "onCreate() start"

    .line 53
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a005d

    .line 55
    invoke-virtual {p0, p1}, Lepson/maintain/activity/EccPrintLog;->setContentView(I)V

    .line 58
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgressViewModel;

    iput-object p1, p0, Lepson/maintain/activity/EccPrintLog;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 59
    iget-object p1, p0, Lepson/maintain/activity/EccPrintLog;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lepson/maintain/activity/-$$Lambda$EccPrintLog$SANi6hPFCjheyNcFbDbjWxyLDlU;

    invoke-direct {v0, p0}, Lepson/maintain/activity/-$$Lambda$EccPrintLog$SANi6hPFCjheyNcFbDbjWxyLDlU;-><init>(Lepson/maintain/activity/EccPrintLog;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 80
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/maintain/activity/EccPrintLogViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/maintain/activity/EccPrintLogViewModel;

    iput-object p1, p0, Lepson/maintain/activity/EccPrintLog;->mEccPrintLogViewModel:Lepson/maintain/activity/EccPrintLogViewModel;

    const/4 p1, 0x1

    const v0, 0x7f0e0351

    .line 83
    invoke-virtual {p0, v0, p1}, Lepson/maintain/activity/EccPrintLog;->setActionBar(IZ)V

    const v0, 0x7f08029f

    .line 85
    invoke-virtual {p0, v0}, Lepson/maintain/activity/EccPrintLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lepson/maintain/activity/EccPrintLog;->mWebView:Landroid/webkit/WebView;

    .line 86
    iget-object v0, p0, Lepson/maintain/activity/EccPrintLog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 87
    iget-object v0, p0, Lepson/maintain/activity/EccPrintLog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 88
    iget-object p1, p0, Lepson/maintain/activity/EccPrintLog;->mWebView:Landroid/webkit/WebView;

    new-instance v0, Lepson/maintain/activity/EccPrintLog$1;

    invoke-direct {v0, p0}, Lepson/maintain/activity/EccPrintLog$1;-><init>(Lepson/maintain/activity/EccPrintLog;)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 108
    invoke-direct {p0}, Lepson/maintain/activity/EccPrintLog;->startRemotePrintHistoryTask()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .line 179
    invoke-virtual {p0}, Lepson/maintain/activity/EccPrintLog;->dissmissProgressDialog()V

    .line 180
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    return-void
.end method

.method public onNegativeClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onNeutralClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onPositiveClick(Ljava/lang/String;)V
    .locals 2

    .line 244
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x2b0a1c51

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    .line 246
    :cond_2
    invoke-virtual {p0}, Lepson/maintain/activity/EccPrintLog;->finish()V

    :goto_2
    return-void
.end method

.method public declared-synchronized setUrl(Ljava/lang/String;I)V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    .line 231
    :try_start_0
    sput-object v0, Lepson/maintain/activity/EccPrintLog;->mRemotePrintHistoryTask:Lepson/maintain/activity/EccPrintLog$RemotePrintHistoryTask;

    const-string v0, "EccPrintLog"

    const-string v1, "setUrl"

    .line 232
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lepson/maintain/activity/EccPrintLog;->dissmissProgressDialog()V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_1

    .line 235
    :cond_0
    invoke-direct {p0, p2}, Lepson/maintain/activity/EccPrintLog;->show_message_and_finish(I)V

    .line 237
    :cond_1
    iget-object p2, p0, Lepson/maintain/activity/EccPrintLog;->mWebView:Landroid/webkit/WebView;

    if-eqz p2, :cond_2

    .line 238
    iget-object p2, p0, Lepson/maintain/activity/EccPrintLog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p2, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method showProgressDialog()V
    .locals 2

    .line 190
    iget-object v0, p0, Lepson/maintain/activity/EccPrintLog;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

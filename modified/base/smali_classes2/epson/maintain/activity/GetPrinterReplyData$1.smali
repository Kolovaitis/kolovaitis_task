.class Lepson/maintain/activity/GetPrinterReplyData$1;
.super Ljava/lang/Object;
.source "GetPrinterReplyData.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/GetPrinterReplyData;->startLoadConThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/GetPrinterReplyData;


# direct methods
.method constructor <init>(Lepson/maintain/activity/GetPrinterReplyData;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const-string v0, "MAINTAIN"

    const-string v1, "new load Config thread"

    .line 184
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iget-object v0, v0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-wide/16 v0, 0x1f4

    .line 188
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 190
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 192
    :goto_0
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v0}, Lepson/maintain/activity/GetPrinterReplyData;->access$000(Lepson/maintain/activity/GetPrinterReplyData;)V

    .line 193
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v0}, Lepson/maintain/activity/GetPrinterReplyData;->access$100(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v0}, Lepson/maintain/activity/GetPrinterReplyData;->access$200(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 194
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iget-object v0, v0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, "startLoadConThread"

    const-string v2, "mPrinter.doProbePrinter"

    .line 195
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iget-boolean v0, v0, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    if-eqz v0, :cond_0

    return-void

    .line 208
    :cond_0
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    iget-object v2, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v2}, Lepson/maintain/activity/GetPrinterReplyData;->access$100(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v3}, Lepson/maintain/activity/GetPrinterReplyData;->access$300(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v4}, Lepson/maintain/activity/GetPrinterReplyData;->access$400(Lepson/maintain/activity/GetPrinterReplyData;)I

    move-result v4

    const/16 v5, 0x3c

    invoke-virtual {v0, v5, v2, v3, v4}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 209
    :cond_1
    :goto_1
    iget-object v2, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iget-boolean v2, v2, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    if-nez v2, :cond_3

    if-nez v0, :cond_2

    goto :goto_2

    .line 214
    :cond_2
    :try_start_1
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    const/4 v3, 0x7

    .line 215
    iput v3, v2, Landroid/os/Message;->what:I

    .line 216
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "PROBE_ERROR"

    .line 217
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 218
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 219
    iget-object v3, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iget-object v3, v3, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-wide/16 v2, 0xfa0

    .line 220
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 221
    iget-object v2, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iget-boolean v2, v2, Lepson/maintain/activity/GetPrinterReplyData;->mIsCancelProbe:Z

    if-nez v2, :cond_1

    .line 223
    sget-object v2, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    iget-object v3, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v3}, Lepson/maintain/activity/GetPrinterReplyData;->access$100(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v4}, Lepson/maintain/activity/GetPrinterReplyData;->access$300(Lepson/maintain/activity/GetPrinterReplyData;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    invoke-static {v6}, Lepson/maintain/activity/GetPrinterReplyData;->access$400(Lepson/maintain/activity/GetPrinterReplyData;)I

    move-result v6

    invoke-virtual {v2, v5, v3, v4, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v2

    .line 226
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_3
    :goto_2
    const-string v2, "MAINTAIN"

    .line 229
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Probe Printer result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_5

    .line 231
    sget-object v0, Lepson/maintain/activity/GetPrinterReplyData;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setMSearchPos(I)V

    .line 232
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lepson/maintain/activity/GetPrinterReplyData;->mHavePrinter:Z

    .line 236
    iget-object v0, v0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_3

    .line 240
    :cond_4
    iget-object v0, p0, Lepson/maintain/activity/GetPrinterReplyData$1;->this$0:Lepson/maintain/activity/GetPrinterReplyData;

    iput-boolean v1, v0, Lepson/maintain/activity/GetPrinterReplyData;->mHavePrinter:Z

    .line 241
    iget-object v0, v0, Lepson/maintain/activity/GetPrinterReplyData;->mUiHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, "MAINTAIN"

    const-string v1, "not select printer"

    .line 242
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_3
    return-void
.end method

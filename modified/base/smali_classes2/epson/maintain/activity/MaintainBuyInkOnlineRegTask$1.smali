.class Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;
.super Landroid/os/AsyncTask;
.source "MaintainBuyInkOnlineRegTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->loadData(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$printerIp:Ljava/lang/String;

.field final synthetic val$urlType:I


# direct methods
.method constructor <init>(Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0

    .line 55
    iput-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    iput-object p2, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->val$printerIp:Ljava/lang/String;

    iput p4, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->val$urlType:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/net/Uri;
    .locals 1

    .line 64
    iget-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->val$context:Landroid/content/Context;

    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->val$printerIp:Ljava/lang/String;

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainActivity;->disableSimpleApAndWait(Landroid/content/Context;Ljava/lang/String;)Z

    .line 67
    iget p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->val$urlType:I

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 72
    iget-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->val$context:Landroid/content/Context;

    invoke-static {p1}, Lepson/print/Util/BuyInkUrl;->getBuyInkUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_0

    .line 69
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->val$context:Landroid/content/Context;

    invoke-static {p1}, Lepson/print/Util/BuyInkUrl;->getNozzleCheckGuidanceUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->doInBackground([Ljava/lang/Void;)Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/net/Uri;)V
    .locals 1

    .line 79
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 80
    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    invoke-virtual {v0, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->setUriEvent(Landroid/net/Uri;)V

    .line 81
    iget-object p1, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->setReady(Z)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 55
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->onPostExecute(Landroid/net/Uri;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 58
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 59
    iget-object v0, p0, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask$1;->this$0:Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/maintain/activity/MaintainBuyInkOnlineRegTask;->setReady(Z)V

    return-void
.end method

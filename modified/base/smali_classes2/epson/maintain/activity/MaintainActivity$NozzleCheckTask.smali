.class Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;
.super Landroid/os/AsyncTask;
.source "MaintainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/activity/MaintainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NozzleCheckTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/activity/MaintainActivity;


# direct methods
.method private constructor <init>(Lepson/maintain/activity/MaintainActivity;)V
    .locals 0

    .line 2238
    iput-object p1, p0, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/maintain/activity/MaintainActivity;Lepson/maintain/activity/MaintainActivity$1;)V
    .locals 0

    .line 2238
    invoke-direct {p0, p1}, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;-><init>(Lepson/maintain/activity/MaintainActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1

    .line 2247
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$1100(Lepson/maintain/activity/MaintainActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doDoMainteCmd(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2238
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1

    .line 2253
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$1400(Lepson/maintain/activity/MaintainActivity;)V

    .line 2256
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/16 v0, 0x6c

    if-ne p1, v0, :cond_0

    .line 2257
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;->this$0:Lepson/maintain/activity/MaintainActivity;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lepson/maintain/activity/MaintainActivity;->access$2500(Lepson/maintain/activity/MaintainActivity;I)V

    return-void

    .line 2261
    :cond_0
    iget-object p1, p0, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {p1}, Lepson/maintain/activity/MaintainActivity;->access$4000(Lepson/maintain/activity/MaintainActivity;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 2238
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 2242
    iget-object v0, p0, Lepson/maintain/activity/MaintainActivity$NozzleCheckTask;->this$0:Lepson/maintain/activity/MaintainActivity;

    invoke-static {v0}, Lepson/maintain/activity/MaintainActivity;->access$1800(Lepson/maintain/activity/MaintainActivity;)V

    return-void
.end method

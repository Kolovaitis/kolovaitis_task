.class Lepson/maintain/FirmwareManager$LocalAsyncTask;
.super Landroid/os/AsyncTask;
.source "FirmwareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/FirmwareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lepson/maintain/FirmwareManager$FWUpdateStep;",
        "Ljava/lang/Integer;",
        "Lepson/maintain/FirmwareManager$FWUpdateProcResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/maintain/FirmwareManager;


# direct methods
.method constructor <init>(Lepson/maintain/FirmwareManager;)V
    .locals 0

    .line 396
    iput-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method static synthetic access$800(Lepson/maintain/FirmwareManager$LocalAsyncTask;[Ljava/lang/Object;)V
    .locals 0

    .line 396
    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method private downloadEfuFile(Ljava/io/File;)Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 2

    .line 497
    iget-object v0, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {v0}, Lepson/maintain/FirmwareManager;->access$100(Lepson/maintain/FirmwareManager;)Lepson/maintain/FirmInfFile;

    move-result-object v0

    invoke-virtual {v0}, Lepson/maintain/FirmInfFile;->getfuUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 499
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object p1

    :cond_0
    const/4 v1, 0x0

    .line 502
    invoke-virtual {p0, v0, p1, v1}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->download(Ljava/lang/String;Ljava/io/File;Z)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    return-object p1
.end method

.method private extractFirmDataFromRcx()Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 5

    .line 515
    :try_start_0
    invoke-static {}, Lepson/maintain/FirmwareManager;->createNewSaveFile()Ljava/io/File;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 522
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    const-string v3, "zip.efu"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 523
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    const-string v4, "unzip.efu"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 525
    sget-object v3, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    .line 527
    :try_start_1
    invoke-direct {p0, v1}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->downloadEfuFile(Ljava/io/File;)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object v3

    .line 528
    sget-object v4, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_1
    .catch Lepson/maintain/EfuReader$WriteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v3, v4, :cond_2

    .line 546
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 549
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 550
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_1
    return-object v3

    .line 532
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lepson/maintain/EfuReader;->unzipOnlyOneEntry(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    new-instance v3, Lepson/maintain/EfuReader;

    invoke-direct {v3}, Lepson/maintain/EfuReader;-><init>()V

    .line 535
    invoke-virtual {v3, v2, v0}, Lepson/maintain/EfuReader;->writeOnlyOneFirmwareData(Ljava/io/File;Ljava/io/File;)V

    .line 537
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_2
    .catch Lepson/maintain/EfuReader$WriteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 546
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 547
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 549
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 550
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    .line 543
    :catch_0
    :try_start_3
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 546
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 547
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 549
    :cond_4
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_0

    .line 540
    :catch_1
    :try_start_4
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->DiskFull:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 546
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 547
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 549
    :cond_5
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_0

    :cond_6
    :goto_1
    return-object v0

    .line 546
    :goto_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 547
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 549
    :cond_7
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 550
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_8
    throw v0

    .line 518
    :catch_2
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->DiskFull:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object v0
.end method


# virtual methods
.method protected varargs doInBackground([Lepson/maintain/FirmwareManager$FWUpdateStep;)Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 3

    .line 405
    iget-object v0, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->None:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    iput-object v1, v0, Lepson/maintain/FirmwareManager;->mProcResult:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    .line 406
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    iget-object v1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    const/4 v2, 0x0

    aget-object p1, p1, v2

    iput-object p1, v1, Lepson/maintain/FirmwareManager;->mProcStep:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p1}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 440
    :pswitch_0
    invoke-virtual {p0}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->sendFW()Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    return-object p1

    .line 430
    :pswitch_1
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {p1}, Lepson/maintain/FirmwareManager;->access$200(Lepson/maintain/FirmwareManager;)Lepson/print/MyPrinter;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-static {}, Lepson/maintain/FirmwareManager;->isExistSaveFile()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 431
    :cond_0
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    const-string v1, "https"

    iput-object v1, p1, Lepson/maintain/FirmwareManager;->mSendProtocol:Ljava/lang/String;

    .line 432
    invoke-virtual {p1}, Lepson/maintain/FirmwareManager;->getStartUpdateURL()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v0, v2}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->download(Ljava/lang/String;Ljava/io/File;Z)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    .line 434
    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    if-eq p1, v1, :cond_1

    .line 435
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    const-string v1, "http"

    iput-object v1, p1, Lepson/maintain/FirmwareManager;->mSendProtocol:Ljava/lang/String;

    .line 436
    invoke-virtual {p1}, Lepson/maintain/FirmwareManager;->getStartUpdateURL()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v0, v2}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->download(Ljava/lang/String;Ljava/io/File;Z)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    :cond_1
    return-object p1

    .line 415
    :pswitch_2
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {p1}, Lepson/maintain/FirmwareManager;->access$100(Lepson/maintain/FirmwareManager;)Lepson/maintain/FirmInfFile;

    move-result-object p1

    invoke-virtual {p1}, Lepson/maintain/FirmInfFile;->getInfFileVersion()I

    move-result p1

    packed-switch p1, :pswitch_data_1

    .line 426
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object p1

    .line 420
    :pswitch_3
    invoke-direct {p0}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->extractFirmDataFromRcx()Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    return-object p1

    .line 418
    :pswitch_4
    invoke-virtual {p0}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->downloadNewFW()Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    return-object p1

    .line 410
    :pswitch_5
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {p1}, Lepson/maintain/FirmwareManager;->access$000(Lepson/maintain/FirmwareManager;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    .line 411
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object p1

    .line 413
    :cond_2
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {p1}, Lepson/maintain/FirmwareManager;->access$000(Lepson/maintain/FirmwareManager;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v0, v2}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->download(Ljava/lang/String;Ljava/io/File;Z)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    return-object p1

    .line 408
    :pswitch_6
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-virtual {p1}, Lepson/maintain/FirmwareManager;->getPrinterFWVer()Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    return-object p1

    .line 445
    :cond_3
    :goto_0
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 396
    check-cast p1, [Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->doInBackground([Lepson/maintain/FirmwareManager$FWUpdateStep;)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    return-object p1
.end method

.method download(Ljava/lang/String;Ljava/io/File;Z)Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    .line 558
    invoke-virtual/range {v0 .. v5}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->download(Ljava/lang/String;Ljava/io/File;ZII)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object p1

    return-object p1
.end method

.method download(Ljava/lang/String;Ljava/io/File;ZII)Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    if-nez v0, :cond_0

    .line 562
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object v0

    :cond_0
    const/4 v3, 0x1

    .line 563
    new-array v4, v3, [Ljava/lang/Integer;

    add-int/lit8 v5, p4, -0x1

    int-to-double v5, v5

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    mul-double v9, v5, v7

    move/from16 v11, p5

    int-to-double v11, v11

    div-double/2addr v9, v11

    double-to-int v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x0

    aput-object v9, v4, v10

    invoke-virtual {v1, v4}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 567
    sget-object v4, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    .line 569
    :try_start_0
    new-instance v13, Ljava/net/URL;

    invoke-direct {v13, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    const-string v0, "GET"

    .line 570
    invoke-virtual {v13, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 571
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->connect()V

    .line 572
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v14, 0x190

    const/16 v15, 0x1f4

    if-gt v14, v0, :cond_2

    if-ge v0, v15, :cond_2

    .line 574
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-eqz v13, :cond_1

    .line 630
    :try_start_2
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :cond_1
    return-object v0

    :cond_2
    if-gt v15, v0, :cond_6

    const/16 v14, 0x258

    if-ge v0, v14, :cond_6

    if-ne v15, v0, :cond_4

    .line 579
    :try_start_3
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    iget-object v2, v1, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    iget-object v2, v2, Lepson/maintain/FirmwareManager;->mProcStep:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v2}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 582
    :pswitch_0
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->PrinterBadStatus:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v13, :cond_3

    .line 630
    :try_start_4
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    :cond_3
    return-object v0

    .line 586
    :cond_4
    :goto_0
    :try_start_5
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v13, :cond_5

    .line 630
    :try_start_6
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    :cond_5
    return-object v0

    .line 589
    :cond_6
    :try_start_7
    new-instance v14, Ljava/io/BufferedInputStream;

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    const/16 v15, 0x1000

    invoke-direct {v14, v0, v15}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v2, :cond_7

    .line 590
    :try_start_8
    new-instance v0, Ljava/io/FileOutputStream;

    move/from16 v9, p3

    invoke-direct {v0, v2, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    goto :goto_1

    :cond_7
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :goto_1
    move-object v9, v0

    .line 592
    :try_start_9
    new-array v0, v15, [B

    .line 593
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v2

    int-to-long v7, v2

    const-wide/16 v17, 0x0

    .line 596
    :goto_2
    invoke-virtual {v14, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v2

    const/4 v15, -0x1

    if-eq v2, v15, :cond_a

    .line 597
    invoke-virtual/range {p0 .. p0}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->isCancelled()Z

    move-result v15

    if-eqz v15, :cond_8

    .line 598
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 599
    sget-object v4, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Cancel:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    :cond_8
    move-object/from16 p5, v4

    int-to-long v3, v2

    add-long v3, v17, v3

    .line 604
    :try_start_a
    invoke-virtual {v9, v0, v10, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const/4 v2, 0x1

    .line 610
    :try_start_b
    new-array v15, v2, [Ljava/lang/Integer;

    move-wide/from16 v16, v11

    long-to-double v10, v3

    move-wide/from16 p3, v3

    long-to-double v2, v7

    div-double/2addr v10, v2

    add-double/2addr v10, v5

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double v10, v10, v2

    div-double v10, v10, v16

    double-to-int v10, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x0

    aput-object v10, v15, v11

    invoke-virtual {v1, v15}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->publishProgress([Ljava/lang/Object;)V

    move-object/from16 v4, p5

    move-wide/from16 v11, v16

    const/4 v3, 0x1

    const/4 v10, 0x0

    move-wide/from16 v17, p3

    goto :goto_2

    .line 606
    :catch_3
    instance-of v0, v9, Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_9

    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    goto :goto_3

    :cond_9
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->DiskFull:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    :goto_3
    move-object v4, v0

    goto :goto_4

    :cond_a
    move-object/from16 p5, v4

    .line 612
    :goto_4
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    if-ne v4, v0, :cond_b

    .line 613
    invoke-virtual {v9}, Ljava/io/OutputStream;->flush()V

    .line 614
    instance-of v0, v9, Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_b

    .line 615
    iget-object v0, v1, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    move-object v2, v9

    check-cast v2, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iput-object v2, v0, Lepson/maintain/FirmwareManager;->mDlData:[B
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 622
    :cond_b
    :try_start_c
    invoke-virtual {v14}, Ljava/io/BufferedInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4

    .line 626
    :catch_4
    :try_start_d
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5

    :catch_5
    if-eqz v13, :cond_e

    .line 630
    :goto_5
    :try_start_e
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_c

    goto :goto_a

    :catchall_0
    move-exception v0

    goto :goto_b

    :catch_6
    move-exception v0

    move-object v2, v9

    move-object v9, v14

    goto :goto_7

    :catchall_1
    move-exception v0

    const/4 v9, 0x0

    goto :goto_b

    :catch_7
    move-exception v0

    move-object v9, v14

    const/4 v2, 0x0

    goto :goto_7

    :catchall_2
    move-exception v0

    const/4 v9, 0x0

    goto :goto_6

    :catch_8
    move-exception v0

    const/4 v2, 0x0

    const/4 v9, 0x0

    goto :goto_7

    :catchall_3
    move-exception v0

    const/4 v9, 0x0

    const/4 v13, 0x0

    :goto_6
    const/4 v14, 0x0

    goto :goto_b

    :catch_9
    move-exception v0

    const/4 v2, 0x0

    const/4 v9, 0x0

    const/4 v13, 0x0

    .line 618
    :goto_7
    :try_start_f
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 619
    sget-object v4, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    if-eqz v9, :cond_c

    .line 622
    :try_start_10
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_a

    goto :goto_8

    :catch_a
    nop

    :cond_c
    :goto_8
    if-eqz v2, :cond_d

    .line 626
    :try_start_11
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_b

    goto :goto_9

    :catch_b
    nop

    :cond_d
    :goto_9
    if-eqz v13, :cond_e

    goto :goto_5

    :catch_c
    :cond_e
    :goto_a
    return-object v4

    :catchall_4
    move-exception v0

    move-object v14, v9

    move-object v9, v2

    :goto_b
    if-eqz v14, :cond_f

    .line 622
    :try_start_12
    invoke-virtual {v14}, Ljava/io/BufferedInputStream;->close()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_d

    goto :goto_c

    :catch_d
    nop

    :cond_f
    :goto_c
    if-eqz v9, :cond_10

    .line 626
    :try_start_13
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_e

    goto :goto_d

    :catch_e
    nop

    :cond_10
    :goto_d
    if-eqz v13, :cond_11

    .line 630
    :try_start_14
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_f

    .line 632
    :catch_f
    :cond_11
    throw v0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method downloadNewFW()Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 8

    .line 638
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    .line 640
    :try_start_0
    invoke-static {}, Lepson/maintain/FirmwareManager;->createNewSaveFile()Ljava/io/File;

    move-result-object v0

    .line 641
    iget-object v1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {v1}, Lepson/maintain/FirmwareManager;->access$500(Lepson/maintain/FirmwareManager;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 642
    iget-object v1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    iget-object v1, v1, Lepson/maintain/FirmwareManager;->mCurVer:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {v1}, Lepson/maintain/FirmwareManager;->access$100(Lepson/maintain/FirmwareManager;)Lepson/maintain/FirmInfFile;

    move-result-object v1

    invoke-virtual {v1}, Lepson/maintain/FirmInfFile;->getMainVersion()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 643
    iget-object v1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {v1}, Lepson/maintain/FirmwareManager;->access$100(Lepson/maintain/FirmwareManager;)Lepson/maintain/FirmInfFile;

    move-result-object v1

    invoke-virtual {v1}, Lepson/maintain/FirmInfFile;->getRomNum()I

    move-result v7

    const/4 v4, 0x0

    .line 644
    iget-object v1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    iget v5, v1, Lepson/maintain/FirmwareManager;->mCurRomNum:I

    move-object v1, p0

    move-object v3, v0

    move v6, v7

    invoke-virtual/range {v1 .. v6}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->download(Ljava/lang/String;Ljava/io/File;ZII)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object v1

    .line 645
    :goto_0
    sget-object v2, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    if-ne v1, v2, :cond_2

    iget-object v2, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {v2}, Lepson/maintain/FirmwareManager;->access$500(Lepson/maintain/FirmwareManager;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 646
    invoke-virtual {p0}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 647
    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Cancel:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    goto :goto_1

    :cond_0
    const/4 v4, 0x1

    .line 650
    iget-object v1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    iget v5, v1, Lepson/maintain/FirmwareManager;->mCurRomNum:I

    move-object v1, p0

    move-object v3, v0

    move v6, v7

    invoke-virtual/range {v1 .. v6}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->download(Ljava/lang/String;Ljava/io/File;ZII)Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object v1

    goto :goto_0

    .line 653
    :cond_1
    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 659
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 660
    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    goto :goto_1

    :catch_1
    move-exception v0

    .line 656
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 657
    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->DiskFull:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    :cond_2
    :goto_1
    return-object v1
.end method

.method protected onCancelled()V
    .locals 2

    .line 493
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onPostExecute(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V
    .locals 2

    .line 450
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    if-ne p1, v0, :cond_0

    .line 451
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    iget-object v1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    iget-object v1, v1, Lepson/maintain/FirmwareManager;->mProcStep:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v1}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 471
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    invoke-virtual {p1, v0}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    goto :goto_0

    .line 465
    :pswitch_0
    iget-object v0, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-virtual {v0, p1}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    return-void

    .line 462
    :pswitch_1
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->Send:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p1, v0}, Lepson/maintain/FirmwareManager;->startProc(Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    return-void

    .line 459
    :pswitch_2
    iget-object v0, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-virtual {v0, p1}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    return-void

    .line 456
    :pswitch_3
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    iget-object v0, p1, Lepson/maintain/FirmwareManager;->mDlData:[B

    invoke-static {p1, v0}, Lepson/maintain/FirmwareManager;->access$300(Lepson/maintain/FirmwareManager;[B)V

    return-void

    .line 453
    :pswitch_4
    iget-object v0, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-virtual {v0, p1}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    return-void

    .line 473
    :cond_0
    iget-object v0, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-virtual {v0, p1}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 396
    check-cast p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->onPostExecute(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 399
    iget-object v0, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    const/4 v1, 0x0

    iput-object v1, v0, Lepson/maintain/FirmwareManager;->mDlData:[B

    const/4 v0, 0x0

    .line 400
    new-array v0, v0, [Ljava/lang/Integer;

    invoke-virtual {p0, v0}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3

    .line 480
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 481
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    iget-object v2, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    iget-object v2, v2, Lepson/maintain/FirmwareManager;->mProcStep:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v2}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    .line 485
    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 488
    :cond_0
    iget-object p1, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {p1}, Lepson/maintain/FirmwareManager;->access$400(Lepson/maintain/FirmwareManager;)Lepson/maintain/FirmwareManager$FWUpdateListener;

    move-result-object p1

    invoke-interface {p1, v1}, Lepson/maintain/FirmwareManager$FWUpdateListener;->fwManagerProcDoing(I)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 396
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method sendFW()Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 10

    .line 674
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const/4 v1, 0x0

    .line 676
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-virtual {v3}, Lepson/maintain/FirmwareManager;->getSendFWURL()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 679
    :try_start_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 680
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "--"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {v4}, Lepson/maintain/FirmwareManager;->access$600(Lepson/maintain/FirmwareManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 681
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Content-Disposition: form-data; name=\"\"fname\"\"; filename=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v4, Ljava/io/File;

    .line 682
    invoke-static {}, Lepson/maintain/FirmwareManager;->access$700()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    .line 681
    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    const-string v3, "Content-Type: application/octet-stream\r\n"

    const-string v4, "UTF-8"

    .line 683
    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    const-string v3, "Content-Transfer-Encoding: binary\r\n"

    const-string v4, "UTF-8"

    .line 684
    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    const-string v3, "\r\n"

    const-string v4, "UTF-8"

    .line 685
    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 688
    new-instance v3, Ljava/io/File;

    invoke-static {}, Lepson/maintain/FirmwareManager;->access$700()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 691
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 692
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "--"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {v6}, Lepson/maintain/FirmwareManager;->access$600(Lepson/maintain/FirmwareManager;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "--"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 695
    iget-object v5, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v8

    int-to-long v8, v8

    add-long/2addr v6, v8

    iput-wide v6, v5, Lepson/maintain/FirmwareManager;->mTotalSize:J

    const-string v5, "POST"

    .line 697
    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const/4 v5, 0x1

    .line 698
    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const-string v5, "Content-Type"

    .line 699
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "multipart/form-data; boundary="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    invoke-static {v7}, Lepson/maintain/FirmwareManager;->access$600(Lepson/maintain/FirmwareManager;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    iget-object v5, p0, Lepson/maintain/FirmwareManager$LocalAsyncTask;->this$0:Lepson/maintain/FirmwareManager;

    iget-wide v5, v5, Lepson/maintain/FirmwareManager;->mTotalSize:J

    long-to-int v6, v5

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 703
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 706
    new-instance v5, Ljava/io/BufferedInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    .line 707
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1000

    invoke-direct {v5, v6, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 711
    new-instance v6, Lepson/maintain/FirmwareManager$CountingOutputStream;

    .line 712
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    new-instance v8, Lepson/maintain/FirmwareManager$LocalAsyncTask$1;

    invoke-direct {v8, p0, v2}, Lepson/maintain/FirmwareManager$LocalAsyncTask$1;-><init>(Lepson/maintain/FirmwareManager$LocalAsyncTask;Ljava/net/HttpURLConnection;)V

    invoke-direct {v6, v7, v8}, Lepson/maintain/FirmwareManager$CountingOutputStream;-><init>(Ljava/io/OutputStream;Lepson/maintain/FirmwareManager$ProgressListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 722
    :try_start_2
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 726
    new-array v1, v3, [B

    .line 727
    :goto_0
    invoke-virtual {v5, v1}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v7, -0x1

    if-eq v3, v7, :cond_0

    const/4 v7, 0x0

    .line 729
    :try_start_3
    invoke-virtual {v6, v1, v7, v3}, Lepson/maintain/FirmwareManager$CountingOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "FirmwareManager"

    .line 731
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error: in executeFile() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    throw v0

    .line 737
    :cond_0
    invoke-virtual {v4, v6}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 739
    invoke-virtual {v6}, Lepson/maintain/FirmwareManager$CountingOutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 746
    :try_start_5
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 753
    :catch_1
    :try_start_6
    invoke-virtual {v6}, Lepson/maintain/FirmwareManager$CountingOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 760
    :catch_2
    :try_start_7
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    const/16 v3, 0x190

    const/16 v4, 0x1f4

    if-gt v3, v1, :cond_1

    if-ge v1, v4, :cond_1

    .line 763
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    goto :goto_1

    :cond_1
    if-gt v4, v1, :cond_2

    const/16 v3, 0x258

    if-ge v1, v3, :cond_2

    .line 765
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    .line 772
    :try_start_8
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_4

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    .line 742
    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 746
    :goto_2
    :try_start_a
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 753
    :catch_4
    :try_start_b
    invoke-virtual {v6}, Lepson/maintain/FirmwareManager$CountingOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 756
    :catch_5
    :try_start_c
    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_6
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_5

    :catch_7
    move-exception v0

    .line 768
    :goto_3
    :try_start_d
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 769
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    if-eqz v1, :cond_3

    .line 772
    :try_start_e
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_8

    .line 776
    :catch_8
    :cond_3
    :goto_4
    invoke-virtual {p0}, Lepson/maintain/FirmwareManager$LocalAsyncTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Cancel:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    :cond_4
    return-object v0

    :goto_5
    if-eqz v2, :cond_5

    .line 772
    :try_start_f
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_9

    .line 774
    :catch_9
    :cond_5
    throw v0
.end method

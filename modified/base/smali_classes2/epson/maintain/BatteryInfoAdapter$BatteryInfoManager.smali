.class public Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;
.super Ljava/lang/Object;
.source "BatteryInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/BatteryInfoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BatteryInfoManager"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static batteryIsUnknown(II)Z
    .locals 0

    if-eqz p0, :cond_1

    const/4 p0, 0x2

    if-ne p1, p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static clearBatteryList(Landroid/app/Activity;)V
    .locals 2

    const v0, 0x7f080087

    .line 127
    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    .line 128
    invoke-static {p0, v0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->showListView(Landroid/app/Activity;Lepson/maintain/BatteryInfoAdapter;)V

    return-void
.end method

.method static setBatteryIcon(Landroid/app/Activity;Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;III)V
    .locals 3

    .line 155
    invoke-static {p2, p3}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->batteryIsUnknown(II)Z

    move-result p2

    if-eqz p2, :cond_0

    const p0, 0x7f070064

    .line 156
    invoke-virtual {p1, p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->setBatteryStatusIcon(I)V

    goto :goto_2

    :cond_0
    const/4 p2, 0x0

    const/16 v0, 0x64

    const/16 v1, 0x4c

    if-gt v1, p4, :cond_1

    if-gt p4, v0, :cond_1

    const/16 p4, 0x64

    goto :goto_0

    :cond_1
    const/16 v0, 0x33

    if-gt v0, p4, :cond_2

    if-ge p4, v1, :cond_2

    const/16 p4, 0x59

    goto :goto_0

    :cond_2
    const/16 v1, 0x1a

    if-gt v1, p4, :cond_3

    if-ge p4, v0, :cond_3

    const/16 p4, 0x3b

    goto :goto_0

    :cond_3
    if-lez p4, :cond_4

    if-ge p4, v1, :cond_4

    const/16 p4, 0x27

    goto :goto_0

    :cond_4
    const/4 p4, 0x0

    :goto_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_5

    const-string p3, "charge"

    goto :goto_1

    :cond_5
    const-string p3, "running"

    :goto_1
    const-string v1, "bi_%s_%d"

    const/4 v2, 0x2

    .line 174
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, p2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 175
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const-string p4, "drawable"

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p3, p2, p4, p0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    .line 176
    invoke-virtual {p1, p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->setBatteryStatusIcon(I)V

    :goto_2
    return-void
.end method

.method static setBatteryName(Landroid/content/Context;Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;II)V
    .locals 2

    const/4 v0, 0x1

    add-int/2addr p3, v0

    const v1, 0x7f0e02b3

    if-ne p2, v0, :cond_0

    .line 134
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->setBatteryStatusName(Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->setBatteryStatusName(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method static setBatteryPercentage(Landroid/content/Context;Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;III)V
    .locals 0

    .line 145
    invoke-static {p2, p3}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->batteryIsUnknown(II)Z

    move-result p2

    if-nez p2, :cond_1

    if-ltz p4, :cond_0

    const/16 p2, 0x64

    if-gt p4, p2, :cond_0

    .line 147
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " %"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->setBatteryStatusPercentage(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const p2, 0x7f0e02b2

    .line 149
    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->setBatteryStatusPercentage(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method static setListViewHeight(Landroid/widget/ListView;)V
    .locals 6

    .line 199
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 200
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 204
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 208
    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    const/4 v5, 0x0

    .line 209
    invoke-interface {v0, v3, v5, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 210
    invoke-virtual {v5, v2, v2}, Landroid/view/View;->measure(II)V

    .line 211
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 219
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v2

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int v2, v2, v0

    add-int/2addr v4, v2

    .line 222
    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 225
    :goto_1
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    return-void
.end method

.method public static showBatteryList(Landroid/app/Activity;II[I[I)V
    .locals 5

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 110
    new-instance v2, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;

    invoke-direct {v2}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;-><init>()V

    .line 112
    invoke-static {p0, v2, p1, v1}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->setBatteryName(Landroid/content/Context;Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;II)V

    .line 113
    aget v3, p3, v1

    aget v4, p4, v1

    invoke-static {p0, v2, p2, v3, v4}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->setBatteryPercentage(Landroid/content/Context;Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;III)V

    .line 114
    aget v3, p3, v1

    aget v4, p4, v1

    invoke-static {p0, v2, p2, v3, v4}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->setBatteryIcon(Landroid/app/Activity;Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;III)V

    .line 116
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    :cond_0
    new-instance p1, Lepson/maintain/BatteryInfoAdapter;

    const p2, 0x7f0a008a

    invoke-direct {p1, p0, p2, v0}, Lepson/maintain/BatteryInfoAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 122
    invoke-static {p0, p1}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->showListView(Landroid/app/Activity;Lepson/maintain/BatteryInfoAdapter;)V

    return-void
.end method

.method static showListView(Landroid/app/Activity;Lepson/maintain/BatteryInfoAdapter;)V
    .locals 1

    const v0, 0x7f08006f

    .line 189
    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/ListView;

    .line 190
    invoke-virtual {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 191
    invoke-static {p0}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;->setListViewHeight(Landroid/widget/ListView;)V

    return-void
.end method

.method public static showPowerText(Landroid/app/Activity;)V
    .locals 1

    const v0, 0x7f080087

    .line 181
    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.class public final enum Lepson/maintain/FirmwareManager$FWUpdateStep;
.super Ljava/lang/Enum;
.source "FirmwareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/FirmwareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FWUpdateStep"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/maintain/FirmwareManager$FWUpdateStep;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/maintain/FirmwareManager$FWUpdateStep;

.field public static final enum Download:Lepson/maintain/FirmwareManager$FWUpdateStep;

.field public static final enum GetPrinterFWVersion:Lepson/maintain/FirmwareManager$FWUpdateStep;

.field public static final enum GetUpdateInf:Lepson/maintain/FirmwareManager$FWUpdateStep;

.field public static final enum Init:Lepson/maintain/FirmwareManager$FWUpdateStep;

.field public static final enum ReadyUpdate:Lepson/maintain/FirmwareManager$FWUpdateStep;

.field public static final enum Send:Lepson/maintain/FirmwareManager$FWUpdateStep;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 45
    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateStep;

    const-string v1, "Init"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/maintain/FirmwareManager$FWUpdateStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->Init:Lepson/maintain/FirmwareManager$FWUpdateStep;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateStep;

    const-string v1, "GetPrinterFWVersion"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/maintain/FirmwareManager$FWUpdateStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetPrinterFWVersion:Lepson/maintain/FirmwareManager$FWUpdateStep;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateStep;

    const-string v1, "GetUpdateInf"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/maintain/FirmwareManager$FWUpdateStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetUpdateInf:Lepson/maintain/FirmwareManager$FWUpdateStep;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateStep;

    const-string v1, "Download"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/maintain/FirmwareManager$FWUpdateStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->Download:Lepson/maintain/FirmwareManager$FWUpdateStep;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateStep;

    const-string v1, "ReadyUpdate"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/maintain/FirmwareManager$FWUpdateStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->ReadyUpdate:Lepson/maintain/FirmwareManager$FWUpdateStep;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateStep;

    const-string v1, "Send"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lepson/maintain/FirmwareManager$FWUpdateStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->Send:Lepson/maintain/FirmwareManager$FWUpdateStep;

    const/4 v0, 0x6

    new-array v0, v0, [Lepson/maintain/FirmwareManager$FWUpdateStep;

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->Init:Lepson/maintain/FirmwareManager$FWUpdateStep;

    aput-object v1, v0, v2

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetPrinterFWVersion:Lepson/maintain/FirmwareManager$FWUpdateStep;

    aput-object v1, v0, v3

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetUpdateInf:Lepson/maintain/FirmwareManager$FWUpdateStep;

    aput-object v1, v0, v4

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->Download:Lepson/maintain/FirmwareManager$FWUpdateStep;

    aput-object v1, v0, v5

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->ReadyUpdate:Lepson/maintain/FirmwareManager$FWUpdateStep;

    aput-object v1, v0, v6

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->Send:Lepson/maintain/FirmwareManager$FWUpdateStep;

    aput-object v1, v0, v7

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->$VALUES:[Lepson/maintain/FirmwareManager$FWUpdateStep;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/maintain/FirmwareManager$FWUpdateStep;
    .locals 1

    .line 45
    const-class v0, Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/maintain/FirmwareManager$FWUpdateStep;

    return-object p0
.end method

.method public static values()[Lepson/maintain/FirmwareManager$FWUpdateStep;
    .locals 1

    .line 45
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->$VALUES:[Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v0}, [Lepson/maintain/FirmwareManager$FWUpdateStep;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/maintain/FirmwareManager$FWUpdateStep;

    return-object v0
.end method

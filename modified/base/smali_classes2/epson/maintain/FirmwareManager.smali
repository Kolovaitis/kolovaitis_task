.class public Lepson/maintain/FirmwareManager;
.super Ljava/lang/Object;
.source "FirmwareManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;,
        Lepson/maintain/FirmwareManager$LocalAsyncTask;,
        Lepson/maintain/FirmwareManager$CountingOutputStream;,
        Lepson/maintain/FirmwareManager$ProgressListener;,
        Lepson/maintain/FirmwareManager$FWUpdateListener;,
        Lepson/maintain/FirmwareManager$FWUpdateStep;,
        Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    }
.end annotation


# static fields
.field static final BUFFER_SIZE:I = 0x1000

.field static final DEV_FWUPDATE:Z = false

.field static final EVAL_UPDATEINF_URL_PREFIX:Ljava/lang/String; = "https://epsonpfu-stg.ebz.epson.net"

.field static FW_POST_FILE_NAME:Ljava/lang/String; = "/FWUPDATE_POST_DATA.dat"

.field private static FW_POST_FILE_PATH:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "FirmwareManager"

.field static final UPDATEINF_URL_PREFIX:Ljava/lang/String; = "https://epsonpfu.ebz.epson.net"


# instance fields
.field private final boundary:Ljava/lang/String;

.field private final lineEnd:Ljava/lang/String;

.field mCurRomNum:I

.field mCurVer:Ljava/lang/String;

.field mDlData:[B

.field mFWUpdateTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Lepson/maintain/FirmwareManager$FWUpdateStep;",
            "Ljava/lang/Integer;",
            "Lepson/maintain/FirmwareManager$FWUpdateProcResult;",
            ">;"
        }
    .end annotation
.end field

.field private mFirmInfFile:Lepson/maintain/FirmInfFile;

.field mMarketID:Ljava/lang/String;

.field mNewVer:Ljava/lang/String;

.field private final mPrinter:Lepson/print/MyPrinter;

.field private mPrinterMainVersion:Ljava/lang/String;

.field mProcResult:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field mProcStep:Lepson/maintain/FirmwareManager$FWUpdateStep;

.field mSendProtocol:Ljava/lang/String;

.field mTotalSize:J

.field private volatile mUpdateInfUrl:Ljava/lang/String;

.field private volatile mUpdateListener:Lepson/maintain/FirmwareManager$FWUpdateListener;

.field private final maintainPrinter:Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;

.field private final twoHyphens:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lepson/print/MyPrinter;)V
    .locals 2

    .line 101
    new-instance v0, Ljava/io/File;

    .line 102
    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getDownloadDir()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lepson/maintain/FirmwareManager;->FW_POST_FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;

    invoke-direct {v0}, Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;-><init>()V

    .line 101
    invoke-direct {p0, p2, p1, v0}, Lepson/maintain/FirmwareManager;-><init>(Lepson/print/MyPrinter;Ljava/lang/String;Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;)V

    return-void
.end method

.method constructor <init>(Lepson/print/MyPrinter;Ljava/lang/String;Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "--"

    .line 38
    iput-object v0, p0, Lepson/maintain/FirmwareManager;->twoHyphens:Ljava/lang/String;

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*****"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "*****"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/FirmwareManager;->boundary:Ljava/lang/String;

    const-string v0, "\r\n"

    .line 40
    iput-object v0, p0, Lepson/maintain/FirmwareManager;->lineEnd:Ljava/lang/String;

    .line 41
    new-instance v0, Lepson/maintain/FirmInfFile;

    invoke-direct {v0}, Lepson/maintain/FirmInfFile;-><init>()V

    iput-object v0, p0, Lepson/maintain/FirmwareManager;->mFirmInfFile:Lepson/maintain/FirmInfFile;

    .line 71
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->None:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    iput-object v0, p0, Lepson/maintain/FirmwareManager;->mProcResult:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    .line 72
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateStep;->Init:Lepson/maintain/FirmwareManager$FWUpdateStep;

    iput-object v0, p0, Lepson/maintain/FirmwareManager;->mProcStep:Lepson/maintain/FirmwareManager$FWUpdateStep;

    .line 75
    invoke-virtual {p0}, Lepson/maintain/FirmwareManager;->buildFWUpdateTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/FirmwareManager;->mFWUpdateTask:Landroid/os/AsyncTask;

    .line 115
    iput-object p1, p0, Lepson/maintain/FirmwareManager;->mPrinter:Lepson/print/MyPrinter;

    .line 116
    sput-object p2, Lepson/maintain/FirmwareManager;->FW_POST_FILE_PATH:Ljava/lang/String;

    .line 117
    iput-object p3, p0, Lepson/maintain/FirmwareManager;->maintainPrinter:Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;

    return-void
.end method

.method static synthetic access$000(Lepson/maintain/FirmwareManager;)Ljava/lang/String;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/maintain/FirmwareManager;->mUpdateInfUrl:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lepson/maintain/FirmwareManager;)Lepson/maintain/FirmInfFile;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/maintain/FirmwareManager;->mFirmInfFile:Lepson/maintain/FirmInfFile;

    return-object p0
.end method

.method static synthetic access$200(Lepson/maintain/FirmwareManager;)Lepson/print/MyPrinter;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/maintain/FirmwareManager;->mPrinter:Lepson/print/MyPrinter;

    return-object p0
.end method

.method static synthetic access$300(Lepson/maintain/FirmwareManager;[B)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lepson/maintain/FirmwareManager;->localCheckInfFileVersion([B)V

    return-void
.end method

.method static synthetic access$400(Lepson/maintain/FirmwareManager;)Lepson/maintain/FirmwareManager$FWUpdateListener;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/maintain/FirmwareManager;->mUpdateListener:Lepson/maintain/FirmwareManager$FWUpdateListener;

    return-object p0
.end method

.method static synthetic access$500(Lepson/maintain/FirmwareManager;)Ljava/lang/String;
    .locals 0

    .line 35
    invoke-direct {p0}, Lepson/maintain/FirmwareManager;->getNextRomURL()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lepson/maintain/FirmwareManager;)Ljava/lang/String;
    .locals 0

    .line 35
    iget-object p0, p0, Lepson/maintain/FirmwareManager;->boundary:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .line 35
    sget-object v0, Lepson/maintain/FirmwareManager;->FW_POST_FILE_PATH:Ljava/lang/String;

    return-object v0
.end method

.method private checkNotBatteryPowered()Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 7

    .line 228
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    .line 231
    new-instance v1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;-><init>()V

    const/4 v2, 0x0

    .line 234
    :goto_0
    iget-object v3, p0, Lepson/maintain/FirmwareManager;->maintainPrinter:Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;

    invoke-virtual {v3, v1}, Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;->getBatteryInfoEx(Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)I

    move-result v3

    const-string v4, "FirmwareManager"

    .line 235
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getBatteryInfoEx = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, -0x44c

    if-ne v3, v4, :cond_1

    add-int/lit8 v4, v2, 0x1

    const/4 v5, 0x5

    if-lt v2, v5, :cond_0

    goto :goto_1

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    :goto_1
    if-nez v3, :cond_2

    .line 239
    iget v2, v1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->powerSourceType:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget v1, v1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->powerSourceType:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 240
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotAC:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    :cond_2
    return-object v0
.end method

.method static createNewSaveFile()Ljava/io/File;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    .line 338
    new-instance v0, Ljava/io/File;

    sget-object v1, Lepson/maintain/FirmwareManager;->FW_POST_FILE_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 339
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 340
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    return-object v0
.end method

.method private getNextRomURL()Ljava/lang/String;
    .locals 2

    .line 168
    iget v0, p0, Lepson/maintain/FirmwareManager;->mCurRomNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lepson/maintain/FirmwareManager;->mCurRomNum:I

    .line 169
    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mFirmInfFile:Lepson/maintain/FirmInfFile;

    iget v1, p0, Lepson/maintain/FirmwareManager;->mCurRomNum:I

    invoke-virtual {v0, v1}, Lepson/maintain/FirmInfFile;->getRomUrl(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getUpdateInfSchemeAndHost()Ljava/lang/String;
    .locals 1

    .line 156
    sget-boolean v0, Lepson/maintain/FirmwareManager;->DEV_FWUPDATE:Z

    if-eqz v0, :cond_0

    const-string v0, "https://epsonpfu-stg.ebz.epson.net"

    goto :goto_0

    :cond_0
    const-string v0, "https://epsonpfu.ebz.epson.net"

    :goto_0
    return-object v0
.end method

.method static getUpdateInfUrlV01(Ljava/util/EnumMap;)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/util/EnumMap;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap<",
            "Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 280
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->FY:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-virtual {p0, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 285
    :cond_0
    sget-object v2, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MainVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-virtual {p0, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-eqz p0, :cond_2

    .line 286
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    goto :goto_0

    .line 290
    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s/%s_%s_00/UPDATE.INF"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 291
    invoke-static {}, Lepson/maintain/FirmwareManager;->getUpdateInfSchemeAndHost()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-virtual {p0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v4, v3

    .line 290
    invoke-static {v1, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    :goto_0
    return-object v1
.end method

.method public static isExistSaveFile()Z
    .locals 2

    .line 347
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lepson/maintain/FirmwareManager;->FW_POST_FILE_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private declared-synchronized localCheckInfFileVersion([B)V
    .locals 2

    monitor-enter p0

    .line 304
    :try_start_0
    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mFirmInfFile:Lepson/maintain/FirmInfFile;

    invoke-virtual {v0, p1}, Lepson/maintain/FirmInfFile;->checkInfFileVersion([B)Z

    move-result p1

    if-nez p1, :cond_0

    .line 305
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    monitor-exit p0

    return-void

    :cond_0
    const/4 p1, 0x0

    .line 311
    :try_start_1
    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mFirmInfFile:Lepson/maintain/FirmInfFile;

    iget-object v1, p0, Lepson/maintain/FirmwareManager;->mPrinterMainVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lepson/maintain/FirmInfFile;->compareVersion(Ljava/lang/String;)I

    move-result p1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 316
    :catch_0
    :try_start_2
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    invoke-virtual {p0, v0}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    :goto_0
    if-ltz p1, :cond_1

    .line 318
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotVerUp:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    goto :goto_1

    :cond_1
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    :goto_1
    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 319
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public static removeSavedFile()V
    .locals 2

    .line 329
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lepson/maintain/FirmwareManager;->FW_POST_FILE_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 330
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 332
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method buildFWUpdateTask()Landroid/os/AsyncTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask<",
            "Lepson/maintain/FirmwareManager$FWUpdateStep;",
            "Ljava/lang/Integer;",
            "Lepson/maintain/FirmwareManager$FWUpdateProcResult;",
            ">;"
        }
    .end annotation

    .line 392
    new-instance v0, Lepson/maintain/FirmwareManager$LocalAsyncTask;

    invoke-direct {v0, p0}, Lepson/maintain/FirmwareManager$LocalAsyncTask;-><init>(Lepson/maintain/FirmwareManager;)V

    return-object v0
.end method

.method public cancelProc()V
    .locals 1

    .line 142
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Cancel:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    invoke-virtual {p0, v0}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    return-void
.end method

.method endConnect()V
    .locals 2

    .line 122
    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mFWUpdateTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    return-void
.end method

.method endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V
    .locals 2

    .line 133
    invoke-virtual {p0}, Lepson/maintain/FirmwareManager;->endConnect()V

    .line 134
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    if-eq p1, v0, :cond_0

    invoke-static {}, Lepson/maintain/FirmwareManager;->removeSavedFile()V

    .line 135
    :cond_0
    iput-object p1, p0, Lepson/maintain/FirmwareManager;->mProcResult:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    .line 136
    iget-object p1, p0, Lepson/maintain/FirmwareManager;->mUpdateListener:Lepson/maintain/FirmwareManager$FWUpdateListener;

    if-eqz p1, :cond_1

    .line 137
    iget-object p1, p0, Lepson/maintain/FirmwareManager;->mUpdateListener:Lepson/maintain/FirmwareManager$FWUpdateListener;

    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mProcResult:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    iget-object v1, p0, Lepson/maintain/FirmwareManager;->mProcStep:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-interface {p1, v0, v1}, Lepson/maintain/FirmwareManager$FWUpdateListener;->fwManagerDidEndProc(Lepson/maintain/FirmwareManager$FWUpdateProcResult;Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    :cond_1
    return-void
.end method

.method getPrinterFWVer()Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 7

    .line 179
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 183
    :goto_0
    iget-object v3, p0, Lepson/maintain/FirmwareManager;->maintainPrinter:Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;

    invoke-virtual {v3, v0}, Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;->getFirmwareInfo(Ljava/util/EnumMap;)I

    move-result v3

    const-string v4, "FirmwareManager"

    .line 184
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getFirmwareInfo = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, -0x44c

    if-ne v3, v4, :cond_1

    add-int/lit8 v4, v2, 0x1

    const/4 v5, 0x5

    if-lt v2, v5, :cond_0

    goto :goto_1

    :cond_0
    move v2, v4

    goto :goto_0

    .line 187
    :cond_1
    :goto_1
    sget-object v2, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const/16 v4, -0x3f8

    if-eq v3, v4, :cond_8

    if-eqz v3, :cond_2

    goto/16 :goto_3

    .line 190
    :cond_2
    invoke-virtual {v0}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 191
    sget-object v2, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->NicFlg:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-virtual {v0, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 192
    sget-object v3, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MarketID:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-virtual {v0, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lepson/maintain/FirmwareManager;->mMarketID:Ljava/lang/String;

    .line 193
    sget-object v3, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MainVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-virtual {v0, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lepson/maintain/FirmwareManager;->mPrinterMainVersion:Ljava/lang/String;

    .line 194
    iget-object v3, p0, Lepson/maintain/FirmwareManager;->mPrinterMainVersion:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 195
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object v0

    :cond_3
    const-string v4, "%s.%s"

    const/4 v5, 0x2

    .line 197
    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v1

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->NetVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/maintain/FirmwareManager;->mCurVer:Ljava/lang/String;

    .line 198
    iget-object v1, p0, Lepson/maintain/FirmwareManager;->mCurVer:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lepson/maintain/FirmwareManager;->mMarketID:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v3, "NA"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz v2, :cond_4

    goto :goto_2

    .line 201
    :cond_4
    invoke-virtual {p0, v0}, Lepson/maintain/FirmwareManager;->getUpdateInfUrlForAllUrlVer(Ljava/util/EnumMap;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/FirmwareManager;->mUpdateInfUrl:Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mUpdateInfUrl:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 203
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object v0

    .line 206
    :cond_5
    invoke-direct {p0}, Lepson/maintain/FirmwareManager;->checkNotBatteryPowered()Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-result-object v0

    .line 207
    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    if-eq v0, v1, :cond_6

    return-object v0

    :cond_6
    move-object v2, v0

    goto :goto_3

    .line 199
    :cond_7
    :goto_2
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    move-object v2, v0

    goto :goto_3

    .line 214
    :cond_8
    sget-object v2, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    :cond_9
    :goto_3
    return-object v2
.end method

.method getSendFWURL()Ljava/lang/String;
    .locals 3

    .line 164
    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mSendProtocol:Ljava/lang/String;

    iget-object v1, p0, Lepson/maintain/FirmwareManager;->mPrinter:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/DOWN/FIRMWAREUPDATE/ROM1"

    invoke-static {v0, v1, v2}, Lepson/common/IPAddressUtils;->buildURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getStartUpdateURL()Ljava/lang/String;
    .locals 3

    .line 160
    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mSendProtocol:Ljava/lang/String;

    iget-object v1, p0, Lepson/maintain/FirmwareManager;->mPrinter:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/FIRMWAREUPDATE"

    invoke-static {v0, v1, v2}, Lepson/common/IPAddressUtils;->buildURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getUpdateInfURL()Ljava/lang/String;
    .locals 5

    const-string v0, "%s/%s_model_%s/UPDATE.INF"

    const/4 v1, 0x3

    .line 151
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Lepson/maintain/FirmwareManager;->getUpdateInfSchemeAndHost()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lepson/maintain/FirmwareManager;->mPrinter:Lepson/print/MyPrinter;

    .line 152
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    const-string v4, "_"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lepson/maintain/FirmwareManager;->mMarketID:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 151
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getUpdateInfUrlForAllUrlVer(Ljava/util/EnumMap;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/util/EnumMap;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap<",
            "Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 255
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->UrlVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-virtual {p1, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v2, -0x1

    .line 260
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v3, "01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :pswitch_1
    const-string v3, "00"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    :cond_1
    :goto_0
    packed-switch v2, :pswitch_data_1

    return-object v1

    .line 264
    :pswitch_2
    invoke-static {p1}, Lepson/maintain/FirmwareManager;->getUpdateInfUrlV01(Ljava/util/EnumMap;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 262
    :pswitch_3
    invoke-virtual {p0}, Lepson/maintain/FirmwareManager;->getUpdateInfURL()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x600
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public interruptProc()V
    .locals 1

    .line 146
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Interrupted:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    invoke-virtual {p0, v0}, Lepson/maintain/FirmwareManager;->endWithProcResult(Lepson/maintain/FirmwareManager$FWUpdateProcResult;)V

    return-void
.end method

.method public startFWDownload(Lepson/maintain/FirmwareManager$FWUpdateListener;)V
    .locals 0

    .line 323
    iput-object p1, p0, Lepson/maintain/FirmwareManager;->mUpdateListener:Lepson/maintain/FirmwareManager$FWUpdateListener;

    .line 324
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateStep;->Download:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager;->startProc(Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    return-void
.end method

.method public startFWUpdate(Lepson/maintain/FirmwareManager$FWUpdateListener;)V
    .locals 0

    .line 355
    iput-object p1, p0, Lepson/maintain/FirmwareManager;->mUpdateListener:Lepson/maintain/FirmwareManager$FWUpdateListener;

    .line 356
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateStep;->ReadyUpdate:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager;->startProc(Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    return-void
.end method

.method public startGetUpdateInf(Lepson/maintain/FirmwareManager$FWUpdateListener;)V
    .locals 0

    .line 295
    iput-object p1, p0, Lepson/maintain/FirmwareManager;->mUpdateListener:Lepson/maintain/FirmwareManager$FWUpdateListener;

    .line 296
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetUpdateInf:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager;->startProc(Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    return-void
.end method

.method startProc(Lepson/maintain/FirmwareManager$FWUpdateStep;)V
    .locals 3

    .line 126
    iput-object p1, p0, Lepson/maintain/FirmwareManager;->mProcStep:Lepson/maintain/FirmwareManager$FWUpdateStep;

    .line 127
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->None:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    iput-object v0, p0, Lepson/maintain/FirmwareManager;->mProcResult:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    .line 128
    invoke-virtual {p0}, Lepson/maintain/FirmwareManager;->buildFWUpdateTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/FirmwareManager;->mFWUpdateTask:Landroid/os/AsyncTask;

    .line 129
    iget-object v0, p0, Lepson/maintain/FirmwareManager;->mFWUpdateTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lepson/maintain/FirmwareManager$FWUpdateStep;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public startVersionCheck(Lepson/maintain/FirmwareManager$FWUpdateListener;)V
    .locals 0

    .line 174
    iput-object p1, p0, Lepson/maintain/FirmwareManager;->mUpdateListener:Lepson/maintain/FirmwareManager$FWUpdateListener;

    .line 175
    sget-object p1, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetPrinterFWVersion:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {p0, p1}, Lepson/maintain/FirmwareManager;->startProc(Lepson/maintain/FirmwareManager$FWUpdateStep;)V

    return-void
.end method

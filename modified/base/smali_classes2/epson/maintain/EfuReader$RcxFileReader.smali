.class Lepson/maintain/EfuReader$RcxFileReader;
.super Ljava/lang/Object;
.source "EfuReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/EfuReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RcxFileReader"
.end annotation


# static fields
.field private static final COMMENT_PATTERN:Ljava/util/regex/Pattern;

.field private static final PATTERN_KEY_AND_VALUE:Ljava/util/regex/Pattern;

.field static final PATTERN_KOMMA:Ljava/util/regex/Pattern;

.field private static final PATTERN_SECTION_SEPARATOR:Ljava/util/regex/Pattern;

.field private static final RCX_HEADER_ID:[B


# instance fields
.field private is:Ljava/io/RandomAccessFile;

.field private mHeaderEndPosition:J

.field private final mSectionAKeyAndValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "RCX\r\nSEIKO EPSON EpsonNet Form\r\n"

    const-string v1, "US-ASCII"

    .line 147
    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    sput-object v0, Lepson/maintain/EfuReader$RcxFileReader;->RCX_HEADER_ID:[B

    const-string v0, "^#.*"

    .line 149
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lepson/maintain/EfuReader$RcxFileReader;->COMMENT_PATTERN:Ljava/util/regex/Pattern;

    const-string v0, "\\[([A-Z]+)\\]"

    const/4 v1, 0x2

    .line 150
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lepson/maintain/EfuReader$RcxFileReader;->PATTERN_SECTION_SEPARATOR:Ljava/util/regex/Pattern;

    const-string v0, "(\\d+)=\"(.*)\""

    .line 152
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lepson/maintain/EfuReader$RcxFileReader;->PATTERN_KEY_AND_VALUE:Ljava/util/regex/Pattern;

    const-string v0, ","

    .line 154
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lepson/maintain/EfuReader$RcxFileReader;->PATTERN_KOMMA:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 156
    iput-object v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->is:Ljava/io/RandomAccessFile;

    .line 158
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->mSectionAKeyAndValue:Ljava/util/HashMap;

    const-wide/16 v0, -0x1

    .line 160
    iput-wide v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->mHeaderEndPosition:J

    .line 163
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v0, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->is:Ljava/io/RandomAccessFile;

    return-void
.end method

.method static synthetic access$000(Lepson/maintain/EfuReader$RcxFileReader;)Z
    .locals 0

    .line 146
    invoke-direct {p0}, Lepson/maintain/EfuReader$RcxFileReader;->readHeader()Z

    move-result p0

    return p0
.end method

.method public static checkHeaderId(Ljava/io/File;)Z
    .locals 1

    .line 170
    sget-object v0, Lepson/maintain/EfuReader$RcxFileReader;->RCX_HEADER_ID:[B

    array-length v0, v0

    invoke-static {p0, v0}, Lepson/maintain/EfuReader$RcxFileReader;->getFileByte(Ljava/io/File;I)[B

    move-result-object p0

    .line 172
    sget-object v0, Lepson/maintain/EfuReader$RcxFileReader;->RCX_HEADER_ID:[B

    invoke-static {v0, p0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method private static getFileByte(Ljava/io/File;I)[B
    .locals 5

    .line 176
    new-array v0, p1, [B

    const/4 v1, 0x0

    .line 179
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 p0, 0x0

    :cond_0
    sub-int v3, p1, p0

    .line 182
    :try_start_1
    invoke-virtual {v2, v0, p0, v3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    add-int/2addr p0, v3

    if-lt p0, p1, :cond_0

    .line 193
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return-object v0

    :catchall_0
    move-exception p0

    move-object v1, v2

    goto :goto_0

    :catch_1
    nop

    goto :goto_1

    :catchall_1
    move-exception p0

    :goto_0
    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 196
    :catch_2
    :cond_2
    throw p0

    :catch_3
    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_3

    .line 193
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :cond_3
    return-object v1
.end method

.method private getNextLine(Ljava/io/RandomAccessFile;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/io/RandomAccessFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 289
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v1, 0x0

    .line 292
    :goto_0
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0xa

    if-ne v2, p1, :cond_0

    const-string p1, "US-ASCII"

    .line 313
    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 317
    :cond_0
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1

    :pswitch_1
    const/16 v3, 0xd

    if-ne v2, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/16 v3, 0xc

    if-ne v2, v3, :cond_2

    .line 300
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v3

    if-nez v3, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 305
    :cond_2
    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 323
    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string v0, "file data ends"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static isCommentLine(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 245
    sget-object v0, Lepson/maintain/EfuReader$RcxFileReader;->COMMENT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 246
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->matches()Z

    move-result p0

    return p0
.end method

.method static isKeyAndValueData(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 270
    sget-object v0, Lepson/maintain/EfuReader$RcxFileReader;->PATTERN_KEY_AND_VALUE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 271
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const/4 v0, 0x2

    .line 276
    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v3

    return-object v1
.end method

.method static isSectionSeparator(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 256
    sget-object v0, Lepson/maintain/EfuReader$RcxFileReader;->PATTERN_SECTION_SEPARATOR:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 257
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const/4 v0, 0x1

    .line 260
    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private readHeader()Z
    .locals 6

    .line 209
    iget-object v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->mSectionAKeyAndValue:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const-wide/16 v0, -0x1

    .line 210
    iput-wide v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->mHeaderEndPosition:J

    const/4 v0, 0x0

    :try_start_0
    const-string v1, ""

    .line 215
    :cond_0
    :goto_0
    iget-object v2, p0, Lepson/maintain/EfuReader$RcxFileReader;->is:Ljava/io/RandomAccessFile;

    invoke-direct {p0, v2}, Lepson/maintain/EfuReader$RcxFileReader;->getNextLine(Ljava/io/RandomAccessFile;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    .line 216
    invoke-static {v2}, Lepson/maintain/EfuReader$RcxFileReader;->isCommentLine(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    .line 219
    :cond_1
    invoke-static {v2}, Lepson/maintain/EfuReader$RcxFileReader;->isSectionSeparator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    move-object v1, v4

    goto :goto_0

    .line 223
    :cond_2
    invoke-static {v2}, Lepson/maintain/EfuReader$RcxFileReader;->isKeyAndValueData(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_3
    const-string v4, "A"

    .line 227
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 228
    iget-object v4, p0, Lepson/maintain/EfuReader$RcxFileReader;->mSectionAKeyAndValue:Ljava/util/HashMap;

    aget-object v5, v2, v0

    aget-object v2, v2, v3

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 234
    :cond_4
    iget-object v1, p0, Lepson/maintain/EfuReader$RcxFileReader;->is:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v1

    iput-wide v1, p0, Lepson/maintain/EfuReader$RcxFileReader;->mHeaderEndPosition:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v3

    :catchall_0
    move-exception v0

    .line 237
    throw v0

    :catch_0
    return v0
.end method

.method static searchDataAndWriteRemaining(Ljava/io/RandomAccessFile;J[BLjava/io/File;)Z
    .locals 4
    .param p0    # Ljava/io/RandomAccessFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [B
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 383
    :try_start_0
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 384
    :try_start_1
    invoke-static {p0, p1, p2, p3}, Lepson/maintain/EfuReader$RcxFileReader;->seekDataPosition(Ljava/io/RandomAccessFile;J[B)V

    .line 385
    invoke-static {p0, v2}, Lepson/maintain/EfuReader$RcxFileReader;->writeRemainingData(Ljava/io/RandomAccessFile;Ljava/io/BufferedOutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p0, 0x1

    .line 395
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v0, 0x1

    goto :goto_2

    .line 398
    :catch_0
    invoke-virtual {p4}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 399
    :goto_0
    invoke-virtual {p4}, Ljava/io/File;->delete()Z

    goto :goto_2

    :catchall_0
    move-exception p0

    move-object v1, v2

    goto :goto_3

    :catch_1
    move-object v1, v2

    goto :goto_1

    :catchall_1
    move-exception p0

    goto :goto_3

    .line 389
    :catch_2
    :goto_1
    :try_start_3
    invoke-virtual {p4}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 390
    invoke-virtual {p4}, Ljava/io/File;->delete()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    .line 395
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    .line 398
    :catch_3
    invoke-virtual {p4}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    :goto_2
    return v0

    :goto_3
    if-eqz v1, :cond_2

    .line 395
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_4

    .line 398
    :catch_4
    invoke-virtual {p4}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 399
    invoke-virtual {p4}, Ljava/io/File;->delete()Z

    .line 401
    :cond_2
    :goto_4
    throw p0
.end method

.method static seekDataPosition(Ljava/io/RandomAccessFile;J[B)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 424
    invoke-virtual {p0, p1, p2}, Ljava/io/RandomAccessFile;->seek(J)V

    const/4 p1, 0x0

    const-wide/16 v0, 0x0

    const/4 p2, 0x0

    .line 430
    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    if-nez p2, :cond_1

    .line 432
    aget-byte v3, p3, p1

    if-ne v2, v3, :cond_0

    .line 433
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 437
    :cond_1
    aget-byte v3, p3, p2

    if-ne v2, v3, :cond_2

    add-int/lit8 p2, p2, 0x1

    .line 439
    array-length v2, p3

    if-lt p2, v2, :cond_0

    const-wide/16 p1, 0x1

    sub-long/2addr v0, p1

    .line 441
    invoke-virtual {p0, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    return-void

    .line 446
    :cond_2
    invoke-virtual {p0, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    const/4 p2, 0x0

    goto :goto_0

    .line 450
    :cond_3
    new-instance p0, Ljava/io/IOException;

    const-string p1, "data not found"

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static writeRemainingData(Ljava/io/RandomAccessFile;JLjava/io/File;)V
    .locals 3
    .param p0    # Ljava/io/RandomAccessFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 351
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 352
    :try_start_1
    invoke-virtual {p0, p1, p2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 353
    invoke-static {p0, v1}, Lepson/maintain/EfuReader$RcxFileReader;->writeRemainingData(Ljava/io/RandomAccessFile;Ljava/io/BufferedOutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 363
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :catch_0
    move-exception p0

    .line 365
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 366
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 368
    :cond_0
    new-instance p1, Lepson/maintain/EfuReader$WriteException;

    invoke-direct {p1, p0}, Lepson/maintain/EfuReader$WriteException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :catchall_0
    move-exception p0

    move-object v0, v1

    goto :goto_1

    :catch_1
    move-exception p0

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p0

    goto :goto_1

    :catch_2
    move-exception p0

    .line 356
    :goto_0
    :try_start_3
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 357
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 359
    :cond_1
    throw p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_1
    if-eqz v0, :cond_3

    .line 363
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    :catch_3
    move-exception p0

    .line 365
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 366
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 368
    :cond_2
    new-instance p1, Lepson/maintain/EfuReader$WriteException;

    invoke-direct {p1, p0}, Lepson/maintain/EfuReader$WriteException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :cond_3
    :goto_2
    throw p0
.end method

.method static writeRemainingData(Ljava/io/RandomAccessFile;Ljava/io/BufferedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x1000

    .line 412
    new-array v0, v0, [B

    .line 414
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, 0x0

    .line 416
    :try_start_0
    invoke-virtual {p1, v0, v2, v1}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 418
    new-instance p1, Lepson/maintain/EfuReader$WriteException;

    invoke-direct {p1, p0}, Lepson/maintain/EfuReader$WriteException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 328
    iget-object v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->is:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_0

    .line 330
    :try_start_0
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    const/4 v0, 0x0

    .line 331
    iput-object v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->is:Ljava/io/RandomAccessFile;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public getSectionAData(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 205
    iget-object v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->mSectionAKeyAndValue:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public writeOnlyOneFirmwareData(Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 339
    iget-object v0, p0, Lepson/maintain/EfuReader$RcxFileReader;->is:Ljava/io/RandomAccessFile;

    iget-wide v1, p0, Lepson/maintain/EfuReader$RcxFileReader;->mHeaderEndPosition:J

    invoke-static {v0, v1, v2, p1}, Lepson/maintain/EfuReader$RcxFileReader;->writeRemainingData(Ljava/io/RandomAccessFile;JLjava/io/File;)V

    return-void
.end method

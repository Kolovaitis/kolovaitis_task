.class Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;
.super Ljava/lang/Object;
.source "FirmwareManager.java"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/FirmwareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MaintainPrinterWrapper"
.end annotation


# instance fields
.field private final mMaintainPrinter2:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 789
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    iput-object v0, p0, Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;->mMaintainPrinter2:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-void
.end method


# virtual methods
.method public getBatteryInfoEx(Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)I
    .locals 1

    .line 797
    iget-object v0, p0, Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;->mMaintainPrinter2:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getBatteryInfo(Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)I

    move-result p1

    return p1
.end method

.method public getFirmwareInfo(Ljava/util/EnumMap;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap<",
            "Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .line 793
    iget-object v0, p0, Lepson/maintain/FirmwareManager$MaintainPrinterWrapper;->mMaintainPrinter2:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getFirmwareInfo(Ljava/util/EnumMap;)I

    move-result p1

    return p1
.end method

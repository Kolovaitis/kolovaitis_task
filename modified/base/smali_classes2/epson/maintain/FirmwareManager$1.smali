.class synthetic Lepson/maintain/FirmwareManager$1;
.super Ljava/lang/Object;
.source "FirmwareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/FirmwareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 406
    invoke-static {}, Lepson/maintain/FirmwareManager$FWUpdateStep;->values()[Lepson/maintain/FirmwareManager$FWUpdateStep;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    :try_start_0
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetPrinterFWVersion:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v1}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->GetUpdateInf:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v1}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->Download:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v1}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->ReadyUpdate:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v1}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->Send:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v1}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lepson/maintain/FirmwareManager$1;->$SwitchMap$epson$maintain$FirmwareManager$FWUpdateStep:[I

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateStep;->Init:Lepson/maintain/FirmwareManager$FWUpdateStep;

    invoke-virtual {v1}, Lepson/maintain/FirmwareManager$FWUpdateStep;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    return-void
.end method

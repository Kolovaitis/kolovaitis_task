.class public Lepson/maintain/FirmInfFile;
.super Ljava/lang/Object;
.source "FirmInfFile.java"


# static fields
.field private static final INF_KEY_URL_ROM_EFU:Ljava/lang/String; = "URLROMEFU"


# instance fields
.field private mInfFileVersion:I

.field mRomNum:I

.field final mUpdateInf:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lepson/maintain/FirmInfFile;->mInfFileVersion:I

    return-void
.end method

.method static getComparableVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    if-eqz p0, :cond_0

    .line 122
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x4

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 123
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method


# virtual methods
.method public declared-synchronized checkInfFileVersion([B)Z
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    .line 51
    :try_start_0
    iput v0, p0, Lepson/maintain/FirmInfFile;->mInfFileVersion:I

    .line 52
    invoke-virtual {p0, p1}, Lepson/maintain/FirmInfFile;->parseInfFile([B)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    .line 53
    monitor-exit p0

    return v0

    .line 56
    :cond_0
    :try_start_1
    iget-object p1, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const-string v1, "VERMAIN"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const-string v1, "VERNET"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    .line 60
    :cond_1
    iget-object p1, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const-string v1, "URLROM1"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    const/4 v1, 0x1

    if-eqz p1, :cond_2

    .line 64
    iput v1, p0, Lepson/maintain/FirmInfFile;->mInfFileVersion:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    monitor-exit p0

    return v1

    .line 68
    :cond_2
    :try_start_2
    iget-object p1, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const-string v2, "URLROMEFU"

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x2

    .line 72
    iput p1, p0, Lepson/maintain/FirmInfFile;->mInfFileVersion:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 73
    monitor-exit p0

    return v1

    .line 76
    :cond_3
    monitor-exit p0

    return v0

    .line 57
    :cond_4
    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public compareVersion(Ljava/lang/String;)I
    .locals 1

    .line 133
    invoke-virtual {p0}, Lepson/maintain/FirmInfFile;->getMainVersion()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/maintain/FirmInfFile;->getComparableVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-static {p1}, Lepson/maintain/FirmInfFile;->getComparableVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 135
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public declared-synchronized getInfFileVersion()I
    .locals 1

    monitor-enter p0

    .line 47
    :try_start_0
    iget v0, p0, Lepson/maintain/FirmInfFile;->mInfFileVersion:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMainVersion()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    .line 23
    :try_start_0
    iget-object v0, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const-string v1, "VERMAIN"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNetVersion()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    .line 27
    :try_start_0
    iget-object v0, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const-string v1, "VERNET"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRomNum()I
    .locals 1

    monitor-enter p0

    .line 31
    :try_start_0
    iget v0, p0, Lepson/maintain/FirmInfFile;->mRomNum:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRomUrl(I)Ljava/lang/String;
    .locals 4

    monitor-enter p0

    .line 18
    :try_start_0
    iget v0, p0, Lepson/maintain/FirmInfFile;->mRomNum:I

    if-gt p1, v0, :cond_0

    iget-object v0, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const-string v1, "URLROM%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 19
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 18
    :goto_0
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getfuUrl()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    monitor-enter p0

    .line 39
    :try_start_0
    iget-object v0, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    const-string v1, "URLROMEFU"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized parseInfFile([B)Z
    .locals 9
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    .line 88
    :try_start_0
    array-length v1, p1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    goto :goto_2

    .line 92
    :cond_0
    array-length v1, p1

    sub-int/2addr v1, v2

    new-array v1, v1, [B

    .line 93
    array-length v3, v1

    invoke-static {p1, v2, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v1}, Ljava/lang/String;-><init>([B)V

    const-string v1, ";\r\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 95
    iget-object v1, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 96
    iput v0, p0, Lepson/maintain/FirmInfFile;->mRomNum:I

    .line 97
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    if-ge v2, v1, :cond_4

    aget-object v4, p1, v2

    const-string v5, ":"

    .line 98
    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    goto :goto_1

    .line 100
    :cond_1
    invoke-virtual {v4, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v4, v5, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 102
    sget-boolean v5, Lepson/maintain/FirmwareManager;->DEV_FWUPDATE:Z

    if-eqz v5, :cond_2

    const-string v5, "URLROM"

    .line 103
    invoke-virtual {v7, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v6, :cond_2

    const-string v5, "https://epsonpfu.ebz.epson.net"

    const-string v8, "https://epsonpfu-stg.ebz.epson.net"

    .line 104
    invoke-virtual {v4, v5, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_2
    const-string v5, "URLROM"

    .line 108
    invoke-virtual {v7, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v6, :cond_3

    iget v5, p0, Lepson/maintain/FirmInfFile;->mRomNum:I

    add-int/2addr v5, v3

    iput v5, p0, Lepson/maintain/FirmInfFile;->mRomNum:I

    .line 109
    :cond_3
    iget-object v3, p0, Lepson/maintain/FirmInfFile;->mUpdateInf:Ljava/util/HashMap;

    invoke-virtual {v3, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 112
    :cond_4
    monitor-exit p0

    return v3

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    .line 89
    :cond_5
    :goto_2
    monitor-exit p0

    return v0
.end method

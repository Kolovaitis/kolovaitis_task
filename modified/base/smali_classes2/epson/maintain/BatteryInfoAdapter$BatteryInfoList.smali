.class Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;
.super Ljava/lang/Object;
.source "BatteryInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/BatteryInfoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BatteryInfoList"
.end annotation


# instance fields
.field private mBatteryName:Ljava/lang/String;

.field private mBatteryPercentage:Ljava/lang/String;

.field private mBatteryStatusIcon:I


# direct methods
.method constructor <init>()V
    .locals 1

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 73
    iput-object v0, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryName:Ljava/lang/String;

    .line 74
    iput-object v0, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryPercentage:Ljava/lang/String;

    const/4 v0, 0x0

    .line 75
    iput v0, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryStatusIcon:I

    return-void
.end method


# virtual methods
.method getBatteryName()Ljava/lang/String;
    .locals 1

    .line 78
    iget-object v0, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryName:Ljava/lang/String;

    return-object v0
.end method

.method getBatteryStatusIcon()I
    .locals 1

    .line 94
    iget v0, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryStatusIcon:I

    return v0
.end method

.method getBatteryStatusPercentage()Ljava/lang/String;
    .locals 1

    .line 86
    iget-object v0, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryPercentage:Ljava/lang/String;

    return-object v0
.end method

.method setBatteryStatusIcon(I)V
    .locals 0

    .line 98
    iput p1, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryStatusIcon:I

    return-void
.end method

.method setBatteryStatusName(Ljava/lang/String;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryName:Ljava/lang/String;

    return-void
.end method

.method setBatteryStatusPercentage(Ljava/lang/String;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->mBatteryPercentage:Ljava/lang/String;

    return-void
.end method

.class public Lepson/maintain/FirmwareManager$CountingOutputStream;
.super Ljava/io/FilterOutputStream;
.source "FirmwareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/FirmwareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CountingOutputStream"
.end annotation


# instance fields
.field private final listener:Lepson/maintain/FirmwareManager$ProgressListener;

.field private transferred:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Lepson/maintain/FirmwareManager$ProgressListener;)V
    .locals 0

    .line 372
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 373
    iput-object p2, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->listener:Lepson/maintain/FirmwareManager$ProgressListener;

    const-wide/16 p1, 0x0

    .line 374
    iput-wide p1, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->transferred:J

    return-void
.end method


# virtual methods
.method public write(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 384
    iget-object v0, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 385
    iget-wide v0, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->transferred:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->transferred:J

    .line 386
    iget-object p1, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->listener:Lepson/maintain/FirmwareManager$ProgressListener;

    iget-wide v0, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->transferred:J

    invoke-interface {p1, v0, v1}, Lepson/maintain/FirmwareManager$ProgressListener;->transferred(J)V

    return-void
.end method

.method public write([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 378
    iget-object v0, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 379
    iget-wide p1, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->transferred:J

    int-to-long v0, p3

    add-long/2addr p1, v0

    iput-wide p1, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->transferred:J

    .line 380
    iget-object p1, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->listener:Lepson/maintain/FirmwareManager$ProgressListener;

    iget-wide p2, p0, Lepson/maintain/FirmwareManager$CountingOutputStream;->transferred:J

    invoke-interface {p1, p2, p3}, Lepson/maintain/FirmwareManager$ProgressListener;->transferred(J)V

    return-void
.end method

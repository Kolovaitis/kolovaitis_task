.class public Lepson/maintain/BatteryInfoAdapter;
.super Landroid/widget/ArrayAdapter;
.source "BatteryInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/maintain/BatteryInfoAdapter$BatteryInfoManager;,
        Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;",
        ">;"
    }
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;",
            ">;"
        }
    .end annotation
.end field

.field private mResourceId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList<",
            "Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 33
    iput p2, p0, Lepson/maintain/BatteryInfoAdapter;->mResourceId:I

    .line 34
    iput-object p3, p0, Lepson/maintain/BatteryInfoAdapter;->mItems:Ljava/util/ArrayList;

    const-string p2, "layout_inflater"

    .line 35
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lepson/maintain/BatteryInfoAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-eqz p2, :cond_0

    goto :goto_0

    .line 44
    :cond_0
    iget-object p2, p0, Lepson/maintain/BatteryInfoAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget p3, p0, Lepson/maintain/BatteryInfoAdapter;->mResourceId:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 47
    :goto_0
    iget-object p3, p0, Lepson/maintain/BatteryInfoAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;

    const p3, 0x7f080088

    .line 50
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 51
    invoke-virtual {p1}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->getBatteryName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f080089

    .line 54
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 55
    invoke-virtual {p1}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->getBatteryStatusPercentage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f08008b

    .line 58
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 59
    invoke-virtual {p1}, Lepson/maintain/BatteryInfoAdapter$BatteryInfoList;->getBatteryStatusIcon()I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object p2
.end method

.method public isEnabled(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

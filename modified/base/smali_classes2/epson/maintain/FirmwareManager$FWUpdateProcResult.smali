.class public final enum Lepson/maintain/FirmwareManager$FWUpdateProcResult;
.super Ljava/lang/Enum;
.source "FirmwareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/maintain/FirmwareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FWUpdateProcResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/maintain/FirmwareManager$FWUpdateProcResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum Cancel:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum DiskFull:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum Interrupted:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum None:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum NotAC:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum NotVerUp:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum PrinterBadStatus:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

.field public static final enum Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 43
    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "None"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->None:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "Success"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "Cancel"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Cancel:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "Fail"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "NotVerUp"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotVerUp:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "NotSupport"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "Interrupted"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Interrupted:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "DiskFull"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->DiskFull:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "NotAC"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotAC:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    new-instance v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const-string v1, "PrinterBadStatus"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lepson/maintain/FirmwareManager$FWUpdateProcResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->PrinterBadStatus:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    const/16 v0, 0xa

    new-array v0, v0, [Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->None:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v2

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Success:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v3

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Cancel:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v4

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Fail:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v5

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotVerUp:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v6

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotSupport:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v7

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->Interrupted:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v8

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->DiskFull:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v9

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->NotAC:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v10

    sget-object v1, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->PrinterBadStatus:Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    aput-object v1, v0, v11

    sput-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->$VALUES:[Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 1

    .line 43
    const-class v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object p0
.end method

.method public static values()[Lepson/maintain/FirmwareManager$FWUpdateProcResult;
    .locals 1

    .line 43
    sget-object v0, Lepson/maintain/FirmwareManager$FWUpdateProcResult;->$VALUES:[Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    invoke-virtual {v0}, [Lepson/maintain/FirmwareManager$FWUpdateProcResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/maintain/FirmwareManager$FWUpdateProcResult;

    return-object v0
.end method

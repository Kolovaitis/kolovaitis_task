.class public final Lepson/image/epsonImage;
.super Ljava/lang/Object;
.source "epsonImage.java"


# static fields
.field private static bConvertSuccess:Z

.field private static final mLock:Ljava/lang/Object;

.field private static skipFiles:I


# instance fields
.field private debugString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 7
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "begin load epsonImage lib"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    const-string v0, "epsimage"

    .line 9
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 12
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load lib error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14
    :goto_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "load epsonImage lib finish"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/image/epsonImage;->mLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 54
    sput-boolean v0, Lepson/image/epsonImage;->bConvertSuccess:Z

    const/4 v0, 0x0

    .line 55
    sput v0, Lepson/image/epsonImage;->skipFiles:I

    return-void
.end method

.method private javaDebugCB([C)V
    .locals 1

    .line 118
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public native bmp2Jpg([C[C)I
.end method

.method public native bmp2Jpg2(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native epsmpBmpAddText([C[CIII)I
.end method

.method public native epsmpBmpAddText2(Ljava/lang/String;Ljava/lang/String;III)I
.end method

.method public native epsmpBmpBorderless([C[CI)I
.end method

.method public native epsmpBmpBorderless2(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public native epsmpBmpCDMaker(II[C[CI)I
.end method

.method public native epsmpBmpCDMaker2(IILjava/lang/String;Ljava/lang/String;I)I
.end method

.method public native epsmpBmpCrop([C[CIIIIII)I
.end method

.method public native epsmpBmpCrop2(Ljava/lang/String;Ljava/lang/String;IIIIII)I
.end method

.method public native epsmpBmpMerge([C[C[CII)I
.end method

.method public native epsmpBmpMerge2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public native epsmpCropImage([C[C[C)I
.end method

.method public native epsmpCropImage2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native epsmpGetDate(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public native epsmpGetImageSize([C[I)I
.end method

.method public native epsmpGetImageSize2(Ljava/lang/String;[I)I
.end method

.method public native epsmpJpgAppendFile([C[CI)I
.end method

.method public native epsmpJpgAppendFile2(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public native epsmpReziseImage([C[CII)I
.end method

.method public native epsmpReziseImage2(Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public native epsmpRotateImage([C[C)I
.end method

.method public native epsmpRotateImage2(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native epsmpRotateImageL90(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native jpg2Bmp([C[C)I
.end method

.method public native jpg2Bmp2(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native jpg2PDF([C[C)I
.end method

.method public native jpg2PDF2(Ljava/lang/String;Ljava/lang/String;)I
.end method

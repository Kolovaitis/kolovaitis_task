.class public final Landroid/support/v7/appcompat/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f08003a

.field public static final action_bar_activity_content:I = 0x7f08003b

.field public static final action_bar_container:I = 0x7f08003c

.field public static final action_bar_root:I = 0x7f08003d

.field public static final action_bar_spinner:I = 0x7f08003e

.field public static final action_bar_subtitle:I = 0x7f08003f

.field public static final action_bar_title:I = 0x7f080040

.field public static final action_container:I = 0x7f080041

.field public static final action_context_bar:I = 0x7f080042

.field public static final action_divider:I = 0x7f080043

.field public static final action_image:I = 0x7f080045

.field public static final action_menu_divider:I = 0x7f080046

.field public static final action_menu_presenter:I = 0x7f080047

.field public static final action_mode_bar:I = 0x7f080048

.field public static final action_mode_bar_stub:I = 0x7f080049

.field public static final action_mode_close_button:I = 0x7f08004a

.field public static final action_text:I = 0x7f08004c

.field public static final actions:I = 0x7f08004f

.field public static final activity_chooser_view_content:I = 0x7f080051

.field public static final add:I = 0x7f080054

.field public static final alertTitle:I = 0x7f08005b

.field public static final async:I = 0x7f080067

.field public static final blocking:I = 0x7f080072

.field public static final bottom:I = 0x7f080074

.field public static final buttonPanel:I = 0x7f0800a6

.field public static final checkbox:I = 0x7f0800bd

.field public static final chronometer:I = 0x7f0800be

.field public static final content:I = 0x7f0800d9

.field public static final contentPanel:I = 0x7f0800da

.field public static final custom:I = 0x7f0800fd

.field public static final customPanel:I = 0x7f0800fe

.field public static final decor_content_parent:I = 0x7f080101

.field public static final default_activity_button:I = 0x7f080102

.field public static final edit_query:I = 0x7f080122

.field public static final end:I = 0x7f080124

.field public static final expand_activities_button:I = 0x7f080139

.field public static final expanded_menu:I = 0x7f08013a

.field public static final forever:I = 0x7f080156

.field public static final group_divider:I = 0x7f080168

.field public static final home:I = 0x7f08017e

.field public static final icon:I = 0x7f080189

.field public static final icon_group:I = 0x7f08018b

.field public static final image:I = 0x7f080190

.field public static final info:I = 0x7f0801a1

.field public static final italic:I = 0x7f0801b1

.field public static final left:I = 0x7f0801bc

.field public static final line1:I = 0x7f0801c1

.field public static final line3:I = 0x7f0801c2

.field public static final listMode:I = 0x7f0801df

.field public static final list_item:I = 0x7f0801e2

.field public static final message:I = 0x7f080218

.field public static final multiply:I = 0x7f08021e

.field public static final none:I = 0x7f080228

.field public static final normal:I = 0x7f080229

.field public static final notification_background:I = 0x7f08022c

.field public static final notification_main_column:I = 0x7f08022d

.field public static final notification_main_column_container:I = 0x7f08022e

.field public static final parentPanel:I = 0x7f080256

.field public static final progress_circular:I = 0x7f080299

.field public static final progress_horizontal:I = 0x7f08029b

.field public static final radio:I = 0x7f0802a3

.field public static final right:I = 0x7f0802af

.field public static final right_icon:I = 0x7f0802b0

.field public static final right_side:I = 0x7f0802b1

.field public static final screen:I = 0x7f0802dc

.field public static final scrollIndicatorDown:I = 0x7f0802e0

.field public static final scrollIndicatorUp:I = 0x7f0802e1

.field public static final scrollView:I = 0x7f0802e2

.field public static final search_badge:I = 0x7f0802e4

.field public static final search_bar:I = 0x7f0802e5

.field public static final search_button:I = 0x7f0802e6

.field public static final search_close_btn:I = 0x7f0802e7

.field public static final search_edit_frame:I = 0x7f0802e8

.field public static final search_go_btn:I = 0x7f0802e9

.field public static final search_mag_icon:I = 0x7f0802ea

.field public static final search_plate:I = 0x7f0802eb

.field public static final search_src_text:I = 0x7f0802ec

.field public static final search_voice_btn:I = 0x7f0802ed

.field public static final select_dialog_listview:I = 0x7f0802f1

.field public static final shortcut:I = 0x7f0802fc

.field public static final spacer:I = 0x7f080303

.field public static final split_action_bar:I = 0x7f080304

.field public static final src_atop:I = 0x7f080305

.field public static final src_in:I = 0x7f080306

.field public static final src_over:I = 0x7f080307

.field public static final start:I = 0x7f080309

.field public static final submenuarrow:I = 0x7f080313

.field public static final submit_area:I = 0x7f080314

.field public static final tabMode:I = 0x7f080317

.field public static final tag_transition_group:I = 0x7f080318

.field public static final tag_unhandled_key_event_manager:I = 0x7f080319

.field public static final tag_unhandled_key_listeners:I = 0x7f08031a

.field public static final text:I = 0x7f08031d

.field public static final text2:I = 0x7f08031e

.field public static final textSpacerNoButtons:I = 0x7f080323

.field public static final textSpacerNoTitle:I = 0x7f080324

.field public static final time:I = 0x7f08033a

.field public static final title:I = 0x7f08033b

.field public static final titleDividerNoCustom:I = 0x7f08033d

.field public static final title_template:I = 0x7f080340

.field public static final top:I = 0x7f08034e

.field public static final topPanel:I = 0x7f08034f

.field public static final uniform:I = 0x7f080378

.field public static final up:I = 0x7f080379

.field public static final wrap_content:I = 0x7f080398


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

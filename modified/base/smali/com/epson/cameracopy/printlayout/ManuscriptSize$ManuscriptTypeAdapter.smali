.class interface abstract Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;
.super Ljava/lang/Object;
.source "ManuscriptSize.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/printlayout/ManuscriptSize;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "ManuscriptTypeAdapter"
.end annotation


# virtual methods
.method public abstract displayLength()Z
.end method

.method public abstract getBasePixelSize(I)Landroid/graphics/Point;
.end method

.method public abstract getSizeName(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getUnitType(Landroid/content/Context;)I
.end method

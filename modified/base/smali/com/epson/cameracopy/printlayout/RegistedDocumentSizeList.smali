.class public Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;
.super Ljava/lang/Object;
.source "RegistedDocumentSizeList.java"


# static fields
.field private static final CLASS_SAVE_VERSION:I = 0x1

.field private static DEFAULT_PAPER_SIZE_COUNT:I = 0x11

.field private static final DOCSIZE_NAME_ID:[I

.field private static final PAPER_SIZE_HEIGHT:[D

.field private static final PAPER_SIZE_ID:[I

.field private static final PAPER_SIZE_NAME:[I

.field private static final PAPER_SIZE_SCALE:[I

.field private static final PAPER_SIZE_WIDTH:[D

.field private static final REGISTED_DOCUMENTSIZE_LIST_FILENAME:Ljava/lang/String; = "registed_documentsize.bin"

.field static sRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;


# instance fields
.field mContext:Landroid/content/Context;

.field mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

.field private mDocumentSizeList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x11

    .line 45
    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_NAME:[I

    .line 51
    new-array v1, v0, [I

    fill-array-data v1, :array_1

    sput-object v1, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_SCALE:[I

    .line 54
    new-array v1, v0, [D

    fill-array-data v1, :array_2

    sput-object v1, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_WIDTH:[D

    .line 57
    new-array v1, v0, [D

    fill-array-data v1, :array_3

    sput-object v1, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_HEIGHT:[D

    .line 60
    new-array v1, v0, [I

    fill-array-data v1, :array_4

    sput-object v1, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_ID:[I

    .line 63
    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->DOCSIZE_NAME_ID:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0e040f
        0x7f0e040b
        0x7f0e040c
        0x7f0e040d
        0x7f0e040e
        0x7f0e0410
        0x7f0e0411
        0x7f0e0412
        0x7f0e0417
        0x7f0e0416
        0x7f0e0419
        0x7f0e0409
        0x7f0e0415
        0x7f0e040a
        0x7f0e0414
        0x7f0e0413
        0x7f0e0418
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x2
        0x2
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
    .end array-data

    :array_2
    .array-data 8
        0x406a400000000000L    # 210.0
        0x4072900000000000L    # 297.0
        0x406a400000000000L    # 210.0
        0x4062800000000000L    # 148.0
        0x405a400000000000L    # 105.0
        0x4070100000000000L    # 257.0
        0x4066c00000000000L    # 182.0
        0x4060000000000000L    # 128.0
        0x4021000000000000L    # 8.5
        0x4021000000000000L    # 8.5
        0x4059000000000000L    # 100.0
        0x4059800000000000L    # 102.0
        0x4056400000000000L    # 89.0
        0x405fc00000000000L    # 127.0
        0x404b000000000000L    # 54.0
        0x404b800000000000L    # 55.0
        0x4060000000000000L    # 128.0
    .end array-data

    :array_3
    .array-data 8
        0x4072900000000000L    # 297.0
        0x407a400000000000L    # 420.0
        0x4072900000000000L    # 297.0
        0x406a400000000000L    # 210.0
        0x4062800000000000L    # 148.0
        0x4076c00000000000L    # 364.0
        0x4070100000000000L    # 257.0
        0x4066c00000000000L    # 182.0
        0x4026000000000000L    # 11.0
        0x402c000000000000L    # 14.0
        0x4062800000000000L    # 148.0
        0x4063000000000000L    # 152.0
        0x405fc00000000000L    # 127.0
        0x4066400000000000L    # 178.0
        0x4055800000000000L    # 86.0
        0x4056c00000000000L    # 91.0
        0x4066c00000000000L    # 182.0
    .end array-data

    :array_4
    .array-data 4
        -0x1
        0x3e
        0x0
        0x3
        0x4
        0x3f
        0x5
        0x2e
        0x1
        0x2
        0x10
        0xa
        0xf
        0x1c
        0x23
        0x24
        0x2e
    .end array-data

    :array_5
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    if-eqz p1, :cond_0

    .line 75
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mContext:Landroid/content/Context;

    .line 76
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->initData()V

    return-void

    .line 73
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "context is null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static getCurrentDocumentSize(Landroid/content/Context;)Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;
    .locals 1

    .line 86
    sget-object v0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->sRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getCurrentDocumentSize()Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    move-result-object p0

    return-object p0

    .line 91
    :cond_0
    invoke-static {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    move-result-object p0

    .line 92
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getCurrentDocumentSize()Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;
    .locals 1

    if-eqz p0, :cond_1

    .line 104
    sget-object v0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->sRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->sRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    .line 106
    sget-object v0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->sRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->restoreData(Landroid/content/Context;)V

    .line 108
    :cond_0
    sget-object p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->sRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    return-object p0

    .line 102
    :cond_1
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "context == null"

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private initData()V
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    const/4 v0, 0x0

    .line 328
    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    return-void
.end method

.method private restoreData(Landroid/content/Context;)V
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "registed_documentsize.bin"

    .line 387
    invoke-virtual {p1, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v2, 0x1

    .line 392
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->available()I

    move-result v3

    .line 393
    new-array v3, v3, [B

    .line 394
    invoke-virtual {v1, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 396
    new-instance v4, Lorg/json/JSONObject;

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v3}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "version"

    .line 398
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 399
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 402
    invoke-static {v4}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocumentSizeInfoList(Lorg/json/JSONObject;)Ljava/util/LinkedList;

    move-result-object v3

    iput-object v3, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    const-string v3, "currentDocumentSize"

    .line 403
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 400
    :cond_0
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    throw v3
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    goto/16 :goto_b

    :catch_0
    move-object p1, v1

    goto/16 :goto_6

    :catch_1
    move-object p1, v1

    goto/16 :goto_8

    :catch_2
    move-exception v3

    .line 406
    :try_start_2
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    move-object v3, v0

    move-object v4, v3

    :goto_0
    if-nez v4, :cond_2

    .line 412
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    const-string v3, "registed_documentsize.bin"

    .line 413
    invoke-virtual {p1, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 414
    :try_start_3
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, p1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 415
    :try_start_4
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 416
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 419
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    iput-object v2, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    .line 420
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/Integer;

    goto :goto_1

    .line 417
    :cond_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :cond_2
    move-object p1, v1

    move-object v1, v0

    :goto_1
    if-eqz v3, :cond_5

    .line 423
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v4, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-lt v2, v4, :cond_3

    goto :goto_2

    .line 426
    :cond_3
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gez v2, :cond_4

    .line 428
    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    goto :goto_3

    .line 430
    :cond_4
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    goto :goto_3

    .line 425
    :cond_5
    :goto_2
    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_3
    if-eqz v1, :cond_6

    .line 445
    :try_start_5
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_4

    :catch_3
    nop

    :cond_6
    :goto_4
    if-eqz p1, :cond_9

    .line 452
    :goto_5
    :try_start_6
    invoke-virtual {p1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_c

    goto :goto_a

    :catchall_1
    move-exception v0

    move-object v6, v1

    move-object v1, p1

    move-object p1, v0

    move-object v0, v6

    goto :goto_b

    :catch_4
    move-object v0, v1

    goto :goto_6

    :catch_5
    move-object v0, v1

    goto :goto_8

    :catchall_2
    move-exception p1

    move-object v1, v0

    goto :goto_b

    :catch_6
    move-object p1, v0

    .line 440
    :catch_7
    :goto_6
    :try_start_7
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->reset()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v0, :cond_7

    .line 445
    :try_start_8
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_7

    :catch_8
    nop

    :cond_7
    :goto_7
    if-eqz p1, :cond_9

    goto :goto_5

    :catch_9
    move-object p1, v0

    .line 435
    :catch_a
    :goto_8
    :try_start_9
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->reset()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    if-eqz v0, :cond_8

    .line 445
    :try_start_a
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_b

    goto :goto_9

    :catch_b
    nop

    :cond_8
    :goto_9
    if-eqz p1, :cond_9

    goto :goto_5

    :catch_c
    :cond_9
    :goto_a
    return-void

    :catchall_3
    move-exception v1

    move-object v6, v1

    move-object v1, p1

    move-object p1, v6

    :goto_b
    if-eqz v0, :cond_a

    :try_start_b
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_d

    goto :goto_c

    :catch_d
    nop

    :cond_a
    :goto_c
    if-eqz v1, :cond_b

    .line 452
    :try_start_c
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_e

    .line 455
    :catch_e
    :cond_b
    throw p1
.end method

.method private saveData(Landroid/content/Context;)V
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "registed_documentsize.bin"

    const/4 v2, 0x0

    .line 341
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    .line 348
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "version"

    const/4 v2, 0x1

    .line 350
    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "DocumentSizeInfoList"

    .line 351
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-static {v2}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->makeDocumentSizeInfoList(Ljava/util/LinkedList;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "currentDocumentSize"

    .line 352
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 355
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 371
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 360
    :try_start_2
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    goto :goto_0

    :goto_1
    if-eqz v0, :cond_0

    .line 371
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 374
    :catch_1
    :cond_0
    throw p1

    :catch_2
    if-eqz v0, :cond_1

    goto :goto_0

    :catch_3
    :cond_1
    :goto_2
    return-void
.end method


# virtual methods
.method public add(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 133
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 134
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 135
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->saveData(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public delete(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;I)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 158
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    if-lez p2, :cond_0

    .line 160
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {p1, p2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    goto :goto_0

    .line 162
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result p1

    if-lez p1, :cond_1

    .line 163
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 166
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    :cond_2
    :goto_0
    return-void
.end method

.method public deleteAll()V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, v0}, Ljava/util/LinkedList;->removeAll(Ljava/util/Collection;)Z

    const/4 v0, 0x0

    .line 173
    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    return-void
.end method

.method public getCurrentDocumentSize()Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    return-object v0
.end method

.method public getCurrentDocumentSizePosition()I
    .locals 4

    .line 293
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    .line 294
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 295
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 296
    iget-object v3, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    if-ne v2, v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getItem(I)Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    return-object p1
.end method

.method public getItem(Ljava/lang/String;)Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;
    .locals 3

    .line 208
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    const/4 v1, 0x0

    .line 209
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 210
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 211
    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    return-object v1
.end method

.method public getIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;",
            ">;"
        }
    .end annotation

    .line 309
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getRegistedList()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    return-object v0
.end method

.method public indexOf(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)I
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 321
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public isExistDocumentSizeName(Ljava/lang/String;I)Z
    .locals 1

    .line 222
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getItem(Ljava/lang/String;)Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-eq p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isSelected(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)Z
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 243
    :cond_0
    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public reset()V
    .locals 6

    .line 177
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->initData()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 178
    :goto_0
    sget v2, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->DEFAULT_PAPER_SIZE_COUNT:I

    if-ge v1, v2, :cond_0

    .line 179
    new-instance v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-direct {v2}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;-><init>()V

    .line 180
    iget-object v3, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_NAME:[I

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setDocSizeName(Ljava/lang/String;)V

    .line 181
    sget-object v3, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_SCALE:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setScaleId(I)V

    .line 182
    sget-object v3, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_WIDTH:[D

    aget-wide v4, v3, v1

    invoke-virtual {v2, v4, v5}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setWidth(D)V

    .line 183
    sget-object v3, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_HEIGHT:[D

    aget-wide v4, v3, v1

    invoke-virtual {v2, v4, v5}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setHeight(D)V

    .line 184
    sget-object v3, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->PAPER_SIZE_ID:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setPaperId(I)V

    .line 185
    sget-object v3, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->DOCSIZE_NAME_ID:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setDocSizeNameId(I)V

    .line 186
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->add(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 188
    :cond_0
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->select(I)V

    return-void
.end method

.method public select(I)V
    .locals 4

    if-gez p1, :cond_0

    const/4 p1, 0x0

    .line 252
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 260
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->saveData(Landroid/content/Context;)V

    return-void

    .line 256
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 257
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-object v3, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, p1

    const-string p1, "position <%d> learger than list size <%s>"

    .line 256
    invoke-static {v1, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public select(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 276
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 279
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    goto :goto_0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 282
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 284
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->saveData(Landroid/content/Context;)V

    return-void

    .line 274
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "DocumentSizeInfo is null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public size()I
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public storeData()V
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->saveData(Landroid/content/Context;)V

    return-void
.end method

.method public update(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;I)V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->update(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V

    .line 149
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mDocumentSizeList:Ljava/util/LinkedList;

    invoke-virtual {p1, p2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mCurrentDocumentSize:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 150
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->saveData(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

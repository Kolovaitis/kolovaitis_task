.class Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;
.super Ljava/lang/Object;
.source "PreviewPosition.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/printlayout/PreviewPosition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PreviewRect"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mCx:I

.field private mCy:I

.field private mRotation:I

.field private mScale:D

.field private mScaledHeight:I

.field private mScaledWidth:I

.field private mX1Height:I

.field private mX1Width:I


# direct methods
.method constructor <init>()V
    .locals 2

    .line 621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 639
    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScale:D

    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 2

    .line 682
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getTop()I

    move-result v0

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledHeight:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getHeight()I
    .locals 1

    .line 691
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledHeight:I

    return v0
.end method

.method public getLeft()I
    .locals 2

    .line 670
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mCx:I

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledWidth:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method public getRight()I
    .locals 2

    .line 674
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledWidth:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getRotation()I
    .locals 1

    .line 724
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mRotation:I

    return v0
.end method

.method public getScale()D
    .locals 2

    .line 711
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScale:D

    return-wide v0
.end method

.method public getTop()I
    .locals 2

    .line 678
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mCy:I

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledHeight:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method public getWidth()I
    .locals 1

    .line 687
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledWidth:I

    return v0
.end method

.method public move(II)V
    .locals 1

    .line 715
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mCx:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mCx:I

    .line 716
    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mCy:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mCy:I

    return-void
.end method

.method public resetRotate()V
    .locals 2

    const/4 v0, 0x0

    .line 741
    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mRotation:I

    .line 743
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScale:D

    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setScale(D)V

    return-void
.end method

.method public rotateRight90()V
    .locals 2

    .line 732
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mRotation:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mRotation:I

    .line 733
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mRotation:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    .line 734
    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mRotation:I

    .line 737
    :cond_0
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScale:D

    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setScale(D)V

    return-void
.end method

.method public final setBottom(I)V
    .locals 1

    .line 666
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledHeight:I

    sub-int/2addr p1, v0

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setTop(I)V

    return-void
.end method

.method public final setLeft(I)V
    .locals 1

    .line 654
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledWidth:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mCx:I

    return-void
.end method

.method public final setRight(I)V
    .locals 1

    .line 658
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledWidth:I

    sub-int/2addr p1, v0

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setLeft(I)V

    return-void
.end method

.method public setScale(D)V
    .locals 2

    .line 699
    iput-wide p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScale:D

    .line 700
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mX1Width:I

    int-to-double v0, v0

    mul-double v0, v0, p1

    double-to-int v0, v0

    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledWidth:I

    .line 701
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mX1Height:I

    int-to-double v0, v0

    mul-double v0, v0, p1

    double-to-int p1, v0

    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledHeight:I

    .line 703
    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mRotation:I

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 704
    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledWidth:I

    .line 705
    iget p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledHeight:I

    iput p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledWidth:I

    .line 706
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledHeight:I

    :cond_0
    return-void
.end method

.method public final setTop(I)V
    .locals 1

    .line 662
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScaledHeight:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mCy:I

    return-void
.end method

.method public final setX1Size(II)V
    .locals 0

    .line 646
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mX1Width:I

    .line 647
    iput p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mX1Height:I

    .line 650
    iget-wide p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->mScale:D

    invoke-virtual {p0, p1, p2}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setScale(D)V

    return-void
.end method

.class Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;
.super Ljava/lang/Object;
.source "ManuscriptSize.java"

# interfaces
.implements Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;


# instance fields
.field private mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

.field private mHeight:D

.field private mScale:I

.field private mWidth:D


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V
    .locals 2

    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 319
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getWidth()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mWidth:D

    .line 320
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getHeight()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mHeight:D

    .line 321
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getScaleId()I

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mScale:I

    return-void
.end method


# virtual methods
.method public displayLength()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 387
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    .line 389
    :cond_2
    check-cast p1, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;

    .line 390
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    if-nez v2, :cond_3

    .line 391
    iget-object v2, p1, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    if-eqz v2, :cond_4

    return v1

    .line 393
    :cond_3
    iget-object v3, p1, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    .line 395
    :cond_4
    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mHeight:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p1, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mHeight:D

    .line 396
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-eqz v6, :cond_5

    return v1

    .line 398
    :cond_5
    iget v2, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mScale:I

    iget v3, p1, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mScale:I

    if-eq v2, v3, :cond_6

    return v1

    .line 400
    :cond_6
    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mWidth:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p1, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mWidth:D

    .line 401
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long p1, v2, v4

    if-eqz p1, :cond_7

    return v1

    :cond_7
    return v0
.end method

.method public getBasePixelSize(I)Landroid/graphics/Point;
    .locals 3

    .line 327
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mWidth:D

    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->lengthToPixel(DI)I

    move-result v0

    .line 328
    iget-wide v1, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mHeight:D

    invoke-virtual {p0, v1, v2, p1}, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->lengthToPixel(DI)I

    move-result p1

    .line 330
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v0, p1}, Landroid/graphics/Point;-><init>(II)V

    return-object v1
.end method

.method public getCustomDocumentSize(Landroid/content/Context;)Landroid/graphics/PointF;
    .locals 3

    .line 359
    new-instance p1, Landroid/graphics/PointF;

    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mWidth:D

    double-to-float v0, v0

    iget-wide v1, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mHeight:D

    double-to-float v1, v1

    invoke-direct {p1, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object p1
.end method

.method public getSizeName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUnitType(Landroid/content/Context;)I
    .locals 1

    .line 344
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getScaleId()I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    return v0

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method protected lengthToPixel(DI)I
    .locals 5

    .line 368
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getScaleId()I

    move-result v0

    const-wide/high16 v1, 0x3fe0000000000000L    # 0.5

    packed-switch v0, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    :pswitch_0
    int-to-double v3, p3

    mul-double p1, p1, v3

    add-double/2addr p1, v1

    double-to-int p1, p1

    return p1

    :pswitch_1
    int-to-double v3, p3

    mul-double p1, p1, v3

    const-wide v3, 0x4039666666666666L    # 25.4

    div-double/2addr p1, v3

    add-double/2addr p1, v1

    double-to-int p1, p1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

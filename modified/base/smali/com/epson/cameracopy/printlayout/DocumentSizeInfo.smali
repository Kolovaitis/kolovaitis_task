.class public Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;
.super Ljava/lang/Object;
.source "DocumentSizeInfo.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DOCSIZE_NAME_USER_DEFINE:I = 0x0

.field private static final DOCUMENT_SIZE_NAME:[I

.field public static final PAPER_A4_HEIGHT:D = 297.0

.field public static final PAPER_A4_WIDTH:D = 210.0

.field public static final PAPER_AUTO:I = -0x1

.field public static final PAPER_LETTER_HEIGHT:D = 11.0

.field public static final PAPER_LETTER_WIDTH:D = 8.5

.field public static final PAPER_USER_DEFINE:I = -0x2

.field public static final SCALE_TYPE_INCH:I = 0x2

.field public static final SCALE_TYPE_MM:I = 0x1

.field private static final serialVersionUID:J = 0x433dca535522dd23L


# instance fields
.field private mDocSizeName:Ljava/lang/String;

.field private mDocSizeNameId:I

.field private mHeight:D

.field private mPaperId:I

.field private mScaleId:I

.field private mWidth:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x12

    .line 33
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->DOCUMENT_SIZE_NAME:[I

    .line 157
    new-instance v0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo$1;

    invoke-direct {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo$1;-><init>()V

    sput-object v0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0e040f
        0x7f0e040f
        0x7f0e040b
        0x7f0e040c
        0x7f0e040d
        0x7f0e040e
        0x7f0e0410
        0x7f0e0411
        0x7f0e0412
        0x7f0e0417
        0x7f0e0416
        0x7f0e0419
        0x7f0e0409
        0x7f0e0415
        0x7f0e040a
        0x7f0e0414
        0x7f0e0413
        0x7f0e0418
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    .line 170
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mScaleId:I

    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mWidth:D

    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mHeight:D

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mPaperId:I

    .line 174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/epson/cameracopy/printlayout/DocumentSizeInfo$1;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getDocumentSizeInfoList(Lorg/json/JSONObject;)Ljava/util/LinkedList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/LinkedList<",
            "Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 183
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    const-string v1, "DocumentSizeInfoList"

    .line 186
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p0

    const/4 v1, 0x0

    .line 188
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 189
    new-instance v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-direct {v2}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;-><init>()V

    .line 193
    :try_start_0
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "mDocSizeName"

    .line 195
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    const-string v4, "mScaleId"

    .line 196
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mScaleId:I

    const-string v4, "mWidth"

    .line 197
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mWidth:D

    const-string v4, "mHeight"

    .line 198
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mHeight:D

    const-string v4, "mPaperId"

    .line 199
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mPaperId:I

    const-string v4, "mDocSizeNameId"

    .line 200
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    .line 202
    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 204
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static makeDocumentSizeInfoList(Ljava/util/LinkedList;)Lorg/json/JSONArray;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList<",
            "Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;",
            ">;)",
            "Lorg/json/JSONArray;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 218
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    const/4 v1, 0x0

    .line 220
    :goto_0
    invoke-virtual {p0}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 221
    invoke-virtual {p0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 222
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v4, "mDocSizeName"

    .line 225
    iget-object v5, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v4, "mScaleId"

    .line 226
    iget v5, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mScaleId:I

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v4, "mWidth"

    .line 227
    iget-wide v5, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mWidth:D

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v4, "mHeight"

    .line 228
    iget-wide v5, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mHeight:D

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v4, "mPaperId"

    .line 229
    iget v5, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mPaperId:I

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v4, "mDocSizeNameId"

    .line 230
    iget v2, v2, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 232
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 235
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 125
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    .line 130
    :cond_2
    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 131
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 132
    iget-object p1, p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    if-eqz p1, :cond_4

    return v1

    .line 136
    :cond_3
    iget-object p1, p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    return v1

    :cond_4
    return v0
.end method

.method public getDocSizeName()Ljava/lang/String;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    return-object v0
.end method

.method public getDocSizeName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .line 62
    iget v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    if-lez v0, :cond_1

    sget-object v1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->DOCUMENT_SIZE_NAME:[I

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    if-le v0, v2, :cond_0

    goto :goto_0

    .line 66
    :cond_0
    aget v0, v1, v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 63
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    return-object p1
.end method

.method public getDocSizeNameId()I
    .locals 1

    .line 103
    iget v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    return v0
.end method

.method public getHeight()D
    .locals 2

    .line 89
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mHeight:D

    return-wide v0
.end method

.method public getPaperId()I
    .locals 1

    .line 96
    iget v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mPaperId:I

    return v0
.end method

.method public getScaleId()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mScaleId:I

    return v0
.end method

.method public getWidth()D
    .locals 2

    .line 82
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mWidth:D

    return-wide v0
.end method

.method public setDocSizeName(Ljava/lang/String;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    return-void
.end method

.method public setDocSizeNameId(I)V
    .locals 0

    .line 106
    iput p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    return-void
.end method

.method public setHeight(D)V
    .locals 0

    .line 92
    iput-wide p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mHeight:D

    return-void
.end method

.method public setPaperId(I)V
    .locals 0

    .line 99
    iput p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mPaperId:I

    return-void
.end method

.method public setScaleId(I)V
    .locals 0

    .line 78
    iput p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mScaleId:I

    return-void
.end method

.method public setWidth(D)V
    .locals 0

    .line 85
    iput-wide p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mWidth:D

    return-void
.end method

.method public update(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V
    .locals 2

    .line 110
    iget-object v0, p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    .line 111
    iget v0, p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mScaleId:I

    iput v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mScaleId:I

    .line 112
    iget-wide v0, p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mWidth:D

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mWidth:D

    .line 113
    iget-wide v0, p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mHeight:D

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mHeight:D

    .line 114
    iget p1, p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    iput p1, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 149
    iget-object p2, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    iget p2, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mScaleId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 151
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mWidth:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 152
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mHeight:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 153
    iget p2, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mPaperId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    iget p2, p0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->mDocSizeNameId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.class public Lcom/epson/cameracopy/printlayout/ManuscriptSize;
.super Ljava/lang/Object;
.source "ManuscriptSize.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field public static final INCH_MM:D = 25.4

.field public static final SCALE_TYPE_INCH:I = 0x2

.field public static final SCALE_TYPE_MM:I = 0x1


# instance fields
.field private mBaseDpi:I

.field private mManuscriptFileName:Ljava/lang/String;

.field private mManuscriptPixelSize:Landroid/graphics/Point;

.field private mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;Ljava/lang/String;I)V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput p3, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mBaseDpi:I

    .line 83
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result p3

    packed-switch p3, :pswitch_data_0

    .line 93
    new-instance p3, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;

    invoke-direct {p3, p1}, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;-><init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V

    iput-object p3, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    goto :goto_0

    .line 86
    :pswitch_0
    new-instance p3, Lcom/epson/cameracopy/printlayout/AutoManuscriptSize;

    invoke-direct {p3, p1}, Lcom/epson/cameracopy/printlayout/AutoManuscriptSize;-><init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V

    iput-object p3, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    goto :goto_0

    .line 89
    :pswitch_1
    new-instance p3, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;

    invoke-direct {p3, p1}, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;-><init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V

    iput-object p3, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    .line 96
    :goto_0
    invoke-direct {p0, p2}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->setManuscriptPixlSize(Ljava/lang/String;)V

    .line 98
    iput-object p2, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptFileName:Ljava/lang/String;

    return-void

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static calcRate(IIII)D
    .locals 4

    int-to-double v0, p0

    int-to-double v2, p2

    div-double/2addr v0, v2

    int-to-double p0, p1

    int-to-double p2, p3

    div-double/2addr p0, p2

    .line 175
    invoke-static {v0, v1, p0, p1}, Ljava/lang/Math;->min(DD)D

    move-result-wide p0

    return-wide p0
.end method

.method public static getPixelToPhysicalSizeScale(II)D
    .locals 2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    int-to-double p0, p0

    div-double/2addr v0, p0

    return-wide v0

    :cond_0
    const-wide v0, 0x4039666666666666L    # 25.4

    int-to-double p0, p0

    div-double/2addr v0, p0

    return-wide v0
.end method

.method public static load(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/cameracopy/printlayout/ManuscriptSize;
    .locals 2

    .line 192
    invoke-static {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getCurrentDocumentSize()Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    move-result-object v0

    .line 196
    invoke-static {p0}, Lcom/epson/cameracopy/alt/AdditionalPrinterInfo;->getCurrentPrintModeResolution(Landroid/content/Context;)I

    move-result p0

    .line 198
    new-instance v1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    invoke-direct {v1, v0, p1, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;-><init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;Ljava/lang/String;I)V

    return-object v1
.end method

.method private setManuscriptPixlSize(Ljava/lang/String;)V
    .locals 7

    .line 237
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    .line 239
    invoke-static {p1}, Lepson/common/ImageUtil;->getImageSize(Ljava/lang/String;)Landroid/graphics/Point;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    iget v1, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mBaseDpi:I

    invoke-interface {v0, v1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;->getBasePixelSize(I)Landroid/graphics/Point;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 250
    :cond_1
    iget v1, p1, Landroid/graphics/Point;->x:I

    .line 251
    iget p1, p1, Landroid/graphics/Point;->y:I

    sub-int v2, p1, v1

    .line 253
    iget v3, v0, Landroid/graphics/Point;->y:I

    iget v4, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    mul-int v2, v2, v3

    if-gez v2, :cond_2

    goto :goto_0

    :cond_2
    move v6, v1

    move v1, p1

    move p1, v6

    .line 262
    :goto_0
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v0, p1, v1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->calcRate(IIII)D

    move-result-wide v2

    .line 264
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    int-to-double v4, p1

    mul-double v4, v4, v2

    double-to-int p1, v4

    iput p1, v0, Landroid/graphics/Point;->x:I

    int-to-double v4, v1

    mul-double v2, v2, v4

    double-to-int p1, v2

    .line 265
    iput p1, v0, Landroid/graphics/Point;->y:I

    return-void
.end method


# virtual methods
.method public displayLength()Z
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    invoke-interface {v0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;->displayLength()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 275
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    .line 277
    :cond_2
    check-cast p1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 278
    iget v2, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mBaseDpi:I

    iget v3, p1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mBaseDpi:I

    if-eq v2, v3, :cond_3

    return v1

    .line 280
    :cond_3
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptFileName:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 281
    iget-object v2, p1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptFileName:Ljava/lang/String;

    if-eqz v2, :cond_5

    return v1

    .line 283
    :cond_4
    iget-object v3, p1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    return v1

    .line 285
    :cond_5
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    if-nez v2, :cond_6

    .line 286
    iget-object v2, p1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    if-eqz v2, :cond_7

    return v1

    .line 288
    :cond_6
    iget-object v3, p1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    return v1

    .line 290
    :cond_7
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    if-nez v2, :cond_8

    .line 291
    iget-object p1, p1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    if-eqz p1, :cond_9

    return v1

    .line 293
    :cond_8
    iget-object p1, p1, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    return v1

    :cond_9
    return v0
.end method

.method public getDocumentSize(Landroid/content/Context;)Landroid/graphics/PointF;
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    instance-of v1, v0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;

    if-eqz v1, :cond_0

    .line 133
    check-cast v0, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/CustomManuscriptSize;->getCustomDocumentSize(Landroid/content/Context;)Landroid/graphics/PointF;

    move-result-object p1

    return-object p1

    .line 136
    :cond_0
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getPhysicalSize(Landroid/content/Context;)Landroid/graphics/PointF;

    move-result-object p1

    return-object p1
.end method

.method public getPhysicalSize(Landroid/content/Context;)Landroid/graphics/PointF;
    .locals 5

    .line 116
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    invoke-interface {v0, p1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;->getUnitType(Landroid/content/Context;)I

    move-result p1

    .line 117
    iget v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mBaseDpi:I

    invoke-static {v0, p1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getPixelToPhysicalSizeScale(II)D

    move-result-wide v0

    .line 119
    new-instance p1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    mul-double v2, v2, v0

    double-to-float v2, v2

    iget-object v3, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-double v3, v3

    mul-double v3, v3, v0

    double-to-float v0, v3

    invoke-direct {p1, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    return-object p1
.end method

.method public getPixelHeight()I
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method

.method public getPixelWidth()I
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptPixelSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method

.method public getSizeName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    invoke-interface {v0, p1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;->getSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUnitStr(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 209
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getUnitType(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const v0, 0x7f0e0539

    .line 211
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const v0, 0x7f0e053a

    .line 214
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUnitType(Landroid/content/Context;)I
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->mManuscriptTypeAdapter:Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;

    invoke-interface {v0, p1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;->getUnitType(Landroid/content/Context;)I

    move-result p1

    return p1
.end method

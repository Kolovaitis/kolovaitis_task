.class public Lcom/epson/cameracopy/printlayout/ImageAndLayout;
.super Ljava/lang/Object;
.source "ImageAndLayout.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/epson/cameracopy/printlayout/ImageAndLayout;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mInterpolation:I

.field private mIsPaperLandscape:Z

.field private mOffsetX:D

.field private mOffsetY:D

.field private mOpenCvExifRotation:I

.field private mOpenCvExifRotationCancel:Z

.field private mOrgFilename:Ljava/lang/String;

.field private mPrintTargetHeight:D

.field private mPrintTargetWidth:D

.field private mPrintableAreaHeight:I

.field private mPrintableAreaWidth:I

.field private mRotation:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "opencv_java3"

    .line 83
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 112
    new-instance v0, Lcom/epson/cameracopy/printlayout/ImageAndLayout$1;

    invoke-direct {v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout$1;-><init>()V

    sput-object v0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 78
    iput v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mInterpolation:I

    const/4 v0, 0x1

    .line 94
    iput-boolean v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mIsPaperLandscape:Z

    .line 95
    iput-boolean v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotationCancel:Z

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 78
    iput v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mInterpolation:I

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintableAreaWidth:I

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintableAreaHeight:I

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOrgFilename:Ljava/lang/String;

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintTargetWidth:D

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintTargetHeight:D

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOffsetX:D

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOffsetY:D

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mRotation:I

    const/4 v0, 0x1

    .line 135
    new-array v1, v0, [Z

    .line 136
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    const/4 p1, 0x0

    .line 137
    aget-boolean p1, v1, p1

    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mIsPaperLandscape:Z

    .line 139
    iput-boolean v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotationCancel:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/epson/cameracopy/printlayout/ImageAndLayout$1;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private affineBand(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIILjava/io/File;)Z
    .locals 23

    move-object/from16 v1, p0

    .line 566
    invoke-virtual/range {p2 .. p2}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v9

    .line 567
    new-instance v10, Lorg/opencv/core/Mat;

    invoke-direct {v10}, Lorg/opencv/core/Mat;-><init>()V

    .line 568
    new-instance v5, Lorg/opencv/core/Size;

    move/from16 v0, p3

    int-to-double v2, v0

    move/from16 v0, p4

    int-to-double v6, v0

    invoke-direct {v5, v2, v3, v6, v7}, Lorg/opencv/core/Size;-><init>(DD)V

    const/4 v0, 0x2

    const/4 v11, 0x1

    .line 570
    invoke-virtual {v9, v11, v0}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v2

    const/4 v12, 0x0

    aget-wide v3, v2, v12

    .line 572
    new-array v2, v11, [D

    move/from16 v6, p5

    int-to-double v6, v6

    sub-double/2addr v3, v6

    aput-wide v3, v2, v12

    invoke-virtual {v9, v11, v0, v2}, Lorg/opencv/core/Mat;->put(II[D)I

    const/4 v13, 0x0

    .line 577
    :try_start_0
    iget v6, v1, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mInterpolation:I

    const/4 v7, 0x0

    new-instance v0, Lorg/opencv/core/Scalar;

    const-wide v15, 0x406fe00000000000L    # 255.0

    const-wide v17, 0x406fe00000000000L    # 255.0

    const-wide v19, 0x406fe00000000000L    # 255.0

    const-wide v21, 0x406fe00000000000L    # 255.0

    move-object v14, v0

    invoke-direct/range {v14 .. v22}, Lorg/opencv/core/Scalar;-><init>(DDDD)V

    move-object/from16 v2, p1

    move-object v3, v10

    move-object v4, v9

    move-object v8, v0

    invoke-static/range {v2 .. v8}, Lorg/opencv/imgproc/Imgproc;->warpAffine(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;IILorg/opencv/core/Scalar;)V

    .line 581
    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/Mat;->type()I

    move-result v0

    sget v2, Lorg/opencv/core/CvType;->CV_8UC1:I

    if-ne v0, v2, :cond_1

    .line 582
    invoke-virtual {v1, v10}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->make24BitMatFromGrayMat(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;

    move-result-object v13

    .line 584
    invoke-virtual/range {p6 .. p6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v13}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    .line 596
    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    .line 597
    invoke-virtual {v10}, Lorg/opencv/core/Mat;->release()V

    if-eqz v13, :cond_0

    .line 599
    invoke-virtual {v13}, Lorg/opencv/core/Mat;->release()V

    :cond_0
    return v12

    .line 588
    :cond_1
    :try_start_1
    invoke-virtual/range {p6 .. p6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v10}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    .line 596
    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    .line 597
    invoke-virtual {v10}, Lorg/opencv/core/Mat;->release()V

    return v12

    .line 596
    :cond_2
    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    .line 597
    invoke-virtual {v10}, Lorg/opencv/core/Mat;->release()V

    if-eqz v13, :cond_3

    .line 599
    invoke-virtual {v13}, Lorg/opencv/core/Mat;->release()V

    :cond_3
    return v11

    :catchall_0
    move-exception v0

    .line 596
    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    .line 597
    invoke-virtual {v10}, Lorg/opencv/core/Mat;->release()V

    if-eqz v13, :cond_4

    .line 599
    invoke-virtual {v13}, Lorg/opencv/core/Mat;->release()V

    :cond_4
    throw v0

    :catch_0
    nop

    .line 596
    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    .line 597
    invoke-virtual {v10}, Lorg/opencv/core/Mat;->release()V

    if-eqz v13, :cond_5

    .line 599
    invoke-virtual {v13}, Lorg/opencv/core/Mat;->release()V

    :cond_5
    return v12
.end method

.method static calc255Alpha(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 780
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 782
    new-instance v1, Lorg/opencv/core/Scalar;

    const-wide v2, -0x3f90200000000000L    # -255.0

    invoke-direct {v1, v2, v3}, Lorg/opencv/core/Scalar;-><init>(D)V

    invoke-static {p0, v1, v0}, Lorg/opencv/core/Core;->multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V

    .line 785
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    .line 786
    new-instance v1, Lorg/opencv/core/Scalar;

    const-wide v2, 0x406fe00000000000L    # 255.0

    invoke-direct {v1, v2, v3}, Lorg/opencv/core/Scalar;-><init>(D)V

    invoke-static {v0, v1, p0}, Lorg/opencv/core/Core;->add(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V

    .line 787
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    return-object p0
.end method

.method private static convertRgbaToRgb(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 868
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 869
    invoke-static {p0, v0}, Lorg/opencv/core/Core;->split(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 870
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->release()V

    .line 873
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    const/4 v1, 0x3

    .line 874
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32FC1:I

    const-wide v4, 0x3f70101010101010L    # 0.00392156862745098

    invoke-virtual {v2, p0, v3, v4, v5}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;ID)V

    .line 876
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/opencv/core/Mat;

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 877
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 879
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    .line 880
    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 882
    :cond_0
    new-instance v1, Lorg/opencv/core/Mat;

    invoke-direct {v1}, Lorg/opencv/core/Mat;-><init>()V

    .line 883
    invoke-static {v2, v1}, Lorg/opencv/core/Core;->merge(Ljava/util/List;Lorg/opencv/core/Mat;)V

    .line 885
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->release()V

    .line 888
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    .line 889
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    .line 890
    invoke-static {v0, p0}, Lorg/opencv/core/Core;->merge(Ljava/util/List;Lorg/opencv/core/Mat;)V

    .line 892
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Mat;

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    const/4 v3, 0x1

    .line 893
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Mat;

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    const/4 v3, 0x2

    .line 894
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 897
    sget v0, Lorg/opencv/core/CvType;->CV_32FC1:I

    invoke-virtual {p0, v2, v0}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;I)V

    .line 898
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->release()V

    .line 901
    invoke-virtual {v2, v1}, Lorg/opencv/core/Mat;->mul(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;

    move-result-object p0

    .line 902
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 905
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 906
    sget v1, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-virtual {p0, v0, v1}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;I)V

    .line 907
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->release()V

    return-object v0
.end method

.method static convertRgbaToRgbTrWhite(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 803
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 804
    invoke-static {p0, v0}, Lorg/opencv/core/Core;->split(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 805
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->release()V

    .line 808
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    const/4 v1, 0x3

    .line 809
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32FC1:I

    const-wide v4, 0x3f70101010101010L    # 0.00392156862745098

    invoke-virtual {v2, p0, v3, v4, v5}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;ID)V

    .line 811
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/opencv/core/Mat;

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 812
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 814
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    .line 815
    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 817
    :cond_0
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .line 818
    invoke-static {v2, v4}, Lorg/opencv/core/Core;->merge(Ljava/util/List;Lorg/opencv/core/Mat;)V

    .line 820
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    .line 821
    new-instance v5, Lorg/opencv/core/Mat;

    invoke-direct {v5}, Lorg/opencv/core/Mat;-><init>()V

    .line 822
    invoke-static {v0, v2}, Lorg/opencv/core/Core;->merge(Ljava/util/List;Lorg/opencv/core/Mat;)V

    .line 824
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Mat;

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    const/4 v3, 0x1

    .line 825
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Mat;

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    const/4 v3, 0x2

    .line 826
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 829
    sget v0, Lorg/opencv/core/CvType;->CV_32FC1:I

    invoke-virtual {v2, v5, v0}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;I)V

    .line 830
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 833
    invoke-virtual {v5, v4}, Lorg/opencv/core/Mat;->mul(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 834
    invoke-virtual {v5}, Lorg/opencv/core/Mat;->release()V

    .line 837
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    .line 838
    sget v3, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-virtual {v0, v2, v3}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;I)V

    .line 839
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 843
    invoke-static {p0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->calc255Alpha(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;

    move-result-object p0

    .line 846
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 847
    sget v3, Lorg/opencv/core/CvType;->CV_8UC1:I

    invoke-virtual {p0, v0, v3}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;I)V

    .line 849
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    const/16 v3, 0x8

    .line 850
    invoke-static {v0, p0, v3, v1}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V

    .line 853
    invoke-static {v2, p0, v2}, Lorg/opencv/core/Core;->add(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V

    return-object v2
.end method

.method private getImageRotation(I)I
    .locals 1

    .line 932
    iget-boolean v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotationCancel:Z

    if-eqz v0, :cond_0

    .line 934
    iget v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotation:I

    sub-int/2addr p1, v0

    :cond_0
    if-gez p1, :cond_1

    neg-int v0, p1

    .line 937
    div-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr p1, v0

    .line 941
    :cond_1
    rem-int/lit8 p1, p1, 0x4

    return p1
.end method

.method private getTransMat(Lorg/opencv/core/Mat;I[I)Lorg/opencv/core/Mat;
    .locals 11

    const/4 v0, 0x2

    .line 535
    new-array v0, v0, [I

    and-int/lit8 v1, p2, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    .line 537
    aget v1, p3, v3

    aput v1, v0, v2

    .line 538
    aget v1, p3, v2

    aput v1, v0, v3

    goto :goto_0

    .line 540
    :cond_0
    aget v1, p3, v2

    aput v1, v0, v2

    .line 541
    aget v1, p3, v3

    aput v1, v0, v3

    .line 544
    :goto_0
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->getExRate([I)[D

    move-result-object v0

    .line 545
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->getPrintImageSize([D)Lorg/opencv/core/Size;

    move-result-object v4

    .line 547
    iget-wide v5, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOffsetX:D

    aget-wide v1, v0, v2

    mul-double v6, v5, v1

    .line 548
    iget-wide v1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOffsetY:D

    aget-wide v8, v0, v3

    mul-double v8, v8, v1

    .line 550
    iget v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mRotation:I

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->getImageRotation(I)I

    move-result v10

    .line 551
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v5

    invoke-static/range {v4 .. v10}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->getTransformMat(Lorg/opencv/core/Size;Lorg/opencv/core/Size;DDI)Lorg/opencv/core/Mat;

    move-result-object p1

    .line 554
    invoke-virtual {p0, p3, p2, p1}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->getPrintMat([IILorg/opencv/core/Mat;)Lorg/opencv/core/Mat;

    move-result-object p2

    .line 555
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->release()V

    return-object p2
.end method

.method static getTransformMat(Lorg/opencv/core/Size;Lorg/opencv/core/Size;DDI)Lorg/opencv/core/Mat;
    .locals 22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    and-int/lit8 v2, p6, 0x1

    if-nez v2, :cond_0

    .line 371
    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v4, v1, Lorg/opencv/core/Size;->width:D

    div-double/2addr v2, v4

    .line 372
    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v6, v1, Lorg/opencv/core/Size;->height:D

    div-double/2addr v4, v6

    move-wide v8, v2

    goto :goto_0

    .line 375
    :cond_0
    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v4, v1, Lorg/opencv/core/Size;->height:D

    div-double/2addr v2, v4

    .line 376
    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v6, v1, Lorg/opencv/core/Size;->width:D

    div-double/2addr v4, v6

    move-wide v8, v2

    :goto_0
    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    div-double v6, v8, v1

    double-to-int v3, v6

    div-double v1, v4, v1

    double-to-int v1, v1

    and-int/lit8 v2, p6, 0x3

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    packed-switch v2, :pswitch_data_0

    const-wide/16 v10, 0x0

    int-to-double v2, v3

    add-double v2, p2, v2

    const-wide/16 v12, 0x0

    int-to-double v0, v1

    add-double v16, p4, v0

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v2

    move-wide v14, v4

    .line 411
    invoke-static/range {v6 .. v17}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->new3x2Mat(DDDDDD)Lorg/opencv/core/Mat;

    move-result-object v0

    goto :goto_1

    :pswitch_0
    const-wide/16 v10, 0x0

    int-to-double v2, v3

    add-double v2, p2, v2

    neg-double v12, v4

    const-wide/16 v14, 0x0

    .line 386
    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    add-double v4, p4, v4

    int-to-double v0, v1

    sub-double/2addr v4, v0

    sub-double v16, v4, v6

    move-wide v6, v10

    move-wide v10, v2

    invoke-static/range {v6 .. v17}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->new3x2Mat(DDDDDD)Lorg/opencv/core/Mat;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    neg-double v8, v8

    const-wide/16 v10, 0x0

    .line 395
    iget-wide v12, v0, Lorg/opencv/core/Size;->width:D

    add-double v12, p2, v12

    int-to-double v2, v3

    sub-double/2addr v12, v2

    sub-double/2addr v12, v6

    neg-double v3, v4

    iget-wide v14, v0, Lorg/opencv/core/Size;->height:D

    add-double v14, p4, v14

    int-to-double v0, v1

    sub-double/2addr v14, v0

    sub-double/2addr v14, v6

    move-wide v1, v8

    move-wide/from16 v16, v3

    move-wide v3, v10

    move-wide v5, v12

    const-wide/16 v7, 0x0

    move-wide/from16 v9, v16

    move-wide v11, v14

    invoke-static/range {v1 .. v12}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->new3x2Mat(DDDDDD)Lorg/opencv/core/Mat;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const-wide/16 v10, 0x0

    neg-double v12, v8

    .line 403
    iget-wide v8, v0, Lorg/opencv/core/Size;->width:D

    add-double v8, p2, v8

    int-to-double v2, v3

    sub-double/2addr v8, v2

    sub-double v14, v8, v6

    const-wide/16 v18, 0x0

    int-to-double v0, v1

    add-double v20, p4, v0

    move-wide/from16 v16, v4

    invoke-static/range {v10 .. v21}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->new3x2Mat(DDDDDD)Lorg/opencv/core/Mat;

    move-result-object v0

    :goto_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static getTransformMat0(Lorg/opencv/core/Size;Lorg/opencv/core/Size;DDI)Lorg/opencv/core/Mat;
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p1

    .line 250
    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    .line 251
    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    .line 253
    new-instance v0, Lorg/opencv/core/MatOfPoint2f;

    const/4 v6, 0x3

    new-array v7, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    const-wide/16 v9, 0x0

    invoke-direct {v8, v9, v10, v9, v10}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 v11, 0x0

    aput-object v8, v7, v11

    new-instance v8, Lorg/opencv/core/Point;

    iget-wide v12, v1, Lorg/opencv/core/Size;->height:D

    invoke-direct {v8, v9, v10, v12, v13}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 v12, 0x1

    aput-object v8, v7, v12

    new-instance v8, Lorg/opencv/core/Point;

    iget-wide v13, v1, Lorg/opencv/core/Size;->width:D

    invoke-direct {v8, v13, v14, v9, v10}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 v1, 0x2

    aput-object v8, v7, v1

    invoke-direct {v0, v7}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    and-int/lit8 v7, p6, 0x3

    packed-switch v7, :pswitch_data_0

    .line 283
    new-instance v7, Lorg/opencv/core/MatOfPoint2f;

    new-array v6, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    add-double v13, p2, v9

    add-double v9, p4, v9

    invoke-direct {v8, v13, v14, v9, v10}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v11

    new-instance v8, Lorg/opencv/core/Point;

    add-double v4, v4, p4

    invoke-direct {v8, v13, v14, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v12

    new-instance v4, Lorg/opencv/core/Point;

    add-double v2, v2, p2

    invoke-direct {v4, v2, v3, v9, v10}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v4, v6, v1

    invoke-direct {v7, v6}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    goto :goto_0

    .line 262
    :pswitch_0
    new-instance v7, Lorg/opencv/core/MatOfPoint2f;

    new-array v6, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    add-double v13, p2, v9

    add-double v4, v4, p4

    invoke-direct {v8, v13, v14, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v11

    new-instance v8, Lorg/opencv/core/Point;

    add-double v2, v2, p2

    invoke-direct {v8, v2, v3, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v12

    new-instance v2, Lorg/opencv/core/Point;

    add-double v3, p4, v9

    invoke-direct {v2, v13, v14, v3, v4}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v2, v6, v1

    invoke-direct {v7, v6}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    goto :goto_0

    .line 269
    :pswitch_1
    new-instance v7, Lorg/opencv/core/MatOfPoint2f;

    new-array v6, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    add-double v2, v2, p2

    add-double v4, v4, p4

    invoke-direct {v8, v2, v3, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v11

    new-instance v8, Lorg/opencv/core/Point;

    add-double v13, p4, v9

    invoke-direct {v8, v2, v3, v13, v14}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v12

    new-instance v2, Lorg/opencv/core/Point;

    add-double v8, p2, v9

    invoke-direct {v2, v8, v9, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v2, v6, v1

    invoke-direct {v7, v6}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    goto :goto_0

    .line 276
    :pswitch_2
    new-instance v7, Lorg/opencv/core/MatOfPoint2f;

    new-array v6, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    add-double v2, v2, p2

    add-double v13, p4, v9

    invoke-direct {v8, v2, v3, v13, v14}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v11

    new-instance v8, Lorg/opencv/core/Point;

    add-double v9, p2, v9

    invoke-direct {v8, v9, v10, v13, v14}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v12

    new-instance v8, Lorg/opencv/core/Point;

    add-double v4, v4, p4

    invoke-direct {v8, v2, v3, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v1

    invoke-direct {v7, v6}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    .line 289
    :goto_0
    invoke-static {v0, v7}, Lorg/opencv/imgproc/Imgproc;->getAffineTransform(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/MatOfPoint2f;)Lorg/opencv/core/Mat;

    move-result-object v1

    .line 290
    invoke-virtual {v0}, Lorg/opencv/core/MatOfPoint2f;->release()V

    .line 291
    invoke-virtual {v7}, Lorg/opencv/core/MatOfPoint2f;->release()V

    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static getTransformMat1(Lorg/opencv/core/Size;Lorg/opencv/core/Size;DDI)Lorg/opencv/core/Mat;
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p1

    .line 303
    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    .line 304
    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    .line 307
    iget-wide v6, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v6, v1, Lorg/opencv/core/Size;->width:D

    .line 308
    iget-wide v6, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v6, v1, Lorg/opencv/core/Size;->height:D

    .line 313
    new-instance v0, Lorg/opencv/core/MatOfPoint2f;

    const/4 v6, 0x3

    new-array v7, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    const-wide/16 v9, 0x0

    invoke-direct {v8, v9, v10, v9, v10}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 v11, 0x0

    aput-object v8, v7, v11

    new-instance v8, Lorg/opencv/core/Point;

    iget-wide v12, v1, Lorg/opencv/core/Size;->height:D

    invoke-direct {v8, v9, v10, v12, v13}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 v12, 0x1

    aput-object v8, v7, v12

    new-instance v8, Lorg/opencv/core/Point;

    iget-wide v13, v1, Lorg/opencv/core/Size;->width:D

    invoke-direct {v8, v13, v14, v9, v10}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 v1, 0x2

    aput-object v8, v7, v1

    invoke-direct {v0, v7}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    and-int/lit8 v7, p6, 0x3

    packed-switch v7, :pswitch_data_0

    .line 343
    new-instance v7, Lorg/opencv/core/MatOfPoint2f;

    new-array v6, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    add-double v13, p2, v9

    add-double v9, p4, v9

    invoke-direct {v8, v13, v14, v9, v10}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v11

    new-instance v8, Lorg/opencv/core/Point;

    add-double v4, v4, p4

    invoke-direct {v8, v13, v14, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v12

    new-instance v4, Lorg/opencv/core/Point;

    add-double v2, v2, p2

    invoke-direct {v4, v2, v3, v9, v10}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v4, v6, v1

    invoke-direct {v7, v6}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    goto :goto_0

    .line 322
    :pswitch_0
    new-instance v7, Lorg/opencv/core/MatOfPoint2f;

    new-array v6, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    add-double v13, p2, v9

    add-double v4, v4, p4

    invoke-direct {v8, v13, v14, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v11

    new-instance v8, Lorg/opencv/core/Point;

    add-double v2, v2, p2

    invoke-direct {v8, v2, v3, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v12

    new-instance v2, Lorg/opencv/core/Point;

    add-double v3, p4, v9

    invoke-direct {v2, v13, v14, v3, v4}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v2, v6, v1

    invoke-direct {v7, v6}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    goto :goto_0

    .line 329
    :pswitch_1
    new-instance v7, Lorg/opencv/core/MatOfPoint2f;

    new-array v6, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    add-double v2, v2, p2

    add-double v4, v4, p4

    invoke-direct {v8, v2, v3, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v11

    new-instance v8, Lorg/opencv/core/Point;

    add-double v13, p4, v9

    invoke-direct {v8, v2, v3, v13, v14}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v12

    new-instance v2, Lorg/opencv/core/Point;

    add-double v8, p2, v9

    invoke-direct {v2, v8, v9, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v2, v6, v1

    invoke-direct {v7, v6}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    goto :goto_0

    .line 336
    :pswitch_2
    new-instance v7, Lorg/opencv/core/MatOfPoint2f;

    new-array v6, v6, [Lorg/opencv/core/Point;

    new-instance v8, Lorg/opencv/core/Point;

    add-double v2, v2, p2

    add-double v13, p4, v9

    invoke-direct {v8, v2, v3, v13, v14}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v11

    new-instance v8, Lorg/opencv/core/Point;

    add-double v9, p2, v9

    invoke-direct {v8, v9, v10, v13, v14}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v12

    new-instance v8, Lorg/opencv/core/Point;

    add-double v4, v4, p4

    invoke-direct {v8, v2, v3, v4, v5}, Lorg/opencv/core/Point;-><init>(DD)V

    aput-object v8, v6, v1

    invoke-direct {v7, v6}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    .line 349
    :goto_0
    invoke-static {v0, v7}, Lorg/opencv/imgproc/Imgproc;->getAffineTransform(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/MatOfPoint2f;)Lorg/opencv/core/Mat;

    move-result-object v1

    .line 350
    invoke-virtual {v0}, Lorg/opencv/core/MatOfPoint2f;->release()V

    .line 351
    invoke-virtual {v7}, Lorg/opencv/core/MatOfPoint2f;->release()V

    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private makeBandImageFile(Lorg/opencv/core/Mat;I[ILjava/lang/String;Ljava/util/LinkedList;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "I[I",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/io/File;",
            ">;)Z"
        }
    .end annotation

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    .line 617
    invoke-direct/range {p0 .. p3}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->getTransMat(Lorg/opencv/core/Mat;I[I)Lorg/opencv/core/Mat;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    :goto_0
    add-int/lit16 v12, v7, 0x200

    const/4 v13, 0x1

    .line 623
    aget v2, p3, v13

    if-ge v12, v2, :cond_1

    .line 624
    new-instance v13, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ".bmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v13, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    aget v5, p3, v10

    const/16 v6, 0x200

    move-object v2, p0

    move-object v3, p1

    move-object v4, v9

    move-object v8, v13

    invoke-direct/range {v2 .. v8}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->affineBand(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIILjava/io/File;)Z

    .line 630
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    return v10

    .line 634
    :cond_0
    invoke-virtual {v1, v13}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    add-int/lit8 v11, v11, 0x1

    move v7, v12

    goto :goto_0

    .line 641
    :cond_1
    new-instance v12, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ".bmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    aget v0, p3, v13

    sub-int v6, v0, v7

    .line 644
    aget v5, p3, v10

    move-object v2, p0

    move-object v3, p1

    move-object v4, v9

    move-object v8, v12

    invoke-direct/range {v2 .. v8}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->affineBand(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIILjava/io/File;)Z

    move-result v0

    if-nez v0, :cond_2

    return v10

    .line 648
    :cond_2
    invoke-virtual {v1, v12}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    return v13
.end method

.method private makePrintImageFile(Lorg/opencv/core/Mat;I[ILjava/lang/String;)Z
    .locals 7

    .line 662
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 663
    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    .line 665
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v6, v0

    .line 666
    invoke-direct/range {v1 .. v6}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->makeBandImageFile(Lorg/opencv/core/Mat;I[ILjava/lang/String;Ljava/util/LinkedList;)Z

    move-result p1

    const/4 p2, 0x0

    if-nez p1, :cond_0

    return p2

    .line 671
    :cond_0
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    invoke-static {p4, p1}, Lcom/epson/cameracopy/alt/SimpleBmpMerger;->margeBmpFile(Ljava/lang/String;Ljava/util/Iterator;)Z

    move-result p1

    if-nez p1, :cond_1

    return p2

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method static new3x2Mat(DDDDDD)Lorg/opencv/core/Mat;
    .locals 17

    .line 233
    new-instance v16, Lcom/epson/cameracopy/printlayout/ImageAndLayout$2;

    const/4 v1, 0x2

    const/4 v2, 0x3

    const/4 v3, 0x6

    move-object/from16 v0, v16

    move-wide/from16 v4, p0

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move-wide/from16 v10, p6

    move-wide/from16 v12, p8

    move-wide/from16 v14, p10

    invoke-direct/range {v0 .. v15}, Lcom/epson/cameracopy/printlayout/ImageAndLayout$2;-><init>(IIIDDDDDD)V

    return-object v16
.end method

.method private setOpenCvJpegRotation(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    .line 916
    iput v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotation:I

    .line 917
    invoke-static {}, Lepson/print/Util/OpenCvHelper;->doseImreadRotateWithExifTag()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 920
    :cond_0
    invoke-static {p1}, Lepson/print/Util/ImageFileUtil;->getExifRotationDegree(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotation:I

    .line 921
    iget p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotation:I

    if-gez p1, :cond_1

    .line 922
    iput v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotation:I

    :cond_1
    return-void
.end method


# virtual methods
.method public createPrintData(Ljava/lang/String;Z[II)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 717
    :try_start_0
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOrgFilename:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setOpenCvJpegRotation(Ljava/lang/String;)V

    .line 719
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOrgFilename:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-static {v2, v3}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;I)Lorg/opencv/core/Mat;

    move-result-object v2

    .line 720
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->channels()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 723
    invoke-static {v2}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->convertRgbaToRgbTrWhite(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    if-eqz p2, :cond_4

    .line 730
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->channels()I

    move-result p2
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x1

    if-eq p2, v2, :cond_4

    const/4 v2, 0x3

    if-eq p2, v2, :cond_3

    if-eqz v1, :cond_1

    .line 757
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 765
    :cond_1
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 766
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 767
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_2
    return v0

    :cond_3
    const/4 p2, 0x7

    .line 732
    :try_start_1
    invoke-static {v1, v1, p2}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    const/16 p2, 0x8

    .line 733
    invoke-static {v1, v1, p2, v2}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V

    .line 746
    :cond_4
    invoke-direct {p0, v1, p4, p3, p1}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->makePrintImageFile(Lorg/opencv/core/Mat;I[ILjava/lang/String;)Z

    move-result p2
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_5

    .line 757
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    :cond_5
    if-nez p2, :cond_6

    .line 765
    new-instance p3, Ljava/io/File;

    invoke-direct {p3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 766
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 767
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    :cond_6
    return p2

    :catchall_0
    move-exception p2

    if-eqz v1, :cond_7

    .line 757
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 765
    :cond_7
    new-instance p3, Ljava/io/File;

    invoke-direct {p3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 766
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 767
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 769
    :cond_8
    throw p2

    :catch_0
    nop

    if-eqz v1, :cond_9

    .line 757
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 765
    :cond_9
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 766
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_a

    .line 767
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_a
    return v0

    :catch_1
    nop

    if-eqz v1, :cond_b

    .line 757
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 765
    :cond_b
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 766
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_c

    .line 767
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_c
    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getExRate([I)[D
    .locals 8

    const/4 v0, 0x2

    .line 208
    new-array v1, v0, [D

    fill-array-data v1, :array_0

    if-eqz p1, :cond_3

    .line 212
    array-length v2, p1

    if-ge v2, v0, :cond_0

    goto :goto_1

    .line 220
    :cond_0
    iget v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintableAreaWidth:I

    if-lez v0, :cond_2

    iget v2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintableAreaHeight:I

    if-gtz v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 224
    aget v4, p1, v3

    int-to-double v4, v4

    int-to-double v6, v0

    div-double/2addr v4, v6

    aput-wide v4, v1, v3

    const/4 v0, 0x1

    .line 225
    aget p1, p1, v0

    int-to-double v3, p1

    int-to-double v5, v2

    div-double/2addr v3, v5

    aput-wide v3, v1, v0

    return-object v1

    :cond_2
    :goto_0
    return-object v1

    :cond_3
    :goto_1
    return-object v1

    :array_0
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
    .end array-data
.end method

.method public getOrgFileName()Ljava/lang/String;
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOrgFilename:Ljava/lang/String;

    return-object v0
.end method

.method getPrintImageSize([D)Lorg/opencv/core/Size;
    .locals 6

    .line 427
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintTargetWidth:D

    const/4 v2, 0x0

    aget-wide v2, p1, v2

    mul-double v0, v0, v2

    .line 428
    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintTargetHeight:D

    const/4 v4, 0x1

    aget-wide v4, p1, v4

    mul-double v2, v2, v4

    .line 429
    new-instance p1, Lorg/opencv/core/Size;

    invoke-direct {p1, v0, v1, v2, v3}, Lorg/opencv/core/Size;-><init>(DD)V

    return-object p1
.end method

.method getPrintMat([IILorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 11

    const/4 v0, 0x3

    and-int/2addr p2, v0

    const/4 v1, 0x6

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    packed-switch p2, :pswitch_data_0

    .line 488
    invoke-virtual {p3}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object p1

    return-object p1

    .line 475
    :pswitch_0
    new-instance p2, Lorg/opencv/core/Mat;

    invoke-direct {p2, v4, v0, v1}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 477
    new-array v0, v5, [D

    invoke-virtual {p3, v5, v6}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    aput-wide v7, v0, v6

    invoke-virtual {p2, v6, v6, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 478
    new-array v0, v5, [D

    invoke-virtual {p3, v5, v5}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    aput-wide v7, v0, v6

    invoke-virtual {p2, v6, v5, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 479
    new-array v0, v5, [D

    invoke-virtual {p3, v5, v4}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    aget p1, p1, v6

    int-to-double v9, p1

    add-double/2addr v7, v9

    aput-wide v7, v0, v6

    invoke-virtual {p2, v6, v4, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 480
    new-array p1, v5, [D

    invoke-virtual {p3, v6, v6}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v0

    aget-wide v7, v0, v6

    mul-double v7, v7, v2

    aput-wide v7, p1, v6

    invoke-virtual {p2, v5, v6, p1}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 481
    new-array p1, v5, [D

    invoke-virtual {p3, v6, v5}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v0

    aget-wide v7, v0, v6

    mul-double v7, v7, v2

    aput-wide v7, p1, v6

    invoke-virtual {p2, v5, v5, p1}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 482
    new-array p1, v5, [D

    invoke-virtual {p3, v6, v4}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object p3

    aget-wide v0, p3, v6

    mul-double v0, v0, v2

    aput-wide v0, p1, v6

    invoke-virtual {p2, v5, v4, p1}, Lorg/opencv/core/Mat;->put(II[D)I

    return-object p2

    .line 464
    :pswitch_1
    new-array p2, v5, [D

    invoke-virtual {p3, v6, v6}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v0

    aget-wide v7, v0, v6

    mul-double v7, v7, v2

    aput-wide v7, p2, v6

    const/4 v0, 0x0

    invoke-virtual {v0, v6, v6, p2}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 465
    new-array p2, v5, [D

    invoke-virtual {p3, v6, v5}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    mul-double v7, v7, v2

    aput-wide v7, p2, v6

    invoke-virtual {v0, v6, v5, p2}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 466
    new-array p2, v5, [D

    invoke-virtual {p3, v6, v4}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    mul-double v7, v7, v2

    aget v1, p1, v6

    int-to-double v9, v1

    add-double/2addr v7, v9

    aput-wide v7, p2, v6

    invoke-virtual {v0, v6, v4, p2}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 467
    new-array p2, v5, [D

    invoke-virtual {p3, v5, v6}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    mul-double v7, v7, v2

    aput-wide v7, p2, v6

    invoke-virtual {v0, v5, v6, p2}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 468
    new-array p2, v5, [D

    invoke-virtual {p3, v5, v5}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    mul-double v7, v7, v2

    aput-wide v7, p2, v6

    invoke-virtual {v0, v5, v5, p2}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 469
    new-array p2, v5, [D

    invoke-virtual {p3, v5, v4}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object p3

    aget-wide v7, p3, v6

    mul-double v7, v7, v2

    aget p1, p1, v5

    int-to-double v1, p1

    add-double/2addr v7, v1

    aput-wide v7, p2, v6

    invoke-virtual {v0, v5, v4, p2}, Lorg/opencv/core/Mat;->put(II[D)I

    return-object v0

    .line 451
    :pswitch_2
    new-instance p2, Lorg/opencv/core/Mat;

    invoke-direct {p2, v4, v0, v1}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 453
    new-array v0, v5, [D

    invoke-virtual {p3, v5, v6}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    mul-double v7, v7, v2

    aput-wide v7, v0, v6

    invoke-virtual {p2, v6, v6, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 454
    new-array v0, v5, [D

    invoke-virtual {p3, v5, v5}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    mul-double v7, v7, v2

    aput-wide v7, v0, v6

    invoke-virtual {p2, v6, v5, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 455
    new-array v0, v5, [D

    invoke-virtual {p3, v5, v4}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v1

    aget-wide v7, v1, v6

    mul-double v7, v7, v2

    aget p1, p1, v6

    int-to-double v1, p1

    add-double/2addr v7, v1

    aput-wide v7, v0, v6

    invoke-virtual {p2, v6, v4, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 456
    new-array p1, v5, [D

    invoke-virtual {p3, v6, v6}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v0

    aget-wide v1, v0, v6

    aput-wide v1, p1, v6

    invoke-virtual {p2, v5, v6, p1}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 457
    new-array p1, v5, [D

    invoke-virtual {p3, v6, v5}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v0

    aget-wide v1, v0, v6

    aput-wide v1, p1, v6

    invoke-virtual {p2, v5, v5, p1}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 458
    new-array p1, v5, [D

    invoke-virtual {p3, v6, v4}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object p3

    aget-wide v0, p3, v6

    aput-wide v0, p1, v6

    invoke-virtual {p2, v5, v4, p1}, Lorg/opencv/core/Mat;->put(II[D)I

    return-object p2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isPaperLandscape()Z
    .locals 1

    .line 188
    iget-boolean v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mIsPaperLandscape:Z

    return v0
.end method

.method make24BitMatFromGrayMat(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 4

    .line 688
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->height()I

    move-result v1

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->width()I

    move-result v2

    sget v3, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-direct {v0, v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    const/4 v1, 0x1

    .line 689
    new-array v2, v1, [Lorg/opencv/core/Mat;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 690
    new-array v1, v1, [Lorg/opencv/core/Mat;

    aput-object v0, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 691
    new-instance v2, Lorg/opencv/core/MatOfInt;

    const/4 v3, 0x6

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v2, v3}, Lorg/opencv/core/MatOfInt;-><init>([I)V

    .line 692
    invoke-static {p1, v1, v2}, Lorg/opencv/core/Core;->mixChannels(Ljava/util/List;Ljava/util/List;Lorg/opencv/core/MatOfInt;)V

    return-object v0

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x1
        0x0
        0x2
    .end array-data
.end method

.method public makePrintImageMat(Lorg/opencv/core/Mat;I[I)Lorg/opencv/core/Mat;
    .locals 19

    .line 511
    invoke-direct/range {p0 .. p3}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->getTransMat(Lorg/opencv/core/Mat;I[I)Lorg/opencv/core/Mat;

    move-result-object v7

    .line 513
    new-instance v8, Lorg/opencv/core/Mat;

    const/4 v0, 0x1

    aget v0, p3, v0

    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    invoke-direct {v8, v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 515
    invoke-virtual {v8}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v3

    move-object/from16 v9, p0

    iget v4, v9, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mInterpolation:I

    const/4 v5, 0x0

    new-instance v6, Lorg/opencv/core/Scalar;

    const-wide v11, 0x406fe00000000000L    # 255.0

    const-wide v13, 0x406fe00000000000L    # 255.0

    const-wide v15, 0x406fe00000000000L    # 255.0

    const-wide v17, 0x406fe00000000000L    # 255.0

    move-object v10, v6

    invoke-direct/range {v10 .. v18}, Lorg/opencv/core/Scalar;-><init>(DDDD)V

    move-object/from16 v0, p1

    move-object v1, v8

    move-object v2, v7

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->warpAffine(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;IILorg/opencv/core/Scalar;)V

    .line 519
    invoke-virtual {v7}, Lorg/opencv/core/Mat;->release()V

    return-object v8
.end method

.method setInterpolation(I)V
    .locals 0

    .line 950
    iput p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mInterpolation:I

    return-void
.end method

.method public setLayout(DDDDI)V
    .locals 0

    .line 174
    iput-wide p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintTargetWidth:D

    .line 175
    iput-wide p3, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintTargetHeight:D

    .line 176
    iput-wide p5, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOffsetX:D

    .line 177
    iput-wide p7, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOffsetY:D

    .line 178
    iput p9, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mRotation:I

    return-void
.end method

.method public setLayoutAreaSize(II)V
    .locals 0

    .line 157
    iput p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintableAreaWidth:I

    .line 158
    iput p2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintableAreaHeight:I

    return-void
.end method

.method public setOpenCvExifRotationCancel(Z)V
    .locals 0

    .line 148
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOpenCvExifRotationCancel:Z

    return-void
.end method

.method public setOrgFileName(Ljava/lang/String;)V
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOrgFilename:Ljava/lang/String;

    return-void
.end method

.method public setPaperIsLandscape(Z)V
    .locals 0

    .line 183
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mIsPaperLandscape:Z

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 101
    iget p2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintableAreaWidth:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget p2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintableAreaHeight:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    iget-object p2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOrgFilename:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintTargetWidth:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 105
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mPrintTargetHeight:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 106
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOffsetX:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 107
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mOffsetY:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 108
    iget p2, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mRotation:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 p2, 0x1

    .line 109
    new-array p2, p2, [Z

    iget-boolean v0, p0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->mIsPaperLandscape:Z

    const/4 v1, 0x0

    aput-boolean v0, p2, v1

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    return-void
.end method

.class Lcom/epson/cameracopy/printlayout/IdManuscriptSize;
.super Ljava/lang/Object;
.source "ManuscriptSize.java"

# interfaces
.implements Lcom/epson/cameracopy/printlayout/ManuscriptSize$ManuscriptTypeAdapter;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V
    .locals 0

    .line 416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    return-void
.end method

.method public static get360DpiPaperSizeById(I)Landroid/graphics/Point;
    .locals 2

    .line 427
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 429
    invoke-static {p0}, Lepson/common/Info_paper;->get360DpiPaperSizeInfo(I)Lepson/common/Info_paper;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v0

    .line 436
    :cond_0
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 437
    invoke-virtual {p0}, Lepson/common/Info_paper;->getPaper_height()I

    move-result p0

    iput p0, v0, Landroid/graphics/Point;->y:I

    return-object v0
.end method

.method protected static getDpiPaperSizeById(II)Landroid/graphics/Point;
    .locals 8

    .line 448
    invoke-static {p0}, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->get360DpiPaperSizeById(I)Landroid/graphics/Point;

    move-result-object p0

    const/16 v0, 0x168

    if-eq p1, v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 455
    :cond_0
    iget v0, p0, Landroid/graphics/Point;->x:I

    int-to-double v0, v0

    int-to-double v2, p1

    mul-double v0, v0, v2

    const-wide v4, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v6

    double-to-int p1, v0

    iput p1, p0, Landroid/graphics/Point;->x:I

    .line 456
    iget p1, p0, Landroid/graphics/Point;->y:I

    int-to-double v0, p1

    mul-double v0, v0, v2

    div-double/2addr v0, v4

    add-double/2addr v0, v6

    double-to-int p1, v0

    iput p1, p0, Landroid/graphics/Point;->y:I

    return-object p0

    :cond_1
    :goto_0
    return-object p0
.end method


# virtual methods
.method public displayLength()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 511
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    .line 513
    :cond_2
    check-cast p1, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;

    .line 514
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    if-nez v2, :cond_3

    .line 515
    iget-object p1, p1, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    if-eqz p1, :cond_4

    return v1

    .line 517
    :cond_3
    iget-object p1, p1, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v2, p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    return v1

    :cond_4
    return v0
.end method

.method public getBasePixelSize(I)Landroid/graphics/Point;
    .locals 1

    .line 464
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result v0

    invoke-static {v0, p1}, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->getDpiPaperSizeById(II)Landroid/graphics/Point;

    move-result-object p1

    return-object p1
.end method

.method public getSizeName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 473
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e0418

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 478
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    .line 479
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result v1

    .line 480
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;->getStringId(I)I

    move-result v0

    .line 481
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUnitType(Landroid/content/Context;)I
    .locals 1

    .line 489
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->getSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v0, ".*in *$"

    .line 491
    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

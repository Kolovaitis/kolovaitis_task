.class public Lcom/epson/cameracopy/printlayout/PreviewView;
.super Landroid/view/View;
.source "PreviewView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;,
        Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;,
        Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;,
        Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;
    }
.end annotation


# static fields
.field private static final BORDER_AREA_COLOR:I = -0x1

.field private static final COLOR_FILTER:[Landroid/graphics/ColorMatrixColorFilter;

.field private static final MANUSCRIPT_FRAME_BORDER_SIZE_DP:F = 1.5f

.field private static final MANUSCRIPT_FRAME_COLOR:I = -0x3f010000

.field private static final PAPER_AREA_COLOR:I = -0x1

.field private static final colorMatrix:[F

.field private static final monoMatrix:[F

.field private static final sepiaMatrix:[F


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mBitmapPaint:Landroid/graphics/Paint;

.field private mBorderlessMode:Z

.field private mDpToPixelScale:F

.field private final mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field private mImageFileName:Ljava/lang/String;

.field private mIsMoving:Z

.field private mIsPrintPositionResetRequired:Z

.field private mLinePoints:[F

.field private final mMatrix:Landroid/graphics/Matrix;

.field private final mPaint:Landroid/graphics/Paint;

.field private mPapaerSizeId:I

.field private mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

.field private mPrttargetScaleChangeListener:Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;

.field private final mScaleGestureDetector:Landroid/view/ScaleGestureDetector;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x14

    .line 56
    new-array v1, v0, [F

    fill-array-data v1, :array_0

    sput-object v1, Lcom/epson/cameracopy/printlayout/PreviewView;->colorMatrix:[F

    .line 63
    new-array v1, v0, [F

    fill-array-data v1, :array_1

    sput-object v1, Lcom/epson/cameracopy/printlayout/PreviewView;->monoMatrix:[F

    .line 74
    new-array v0, v0, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/epson/cameracopy/printlayout/PreviewView;->sepiaMatrix:[F

    const/4 v0, 0x3

    .line 86
    new-array v0, v0, [Landroid/graphics/ColorMatrixColorFilter;

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    new-instance v2, Landroid/graphics/ColorMatrix;

    sget-object v3, Lcom/epson/cameracopy/printlayout/PreviewView;->colorMatrix:[F

    invoke-direct {v2, v3}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    new-instance v2, Landroid/graphics/ColorMatrix;

    sget-object v3, Lcom/epson/cameracopy/printlayout/PreviewView;->monoMatrix:[F

    invoke-direct {v2, v3}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    new-instance v2, Landroid/graphics/ColorMatrix;

    sget-object v3, Lcom/epson/cameracopy/printlayout/PreviewView;->sepiaMatrix:[F

    invoke-direct {v2, v3}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/epson/cameracopy/printlayout/PreviewView;->COLOR_FILTER:[Landroid/graphics/ColorMatrixColorFilter;

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x3e990afe
        0x3f162c23
        0x3dea7371    # 0.114478f
        0x0
        0x0
        0x3e990afe
        0x3f162c23
        0x3dea7371    # 0.114478f
        0x0
        0x0
        0x3e990afe
        0x3f162c23
        0x3dea7371    # 0.114478f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x3df09561
        0x3ee6f716
        0x3cb13eb3
        0x0
        0x0
        0x3dd5a5e0
        0x3ece095c
        0x3c9d8d11
        0x0
        0x0
        0x3d4db091
        0x3ea06257
        0x3c75b460
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 167
    invoke-direct {p0, p1, v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 163
    invoke-direct {p0, p1, p2, v0}, Lcom/epson/cameracopy/printlayout/PreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 144
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmapPaint:Landroid/graphics/Paint;

    .line 120
    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mMatrix:Landroid/graphics/Matrix;

    const/4 p2, -0x1

    .line 136
    iput p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPapaerSizeId:I

    .line 146
    new-instance p2, Landroid/support/v4/view/GestureDetectorCompat;

    new-instance p3, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;

    invoke-direct {p3, p0}, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;-><init>(Lcom/epson/cameracopy/printlayout/PreviewView;)V

    invoke-direct {p2, p1, p3}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    .line 149
    iget-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Landroid/support/v4/view/GestureDetectorCompat;->setIsLongpressEnabled(Z)V

    .line 151
    new-instance p2, Landroid/view/ScaleGestureDetector;

    new-instance p3, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;

    invoke-direct {p3, p0}, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;-><init>(Lcom/epson/cameracopy/printlayout/PreviewView;)V

    invoke-direct {p2, p1, p3}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 154
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    const/16 p2, 0x10

    .line 155
    new-array p2, p2, [F

    iput-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    .line 157
    new-instance p2, Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-direct {p2}, Lcom/epson/cameracopy/printlayout/PreviewPosition;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    .line 159
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mDpToPixelScale:F

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/printlayout/PreviewView;)Lcom/epson/cameracopy/printlayout/PreviewPosition;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    return-object p0
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/printlayout/PreviewView;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->updateScale()V

    return-void
.end method

.method private createPreviewBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    const/4 v0, 0x1

    const/16 v1, 0x258

    const/16 v2, 0x320

    .line 598
    invoke-static {p1, v1, v2, v0, v0}, Lepson/common/ImageUtil;->loadBitmap(Ljava/lang/String;IIZZ)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private drawNonPrintableArea(Landroid/graphics/Canvas;Landroid/graphics/Rect;[I)V
    .locals 10

    .line 283
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v0, 0x0

    .line 285
    aget v0, p3, v0

    const/4 v1, 0x1

    .line 286
    aget v1, p3, v1

    const/4 v2, 0x2

    .line 287
    aget v2, p3, v2

    const/4 v3, 0x3

    .line 288
    aget p3, p3, v3

    .line 292
    iget v3, p2, Landroid/graphics/Rect;->left:I

    int-to-float v5, v3

    iget v3, p2, Landroid/graphics/Rect;->top:I

    int-to-float v6, v3

    iget v3, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    int-to-float v7, v3

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v3

    iget-object v9, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 295
    iget v3, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    int-to-float v5, v3

    iget v3, p2, Landroid/graphics/Rect;->top:I

    int-to-float v6, v3

    iget v3, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    add-int/2addr v3, v2

    int-to-float v7, v3

    iget v3, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v1

    int-to-float v8, v3

    iget-object v9, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 299
    iget v3, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    int-to-float v5, v3

    iget v3, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v1

    add-int/2addr v3, p3

    int-to-float v6, v3

    iget p3, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr p3, v0

    add-int/2addr p3, v2

    int-to-float v7, p3

    iget p3, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, p3

    iget-object v9, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 302
    iget p3, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr p3, v0

    add-int/2addr p3, v2

    int-to-float v1, p3

    iget p3, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, p3

    iget p3, p2, Landroid/graphics/Rect;->right:I

    int-to-float v3, p3

    iget p2, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, p2

    iget-object v5, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawOutsidePaperArea(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 11

    .line 313
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 314
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 315
    iget-boolean v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsMoving:Z

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    const/16 v1, 0xdd

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 319
    :cond_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->getWidth()I

    move-result v0

    int-to-float v4, v0

    .line 320
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 324
    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v8, v1

    iget-object v10, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    move-object v5, p1

    move v9, v0

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 326
    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v6, v1

    iget v1, p2, Landroid/graphics/Rect;->right:I

    int-to-float v8, v1

    iget v1, p2, Landroid/graphics/Rect;->top:I

    int-to-float v9, v1

    iget-object v10, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 329
    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v6, v1

    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v1

    iget v1, p2, Landroid/graphics/Rect;->right:I

    int-to-float v8, v1

    iget-object v10, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    move v9, v0

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 332
    iget p2, p2, Landroid/graphics/Rect;->right:I

    int-to-float v2, p2

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    move-object v1, p1

    move v5, v0

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawPrintTargetPreview(Landroid/graphics/Canvas;)V
    .locals 8

    .line 205
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getPreviewPrtimageRect()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 207
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    return-void

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getPrtimageRotation()I

    move-result v1

    .line 212
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 213
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mMatrix:Landroid/graphics/Matrix;

    int-to-float v3, v1

    const/high16 v4, 0x42b40000    # 90.0f

    mul-float v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_1

    .line 220
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-double v2, v2

    iget-object v4, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v2, v4

    .line 221
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-double v4, v4

    iget-object v6, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v4, v6

    goto :goto_0

    .line 223
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-double v2, v2

    iget-object v4, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v2, v4

    .line 224
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-double v4, v4

    iget-object v6, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v4, v6

    .line 226
    :goto_0
    iget-object v6, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mMatrix:Landroid/graphics/Matrix;

    double-to-float v2, v2

    double-to-float v3, v4

    invoke-virtual {v6, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    const/4 v3, 0x0

    goto :goto_1

    .line 239
    :pswitch_0
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    move v3, v1

    const/4 v1, 0x0

    goto :goto_1

    .line 235
    :pswitch_1
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 236
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    goto :goto_1

    .line 232
    :pswitch_2
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    const/4 v3, 0x0

    .line 242
    :goto_1
    iget-object v4, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mMatrix:Landroid/graphics/Matrix;

    iget v5, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v1

    int-to-float v1, v5

    iget v5, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v3

    int-to-float v3, v5

    invoke-virtual {v4, v1, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 243
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 246
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, -0x3f010000    # -7.96875f

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v1, 0x3fc00000    # 1.5f

    .line 247
    iget v3, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mDpToPixelScale:F

    mul-float v3, v3, v1

    .line 248
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 249
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v1, v3

    .line 250
    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    .line 251
    iget-object v3, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    aput v5, v3, v2

    .line 252
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    const/4 v5, 0x1

    aput v3, v2, v5

    .line 253
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    const/4 v3, 0x2

    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    aput v5, v2, v3

    .line 254
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    const/4 v3, 0x3

    aput v1, v2, v3

    const/4 v3, 0x4

    .line 256
    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    aput v5, v2, v3

    .line 257
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    const/4 v3, 0x5

    aput v1, v2, v3

    const/4 v3, 0x6

    .line 258
    aput v4, v2, v3

    const/4 v3, 0x7

    .line 259
    aput v1, v2, v3

    const/16 v3, 0x8

    .line 261
    aput v4, v2, v3

    const/16 v3, 0x9

    .line 262
    aput v1, v2, v3

    const/16 v1, 0xa

    .line 263
    aput v4, v2, v1

    const/16 v1, 0xb

    .line 264
    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    aput v3, v2, v1

    .line 266
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    const/16 v2, 0xc

    aput v4, v1, v2

    const/16 v2, 0xd

    .line 267
    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    aput v3, v1, v2

    .line 268
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    const/16 v2, 0xe

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    aput v3, v1, v2

    .line 269
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    const/16 v2, 0xf

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    aput v0, v1, v2

    .line 271
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mLinePoints:[F

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setPaperSizeInternal(I)V
    .locals 11

    .line 460
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPapaerSizeId:I

    .line 462
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 467
    :cond_0
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v1

    .line 468
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v2

    .line 470
    invoke-virtual {p1}, Lepson/common/Info_paper;->getLeftMargin_border()I

    move-result v3

    .line 471
    invoke-virtual {p1}, Lepson/common/Info_paper;->getTopMargin_border()I

    move-result v4

    .line 473
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width_boder()I

    move-result v5

    .line 474
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height_boder()I

    move-result v6

    .line 476
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    .line 478
    invoke-virtual {p1}, Lepson/common/Info_paper;->getLeftMargin()I

    move-result v7

    neg-int v7, v7

    .line 479
    invoke-virtual {p1}, Lepson/common/Info_paper;->getTopMargin()I

    move-result v8

    neg-int v8, v8

    .line 480
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width_boderless()I

    move-result v9

    .line 481
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height_boderless()I

    move-result v10

    .line 476
    invoke-virtual/range {v0 .. v10}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->setPaperSize(IIIIIIIIII)V

    const/4 p1, 0x1

    .line 482
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsPrintPositionResetRequired:Z

    return-void
.end method

.method private updateScale()V
    .locals 3

    .line 438
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPrttargetScaleChangeListener:Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;

    if-eqz v0, :cond_0

    .line 439
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    .line 440
    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getCurrentPrttargetScale()D

    move-result-wide v1

    .line 439
    invoke-interface {v0, v1, v2}, Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;->onPrttargetScaleChange(D)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getImageAndLayout()Lcom/epson/cameracopy/printlayout/ImageAndLayout;
    .locals 2

    .line 666
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    iget-boolean v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBorderlessMode:Z

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getImageAndLayout(Z)Lcom/epson/cameracopy/printlayout/ImageAndLayout;

    move-result-object v0

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 185
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getPreviewPaperRect()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 189
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 193
    :cond_0
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->drawPrintTargetPreview(Landroid/graphics/Canvas;)V

    .line 195
    invoke-direct {p0, p1, v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->drawOutsidePaperArea(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 197
    iget-boolean v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBorderlessMode:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 198
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getPaperMaginAnd3mPrintableArea()[I

    move-result-object v1

    .line 199
    invoke-direct {p0, p1, v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewView;->drawNonPrintableArea(Landroid/graphics/Canvas;Landroid/graphics/Rect;[I)V

    :cond_1
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 339
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    if-lez p1, :cond_5

    if-gtz p2, :cond_0

    goto :goto_2

    :cond_0
    if-lez p3, :cond_3

    if-gtz p4, :cond_1

    goto :goto_0

    :cond_1
    if-ne p1, p3, :cond_2

    if-eq p2, p4, :cond_4

    .line 353
    :cond_2
    iget-object p3, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {p3, p1, p2}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->changeScreenSize(II)V

    goto :goto_1

    .line 348
    :cond_3
    :goto_0
    iget-object p3, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {p3, p1, p2}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->setScreenSize(II)V

    .line 349
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->update()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    :goto_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 172
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 173
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v1, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 175
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 177
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/printlayout/PreviewView;->setMoving(Z)V

    .line 178
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->invalidate()V

    :cond_2
    if-nez v0, :cond_3

    .line 180
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_4

    :cond_3
    const/4 v2, 0x1

    :cond_4
    return v2
.end method

.method public printPostionResetRequest()V
    .locals 1

    const/4 v0, 0x1

    .line 651
    iput-boolean v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsPrintPositionResetRequired:Z

    return-void
.end method

.method public releaseBitmap()V
    .locals 1

    .line 610
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    .line 611
    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public restoreBitmap()V
    .locals 1

    .line 621
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    return-void

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mImageFileName:Ljava/lang/String;

    if-nez v0, :cond_1

    return-void

    .line 628
    :cond_1
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->createPreviewBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public restoreInstanceState(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .line 676
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/printlayout/PreviewPosition;

    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    const/4 p1, 0x0

    .line 678
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsPrintPositionResetRequired:Z

    .line 681
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->updateScale()V

    return-void
.end method

.method public rotateImageR90()V
    .locals 1

    .line 515
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->rotateRight90()V

    .line 516
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->invalidate()V

    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .line 671
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method

.method public setBorderless(Z)V
    .locals 1

    .line 639
    iget-boolean v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBorderlessMode:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 643
    :cond_0
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBorderlessMode:Z

    const/4 p1, 0x1

    .line 644
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsPrintPositionResetRequired:Z

    return-void
.end method

.method public setColor(I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    goto :goto_0

    .line 499
    :pswitch_0
    sget-object p1, Lcom/epson/cameracopy/printlayout/PreviewView;->COLOR_FILTER:[Landroid/graphics/ColorMatrixColorFilter;

    const/4 v0, 0x1

    aget-object p1, p1, v0

    goto :goto_0

    .line 496
    :pswitch_1
    sget-object p1, Lcom/epson/cameracopy/printlayout/PreviewView;->COLOR_FILTER:[Landroid/graphics/ColorMatrixColorFilter;

    const/4 v0, 0x0

    aget-object p1, p1, v0

    :goto_0
    if-eqz p1, :cond_0

    .line 504
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 0

    .line 526
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mImageFileName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 528
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsPrintPositionResetRequired:Z

    return-void
.end method

.method public setMoving(Z)V
    .locals 0

    .line 363
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsMoving:Z

    return-void
.end method

.method public setPaperSize(I)V
    .locals 1

    .line 452
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPapaerSizeId:I

    if-ne p1, v0, :cond_0

    return-void

    .line 456
    :cond_0
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->setPaperSizeInternal(I)V

    return-void
.end method

.method public setPrintTargetSize(II)V
    .locals 1

    .line 541
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v0, p1, p2}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->setPrtimageActualSize(II)V

    const/4 p1, 0x1

    .line 542
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsPrintPositionResetRequired:Z

    return-void
.end method

.method public setPrttargetScaleChangeListener(Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;)V
    .locals 0

    .line 662
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPrttargetScaleChangeListener:Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;

    return-void
.end method

.method public update()V
    .locals 5

    .line 552
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mImageFileName:Ljava/lang/String;

    if-nez v0, :cond_0

    return-void

    .line 555
    :cond_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->getHeight()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->getWidth()I

    move-result v0

    if-gtz v0, :cond_1

    goto :goto_1

    .line 561
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mImageFileName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->createPreviewBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    .line 562
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    return-void

    .line 569
    :cond_2
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getPrtImageActualSIze()[I

    move-result-object v0

    const/4 v1, 0x0

    .line 573
    aget v2, v0, v1

    const/4 v3, 0x1

    aget v0, v0, v3

    sub-int/2addr v2, v0

    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    .line 574
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v4, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v0, v4

    mul-int v2, v2, v0

    if-gez v2, :cond_3

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    .line 578
    :goto_0
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v0, v3}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->setPrintTargetAddRotation(I)V

    .line 581
    iget-boolean v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsPrintPositionResetRequired:Z

    if-eqz v0, :cond_4

    .line 583
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPapaerSizeId:I

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->setPaperSizeInternal(I)V

    .line 584
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    iget-boolean v2, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mBorderlessMode:Z

    invoke-virtual {v0, v2}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->setBorderless(Z)V

    .line 585
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->resetRotation()V

    .line 586
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mPreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->calcSizeAndPositionOnScreen()V

    .line 587
    iput-boolean v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView;->mIsPrintPositionResetRequired:Z

    .line 588
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->updateScale()V

    .line 591
    :cond_4
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->invalidate()V

    return-void

    :cond_5
    :goto_1
    return-void
.end method

.class Lcom/epson/cameracopy/printlayout/AutoManuscriptSize;
.super Lcom/epson/cameracopy/printlayout/IdManuscriptSize;
.source "ManuscriptSize.java"


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V
    .locals 0

    .line 532
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;-><init>(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V

    return-void
.end method


# virtual methods
.method public displayLength()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getBasePixelSize(I)Landroid/graphics/Point;
    .locals 1

    const/4 v0, 0x0

    .line 538
    invoke-static {v0, p1}, Lcom/epson/cameracopy/printlayout/IdManuscriptSize;->getDpiPaperSizeById(II)Landroid/graphics/Point;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object p1
.end method

.method public getSizeName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    const v0, 0x7f0e040f

    .line 561
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUnitType(Landroid/content/Context;)I
    .locals 1

    .line 548
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object p1

    const-string v0, "US"

    .line 550
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "USA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "CA"

    .line 551
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "CAN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x2

    return p1
.end method

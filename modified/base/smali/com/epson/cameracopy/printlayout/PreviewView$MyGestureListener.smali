.class Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "PreviewView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/printlayout/PreviewView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyGestureListener"
.end annotation


# instance fields
.field private final mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/printlayout/PreviewView;)V
    .locals 0

    .line 370
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 371
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 393
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$000(Lcom/epson/cameracopy/printlayout/PreviewView;)Lcom/epson/cameracopy/printlayout/PreviewPosition;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->isFitSize()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 394
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$000(Lcom/epson/cameracopy/printlayout/PreviewView;)Lcom/epson/cameracopy/printlayout/PreviewPosition;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->resetPrtimagePosition()V

    .line 395
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$100(Lcom/epson/cameracopy/printlayout/PreviewView;)V

    .line 396
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->invalidate()V

    goto :goto_0

    .line 398
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$000(Lcom/epson/cameracopy/printlayout/PreviewView;)Lcom/epson/cameracopy/printlayout/PreviewPosition;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->fitSize()V

    .line 399
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$100(Lcom/epson/cameracopy/printlayout/PreviewView;)V

    .line 400
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->invalidate()V

    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 386
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->setMoving(Z)V

    .line 387
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->invalidate()V

    return v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 0

    .line 377
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/epson/cameracopy/printlayout/PreviewView;->setMoving(Z)V

    .line 379
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$000(Lcom/epson/cameracopy/printlayout/PreviewView;)Lcom/epson/cameracopy/printlayout/PreviewPosition;

    move-result-object p1

    float-to-int p3, p3

    neg-int p3, p3

    float-to-int p4, p4

    neg-int p4, p4

    invoke-virtual {p1, p3, p4}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->move(II)V

    .line 380
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->invalidate()V

    return p2
.end method

.class public Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;
.super Ljava/lang/Object;
.source "PreviewView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/printlayout/PreviewView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SizeAndPosition"
.end annotation


# instance fields
.field public height:I

.field public offsetX:I

.field public offsetY:I

.field public rotation:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;->width:I

    .line 100
    iput p2, p0, Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;->height:I

    .line 101
    iput p3, p0, Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;->offsetX:I

    .line 102
    iput p4, p0, Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;->offsetY:I

    .line 103
    iput p5, p0, Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;->rotation:I

    return-void
.end method

.class Lcom/epson/cameracopy/printlayout/PreviewPosition;
.super Ljava/lang/Object;
.source "PreviewPosition.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field public static final MAX_IMAGE_RATE:D = 4.0

.field public static final MIN_IMAGE_RATE:D = 0.25

.field private static final MOVABLE_PREVIE_PIXEL:I = 0xa

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private m3mPrintAreaActualHeight:I

.field private m3mPrintAreaActualWidth:I

.field private mActualPreviewRate:D

.field private mBorderlessDataHeight:I

.field private mBorderlessDataWidth:I

.field private mBorderlessLeftMargin:I

.field private mBorderlessMode:Z

.field private mBorderlessTopMargin:I

.field private mFitSizeMode:Z

.field private mPaperActualHeight:I

.field private mPaperActualWidth:I

.field private mPaperTo3mActualLeftMargin:I

.field private mPaperTo3mActualTopMargin:I

.field private mPreviewMovableArea:[I

.field private mPreviewPaperRate:D

.field private transient mPreviewPaperRect:Landroid/graphics/Rect;

.field private mPrintTargetAddRotation:I

.field private mPrtimageActualHeight:I

.field private mPrtimageActualWidth:I

.field private final mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

.field private transient mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

.field private mScreenHeight:I

.field private mScreenWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide v0, 0x3fef5c28f5c28f5cL    # 0.98

    .line 26
    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRate:D

    const/16 v0, 0xd2

    .line 36
    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualWidth:I

    const/16 v0, 0x129

    .line 37
    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualHeight:I

    .line 56
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    .line 67
    new-instance v0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-direct {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    const/4 v0, 0x4

    .line 89
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    return-void
.end method

.method public constructor <init>(D)V
    .locals 2

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide v0, 0x3fef5c28f5c28f5cL    # 0.98

    .line 26
    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRate:D

    const/16 v0, 0xd2

    .line 36
    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualWidth:I

    const/16 v0, 0x129

    .line 37
    iput v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualHeight:I

    .line 56
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    .line 67
    new-instance v0, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-direct {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    const/4 v0, 0x4

    .line 89
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    .line 103
    iput-wide p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRate:D

    return-void
.end method

.method private adjustPrtimagePosition()V
    .locals 5

    .line 436
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    const/4 v2, 0x0

    aget v3, v1, v2

    if-ge v0, v3, :cond_0

    .line 437
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setRight(I)V

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    const/4 v3, 0x1

    aget v4, v1, v3

    if-ge v0, v4, :cond_1

    .line 441
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setBottom(I)V

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    const/4 v3, 0x2

    aget v4, v1, v3

    if-le v0, v4, :cond_2

    .line 445
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setLeft(I)V

    .line 448
    :cond_2
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getTop()I

    move-result v0

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    const/4 v3, 0x3

    aget v4, v1, v3

    if-le v0, v4, :cond_3

    .line 449
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setTop(I)V

    .line 452
    :cond_3
    iput-boolean v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mFitSizeMode:Z

    return-void
.end method

.method private calcPreviewPaperSize()V
    .locals 8

    .line 314
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenWidth:I

    if-lez v0, :cond_1

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenHeight:I

    if-lez v1, :cond_1

    iget v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualWidth:I

    if-lez v2, :cond_1

    iget v3, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualHeight:I

    if-gtz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 318
    iput-boolean v4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mFitSizeMode:Z

    int-to-double v4, v0

    .line 320
    iget-wide v6, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRate:D

    mul-double v4, v4, v6

    int-to-double v0, v1

    mul-double v0, v0, v6

    int-to-double v6, v2

    div-double/2addr v4, v6

    int-to-double v2, v3

    div-double/2addr v0, v2

    .line 326
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    .line 327
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 328
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualWidth:I

    int-to-double v1, v1

    iget-wide v3, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    mul-double v1, v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 329
    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualHeight:I

    int-to-double v1, v1

    mul-double v1, v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 332
    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenWidth:I

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    .line 333
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenHeight:I

    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 335
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private initPreviewPrtimageRect()V
    .locals 8

    .line 286
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_1

    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualWidth:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualHeight:I

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 292
    iput-boolean v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mFitSizeMode:Z

    .line 294
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setScale(D)V

    .line 295
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->resetRotate()V

    .line 297
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualWidth:I

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    mul-double v0, v0, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v4

    double-to-int v0, v0

    .line 298
    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualHeight:I

    int-to-double v6, v1

    mul-double v6, v6, v2

    add-double/2addr v6, v4

    double-to-int v1, v6

    .line 299
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v2, v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setX1Size(II)V

    .line 302
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setLeft(I)V

    .line 303
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setTop(I)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 605
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 607
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    .line 608
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 609
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 610
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 611
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 613
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    .line 614
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 615
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 616
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 617
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result p1

    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private setMovableArea()V
    .locals 8

    .line 257
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0xa

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 258
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v1, 0xa

    const/4 v3, 0x1

    aput v1, v0, v3

    .line 259
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v1, v1, -0xa

    const/4 v4, 0x2

    aput v1, v0, v4

    .line 260
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v1, v1, -0xa

    const/4 v5, 0x3

    aput v1, v0, v5

    .line 262
    iget-boolean v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessMode:Z

    if-nez v0, :cond_0

    .line 263
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getPaperMaginAnd3mPrintableArea()[I

    move-result-object v0

    .line 264
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    aget v6, v1, v2

    aget v7, v0, v2

    add-int/2addr v6, v7

    aput v6, v1, v2

    .line 265
    aget v6, v1, v3

    aget v7, v0, v3

    add-int/2addr v6, v7

    aput v6, v1, v3

    .line 266
    iget-object v6, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    aget v2, v0, v2

    add-int/2addr v6, v2

    aget v2, v0, v4

    add-int/2addr v6, v2

    add-int/lit8 v6, v6, -0xa

    aput v6, v1, v4

    .line 268
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewMovableArea:[I

    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    aget v3, v0, v3

    add-int/2addr v2, v3

    aget v0, v0, v5

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0xa

    aput v2, v1, v5

    :cond_0
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 589
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 591
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 592
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 593
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 594
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 596
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 597
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 598
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 599
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    return-void
.end method


# virtual methods
.method public calcSizeAndPositionOnScreen()V
    .locals 0

    .line 245
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->calcPreviewPaperSize()V

    .line 246
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->initPreviewPrtimageRect()V

    .line 247
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->setMovableArea()V

    return-void
.end method

.method public changeScreenSize(II)V
    .locals 8

    .line 151
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    const-wide/16 v2, 0x0

    cmpg-double v4, v0, v2

    if-gtz v4, :cond_0

    return-void

    :cond_0
    if-lez p1, :cond_3

    if-gtz p2, :cond_1

    goto :goto_0

    .line 157
    :cond_1
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenWidth:I

    if-ne v0, p1, :cond_2

    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenHeight:I

    if-ne v0, p2, :cond_2

    return-void

    .line 163
    :cond_2
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenWidth:I

    .line 164
    iput p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenHeight:I

    .line 166
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getLeft()I

    move-result p1

    iget-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr p1, p2

    int-to-double p1, p1

    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr p1, v0

    .line 168
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getTop()I

    move-result v0

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v0, v2

    .line 173
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->calcPreviewPaperSize()V

    .line 176
    iget v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualWidth:I

    int-to-double v2, v2

    iget-wide v4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    mul-double v2, v2, v4

    double-to-int v2, v2

    .line 177
    iget v3, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualHeight:I

    int-to-double v6, v3

    mul-double v6, v6, v4

    double-to-int v3, v6

    .line 178
    iget-object v4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v4, v2, v3}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setX1Size(II)V

    .line 181
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    iget-object v3, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-wide v4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    mul-double p1, p1, v4

    double-to-int p1, p1

    add-int/2addr v3, p1

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setLeft(I)V

    .line 183
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    iget-object p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->top:I

    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    mul-double v0, v0, v2

    double-to-int v0, v0

    add-int/2addr p2, v0

    invoke-virtual {p1, p2}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setTop(I)V

    .line 186
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->setMovableArea()V

    return-void

    :cond_3
    :goto_0
    return-void
.end method

.method public fitSize()V
    .locals 9

    .line 528
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualWidth:I

    if-lez v0, :cond_5

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualHeight:I

    if-gtz v1, :cond_0

    goto :goto_1

    .line 535
    :cond_0
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v2}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getRotation()I

    move-result v2

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    move v8, v1

    move v1, v0

    move v0, v8

    .line 543
    :cond_1
    iget v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualWidth:I

    int-to-double v4, v2

    int-to-double v6, v0

    div-double/2addr v4, v6

    .line 544
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualHeight:I

    int-to-double v6, v0

    int-to-double v0, v1

    div-double v0, v6, v0

    cmpl-double v2, v4, v0

    if-lez v2, :cond_2

    goto :goto_0

    :cond_2
    move-wide v0, v4

    :goto_0
    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    cmpl-double v2, v0, v4

    if-lez v2, :cond_3

    move-wide v0, v4

    :cond_3
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    cmpg-double v2, v0, v4

    if-gez v2, :cond_4

    move-wide v0, v4

    .line 559
    :cond_4
    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v2, v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setScale(D)V

    .line 562
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setLeft(I)V

    .line 563
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setTop(I)V

    .line 565
    iput-boolean v3, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mFitSizeMode:Z

    return-void

    :cond_5
    :goto_1
    return-void
.end method

.method public getCurrentPrttargetScale()D
    .locals 2

    .line 409
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getScale()D

    move-result-wide v0

    return-wide v0
.end method

.method public getImageAndLayout(Z)Lcom/epson/cameracopy/printlayout/ImageAndLayout;
    .locals 13

    .line 488
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    const-wide/16 v2, 0x0

    cmpg-double v4, v0, v2

    if-gtz v4, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 492
    :cond_0
    new-instance v10, Lcom/epson/cameracopy/printlayout/ImageAndLayout;

    invoke-direct {v10}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;-><init>()V

    if-eqz p1, :cond_1

    .line 498
    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessDataWidth:I

    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessDataHeight:I

    invoke-virtual {v10, p1, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setLayoutAreaSize(II)V

    .line 500
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getLeft()I

    move-result p1

    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr p1, v0

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v0, v2

    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessLeftMargin:I

    int-to-double v2, p1

    add-double/2addr v0, v2

    .line 502
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getTop()I

    move-result p1

    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr p1, v2

    int-to-double v2, p1

    iget-wide v4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v2, v4

    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessTopMargin:I

    int-to-double v4, p1

    add-double/2addr v2, v4

    move-wide v5, v0

    move-wide v7, v2

    goto :goto_0

    .line 506
    :cond_1
    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->m3mPrintAreaActualWidth:I

    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->m3mPrintAreaActualHeight:I

    invoke-virtual {v10, p1, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setLayoutAreaSize(II)V

    .line 509
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getLeft()I

    move-result p1

    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr p1, v0

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v0, v2

    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperTo3mActualLeftMargin:I

    int-to-double v2, p1

    sub-double/2addr v0, v2

    .line 511
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getTop()I

    move-result p1

    iget-object v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr p1, v2

    int-to-double v2, p1

    iget-wide v4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v2, v4

    iget p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperTo3mActualTopMargin:I

    int-to-double v4, p1

    sub-double/2addr v2, v4

    move-wide v5, v0

    move-wide v7, v2

    .line 515
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getWidth()I

    move-result p1

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double v1, v0, v2

    iget-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    .line 516
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getHeight()I

    move-result p1

    int-to-double v3, p1

    iget-wide v11, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v3, v11

    .line 518
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getPrtimageRotation()I

    move-result v9

    move-object v0, v10

    .line 515
    invoke-virtual/range {v0 .. v9}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setLayout(DDDDI)V

    return-object v10
.end method

.method public getPaperMaginAnd3mPrintableArea()[I
    .locals 7

    .line 229
    iget v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperTo3mActualLeftMargin:I

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    mul-double v0, v0, v2

    double-to-int v0, v0

    .line 230
    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperTo3mActualTopMargin:I

    int-to-double v4, v1

    mul-double v4, v4, v2

    double-to-int v1, v4

    .line 231
    iget v4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->m3mPrintAreaActualWidth:I

    int-to-double v4, v4

    mul-double v4, v4, v2

    double-to-int v4, v4

    .line 232
    iget v5, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->m3mPrintAreaActualHeight:I

    int-to-double v5, v5

    mul-double v5, v5, v2

    double-to-int v2, v5

    const/4 v3, 0x4

    .line 234
    new-array v3, v3, [I

    const/4 v5, 0x0

    aput v0, v3, v5

    const/4 v0, 0x1

    aput v1, v3, v0

    const/4 v0, 0x2

    aput v4, v3, v0

    const/4 v0, 0x3

    aput v2, v3, v0

    return-object v3
.end method

.method public getPreviewPaperRect()Landroid/graphics/Rect;
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPreviewPrtimageRect()Landroid/graphics/Rect;
    .locals 2

    .line 365
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getLeft()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 366
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getRight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 367
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getTop()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 368
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getBottom()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 370
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScaledPrtimagePreviewRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPrintSizeAndPosition()Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;
    .locals 9

    .line 467
    iget-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    const-wide/16 v2, 0x0

    cmpg-double v4, v0, v2

    if-gtz v4, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 470
    :cond_0
    new-instance v0, Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    .line 471
    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getWidth()I

    move-result v1

    int-to-double v1, v1

    iget-wide v3, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v1, v3

    double-to-int v2, v1

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    .line 472
    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getHeight()I

    move-result v1

    int-to-double v3, v1

    iget-wide v5, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v3, v5

    double-to-int v3, v3

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    .line 473
    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getLeft()I

    move-result v1

    iget-object v4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v4

    int-to-double v4, v1

    iget-wide v6, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v4, v6

    double-to-int v4, v4

    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    .line 474
    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getTop()I

    move-result v1

    iget-object v5, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPreviewPaperRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v5

    int-to-double v5, v1

    iget-wide v7, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mActualPreviewRate:D

    div-double/2addr v5, v7

    double-to-int v5, v5

    .line 475
    invoke-virtual {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getPrtimageRotation()I

    move-result v6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/epson/cameracopy/printlayout/PreviewView$SizeAndPosition;-><init>(IIIII)V

    return-object v0
.end method

.method public getPrtImageActualSIze()[I
    .locals 3

    const/4 v0, 0x2

    .line 130
    new-array v0, v0, [I

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualWidth:I

    const/4 v2, 0x0

    aput v1, v0, v2

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualHeight:I

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object v0
.end method

.method public getPrtimageRotation()I
    .locals 2

    .line 377
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->getRotation()I

    move-result v0

    iget v1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrintTargetAddRotation:I

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public isFitSize()Z
    .locals 1

    .line 574
    iget-boolean v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mFitSizeMode:Z

    return v0
.end method

.method public move(II)V
    .locals 1

    .line 346
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0, p1, p2}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->move(II)V

    .line 347
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->adjustPrtimagePosition()V

    return-void
.end method

.method public resetPrtimagePosition()V
    .locals 0

    .line 460
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->initPreviewPrtimageRect()V

    return-void
.end method

.method public resetRotation()V
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->resetRotate()V

    .line 394
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->adjustPrtimagePosition()V

    return-void
.end method

.method public rotateRight90()V
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->rotateRight90()V

    .line 386
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->adjustPrtimagePosition()V

    return-void
.end method

.method public setBorderless(Z)V
    .locals 0

    .line 583
    iput-boolean p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessMode:Z

    return-void
.end method

.method public setPaperSize(IIIIIIIIII)V
    .locals 0

    .line 207
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualWidth:I

    .line 208
    iput p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperActualHeight:I

    .line 210
    iput p3, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperTo3mActualLeftMargin:I

    .line 211
    iput p4, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPaperTo3mActualTopMargin:I

    .line 212
    iput p5, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->m3mPrintAreaActualWidth:I

    .line 213
    iput p6, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->m3mPrintAreaActualHeight:I

    .line 215
    iput p7, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessLeftMargin:I

    .line 216
    iput p8, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessTopMargin:I

    .line 218
    iput p9, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessDataWidth:I

    .line 219
    iput p10, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mBorderlessDataHeight:I

    return-void
.end method

.method public setPrintTargetAddRotation(I)V
    .locals 0

    .line 402
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrintTargetAddRotation:I

    return-void
.end method

.method public setPrtimageActualSize(II)V
    .locals 0

    .line 117
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualWidth:I

    .line 118
    iput p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimageActualHeight:I

    return-void
.end method

.method public setScaleFactor(D)V
    .locals 5

    const-wide/high16 v0, 0x4010000000000000L    # 4.0

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    cmpg-double v4, p1, v2

    if-gez v4, :cond_0

    move-wide p1, v2

    goto :goto_0

    :cond_0
    cmpl-double v2, p1, v0

    if-lez v2, :cond_1

    move-wide p1, v0

    .line 424
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mPrtimagePreviewPosition:Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;

    invoke-virtual {v0, p1, p2}, Lcom/epson/cameracopy/printlayout/PreviewPosition$PreviewRect;->setScale(D)V

    .line 425
    invoke-direct {p0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->adjustPrtimagePosition()V

    return-void
.end method

.method public setScreenSize(II)V
    .locals 0

    .line 143
    iput p1, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenWidth:I

    .line 144
    iput p2, p0, Lcom/epson/cameracopy/printlayout/PreviewPosition;->mScreenHeight:I

    return-void
.end method

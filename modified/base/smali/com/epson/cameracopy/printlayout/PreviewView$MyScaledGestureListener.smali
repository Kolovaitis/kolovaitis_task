.class Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "PreviewView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/printlayout/PreviewView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyScaledGestureListener"
.end annotation


# instance fields
.field private mBaseFactor:D

.field private final mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/printlayout/PreviewView;)V
    .locals 0

    .line 413
    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 414
    iput-object p1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6

    .line 426
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    .line 427
    iget-object v1, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {v1}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$000(Lcom/epson/cameracopy/printlayout/PreviewView;)Lcom/epson/cameracopy/printlayout/PreviewPosition;

    move-result-object v1

    float-to-double v2, v0

    iget-wide v4, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;->mBaseFactor:D

    mul-double v2, v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->setScaleFactor(D)V

    .line 429
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$100(Lcom/epson/cameracopy/printlayout/PreviewView;)V

    .line 430
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->invalidate()V

    .line 432
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScale(Landroid/view/ScaleGestureDetector;)Z

    move-result p1

    return p1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .line 419
    iget-object v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-static {v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->access$000(Lcom/epson/cameracopy/printlayout/PreviewView;)Lcom/epson/cameracopy/printlayout/PreviewPosition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewPosition;->getCurrentPrttargetScale()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/printlayout/PreviewView$MyScaledGestureListener;->mBaseFactor:D

    .line 421
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result p1

    return p1
.end method

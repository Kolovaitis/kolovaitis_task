.class Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;
.super Ljava/lang/Object;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageCollectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataDrawPreview"
.end annotation


# instance fields
.field public mBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field public mMatrixBitmap:Landroid/graphics/Matrix;

.field public mPointInitOffset:Landroid/graphics/PointF;

.field public mPointMoveDeff:[Landroid/graphics/PointF;

.field public mPointMovePoint:Landroid/graphics/PointF;

.field public mRectBitmap:Landroid/graphics/RectF;

.field public mRectDrawScale:Landroid/graphics/RectF;

.field public mScale:[F

.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImageCollectView;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V
    .locals 4

    .line 117
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 108
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 109
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mMatrixBitmap:Landroid/graphics/Matrix;

    .line 110
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectBitmap:Landroid/graphics/RectF;

    .line 111
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    .line 112
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointInitOffset:Landroid/graphics/PointF;

    .line 113
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMovePoint:Landroid/graphics/PointF;

    .line 114
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    .line 118
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mMatrixBitmap:Landroid/graphics/Matrix;

    .line 119
    new-instance p1, Landroid/graphics/RectF;

    const/4 v0, 0x0

    invoke-direct {p1, v0, v0, v0, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectBitmap:Landroid/graphics/RectF;

    .line 120
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1, v0, v0, v0, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    .line 121
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointInitOffset:Landroid/graphics/PointF;

    .line 122
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1, v0, v0}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMovePoint:Landroid/graphics/PointF;

    const/4 p1, 0x2

    .line 123
    new-array v1, p1, [Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    .line 124
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, v0}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 125
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, v0}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v0, 0x1

    aput-object v2, v1, v0

    .line 126
    new-array p1, p1, [F

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    .line 127
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, p1, v3

    .line 128
    aput v1, p1, v0

    return-void
.end method

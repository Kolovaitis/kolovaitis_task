.class public Lcom/epson/cameracopy/ui/PrintPreviewActivity;
.super Lepson/print/ActivityIACommon;
.source "PrintPreviewActivity.java"

# interfaces
.implements Lepson/print/CommonDefine;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;
.implements Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final DIALOG_KEY_STORE_DIALOG:Ljava/lang/String; = "store-dialog"

.field private static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field private static final KEY_PREVIEW_VIEW:Ljava/lang/String; = "preview-view"

.field private static final MESSAGE_MANUSCRIPT_SCALE_TEXT_UPDATE:I = 0x8

.field public static final PARAM_DISPLAY_SIZE_SET_MESSAGE:Ljava/lang/String; = "display-sizeset-message"

.field public static final PARAM_IMAGE_LIST:Ljava/lang/String; = "imageList"

.field private static final REQUEST_CODE_NFC_PRINTER_CHANGE:I = 0x4

.field private static final REQUEST_CODE_ORG_PAPER_SIZE_SELECT:I = 0x3

.field private static final REQUEST_CODE_PRINT:I = 0x1

.field private static final REQUEST_CODE_SETTING:I = 0x2


# instance fields
.field private final LOAD_ALL_IMAGE:I

.field private final OUT_OF_MEMORY_ERROR:I

.field private final PREVIEW_LOAD_DIALOG_DISMISS:I

.field private final PREVIEW_LOAD_DIALOG_SHOW:I

.field private final PRINT_MODE_CAMERACOPY:Lepson/print/screen/PrintSetting$Kind;

.field private final START_PRINT:I

.field private final ZOOM_CONTROL:I

.field private aPaperSourceSetting:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/screen/PaperSourceSetting;",
            ">;"
        }
    .end annotation
.end field

.field bAutoStartPrint:Z

.field private backButton:Landroid/widget/Button;

.field private currentColor:I

.field private currentLayout:I

.field private currentPaperSize:I

.field private currentPrinterName:Ljava/lang/String;

.field private enableShowWarning:Z

.field private final mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private final mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field mHandler:Landroid/os/Handler;

.field private final mImageList:Lepson/print/EPImageList;

.field private mIsRemotePrinter:Z

.field mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mOriginalImageSizeButton:Landroid/widget/Button;

.field private mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

.field mPrintTargetPaperSizeId:I

.field private mPrintTargetSizeText:Landroid/widget/TextView;

.field private mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

.field private mScaleText:Landroid/widget/TextView;

.field private paperMissmath:Landroid/widget/ImageView;

.field paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

.field private papersizeTextView:Landroid/widget/TextView;

.field private printButton:Landroid/widget/Button;

.field private printerId:Ljava/lang/String;

.field private printsettingButton:Landroid/widget/ImageButton;

.field private reloadTask:Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;

.field private rotateButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .line 92
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 116
    iput v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->PREVIEW_LOAD_DIALOG_SHOW:I

    const/4 v1, 0x1

    .line 117
    iput v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->PREVIEW_LOAD_DIALOG_DISMISS:I

    const/4 v2, 0x2

    .line 118
    iput v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->ZOOM_CONTROL:I

    const/4 v3, 0x3

    .line 119
    iput v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->LOAD_ALL_IMAGE:I

    const/4 v3, 0x5

    .line 120
    iput v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->START_PRINT:I

    const/4 v3, 0x7

    .line 121
    iput v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->OUT_OF_MEMORY_ERROR:I

    .line 124
    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->PRINT_MODE_CAMERACOPY:Lepson/print/screen/PrintSetting$Kind;

    const/4 v3, 0x0

    .line 126
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->backButton:Landroid/widget/Button;

    .line 127
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printsettingButton:Landroid/widget/ImageButton;

    .line 128
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->papersizeTextView:Landroid/widget/TextView;

    .line 129
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    .line 130
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->rotateButton:Landroid/widget/Button;

    .line 131
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printButton:Landroid/widget/Button;

    .line 132
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperMissmath:Landroid/widget/ImageView;

    .line 138
    new-instance v4, Lepson/print/EPImageList;

    invoke-direct {v4}, Lepson/print/EPImageList;-><init>()V

    iput-object v4, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    const-string v4, ""

    .line 139
    iput-object v4, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPrinterName:Ljava/lang/String;

    const-string v4, ""

    .line 140
    iput-object v4, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printerId:Ljava/lang/String;

    .line 142
    iput v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentLayout:I

    .line 143
    iput v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPaperSize:I

    .line 144
    iput v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentColor:I

    .line 148
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mIsRemotePrinter:Z

    .line 149
    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->enableShowWarning:Z

    .line 152
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    .line 155
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->reloadTask:Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;

    const/4 v1, -0x2

    .line 158
    iput v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPrintTargetPaperSizeId:I

    .line 165
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->bAutoStartPrint:Z

    .line 913
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    .line 1217
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 1218
    new-instance v0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 1245
    new-instance v0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$8;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$8;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->showStoreDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->showProgressDialog()V

    return-void
.end method

.method static synthetic access$1000(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Z
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->isRemotePrinter()Z

    move-result p0

    return p0
.end method

.method static synthetic access$1100(Lcom/epson/cameracopy/ui/PrintPreviewActivity;Z)V
    .locals 0

    .line 92
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->loadPrinterSettingAndChangeView(Z)V

    return-void
.end method

.method static synthetic access$1200(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Landroid/widget/Button;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printButton:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$200(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startLoadtask()V

    return-void
.end method

.method static synthetic access$400(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->disablePrintButton()V

    return-void
.end method

.method static synthetic access$500(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startPrint()V

    return-void
.end method

.method static synthetic access$600(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/EPImageList;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    return-object p0
.end method

.method static synthetic access$700(Lcom/epson/cameracopy/ui/PrintPreviewActivity;Landroid/os/Message;)V
    .locals 0

    .line 92
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->updatePaperDisplay(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$800(Lcom/epson/cameracopy/ui/PrintPreviewActivity;D)V
    .locals 0

    .line 92
    invoke-direct {p0, p1, p2}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->updateManuscriptScaleText(D)V

    return-void
.end method

.method static synthetic access$900(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$902(Lcom/epson/cameracopy/ui/PrintPreviewActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method private checkEPImage(Lepson/print/EPImage;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 385
    :cond_0
    iget v1, p1, Lepson/print/EPImage;->srcWidth:I

    if-lez v1, :cond_2

    iget p1, p1, Lepson/print/EPImage;->srcHeight:I

    if-gtz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_0
    return v0
.end method

.method private checkSizeSetMessage()V
    .locals 3

    .line 315
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v1, "display-sizeset-message"

    const/4 v2, 0x0

    .line 320
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0e03a6

    .line 322
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x8

    .line 323
    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 324
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    return-void
.end method

.method private disablePrintButton()V
    .locals 2

    .line 569
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 904
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 906
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private dismissProgressDialog()V
    .locals 2

    .line 817
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method private enablePrintButton()V
    .locals 2

    .line 565
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private getImageListFromIntent()V
    .locals 5

    .line 344
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    invoke-virtual {v0}, Lepson/print/EPImageList;->clear()V

    .line 346
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 350
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->finish()V

    return-void

    :cond_0
    const-string v1, "imageList"

    .line 354
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    .line 358
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->finish()V

    return-void

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 362
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 363
    iget-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Lepson/print/EPImageList;->add(Ljava/lang/String;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 367
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    invoke-virtual {v0}, Lepson/print/EPImageList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 368
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    invoke-virtual {v0, v1}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0

    .line 369
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->checkEPImage(Lepson/print/EPImage;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 371
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    invoke-virtual {v0}, Lepson/print/EPImageList;->clear()V

    .line 372
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->finish()V

    return-void

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method private isPrinterChanged()Z
    .locals 5

    .line 580
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->isRemotePrinter()Z

    move-result v0

    .line 581
    iget-boolean v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mIsRemotePrinter:Z

    const/4 v2, 0x1

    if-eq v1, v0, :cond_0

    return v2

    :cond_0
    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 585
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-nez v0, :cond_1

    return v2

    :cond_1
    const-string v3, "PRINTER_NAME"

    const v4, 0x7f0e04d6

    .line 591
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 590
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    return v2

    .line 595
    :cond_2
    iget-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPrinterName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    return v2

    :cond_3
    return v1
.end method

.method private isRemotePrinter()Z
    .locals 1

    .line 887
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$onCreate$0(Lcom/epson/cameracopy/ui/PrintPreviewActivity;Ljava/util/Deque;)V
    .locals 2

    .line 186
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 188
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 189
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 192
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 196
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 197
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private loadPrinterSettingAndChangeView(Z)V
    .locals 7

    .line 465
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->isRemotePrinter()Z

    move-result v0

    const-string v1, "PrintSetting"

    const/4 v2, 0x0

    .line 467
    invoke-virtual {p0, v1, v2}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    if-nez v1, :cond_1

    .line 469
    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mIsRemotePrinter:Z

    if-eq p1, v0, :cond_0

    .line 470
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mIsRemotePrinter:Z

    :cond_0
    return-void

    :cond_1
    const-string v3, "PRINTER_NAME"

    const v4, 0x7f0e04d6

    .line 478
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 477
    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 480
    new-instance v4, Lepson/print/screen/PrintSetting;

    sget-object v5, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v4, p0, v5}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 481
    invoke-virtual {v4}, Lepson/print/screen/PrintSetting;->loadSettings()V

    const-string v5, "PRINTER_ID"

    const/4 v6, 0x0

    .line 489
    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printerId:Ljava/lang/String;

    const/4 v1, 0x1

    if-nez p1, :cond_3

    .line 491
    iget-boolean v5, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mIsRemotePrinter:Z

    if-eq v5, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    const/4 v5, 0x0

    goto :goto_1

    .line 495
    :cond_3
    :goto_0
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mIsRemotePrinter:Z

    const/4 v0, 0x1

    const/4 v5, 0x1

    :goto_1
    if-nez p1, :cond_4

    .line 498
    iget-object v6, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPrinterName:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 502
    :cond_4
    iput-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPrinterName:Ljava/lang/String;

    const/4 v0, 0x1

    const/4 v5, 0x1

    :cond_5
    if-nez p1, :cond_6

    .line 505
    iget v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPaperSize:I

    iget v6, v4, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    if-eq v3, v6, :cond_8

    .line 509
    :cond_6
    iget v0, v4, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    iput v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPaperSize:I

    .line 510
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    if-eqz v0, :cond_7

    .line 511
    iget v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPaperSize:I

    invoke-virtual {v0, v3}, Lcom/epson/cameracopy/printlayout/PreviewView;->setPaperSize(I)V

    :cond_7
    const/4 v0, 0x1

    const/4 v5, 0x1

    :cond_8
    if-nez p1, :cond_9

    .line 515
    iget v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentLayout:I

    iget v6, v4, Lepson/print/screen/PrintSetting;->layoutValue:I

    if-eq v3, v6, :cond_c

    .line 519
    :cond_9
    iget v0, v4, Lepson/print/screen/PrintSetting;->layoutValue:I

    iput v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentLayout:I

    .line 520
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    if-eqz v0, :cond_b

    .line 522
    iget v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentLayout:I

    if-ne v0, v1, :cond_a

    const/4 v2, 0x1

    .line 525
    :cond_a
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {v0, v2}, Lcom/epson/cameracopy/printlayout/PreviewView;->setBorderless(Z)V

    :cond_b
    const/4 v0, 0x1

    const/4 v5, 0x1

    :cond_c
    if-nez p1, :cond_e

    .line 529
    iget p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentColor:I

    iget v2, v4, Lepson/print/screen/PrintSetting;->colorValue:I

    if-eq p1, v2, :cond_d

    goto :goto_2

    :cond_d
    move v1, v5

    goto :goto_3

    .line 535
    :cond_e
    :goto_2
    iget p1, v4, Lepson/print/screen/PrintSetting;->colorValue:I

    iput p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentColor:I

    .line 536
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    if-eqz p1, :cond_f

    .line 537
    iget v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentColor:I

    invoke-virtual {p1, v2}, Lcom/epson/cameracopy/printlayout/PreviewView;->setColor(I)V

    :cond_f
    :goto_3
    if-eqz v0, :cond_11

    .line 543
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->updateManuscriptSize()Z

    .line 545
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    if-eqz p1, :cond_10

    .line 547
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->printPostionResetRequest()V

    .line 549
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 550
    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getPixelWidth()I

    move-result v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 551
    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getPixelHeight()I

    move-result v1

    .line 549
    invoke-virtual {p1, v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewView;->setPrintTargetSize(II)V

    .line 552
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->update()V

    .line 554
    :cond_10
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->updateParaSizeText()V

    goto :goto_4

    :cond_11
    if-eqz v1, :cond_12

    .line 557
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    if-eqz p1, :cond_12

    .line 558
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->update()V

    :cond_12
    :goto_4
    return-void
.end method

.method private setPaperSizeDisplayForNoPapaerInfoPrinter()V
    .locals 3

    .line 878
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->papersizeTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 879
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPrintTargetSizeText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 880
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mScaleText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 882
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperMissmath:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private setPreferencesDocumentMode()V
    .locals 3

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 1059
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1060
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SOURCE_TYPE"

    const/4 v2, 0x1

    .line 1061
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1062
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 2

    const v0, 0x7f0e04d9

    .line 895
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 896
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 897
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showProgressDialog()V
    .locals 2

    .line 812
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method private showStoreDialog()V
    .locals 3

    .line 306
    invoke-static {}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->newInstance()Lcom/epson/mobilephone/common/ReviewInvitationDialog;

    move-result-object v0

    .line 307
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "store-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startLoadtask()V
    .locals 2

    .line 806
    new-instance v0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->reloadTask:Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;

    .line 807
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->reloadTask:Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private startPrint()V
    .locals 3

    .line 823
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 825
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->getImageAndLayout()Lcom/epson/cameracopy/printlayout/ImageAndLayout;

    move-result-object v0

    .line 826
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v1

    iget-object v1, v1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setOrgFileName(Ljava/lang/String;)V

    .line 828
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 829
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 834
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "image_and_layout"

    .line 835
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, 0x1

    .line 836
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updateManuscriptScaleText(D)V
    .locals 8

    .line 441
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    invoke-virtual {v0, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getPhysicalSize(Landroid/content/Context;)Landroid/graphics/PointF;

    move-result-object v0

    .line 443
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    invoke-virtual {v1, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getUnitType(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const-string v1, "%s: %.0f%% (%.1f x %.1f %s)"

    goto :goto_0

    :cond_0
    const-string v1, "%s: %.0f%% (%.0f x %.0f %s)"

    .line 446
    :goto_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const v6, 0x7f0e03a3

    .line 447
    invoke-virtual {p0, v6}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double v6, v6, p1

    .line 448
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    iget v5, v0, Landroid/graphics/PointF;->x:F

    float-to-double v5, v5

    mul-double v5, v5, p1

    .line 449
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x3

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-double v5, v0

    mul-double v5, v5, p1

    .line 450
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    aput-object p1, v4, v2

    const/4 p1, 0x4

    iget-object p2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 451
    invoke-virtual {p2, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getUnitStr(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v4, p1

    .line 446
    invoke-static {v3, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 452
    iget-object p2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mScaleText:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateManuscriptSize()Z
    .locals 9

    .line 408
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 410
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    const/4 v2, 0x0

    .line 411
    invoke-virtual {v1, v2}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v1

    iget-object v1, v1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 410
    invoke-static {p0, v1}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->load(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 413
    iget-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    invoke-virtual {v0, v3}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 417
    :goto_0
    iget-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    invoke-virtual {v3}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->displayLength()Z

    move-result v3

    const v4, 0x7f0e031d

    const/4 v5, 0x2

    if-eqz v3, :cond_2

    .line 419
    iget-object v3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    invoke-virtual {v3, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getDocumentSize(Landroid/content/Context;)Landroid/graphics/PointF;

    move-result-object v3

    .line 421
    iget-object v6, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    invoke-virtual {v6, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getUnitType(Landroid/content/Context;)I

    move-result v6

    if-ne v6, v5, :cond_1

    const-string v6, "%s: %s (%.1f x %.1f %s)"

    goto :goto_1

    :cond_1
    const-string v6, "%s: %s (%.0f x %.0f %s)"

    .line 423
    :goto_1
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    .line 424
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v2

    iget-object v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 425
    invoke-virtual {v2, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v1

    iget v1, v3, Landroid/graphics/PointF;->x:F

    .line 426
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v8, v5

    const/4 v1, 0x3

    iget v2, v3, Landroid/graphics/PointF;->y:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 427
    invoke-virtual {v2, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getUnitStr(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v1

    .line 423
    invoke-static {v7, v6, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 428
    iget-object v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPrintTargetSizeText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 430
    :cond_2
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s: %s"

    new-array v5, v5, [Ljava/lang/Object;

    .line 431
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v2

    iget-object v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 432
    invoke-virtual {v2, p0}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 430
    invoke-static {v3, v6, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 433
    iget-object v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPrintTargetSizeText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return v0
.end method

.method private updatePaperDisplay(Landroid/os/Message;)V
    .locals 2

    .line 845
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "PAPERSOURCEINFO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->aPaperSourceSetting:Ljava/util/ArrayList;

    .line 846
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->aPaperSourceSetting:Ljava/util/ArrayList;

    if-eqz p1, :cond_1

    .line 849
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->papersizeTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 850
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPrintTargetSizeText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 851
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mScaleText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 854
    new-instance p1, Lepson/print/screen/PrintSetting;

    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {p1, p0, v0}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 855
    invoke-virtual {p1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 857
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->aPaperSourceSetting:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, v1}, Lepson/print/screen/PaperSourceInfo;->checkPaperMissmatch(Lepson/print/screen/PrintSetting;Ljava/util/ArrayList;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 859
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperMissmath:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 861
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperMissmath:Landroid/widget/ImageView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 865
    :cond_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->setPaperSizeDisplayForNoPapaerInfoPrinter()V

    :goto_0
    return-void
.end method

.method private updateParaSizeText()V
    .locals 4

    .line 392
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    .line 393
    iget v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPaperSize:I

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;->getStringId(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 394
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->papersizeTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0e0405

    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public callPrintSetting()V
    .locals 2

    .line 1067
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1073
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    .line 1076
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x2

    .line 1078
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method check3GAndStartPrint()V
    .locals 2

    .line 801
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public invitationDialogClicked(Z)V
    .locals 0

    .line 1140
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->setStartStoreEnd()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 607
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_3

    .line 661
    iput-object p3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->aPaperSourceSetting:Ljava/util/ArrayList;

    .line 662
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->setPaperSizeDisplayForNoPapaerInfoPrinter()V

    const/4 p1, 0x1

    .line 667
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->bAutoStartPrint:Z

    .line 670
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startLoadtask()V

    goto :goto_1

    .line 645
    :pswitch_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->updateManuscriptSize()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 649
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    iget-object p2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    invoke-virtual {p2}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getPixelWidth()I

    move-result p2

    iget-object p3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mManuscriptSize:Lcom/epson/cameracopy/printlayout/ManuscriptSize;

    .line 650
    invoke-virtual {p3}, Lcom/epson/cameracopy/printlayout/ManuscriptSize;->getPixelHeight()I

    move-result p3

    .line 649
    invoke-virtual {p1, p2, p3}, Lcom/epson/cameracopy/printlayout/PreviewView;->setPrintTargetSize(II)V

    .line 651
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->update()V

    return-void

    :pswitch_2
    const/4 p1, 0x3

    if-eq p2, p1, :cond_1

    return-void

    .line 623
    :cond_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->enablePrintButton()V

    .line 625
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->isPrinterChanged()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 629
    iput-object p3, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->aPaperSourceSetting:Ljava/util/ArrayList;

    .line 630
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->setPaperSizeDisplayForNoPapaerInfoPrinter()V

    .line 633
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startLoadtask()V

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 636
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->loadPrinterSettingAndChangeView(Z)V

    :goto_0
    return-void

    .line 611
    :pswitch_3
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->enablePrintButton()V

    .line 614
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 615
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-static {p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isPrintSuccess(I)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->onPrintEnd(Z)V

    return-void

    :cond_3
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 965
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    return-void

    .line 1009
    :sswitch_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->rotateImageR90()V

    .line 1010
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/PreviewView;->update()V

    return-void

    .line 968
    :sswitch_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->isRemotePrinter()Z

    move-result p1

    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mIsRemotePrinter:Z

    .line 969
    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mIsRemotePrinter:Z

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->enableShowWarning:Z

    if-ne p1, v1, :cond_0

    .line 970
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 971
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0e0362

    .line 972
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x7f0e0363

    .line 973
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 972
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e052b

    .line 974
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/PrintPreviewActivity$5;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$5;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04e6

    .line 981
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/PrintPreviewActivity$4;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$4;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 987
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 988
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->currentPrinterName:Ljava/lang/String;

    const v1, 0x7f0e04d6

    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 990
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 991
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e043a

    .line 992
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e043b

    .line 993
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    .line 994
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/PrintPreviewActivity$6;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$6;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1000
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 1002
    :cond_1
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->check3GAndStartPrint()V

    :goto_0
    return-void

    .line 1018
    :sswitch_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->aPaperSourceSetting:Ljava/util/ArrayList;

    if-eqz p1, :cond_2

    .line 1020
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 1021
    const-class v0, Lepson/print/screen/PaperSourceSettingScr;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v0, "PAPERSOURCEINFO"

    .line 1022
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->aPaperSourceSetting:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "print-setting-type"

    .line 1024
    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    .line 1025
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v1

    .line 1024
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x2

    .line 1026
    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    .line 1030
    :cond_2
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->callPrintSetting()V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f08018d -> :sswitch_2
        0x7f08024e -> :sswitch_2
        0x7f080268 -> :sswitch_1
        0x7f08028e -> :sswitch_2
        0x7f0802bc -> :sswitch_0
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 1103
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 176
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a001e

    .line 177
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->setContentView(I)V

    .line 180
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lepson/common/DialogProgressViewModel;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lepson/common/DialogProgressViewModel;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 181
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {v0}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/-$$Lambda$PrintPreviewActivity$ptufwm_MwK6kADSAC3PDvGbo_sk;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/-$$Lambda$PrintPreviewActivity$ptufwm_MwK6kADSAC3PDvGbo_sk;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 202
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    .line 203
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->getShowInvitationLiveData()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/PrintPreviewActivity$1;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$1;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    const/4 v0, 0x1

    const v1, 0x7f0e0435

    .line 213
    invoke-virtual {p0, v1, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->setActionBar(IZ)V

    .line 216
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 219
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getImageListFromIntent()V

    .line 221
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lepson/print/EPImageList;->size()I

    move-result v1

    if-eq v1, v0, :cond_0

    goto/16 :goto_0

    :cond_0
    const v1, 0x7f080264

    .line 230
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/epson/cameracopy/printlayout/PreviewView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    const v1, 0x7f08024e

    .line 235
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->papersizeTextView:Landroid/widget/TextView;

    .line 236
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->papersizeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08028e

    .line 239
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPrintTargetSizeText:Landroid/widget/TextView;

    .line 240
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPrintTargetSizeText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0802d4

    .line 243
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mScaleText:Landroid/widget/TextView;

    .line 244
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mScaleText:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08018d

    .line 247
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperMissmath:Landroid/widget/ImageView;

    .line 248
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperMissmath:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08024b

    .line 251
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mOriginalImageSizeButton:Landroid/widget/Button;

    .line 252
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mOriginalImageSizeButton:Landroid/widget/Button;

    new-instance v2, Lcom/epson/cameracopy/ui/PrintPreviewActivity$2;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$2;-><init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0802bc

    .line 259
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->rotateButton:Landroid/widget/Button;

    .line 260
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->rotateButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f080268

    .line 263
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printButton:Landroid/widget/Button;

    .line 264
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->printButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 267
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mImageList:Lepson/print/EPImageList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v2

    iget-object v2, v2, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/epson/cameracopy/printlayout/PreviewView;->setImage(Ljava/lang/String;)V

    .line 270
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->loadPrinterSettingAndChangeView(Z)V

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    .line 272
    invoke-direct {p0, v1, v2}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->updateManuscriptScaleText(D)V

    .line 273
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    invoke-virtual {v1, p0}, Lcom/epson/cameracopy/printlayout/PreviewView;->setPrttargetScaleChangeListener(Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;)V

    const-string v1, "PREFS_EPSON_CONNECT"

    .line 276
    invoke-virtual {p0, v1, v3}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "ENABLE_SHOW_WARNING"

    .line 277
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->enableShowWarning:Z

    .line 284
    invoke-static {p0}, Lepson/print/screen/PaperSourceInfo;->getInstance(Landroid/content/Context;)Lepson/print/screen/PaperSourceInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    .line 287
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v1, :cond_1

    .line 290
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lepson/print/service/EpsonService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1, v2, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_1
    if-nez p1, :cond_2

    .line 297
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x3

    const-wide/16 v1, 0x12c

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 300
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->checkSizeSetMessage()V

    :cond_2
    return-void

    .line 224
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->finish()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 1050
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1051
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0008

    .line 1052
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "ActivityImageViewSelect"

    const-string v1, "onDestroy"

    .line 733
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 737
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 739
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I

    .line 741
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 742
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 744
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .line 1084
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    const-string p1, "key search"

    const-string p2, "diable"

    .line 1086
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 1089
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x4

    if-ne p1, p2, :cond_1

    .line 1091
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->onBackPressed()V

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .line 699
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onNewIntent(Landroid/content/Intent;)V

    .line 701
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 703
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->reloadTask:Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v1, v2, :cond_0

    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    .line 705
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTag(Landroid/content/Context;Landroid/content/Intent;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 714
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 715
    const-class v1, Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "connectInfo"

    .line 716
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "changeMode"

    const/4 v1, 0x1

    .line 717
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p1, 0x4

    .line 718
    invoke-virtual {p0, v0, p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 1038
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080021

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 1040
    :cond_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->callPrintSetting()V

    .line 1044
    :goto_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 3

    const-string v0, "ActivityImageViewSelect"

    const-string v1, "onPause"

    .line 752
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 757
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 758
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->reloadTask:Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 759
    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->cancel(Z)Z

    .line 762
    :cond_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->dismissProgressDialog()V

    .line 764
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 765
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 766
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 767
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 768
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 769
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 772
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/PreviewView;->setPrttargetScaleChangeListener(Lcom/epson/cameracopy/printlayout/PreviewView$PrttargetScaleChangeListener;)V

    .line 773
    iput-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    .line 776
    :cond_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    .line 779
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    invoke-virtual {v0}, Lepson/print/screen/PaperSourceInfo;->stop()V

    return-void
.end method

.method public onPrttargetScaleChange(D)V
    .locals 1

    .line 1133
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    .line 1134
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const/16 p2, 0x8

    .line 1133
    invoke-virtual {v0, p2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    .line 1135
    iget-object p2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p2, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected onRestart()V
    .locals 1

    .line 678
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onRestart()V

    .line 679
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    if-eqz v0, :cond_0

    .line 680
    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/PreviewView;->restoreBitmap()V

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1113
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1115
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    const-string v1, "preview-view"

    invoke-virtual {v0, p1, v1}, Lcom/epson/cameracopy/printlayout/PreviewView;->restoreInstanceState(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 687
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x0

    .line 689
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    .line 692
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperMissmath:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 693
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1}, Lepson/print/screen/PaperSourceInfo;->start(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1121
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->mPreviewView:Lcom/epson/cameracopy/printlayout/PreviewView;

    const-string v1, "preview-view"

    invoke-virtual {v0, p1, v1}, Lcom/epson/cameracopy/printlayout/PreviewView;->saveInstanceState(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1123
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 785
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 788
    new-instance v0, Lepson/print/screen/PrintSetting;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, p0, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 789
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->resetPrintSettings()V

    .line 792
    :cond_0
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    return-void
.end method

.method protected startOrgImageSizeActivity()V
    .locals 2

    .line 335
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x3

    .line 336
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

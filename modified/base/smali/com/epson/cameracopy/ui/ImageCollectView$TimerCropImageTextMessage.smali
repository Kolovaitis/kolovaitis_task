.class Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;
.super Ljava/lang/Object;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageCollectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TimerCropImageTextMessage"
.end annotation


# static fields
.field private static final CROP_IMAGE_TIMER_LIMIT:I = 0x1388

.field private static final CROP_IMAGE_TIMER_TIME:I = 0xc8

.field private static final MSG_TIMER_CROP_IMAGE_TEXT_MESSAGE_TIMEOUT:I = 0x1f5


# instance fields
.field private mBreak:Z

.field private mHandlerTimerCropImageTextMessage:Landroid/os/Handler;

.field private mThreadTimer:Ljava/lang/Thread;

.field private mView:Landroid/view/View;

.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImageCollectView;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V
    .locals 0

    .line 1779
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 1783
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mThreadTimer:Ljava/lang/Thread;

    .line 1784
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mView:Landroid/view/View;

    const/4 p1, 0x0

    .line 1785
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mBreak:Z

    .line 1828
    new-instance p1, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage$2;

    invoke-direct {p1, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage$2;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mHandlerTimerCropImageTextMessage:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$300(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;)Z
    .locals 0

    .line 1779
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mBreak:Z

    return p0
.end method

.method static synthetic access$400(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;)Landroid/os/Handler;
    .locals 0

    .line 1779
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mHandlerTimerCropImageTextMessage:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$600(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;)Landroid/view/View;
    .locals 0

    .line 1779
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mView:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method public Timer(Landroid/view/View;)V
    .locals 1

    .line 1797
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mView:Landroid/view/View;

    .line 1800
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mThreadTimer:Ljava/lang/Thread;

    if-nez p1, :cond_0

    .line 1802
    new-instance p1, Ljava/lang/Thread;

    new-instance v0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage$1;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage$1;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;)V

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mThreadTimer:Ljava/lang/Thread;

    .line 1823
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mThreadTimer:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public TimerOff()V
    .locals 1

    .line 1788
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mThreadTimer:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1789
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mBreak:Z

    .line 1790
    :goto_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;->mThreadTimer:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    return-void
.end method

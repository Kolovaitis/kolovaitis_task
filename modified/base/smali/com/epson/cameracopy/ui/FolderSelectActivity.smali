.class public Lcom/epson/cameracopy/ui/FolderSelectActivity;
.super Lepson/print/ActivityIACommon;
.source "FolderSelectActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final MSG_UPDATE_LIST_VIEW:I = 0x385

.field private static final REQEST_RUNTIMEPERMMISSION:I = 0x1

.field private static mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static mFolderSelectActivity:Lcom/epson/cameracopy/ui/FolderSelectActivity;

.field private static mHandler:Landroid/os/Handler;


# instance fields
.field listViewListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mFolderInformation:Lcom/epson/cameracopy/ui/FolderInformation;

.field private mFolderSelectBase:Landroid/widget/LinearLayout;

.field private mFolderSelectCurrent:Landroid/widget/LinearLayout;

.field private mFolderSelectText:Landroid/widget/TextView;

.field private mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

.field private mItemDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/cameracopy/ui/ItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

.field private mRootFolder:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 185
    new-instance v0, Lcom/epson/cameracopy/ui/FolderSelectActivity$2;

    invoke-direct {v0}, Lcom/epson/cameracopy/ui/FolderSelectActivity$2;-><init>()V

    sput-object v0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 45
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mRootFolder:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mListView:Landroid/widget/ListView;

    .line 53
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderInformation:Lcom/epson/cameracopy/ui/FolderInformation;

    .line 55
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectBase:Landroid/widget/LinearLayout;

    .line 56
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectCurrent:Landroid/widget/LinearLayout;

    .line 57
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectText:Landroid/widget/TextView;

    .line 58
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

    .line 60
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mItemDataList:Ljava/util/ArrayList;

    .line 61
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

    .line 222
    new-instance v0, Lcom/epson/cameracopy/ui/FolderSelectActivity$3;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/FolderSelectActivity$3;-><init>(Lcom/epson/cameracopy/ui/FolderSelectActivity;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->listViewListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/FolderSelectActivity;)Lcom/epson/cameracopy/ui/FolderInformation;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderInformation:Lcom/epson/cameracopy/ui/FolderInformation;

    return-object p0
.end method

.method static synthetic access$100()Lcom/epson/cameracopy/ui/FolderSelectActivity;
    .locals 1

    .line 45
    sget-object v0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectActivity:Lcom/epson/cameracopy/ui/FolderSelectActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/epson/cameracopy/ui/FolderSelectActivity;Landroid/os/Message;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->executeMessage(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$300()Landroid/os/Handler;
    .locals 1

    .line 45
    sget-object v0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private executeMessage(Landroid/os/Message;)V
    .locals 1

    .line 197
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0x385

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->updateListView()V

    :goto_0
    return-void
.end method

.method private updateListView()V
    .locals 3

    const/4 v0, 0x0

    .line 209
    :goto_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectBase:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 210
    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectBase:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 211
    iget-object v2, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectBase:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 216
    :cond_1
    :goto_1
    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mListView:Landroid/widget/ListView;

    .line 217
    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 218
    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->listViewListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 219
    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectBase:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method initFolder()V
    .locals 8

    .line 151
    :try_start_0
    new-instance v5, Lcom/epson/cameracopy/ui/CurrentFolderName;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectCurrent:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectText:Landroid/widget/TextView;

    invoke-direct {v5, v0, v1}, Lcom/epson/cameracopy/ui/CurrentFolderName;-><init>(Landroid/widget/LinearLayout;Landroid/widget/TextView;)V

    const/4 v2, 0x0

    .line 153
    new-instance v7, Lcom/epson/cameracopy/ui/FolderInformation;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mRootFolder:Ljava/lang/String;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mItemDataList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/epson/cameracopy/ui/FolderInformation;-><init>(Ljava/lang/String;ILcom/epson/cameracopy/ui/ItemDataAdapter;Ljava/util/ArrayList;Lcom/epson/cameracopy/ui/CurrentFolderName;Lcom/epson/cameracopy/ui/MenuControl;)V

    iput-object v7, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderInformation:Lcom/epson/cameracopy/ui/FolderInformation;

    .line 155
    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderInformation:Lcom/epson/cameracopy/ui/FolderInformation;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/FolderInformation;->UpdateFolder()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 159
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->setResult(ILandroid/content/Intent;)V

    .line 160
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->finish()V

    :goto_0
    return-void
.end method

.method public onActionbarMenuNewClicked(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onActionbarMenuidOkClicked(Landroid/view/View;)V
    .locals 2

    .line 299
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderInformation:Lcom/epson/cameracopy/ui/FolderInformation;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/FolderInformation;->GetCurrentFolder()Ljava/io/File;

    move-result-object p1

    .line 300
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "SELECT_Folder"

    .line 301
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 302
    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->setResult(ILandroid/content/Intent;)V

    .line 303
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->finish()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 166
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_1

    const/4 p1, 0x0

    .line 175
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->setResult(I)V

    .line 176
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->finish()V

    goto :goto_0

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->initFolder()V

    :goto_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 0

    .line 333
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    const/4 p1, 0x1

    return p1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 67
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a002a

    .line 68
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->setContentView(I)V

    const/4 p1, 0x1

    const v0, 0x7f0e0376

    .line 71
    invoke-virtual {p0, v0, p1}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->setActionBar(IZ)V

    .line 74
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mActivity:Ljava/lang/ref/WeakReference;

    .line 75
    sget-object v0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/ui/FolderSelectActivity;

    sput-object v0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectActivity:Lcom/epson/cameracopy/ui/FolderSelectActivity;

    const v0, 0x7f080152

    .line 88
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectBase:Landroid/widget/LinearLayout;

    const v0, 0x7f080153

    .line 89
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectCurrent:Landroid/widget/LinearLayout;

    const v0, 0x7f080154

    .line 90
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderSelectText:Landroid/widget/TextView;

    .line 93
    new-instance v0, Lcom/epson/cameracopy/ui/MenuControl;

    invoke-direct {v0, p0, p1}, Lcom/epson/cameracopy/ui/MenuControl;-><init>(Lcom/epson/cameracopy/ui/FolderSelectActivity;I)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

    .line 96
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mRootFolder:Ljava/lang/String;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mItemDataList:Ljava/util/ArrayList;

    .line 100
    new-instance v0, Lcom/epson/cameracopy/ui/ItemDataAdapter;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mItemDataList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2, v1}, Lcom/epson/cameracopy/ui/ItemDataAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

    .line 101
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->updateListView()V

    .line 126
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 127
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    .line 128
    new-array v3, v1, [Ljava/lang/String;

    const v4, 0x7f0e0422

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p1

    .line 129
    new-array v1, v1, [Ljava/lang/String;

    const v4, 0x7f0e0420

    .line 130
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    .line 131
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0e0424

    invoke-virtual {p0, v5}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, p1

    .line 137
    new-instance v4, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v2, v0, v2

    invoke-direct {v4, v2, v3, v1}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 139
    invoke-static {p0, v0}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    invoke-static {p0, v4, p1}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 145
    :cond_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->initFolder()V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .line 315
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 317
    check-cast p2, Landroid/widget/ListView;

    .line 319
    iget-object p2, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mFolderInformation:Lcom/epson/cameracopy/ui/FolderInformation;

    invoke-virtual {p2}, Lcom/epson/cameracopy/ui/FolderInformation;->IsFolder()Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "\u9078\u629e"

    const/4 p3, 0x0

    .line 323
    invoke-interface {p1, p3, p3, p3, p2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 p2, 0x1

    const-string v0, "\u524a\u9664"

    .line 324
    invoke-interface {p1, p3, p2, p3, v0}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderSelectActivity;->mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/ui/MenuControl;->CreateMenu(Landroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 281
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080233

    if-eq v0, v1, :cond_0

    .line 293
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    .line 283
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->onActionbarMenuidOkClicked(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1
.end method

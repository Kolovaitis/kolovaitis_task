.class Lcom/epson/cameracopy/ui/CropImageActivity$4;
.super Ljava/lang/Object;
.source "CropImageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;->executeMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V
    .locals 0

    .line 859
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .line 863
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result v0

    .line 864
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getWidth()D

    move-result-wide v1

    .line 865
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getHeight()D

    move-result-wide v3

    .line 868
    iget-object v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v5}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$600(Lcom/epson/cameracopy/ui/CropImageActivity;)Lcom/epson/cameracopy/ui/ImageCollectView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetCropPoint()[Landroid/graphics/PointF;

    move-result-object v5

    .line 872
    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-eqz v6, :cond_1

    if-eqz v5, :cond_1

    .line 873
    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    aget-object v6, v6, v7

    iget v6, v6, Landroid/graphics/PointF;->x:F

    aget-object v9, v5, v7

    iget v9, v9, Landroid/graphics/PointF;->x:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 874
    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    aget-object v6, v6, v7

    iget v6, v6, Landroid/graphics/PointF;->y:F

    aget-object v9, v5, v7

    iget v9, v9, Landroid/graphics/PointF;->y:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 875
    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/PointF;->x:F

    aget-object v9, v5, v8

    iget v9, v9, Landroid/graphics/PointF;->x:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 876
    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/PointF;->y:F

    aget-object v9, v5, v8

    iget v9, v9, Landroid/graphics/PointF;->y:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 877
    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    const/4 v9, 0x2

    aget-object v6, v6, v9

    iget v6, v6, Landroid/graphics/PointF;->x:F

    aget-object v10, v5, v9

    iget v10, v10, Landroid/graphics/PointF;->x:F

    cmpl-float v6, v6, v10

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 878
    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    aget-object v6, v6, v9

    iget v6, v6, Landroid/graphics/PointF;->y:F

    aget-object v9, v5, v9

    iget v9, v9, Landroid/graphics/PointF;->y:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 879
    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    const/4 v9, 0x3

    aget-object v6, v6, v9

    iget v6, v6, Landroid/graphics/PointF;->x:F

    aget-object v10, v5, v9

    iget v10, v10, Landroid/graphics/PointF;->x:F

    cmpl-float v6, v6, v10

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 880
    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v6

    aget-object v6, v6, v9

    iget v6, v6, Landroid/graphics/PointF;->y:F

    aget-object v9, v5, v9

    iget v9, v9, Landroid/graphics/PointF;->y:F

    cmpl-float v6, v6, v9

    if-eqz v6, :cond_1

    :cond_0
    const/4 v7, 0x1

    :cond_1
    if-nez v7, :cond_4

    .line 888
    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$800(Lcom/epson/cameracopy/ui/CropImageActivity;)I

    move-result v6

    const/4 v9, -0x1

    if-eq v6, v9, :cond_3

    if-ne v0, v9, :cond_2

    goto :goto_0

    .line 894
    :cond_2
    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$900(Lcom/epson/cameracopy/ui/CropImageActivity;)D

    move-result-wide v9

    cmpl-double v6, v9, v1

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$1000(Lcom/epson/cameracopy/ui/CropImageActivity;)D

    move-result-wide v9

    cmpl-double v6, v9, v3

    if-eqz v6, :cond_4

    goto :goto_1

    .line 889
    :cond_3
    :goto_0
    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$800(Lcom/epson/cameracopy/ui/CropImageActivity;)I

    move-result v6

    if-eq v6, v0, :cond_4

    goto :goto_1

    :cond_4
    move v8, v7

    .line 900
    :cond_5
    :goto_1
    iget-object v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v6, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$802(Lcom/epson/cameracopy/ui/CropImageActivity;I)I

    .line 901
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v0, v1, v2}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$902(Lcom/epson/cameracopy/ui/CropImageActivity;D)D

    .line 902
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v0, v3, v4}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$1002(Lcom/epson/cameracopy/ui/CropImageActivity;D)D

    if-eqz v8, :cond_6

    .line 906
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v0, v5}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$702(Lcom/epson/cameracopy/ui/CropImageActivity;[Landroid/graphics/PointF;)[Landroid/graphics/PointF;

    .line 908
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity$4;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/CropImageActivity;->mDeviceSize:Lorg/opencv/core/Size;

    invoke-virtual {v0, v1, v2, v3}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageCROP([Landroid/graphics/PointF;Lorg/opencv/core/Size;Lorg/opencv/core/Size;)V

    .line 909
    invoke-static {}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$100()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x32b

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 912
    :cond_6
    invoke-static {}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$100()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x32c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_2
    return-void
.end method

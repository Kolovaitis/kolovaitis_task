.class Lcom/epson/cameracopy/ui/CurrentFolderName;
.super Ljava/lang/Object;
.source "FolderSelectActivity.java"


# instance fields
.field mFolderSelectCurrent:Landroid/widget/LinearLayout;

.field mFolderSelectFile:Ljava/io/File;

.field mFolderSelectText:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/LinearLayout;Landroid/widget/TextView;)V
    .locals 1

    .line 388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 384
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectCurrent:Landroid/widget/LinearLayout;

    .line 385
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectText:Landroid/widget/TextView;

    .line 386
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectFile:Ljava/io/File;

    .line 389
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectCurrent:Landroid/widget/LinearLayout;

    .line 390
    iput-object p2, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectText:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public GetFile()Ljava/io/File;
    .locals 1

    .line 411
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectFile:Ljava/io/File;

    return-object v0
.end method

.method public SetFile(Ljava/io/File;)V
    .locals 1

    .line 398
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectFile:Ljava/io/File;

    .line 399
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public SetText(Ljava/lang/String;)V
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public Show(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 404
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectCurrent:Landroid/widget/LinearLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 406
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CurrentFolderName;->mFolderSelectCurrent:Landroid/widget/LinearLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.class public Lcom/epson/cameracopy/ui/CropImageActivity;
.super Lepson/print/ActivityIACommon;
.source "CropImageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/CropImageActivity$EPxLog;,
        Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;,
        Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;,
        Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;,
        Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;
    }
.end annotation


# static fields
.field private static final ACTIVITY_MODE_COLOR_ADJUSTMENT:I = 0x2

.field private static final ACTIVITY_MODE_CROP_IMAGE:I = 0x0

.field private static final ACTIVITY_MODE_ENHANCE_TEXT:I = 0x1

.field private static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field private static final ICPD_FOLDER_PRINT_DATA:Ljava/lang/String; = "ic"

.field public static final IMAGE_FILE_NAME:Ljava/lang/String; = "image_file_name"

.field private static final MESSAGE_BOX_TYPE_CANCEL:I = 0x67

.field private static final MESSAGE_BOX_TYPE_OK:I = 0x66

.field private static final MESSAGE_BOX_TYPE_YES_NO:I = 0x65

.field private static final MSG_DEBUG_MODE:I = 0x186

.field private static final MSG_DEBUG_RESOLUTION_THRESHOLD:I = 0x187

.field private static final MSG_DEBUG_SMALL_PREVIEW_RATE:I = 0x188

.field private static final MSG_PREVIEW_CROP:I = 0x321

.field public static final MSG_PREVIEW_EDGE_DETECTION:I = 0x12d

.field private static final MSG_PREVIEW_LOAD_DIALOG_DISMISS:I = 0x386

.field private static final MSG_PREVIEW_LOAD_DIALOG_SHOW:I = 0x385

.field private static final MSG_PREVIEW_LOAD_FINISH:I = 0x390

.field private static final MSG_PREVIEW_LOAD_PHOTO:I = 0x38f

.field private static final MSG_PREVIEW_PRINT_PREVIEW_EXECUTE:I = 0x349

.field private static final MSG_PREVIEW_PRINT_PREVIEW_FINISH:I = 0x34a

.field private static final MSG_PREVIEW_SAVE_EXECUTE:I = 0x33f

.field private static final MSG_PREVIEW_SAVE_FINISH:I = 0x340

.field public static final MSG_PREVIEW_TOUCH_EVENT_ACTION_DOWN:I = 0x137

.field private static final MSG_SDIC_BCSC_FIX_CANCEL:I = 0x339

.field private static final MSG_SDIC_BCSC_FIX_PROC:I = 0x338

.field private static final MSG_SDIC_BCSC_FIX_QUIT:I = 0x33a

.field private static final MSG_SDIC_BCSC_RESET:I = 0x337

.field private static final MSG_SDIC_PWC_EXECUTE:I = 0x32b

.field private static final MSG_SDIC_PWC_FINISH:I = 0x32c

.field private static final REQUEST_CODE_ORG_PAPER_SIZE_SELECT:I = 0x3

.field private static final REQUEST_RUNTIMEPERMMISSION:I = 0x4

.field public static final RESULT_COLLECTED_FILE_NAME:Ljava/lang/String; = "corrected_file_name"

.field private static final SEEK_BAR_STEP:I = 0xa

.field private static mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static mCropImageActivity:Lcom/epson/cameracopy/ui/CropImageActivity;

.field private static mHandler:Landroid/os/Handler;


# instance fields
.field private mActivityMode:I

.field private mBcscBDefault:F

.field private mBcscBValue:F

.field private mBcscCDefault:F

.field private mBcscCValue:F

.field private mBcscSDefault:F

.field private mBcscSValue:F

.field private mBcscSeekBarBDefault:I

.field private mBcscSeekBarCDefault:I

.field private mBcscSeekBarSDefault:I

.field private mBcscType:I

.field private mBitmapPreview:Landroid/graphics/Bitmap;

.field private mButtonColorAdjustment:Landroid/widget/Button;

.field private mButtonDocSize:Landroid/widget/Button;

.field private mButtonExecute:Landroid/widget/Button;

.field private mButtonReset:Landroid/widget/Button;

.field private mButtonSave:Landroid/widget/Button;

.field private mCacheFolder:Ljava/io/File;

.field private mCorrectionHistory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;",
            ">;"
        }
    .end annotation
.end field

.field private mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

.field private mCropImageFrameLayout:Landroid/widget/FrameLayout;

.field private mCropImageHeader:Landroid/view/View;

.field mCropImageMessageShow:Z

.field private mCurrentPaperId:I

.field mCurrentPaperSize:Lorg/opencv/core/Size;

.field private mCustomToast:Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

.field private mDaemonView:Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;

.field private mDebugID:I

.field private mDebugMode:I

.field private mDebugResolutionThreshold:I

.field private mDebugSmallPreviewRate:I

.field mDeviceSize:Lorg/opencv/core/Size;

.field mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mImageCollect:Lepson/colorcorrection/ImageCollect;

.field private mImageCollectAdjustmentBrightness:Landroid/widget/LinearLayout;

.field private mImageCollectAdjustmentBrightnessSeekBar:Landroid/widget/SeekBar;

.field private mImageCollectAdjustmentBrightnessTitle:Landroid/widget/TextView;

.field private mImageCollectAdjustmentContrast:Landroid/widget/LinearLayout;

.field private mImageCollectAdjustmentContrastSeekBar:Landroid/widget/SeekBar;

.field private mImageCollectAdjustmentContrastTitle:Landroid/widget/TextView;

.field private mImageCollectAdjustmentSaturation:Landroid/widget/LinearLayout;

.field private mImageCollectAdjustmentSaturationSeekBar:Landroid/widget/SeekBar;

.field private mImageCollectAdjustmentSaturationTitle:Landroid/widget/TextView;

.field private mImageCollectAdjustmentSwitch:Landroid/widget/RadioGroup;

.field private mImageCollectAdjustmentSwitchId:I

.field private mImageCollectAdjustmentSwitchLeft:Landroid/widget/RadioButton;

.field private mImageCollectAdjustmentSwitchRight:Landroid/widget/RadioButton;

.field private mImageCollectLayout:Landroid/widget/RelativeLayout;

.field private mImageCollectPalletButton:Landroid/widget/Button;

.field private mImageCollectPalletLayout:Landroid/widget/LinearLayout;

.field private mImageCollectPaperSizeInfo:Landroid/widget/TextView;

.field private mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

.field private mImageCollectSwitch:Landroid/widget/RadioGroup;

.field private mImageCollectSwitchId:I

.field private mImageCollectSwitchLayout:Landroid/widget/LinearLayout;

.field private mImageCollectSwitchLeft:Landroid/widget/RadioButton;

.field private mImageCollectSwitchMiddle:Landroid/widget/RadioButton;

.field private mImageCollectSwitchRight:Landroid/widget/RadioButton;

.field private mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

.field private mImageType:I

.field mInfoPaper:Lepson/common/Info_paper;

.field private mMaskType:I

.field private mMessageDialog:Landroid/app/AlertDialog;

.field private mMetrics:Landroid/util/DisplayMetrics;

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mPaperHeight:D

.field private mPaperId:I

.field mPaperSize:Lorg/opencv/core/Size;

.field private mPaperWidth:D

.field private mPointPreviewCropPoint:[Landroid/graphics/PointF;

.field private mPreviewType:I

.field private mProgressDialog:Lepson/common/DialogProgress;

.field private mSaveApprovalTable:[Z

.field private mToolBartLayout:Landroid/widget/LinearLayout;

.field private mToolbarColorAdjustment:Landroid/widget/LinearLayout;

.field private mToolbarCropImage:Landroid/widget/LinearLayout;

.field private mToolbarEnhanceText:Landroid/widget/LinearLayout;

.field private mUserFolder:Ljava/io/File;

.field private final onGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 815
    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$2;

    invoke-direct {v0}, Lcom/epson/cameracopy/ui/CropImageActivity$2;-><init>()V

    sput-object v0, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 76
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 138
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageFrameLayout:Landroid/widget/FrameLayout;

    .line 140
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectLayout:Landroid/widget/RelativeLayout;

    .line 141
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    .line 142
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    .line 143
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    .line 144
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    .line 146
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchLayout:Landroid/widget/LinearLayout;

    .line 147
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitch:Landroid/widget/RadioGroup;

    .line 148
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchLeft:Landroid/widget/RadioButton;

    .line 149
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchMiddle:Landroid/widget/RadioButton;

    .line 150
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchRight:Landroid/widget/RadioButton;

    .line 151
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletLayout:Landroid/widget/LinearLayout;

    .line 152
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightness:Landroid/widget/LinearLayout;

    .line 153
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessTitle:Landroid/widget/TextView;

    .line 154
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessSeekBar:Landroid/widget/SeekBar;

    .line 155
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrast:Landroid/widget/LinearLayout;

    .line 156
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastTitle:Landroid/widget/TextView;

    .line 157
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastSeekBar:Landroid/widget/SeekBar;

    .line 158
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturation:Landroid/widget/LinearLayout;

    .line 159
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationTitle:Landroid/widget/TextView;

    .line 160
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationSeekBar:Landroid/widget/SeekBar;

    .line 161
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitch:Landroid/widget/RadioGroup;

    .line 162
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchLeft:Landroid/widget/RadioButton;

    .line 163
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchRight:Landroid/widget/RadioButton;

    .line 165
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageHeader:Landroid/view/View;

    .line 166
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolBartLayout:Landroid/widget/LinearLayout;

    .line 167
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    .line 168
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    .line 169
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarColorAdjustment:Landroid/widget/LinearLayout;

    .line 172
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMetrics:Landroid/util/DisplayMetrics;

    .line 173
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDeviceSize:Lorg/opencv/core/Size;

    .line 176
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    .line 177
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    .line 178
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPointPreviewCropPoint:[Landroid/graphics/PointF;

    const/4 v1, 0x2

    .line 183
    iput v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    .line 196
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mProgressDialog:Lepson/common/DialogProgress;

    .line 199
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMessageDialog:Landroid/app/AlertDialog;

    .line 202
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 203
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mInfoPaper:Lepson/common/Info_paper;

    .line 204
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    .line 205
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperSize:Lorg/opencv/core/Size;

    .line 211
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCacheFolder:Ljava/io/File;

    .line 212
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mUserFolder:Ljava/io/File;

    .line 215
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mSaveApprovalTable:[Z

    const/16 v2, -0x3e7

    .line 231
    iput v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperId:I

    const-wide/16 v2, 0x0

    .line 232
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperWidth:D

    .line 233
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperHeight:D

    .line 236
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    .line 237
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionHistory:Ljava/util/List;

    const/4 v2, 0x1

    .line 240
    iput-boolean v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageMessageShow:Z

    .line 243
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCustomToast:Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

    .line 250
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mGestureDetector:Landroid/view/GestureDetector;

    .line 254
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDaemonView:Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;

    const/4 v0, 0x0

    .line 257
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperId:I

    .line 260
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    .line 261
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugID:I

    .line 262
    iput v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugResolutionThreshold:I

    .line 263
    iput v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugSmallPreviewRate:I

    .line 733
    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$1;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$1;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->onGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    return-void
.end method

.method private AdjustmentModeProc(ZZII)V
    .locals 0

    .line 1277
    invoke-direct {p0, p1, p2}, Lcom/epson/cameracopy/ui/CropImageActivity;->StackBCSCParams(ZZ)V

    .line 1280
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscType:I

    .line 1281
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    iget p2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscType:I

    invoke-virtual {p1, p2}, Lepson/colorcorrection/ImageCollect;->SiwtchPreviewImageBCSC(I)V

    if-lez p3, :cond_0

    .line 1285
    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->ToastMessage(Ljava/lang/CharSequence;)V

    .line 1289
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {p1, p4}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetViewMode(I)V

    .line 1290
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->invalidate()V

    return-void
.end method

.method private AdjustmentModeSwitchALL()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 1260
    invoke-direct {p0, v0, v1, v0, v2}, Lcom/epson/cameracopy/ui/CropImageActivity;->AdjustmentModeProc(ZZII)V

    return-void
.end method

.method private AdjustmentModeSwitchEXECUTE()V
    .locals 4

    .line 1268
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitch:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v3, 0x7f080006

    if-ne v0, v3, :cond_0

    const/4 v0, 0x3

    .line 1269
    invoke-direct {p0, v1, v2, v1, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->AdjustmentModeProc(ZZII)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    .line 1271
    invoke-direct {p0, v2, v2, v1, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->AdjustmentModeProc(ZZII)V

    :goto_0
    return-void
.end method

.method private AdjustmentModeSwitchPART()V
    .locals 3

    const/4 v0, 0x1

    const v1, 0x7f0e046a

    const/4 v2, 0x3

    .line 1264
    invoke-direct {p0, v0, v0, v1, v2}, Lcom/epson/cameracopy/ui/CropImageActivity;->AdjustmentModeProc(ZZII)V

    return-void
.end method

.method private CancelToast()V
    .locals 1

    .line 506
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCustomToast:Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

    if-eqz v0, :cond_0

    const-string v0, " Toast Cancel"

    .line 507
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 508
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCustomToast:Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->cancel()V

    const/4 v0, 0x0

    .line 509
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCustomToast:Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

    :cond_0
    return-void
.end method

.method private GetCacheFolder(Z)Ljava/io/File;
    .locals 2

    if-eqz p1, :cond_0

    .line 1454
    new-instance p1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "ic"

    invoke-direct {p1, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1455
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    goto :goto_0

    .line 1458
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private GetPaperInfo()V
    .locals 10

    .line 1497
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lorg/opencv/core/Size;->width:D

    .line 1498
    iput-wide v1, v0, Lorg/opencv/core/Size;->height:D

    .line 1500
    invoke-static {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    move-result-object v0

    .line 1501
    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getCurrentDocumentSize()Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 1503
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result v0

    .line 1504
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getScaleId()I

    move-result v1

    const/4 v2, -0x1

    if-le v0, v2, :cond_0

    .line 1507
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result v0

    invoke-static {p0, v0}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mInfoPaper:Lepson/common/Info_paper;

    .line 1508
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mInfoPaper:Lepson/common/Info_paper;

    if-eqz v0, :cond_2

    .line 1509
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    invoke-virtual {v0}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v0

    int-to-double v2, v0

    iput-wide v2, v1, Lorg/opencv/core/Size;->width:D

    .line 1510
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mInfoPaper:Lepson/common/Info_paper;

    invoke-virtual {v1}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v1

    int-to-double v1, v1

    iput-wide v1, v0, Lorg/opencv/core/Size;->height:D

    goto :goto_0

    :cond_0
    if-ge v0, v2, :cond_2

    .line 1514
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getWidth()D

    move-result-wide v2

    .line 1515
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getHeight()D

    move-result-wide v4

    const/4 v0, 0x1

    const-wide v6, 0x4076800000000000L    # 360.0

    if-ne v1, v0, :cond_1

    .line 1517
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    const-wide v8, 0x4039666666666666L    # 25.4

    div-double/2addr v2, v8

    mul-double v2, v2, v6

    iput-wide v2, v0, Lorg/opencv/core/Size;->width:D

    div-double/2addr v4, v8

    mul-double v4, v4, v6

    .line 1518
    iput-wide v4, v0, Lorg/opencv/core/Size;->height:D

    goto :goto_0

    .line 1520
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    mul-double v2, v2, v6

    iput-wide v2, v0, Lorg/opencv/core/Size;->width:D

    mul-double v4, v4, v6

    .line 1521
    iput-wide v4, v0, Lorg/opencv/core/Size;->height:D

    :cond_2
    :goto_0
    return-void
.end method

.method private GetPreviewImage(I)V
    .locals 2

    const/4 v0, 0x4

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    if-ne p1, v0, :cond_1

    .line 1421
    :cond_0
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    .line 1422
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->SaveButtonState()V

    :cond_1
    const/4 v1, 0x6

    if-eq p1, v1, :cond_2

    if-eq p1, v0, :cond_2

    .line 1425
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPreviewType:I

    if-eq p1, v0, :cond_4

    .line 1429
    :cond_2
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    invoke-virtual {v0, p1}, Lepson/colorcorrection/ImageCollect;->GetPreviewImage(I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1434
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    .line 1435
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1438
    :cond_3
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    .line 1439
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPreviewType:I

    :cond_4
    return-void
.end method

.method private ImageCollectSwitchCROP()V
    .locals 2

    const/4 v0, 0x1

    .line 1245
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    .line 1250
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    .line 1253
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;)V

    .line 1256
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionHistory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method private ImageCollectSwitchPWC(I)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x2

    goto :goto_0

    :cond_0
    const/4 p1, 0x3

    .line 1207
    :goto_0
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    .line 1208
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;)V

    .line 1209
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionHistory:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method private InitBCSC()V
    .locals 3

    .line 1296
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->InitBCSCParams()V

    .line 1299
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->Reset()V

    .line 1302
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    invoke-virtual {v0}, Lepson/colorcorrection/ImageCollect;->ResetPreviewImageBCSC()V

    .line 1304
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscType:I

    .line 1305
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscType:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_Init(IZ)V

    .line 1308
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitch:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f080006

    if-ne v0, v1, :cond_0

    .line 1310
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetViewMode(I)V

    goto :goto_0

    .line 1313
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetViewMode(I)V

    .line 1316
    :goto_0
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    .line 1317
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private InitBCSCParams()V
    .locals 3

    .line 1321
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessSeekBar:Landroid/widget/SeekBar;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1322
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessSeekBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarBDefault:I

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1323
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1324
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastSeekBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarCDefault:I

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1325
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1326
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarSDefault:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1327
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBDefault:F

    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBValue:F

    .line 1328
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCDefault:F

    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCValue:F

    .line 1329
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSDefault:F

    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSValue:F

    return-void
.end method

.method private LoadProgressHide()V
    .locals 2

    .line 1596
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method private LoadProgressShow()V
    .locals 2

    .line 1592
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method private MakePrintData(ZI)V
    .locals 2

    .line 1465
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/epson/cameracopy/ui/CropImageActivity$13;

    invoke-direct {v1, p0, p1, p2}, Lcom/epson/cameracopy/ui/CropImageActivity$13;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;ZI)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1492
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private MessageBox(IIII)V
    .locals 6

    .line 1600
    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p4

    .line 1601
    invoke-direct/range {v0 .. v5}, Lcom/epson/cameracopy/ui/CropImageActivity;->MessageBox(IILjava/lang/String;II)V

    return-void
.end method

.method private MessageBox(IIIII)V
    .locals 6

    .line 1605
    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p4

    move v5, p5

    .line 1606
    invoke-direct/range {v0 .. v5}, Lcom/epson/cameracopy/ui/CropImageActivity;->MessageBox(IILjava/lang/String;II)V

    return-void
.end method

.method private MessageBox(IILjava/lang/String;II)V
    .locals 2

    .line 1612
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 1614
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    .line 1617
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    :cond_0
    if-eqz p3, :cond_1

    .line 1621
    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :cond_1
    const/4 p2, 0x0

    if-eq p4, v1, :cond_2

    .line 1626
    new-instance p3, Lcom/epson/cameracopy/ui/CropImageActivity$14;

    invoke-direct {p3, p0, p4}, Lcom/epson/cameracopy/ui/CropImageActivity$14;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;I)V

    goto :goto_0

    :cond_2
    move-object p3, p2

    :goto_0
    if-eq p5, v1, :cond_3

    .line 1636
    new-instance p2, Lcom/epson/cameracopy/ui/CropImageActivity$15;

    invoke-direct {p2, p0, p5}, Lcom/epson/cameracopy/ui/CropImageActivity$15;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;I)V

    :cond_3
    const/16 p4, 0x65

    if-ne p1, p4, :cond_4

    const p1, 0x7f0e052b

    .line 1645
    invoke-virtual {v0, p1, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const p1, 0x7f0e04e6

    .line 1646
    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    :cond_4
    const/16 p4, 0x66

    if-ne p1, p4, :cond_5

    const p1, 0x7f0e04f2

    .line 1648
    invoke-virtual {v0, p1, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    :cond_5
    const/16 p3, 0x67

    if-ne p1, p3, :cond_6

    const p1, 0x7f0e0476

    .line 1650
    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1653
    :cond_6
    :goto_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMessageDialog:Landroid/app/AlertDialog;

    .line 1654
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMessageDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private PreviewImageBCSC()V
    .locals 8

    .line 1389
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitch:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f080005

    if-ne v0, v1, :cond_0

    const/4 v0, 0x7

    .line 1390
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMaskType:I

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    .line 1392
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMaskType:I

    .line 1395
    :goto_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    iget v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMaskType:I

    const/4 v3, 0x1

    iget v4, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBValue:F

    iget v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCValue:F

    iget v6, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSValue:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetBcscScale()F

    move-result v7

    invoke-virtual/range {v1 .. v7}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_Exec(IZFFFF)V

    const/4 v0, 0x6

    .line 1398
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    .line 1399
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private SaveButtonDisable()V
    .locals 3

    .line 1679
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mSaveApprovalTable:[Z

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    .line 1680
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonSave:Landroid/widget/Button;

    aget-boolean v0, v0, v1

    invoke-static {v2, v0}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    return-void
.end method

.method private SaveButtonState()V
    .locals 3

    .line 1674
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonSave:Landroid/widget/Button;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mSaveApprovalTable:[Z

    iget v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    aget-boolean v1, v1, v2

    invoke-static {v0, v1}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    return-void
.end method

.method private SeekBarTracking()V
    .locals 6

    .line 1357
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    .line 1358
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    int-to-float v1, v1

    .line 1359
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3e4ccccd    # 0.2f

    mul-float v0, v0, v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v0, v4

    const v5, 0x3dcccccd    # 0.1f

    mul-float v1, v1, v5

    mul-float v2, v2, v3

    sub-float/2addr v2, v4

    .line 1368
    iget v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBValue:F

    cmpl-float v3, v0, v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCValue:F

    cmpl-float v3, v1, v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSValue:F

    cmpl-float v3, v2, v3

    if-eqz v3, :cond_1

    .line 1372
    :cond_0
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBValue:F

    .line 1373
    iput v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCValue:F

    .line 1374
    iput v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSValue:F

    .line 1377
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->PreviewImageBCSC()V

    .line 1380
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonReset:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    :cond_1
    return-void
.end method

.method private StackBCSCParams(ZZ)V
    .locals 2

    .line 1334
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBValue:F

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBDefault:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCValue:F

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCDefault:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSValue:F

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSDefault:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 1335
    :cond_0
    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    .line 1336
    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->access$1402(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;Z)Z

    .line 1337
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBValue:F

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->access$1502(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F

    .line 1338
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCValue:F

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->access$1602(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F

    .line 1339
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSValue:F

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->access$1702(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F

    if-nez p1, :cond_1

    .line 1341
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetBcscPointArray()Ljava/util/List;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->access$1802(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;Ljava/util/List;)Ljava/util/List;

    .line 1342
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetBcscScale()F

    move-result p1

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->access$1902(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F

    .line 1343
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetBcscStrokeWidth()F

    move-result p1

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->access$2002(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F

    .line 1344
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    invoke-virtual {p1}, Lepson/colorcorrection/ImageCollect;->GetMakedPreviewImageSize()Landroid/graphics/Point;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->access$2102(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 1346
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->AddParam(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;)V

    if-eqz p2, :cond_2

    .line 1350
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->InitBCSCParams()V

    :cond_2
    return-void
.end method

.method private ToastMessage(Ljava/lang/CharSequence;)V
    .locals 3

    .line 1820
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->CancelToast()V

    .line 1821
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDaemonView:Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;

    sget-object v1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->SetHandler(Landroid/os/Handler;)V

    .line 1822
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 1823
    new-instance v1, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;Lcom/epson/cameracopy/ui/CropImageActivity$1;)V

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCustomToast:Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

    .line 1824
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCustomToast:Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, p1, v2, v0}, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;II)V

    .line 1825
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCustomToast:Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->show()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/CropImageActivity;)I
    .locals 0

    .line 76
    iget p0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    return p0
.end method

.method static synthetic access$100()Landroid/os/Handler;
    .locals 1

    .line 76
    sget-object v0, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/epson/cameracopy/ui/CropImageActivity;)D
    .locals 2

    .line 76
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperHeight:D

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/epson/cameracopy/ui/CropImageActivity;D)D
    .locals 0

    .line 76
    iput-wide p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperHeight:D

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/epson/cameracopy/ui/CropImageActivity;)Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    return-object p0
.end method

.method static synthetic access$1202(Lcom/epson/cameracopy/ui/CropImageActivity;I)I
    .locals 0

    .line 76
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugResolutionThreshold:I

    return p1
.end method

.method static synthetic access$1302(Lcom/epson/cameracopy/ui/CropImageActivity;I)I
    .locals 0

    .line 76
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugSmallPreviewRate:I

    return p1
.end method

.method static synthetic access$200()Lcom/epson/cameracopy/ui/CropImageActivity;
    .locals 1

    .line 76
    sget-object v0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageActivity:Lcom/epson/cameracopy/ui/CropImageActivity;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/epson/cameracopy/ui/CropImageActivity;)I
    .locals 0

    .line 76
    iget p0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    return p0
.end method

.method static synthetic access$2300(Lcom/epson/cameracopy/ui/CropImageActivity;)Ljava/util/List;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionHistory:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$300(Lcom/epson/cameracopy/ui/CropImageActivity;Landroid/os/Message;)V
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->executeMessage(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    return-object p0
.end method

.method static synthetic access$500(Lcom/epson/cameracopy/ui/CropImageActivity;I)V
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/epson/cameracopy/ui/CropImageActivity;)Lcom/epson/cameracopy/ui/ImageCollectView;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    return-object p0
.end method

.method static synthetic access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPointPreviewCropPoint:[Landroid/graphics/PointF;

    return-object p0
.end method

.method static synthetic access$702(Lcom/epson/cameracopy/ui/CropImageActivity;[Landroid/graphics/PointF;)[Landroid/graphics/PointF;
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPointPreviewCropPoint:[Landroid/graphics/PointF;

    return-object p1
.end method

.method static synthetic access$800(Lcom/epson/cameracopy/ui/CropImageActivity;)I
    .locals 0

    .line 76
    iget p0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperId:I

    return p0
.end method

.method static synthetic access$802(Lcom/epson/cameracopy/ui/CropImageActivity;I)I
    .locals 0

    .line 76
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperId:I

    return p1
.end method

.method static synthetic access$900(Lcom/epson/cameracopy/ui/CropImageActivity;)D
    .locals 2

    .line 76
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperWidth:D

    return-wide v0
.end method

.method static synthetic access$902(Lcom/epson/cameracopy/ui/CropImageActivity;D)D
    .locals 0

    .line 76
    iput-wide p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperWidth:D

    return-wide p1
.end method

.method private activityModeChange(I)V
    .locals 8

    const v0, 0x7f0e036f

    const/4 v1, 0x2

    const v2, 0x7f080138

    const/4 v3, 0x4

    const/4 v4, 0x1

    const/16 v5, 0x8

    const/4 v6, 0x0

    if-nez p1, :cond_0

    .line 1688
    iget-object v7, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1689
    iget-object v7, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1690
    iget-object v7, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    invoke-virtual {v7, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1691
    iget-object v7, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1692
    iget-object v7, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v7, 0x7f0e0313

    .line 1697
    invoke-virtual {p0, v7}, Lcom/epson/cameracopy/ui/CropImageActivity;->setTitle(I)V

    .line 1702
    iget-object v7, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1703
    iget-object v7, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1704
    iget-object v7, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarColorAdjustment:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1707
    iget-object v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    const v7, 0x7f080114

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonDocSize:Landroid/widget/Button;

    .line 1708
    iget-object v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonDocSize:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1711
    iget-object v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    .line 1712
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    .line 1713
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1718
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {v0, v6}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetViewMode(I)V

    .line 1723
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mSaveApprovalTable:[Z

    aput-boolean v6, v0, v6

    .line 1724
    aput-boolean v4, v0, v4

    .line 1725
    aput-boolean v4, v0, v1

    const/4 v1, 0x3

    .line 1726
    aput-boolean v4, v0, v1

    .line 1727
    aput-boolean v4, v0, v3

    goto/16 :goto_0

    :cond_0
    if-ne p1, v4, :cond_1

    .line 1732
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1733
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1734
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1735
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1736
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f0e033d

    .line 1741
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->setTitle(I)V

    .line 1746
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1747
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1748
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarColorAdjustment:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1751
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    const v1, 0x7f0802cc

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonSave:Landroid/widget/Button;

    .line 1752
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonSave:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1755
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    const v1, 0x7f0800d4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonColorAdjustment:Landroid/widget/Button;

    .line 1756
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonColorAdjustment:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1759
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    .line 1760
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    const v1, 0x7f0e0434

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1761
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1766
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {v0, v4}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetViewMode(I)V

    goto :goto_0

    :cond_1
    if-ne p1, v1, :cond_2

    .line 1772
    iget-object v4, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1773
    iget-object v4, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1774
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 1775
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    const-string v4, "\u25bc"

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1776
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1777
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v3, 0x7f0e02f5

    .line 1782
    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CropImageActivity;->setTitle(I)V

    .line 1787
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1788
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1789
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarColorAdjustment:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1792
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarColorAdjustment:Landroid/widget/LinearLayout;

    const v4, 0x7f0802ab

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonReset:Landroid/widget/Button;

    .line 1793
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonReset:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1794
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonReset:Landroid/widget/Button;

    invoke-static {v3, v6}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    .line 1797
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarColorAdjustment:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    .line 1798
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    .line 1799
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonExecute:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080005

    .line 1805
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchId:I

    .line 1806
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitch:Landroid/widget/RadioGroup;

    iget v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchId:I

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 1807
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->AdjustmentModeSwitchALL()V

    .line 1812
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetViewMode(I)V

    .line 1816
    :cond_2
    :goto_0
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mActivityMode:I

    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 1842
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 1844
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private executeMessage(Landroid/os/Message;)V
    .locals 12

    .line 825
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x4

    const/4 v1, 0x2

    const/16 v2, 0x385

    const/16 v3, 0x386

    const/4 v4, 0x0

    const/4 v5, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string p1, "MSG_PREVIEW_LOAD_FINISH"

    .line 849
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 850
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    .line 851
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    invoke-virtual {p1}, Lepson/colorcorrection/ImageCollect;->GetCornerPoints()[Landroid/graphics/PointF;

    move-result-object p1

    .line 852
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;[Landroid/graphics/PointF;)V

    .line 854
    :cond_0
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 835
    :sswitch_1
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 836
    new-instance p1, Ljava/lang/Thread;

    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$3;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$3;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 845
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 831
    :sswitch_2
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->LoadProgressHide()V

    goto/16 :goto_1

    .line 827
    :sswitch_3
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->LoadProgressShow()V

    goto/16 :goto_1

    .line 1050
    :sswitch_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    invoke-virtual {p1, v5}, Lepson/colorcorrection/ImageCollect;->GetSavePath(Z)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->startPrintPreviewActivity(Ljava/lang/String;)V

    const/4 p1, -0x1

    .line 1051
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mActivityMode:I

    .line 1052
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 1045
    :sswitch_5
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/16 p1, 0x34a

    .line 1046
    invoke-direct {p0, v5, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->MakePrintData(ZI)V

    goto/16 :goto_1

    .line 1032
    :sswitch_6
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1035
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    invoke-virtual {p1, v4}, Lepson/colorcorrection/ImageCollect;->GetSavePath(Z)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const v0, 0x7f0e0394

    .line 1037
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    move-object v3, p1

    goto :goto_0

    :cond_1
    const p1, 0x7f0e0372

    .line 1039
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    move-object v3, p1

    :goto_0
    const/16 v1, 0x66

    const/4 v2, -0x1

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-object v0, p0

    .line 1041
    invoke-direct/range {v0 .. v5}, Lcom/epson/cameracopy/ui/CropImageActivity;->MessageBox(IILjava/lang/String;II)V

    goto/16 :goto_1

    .line 1009
    :sswitch_7
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 1010
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    .line 1011
    new-array v2, v1, [Ljava/lang/String;

    const v3, 0x7f0e0422

    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 1012
    new-array v1, v1, [Ljava/lang/String;

    const v3, 0x7f0e0420

    .line 1013
    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v4

    .line 1014
    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v6, 0x7f0e0424

    invoke-virtual {p0, v6}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v3, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v5

    .line 1020
    new-instance v3, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v4, p1, v4

    invoke-direct {v3, v4, v2, v1}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 1022
    invoke-static {p0, p1}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 1023
    invoke-static {p0, v3, v0}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 1028
    :cond_2
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->savePreviewExecute()V

    goto/16 :goto_1

    .line 999
    :sswitch_8
    invoke-direct {p0, v5}, Lcom/epson/cameracopy/ui/CropImageActivity;->activityModeChange(I)V

    .line 1001
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    .line 1002
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;)V

    .line 1004
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 975
    :sswitch_9
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->AdjustmentModeSwitchEXECUTE()V

    .line 976
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mSaveApprovalTable:[Z

    aput-boolean v5, p1, v0

    .line 978
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 980
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->SetImageType(I)V

    .line 981
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionHistory:Ljava/util/List;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->Clone()Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 983
    new-instance p1, Ljava/lang/Thread;

    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$6;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$6;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 992
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 966
    :sswitch_a
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->InitBCSC()V

    .line 969
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonReset:Landroid/widget/Button;

    invoke-static {p1, v4}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    goto/16 :goto_1

    :sswitch_b
    const p1, 0x7f080018

    .line 932
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchId:I

    .line 933
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitch:Landroid/widget/RadioGroup;

    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchId:I

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 934
    iput v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    .line 937
    invoke-direct {p0, v5}, Lcom/epson/cameracopy/ui/CropImageActivity;->activityModeChange(I)V

    .line 940
    invoke-direct {p0, v5}, Lcom/epson/cameracopy/ui/CropImageActivity;->ImageCollectSwitchPWC(I)V

    .line 942
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 920
    :sswitch_c
    new-instance p1, Ljava/lang/Thread;

    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$5;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$5;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 927
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 858
    :sswitch_d
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 859
    new-instance p1, Ljava/lang/Thread;

    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$4;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$4;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 916
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :sswitch_e
    const-string v5, "1/1.0"

    const-string v6, "1/1.5"

    const-string v7, "1/2.0"

    const-string v8, "1/2.5"

    const-string v9, "1/3.0"

    const-string v10, "1/3.5"

    const-string v11, "1/4.0"

    .line 1151
    filled-new-array/range {v5 .. v11}, [Ljava/lang/String;

    move-result-object p1

    .line 1152
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Small Preview Rate"

    .line 1153
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1154
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1155
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugSmallPreviewRate:I

    new-instance v2, Lcom/epson/cameracopy/ui/CropImageActivity$11;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$11;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string p1, "OK"

    .line 1184
    new-instance v1, Lcom/epson/cameracopy/ui/CropImageActivity$12;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$12;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1188
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    :sswitch_f
    const-string p1, "360dpi"

    const-string v0, "300dpi"

    const-string v1, "240dpi"

    const-string v2, "180dpi"

    const-string v3, "120dpi"

    .line 1114
    filled-new-array {p1, v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object p1

    .line 1115
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Resolution Threshold"

    .line 1116
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1117
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1118
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugResolutionThreshold:I

    new-instance v2, Lcom/epson/cameracopy/ui/CropImageActivity$9;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$9;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string p1, "OK"

    .line 1141
    new-instance v1, Lcom/epson/cameracopy/ui/CropImageActivity$10;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$10;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1145
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    :sswitch_10
    const-string p1, "Resolution Threshold"

    const-string v0, "Small Preview Rate"

    .line 1084
    filled-new-array {p1, v0}, [Ljava/lang/String;

    move-result-object p1

    .line 1088
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Menu"

    .line 1089
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1090
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1091
    new-instance v1, Lcom/epson/cameracopy/ui/CropImageActivity$7;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$7;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string p1, "Close"

    .line 1104
    new-instance v1, Lcom/epson/cameracopy/ui/CropImageActivity$8;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$8;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1108
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    .line 1079
    :sswitch_11
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->CancelToast()V

    goto :goto_1

    .line 1059
    :sswitch_12
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    .line 1061
    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetMaskImage()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1060
    invoke-virtual {v0, v1}, Lepson/colorcorrection/ImageCollect;->DetectEdge(Landroid/graphics/Bitmap;)Ljava/util/List;

    move-result-object v0

    .line 1059
    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetMaskEdge(Ljava/util/List;)V

    .line 1064
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result p1

    int-to-float p1, p1

    .line 1065
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    .line 1066
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    int-to-float v1, v1

    .line 1070
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mButtonReset:Landroid/widget/Button;

    invoke-static {v2, v5}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    .line 1072
    iget v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarBDefault:I

    int-to-float v2, v2

    cmpl-float p1, p1, v2

    if-nez p1, :cond_3

    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarCDefault:I

    int-to-float p1, p1

    cmpl-float p1, v0, p1

    if-nez p1, :cond_3

    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarSDefault:I

    int-to-float p1, p1

    cmpl-float p1, v1, p1

    if-eqz p1, :cond_4

    .line 1074
    :cond_3
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->PreviewImageBCSC()V

    :cond_4
    :goto_1
    :sswitch_13
    return-void

    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_12
        0x137 -> :sswitch_11
        0x186 -> :sswitch_10
        0x187 -> :sswitch_f
        0x188 -> :sswitch_e
        0x321 -> :sswitch_d
        0x32b -> :sswitch_c
        0x32c -> :sswitch_b
        0x337 -> :sswitch_a
        0x338 -> :sswitch_9
        0x339 -> :sswitch_13
        0x33a -> :sswitch_8
        0x33f -> :sswitch_7
        0x340 -> :sswitch_6
        0x349 -> :sswitch_5
        0x34a -> :sswitch_4
        0x385 -> :sswitch_3
        0x386 -> :sswitch_2
        0x38f -> :sswitch_1
        0x390 -> :sswitch_0
    .end sparse-switch
.end method

.method private getDocumentSizeText()Ljava/lang/String;
    .locals 7

    .line 1529
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 1532
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_1

    .line 1533
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getScaleId()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "%s (%.1f x %.1f in)"

    goto :goto_0

    :cond_0
    const-string v0, "%s (%.0f x %.0f mm)"

    .line 1535
    :goto_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 1536
    invoke-virtual {v5, p0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v5}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getWidth()D

    move-result-wide v5

    double-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v4}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getHeight()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v1

    .line 1535
    invoke-static {v2, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1540
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0, p0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public static synthetic lambda$onCreate$0(Lcom/epson/cameracopy/ui/CropImageActivity;Ljava/util/Deque;)V
    .locals 3

    .line 280
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 282
    aget-object v1, p1, v0

    const/4 v2, 0x1

    .line 283
    aget-object p1, p1, v2

    const-string v2, "do_show"

    .line 286
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287
    invoke-direct {p0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v2, "do_dismiss"

    .line 290
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 291
    invoke-direct {p0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->dismissDialog(Ljava/lang/String;)V

    const-string p1, "dismissDialog"

    .line 292
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 293
    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageMessageShow:Z

    if-eqz p1, :cond_1

    const-string p1, "TOAST"

    .line 294
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 296
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageMessageShow:Z

    const p1, 0x7f0e0312

    .line 297
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->ToastMessage(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private logRecordImageImageProcessingType()V
    .locals 1

    .line 1571
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchMiddle:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1573
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchRight:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    :goto_0
    return-void
.end method

.method private palletShow()V
    .locals 2

    .line 1582
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1583
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1584
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    const-string v1, "\u25b2"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1586
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1587
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    const-string v1, "\u25bc"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private releaseResource()V
    .locals 2

    .line 1658
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1659
    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->ReleaseResource()V

    .line 1660
    iput-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    .line 1662
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    if-eqz v0, :cond_1

    .line 1663
    invoke-virtual {v0}, Lepson/colorcorrection/ImageCollect;->ReleaseImageCollect()V

    .line 1664
    iput-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    .line 1666
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 1667
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1668
    iput-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    .line 1670
    :cond_2
    invoke-static {}, Ljava/lang/System;->gc()V

    return-void
.end method

.method private savePreviewExecute()V
    .locals 2

    .line 1198
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->SaveButtonDisable()V

    .line 1199
    sget-object v0, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x385

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    const/16 v1, 0x340

    .line 1200
    invoke-direct {p0, v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->MakePrintData(ZI)V

    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 2

    const v0, 0x7f0e0440

    .line 1833
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mProgressDialog:Lepson/common/DialogProgress;

    .line 1834
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mProgressDialog:Lepson/common/DialogProgress;

    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 1835
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mProgressDialog:Lepson/common/DialogProgress;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startPrintPreviewActivity(Ljava/lang/String;)V
    .locals 2

    .line 1547
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->releaseResource()V

    if-eqz p1, :cond_0

    .line 1557
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "corrected_file_name"

    .line 1558
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 1559
    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 1561
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->setResult(I)V

    .line 1563
    :goto_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->logRecordImageImageProcessingType()V

    .line 1564
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 p3, 0x3

    if-ne p1, p3, :cond_0

    .line 580
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPaperInfo()V

    .line 581
    iget-object p3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getDocumentSizeText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const/4 p3, 0x4

    if-ne p1, p3, :cond_2

    const/4 p1, -0x1

    if-eq p2, p1, :cond_1

    goto :goto_0

    .line 588
    :cond_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->savePreviewExecute()V

    :cond_2
    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .line 535
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mActivityMode:I

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 536
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mActivityMode:I

    .line 537
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->MessageOff()V

    .line 540
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->CancelToast()V

    .line 543
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->releaseResource()V

    .line 545
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->finish()V

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    .line 549
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->activityModeChange(I)V

    .line 550
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    invoke-virtual {v1}, Lepson/colorcorrection/ImageCollect;->GetCornerPoints()[Landroid/graphics/PointF;

    move-result-object v1

    .line 551
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    .line 552
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;[Landroid/graphics/PointF;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 556
    invoke-direct {p0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->activityModeChange(I)V

    .line 557
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    .line 558
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .line 600
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const/4 v0, -0x1

    const/16 v1, 0x65

    const/4 v2, 0x2

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_0

    .line 657
    :sswitch_0
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x33f

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :sswitch_1
    const p1, 0x7f0e044a

    const/16 v2, 0x337

    .line 670
    invoke-direct {p0, v1, v0, p1, v2}, Lcom/epson/cameracopy/ui/CropImageActivity;->MessageBox(IIII)V

    goto/16 :goto_0

    .line 608
    :sswitch_2
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mActivityMode:I

    if-nez p1, :cond_0

    .line 610
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->MessageOff()V

    .line 614
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x321

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_0
    if-ne p1, v3, :cond_1

    const p1, 0x7f0e0374

    const/16 v2, 0x349

    .line 618
    invoke-direct {p0, v1, v0, p1, v2}, Lcom/epson/cameracopy/ui/CropImageActivity;->MessageBox(IIII)V

    goto/16 :goto_0

    :cond_1
    if-ne p1, v2, :cond_6

    const/4 p1, 0x0

    .line 629
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBValue:F

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBDefault:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCValue:F

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCDefault:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSValue:F

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSDefault:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    :cond_2
    const/4 p1, 0x1

    .line 632
    :cond_3
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->Size()I

    move-result v0

    if-lez v0, :cond_4

    const/4 p1, 0x1

    :cond_4
    if-eqz p1, :cond_5

    const/16 v5, 0x65

    const/4 v6, -0x1

    const v7, 0x7f0e0375

    const/16 v8, 0x338

    const/16 v9, 0x339

    move-object v4, p0

    .line 639
    invoke-direct/range {v4 .. v9}, Lcom/epson/cameracopy/ui/CropImageActivity;->MessageBox(IIIII)V

    goto :goto_0

    .line 644
    :cond_5
    invoke-direct {p0, v3}, Lcom/epson/cameracopy/ui/CropImageActivity;->activityModeChange(I)V

    .line 645
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageType:I

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPreviewImage(I)V

    .line 646
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBitmapPreview:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 602
    :sswitch_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->MessageOff()V

    .line 603
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->setDocumentSize()V

    goto :goto_0

    .line 663
    :sswitch_4
    invoke-direct {p0, v2}, Lcom/epson/cameracopy/ui/CropImageActivity;->activityModeChange(I)V

    .line 666
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->InitBCSC()V

    goto :goto_0

    .line 692
    :sswitch_5
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchId:I

    const v0, 0x7f08001a

    if-eq p1, v0, :cond_6

    .line 695
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchId:I

    .line 696
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->ImageCollectSwitchCROP()V

    goto :goto_0

    .line 683
    :sswitch_6
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchId:I

    const v0, 0x7f080019

    if-eq p1, v0, :cond_6

    .line 686
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchId:I

    .line 687
    invoke-direct {p0, v2}, Lcom/epson/cameracopy/ui/CropImageActivity;->ImageCollectSwitchPWC(I)V

    goto :goto_0

    .line 674
    :sswitch_7
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchId:I

    const v0, 0x7f080018

    if-eq p1, v0, :cond_6

    .line 677
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchId:I

    .line 678
    invoke-direct {p0, v3}, Lcom/epson/cameracopy/ui/CropImageActivity;->ImageCollectSwitchPWC(I)V

    goto :goto_0

    .line 653
    :sswitch_8
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->palletShow()V

    goto :goto_0

    .line 710
    :sswitch_9
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchId:I

    const v0, 0x7f080006

    if-eq p1, v0, :cond_6

    .line 713
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchId:I

    .line 714
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->AdjustmentModeSwitchPART()V

    goto :goto_0

    .line 701
    :sswitch_a
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchId:I

    const v0, 0x7f080005

    if-eq p1, v0, :cond_6

    .line 704
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchId:I

    .line 705
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->AdjustmentModeSwitchALL()V

    :cond_6
    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f080005 -> :sswitch_a
        0x7f080006 -> :sswitch_9
        0x7f080011 -> :sswitch_8
        0x7f080018 -> :sswitch_7
        0x7f080019 -> :sswitch_6
        0x7f08001a -> :sswitch_5
        0x7f0800d4 -> :sswitch_4
        0x7f080114 -> :sswitch_3
        0x7f080138 -> :sswitch_2
        0x7f0802ab -> :sswitch_1
        0x7f0802cc -> :sswitch_0
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 522
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 270
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0025

    .line 271
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->setContentView(I)V

    .line 274
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgressViewModel;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 275
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/cameracopy/ui/-$$Lambda$CropImageActivity$KmVxEtxFUlwDCVSkD3evA3U5IGw;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/-$$Lambda$CropImageActivity$KmVxEtxFUlwDCVSkD3evA3U5IGw;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    const/4 p1, 0x1

    const v0, 0x7f0e0313

    .line 305
    invoke-virtual {p0, v0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->setActionBar(IZ)V

    .line 310
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/epson/cameracopy/ui/CropImageActivity;->mActivity:Ljava/lang/ref/WeakReference;

    .line 311
    sget-object v0, Lcom/epson/cameracopy/ui/CropImageActivity;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/ui/CropImageActivity;

    sput-object v0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageActivity:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 314
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    .line 317
    new-instance v0, Lorg/opencv/core/Size;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2, v1, v2}, Lorg/opencv/core/Size;-><init>(DD)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    .line 318
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetPaperInfo()V

    .line 326
    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;

    invoke-direct {v0, p0, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDaemonView:Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;

    const v0, 0x7f0800fb

    .line 327
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageFrameLayout:Landroid/widget/FrameLayout;

    .line 328
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageFrameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDaemonView:Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    const v0, 0x7f08000f

    .line 331
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectLayout:Landroid/widget/RelativeLayout;

    const v0, 0x7f080015

    .line 335
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    .line 336
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f08001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/ui/ImageCollectView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    .line 338
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectView:Lcom/epson/cameracopy/ui/ImageCollectView;

    sget-object v1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetHandler(Landroid/os/Handler;)V

    .line 339
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f080013

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    .line 340
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 341
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 342
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPaperSizeInfo:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getDocumentSizeText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPreviewLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    .line 344
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 345
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f080017

    .line 346
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f080016

    .line 347
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitch:Landroid/widget/RadioGroup;

    const v0, 0x7f080018

    .line 349
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchLeft:Landroid/widget/RadioButton;

    .line 350
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchLeft:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080019

    .line 351
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchMiddle:Landroid/widget/RadioButton;

    .line 352
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchMiddle:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08001a

    .line 353
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchRight:Landroid/widget/RadioButton;

    .line 354
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectSwitchRight:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080012

    .line 356
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectPalletLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f08000c

    .line 357
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightness:Landroid/widget/LinearLayout;

    .line 358
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightness:Landroid/widget/LinearLayout;

    const v1, 0x7f080003

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessTitle:Landroid/widget/TextView;

    .line 359
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessTitle:Landroid/widget/TextView;

    const v2, 0x7f0e02e9

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 360
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightness:Landroid/widget/LinearLayout;

    const v2, 0x7f080002

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessSeekBar:Landroid/widget/SeekBar;

    .line 361
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v0, 0x7f08000d

    .line 362
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrast:Landroid/widget/LinearLayout;

    .line 363
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrast:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastTitle:Landroid/widget/TextView;

    .line 364
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastTitle:Landroid/widget/TextView;

    const v3, 0x7f0e030a

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 365
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrast:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastSeekBar:Landroid/widget/SeekBar;

    .line 366
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentContrastSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v0, 0x7f08000e

    .line 367
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturation:Landroid/widget/LinearLayout;

    .line 368
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationTitle:Landroid/widget/TextView;

    .line 369
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationTitle:Landroid/widget/TextView;

    const v1, 0x7f0e0451

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 370
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationSeekBar:Landroid/widget/SeekBar;

    .line 371
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSaturationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v0, 0x7f080004

    .line 372
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitch:Landroid/widget/RadioGroup;

    const v0, 0x7f080005

    .line 374
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchLeft:Landroid/widget/RadioButton;

    .line 375
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchLeft:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080006

    .line 376
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchRight:Landroid/widget/RadioButton;

    .line 377
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollectAdjustmentSwitchRight:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080223

    .line 380
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageHeader:Landroid/view/View;

    const v0, 0x7f08034c

    .line 381
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolBartLayout:Landroid/widget/LinearLayout;

    .line 382
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolBartLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f08001c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    .line 383
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolBartLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f08001d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    .line 384
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolBartLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f08001b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarColorAdjustment:Landroid/widget/LinearLayout;

    .line 385
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageHeader:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 386
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageHeader:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 388
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 389
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 390
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x5

    .line 393
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarBDefault:I

    const/4 v1, 0x0

    .line 394
    iput v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarCDefault:I

    .line 395
    iput v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSeekBarSDefault:I

    const/4 v2, 0x0

    .line 396
    iput v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscBDefault:F

    .line 397
    iput v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscCDefault:F

    .line 398
    iput v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mBcscSDefault:F

    .line 399
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->InitBCSCParams()V

    .line 402
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mSaveApprovalTable:[Z

    .line 405
    invoke-direct {p0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->activityModeChange(I)V

    .line 408
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "image_file_name"

    .line 409
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 413
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMetrics:Landroid/util/DisplayMetrics;

    .line 414
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v2, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 418
    new-instance v2, Lorg/opencv/core/Size;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMetrics:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v3, v3

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v5, v5

    invoke-direct {v2, v3, v4, v5, v6}, Lorg/opencv/core/Size;-><init>(DD)V

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDeviceSize:Lorg/opencv/core/Size;

    .line 421
    new-instance v2, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionParamSet:Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    .line 422
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCorrectionHistory:Ljava/util/List;

    .line 425
    invoke-direct {p0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetCacheFolder(Z)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mUserFolder:Ljava/io/File;

    .line 426
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->GetCacheFolder(Z)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCacheFolder:Ljava/io/File;

    .line 427
    invoke-static {p0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object v2

    .line 430
    iput v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperId:I

    .line 431
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 432
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 433
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/util/Locale;->CANADA:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 434
    :cond_0
    sget-object v1, Ljava/util/Locale;->FRENCH:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 435
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperId:I

    .line 440
    :cond_1
    new-instance p1, Lorg/opencv/core/Size;

    const-wide v3, 0x40a7d00000000000L    # 3048.0

    const-wide v5, 0x40b0e10000000000L    # 4321.0

    invoke-direct {p1, v3, v4, v5, v6}, Lorg/opencv/core/Size;-><init>(DD)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperSize:Lorg/opencv/core/Size;

    .line 441
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperId:I

    invoke-static {p0, p1}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 443
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperSize:Lorg/opencv/core/Size;

    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v3

    int-to-double v3, v3

    iput-wide v3, v1, Lorg/opencv/core/Size;->width:D

    .line 444
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperSize:Lorg/opencv/core/Size;

    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height()I

    move-result p1

    int-to-double v3, p1

    iput-wide v3, v1, Lorg/opencv/core/Size;->height:D

    .line 448
    :cond_2
    new-instance p1, Lepson/colorcorrection/ImageCollect;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCurrentPaperSize:Lorg/opencv/core/Size;

    invoke-direct {p1, v0, v1, v3}, Lepson/colorcorrection/ImageCollect;-><init>(Ljava/lang/String;FLorg/opencv/core/Size;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    .line 449
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mImageCollect:Lepson/colorcorrection/ImageCollect;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCacheFolder:Ljava/io/File;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mUserFolder:Ljava/io/File;

    invoke-virtual {v2}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getSaveDirecotry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lepson/colorcorrection/ImageCollect;->SetPath(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V

    const/4 p1, -0x1

    .line 451
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mPreviewType:I

    .line 453
    sget-object p1, Lcom/epson/cameracopy/ui/CropImageActivity;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x38f

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 456
    new-instance p1, Landroid/view/GestureDetector;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->onGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {p1, p0, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .line 515
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->releaseResource()V

    .line 516
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 751
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugID:I

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mCropImageHeader:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, 0x1

    if-ne p1, v0, :cond_1

    .line 752
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    if-ne p1, v1, :cond_0

    add-int/2addr p1, v1

    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    .line 753
    :cond_0
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    add-int/2addr p1, v1

    .line 755
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    .line 756
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    const/16 v0, 0x82

    const/16 v1, 0x23

    const/16 v2, 0x8

    invoke-static {v2, v1, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 757
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    invoke-static {v2, v1, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 758
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarColorAdjustment:Landroid/widget/LinearLayout;

    invoke-static {v2, v1, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 760
    :cond_1
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugID:I

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarCropImage:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v0

    if-eq p1, v0, :cond_2

    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugID:I

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mToolbarEnhanceText:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v0

    if-ne p1, v0, :cond_4

    .line 761
    :cond_2
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    if-nez p1, :cond_3

    add-int/2addr p1, v1

    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    .line 762
    :cond_3
    iget p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    add-int/2addr p1, v1

    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugMode:I

    :cond_4
    :goto_0
    const/4 p1, 0x0

    .line 764
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugID:I

    return p1
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onPause()V
    .locals 0

    .line 490
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 491
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->d()V

    .line 493
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->CancelToast()V

    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0

    if-eqz p3, :cond_0

    .line 790
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->SeekBarTracking()V

    :cond_0
    return-void
.end method

.method public onRestart()V
    .locals 0

    .line 471
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onRestart()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .line 571
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    .line 484
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .line 565
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onStart()V
    .locals 0

    .line 478
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStart()V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 0

    .line 498
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    .line 499
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->d()V

    .line 501
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity;->CancelToast()V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 728
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mDebugID:I

    .line 729
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {p1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 p1, 0x0

    return p1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .line 528
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onWindowFocusChanged(Z)V

    return-void
.end method

.method protected setDocumentSize()V
    .locals 2

    .line 1446
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x3

    .line 1447
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

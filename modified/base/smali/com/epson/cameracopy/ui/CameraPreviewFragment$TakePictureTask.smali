.class Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;
.super Landroid/os/AsyncTask;
.source "CameraPreviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CameraPreviewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TakePictureTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mMyOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPreviewFragment;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/CameraPreviewFragment;Lcom/epson/cameracopy/device/OpenCvCameraView;)V
    .locals 0

    .line 921
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->this$0:Lcom/epson/cameracopy/ui/CameraPreviewFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 922
    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->mMyOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 918
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 928
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 931
    :goto_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->isCancelled()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->mMyOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {p1}, Lcom/epson/cameracopy/device/OpenCvCameraView;->isInAutofocus()Z

    move-result p1

    if-eqz p1, :cond_0

    const-wide/16 v0, 0x64

    .line 934
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 936
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 918
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0

    .line 949
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->this$0:Lcom/epson/cameracopy/ui/CameraPreviewFragment;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->access$700(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V

    return-void
.end method

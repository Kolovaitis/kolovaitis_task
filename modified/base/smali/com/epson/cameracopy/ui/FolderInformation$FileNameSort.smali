.class Lcom/epson/cameracopy/ui/FolderInformation$FileNameSort;
.super Ljava/lang/Object;
.source "FolderSelectActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/FolderInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileNameSort"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/FolderInformation;


# direct methods
.method private constructor <init>(Lcom/epson/cameracopy/ui/FolderInformation;)V
    .locals 0

    .line 551
    iput-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation$FileNameSort;->this$0:Lcom/epson/cameracopy/ui/FolderInformation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/cameracopy/ui/FolderInformation;Lcom/epson/cameracopy/ui/FolderInformation$1;)V
    .locals 0

    .line 551
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/FolderInformation$FileNameSort;-><init>(Lcom/epson/cameracopy/ui/FolderInformation;)V

    return-void
.end method


# virtual methods
.method public compare(Ljava/io/File;Ljava/io/File;)I
    .locals 0

    .line 554
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 551
    check-cast p1, Ljava/io/File;

    check-cast p2, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Lcom/epson/cameracopy/ui/FolderInformation$FileNameSort;->compare(Ljava/io/File;Ljava/io/File;)I

    move-result p1

    return p1
.end method

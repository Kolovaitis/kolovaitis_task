.class Lcom/epson/cameracopy/ui/CameraPrintProgress$4;
.super Ljava/lang/Object;
.source "CameraPrintProgress.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CameraPrintProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 455
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 8

    .line 458
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-boolean v0, v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCanceled:Z

    const/4 v1, 0x5

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 459
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v1, :cond_1

    const/16 p1, 0x8

    if-eq v0, p1, :cond_0

    return v2

    .line 468
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v2

    .line 464
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Landroid/content/Context;

    move-result-object v0

    const-string v3, "printer"

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v4}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$000(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 474
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x4

    const/4 v4, 0x2

    const-wide/16 v5, 0x64

    const/4 v7, 0x0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_7

    .line 587
    :pswitch_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v0, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$302(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 592
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, -0x44c

    if-eq v0, v1, :cond_4

    const/16 v1, -0x420

    const v3, 0x7f0e0053

    const v4, 0x7f0e0052

    const v5, 0x7f0e0054

    const v6, 0x7f0e0055

    if-eq v0, v1, :cond_3

    packed-switch v0, :pswitch_data_1

    packed-switch v0, :pswitch_data_2

    .line 654
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {v0, v6}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 655
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {v6, v5}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    .line 656
    invoke-virtual {v5, v4}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "0X"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 657
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    .line 658
    invoke-virtual {p1, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 614
    :pswitch_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v0, 0x7f0e0059

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 615
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v1, 0x7f0e0058

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 619
    :pswitch_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v0, 0x7f0e005c

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 620
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v1, 0x7f0e005b

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 609
    :pswitch_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v0, 0x7f0e0057

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 610
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v1, 0x7f0e0056

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 602
    :pswitch_5
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v0, 0x7f0e0050

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 603
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v1, 0x7f0e004c

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 635
    :pswitch_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v0, 0x7f0e0353

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 636
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v1, 0x7f0e0352

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 625
    :pswitch_7
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v1, 0x7f0e0049

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 626
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v4, 0x7f0e0047

    invoke-virtual {v3, v4}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "0X"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 627
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v3, 0x7f0e0048

    .line 628
    invoke-virtual {p1, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 645
    :cond_3
    :pswitch_8
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {v0, v6}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 646
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {v6, v5}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    .line 647
    invoke-virtual {v5, v4}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "0X"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 648
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    .line 649
    invoke-virtual {p1, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 594
    :cond_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v0, 0x7f0e0060

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 595
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v1, 0x7f0e005f

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 661
    :goto_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {v1, v0, p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 665
    :pswitch_9
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 666
    invoke-virtual {p1, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v3, 0x7f0e035c

    .line 667
    invoke-virtual {v1, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v3, 0x7f0e035d

    .line 668
    invoke-virtual {v1, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v3, 0x7f0e035e

    .line 669
    invoke-virtual {v1, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 667
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v1, 0x7f0e04f2

    .line 670
    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPrintProgress$4$1;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$4$1;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress$4;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 677
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_7

    .line 476
    :pswitch_a
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1202(Lcom/epson/cameracopy/ui/CameraPrintProgress;I)I

    .line 477
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1302(Lcom/epson/cameracopy/ui/CameraPrintProgress;I)I

    .line 480
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Landroid/content/Context;

    move-result-object p1

    const-string v0, "printer"

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_1

    .line 484
    :cond_5
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const-string v0, "printer"

    invoke-static {p1, v0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_6

    goto/16 :goto_7

    .line 492
    :cond_6
    :goto_1
    :pswitch_b
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_9

    :try_start_0
    const-string p1, "Epson"

    const-string v0, "begin probe printer before printing"

    .line 494
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p1

    .line 497
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    .line 498
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_7

    .line 500
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result v5

    if-ne v5, v2, :cond_7

    .line 501
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$702(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 502
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1, v0, v1, v2}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I

    goto/16 :goto_7

    :cond_7
    if-eqz v1, :cond_8

    .line 503
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    const/4 v5, 0x3

    if-ne p1, v5, :cond_8

    .line 504
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$702(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 505
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1, v0, v1, v4}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I

    goto/16 :goto_7

    .line 507
    :cond_8
    sput-boolean v7, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isContinue:Z

    const/16 p1, -0x547

    .line 508
    sput p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    .line 509
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$302(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 510
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_7

    :catch_0
    move-exception p1

    .line 513
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_7

    .line 516
    :cond_9
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xa

    invoke-virtual {p1, v0, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_7

    :pswitch_c
    const-string p1, "PrintProgress"

    .line 681
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message FINISH : mError => "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v4}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$300(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, " mCanceled =>"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-boolean v4, v4, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCanceled:Z

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_d

    .line 686
    :try_start_1
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$100()Ljava/lang/Object;

    move-result-object p1

    monitor-enter p1
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 687
    :try_start_2
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1700()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->isSearchingPrinter()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "PrintProgress"

    const-string v4, "message FINISH: mEpsonService.cancelSearchPrinter()"

    .line 688
    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I

    const/4 v0, 0x1

    goto :goto_2

    :cond_a
    const/4 v0, 0x0

    .line 692
    :goto_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 694
    :try_start_3
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_b

    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1}, Lepson/print/service/IEpsonService;->isPrinting()Z

    move-result p1

    if-eqz p1, :cond_b

    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1700()Z

    move-result p1

    if-nez p1, :cond_b

    const-string p1, "PrintProgress"

    const-string v0, "message FINISH: mEpsonService.confirmCancel(true) call"

    .line 695
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1, v2}, Lepson/print/service/IEpsonService;->confirmCancel(Z)I

    const/4 v0, 0x1

    :cond_b
    if-eqz v0, :cond_c

    .line 699
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1800(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result p1

    if-eqz p1, :cond_c

    const-string p1, "PrintProgress"

    const-string v0, "on message FINISH: sendEmptyMessageDelayed(FINISH...)"

    .line 700
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {p1, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    return v2

    :catchall_0
    move-exception v0

    .line 692
    :try_start_4
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception p1

    .line 706
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 709
    :cond_c
    :try_start_6
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_d

    const-string p1, "PrintProgress"

    const-string v0, "message FINISH: mEpsonService.unregisterCallback() call"

    .line 710
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$900()Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 712
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1900()Landroid/content/ServiceConnection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 p1, 0x0

    .line 713
    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$802(Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3

    :catch_2
    move-exception p1

    .line 716
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 722
    :cond_d
    :goto_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-boolean p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCanceled:Z

    if-eqz p1, :cond_e

    const/4 v3, 0x0

    goto :goto_4

    .line 724
    :cond_e
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$300(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result p1

    if-eqz p1, :cond_f

    const/16 v3, 0x3e8

    .line 727
    :cond_f
    :goto_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getParent()Landroid/app/Activity;

    move-result-object p1

    if-nez p1, :cond_10

    .line 728
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p1, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->setResult(I)V

    goto :goto_5

    .line 730
    :cond_10
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getParent()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/app/Activity;->setResult(I)V

    .line 733
    :goto_5
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v7}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$702(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    const-string p1, "PrintProgress"

    .line 734
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "finish with return code => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Landroid/content/Context;

    move-result-object p1

    const-string v0, "printer"

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$000(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 739
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$2000(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    .line 741
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->finish()V

    goto/16 :goto_7

    :pswitch_d
    const-string p1, "Epson"

    const-string v0, "NOTIFY ERROR"

    .line 573
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$600(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result p1

    if-nez p1, :cond_11

    const-string p1, "Epson"

    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Show Error code:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_11
    const-string p1, "Epson"

    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Remove and show Error code:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p1, v7}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->removeDialog(I)V

    .line 582
    :goto_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$602(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 583
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p1, v7}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->showDialog(I)V

    goto/16 :goto_7

    .line 554
    :pswitch_e
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v7}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$302(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 555
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_12

    .line 557
    :try_start_7
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v7}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$702(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 559
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/util/ArrayList;

    move-result-object v0

    sget-boolean v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isBkRetry:Z

    invoke-interface {p1, v0, v1}, Lepson/print/service/IEpsonService;->printWithImagesAndLayouts(Ljava/util/List;Z)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_7

    :catch_3
    move-exception p1

    .line 561
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_7

    :cond_12
    const-string p1, "Epson"

    const-string v0, "Service or resource file not ready, please wait..."

    .line 564
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "         0%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 566
    sget-object p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 567
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_7

    .line 522
    :pswitch_f
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "PERCENT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    const/16 v0, 0x64

    if-lt p1, v0, :cond_13

    const/16 p1, 0x64

    :cond_13
    if-gtz p1, :cond_16

    .line 528
    sget-object p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 529
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "         0%"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 531
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result p1

    if-le p1, v2, :cond_14

    .line 532
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPage:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v4, 0x7f0e0403

    invoke-virtual {v3, v4}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1200(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    sget-object p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1200(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result v3

    div-int/2addr v1, v3

    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 536
    :cond_14
    sget p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->copies:I

    if-le p1, v2, :cond_15

    .line 537
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCopies:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const v4, 0x7f0e030c

    invoke-virtual {v3, v4}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1300(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lcom/epson/cameracopy/ui/CameraPrintProgress;->copies:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 538
    sget-object p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1300(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    sget v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->copies:I

    div-int/2addr v1, v0

    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 541
    :cond_15
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1208(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    .line 542
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1200(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    move-result v0

    if-le p1, v0, :cond_17

    .line 543
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1202(Lcom/epson/cameracopy/ui/CameraPrintProgress;I)I

    .line 544
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1308(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I

    goto :goto_7

    .line 547
    :cond_16
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 548
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "         "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "%"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_17
    :goto_7
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_d
        :pswitch_c
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_1
        :pswitch_b
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x4b9
        :pswitch_7
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x4b6
        :pswitch_6
        :pswitch_5
        :pswitch_8
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_8
    .end packed-switch
.end method

.class Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;
.super Ljava/lang/Object;
.source "CameraPrintSettingActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V
    .locals 0

    .line 1565
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    .line 1580
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p2}, Lepson/print/service/IEpsonService$Stub;->asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    const-string p1, "SettingScr"

    const-string p2, "onServiceConnected"

    .line 1584
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1585
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1587
    :try_start_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object p2

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->registerCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1589
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    const-string p1, "SettingScr"

    const-string v0, "onServiceDisconnected"

    .line 1569
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    :try_start_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1573
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1575
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    return-void
.end method

.class Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;
.super Ljava/lang/Object;
.source "CameraPrintSettingActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V
    .locals 0

    .line 1619
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private removeCddvdMedia([I)[I
    .locals 6

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 2305
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2306
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget v4, p1, v3

    .line 2307
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x3

    .line 2310
    new-array p1, p1, [Ljava/lang/Integer;

    const/16 v1, 0x5b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v2

    const/16 v1, 0x5c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, p1, v3

    const/4 v1, 0x2

    const/16 v3, 0x5d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v1

    .line 2312
    array-length v1, p1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_3

    aget-object v4, p1, v3

    .line 2313
    :goto_2
    invoke-interface {v0, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2316
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [I

    .line 2317
    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    .line 2318
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, p1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    return-object p1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 14

    const-string v0, "SettingScr"

    const-string v1, "handleMessage call setScreenState = false"

    .line 1624
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1625
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Ljava/lang/Boolean;)V

    .line 1628
    new-instance v0, Lepson/print/screen/PrintSetting;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, v2, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1631
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x40

    const/16 v4, 0x20

    const/4 v5, 0x0

    const/4 v6, 0x3

    const-wide/16 v7, 0x64

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-eq v2, v4, :cond_41

    if-eq v2, v3, :cond_3a

    const/4 v3, 0x6

    const/4 v11, 0x5

    const/4 v12, 0x4

    const/16 v13, 0x8

    packed-switch v2, :pswitch_data_0

    packed-switch v2, :pswitch_data_1

    const-string p1, "SettingScr"

    const-string v0, "default"

    .line 2284
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2285
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v1, 0x7f0e04d6

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2286
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f080198

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2288
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Ljava/lang/Boolean;)V

    goto/16 :goto_2e

    .line 2259
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v2, -0x44c

    if-eq v0, v2, :cond_1

    const/16 v2, -0x3e8

    if-eq v0, v2, :cond_1

    .line 2269
    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    if-nez p1, :cond_0

    .line 2271
    new-array p1, v6, [Ljava/lang/Integer;

    const v0, 0x7f0e023a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v1

    const v0, 0x7f0e023b

    .line 2272
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v9

    .line 2274
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    aget-object v2, p1, v10

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v3, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2262
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f0e034a

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2263
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v1, 0x7f0e0349

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2264
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-virtual {v1, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->resetSettings()V

    goto/16 :goto_2e

    .line 1647
    :pswitch_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    const/16 v0, 0x11

    if-eqz p1, :cond_4

    .line 1651
    :try_start_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    if-eq p1, v9, :cond_3

    .line 1653
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    .line 1654
    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    if-ne v3, v6, :cond_2

    goto :goto_1

    :cond_2
    const/4 v9, 0x1

    .line 1653
    :goto_1
    invoke-interface {p1, v1, v2, v9}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-eqz p1, :cond_4b

    .line 1656
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    .line 1660
    :cond_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2e

    :catch_0
    move-exception p1

    .line 1663
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2e

    .line 1666
    :cond_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    .line 2108
    :pswitch_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_5

    .line 2109
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    :cond_5
    const-string p1, "SettingScr"

    const-string v0, "GET_COLOR"

    .line 2112
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2113
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2902(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2116
    :try_start_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    if-ne v2, v9, :cond_6

    const/4 v2, 0x1

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    :goto_2
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    .line 2117
    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    .line 2118
    invoke-static {v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v5

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v6}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v6

    .line 2116
    invoke-interface {v0, v2, v3, v5, v6}, Lepson/print/service/IEpsonService;->getColor(ZIII)[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2902(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception p1

    .line 2120
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, v0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2121
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2123
    :goto_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-nez p1, :cond_7

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2125
    :cond_7
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-gtz p1, :cond_9

    .line 2126
    :cond_8
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-array v0, v9, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2902(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2127
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2128
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v10, p1, v10

    .line 2130
    :cond_9
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_4b

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_4b

    const/4 p1, 0x0

    .line 2132
    :goto_4
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_b

    .line 2133
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    aget v2, v2, p1

    if-ne v0, v2, :cond_a

    goto :goto_5

    :cond_a
    add-int/lit8 p1, p1, 0x1

    goto :goto_4

    .line 2137
    :cond_b
    :goto_5
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_c

    .line 2139
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3102(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 2141
    :cond_c
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;-><init>()V

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2142
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2143
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2144
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2e

    .line 2058
    :pswitch_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_d

    .line 2059
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v11, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    :cond_d
    const-string p1, "SettingScr"

    const-string v0, "GET_PAPER_SOURCE"

    .line 2062
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2063
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2066
    :try_start_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v4

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v5

    invoke-interface {v0, v2, v4, v5}, Lepson/print/service/IEpsonService;->getPaperSource(III)[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_6

    :catch_2
    move-exception p1

    .line 2068
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, v0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2069
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2072
    :goto_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-nez p1, :cond_e

    .line 2073
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2074
    new-array v0, v10, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2075
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    const/16 v0, 0x80

    aput v0, p1, v1

    .line 2077
    :cond_e
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_4b

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_4b

    const/4 p1, 0x0

    .line 2079
    :goto_7
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_10

    .line 2080
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    aget v2, v2, p1

    if-ne v0, v2, :cond_f

    goto :goto_8

    :cond_f
    add-int/lit8 p1, p1, 0x1

    goto :goto_7

    .line 2084
    :cond_10
    :goto_8
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_11

    .line 2086
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2702(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 2089
    :cond_11
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    .line 2090
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_12

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v10, :cond_12

    .line 2091
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f080251

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 p1, 0x80

    goto :goto_9

    .line 2095
    :cond_12
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f080251

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2096
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    .line 2098
    :goto_9
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v1, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;-><init>()V

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2100
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2800(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2101
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2102
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2e

    .line 2012
    :pswitch_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_13

    .line 2013
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v12, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    :cond_13
    const-string p1, "SettingScr"

    const-string v0, "GET_QUALITY"

    .line 2016
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2020
    :try_start_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    invoke-interface {v0, v2, v3}, Lepson/print/service/IEpsonService;->getQuality(II)[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_a

    :catch_3
    move-exception p1

    .line 2022
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, v0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2023
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2025
    :goto_a
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-nez p1, :cond_14

    .line 2026
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2027
    new-array v0, v10, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2028
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v9, p1, v1

    .line 2030
    :cond_14
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_4b

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_4b

    .line 2031
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v10, :cond_15

    .line 2032
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f0802a2

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_b

    .line 2034
    :cond_15
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f0802a2

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_b
    const/4 p1, 0x0

    .line 2037
    :goto_c
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_17

    .line 2038
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    aget v2, v2, p1

    if-ne v0, v2, :cond_16

    goto :goto_d

    :cond_16
    add-int/lit8 p1, p1, 0x1

    goto :goto_c

    .line 2042
    :cond_17
    :goto_d
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_18

    .line 2044
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 2046
    :cond_18
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2047
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2048
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;-><init>()V

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2049
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2050
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2051
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2e

    .line 1919
    :pswitch_5
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_19

    .line 1920
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    :cond_19
    const-string p1, "SettingScr"

    const-string v0, "GET_LAYOUT"

    .line 1923
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1924
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1927
    :try_start_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    invoke-interface {v0, v2, v3}, Lepson/print/service/IEpsonService;->getLayout(II)[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_e

    :catch_4
    move-exception p1

    .line 1929
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, v0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1930
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1933
    :goto_e
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Z

    .line 1936
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-nez p1, :cond_1a

    .line 1937
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1941
    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-nez p1, :cond_1a

    .line 1942
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-array v0, v9, [I

    fill-array-data v0, :array_0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1949
    :cond_1a
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_4b

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_4b

    .line 1950
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v10, :cond_1b

    .line 1951
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f0801ba

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_f

    .line 1953
    :cond_1b
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f0801ba

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_f
    const/4 p1, 0x0

    .line 1956
    :goto_10
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_1d

    const-string v0, "layout info"

    .line 1957
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "layout info "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v3

    aget v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1958
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    aget v2, v2, p1

    if-ne v0, v2, :cond_1c

    goto :goto_11

    :cond_1c
    add-int/lit8 p1, p1, 0x1

    goto :goto_10

    .line 1962
    :cond_1d
    :goto_11
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_1e

    .line 1964
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 1969
    :cond_1e
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Layout;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Layout;-><init>()V

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1970
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    if-ne p1, v13, :cond_1f

    .line 1971
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v9}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 1984
    :cond_1f
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    .line 1985
    :goto_12
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_21

    .line 1986
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    aget v2, v2, v0

    if-eq v2, v13, :cond_20

    .line 1987
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 1993
    :cond_21
    :goto_13
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    goto :goto_13

    .line 1996
    :cond_22
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_23

    .line 1997
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1999
    :cond_23
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [I

    invoke-static {v0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2102(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2000
    :goto_14
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_24

    .line 2001
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 2004
    :cond_24
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2005
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2006
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2e

    .line 1814
    :pswitch_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_25

    .line 1815
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v9, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    :cond_25
    const-string p1, "SettingScr"

    const-string v2, "GET_PAPER_TYPE"

    .line 1818
    invoke-static {p1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1819
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1822
    :try_start_5
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    invoke-interface {v2, v3}, Lepson/print/service/IEpsonService;->getPaperType(I)[I

    move-result-object v2

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_15

    :catch_5
    move-exception p1

    .line 1824
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, v2, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1825
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1829
    :goto_15
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->removeCddvdMedia([I)[I

    move-result-object v2

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1832
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-nez p1, :cond_26

    .line 1833
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1837
    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-nez p1, :cond_26

    .line 1838
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-array v2, v9, [I

    sget-object v3, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 1839
    invoke-virtual {v3}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v3

    aput v3, v2, v1

    sget-object v3, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 1840
    invoke-virtual {v3}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v3

    aput v3, v2, v10

    .line 1838
    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1845
    :cond_26
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_4b

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_4b

    .line 1846
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v10, :cond_27

    .line 1847
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v2, 0x7f080254

    invoke-virtual {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_16

    .line 1849
    :cond_27
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v2, 0x7f080254

    invoke-virtual {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1852
    :goto_16
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    :goto_17
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-ge p1, v2, :cond_29

    .line 1853
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    aget v2, v2, v3

    if-ne p1, v2, :cond_28

    goto :goto_18

    .line 1852
    :cond_28
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1608(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    goto :goto_17

    .line 1859
    :cond_29
    :goto_18
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-lt p1, v2, :cond_2b

    .line 1860
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperType()I

    move-result p1

    .line 1861
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    :goto_19
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_2b

    .line 1862
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    aget v0, v0, v2

    if-ne p1, v0, :cond_2a

    .line 1863
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1702(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    goto :goto_1a

    .line 1861
    :cond_2a
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1608(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    goto :goto_19

    .line 1870
    :cond_2b
    :goto_1a
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_2f

    .line 1882
    sget-object p1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result p1

    .line 1883
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    :goto_1b
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_2d

    .line 1884
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    aget v0, v0, v2

    if-ne p1, v0, :cond_2c

    .line 1885
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1702(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    goto :goto_1c

    .line 1883
    :cond_2c
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1608(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    goto :goto_1b

    .line 1889
    :cond_2d
    :goto_1c
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_2f

    .line 1891
    sget-object p1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_AUTO_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result p1

    .line 1892
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    :goto_1d
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_2f

    .line 1893
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    aget v0, v0, v2

    if-ne p1, v0, :cond_2e

    .line 1894
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1702(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    goto :goto_1e

    .line 1892
    :cond_2e
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1608(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    goto :goto_1d

    .line 1903
    :cond_2f
    :goto_1e
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_30

    .line 1904
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 1905
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v1

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1702(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 1908
    :cond_30
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1802(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1909
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1802(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1910
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;-><init>()V

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1911
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1912
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1913
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2e

    .line 1741
    :pswitch_7
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_31

    .line 1742
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v10, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    :cond_31
    const-string p1, "SettingScr"

    const-string v2, "GET_PAPER_SIZE"

    .line 1745
    invoke-static {p1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1749
    :try_start_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object v2

    invoke-interface {v2}, Lepson/print/service/IEpsonService;->getPaperSize()[I

    move-result-object v2

    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_1f

    :catch_6
    move-exception p1

    .line 1751
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1752
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1754
    :goto_1f
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-nez p1, :cond_32

    .line 1755
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    const/16 v2, 0x9

    .line 1759
    new-array v2, v2, [I

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1760
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v2, v1

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1761
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v2, v10

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1762
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v2, v9

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1763
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v2, v6

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1764
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v2, v12

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1765
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v2, v11

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_POSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1766
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1767
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v2, v3

    sget-object v3, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1768
    invoke-virtual {v3}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v3

    aput v3, v2, v13

    .line 1759
    invoke-static {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1772
    :cond_32
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_4b

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_4b

    .line 1773
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v10, :cond_33

    .line 1774
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v2, 0x7f08024d

    invoke-virtual {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_20

    .line 1776
    :cond_33
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v2, 0x7f08024d

    invoke-virtual {p1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1779
    :goto_20
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1102(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    :goto_21
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-ge p1, v2, :cond_35

    .line 1780
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    aget v2, v2, v3

    if-ne p1, v2, :cond_34

    goto :goto_22

    .line 1779
    :cond_34
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1108(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    goto :goto_21

    .line 1786
    :cond_35
    :goto_22
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-lt p1, v2, :cond_37

    .line 1787
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperSize()I

    move-result p1

    .line 1788
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1102(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    :goto_23
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_37

    .line 1789
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    aget v0, v0, v2

    if-ne p1, v0, :cond_36

    .line 1790
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1202(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    goto :goto_24

    .line 1788
    :cond_36
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1108(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    goto :goto_23

    .line 1797
    :cond_37
    :goto_24
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_38

    .line 1798
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1102(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 1799
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v1

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1202(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 1802
    :cond_38
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1803
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 1805
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1806
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1807
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1808
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2e

    .line 1671
    :pswitch_8
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_39

    .line 1672
    new-instance p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3$1;

    invoke-direct {p1, p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3$1;-><init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;)V

    new-array v0, v1, [Ljava/lang/Void;

    .line 1733
    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_2e

    .line 1736
    :cond_39
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    .line 2220
    :cond_3a
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f080245

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ".."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2223
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;-><init>()V

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2224
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    .line 2225
    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3800(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2226
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2229
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;-><init>()V

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2230
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2231
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2234
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2235
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    const/16 v2, -0x32

    if-eq v0, v2, :cond_3b

    const/4 v0, 0x1

    goto :goto_25

    :cond_3b
    const/4 v0, 0x0

    :goto_25
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2236
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    const/16 v3, 0x32

    if-eq v0, v3, :cond_3c

    const/4 v0, 0x1

    goto :goto_26

    :cond_3c
    const/4 v0, 0x0

    :goto_26
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2239
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2240
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4800(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    if-eq v0, v2, :cond_3d

    const/4 v0, 0x1

    goto :goto_27

    :cond_3d
    const/4 v0, 0x0

    :goto_27
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2241
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$4600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    if-eq v0, v3, :cond_3e

    const/4 v0, 0x1

    goto :goto_28

    :cond_3e
    const/4 v0, 0x0

    :goto_28
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2244
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2245
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    if-eq v0, v2, :cond_3f

    const/4 v0, 0x1

    goto :goto_29

    :cond_3f
    const/4 v0, 0x0

    :goto_29
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2246
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    if-eq v0, v3, :cond_40

    const/4 v1, 0x1

    :cond_40
    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2249
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->updateSettingView()V

    .line 2250
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Ljava/lang/Boolean;)V

    .line 2251
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-boolean v0, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Z)Z

    goto/16 :goto_2e

    .line 2150
    :cond_41
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_42

    .line 2151
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2e

    :cond_42
    const-string p1, "SettingScr"

    const-string v0, "GET_DUPLEX"

    .line 2154
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2155
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, v5}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2159
    :try_start_7
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-interface {p1, v0, v2}, Lepson/print/service/IEpsonService;->getDuplex(II)I

    move-result p1
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_2a

    :catch_7
    move-exception p1

    .line 2161
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iput-boolean v10, v0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2162
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 p1, 0x0

    .line 2165
    :goto_2a
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_43

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    if-ne v0, v2, :cond_43

    .line 2169
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-array v0, v6, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2170
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2171
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v10, p1, v10

    .line 2172
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v9, p1, v9

    goto/16 :goto_2b

    :cond_43
    if-eqz p1, :cond_46

    .line 2174
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    const/16 v2, 0x10

    if-eq v0, v2, :cond_46

    .line 2175
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$2000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    if-ne v0, v10, :cond_45

    and-int/2addr p1, v9

    if-eqz p1, :cond_44

    .line 2177
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-array v0, v6, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2178
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2179
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v10, p1, v10

    .line 2180
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v9, p1, v9

    goto :goto_2b

    .line 2182
    :cond_44
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-array v0, v10, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2183
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v1, p1, v1

    goto :goto_2b

    .line 2186
    :cond_45
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-array v0, v6, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2187
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2188
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v10, p1, v10

    .line 2189
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v9, p1, v9

    goto :goto_2b

    .line 2192
    :cond_46
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-array v0, v10, [I

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I

    .line 2193
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2196
    :goto_2b
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    if-eqz p1, :cond_4a

    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_4a

    const/4 p1, 0x0

    .line 2198
    :goto_2c
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_48

    .line 2199
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v2

    aget v2, v2, p1

    if-ne v0, v2, :cond_47

    goto :goto_2d

    :cond_47
    add-int/lit8 p1, p1, 0x1

    goto :goto_2c

    .line 2203
    :cond_48
    :goto_2d
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_49

    .line 2205
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 2207
    :cond_49
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;-><init>()V

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2208
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$3400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2209
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    :cond_4a
    const-string p1, "SettingScr"

    const-string v0, "get Color setScreenState = true"

    .line 2213
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_4b
    :goto_2e
    return v10

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x11
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

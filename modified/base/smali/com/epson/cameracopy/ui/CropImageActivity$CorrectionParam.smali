.class public Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;
.super Ljava/lang/Object;
.source "CropImageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CorrectionParam"
.end annotation


# instance fields
.field private mBcscBValue:F

.field private mBcscCValue:F

.field private mBcscSValue:F

.field private mMaskType:Z

.field private mPointArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPreviewImageSize:Landroid/graphics/Point;

.field private mScale:F

.field private mStrokeWidth:F

.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V
    .locals 1

    .line 2024
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030
    new-instance p1, Landroid/graphics/Point;

    const/4 v0, 0x0

    invoke-direct {p1, v0, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPreviewImageSize:Landroid/graphics/Point;

    return-void
.end method

.method static synthetic access$1402(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;Z)Z
    .locals 0

    .line 2024
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mMaskType:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F
    .locals 0

    .line 2024
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscBValue:F

    return p1
.end method

.method static synthetic access$1602(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F
    .locals 0

    .line 2024
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscCValue:F

    return p1
.end method

.method static synthetic access$1702(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F
    .locals 0

    .line 2024
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscSValue:F

    return p1
.end method

.method static synthetic access$1802(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 2024
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPointArray:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F
    .locals 0

    .line 2024
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mScale:F

    return p1
.end method

.method static synthetic access$2002(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;F)F
    .locals 0

    .line 2024
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mStrokeWidth:F

    return p1
.end method

.method static synthetic access$2102(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 0

    .line 2024
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPreviewImageSize:Landroid/graphics/Point;

    return-object p1
.end method


# virtual methods
.method public Clone()Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;
    .locals 5

    .line 2067
    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    .line 2068
    iget-boolean v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mMaskType:Z

    iput-boolean v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mMaskType:Z

    .line 2069
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscBValue:F

    iput v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscBValue:F

    .line 2070
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscCValue:F

    iput v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscCValue:F

    .line 2071
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscSValue:F

    iput v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscSValue:F

    .line 2072
    new-instance v1, Landroid/graphics/Point;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPreviewImageSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPreviewImageSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPreviewImageSize:Landroid/graphics/Point;

    .line 2073
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPointArray:Ljava/util/List;

    .line 2074
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPointArray:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 2075
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 2076
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2077
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 2078
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2080
    :cond_0
    iget-object v2, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPointArray:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2083
    :cond_1
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mScale:F

    iput v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mScale:F

    .line 2084
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mStrokeWidth:F

    iput v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mStrokeWidth:F

    return-object v0
.end method

.method public GetBcscBValue()F
    .locals 1

    .line 2041
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscBValue:F

    return v0
.end method

.method public GetBcscCValue()F
    .locals 1

    .line 2044
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscCValue:F

    return v0
.end method

.method public GetBcscSValue()F
    .locals 1

    .line 2047
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mBcscSValue:F

    return v0
.end method

.method public GetMaskType()Z
    .locals 1

    .line 2037
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mMaskType:Z

    return v0
.end method

.method public GetPointArray()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation

    .line 2055
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPointArray:Ljava/util/List;

    return-object v0
.end method

.method public GetPreviewImageSize()Landroid/graphics/Point;
    .locals 1

    .line 2051
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mPreviewImageSize:Landroid/graphics/Point;

    return-object v0
.end method

.method public GetScale()F
    .locals 1

    .line 2059
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mScale:F

    return v0
.end method

.method public GetStrokeWidth()F
    .locals 1

    .line 2063
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->mStrokeWidth:F

    return v0
.end method

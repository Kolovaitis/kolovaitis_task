.class Lcom/epson/cameracopy/ui/FolderInformation$DirectoryFileFilter;
.super Ljava/lang/Object;
.source "FolderSelectActivity.java"

# interfaces
.implements Ljava/io/FileFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/FolderInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectoryFileFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/FolderInformation;


# direct methods
.method private constructor <init>(Lcom/epson/cameracopy/ui/FolderInformation;)V
    .locals 0

    .line 539
    iput-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation$DirectoryFileFilter;->this$0:Lcom/epson/cameracopy/ui/FolderInformation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/cameracopy/ui/FolderInformation;Lcom/epson/cameracopy/ui/FolderInformation$1;)V
    .locals 0

    .line 539
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/FolderInformation$DirectoryFileFilter;-><init>(Lcom/epson/cameracopy/ui/FolderInformation;)V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 1

    .line 543
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isHidden()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.class Lcom/epson/cameracopy/ui/CropImageActivity$3;
.super Ljava/lang/Object;
.source "CropImageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;->executeMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V
    .locals 0

    .line 836
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$3;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 840
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$3;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$3;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/CropImageActivity;->mDeviceSize:Lorg/opencv/core/Size;

    iget-wide v1, v1, Lorg/opencv/core/Size;->width:D

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity$3;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/CropImageActivity;->mDeviceSize:Lorg/opencv/core/Size;

    iget-wide v3, v3, Lorg/opencv/core/Size;->height:D

    invoke-virtual {v0, v1, v2, v3, v4}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageRAW(DD)V

    .line 841
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$3;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$500(Lcom/epson/cameracopy/ui/CropImageActivity;I)V

    .line 842
    invoke-static {}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$100()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x390

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

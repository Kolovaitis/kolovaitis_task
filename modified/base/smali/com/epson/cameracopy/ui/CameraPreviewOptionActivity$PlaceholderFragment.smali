.class public Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;
.super Landroid/support/v4/app/Fragment;
.source "CameraPreviewOptionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlaceholderFragment"
.end annotation


# instance fields
.field mAutoPhotoToggleButton:Landroid/widget/Switch;

.field private mCameraPictureResolutionMode:I

.field private mCameraPictureSize:Landroid/graphics/Point;

.field private mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

.field mGuideToggleButton:Landroid/widget/Switch;

.field private mPictureResolutionText:Landroid/widget/TextView;

.field mSaveDirectoryName:Ljava/lang/String;

.field private mSaveDirectoryText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 90
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;Z)V
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->changeAutoPhotoToggleButtonStatus(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->openPictureResolutionDialog()V

    return-void
.end method

.method private changeAutoPhotoToggleButtonStatus(Z)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 254
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/epson/cameracopy/device/CameraPreviewControl;->hasAutoPictureHardware(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 260
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mAutoPhotoToggleButton:Landroid/widget/Switch;

    invoke-virtual {p1, v0}, Landroid/widget/Switch;->setEnabled(Z)V

    goto :goto_1

    .line 262
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mAutoPhotoToggleButton:Landroid/widget/Switch;

    invoke-virtual {p1, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 263
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mAutoPhotoToggleButton:Landroid/widget/Switch;

    invoke-virtual {p1, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    :goto_1
    return-void
.end method

.method private changeValueAndFinish()V
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mGuideToggleButton:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setGuideMode(Z)V

    .line 234
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mAutoPhotoToggleButton:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setAutoPictureMode(Ljava/lang/Boolean;)V

    .line 235
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mSaveDirectoryName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setSaveDirecotry(Ljava/lang/String;)V

    .line 236
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureResolutionMode:I

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setPictureResolutionModeAuto()V

    goto :goto_0

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setPictureSize(Landroid/graphics/Point;)V

    .line 241
    :goto_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    return-void
.end method

.method private openPictureResolutionDialog()V
    .locals 3

    .line 223
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureResolutionMode:I

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->newInstance(ILandroid/graphics/Point;)Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;

    move-result-object v0

    .line 225
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "picture-resolution"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private setCameraPictureResolutionModeFromCameraPreviewControl()V
    .locals 2

    .line 211
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getPictureResolutionMode()I

    move-result v0

    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureResolutionMode:I

    const/4 v0, 0x0

    .line 212
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    .line 213
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureResolutionMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 214
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getPictureSize()Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    :cond_0
    return-void
.end method

.method private setPictureResolutionText()V
    .locals 2

    .line 187
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureResolutionMode:I

    if-nez v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mPictureResolutionText:Landroid/widget/TextView;

    const v1, 0x7f0e0430

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    if-eqz v0, :cond_1

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    if-lez v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getPictureSizeString(II)Ljava/lang/String;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mPictureResolutionText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mPictureResolutionText:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private setSaveDirectoryText(Ljava/lang/String;)V
    .locals 1

    .line 272
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mSaveDirectoryName:Ljava/lang/String;

    .line 273
    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mSaveDirectoryName:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mSaveDirectoryText:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public changeSelectedCameraPictureResolution(ILandroid/graphics/Point;)V
    .locals 0

    .line 168
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureResolutionMode:I

    .line 169
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureResolutionMode:I

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 170
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    goto :goto_0

    .line 172
    :cond_0
    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPictureSize:Landroid/graphics/Point;

    .line 175
    :goto_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->setPictureResolutionText()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_1

    if-eqz p3, :cond_1

    const-string p1, "SELECT_Folder"

    .line 289
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 291
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->setSaveDirectoryText(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-ne p2, v0, :cond_2

    const p1, 0x7f0e0372

    .line 296
    invoke-static {p1}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->newInstance(I)Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    move-result-object p1

    .line 297
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p2

    const-string p3, "error-dialog"

    invoke-virtual {p1, p2, p3}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    const v0, 0x7f0b0003

    .line 306
    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 308
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0a0068

    const/4 v0, 0x0

    .line 96
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 99
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-static {p2}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object p2

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    const/4 p2, 0x1

    .line 101
    invoke-virtual {p0, p2}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->setHasOptionsMenu(Z)V

    const p2, 0x7f08016e

    .line 104
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Switch;

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mGuideToggleButton:Landroid/widget/Switch;

    .line 106
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mGuideToggleButton:Landroid/widget/Switch;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {p3}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getGuideMode()Z

    move-result p3

    invoke-virtual {p2, p3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 108
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mGuideToggleButton:Landroid/widget/Switch;

    new-instance p3, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment$1;

    invoke-direct {p3, p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment$1;-><init>(Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;)V

    invoke-virtual {p2, p3}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const p2, 0x7f080069

    .line 118
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Switch;

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mAutoPhotoToggleButton:Landroid/widget/Switch;

    .line 121
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mAutoPhotoToggleButton:Landroid/widget/Switch;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {p3}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getAutoPictureMode()Z

    move-result p3

    invoke-virtual {p2, p3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 122
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mGuideToggleButton:Landroid/widget/Switch;

    invoke-virtual {p2}, Landroid/widget/Switch;->isChecked()Z

    move-result p2

    invoke-direct {p0, p2}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->changeAutoPhotoToggleButtonStatus(Z)V

    const p2, 0x7f0802cd

    .line 126
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    .line 129
    new-instance p3, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment$2;

    invoke-direct {p3, p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment$2;-><init>(Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;)V

    invoke-virtual {p2, p3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f0802c3

    .line 136
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mSaveDirectoryText:Landroid/widget/TextView;

    .line 137
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {p2}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getSaveDirecotry()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->setSaveDirectoryText(Ljava/lang/String;)V

    const p2, 0x7f080261

    .line 140
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 141
    new-instance p3, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment$3;

    invoke-direct {p3, p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment$3;-><init>(Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f080260

    .line 148
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->mPictureResolutionText:Landroid/widget/TextView;

    .line 150
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->setCameraPictureResolutionModeFromCameraPreviewControl()V

    .line 151
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->setPictureResolutionText()V

    return-object p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 314
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-eq v0, v1, :cond_0

    .line 320
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 316
    :cond_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->changeValueAndFinish()V

    const/4 p1, 0x1

    return p1
.end method

.method public startSaveDirectoryActivity()V
    .locals 3

    .line 278
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 279
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/epson/cameracopy/ui/FolderSelectActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v0, 0x1

    .line 280
    invoke-virtual {p0, v1, v0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.class Lcom/epson/cameracopy/ui/CropImageActivity$13;
.super Ljava/lang/Object;
.source "CropImageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;->MakePrintData(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

.field final synthetic val$bPrintData:Z

.field final synthetic val$nextAction:I


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;ZI)V
    .locals 0

    .line 1465
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iput-boolean p2, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->val$bPrintData:Z

    iput p3, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->val$nextAction:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1470
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$700(Lcom/epson/cameracopy/ui/CropImageActivity;)[Landroid/graphics/PointF;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v3}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$2200(Lcom/epson/cameracopy/ui/CropImageActivity;)I

    move-result v3

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v4}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$2300(Lcom/epson/cameracopy/ui/CropImageActivity;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lepson/colorcorrection/ImageCollect;->MakeCropPrintSizeImage([Landroid/graphics/PointF;Lorg/opencv/core/Size;ILjava/util/List;)Lorg/opencv/core/Mat;

    .line 1473
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object v0

    iget-boolean v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->val$bPrintData:Z

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$2200(Lcom/epson/cameracopy/ui/CropImageActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/CropImageActivity;->mPaperSize:Lorg/opencv/core/Size;

    invoke-virtual {v0, v1, v2, v3}, Lepson/colorcorrection/ImageCollect;->SaveExecute(ZILorg/opencv/core/Size;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1474
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->val$bPrintData:Z

    if-nez v0, :cond_0

    .line 1477
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lepson/colorcorrection/ImageCollect;->GetSavePath(Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1478
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "title"

    .line 1479
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "_display_name"

    .line 1480
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "mime_type"

    const-string v3, "image/jpeg"

    .line 1481
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "_data"

    .line 1482
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "datetaken"

    .line 1483
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1484
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/CropImageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1489
    :cond_0
    invoke-static {}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$100()Landroid/os/Handler;

    move-result-object v0

    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$13;->val$nextAction:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

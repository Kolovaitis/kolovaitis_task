.class Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DocumentSizeSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SelectListAdapter"
.end annotation


# instance fields
.field mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

.field final synthetic this$0:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;)V
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->this$0:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 221
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->reload()V

    return-void
.end method


# virtual methods
.method public delete(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;I)V
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    invoke-virtual {v0, p1, p2}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->delete(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;I)V

    .line 283
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 241
    :cond_0
    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 249
    :cond_0
    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getItem(I)Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 261
    iget-object p2, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->this$0:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;

    invoke-virtual {p2}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p2

    const v1, 0x7f0a00bc

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 263
    :cond_0
    iget-object p3, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    if-nez p3, :cond_1

    return-object p2

    .line 268
    :cond_1
    invoke-virtual {p3, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getItem(I)Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    move-result-object p1

    const p3, 0x7f080112

    .line 269
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->this$0:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f0800fc

    .line 272
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 273
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    invoke-virtual {v1, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->isSelected(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 274
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 p1, 0x4

    .line 276
    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-object p2
.end method

.method public reload()V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->this$0:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    .line 227
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public reset()V
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->reset()V

    .line 288
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public select(I)V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->mRegistedDocumentSizeList:Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->select(I)V

    .line 233
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->notifyDataSetChanged()V

    return-void
.end method

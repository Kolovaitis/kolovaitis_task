.class public Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PictureResolutionDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;,
        Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$OnItemSelectedListener;
    }
.end annotation


# static fields
.field private static final KEY_CAMERA_PICTURE_RESOLUTION_MODE:Ljava/lang/String; = "picture_resolution_mode"

.field private static final KEY_CAMERA_PICTURE_SIZE:Ljava/lang/String; = "picture_size"

.field private static final MANUAL_SIZE_START_INDEX:I = 0x1


# instance fields
.field private mSizeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;I)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->selectListItem(I)V

    return-void
.end method

.method private getListPoisition(ILandroid/graphics/Point;Ljava/util/List;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Point;",
            "Ljava/util/List<",
            "Landroid/graphics/Point;",
            ">;)I"
        }
    .end annotation

    const/4 p3, 0x1

    if-ne p1, p3, :cond_0

    if-eqz p2, :cond_0

    .line 167
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->mSizeList:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    add-int/2addr p1, p3

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private getPictureSizeList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation

    .line 146
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getCameraPictureSizes()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 152
    :cond_0
    new-instance v1, Lcom/epson/cameracopy/ui/AreaComparator;

    invoke-direct {v1}, Lcom/epson/cameracopy/ui/AreaComparator;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private getPictureSizeStrList(Ljava/util/List;)[Ljava/lang/CharSequence;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/Point;",
            ">;)[",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 188
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x1

    add-int/2addr v1, v2

    .line 190
    new-array v1, v1, [Ljava/lang/String;

    .line 192
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0430

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    if-nez p1, :cond_1

    return-object v1

    .line 198
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x1

    .line 199
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 200
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Point;

    .line 201
    iget v4, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-static {v4, v3}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getPictureSizeString(II)Ljava/lang/String;

    move-result-object v3

    .line 202
    aput-object v3, v1, v0

    add-int/2addr v0, v2

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public static newInstance(ILandroid/graphics/Point;)Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;
    .locals 4

    .line 65
    new-instance v0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;

    invoke-direct {v0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;-><init>()V

    .line 67
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "picture_resolution_mode"

    .line 68
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz p1, :cond_0

    const/4 p0, 0x2

    .line 72
    new-array p0, p0, [I

    const/4 v2, 0x0

    iget v3, p1, Landroid/graphics/Point;->x:I

    aput v3, p0, v2

    const/4 v2, 0x1

    iget p1, p1, Landroid/graphics/Point;->y:I

    aput p1, p0, v2

    const-string p1, "picture_size"

    .line 73
    invoke-virtual {v1, p1, p0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 81
    :cond_0
    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private selectListItem(I)V
    .locals 5

    .line 216
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$OnItemSelectedListener;

    .line 217
    iget-object v1, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->mSizeList:Ljava/util/List;

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 p1, -0x1

    .line 218
    invoke-interface {v0, v3, p1, v2}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$OnItemSelectedListener;->onItemSelected(ZILandroid/graphics/Point;)V

    return-void

    :cond_0
    const/4 v4, 0x1

    if-nez p1, :cond_1

    .line 223
    invoke-interface {v0, v4, v3, v2}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$OnItemSelectedListener;->onItemSelected(ZILandroid/graphics/Point;)V

    goto :goto_0

    :cond_1
    sub-int/2addr p1, v4

    .line 228
    :try_start_0
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Point;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    invoke-interface {v0, v4, v4, p1}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$OnItemSelectedListener;->onItemSelected(ZILandroid/graphics/Point;)V

    :goto_0
    return-void

    :catch_0
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .line 133
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .line 89
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "picture_resolution_mode"

    .line 92
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    const-string v4, "picture_size"

    .line 96
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object p1

    if-eqz p1, :cond_0

    .line 97
    array-length v4, p1

    const/4 v5, 0x2

    if-lt v4, v5, :cond_0

    .line 98
    new-instance v4, Landroid/graphics/Point;

    aget v5, p1, v1

    aget p1, p1, v2

    invoke-direct {v4, v5, p1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    :cond_0
    move-object v4, v3

    .line 102
    :goto_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getPictureSizeList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->mSizeList:Ljava/util/List;

    .line 103
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->mSizeList:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getPictureSizeStrList(Ljava/util/List;)[Ljava/lang/CharSequence;

    move-result-object p1

    if-nez p1, :cond_1

    return-object v3

    .line 109
    :cond_1
    iget-object v2, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->mSizeList:Ljava/util/List;

    invoke-direct {p0, v0, v4, v2}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getListPoisition(ILandroid/graphics/Point;Ljava/util/List;)I

    move-result v0

    .line 111
    new-instance v2, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0a00bc

    invoke-direct {v2, v3, v4, p1, v0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/CharSequence;I)V

    .line 114
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {p1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0e042f

    .line 115
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$1;-><init>(Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;)V

    .line 116
    invoke-virtual {v3, v2, v0, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 124
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 125
    invoke-virtual {p1, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p1
.end method

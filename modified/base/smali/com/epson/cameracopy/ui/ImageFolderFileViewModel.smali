.class public Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;
.super Landroid/arch/lifecycle/AndroidViewModel;
.source "ImageFolderFileViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;
    }
.end annotation


# instance fields
.field private final mApplication:Landroid/app/Application;

.field private final mImageFolderFileLiveData:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0
    .param p1    # Landroid/app/Application;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 14
    invoke-direct {p0, p1}, Landroid/arch/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 15
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->mApplication:Landroid/app/Application;

    .line 16
    new-instance p1, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    invoke-direct {p1, p0}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;-><init>(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->mImageFolderFileLiveData:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;)Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->mImageFolderFileLiveData:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    return-object p0
.end method

.method static synthetic access$200(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;)Landroid/app/Application;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->mApplication:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method public getData()Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->mImageFolderFileLiveData:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    return-object v0
.end method

.method public setOriginalData(Lcom/epson/cameracopy/ui/ImageFolderFile;)V
    .locals 1
    .param p1    # Lcom/epson/cameracopy/ui/ImageFolderFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 20
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->mImageFolderFileLiveData:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;->setImageFolderFile(Lcom/epson/cameracopy/ui/ImageFolderFile;)V

    return-void
.end method

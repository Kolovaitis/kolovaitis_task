.class public Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;
.super Lepson/print/ActivityIACommon;
.source "CameraPrintSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final EXTRA_PRINTER:Ljava/lang/String; = "myprinter"

.field private static final EXTRA_SIMPLEAP:Ljava/lang/String; = "simpleap"

.field private static final REQUEST_CODE_INFO:I = 0x1

.field private static final REQUEST_CODE_INK_REPLENISH_PROGRESS:I = 0x4

.field private static final REQUEST_CODE_PAGE_RANGE:I = 0x2

.field private static final REQUEST_CODE_PRINTER:I = 0x0

.field public static final TAG:Ljava/lang/String; = "CameraPrintSetting"


# instance fields
.field private final COLOR_VALUE_MAX:I

.field private final COLOR_VALUE_MIN:I

.field private final COPIES_MAX:I

.field private final COPIES_MIN:I

.field private final EPS_ERR_COMM_ERROR:I

.field private final EPS_ERR_NONE:I

.field private final EPS_ERR_OPR_FAIL:I

.field private final EPS_ERR_PRINTER_NOT_SET:I

.field private final GET_ADVANCED:I

.field private final GET_COLOR:I

.field private final GET_DUPLEX:I

.field private final GET_LAYOUT:I

.field private final GET_PAPER_SIZE:I

.field private final GET_PAPER_SOURCE:I

.field private final GET_PAPER_TYPE:I

.field private final GET_QUALITY:I

.field private final GET_SUPPORTED_MEDIA:I

.field private final PROBE_PRINTER:I

.field private final SEARCH_BY_ID:I

.field private final SEARCH_ERROR:I

.field private final SETTING_DONE:I

.field private final SHOW_ERROR_DIALOG:I

.field private autoConnectSsid:Ljava/lang/String;

.field private brightness:Landroid/widget/TextView;

.field private brightnessMinus:Landroid/widget/Button;

.field private brightnessPlus:Landroid/widget/Button;

.field private brightnessValue:I

.field private color:I

.field private colorInfo:Landroid/widget/TextView;

.field private color_info:[I

.field private contrast:Landroid/widget/TextView;

.field private contrastMinus:Landroid/widget/Button;

.field private contrastPlus:Landroid/widget/Button;

.field private contrastValue:I

.field private copies:Landroid/widget/TextView;

.field private copiesMinus:Landroid/widget/Button;

.field private copiesPlus:Landroid/widget/Button;

.field private copiesValue:I

.field private disablePrintArea:Z

.field private duplex:I

.field private duplexInfo:Landroid/widget/TextView;

.field private duplex_info:[I

.field private enableShowPreview:Z

.field private endValue:I

.field private feedDirection:I

.field private feedDirectionInfo:Landroid/widget/TextView;

.field private info:[I

.field private isDocumentSetting:Z

.field isInfoAvai:Z

.field isNotLoading:Ljava/lang/Boolean;

.field private isRetryAfterConnectSimpleAp:Z

.field private isTryConnectSimpleAp:Z

.field private lang:I

.field private layout:I

.field private layoutInfo:Landroid/widget/TextView;

.field private layout_info:[I

.field private final mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private final mCameraCopy:Z

.field private mContext:Landroid/content/Context;

.field private final mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field mHandler:Landroid/os/Handler;

.field private mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

.field private mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

.field private mPrinterSelectDone:Z

.field private mRemoteSrcType:I

.field private mWaiteInkReplenProgress:Z

.field private paperSize:I

.field private paperSizeInfo:Landroid/widget/TextView;

.field private paperSource:I

.field private paperSourceInfo:Landroid/widget/TextView;

.field private paperType:I

.field private paperTypeInfo:Landroid/widget/TextView;

.field private paper_size_info:[I

.field private paper_source_info:[I

.field private paper_type_info:[I

.field private printAll:Z

.field private printDateInfo:Landroid/widget/TextView;

.field private printdate:I

.field private printer:Lepson/print/MyPrinter;

.field private printerDeviceId:Ljava/lang/String;

.field private printerEmailAddress:Ljava/lang/String;

.field private printerId:Ljava/lang/String;

.field private printerIp:Ljava/lang/String;

.field private printerLocation:I

.field private printerName:Landroid/widget/TextView;

.field private printerSerialNo:Ljava/lang/String;

.field private progressGetOption:Landroid/view/View;

.field private quality:I

.field private qualityInfo:Landroid/widget/TextView;

.field private quality_info:[I

.field private saturation:Landroid/widget/TextView;

.field private saturationMinus:Landroid/widget/Button;

.field private saturationPlus:Landroid/widget/Button;

.field private saturationValue:I

.field private sheets:I

.field private sizeIndex:I

.field private startValue:I

.field private typeIndex:I

.field undoFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    .line 67
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 70
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->EPS_ERR_NONE:I

    const/16 v1, -0x3e8

    .line 71
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->EPS_ERR_OPR_FAIL:I

    const/16 v1, -0x44c

    .line 72
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->EPS_ERR_COMM_ERROR:I

    const/16 v1, -0x547

    .line 73
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->EPS_ERR_PRINTER_NOT_SET:I

    const/16 v1, -0x32

    .line 78
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->COLOR_VALUE_MIN:I

    const/16 v1, 0x32

    .line 79
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->COLOR_VALUE_MAX:I

    const/4 v1, 0x1

    .line 99
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->lang:I

    .line 109
    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    .line 130
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sheets:I

    const/4 v2, 0x3

    .line 131
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->SETTING_DONE:I

    .line 132
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->COPIES_MIN:I

    const/16 v3, 0x1e

    .line 133
    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->COPIES_MAX:I

    .line 135
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isInfoAvai:Z

    .line 138
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->disablePrintArea:Z

    const/4 v3, 0x0

    .line 139
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    .line 141
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isTryConnectSimpleAp:Z

    .line 142
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isRetryAfterConnectSimpleAp:Z

    .line 143
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->autoConnectSsid:Ljava/lang/String;

    .line 146
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mContext:Landroid/content/Context;

    .line 148
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->enableShowPreview:Z

    .line 150
    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mCameraCopy:Z

    .line 507
    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1564
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 1565
    new-instance v4, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;

    invoke-direct {v4, p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$2;-><init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V

    iput-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 1595
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_SUPPORTED_MEDIA:I

    .line 1596
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_PAPER_SIZE:I

    const/4 v4, 0x2

    .line 1597
    iput v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_PAPER_TYPE:I

    .line 1598
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_LAYOUT:I

    const/4 v2, 0x4

    .line 1599
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_QUALITY:I

    const/4 v2, 0x5

    .line 1600
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_PAPER_SOURCE:I

    const/4 v2, 0x6

    .line 1601
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_COLOR:I

    const/16 v2, 0x20

    .line 1602
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_DUPLEX:I

    const/16 v2, 0x40

    .line 1603
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->GET_ADVANCED:I

    const/4 v2, 0x7

    .line 1604
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->SEARCH_BY_ID:I

    const/16 v2, 0x10

    .line 1605
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->SEARCH_ERROR:I

    const/16 v2, 0x11

    .line 1606
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->PROBE_PRINTER:I

    const/16 v2, 0x12

    .line 1607
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->SHOW_ERROR_DIALOG:I

    .line 1609
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sizeIndex:I

    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->typeIndex:I

    .line 1610
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->info:[I

    .line 1611
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_source_info:[I

    .line 1612
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color_info:[I

    .line 1613
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_size_info:[I

    .line 1614
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_type_info:[I

    .line 1615
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout_info:[I

    .line 1616
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality_info:[I

    .line 1617
    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex_info:[I

    .line 1618
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isNotLoading:Ljava/lang/Boolean;

    .line 1619
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$3;-><init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    .line 2327
    new-instance v0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;-><init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->autoConnectSsid:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->info:[I

    return-object p0
.end method

.method static synthetic access$1002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->info:[I

    return-object p1
.end method

.method static synthetic access$1100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sizeIndex:I

    return p0
.end method

.method static synthetic access$1102(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sizeIndex:I

    return p1
.end method

.method static synthetic access$1108(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 2

    .line 67
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sizeIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sizeIndex:I

    return v0
.end method

.method static synthetic access$1200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    return p0
.end method

.method static synthetic access$1202(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    return p1
.end method

.method static synthetic access$1302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_size_info:[I

    return-object p1
.end method

.method static synthetic access$1400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    return-object p0
.end method

.method static synthetic access$1402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSizeInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->typeIndex:I

    return p0
.end method

.method static synthetic access$1602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->typeIndex:I

    return p1
.end method

.method static synthetic access$1608(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 2

    .line 67
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->typeIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->typeIndex:I

    return v0
.end method

.method static synthetic access$1700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    return p0
.end method

.method static synthetic access$1702(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    return p1
.end method

.method static synthetic access$1802(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_type_info:[I

    return-object p1
.end method

.method static synthetic access$1900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperTypeInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout:I

    return p0
.end method

.method static synthetic access$2002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout:I

    return p1
.end method

.method static synthetic access$2100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout_info:[I

    return-object p0
.end method

.method static synthetic access$2102(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout_info:[I

    return-object p1
.end method

.method static synthetic access$2200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layoutInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality:I

    return p0
.end method

.method static synthetic access$2302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality:I

    return p1
.end method

.method static synthetic access$2402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality_info:[I

    return-object p1
.end method

.method static synthetic access$2500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->qualityInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_source_info:[I

    return-object p0
.end method

.method static synthetic access$2602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_source_info:[I

    return-object p1
.end method

.method static synthetic access$2700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSource:I

    return p0
.end method

.method static synthetic access$2702(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSource:I

    return p1
.end method

.method static synthetic access$2800(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSourceInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color_info:[I

    return-object p0
.end method

.method static synthetic access$2902(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color_info:[I

    return-object p1
.end method

.method static synthetic access$300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerEmailAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mRemoteSrcType:I

    return p0
.end method

.method static synthetic access$3100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color:I

    return p0
.end method

.method static synthetic access$3102(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color:I

    return p1
.end method

.method static synthetic access$3200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->colorInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$3300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)[I
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex_info:[I

    return-object p0
.end method

.method static synthetic access$3302(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;[I)[I
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex_info:[I

    return-object p1
.end method

.method static synthetic access$3400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex:I

    return p0
.end method

.method static synthetic access$3402(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex:I

    return p1
.end method

.method static synthetic access$3500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplexInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$3600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startValue:I

    return p0
.end method

.method static synthetic access$3700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->endValue:I

    return p0
.end method

.method static synthetic access$3800(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirection:I

    return p0
.end method

.method static synthetic access$3900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirectionInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    return p0
.end method

.method static synthetic access$4000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printdate:I

    return p0
.end method

.method static synthetic access$4100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printDateInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$4200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    return p0
.end method

.method static synthetic access$4300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightness:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$4400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessMinus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$4500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessPlus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$4600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    return p0
.end method

.method static synthetic access$4700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrast:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$4800(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastMinus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$4900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastPlus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$5000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I
    .locals 0

    .line 67
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    return p0
.end method

.method static synthetic access$5100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturation:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$5200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationMinus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$5300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/Button;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationPlus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$5400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Z)Z
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->updateSupportedMediaFile(Z)Z

    move-result p0

    return p0
.end method

.method static synthetic access$5500(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/widget/TextView;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$5602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I
    .locals 0

    .line 67
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->lang:I

    return p1
.end method

.method static synthetic access$600(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$700(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Ljava/lang/Boolean;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->setScreenState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$800(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Landroid/content/Context;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$900(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Z
    .locals 0

    .line 67
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    return p0
.end method

.method private bindEpsonService()V
    .locals 3

    const-string v0, "SettingScr"

    const-string v1, "bindEpsonService"

    .line 769
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_0

    .line 773
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/service/EpsonService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_0
    return-void
.end method

.method private endInkReplAndGoProbePrinter()V
    .locals 4

    const/4 v0, 0x0

    .line 1375
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mWaiteInkReplenProgress:Z

    .line 1377
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->bindEpsonService()V

    .line 1380
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private loadConfig()V
    .locals 7

    .line 330
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 333
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->getUserDefName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 335
    :cond_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    const v2, 0x7f0e04d6

    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    :goto_0
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerDeviceId:Ljava/lang/String;

    .line 338
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    .line 339
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    .line 340
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerSerialNo:Ljava/lang/String;

    .line 341
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerEmailAddress:Ljava/lang/String;

    .line 342
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    .line 343
    iget v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    const/4 v2, 0x1

    if-nez v1, :cond_1

    .line 344
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    :cond_1
    const-string v1, "printer"

    .line 348
    invoke-static {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->autoConnectSsid:Ljava/lang/String;

    .line 351
    new-instance v1, Lepson/print/screen/PrintSetting;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v1, p0, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 352
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 359
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 361
    iget v3, v1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    .line 362
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSizeInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 363
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 368
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;-><init>()V

    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 370
    iget v3, v1, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    .line 371
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperTypeInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 376
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadPaperSizeTypePear()Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;

    move-result-object v3

    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 394
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    iget v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    invoke-virtual {v3, v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->putID(II)Z

    .line 400
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$Layout;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Layout;-><init>()V

    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 405
    iget v3, v1, Lepson/print/screen/PrintSetting;->layoutValue:I

    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout:I

    .line 406
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layoutInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 410
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;-><init>()V

    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 412
    iget v3, v1, Lepson/print/screen/PrintSetting;->qualityValue:I

    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality:I

    .line 413
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->qualityInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 414
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 417
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;-><init>()V

    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 425
    iget v3, v1, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSource:I

    .line 426
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSourceInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSource:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 430
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;-><init>()V

    iput-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 432
    iget v3, v1, Lepson/print/screen/PrintSetting;->colorValue:I

    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color:I

    .line 433
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->colorInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 434
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 438
    iget v3, v1, Lepson/print/screen/PrintSetting;->copiesValue:I

    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    .line 439
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copies:Landroid/widget/TextView;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesMinus:Landroid/widget/Button;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    const/4 v5, 0x0

    if-eq v4, v2, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 441
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesPlus:Landroid/widget/Button;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    const/16 v6, 0x1e

    if-eq v4, v6, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 445
    iget v2, v1, Lepson/print/screen/PrintSetting;->brightnessValue:I

    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    .line 449
    iget v2, v1, Lepson/print/screen/PrintSetting;->contrastValue:I

    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    .line 453
    iget v2, v1, Lepson/print/screen/PrintSetting;->saturationValue:I

    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    .line 456
    new-instance v2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;

    invoke-direct {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;-><init>()V

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 458
    iget v2, v1, Lepson/print/screen/PrintSetting;->duplexValue:I

    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex:I

    .line 459
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplexInfo:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex:I

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 460
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 463
    new-instance v2, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;

    invoke-direct {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;-><init>()V

    iput-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 465
    iget v2, v1, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirection:I

    .line 466
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirectionInfo:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirection:I

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 467
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 473
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLang()I

    move-result v0

    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->lang:I

    .line 475
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    if-eqz v0, :cond_4

    .line 479
    iget-boolean v0, v1, Lepson/print/screen/PrintSetting;->printAll:Z

    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printAll:Z

    .line 480
    iget v0, v1, Lepson/print/screen/PrintSetting;->startValue:I

    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startValue:I

    .line 481
    iget v0, v1, Lepson/print/screen/PrintSetting;->endValue:I

    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->endValue:I

    const v0, 0x7f080245

    .line 483
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ".."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->endValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 490
    :cond_4
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;-><init>()V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 492
    iget v0, v1, Lepson/print/screen/PrintSetting;->printdate:I

    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printdate:I

    .line 493
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printDateInfo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printdate:I

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    :goto_3
    return-void
.end method

.method private loadSupportedMediaFile()Z
    .locals 6

    .line 512
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    .line 514
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPreSupportedMedia()Ljava/io/File;

    move-result-object v3

    const-string v4, "CameraPrintSetting"

    const-string v5, "call loadSupportedMedia"

    .line 515
    invoke-static {v4, v5}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    .line 517
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v0, "loadSupportedMedia"

    .line 518
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " not exist"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    .line 521
    :cond_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 522
    invoke-static {v1, v3}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 529
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSavedAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 530
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v2

    .line 531
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPreAreaInfo()Ljava/io/File;

    move-result-object v0

    .line 533
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 534
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 535
    invoke-static {v1, v0}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    const-string v1, "loadSupportedMedia"

    .line 538
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failure "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    :catch_1
    move-exception v0

    const-string v1, "loadSupportedMedia"

    .line 524
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failure "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v4
.end method

.method private onPrinterSelectEnd(ILandroid/content/Intent;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-ne p1, v2, :cond_2

    .line 1294
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "myprinter"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    .line 1295
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1296
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerDeviceId:Ljava/lang/String;

    .line 1297
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    .line 1298
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    .line 1299
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerSerialNo:Ljava/lang/String;

    .line 1300
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerEmailAddress:Ljava/lang/String;

    .line 1301
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    .line 1304
    new-instance p1, Lepson/print/EPPrinterManager;

    invoke-direct {p1, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1306
    iget v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1308
    :pswitch_0
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1309
    iget-object v2, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1310
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    iget-object p1, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1314
    :pswitch_1
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1315
    iget-object v2, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1316
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    iget-object p1, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1321
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "simpleap"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->autoConnectSsid:Ljava/lang/String;

    .line 1324
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "PrintSetting"

    const-string v2, "RE_SEARCH"

    invoke-static {p1, p2, v2, v1}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1327
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1329
    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mPrinterSelectDone:Z

    .line 1330
    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mWaiteInkReplenProgress:Z

    .line 1331
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    invoke-static {p1, p2}, Lepson/print/inkrpln/InkReplnHelper;->isSimpleApOrP2p(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1332
    new-instance p1, Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerSerialNo:Ljava/lang/String;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerDeviceId:Ljava/lang/String;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lepson/print/inkrpln/PrintSettingDependencyBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-static {p0, p1}, Lepson/print/inkrpln/InkRplnProgressDialog;->getStartIntent2(Landroid/content/Context;Lepson/print/inkrpln/DependencyBuilder;)Landroid/content/Intent;

    move-result-object p1

    const/4 p2, 0x4

    .line 1335
    invoke-virtual {p0, p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_3

    .line 1337
    :cond_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->endInkReplAndGoProbePrinter()V

    goto :goto_3

    .line 1342
    :cond_2
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->bindEpsonService()V

    .line 1346
    new-instance p1, Lepson/print/EPPrinterManager;

    invoke-direct {p1, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1348
    iget p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    packed-switch p2, :pswitch_data_1

    goto :goto_1

    .line 1355
    :pswitch_2
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-nez p1, :cond_3

    goto :goto_2

    .line 1350
    :pswitch_3
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-nez p1, :cond_3

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_4

    .line 1363
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->resetSettings()V

    :cond_4
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private saveChanged()V
    .locals 11

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 644
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 645
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "PrintSettingSaved"

    const/4 v3, 0x1

    .line 649
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 650
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 653
    new-instance v0, Lepson/print/MyPrinter;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    iget-object v7, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    iget-object v8, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerSerialNo:Ljava/lang/String;

    iget-object v9, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerEmailAddress:Ljava/lang/String;

    iget v10, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    move-object v4, v0

    invoke-direct/range {v4 .. v10}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 661
    iget v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->lang:I

    invoke-virtual {v0, v2}, Lepson/print/MyPrinter;->setLang(I)V

    .line 662
    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->setCurPrinter(Landroid/content/Context;)V

    .line 665
    new-instance v2, Lepson/print/screen/PrintSetting;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v2, p0, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 667
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 668
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 669
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 670
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 671
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSource:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 672
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 673
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 674
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirection:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    .line 676
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->copiesValue:I

    .line 677
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 678
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 679
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->saturationValue:I

    .line 696
    iget-boolean v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    if-eqz v3, :cond_0

    .line 698
    iget-boolean v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printAll:Z

    iput-boolean v3, v2, Lepson/print/screen/PrintSetting;->printAll:Z

    .line 699
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->startValue:I

    .line 700
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->endValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->endValue:I

    goto :goto_0

    .line 704
    :cond_0
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printdate:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->printdate:I

    .line 708
    :goto_0
    invoke-virtual {v2}, Lepson/print/screen/PrintSetting;->saveSettings()V

    .line 730
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    check-cast v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;

    invoke-virtual {v2, v3}, Lepson/print/screen/PrintSetting;->savePaperSizeTypePear(Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;)V

    .line 732
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saveSupportedMediaFile()Z

    .line 735
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->autoConnectSsid:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 736
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->autoConnectSsid:Ljava/lang/String;

    const-string v3, "printer"

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v3, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setConnectInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v0, "printer"

    .line 738
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V

    .line 742
    :goto_1
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 744
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V

    .line 745
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitRemotePrinterInfo()V

    .line 748
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mPrinterSelectDone:Z

    if-eqz v0, :cond_2

    .line 750
    new-instance v0, Lepson/print/inkrpln/InkRplnRepository;

    invoke-direct {v0, v1}, Lepson/print/inkrpln/InkRplnRepository;-><init>(Z)V

    .line 751
    invoke-virtual {v0, p0}, Lepson/print/inkrpln/InkRplnRepository;->savePermanently(Landroid/content/Context;)V

    :cond_2
    return-void
.end method

.method private saveSupportedMediaFile()Z
    .locals 7

    .line 596
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    const-string v1, "CameraPrintSetting"

    const-string v2, "call saveSupportedMedia"

    .line 598
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v2

    const/4 v3, 0x0

    .line 601
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    const-string v0, "updateSupportedMediaFile"

    .line 602
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " not exist"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    .line 605
    :cond_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 613
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 614
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSavedAreaInfo()Ljava/io/File;

    move-result-object v2

    .line 615
    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    const/4 v6, 0x3

    if-eq v4, v6, :cond_1

    const-string v1, "CameraPrintSetting"

    const-string v2, "delete AreaInfo"

    .line 632
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->removeAreaInfo()V

    goto :goto_0

    .line 620
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "updateSupportedMediaFile"

    .line 621
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " not exist"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    .line 624
    :cond_2
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return v5

    :catch_0
    move-exception v0

    const-string v1, "saveSupportedMediaFile"

    .line 626
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failure "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :catch_1
    move-exception v0

    const-string v1, "saveSupportedMediaFile"

    .line 607
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failure "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method

.method private setClickListener()V
    .locals 1

    .line 297
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessPlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastPlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationPlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 307
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    if-eqz v0, :cond_0

    const v0, 0x7f080244

    .line 308
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f08027e

    .line 310
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08024a

    .line 311
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080252

    .line 312
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0801b6

    .line 313
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802a0

    .line 314
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08024f

    .line 315
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0800d2

    .line 317
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08011d

    .line 318
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08013d

    .line 319
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08027b

    .line 320
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setScreenState(Ljava/lang/Boolean;)V
    .locals 2

    .line 2424
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2425
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->progressGetOption:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2427
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->progressGetOption:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const v0, 0x7f08027e

    .line 2429
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08024a

    .line 2430
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f080252

    .line 2431
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f0801b6

    .line 2432
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f0802a0

    .line 2433
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08024f

    .line 2434
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f0800d2

    .line 2436
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08011d

    .line 2437
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08013d

    .line 2438
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08027b

    .line 2439
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method private unbindEpsonService()V
    .locals 2

    const-string v0, "SettingScr"

    const-string v1, "unbindEpsonService"

    .line 782
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 787
    :try_start_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 788
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    .line 789
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 791
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method private updatePrinterIcon()V
    .locals 3

    .line 275
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f0e04d6

    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const v1, 0x7f080198

    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 278
    :cond_0
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 281
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 286
    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f07010f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 283
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f070111

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 289
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f070110

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateSupportedMediaFile(Z)Z
    .locals 5

    .line 553
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    const-string v1, "CameraPrintSetting"

    const-string v2, "call updateSupportedMedia"

    .line 555
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPreSupportedMedia()Ljava/io/File;

    move-result-object v2

    if-eqz p1, :cond_0

    move-object v4, v2

    move-object v2, v1

    move-object v1, v4

    :cond_0
    const/4 v3, 0x0

    .line 563
    :try_start_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 572
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 573
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPreAreaInfo()Ljava/io/File;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 580
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 581
    invoke-static {v0, v1}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    if-eqz v0, :cond_3

    .line 584
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_3
    if-eqz v1, :cond_4

    .line 585
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_4
    const-string v0, "updateSupportedMediaFile"

    .line 586
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failure "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :catch_1
    move-exception p1

    if-eqz v1, :cond_5

    .line 565
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_5
    if-eqz v2, :cond_6

    .line 566
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_6
    const-string v0, "updateSupportedMediaFile"

    .line 567
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failure "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method


# virtual methods
.method protected deleteLongTapMessage()V
    .locals 8

    .line 2523
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    add-long/2addr v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 2525
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2526
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return-void
.end method

.method getPrinterLang()I
    .locals 4

    .line 1505
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1506
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 1508
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->getLang()I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1510
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "CameraPrintSetting"

    .line 1514
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPrinterLang called : ret = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method launchDetailScreen(II)V
    .locals 4

    .line 1071
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/screen/PrinterInfoDetail;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1072
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ID"

    .line 1073
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "sizeIndex"

    .line 1074
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sizeIndex:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "typeIndex"

    .line 1075
    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->typeIndex:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "curValue"

    .line 1076
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p1, "isDocumentSetting"

    .line 1077
    iget-boolean v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    invoke-virtual {v1, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 p1, 0x1

    sparse-switch p2, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string p2, "PRINT_QUALITY_INFO"

    .line 1093
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_1
    const-string p2, "PAPER_TYPE_INFO"

    .line 1085
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_type_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    .line 1099
    :sswitch_2
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_source_info:[I

    if-eqz p2, :cond_0

    array-length p2, p2

    if-gt p2, p1, :cond_0

    .line 1100
    new-array p2, p1, [I

    const/4 v2, 0x0

    const/16 v3, 0x80

    aput v3, p2, v2

    goto :goto_0

    .line 1103
    :cond_0
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_source_info:[I

    :goto_0
    const-string v2, "PAPER_SOURCE_INFO"

    .line 1107
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_3
    const-string p2, "PAPER_SIZE_INFO"

    .line 1081
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_size_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_4
    const-string p2, "LAYOUT_INFO"

    .line 1089
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_5
    const-string p2, "DUPLEX_INFO"

    .line 1115
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_6
    const-string p2, "COLOR_INFO"

    .line 1111
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1130
    :goto_1
    :sswitch_7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1131
    invoke-virtual {p0, v0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0800d2 -> :sswitch_6
        0x7f08011d -> :sswitch_5
        0x7f08013d -> :sswitch_7
        0x7f0801b6 -> :sswitch_4
        0x7f08024a -> :sswitch_3
        0x7f08024f -> :sswitch_2
        0x7f080252 -> :sswitch_1
        0x7f08027b -> :sswitch_7
        0x7f0802a0 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .line 1139
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    .line 1140
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 1143
    invoke-direct {p0, p2, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->onPrinterSelectEnd(ILandroid/content/Intent;)V

    goto/16 :goto_0

    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 1147
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->endInkReplAndGoProbePrinter()V

    goto/16 :goto_0

    :cond_1
    const/4 v0, -0x1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    if-ne p2, v0, :cond_4

    .line 1154
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "printAll"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printAll:Z

    .line 1155
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "startValue"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startValue:I

    .line 1156
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "endValue"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->endValue:I

    .line 1159
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    const/16 p2, 0x40

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x1

    if-ne p1, v2, :cond_4

    if-ne p2, v0, :cond_4

    .line 1164
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "curValue"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    .line 1166
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "ID"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    const/16 v0, 0x20

    const/4 v3, 0x0

    sparse-switch p2, :sswitch_data_0

    goto/16 :goto_0

    .line 1208
    :sswitch_0
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1209
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->qualityInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1210
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1211
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality:I

    goto/16 :goto_0

    .line 1255
    :sswitch_1
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1256
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printDateInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1257
    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    .line 1256
    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1258
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1259
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printdate:I

    goto/16 :goto_0

    .line 1182
    :sswitch_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "INDEX"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->typeIndex:I

    .line 1184
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1185
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperTypeInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1188
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    .line 1189
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    iget p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    invoke-virtual {p1, p2, p3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->putID(II)Z

    .line 1191
    iput-boolean v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1192
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1215
    :sswitch_3
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1216
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSourceInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1217
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1220
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_source_info:[I

    if-eqz p2, :cond_3

    array-length p2, p2

    if-le p2, v2, :cond_3

    .line 1221
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSource:I

    .line 1225
    :cond_3
    iput-boolean v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1226
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1168
    :sswitch_4
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "INDEX"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sizeIndex:I

    .line 1170
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1171
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSizeInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1172
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1174
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    .line 1175
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getID(I)I

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    .line 1177
    iput-boolean v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1178
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1196
    :sswitch_5
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Layout;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Layout;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1197
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layoutInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1198
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1199
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout:I

    .line 1202
    iput-boolean v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 1203
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1247
    :sswitch_6
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1248
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirectionInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1249
    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1250
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1251
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirection:I

    goto :goto_0

    .line 1239
    :sswitch_7
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1240
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplexInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1241
    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1242
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1243
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex:I

    goto :goto_0

    .line 1231
    :sswitch_8
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;-><init>()V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1232
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->colorInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1233
    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1234
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1235
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color:I

    .line 1289
    :cond_4
    :goto_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->updatePrinterIcon()V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0800d2 -> :sswitch_8
        0x7f08011d -> :sswitch_7
        0x7f08013d -> :sswitch_6
        0x7f0801b6 -> :sswitch_5
        0x7f08024a -> :sswitch_4
        0x7f08024f -> :sswitch_3
        0x7f080252 -> :sswitch_2
        0x7f08027b -> :sswitch_1
        0x7f0802a0 -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 2

    .line 2392
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    .line 2396
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 2398
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 2401
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2406
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->loadSupportedMediaFile()Z

    .line 2409
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 2411
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackRemotePrinterInfo()V

    .line 2412
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackIPPrinterInfo()V

    .line 2415
    new-instance v0, Lepson/print/inkrpln/InkRplnRepository;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lepson/print/inkrpln/InkRplnRepository;-><init>(Z)V

    .line 2416
    invoke-virtual {v0, p0}, Lepson/print/inkrpln/InkRplnRepository;->deleteTemporaryData(Landroid/content/Context;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 805
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->progressGetOption:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    return-void

    .line 809
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_1

    .line 811
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 814
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 817
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const/16 v0, 0x32

    const/16 v1, -0x32

    const/4 v2, 0x0

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_9

    .line 913
    :sswitch_0
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    add-int/2addr p1, v3

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    .line 914
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    if-lt p1, v0, :cond_2

    .line 915
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    .line 916
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationPlus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 918
    :cond_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 920
    :goto_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 921
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturation:Landroid/widget/TextView;

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 901
    :sswitch_1
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    sub-int/2addr p1, v3

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    .line 902
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    if-gt p1, v1, :cond_3

    .line 903
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    .line 904
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationMinus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2

    .line 906
    :cond_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 908
    :goto_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 909
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturation:Landroid/widget/TextView;

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 991
    :sswitch_2
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality:I

    const v0, 0x7f0802a0

    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 927
    :sswitch_3
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->unbindEpsonService()V

    .line 930
    new-instance p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;

    invoke-direct {p1, p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;-><init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V

    new-array v0, v2, [Ljava/lang/Void;

    .line 974
    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_9

    .line 1017
    :sswitch_4
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printdate:I

    const v0, 0x7f08027b

    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 983
    :sswitch_5
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperType:I

    const v0, 0x7f080252

    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 996
    :sswitch_6
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSource:I

    .line 997
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_source_info:[I

    if-eqz v0, :cond_4

    array-length v0, v0

    if-gt v0, v3, :cond_4

    const/16 p1, 0x80

    :cond_4
    const v0, 0x7f08024f

    .line 1001
    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 979
    :sswitch_7
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSize:I

    const v0, 0x7f08024a

    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 844
    :sswitch_8
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lepson/print/screen/PageRangeSetting;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "SHEETS"

    .line 845
    iget v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sheets:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "printAll"

    .line 846
    iget-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printAll:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "startValue"

    .line 847
    iget v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startValue:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "endValue"

    .line 848
    iget v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->endValue:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v0, 0x2

    .line 849
    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_9

    .line 987
    :sswitch_9
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout:I

    const v0, 0x7f0801b6

    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 1013
    :sswitch_a
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirection:I

    const v0, 0x7f08013d

    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 1009
    :sswitch_b
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex:I

    const v0, 0x7f08011d

    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 831
    :sswitch_c
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    add-int/2addr p1, v3

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    .line 832
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    const/16 v0, 0x1e

    if-lt p1, v0, :cond_5

    .line 833
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    .line 834
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_3

    .line 836
    :cond_5
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 838
    :goto_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 839
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copies:Landroid/widget/TextView;

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 819
    :sswitch_d
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    sub-int/2addr p1, v3

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    .line 820
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    if-gt p1, v3, :cond_6

    .line 821
    iput v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    .line 822
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_4

    .line 824
    :cond_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 826
    :goto_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 827
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copies:Landroid/widget/TextView;

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 889
    :sswitch_e
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    add-int/2addr p1, v3

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    .line 890
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    if-lt p1, v0, :cond_7

    .line 891
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    .line 892
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastPlus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_5

    .line 894
    :cond_7
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 896
    :goto_5
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 897
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrast:Landroid/widget/TextView;

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 877
    :sswitch_f
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    sub-int/2addr p1, v3

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    .line 878
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    if-gt p1, v1, :cond_8

    .line 879
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    .line 880
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastMinus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_6

    .line 882
    :cond_8
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 884
    :goto_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 885
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrast:Landroid/widget/TextView;

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 1005
    :sswitch_10
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color:I

    const v0, 0x7f0800d2

    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->launchDetailScreen(II)V

    goto :goto_9

    .line 865
    :sswitch_11
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    add-int/2addr p1, v3

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    .line 866
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    if-lt p1, v0, :cond_9

    .line 867
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    .line 868
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessPlus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_7

    .line 870
    :cond_9
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 872
    :goto_7
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 873
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightness:Landroid/widget/TextView;

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 853
    :sswitch_12
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    sub-int/2addr p1, v3

    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    .line 854
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    if-gt p1, v1, :cond_a

    .line 855
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    .line 856
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessMinus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_8

    .line 858
    :cond_a
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 860
    :goto_8
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 861
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightness:Landroid/widget/TextView;

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :goto_9
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080080 -> :sswitch_12
        0x7f080082 -> :sswitch_11
        0x7f0800d2 -> :sswitch_10
        0x7f0800de -> :sswitch_f
        0x7f0800df -> :sswitch_e
        0x7f0800e1 -> :sswitch_d
        0x7f0800e2 -> :sswitch_c
        0x7f08011d -> :sswitch_b
        0x7f08013d -> :sswitch_a
        0x7f0801b6 -> :sswitch_9
        0x7f080244 -> :sswitch_8
        0x7f08024a -> :sswitch_7
        0x7f08024f -> :sswitch_6
        0x7f080252 -> :sswitch_5
        0x7f08027b -> :sswitch_4
        0x7f08027e -> :sswitch_3
        0x7f0802a0 -> :sswitch_2
        0x7f0802c1 -> :sswitch_1
        0x7f0802c2 -> :sswitch_0
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 2531
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2532
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->deleteLongTapMessage()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 172
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00bf

    .line 175
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->setContentView(I)V

    const/4 p1, 0x1

    const v0, 0x7f0e0475

    .line 178
    invoke-virtual {p0, v0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->setActionBar(IZ)V

    .line 181
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->bindEpsonService()V

    .line 184
    iput-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mContext:Landroid/content/Context;

    const/4 v0, 0x2

    .line 186
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mRemoteSrcType:I

    .line 190
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "PRINT_DOCUMENT"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    .line 191
    iget-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    if-eqz v1, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "SHEETS"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sheets:I

    .line 194
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "PRINTAREA"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->disablePrintArea:Z

    .line 196
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mRemoteSrcType:I

    .line 200
    :cond_0
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->sheets:I

    .line 203
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mRemoteSrcType:I

    const v0, 0x7f080284

    .line 207
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    const v0, 0x7f08024c

    .line 208
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSizeInfo:Landroid/widget/TextView;

    const v0, 0x7f080253

    .line 209
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperTypeInfo:Landroid/widget/TextView;

    const v0, 0x7f0801b8

    .line 210
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layoutInfo:Landroid/widget/TextView;

    const v0, 0x7f0802a1

    .line 211
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->qualityInfo:Landroid/widget/TextView;

    const v0, 0x7f080250

    .line 212
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paperSourceInfo:Landroid/widget/TextView;

    const v0, 0x7f0800e0

    .line 213
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copies:Landroid/widget/TextView;

    const v0, 0x7f0800e1

    .line 216
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesMinus:Landroid/widget/Button;

    const v0, 0x7f0800e2

    .line 217
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesPlus:Landroid/widget/Button;

    .line 220
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesMinus:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 221
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->copiesPlus:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f0800d5

    .line 224
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->colorInfo:Landroid/widget/TextView;

    const v0, 0x7f08011e

    .line 225
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplexInfo:Landroid/widget/TextView;

    const v0, 0x7f08013e

    .line 226
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->feedDirectionInfo:Landroid/widget/TextView;

    const v0, 0x7f08027c

    .line 227
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printDateInfo:Landroid/widget/TextView;

    const v0, 0x7f08007e

    .line 230
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightness:Landroid/widget/TextView;

    const v0, 0x7f080080

    .line 231
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessMinus:Landroid/widget/Button;

    const v0, 0x7f080082

    .line 232
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->brightnessPlus:Landroid/widget/Button;

    const v0, 0x7f0800dd

    .line 233
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrast:Landroid/widget/TextView;

    const v0, 0x7f0800de

    .line 234
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastMinus:Landroid/widget/Button;

    const v0, 0x7f0800df

    .line 235
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->contrastPlus:Landroid/widget/Button;

    const v0, 0x7f0802c0

    .line 236
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturation:Landroid/widget/TextView;

    const v0, 0x7f0802c1

    .line 237
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationMinus:Landroid/widget/Button;

    const v0, 0x7f0802c2

    .line 238
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saturationPlus:Landroid/widget/Button;

    const v0, 0x7f080297

    .line 241
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->progressGetOption:Landroid/view/View;

    const-string v0, "PREFS_EPSON_CONNECT"

    .line 244
    invoke-virtual {p0, v0, v3}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ENABLE_SHOW_PREVIEW"

    .line 245
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->enableShowPreview:Z

    .line 248
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->loadConfig()V

    .line 250
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->setClickListener()V

    .line 253
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->updatePrinterIcon()V

    .line 255
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 257
    iput-boolean v3, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 258
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->loadSupportedMediaFile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 261
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 2497
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    .line 2498
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2500
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onDestroy()V
    .locals 2

    .line 757
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 760
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->unbindEpsonService()V

    const-string v0, "SettingScr"

    const-string v1, "onDestroy"

    .line 762
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 2506
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-eq v0, v1, :cond_0

    .line 2515
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 2508
    :cond_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->saveChanged()V

    .line 2509
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x3

    .line 2510
    invoke-virtual {p0, v0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 2511
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->finish()V

    const/4 p1, 0x1

    return p1
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "SettingScr"

    const-string v1, "onPause"

    .line 2480
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2481
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 2483
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2484
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->deleteLongTapMessage()V

    :cond_0
    const-string v0, "printer"

    .line 2488
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2490
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->autoConnectSsid:Ljava/lang/String;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnectSimpleAP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 2463
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "SettingScr"

    const-string v1, "onResume()"

    .line 2464
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2467
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isRetryAfterConnectSimpleAp:Z

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    .line 2468
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2469
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->loadSupportedMediaFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2470
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 2472
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void
.end method

.method resetSettings()V
    .locals 2

    const/4 v0, 0x0

    .line 1541
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerDeviceId:Ljava/lang/String;

    .line 1542
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerId:Ljava/lang/String;

    .line 1543
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerEmailAddress:Ljava/lang/String;

    .line 1544
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerIp:Ljava/lang/String;

    const/4 v1, 0x0

    .line 1545
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    .line 1547
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->info:[I

    .line 1548
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_source_info:[I

    .line 1549
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color_info:[I

    .line 1550
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_size_info:[I

    .line 1551
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->paper_type_info:[I

    .line 1552
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->layout_info:[I

    .line 1553
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->quality_info:[I

    .line 1554
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex_info:[I

    const-string v0, ""

    .line 1555
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->autoConnectSsid:Ljava/lang/String;

    .line 1557
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerName:Landroid/widget/TextView;

    const v1, 0x7f0e04d6

    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f080198

    .line 1558
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1560
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->updateSettingView()V

    const/4 v0, 0x1

    .line 1561
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->setScreenState(Ljava/lang/Boolean;)V

    return-void
.end method

.method setClickablePageRange(Z)V
    .locals 4

    const v0, 0x7f080244

    .line 1532
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f080242

    .line 1533
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080245

    .line 1534
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method setVisibilityPageRange(Z)V
    .locals 4

    const v0, 0x7f080244

    .line 1522
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080243

    .line 1523
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2443
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 2444
    invoke-virtual {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2445
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 2446
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const p2, 0x7f0e03ff

    .line 2447
    invoke-virtual {p0, p2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$5;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$5;-><init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V

    invoke-virtual {p1, p2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 2455
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    const/4 p1, 0x1

    .line 2457
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->updateSupportedMediaFile(Z)Z

    return-void
.end method

.method updateSettingView()V
    .locals 5

    .line 1395
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    const/4 v0, 0x0

    .line 1424
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->setVisibilityPageRange(Z)V

    .line 1430
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->color_info:[I

    const v2, 0x7f0800d6

    const/16 v3, 0x8

    if-eqz v1, :cond_1

    .line 1431
    array-length v1, v1

    const/4 v4, 0x1

    if-gt v1, v4, :cond_0

    .line 1432
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1434
    :cond_0
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1437
    :cond_1
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1443
    :goto_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->duplex_info:[I

    const v1, 0x7f080120

    .line 1452
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f08011d

    .line 1453
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1459
    iget v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->lang:I

    const v2, 0x7f08013d

    const v4, 0x7f080140

    packed-switch v1, :pswitch_data_0

    .line 1469
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1470
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1464
    :pswitch_0
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1465
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1477
    :goto_1
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isDocumentSetting:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->printerLocation:I

    :cond_2
    const v0, 0x7f08027b

    .line 1484
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

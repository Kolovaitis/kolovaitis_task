.class public Lcom/epson/cameracopy/ui/CameraPreviewActivity;
.super Lepson/print/ActivityIACommon;
.source "CameraPreviewActivity.java"


# static fields
.field public static final PARAM_APP_INIT:Ljava/lang/String; = "app_init"

.field private static final RESULT_RUNTIMEPERMMISSION:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 70
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p2, 0x1

    if-eq p1, p2, :cond_0

    goto :goto_0

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->prepareCameraPreviewFragment()V

    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .line 105
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0800d8

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 109
    :cond_0
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 25
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a001c

    .line 26
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->setContentView(I)V

    const/4 v0, 0x1

    const v1, 0x7f0e02f1

    .line 29
    invoke-virtual {p0, v1, v0}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->setActionBar(IZ)V

    if-nez p1, :cond_1

    .line 33
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "android.permission.CAMERA"

    .line 34
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x2

    .line 35
    new-array v2, v1, [Ljava/lang/String;

    const v3, 0x7f0e0422

    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v2, v5

    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 36
    new-array v1, v1, [Ljava/lang/String;

    const v3, 0x7f0e041e

    .line 37
    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v5

    .line 38
    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0e0423

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v3, v4}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 45
    new-instance v3, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v4, p1, v5

    invoke-direct {v3, v4, v2, v1}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 47
    invoke-static {p0, p1}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 48
    invoke-static {p0, v3, v0}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->prepareCameraPreviewFragment()V

    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 0

    .line 94
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    .line 98
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method prepareCameraPreviewFragment()V
    .locals 3

    .line 58
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "app_init"

    const/4 v2, 0x1

    .line 59
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 60
    new-instance v1, Lcom/epson/cameracopy/ui/CameraPreviewFragment;

    invoke-direct {v1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;-><init>()V

    .line 62
    invoke-virtual {v1, v0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->setAppInitFlag(Z)V

    .line 64
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x7f0800d8

    .line 65
    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method

.class public Lcom/epson/cameracopy/ui/ImagePreviewActivity;
.super Lepson/print/ActivityIACommon;
.source "ImagePreviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;,
        Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;
    }
.end annotation


# static fields
.field public static final PARAM_CAMERA_FILE:Ljava/lang/String; = "camera-file"

.field public static final PARAM_PICTURE_FILENAME:Ljava/lang/String; = "file_name"

.field public static final PARAM_SHOW_SIZE_SET_MESSAGE_ON_PRINT_PREVIEW:Ljava/lang/String; = "pram-sizeset-message-printpreview"

.field private static final REQEST_CODE_IMAGE_CROP:I = 0x1

.field private static final REQEST_RUNTIMEPERMMISSION:I = 0x2

.field private static final REQUEST_WRITE_PERMISSION_FOR_IMAGE_FORMAT_CONVERT:I = 0x3


# instance fields
.field private mFragment:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method


# virtual methods
.method public backCameraPreviewActivity()V
    .locals 2

    .line 186
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/cameracopy/ui/CameraPreviewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    .line 188
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x20000000

    .line 189
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 190
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->mFragment:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    if-eqz v0, :cond_0

    .line 176
    invoke-static {v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->access$000(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 182
    :cond_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->backCameraPreviewActivity()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 85
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a003b

    .line 86
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->setContentView(I)V

    const v0, 0x7f0e0393

    const/4 v1, 0x1

    .line 89
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->setActionBar(IZ)V

    if-nez p1, :cond_4

    .line 92
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-nez p1, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->finish()V

    return-void

    :cond_0
    const-string v0, "file_name"

    .line 98
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "camera-file"

    .line 100
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/epson/cameracopy/device/CameraFile;

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->finish()V

    return-void

    .line 106
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "file_name"

    .line 107
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "camera-file"

    .line 108
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 110
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->finish()V

    return-void

    :cond_2
    if-eqz v1, :cond_3

    .line 115
    invoke-virtual {v1}, Lcom/epson/cameracopy/device/CameraFile;->getValidFileName()Ljava/lang/String;

    move-result-object p1

    .line 116
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_3

    .line 119
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->finish()V

    return-void

    .line 124
    :cond_3
    invoke-static {v0, v1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getInstance(Ljava/lang/String;Lcom/epson/cameracopy/device/CameraFile;)Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->mFragment:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    .line 126
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    const v0, 0x7f0800d8

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity;->mFragment:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    .line 127
    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_4
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .line 148
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onNewIntent(Landroid/content/Intent;)V

    .line 150
    invoke-static {p0, p1}, Lepson/print/Util/CommonMessage;->showInvalidPrintMessageIfEpsonNfcPrinter(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 0

    .line 166
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    .line 170
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 0

    .line 133
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 135
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 140
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x0

    .line 143
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    return-void
.end method

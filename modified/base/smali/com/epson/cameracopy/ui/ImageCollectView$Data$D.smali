.class Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;
.super Ljava/lang/Object;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageCollectView$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "D"
.end annotation


# instance fields
.field public dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

.field public dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

.field public ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

.field public det:Lcom/epson/cameracopy/ui/ImageCollectView$DataEnhanceText;

.field public mBaseScale:[F

.field public mPointPreviewImageOffset:Landroid/graphics/PointF;

.field public mRectImageArea:Landroid/graphics/RectF;

.field public mRectMessageArea:Landroid/graphics/RectF;

.field public mRectPreviewImage:Landroid/graphics/RectF;

.field public mRectView:Landroid/graphics/RectF;

.field final synthetic this$1:Lcom/epson/cameracopy/ui/ImageCollectView$Data;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/ImageCollectView$Data;)V
    .locals 5

    .line 147
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->this$1:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 134
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    .line 135
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->det:Lcom/epson/cameracopy/ui/ImageCollectView$DataEnhanceText;

    .line 136
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    .line 137
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    .line 139
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    .line 140
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    .line 141
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    .line 142
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    .line 145
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectMessageArea:Landroid/graphics/RectF;

    .line 148
    new-instance v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    .line 149
    new-instance v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataEnhanceText;

    iget-object v1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView$DataEnhanceText;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->det:Lcom/epson/cameracopy/ui/ImageCollectView$DataEnhanceText;

    .line 150
    new-instance v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    const/4 v0, 0x4

    .line 151
    new-array v0, v0, [Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    .line 152
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    new-instance v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget-object v2, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {v1, v2}, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 153
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    new-instance v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget-object v3, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {v1, v3}, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    const/4 v3, 0x1

    aput-object v1, v0, v3

    .line 154
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    new-instance v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget-object v4, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {v1, v4}, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    const/4 v4, 0x2

    aput-object v1, v0, v4

    .line 155
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    new-instance v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {v1, p1}, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    const/4 p1, 0x3

    aput-object v1, v0, p1

    .line 156
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    .line 157
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    .line 158
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    .line 159
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectMessageArea:Landroid/graphics/RectF;

    .line 160
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    .line 168
    new-array p1, v4, [F

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    .line 169
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    const/high16 v0, 0x3f800000    # 1.0f

    aput v0, p1, v2

    .line 170
    aput v0, p1, v3

    return-void
.end method

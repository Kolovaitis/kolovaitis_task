.class Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;
.super Landroid/os/AsyncTask;
.source "ImageFolderFileViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;->setImageFolderFile(Lcom/epson/cameracopy/ui/ImageFolderFile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/epson/cameracopy/ui/ImageFolderFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

.field final synthetic val$imageFolderFile:Lcom/epson/cameracopy/ui/ImageFolderFile;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;Lcom/epson/cameracopy/ui/ImageFolderFile;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->this$1:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->val$imageFolderFile:Lcom/epson/cameracopy/ui/ImageFolderFile;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/epson/cameracopy/ui/ImageFolderFile;
    .locals 1

    .line 37
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->val$imageFolderFile:Lcom/epson/cameracopy/ui/ImageFolderFile;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->this$1:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;->this$0:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->access$200(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;)Landroid/app/Application;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/ImageFolderFile;->prepareImage(Landroid/content/Context;)Z

    .line 43
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->val$imageFolderFile:Lcom/epson/cameracopy/ui/ImageFolderFile;

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->doInBackground([Ljava/lang/Void;)Lcom/epson/cameracopy/ui/ImageFolderFile;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Lcom/epson/cameracopy/ui/ImageFolderFile;)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->this$1:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;->this$0:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->access$000(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;)Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;->access$100(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/epson/cameracopy/ui/ImageFolderFile;

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->onPostExecute(Lcom/epson/cameracopy/ui/ImageFolderFile;)V

    return-void
.end method

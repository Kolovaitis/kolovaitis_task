.class Lcom/epson/cameracopy/ui/CameraPrintProgress$10;
.super Ljava/lang/Object;
.source "CameraPrintProgress.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CameraPrintProgress;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 914
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$10;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 918
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$10;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$602(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 919
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$10;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->removeDialog(I)V

    .line 921
    :try_start_0
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->confirmContinueable(Z)I

    .line 922
    sput p2, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 925
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 936
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$10;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$1802(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 937
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$10;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$302(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    const-string p1, "PrintProgress"

    const-string p2, "cont/cancel dialog. cancel clicked"

    .line 938
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$10;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

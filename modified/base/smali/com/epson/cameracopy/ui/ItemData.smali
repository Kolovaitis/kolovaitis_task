.class Lcom/epson/cameracopy/ui/ItemData;
.super Ljava/lang/Object;
.source "FolderSelectActivity.java"


# instance fields
.field mFileName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 563
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ItemData;->mFileName:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 565
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ItemData;->mFileName:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method public DrawItem(ILandroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    const p1, 0x7f080143

    .line 570
    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const p3, 0x7f080144

    .line 571
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 572
    iget-object p3, p0, Lcom/epson/cameracopy/ui/ItemData;->mFileName:Ljava/lang/String;

    if-eqz p3, :cond_0

    const p3, 0x7f0700a0

    .line 573
    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 574
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ItemData;->mFileName:Ljava/lang/String;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const p3, 0x7f070103

    .line 576
    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    const-string p1, ""

    .line 577
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

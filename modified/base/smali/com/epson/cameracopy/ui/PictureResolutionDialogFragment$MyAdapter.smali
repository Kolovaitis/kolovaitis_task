.class Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PictureResolutionDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# instance fields
.field mLayoutInflater:Landroid/view/LayoutInflater;

.field mSelectNumber:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/CharSequence;I)V
    .locals 0

    .line 249
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const-string p2, "layout_inflater"

    .line 251
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 253
    iput p4, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;->mSelectNumber:I

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 260
    iget-object p2, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a00bc

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const p3, 0x7f080112

    .line 264
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 265
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 266
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f0800fc

    .line 268
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 269
    iget v1, p0, Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$MyAdapter;->mSelectNumber:I

    if-eq p1, v1, :cond_1

    const/4 p1, 0x4

    .line 270
    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-object p2
.end method

.class Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;
.super Ljava/lang/Object;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageCollectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TimerCropImageCropCircle"
.end annotation


# static fields
.field private static final MSG_TIMER_CROP_IMAGE_CROP_CIRCLE_TIMEOUT:I = 0x1f4


# instance fields
.field private mBreak:Z

.field private mHandlerTimerCropImageCropCircle:Landroid/os/Handler;

.field private mNewTime:J

.field private mStartTime:J

.field private mTimer:Ljava/lang/Thread;

.field private mView:Landroid/view/View;

.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImageCollectView;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V
    .locals 1

    .line 1842
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 1846
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mTimer:Ljava/lang/Thread;

    const/4 v0, 0x0

    .line 1847
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mBreak:Z

    .line 1850
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mView:Landroid/view/View;

    .line 1901
    new-instance p1, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle$2;

    invoke-direct {p1, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle$2;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mHandlerTimerCropImageCropCircle:Landroid/os/Handler;

    return-void
.end method

.method private Timer(I)V
    .locals 2

    const/4 v0, 0x0

    .line 1876
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mBreak:Z

    .line 1877
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle$1;

    invoke-direct {v1, p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle$1;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mTimer:Ljava/lang/Thread;

    .line 1897
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mTimer:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic access$1000(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;)Landroid/os/Handler;
    .locals 0

    .line 1842
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mHandlerTimerCropImageCropCircle:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;)Landroid/view/View;
    .locals 0

    .line 1842
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$700(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;)Z
    .locals 0

    .line 1842
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mBreak:Z

    return p0
.end method

.method static synthetic access$800(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;)J
    .locals 2

    .line 1842
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mNewTime:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;J)J
    .locals 0

    .line 1842
    iput-wide p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mNewTime:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;)J
    .locals 2

    .line 1842
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mStartTime:J

    return-wide v0
.end method


# virtual methods
.method public TimerOff()V
    .locals 1

    .line 1867
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mTimer:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1868
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mBreak:Z

    .line 1869
    :goto_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mTimer:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public TimerOn(Landroid/view/View;)V
    .locals 2

    .line 1853
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mView:Landroid/view/View;

    .line 1854
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mTimer:Ljava/lang/Thread;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mTimer:Ljava/lang/Thread;

    if-nez p1, :cond_2

    .line 1856
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mStartTime:J

    const/16 p1, 0x7d0

    .line 1857
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->Timer(I)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    .line 1859
    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1861
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->mStartTime:J

    :cond_3
    :goto_0
    return-void
.end method

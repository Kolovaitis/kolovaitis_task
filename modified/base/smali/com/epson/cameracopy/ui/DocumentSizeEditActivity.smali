.class public Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;
.super Lepson/print/ActivityIACommon;
.source "DocumentSizeEditActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field private final RATIO_INCHTOMM:D

.field private final SIZE_INCH_MAX:D

.field private final SIZE_INCH_MIN:D

.field private final SIZE_MM_MAX:I

.field private final SIZE_MM_MIN:I

.field mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

.field private mHeight:D

.field private mHeightDec:Landroid/widget/Button;

.field private mHeightInc:Landroid/widget/Button;

.field private mHeightText:Landroid/widget/TextView;

.field private mInch:Z

.field mPosition:I

.field private mScale:Landroid/widget/RadioGroup;

.field private mWidth:D

.field private mWidthDec:Landroid/widget/Button;

.field private mWidthInc:Landroid/widget/Button;

.field private mWidthText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 27
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x1

    .line 30
    iput v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->SIZE_MM_MIN:I

    const/16 v0, 0x3e8

    .line 31
    iput v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->SIZE_MM_MAX:I

    const-wide v0, 0x3fb999999999999aL    # 0.1

    .line 32
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->SIZE_INCH_MIN:D

    const-wide v0, 0x4043af5c28f5c28fL    # 39.37

    .line 33
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->SIZE_INCH_MAX:D

    const-wide v0, 0x4039666666666666L    # 25.4

    .line 34
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->RATIO_INCHTOMM:D

    const/4 v0, 0x0

    .line 36
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    return-void
.end method

.method private checkDocumentSize()V
    .locals 5

    const v0, 0x7f080131

    .line 309
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 310
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 313
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0e03f4

    .line 314
    invoke-static {v0}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->newInstance(I)Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    move-result-object v0

    .line 315
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "error-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 320
    :cond_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v1, v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setDocSizeName(Ljava/lang/String;)V

    .line 321
    iget-boolean v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    .line 322
    :goto_0
    iget-object v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v3, v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setScaleId(I)V

    .line 323
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    invoke-direct {p0, v3, v4, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setWidth(D)V

    .line 324
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    invoke-direct {p0, v3, v4, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setHeight(D)V

    .line 327
    invoke-static {p0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;

    move-result-object v1

    .line 328
    iget v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mPosition:I

    invoke-virtual {v1, v0, v2}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->isExistDocumentSizeName(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0e0370

    .line 329
    invoke-static {v0}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->newInstance(I)Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    move-result-object v0

    .line 330
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "error-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 334
    :cond_2
    iget v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mPosition:I

    if-gez v0, :cond_3

    .line 335
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v1, v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->add(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;)V

    .line 336
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->finish()V

    goto :goto_1

    .line 341
    :cond_3
    iget-object v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v1, v2, v0}, Lcom/epson/cameracopy/printlayout/RegistedDocumentSizeList;->update(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;I)V

    .line 342
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->finish()V

    :goto_1
    return-void
.end method

.method private convertDocumentSize()V
    .locals 10

    .line 267
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    const-wide v1, 0x4039666666666666L    # 25.4

    if-eqz v0, :cond_2

    .line 269
    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    div-double/2addr v3, v1

    iput-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 270
    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const-wide v5, 0x3fb999999999999aL    # 0.1

    cmpg-double v0, v3, v5

    if-gez v0, :cond_0

    move-wide v3, v5

    :cond_0
    iput-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 271
    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const/4 v0, 0x1

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v3

    .line 272
    iget-object v7, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " in"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iput-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 274
    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    div-double/2addr v3, v1

    iput-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 275
    iget-wide v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    cmpg-double v3, v1, v5

    if-gez v3, :cond_1

    move-wide v1, v5

    :cond_1
    iput-wide v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 276
    iget-wide v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    invoke-direct {p0, v1, v2, v0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    .line 277
    iget-object v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " in"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    goto :goto_0

    .line 284
    :cond_2
    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    mul-double v3, v3, v1

    iput-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 285
    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const-wide v5, 0x408f400000000000L    # 1000.0

    cmpl-double v0, v3, v5

    if-lez v0, :cond_3

    move-wide v3, v5

    :cond_3
    iput-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 286
    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const/4 v0, 0x0

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v3

    double-to-int v3, v3

    .line 287
    iget-object v4, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " mm"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    int-to-double v3, v3

    .line 288
    iput-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 289
    iget-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    mul-double v3, v3, v1

    iput-wide v3, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 290
    iget-wide v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    cmpl-double v3, v1, v5

    if-lez v3, :cond_4

    move-wide v1, v5

    :cond_4
    iput-wide v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 291
    iget-wide v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    invoke-direct {p0, v1, v2, v0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    double-to-int v0, v0

    .line 292
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " mm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    int-to-double v0, v0

    .line 293
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    :goto_0
    return-void
.end method

.method private decDouble(DD)D
    .locals 0

    .line 359
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    .line 360
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p2

    .line 361
    new-instance p3, Ljava/math/BigDecimal;

    invoke-direct {p3, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 362
    new-instance p1, Ljava/math/BigDecimal;

    invoke-direct {p1, p2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 363
    invoke-virtual {p3, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 364
    invoke-virtual {p1}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide p1

    return-wide p1
.end method

.method private decHeight()V
    .locals 5

    .line 218
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    if-eqz v0, :cond_1

    .line 219
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const-wide v2, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->decDouble(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 220
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    cmpg-double v4, v0, v2

    if-gez v4, :cond_0

    .line 221
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 223
    :cond_0
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    .line 224
    iget-object v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " in"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 228
    :cond_1
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->decDouble(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 229
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    cmpg-double v4, v0, v2

    if-gez v4, :cond_2

    .line 230
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 232
    :cond_2
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    double-to-int v0, v0

    .line 233
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " mm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    int-to-double v0, v0

    .line 234
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    :goto_0
    return-void
.end method

.method private decWidth()V
    .locals 5

    .line 168
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    if-eqz v0, :cond_1

    .line 169
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const-wide v2, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->decDouble(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 170
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    cmpg-double v4, v0, v2

    if-gez v4, :cond_0

    .line 171
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 173
    :cond_0
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    .line 174
    iget-object v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " in"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    goto :goto_0

    .line 179
    :cond_1
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->decDouble(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 180
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    cmpg-double v4, v0, v2

    if-gez v4, :cond_2

    .line 181
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 183
    :cond_2
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    double-to-int v0, v0

    .line 184
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " mm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    int-to-double v0, v0

    .line 185
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    :goto_0
    return-void
.end method

.method private getRoundDownValue(DI)D
    .locals 0

    .line 300
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    .line 301
    new-instance p2, Ljava/math/BigDecimal;

    invoke-direct {p2, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 302
    invoke-virtual {p2, p3, p1}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object p1

    .line 303
    invoke-virtual {p1}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide p1

    return-wide p1
.end method

.method private incDouble(DD)D
    .locals 0

    .line 349
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    .line 350
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p2

    .line 351
    new-instance p3, Ljava/math/BigDecimal;

    invoke-direct {p3, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 352
    new-instance p1, Ljava/math/BigDecimal;

    invoke-direct {p1, p2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 353
    invoke-virtual {p3, p1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 354
    invoke-virtual {p1}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide p1

    return-wide p1
.end method

.method private incHeight()V
    .locals 5

    .line 242
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    if-eqz v0, :cond_1

    .line 243
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const-wide v2, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->incDouble(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 244
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const-wide v2, 0x4043af5c28f5c28fL    # 39.37

    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    .line 245
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 247
    :cond_0
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    .line 248
    iget-object v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " in"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    goto :goto_0

    .line 253
    :cond_1
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->incDouble(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 254
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const-wide v2, 0x408f400000000000L    # 1000.0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_2

    .line 255
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 257
    :cond_2
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    double-to-int v0, v0

    .line 258
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " mm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    int-to-double v0, v0

    .line 259
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    :goto_0
    return-void
.end method

.method private incWidth()V
    .locals 5

    .line 193
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    if-eqz v0, :cond_1

    .line 194
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const-wide v2, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->incDouble(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 195
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const-wide v2, 0x4043af5c28f5c28fL    # 39.37

    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    .line 196
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 198
    :cond_0
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    .line 199
    iget-object v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " in"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    goto :goto_0

    .line 204
    :cond_1
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->incDouble(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 205
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const-wide v2, 0x408f400000000000L    # 1000.0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_2

    .line 206
    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 208
    :cond_2
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    double-to-int v0, v0

    .line 209
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " mm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    int-to-double v0, v0

    .line 210
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    :goto_0
    return-void
.end method

.method private setDocumentSize()V
    .locals 5

    .line 135
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DocumentSize"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 136
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DocumentSizePos"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mPosition:I

    const v0, 0x7f080131

    .line 139
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 140
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v1, p0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getScaleId()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    .line 144
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mScale:Landroid/widget/RadioGroup;

    const v2, 0x7f0801a0

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 146
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getWidth()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 147
    iget-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    invoke-direct {p0, v2, v3, v1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v2

    .line 148
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " in"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getHeight()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 150
    iget-wide v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    invoke-direct {p0, v2, v3, v1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    .line 151
    iget-object v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " in"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mScale:Landroid/widget/RadioGroup;

    const v1, 0x7f08021d

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 155
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getWidth()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    .line 156
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidth:D

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    double-to-int v0, v0

    .line 157
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " mm"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getHeight()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    .line 159
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeight:D

    invoke-direct {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getRoundDownValue(DI)D

    move-result-wide v0

    double-to-int v0, v0

    .line 160
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " mm"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method


# virtual methods
.method protected deleteLongTapMessage()V
    .locals 8

    .line 395
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    add-long/2addr v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 397
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthDec:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 398
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthInc:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 399
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightDec:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 400
    iget-object v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightInc:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    const v0, 0x7f0801a0

    if-eq p2, v0, :cond_1

    const v0, 0x7f08021d

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 120
    iput-boolean p2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    .line 121
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->convertDocumentSize()V

    goto :goto_0

    :cond_1
    const/4 p2, 0x1

    .line 124
    iput-boolean p2, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mInch:Z

    .line 125
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->convertDocumentSize()V

    .line 128
    :cond_2
    :goto_0
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->invalidate()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 98
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 103
    :sswitch_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->incWidth()V

    goto :goto_0

    .line 100
    :sswitch_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->decWidth()V

    goto :goto_0

    .line 109
    :sswitch_2
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->incHeight()V

    goto :goto_0

    .line 106
    :sswitch_3
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->decHeight()V

    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f08017c -> :sswitch_3
        0x7f08017d -> :sswitch_2
        0x7f080391 -> :sswitch_1
        0x7f080392 -> :sswitch_0
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 405
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 406
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->deleteLongTapMessage()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 55
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0027

    .line 56
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->setContentView(I)V

    const p1, 0x7f0e031f

    const/4 v0, 0x1

    .line 59
    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->setActionBar(IZ)V

    const p1, 0x7f080117

    .line 62
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioGroup;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mScale:Landroid/widget/RadioGroup;

    .line 63
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mScale:Landroid/widget/RadioGroup;

    invoke-virtual {p1, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    const p1, 0x7f080390

    .line 66
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthText:Landroid/widget/TextView;

    const p1, 0x7f080391

    .line 67
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthDec:Landroid/widget/Button;

    .line 68
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthDec:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080392

    .line 69
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthInc:Landroid/widget/Button;

    .line 70
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthInc:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08017b

    .line 73
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightText:Landroid/widget/TextView;

    const p1, 0x7f08017c

    .line 74
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightDec:Landroid/widget/Button;

    .line 75
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightDec:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08017d

    .line 76
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightInc:Landroid/widget/Button;

    .line 77
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightInc:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthDec:Landroid/widget/Button;

    invoke-static {p1}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 81
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mWidthInc:Landroid/widget/Button;

    invoke-static {p1}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 82
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightDec:Landroid/widget/Button;

    invoke-static {p1}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 83
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mHeightInc:Landroid/widget/Button;

    invoke-static {p1}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 85
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->setDocumentSize()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 370
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->mDocumentSizeInfo:Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-virtual {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    .line 372
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 375
    :cond_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 381
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-eq v0, v1, :cond_0

    .line 387
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 383
    :cond_0
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->checkDocumentSize()V

    const/4 p1, 0x1

    return p1
.end method

.method protected onPause()V
    .locals 1

    .line 90
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 91
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;->deleteLongTapMessage()V

    :cond_0
    return-void
.end method

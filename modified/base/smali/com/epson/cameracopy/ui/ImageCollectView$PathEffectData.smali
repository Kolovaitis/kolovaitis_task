.class Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;
.super Ljava/lang/Object;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageCollectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PathEffectData"
.end annotation


# static fields
.field private static final BLINKING_TIME:I = 0x1f4

.field private static final DEFAULT_BLINKING_DELAYTIMES:I = 0xc8

.field private static final MSG_INVALIDATE_VIEW:I = 0x385


# instance fields
.field private mBlinkingDelayTimes:J

.field private mBreak:Z

.field private mHandlerPathEffectData:Landroid/os/Handler;

.field private mNewTime:J

.field private mPathEffectBlinkingIndex:I

.field private mPathEffectBlinkingTime:J

.field private mPathEffectList:[Landroid/graphics/PathEffect;

.field private mRectDirty:Landroid/graphics/Rect;

.field private mStartTime:J

.field private mTimer:Ljava/lang/Thread;

.field private mView:Landroid/view/View;

.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImageCollectView;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/ImageCollectView;Landroid/view/View;)V
    .locals 5

    .line 1932
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x5

    .line 1919
    new-array p1, p1, [Landroid/graphics/PathEffect;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    const/4 p1, 0x0

    .line 1920
    iput p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingIndex:I

    const-wide/16 v0, 0x0

    .line 1921
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingTime:J

    const-wide/16 v0, 0xc8

    .line 1922
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mBlinkingDelayTimes:J

    const/4 v0, 0x0

    .line 1923
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mView:Landroid/view/View;

    .line 1924
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1, p1, p1, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mRectDirty:Landroid/graphics/Rect;

    .line 1927
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mTimer:Ljava/lang/Thread;

    .line 1928
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mBreak:Z

    .line 2018
    new-instance v0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData$2;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData$2;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mHandlerPathEffectData:Landroid/os/Handler;

    .line 1933
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x2

    new-array v3, v2, [F

    fill-array-data v3, :array_0

    const/high16 v4, 0x41000000    # 8.0f

    invoke-direct {v1, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    aput-object v1, v0, p1

    .line 1934
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-direct {v0, v1, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    const/4 v1, 0x1

    aput-object v0, p1, v1

    .line 1935
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v2, [F

    fill-array-data v1, :array_2

    const/high16 v3, 0x40800000    # 4.0f

    invoke-direct {v0, v1, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    aput-object v0, p1, v2

    .line 1936
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v2, [F

    fill-array-data v1, :array_3

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    const/4 v1, 0x3

    aput-object v0, p1, v1

    .line 1937
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v2, [F

    fill-array-data v1, :array_4

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    const/4 v1, 0x4

    aput-object v0, p1, v1

    .line 1940
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mView:Landroid/view/View;

    return-void

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    :array_1
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    :array_2
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    :array_3
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    :array_4
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private BlinkOn(I)V
    .locals 2

    .line 1980
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mTimer:Ljava/lang/Thread;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mTimer:Ljava/lang/Thread;

    if-nez p1, :cond_2

    .line 1982
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mStartTime:J

    const/16 p1, 0x1f4

    .line 1983
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->Timer(I)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    .line 1985
    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1987
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mStartTime:J

    :cond_3
    :goto_0
    return-void
.end method

.method private Timer(I)V
    .locals 2

    const/4 v0, 0x0

    .line 1993
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mBreak:Z

    .line 1994
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData$1;

    invoke-direct {v1, p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData$1;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mTimer:Ljava/lang/Thread;

    .line 2013
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mTimer:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic access$1300(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;)Z
    .locals 0

    .line 1915
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mBreak:Z

    return p0
.end method

.method static synthetic access$1400(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;)J
    .locals 2

    .line 1915
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mNewTime:J

    return-wide v0
.end method

.method static synthetic access$1402(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;J)J
    .locals 0

    .line 1915
    iput-wide p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mNewTime:J

    return-wide p1
.end method

.method static synthetic access$1500(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;)Landroid/os/Handler;
    .locals 0

    .line 1915
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mHandlerPathEffectData:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;)J
    .locals 2

    .line 1915
    iget-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;)Landroid/view/View;
    .locals 0

    .line 1915
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;)Landroid/graphics/Rect;
    .locals 0

    .line 1915
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mRectDirty:Landroid/graphics/Rect;

    return-object p0
.end method


# virtual methods
.method public BlinkingIndex(Landroid/graphics/RectF;)Landroid/graphics/PathEffect;
    .locals 7

    .line 1944
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1945
    iget-wide v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingTime:J

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    .line 1946
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingTime:J

    if-eqz p1, :cond_0

    .line 1948
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mRectDirty:Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 1949
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mRectDirty:Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 1950
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mRectDirty:Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/RectF;->right:F

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 1951
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mRectDirty:Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr p1, v4

    float-to-int p1, p1

    iput p1, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1953
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mRectDirty:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 1956
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->BlinkOn(I)V

    .line 1957
    iget-wide v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingTime:J

    iget-wide v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mBlinkingDelayTimes:J

    add-long/2addr v2, v4

    cmp-long v4, v2, v0

    if-gez v4, :cond_3

    .line 1958
    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingIndex:I

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    .line 1959
    iput v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingIndex:I

    goto :goto_1

    .line 1961
    :cond_2
    iput p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingIndex:I

    .line 1963
    :goto_1
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingTime:J

    .line 1965
    :cond_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectBlinkingIndex:I

    aget-object p1, p1, v0

    return-object p1
.end method

.method public Cleanup()V
    .locals 1

    .line 1969
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mTimer:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1970
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mBreak:Z

    .line 1971
    :goto_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mTimer:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1974
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mTimer:Ljava/lang/Thread;

    .line 1975
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mView:Landroid/view/View;

    .line 1976
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->mPathEffectList:[Landroid/graphics/PathEffect;

    return-void
.end method

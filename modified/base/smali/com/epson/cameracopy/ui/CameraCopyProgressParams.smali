.class public Lcom/epson/cameracopy/ui/CameraCopyProgressParams;
.super Ljava/lang/Object;
.source "CameraCopyProgressParams.java"

# interfaces
.implements Lepson/print/screen/PrintProgress$ProgressParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApfMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getEpsonColorMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getOriginalSheetSize()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 2

    .line 39
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    const/4 v1, 0x5

    .line 40
    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    return-object v0
.end method

.method public getPrintSetting(Landroid/content/Context;)Lepson/print/screen/PrintSetting;
    .locals 2

    .line 47
    new-instance v0, Lepson/print/screen/PrintSetting;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, p1, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    return-object v0
.end method

.method public getPrintSettingType()Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public isPaperLandscape()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public print(Lepson/print/service/IEpsonService;Z)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 p1, 0x0

    return p1
.end method

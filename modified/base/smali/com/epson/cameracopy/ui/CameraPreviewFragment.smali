.class public Lcom/epson/cameracopy/ui/CameraPreviewFragment;
.super Landroid/support/v4/app/Fragment;
.source "CameraPreviewFragment.java"

# interfaces
.implements Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewListener2;
.implements Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;
    }
.end annotation


# static fields
.field private static final COLOR_BLACK:Lorg/opencv/core/Scalar;

.field private static final PAPER_SQUARE_COLOR:[Lorg/opencv/core/Scalar;

.field private static final POSITION_0_0:Lorg/opencv/core/Point;

.field private static final REFERENCE_ACCELARATION:D = 0.45

.field private static final TAG:Ljava/lang/String; = "CameraPreviewFragment"


# instance fields
.field private final AUTO_PICTURE_RANGE_VALUE:D

.field private final INIT_SPAN:I

.field private final PAPER_DETECT_VALUE:D

.field private isFragmentVisible:Z

.field private mAutoPhotoMode:Z

.field private final mAutoPictureStillTime:J

.field private mCameraPictureLowResolutionDialog:Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

.field private mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

.field private mDisableSettingsButton:Z

.field private mDisplayDensity:F

.field private mDisplayRotation:I

.field private mFunctionBarView:Landroid/view/View;

.field private final mGridColor:Lorg/opencv/core/Scalar;

.field private final mGridColorWidth:I

.field private mHasCamera:Z

.field private mInitCount:I

.field private mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

.field private mPAfTime:J

.field private final mPaperSquareCircleSize:I

.field private mPhotoSelectButton:Landroid/widget/Button;

.field private mPhtographButton:Landroid/widget/Button;

.field private volatile mPictureTaking:Z

.field private final mPositionCursorColor:Lorg/opencv/core/Scalar;

.field private final mPositionCursorDispRate:F

.field private volatile mPreviewActivityStarted:Z

.field private mRgba:Lorg/opencv/core/Mat;

.field private final mScreenSize:Lorg/opencv/core/Point;

.field private final mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

.field private mTakePictureTask:Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;

.field private noCameraDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 20

    const/4 v0, 0x2

    .line 79
    new-array v0, v0, [Lorg/opencv/core/Scalar;

    new-instance v10, Lorg/opencv/core/Scalar;

    const-wide v2, 0x406fe00000000000L    # 255.0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide v8, 0x406fe00000000000L    # 255.0

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lorg/opencv/core/Scalar;-><init>(DDDD)V

    const/4 v1, 0x0

    aput-object v10, v0, v1

    new-instance v1, Lorg/opencv/core/Scalar;

    const-wide/16 v12, 0x0

    const-wide v14, 0x406fe00000000000L    # 255.0

    const-wide/16 v16, 0x0

    const-wide v18, 0x406fe00000000000L    # 255.0

    move-object v11, v1

    invoke-direct/range {v11 .. v19}, Lorg/opencv/core/Scalar;-><init>(DDDD)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->PAPER_SQUARE_COLOR:[Lorg/opencv/core/Scalar;

    .line 84
    new-instance v0, Lorg/opencv/core/Scalar;

    const-wide/16 v8, 0x0

    const-wide v10, 0x406fe00000000000L    # 255.0

    move-object v3, v0

    invoke-direct/range {v3 .. v11}, Lorg/opencv/core/Scalar;-><init>(DDDD)V

    sput-object v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->COLOR_BLACK:Lorg/opencv/core/Scalar;

    .line 86
    new-instance v0, Lorg/opencv/core/Point;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2, v1, v2}, Lorg/opencv/core/Point;-><init>(DD)V

    sput-object v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->POSITION_0_0:Lorg/opencv/core/Point;

    .line 151
    invoke-static {}, Lorg/opencv/android/OpenCVLoader;->initDebug()Z

    return-void
.end method

.method public constructor <init>()V
    .locals 11

    .line 156
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/16 v0, 0x14

    .line 61
    invoke-static {v0}, Lcom/epson/cameracopy/device/AptSensorAdapter;->getRangeCompValue(I)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->AUTO_PICTURE_RANGE_VALUE:D

    const/16 v1, 0x2d

    .line 65
    invoke-static {v1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->getRangeCompValue(I)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->PAPER_DETECT_VALUE:D

    .line 68
    new-instance v1, Lcom/epson/cameracopy/device/AptSensorAdapter;

    invoke-direct {v1}, Lcom/epson/cameracopy/device/AptSensorAdapter;-><init>()V

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    .line 75
    new-instance v1, Lorg/opencv/core/Scalar;

    const-wide v3, 0x406fe00000000000L    # 255.0

    const-wide v5, 0x4064600000000000L    # 163.0

    const-wide/16 v7, 0x0

    const-wide v9, 0x406fe00000000000L    # 255.0

    move-object v2, v1

    invoke-direct/range {v2 .. v10}, Lorg/opencv/core/Scalar;-><init>(DDDD)V

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    const/4 v1, 0x1

    .line 76
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColorWidth:I

    .line 78
    new-instance v1, Lorg/opencv/core/Scalar;

    const-wide/16 v5, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v10}, Lorg/opencv/core/Scalar;-><init>(DDDD)V

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPositionCursorColor:Lorg/opencv/core/Scalar;

    .line 87
    new-instance v1, Lorg/opencv/core/Point;

    invoke-direct {v1}, Lorg/opencv/core/Point;-><init>()V

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mScreenSize:Lorg/opencv/core/Point;

    .line 89
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPaperSquareCircleSize:I

    const v0, 0x3d50d67f

    .line 94
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPositionCursorDispRate:F

    const-wide/16 v0, 0xbb8

    .line 104
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mAutoPictureStillTime:J

    const/4 v0, 0x0

    .line 118
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mDisableSettingsButton:Z

    const/4 v0, 0x2

    .line 122
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->INIT_SPAN:I

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)Z
    .locals 0

    .line 52
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    return p0
.end method

.method static synthetic access$002(Lcom/epson/cameracopy/ui/CameraPreviewFragment;Z)Z
    .locals 0

    .line 52
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    return p1
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->startPhotoSelectActivity()V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->disableAllButton()V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/cameracopy/ui/CameraPreviewFragment;Ljava/lang/String;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->startPictureViewActivity(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->enableAllButton()V

    return-void
.end method

.method static synthetic access$500(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)Lcom/epson/cameracopy/device/AptSensorAdapter;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    return-object p0
.end method

.method static synthetic access$600(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->showErrorDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->takePictureStep2()V

    return-void
.end method

.method private autofousTime()Z
    .locals 5

    .line 598
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/AptSensorAdapter;->getLastOverTime2()J

    move-result-wide v0

    .line 599
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v0, 0x1f4

    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private checkAccelerrationTime()Z
    .locals 6

    .line 582
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/AptSensorAdapter;->getLastOverTime2()J

    move-result-wide v0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-gtz v5, :cond_0

    return v2

    .line 588
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    const-wide/16 v0, 0xbb8

    cmp-long v5, v3, v0

    if-ltz v5, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    return v2
.end method

.method private checkAutoPictureAngle()Z
    .locals 3

    .line 569
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    iget-wide v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->AUTO_PICTURE_RANGE_VALUE:D

    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/device/AptSensorAdapter;->isTerminalAngleInRange(D)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private checkPictureSize()V
    .locals 3

    .line 358
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object v0

    .line 359
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getPictureResolutionMode()I

    move-result v1

    if-nez v1, :cond_3

    .line 361
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    if-nez v1, :cond_0

    return-void

    .line 365
    :cond_0
    invoke-virtual {v1}, Lcom/epson/cameracopy/device/OpenCvCameraView;->getCameraPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 368
    iget v2, v1, Landroid/hardware/Camera$Size;->width:I

    iget v1, v1, Landroid/hardware/Camera$Size;->height:I

    mul-int v2, v2, v1

    const v1, 0xf4240

    if-ge v2, v1, :cond_3

    const v1, 0x7f0e02ee

    .line 371
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getCameraPictureSizes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 372
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_1
    const v1, 0x7f0e0391

    .line 377
    :cond_2
    invoke-static {v1}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->newInstance(I)Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mCameraPictureLowResolutionDialog:Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    .line 378
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mCameraPictureLowResolutionDialog:Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "camera-resolution-warning-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method private disableAllButton()V
    .locals 2

    .line 837
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhtographButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    const/4 v0, 0x1

    .line 838
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->disableSettingButton(Z)V

    .line 840
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhotoSelectButton:Landroid/widget/Button;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    return-void
.end method

.method private drawSensorPos(Lorg/opencv/core/Mat;Z)V
    .locals 33

    move-object/from16 v0, p0

    .line 405
    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/Mat;->height()I

    move-result v1

    if-gtz v1, :cond_0

    return-void

    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    .line 410
    iget-object v3, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {v3}, Lcom/epson/cameracopy/device/OpenCvCameraView;->getScale()F

    move-result v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    .line 414
    iget-object v4, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mFunctionBarView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-double v4, v4

    mul-double v4, v4, v2

    double-to-int v4, v4

    .line 417
    iget-object v5, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {v5}, Lcom/epson/cameracopy/device/OpenCvCameraView;->getHeight()I

    move-result v5

    int-to-double v5, v5

    mul-double v5, v5, v2

    double-to-int v5, v5

    int-to-double v6, v1

    add-int/lit8 v8, v4, 0x0

    add-int/2addr v8, v1

    const/4 v9, 0x0

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    if-le v8, v5, :cond_1

    int-to-double v6, v9

    sub-int v1, v5, v1

    int-to-double v12, v1

    div-double/2addr v12, v10

    sub-double/2addr v6, v12

    double-to-int v1, v6

    sub-int/2addr v5, v9

    sub-int/2addr v5, v4

    int-to-double v6, v5

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 441
    :goto_0
    iget-object v4, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    invoke-virtual {v4}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v4

    .line 446
    iget-wide v8, v4, Lorg/opencv/core/Size;->width:D

    div-double/2addr v8, v10

    div-double v10, v6, v10

    .line 448
    iget-wide v12, v4, Lorg/opencv/core/Size;->width:D

    const-wide/high16 v14, 0x4024000000000000L    # 10.0

    div-double/2addr v12, v14

    div-double v14, v6, v14

    move-wide/from16 v16, v2

    int-to-double v1, v1

    move-object/from16 p1, v4

    add-double v3, v1, v14

    .line 452
    new-instance v5, Lorg/opencv/core/Point;

    invoke-direct {v5}, Lorg/opencv/core/Point;-><init>()V

    move-wide/from16 v18, v10

    .line 453
    new-instance v10, Lorg/opencv/core/Point;

    invoke-direct {v10}, Lorg/opencv/core/Point;-><init>()V

    const-wide/high16 v20, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v22, v12

    sub-double v11, v8, v20

    .line 456
    iput-wide v11, v5, Lorg/opencv/core/Point;->x:D

    iput-wide v3, v5, Lorg/opencv/core/Point;->y:D

    .line 457
    iput-wide v11, v10, Lorg/opencv/core/Point;->x:D

    add-double v3, v1, v6

    sub-double v11, v3, v14

    iput-wide v11, v10, Lorg/opencv/core/Point;->y:D

    .line 458
    iget-object v11, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v12, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    const/4 v13, 0x1

    invoke-static {v11, v5, v10, v12, v13}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    move-wide/from16 v11, v22

    .line 461
    iput-wide v11, v5, Lorg/opencv/core/Point;->x:D

    add-double v14, v1, v18

    iput-wide v14, v5, Lorg/opencv/core/Point;->y:D

    move-object/from16 v13, p1

    move-wide/from16 v21, v8

    .line 462
    iget-wide v8, v13, Lorg/opencv/core/Size;->width:D

    sub-double/2addr v8, v11

    iput-wide v8, v10, Lorg/opencv/core/Point;->x:D

    iput-wide v14, v10, Lorg/opencv/core/Point;->y:D

    .line 463
    iget-object v8, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v9, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    const/4 v11, 0x1

    invoke-static {v8, v5, v10, v9, v11}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 466
    iget-wide v8, v13, Lorg/opencv/core/Size;->width:D

    const-wide/high16 v11, 0x4034000000000000L    # 20.0

    div-double/2addr v8, v11

    div-double/2addr v6, v11

    add-double v14, v6, v1

    const-wide/high16 v23, 0x4008000000000000L    # 3.0

    mul-double v11, v8, v23

    move-wide/from16 v25, v14

    .line 471
    iget-wide v14, v13, Lorg/opencv/core/Size;->width:D

    sub-double/2addr v14, v11

    move-wide/from16 v27, v14

    .line 472
    iget-wide v13, v13, Lorg/opencv/core/Size;->width:D

    sub-double/2addr v13, v8

    mul-double v23, v23, v6

    move-wide/from16 v29, v13

    add-double v13, v1, v23

    move-wide/from16 v31, v1

    sub-double v1, v3, v23

    sub-double/2addr v3, v6

    .line 478
    iput-wide v8, v5, Lorg/opencv/core/Point;->x:D

    move-wide/from16 v6, v25

    iput-wide v6, v5, Lorg/opencv/core/Point;->y:D

    .line 479
    iput-wide v11, v10, Lorg/opencv/core/Point;->x:D

    iput-wide v6, v10, Lorg/opencv/core/Point;->y:D

    .line 480
    iget-object v15, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    move-wide/from16 v23, v1

    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    const/4 v2, 0x2

    invoke-static {v15, v5, v10, v1, v2}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    move-wide/from16 v25, v3

    move-wide/from16 v2, v27

    .line 481
    iput-wide v2, v5, Lorg/opencv/core/Point;->x:D

    iput-wide v6, v5, Lorg/opencv/core/Point;->y:D

    move-wide/from16 v27, v13

    move-wide/from16 v13, v29

    .line 482
    iput-wide v13, v10, Lorg/opencv/core/Point;->x:D

    iput-wide v6, v10, Lorg/opencv/core/Point;->y:D

    .line 483
    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v4, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    const/4 v15, 0x2

    invoke-static {v1, v5, v10, v4, v15}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 486
    iput-wide v8, v5, Lorg/opencv/core/Point;->x:D

    move-wide/from16 v29, v6

    move-wide/from16 v6, v25

    iput-wide v6, v5, Lorg/opencv/core/Point;->y:D

    .line 487
    iput-wide v11, v10, Lorg/opencv/core/Point;->x:D

    iput-wide v6, v10, Lorg/opencv/core/Point;->y:D

    .line 488
    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v4, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    invoke-static {v1, v5, v10, v4, v15}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 489
    iput-wide v2, v5, Lorg/opencv/core/Point;->x:D

    iput-wide v6, v5, Lorg/opencv/core/Point;->y:D

    .line 490
    iput-wide v13, v10, Lorg/opencv/core/Point;->x:D

    iput-wide v6, v10, Lorg/opencv/core/Point;->y:D

    .line 491
    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v2, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    invoke-static {v1, v5, v10, v2, v15}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 494
    iput-wide v8, v5, Lorg/opencv/core/Point;->x:D

    move-wide/from16 v1, v29

    iput-wide v1, v5, Lorg/opencv/core/Point;->y:D

    .line 495
    iput-wide v8, v10, Lorg/opencv/core/Point;->x:D

    move-wide/from16 v3, v27

    iput-wide v3, v10, Lorg/opencv/core/Point;->y:D

    .line 496
    iget-object v11, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v12, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    invoke-static {v11, v5, v10, v12, v15}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 497
    iput-wide v8, v5, Lorg/opencv/core/Point;->x:D

    move-wide/from16 v11, v23

    iput-wide v11, v5, Lorg/opencv/core/Point;->y:D

    .line 498
    iput-wide v8, v10, Lorg/opencv/core/Point;->x:D

    iput-wide v6, v10, Lorg/opencv/core/Point;->y:D

    .line 499
    iget-object v8, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v9, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    invoke-static {v8, v5, v10, v9, v15}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 502
    iput-wide v13, v5, Lorg/opencv/core/Point;->x:D

    iput-wide v1, v5, Lorg/opencv/core/Point;->y:D

    .line 503
    iput-wide v13, v10, Lorg/opencv/core/Point;->x:D

    iput-wide v3, v10, Lorg/opencv/core/Point;->y:D

    .line 504
    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v2, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    invoke-static {v1, v5, v10, v2, v15}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 505
    iput-wide v13, v5, Lorg/opencv/core/Point;->x:D

    iput-wide v11, v5, Lorg/opencv/core/Point;->y:D

    .line 506
    iput-wide v13, v10, Lorg/opencv/core/Point;->x:D

    iput-wide v6, v10, Lorg/opencv/core/Point;->y:D

    .line 507
    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    iget-object v2, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mGridColor:Lorg/opencv/core/Scalar;

    invoke-static {v1, v5, v10, v2, v15}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    if-eqz p2, :cond_2

    .line 511
    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    invoke-virtual {v1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->getPosX()F

    move-result v1

    const v2, 0x3d50d67f

    mul-float v1, v1, v2

    float-to-double v3, v1

    mul-double v3, v3, v21

    sub-double v8, v21, v3

    .line 513
    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    invoke-virtual {v1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->getPosY()F

    move-result v1

    mul-float v1, v1, v2

    float-to-double v1, v1

    mul-double v1, v1, v18

    add-double v10, v18, v1

    add-double v10, v10, v31

    const-wide/high16 v1, 0x4034000000000000L    # 20.0

    mul-double v2, v16, v1

    .line 517
    iget v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mDisplayDensity:F

    float-to-double v4, v1

    mul-double v2, v2, v4

    double-to-int v1, v2

    .line 518
    iget-object v2, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    new-instance v3, Lorg/opencv/core/Point;

    invoke-direct {v3, v8, v9, v10, v11}, Lorg/opencv/core/Point;-><init>(DD)V

    iget-object v4, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPositionCursorColor:Lorg/opencv/core/Scalar;

    const/4 v5, 0x1

    invoke-static {v2, v3, v1, v4, v5}, Lorg/opencv/imgproc/Imgproc;->circle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;ILorg/opencv/core/Scalar;I)V

    .line 520
    iget-object v2, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    new-instance v3, Lorg/opencv/core/Point;

    int-to-double v4, v1

    sub-double v6, v10, v4

    invoke-direct {v3, v8, v9, v6, v7}, Lorg/opencv/core/Point;-><init>(DD)V

    new-instance v1, Lorg/opencv/core/Point;

    add-double v6, v10, v4

    invoke-direct {v1, v8, v9, v6, v7}, Lorg/opencv/core/Point;-><init>(DD)V

    iget-object v6, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPositionCursorColor:Lorg/opencv/core/Scalar;

    invoke-static {v2, v3, v1, v6}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)V

    .line 522
    iget-object v1, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    new-instance v2, Lorg/opencv/core/Point;

    sub-double v6, v8, v4

    invoke-direct {v2, v6, v7, v10, v11}, Lorg/opencv/core/Point;-><init>(DD)V

    new-instance v3, Lorg/opencv/core/Point;

    add-double/2addr v8, v4

    invoke-direct {v3, v8, v9, v10, v11}, Lorg/opencv/core/Point;-><init>(DD)V

    iget-object v4, v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPositionCursorColor:Lorg/opencv/core/Scalar;

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)V

    :cond_2
    return-void
.end method

.method private enableAllButton()V
    .locals 2

    .line 827
    invoke-static {}, Lcom/epson/cameracopy/device/CameraPreviewControl;->hasBackCamera()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 828
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhtographButton:Landroid/widget/Button;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    const/4 v0, 0x0

    .line 829
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->disableSettingButton(Z)V

    .line 832
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhotoSelectButton:Landroid/widget/Button;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    return-void
.end method

.method private myDrawRect(Lorg/opencv/core/Mat;[Lorg/opencv/core/Point;Z)V
    .locals 5

    if-eqz p2, :cond_5

    .line 537
    array-length v0, p2

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    goto :goto_0

    .line 540
    :cond_0
    array-length v0, p2

    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    return-void

    .line 543
    :cond_1
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->PAPER_SQUARE_COLOR:[Lorg/opencv/core/Scalar;

    const/4 v3, 0x0

    aget-object v4, v0, v3

    if-eqz p3, :cond_2

    .line 545
    aget-object v4, v0, v1

    .line 548
    :cond_2
    aget-object p3, p2, v3

    aget-object v0, p2, v1

    invoke-static {p1, p3, v0, v4, v2}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 552
    array-length p3, p2

    const/4 v0, 0x3

    if-ge p3, v0, :cond_3

    return-void

    .line 555
    :cond_3
    aget-object p3, p2, v1

    aget-object v1, p2, v2

    invoke-static {p1, p3, v1, v4, v2}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 558
    array-length p3, p2

    const/4 v1, 0x4

    if-ge p3, v1, :cond_4

    return-void

    .line 561
    :cond_4
    aget-object p3, p2, v3

    aget-object v1, p2, v0

    invoke-static {p1, p3, v1, v4, v2}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 562
    aget-object p3, p2, v2

    aget-object p2, p2, v0

    invoke-static {p1, p3, p2, v4, v2}, Lorg/opencv/imgproc/Imgproc;->line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    return-void

    :cond_5
    :goto_0
    return-void
.end method

.method private savePictureSizeList()V
    .locals 4

    .line 327
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    if-nez v0, :cond_0

    return-void

    .line 331
    :cond_0
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->getPictureSizeList()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 337
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 338
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 339
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    .line 340
    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    mul-int v3, v3, v2

    const v2, 0xf4240

    if-ge v3, v2, :cond_2

    .line 342
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 346
    :cond_3
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object v1

    .line 347
    invoke-virtual {v1, v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setCameraPictureSizes(Ljava/util/List;)V

    return-void
.end method

.method private sensorAndCamera()V
    .locals 7

    .line 610
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->isFragmentVisible:Z

    if-nez v0, :cond_0

    return-void

    .line 614
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 615
    iget-boolean v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mAutoPhotoMode:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    iget-wide v3, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->PAPER_DETECT_VALUE:D

    .line 616
    invoke-virtual {v2, v3, v4}, Lcom/epson/cameracopy/device/AptSensorAdapter;->isTerminalAngleInRange(D)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 617
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->checkAutoPictureAngle()Z

    move-result v2

    .line 618
    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v3

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mFunctionBarView:Landroid/view/View;

    .line 620
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    .line 618
    invoke-static {v3, v4, v5, v6, v2}, Lcom/epson/cameracopy/device/RectangleDetector;->detectAndDrawRectangle2(JIIZ)I

    move-result v3

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    if-ne v3, v2, :cond_1

    .line 623
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->checkAccelerrationTime()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 625
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->takePicture()V

    return-void

    :cond_1
    if-eqz v3, :cond_2

    .line 630
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPAfTime:J

    .line 634
    :cond_2
    iget-boolean v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    if-eqz v2, :cond_3

    return-void

    .line 639
    :cond_3
    iget-boolean v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mAutoPhotoMode:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPAfTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0xfa0

    cmp-long v6, v2, v4

    if-lez v6, :cond_4

    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    .line 640
    invoke-virtual {v2}, Lcom/epson/cameracopy/device/OpenCvCameraView;->isInAutofocus()Z

    move-result v2

    if-nez v2, :cond_4

    .line 642
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->autofousTime()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 644
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {v2, p0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->doAutofocus(Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;)V

    .line 645
    iput-wide v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPAfTime:J

    :cond_4
    return-void
.end method

.method private setButtonListener(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f08025c

    .line 240
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhotoSelectButton:Landroid/widget/Button;

    .line 241
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhotoSelectButton:Landroid/widget/Button;

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPreviewFragment$1;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$1;-><init>(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08025e

    .line 265
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhtographButton:Landroid/widget/Button;

    .line 266
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhtographButton:Landroid/widget/Button;

    new-instance v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment$2;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$2;-><init>(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private showErrorDialog()V
    .locals 3

    const v0, 0x7f0e0372

    .line 780
    invoke-static {v0}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->newInstance(I)Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    move-result-object v0

    .line 781
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "error-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startInformationActivity()V
    .locals 0

    return-void
.end method

.method private startPhotoSelectActivity()V
    .locals 3

    .line 309
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lepson/print/imgsel/CameraCopyImageSelectActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startPictureViewActivity(Ljava/lang/String;)V
    .locals 3

    .line 850
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/epson/cameracopy/ui/ImagePreviewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 851
    new-instance v1, Lcom/epson/cameracopy/device/CameraFile;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-direct {v1, v2, p1}, Lcom/epson/cameracopy/device/CameraFile;-><init>(Ljava/util/Date;Ljava/lang/String;)V

    const-string p1, "camera-file"

    .line 852
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 856
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->startActivity(Landroid/content/Intent;)V

    const/4 p1, 0x1

    .line 857
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPreviewActivityStarted:Z

    return-void
.end method

.method private startSettingActivity()V
    .locals 3

    .line 303
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private takePictureStep2()V
    .locals 3

    .line 805
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/cameracopy/alt/FileUtils;->getNewTemporaryJpegFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 807
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/epson/cameracopy/ui/CameraPreviewFragment$4;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$4;-><init>(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/epson/cameracopy/device/OpenCvCameraView;->takePicture(Ljava/lang/String;Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;)V

    return-void
.end method


# virtual methods
.method disableSettingButton(Z)V
    .locals 0

    .line 872
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mDisableSettingsButton:Z

    .line 873
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public isPreviewActivityStarted()Z
    .locals 1

    .line 177
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPreviewActivityStarted:Z

    return v0
.end method

.method public onAutoFocusCompleted(Z)V
    .locals 2

    .line 863
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPAfTime:J

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 910
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onCameraFrame(Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewFrame;)Lorg/opencv/core/Mat;
    .locals 4

    .line 654
    invoke-interface {p1}, Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewFrame;->rgba()Lorg/opencv/core/Mat;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    .line 656
    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mHasCamera:Z

    if-eqz p1, :cond_3

    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    if-eqz p1, :cond_0

    goto :goto_0

    .line 660
    :cond_0
    iget p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mInitCount:I

    const/4 v0, 0x2

    if-ge p1, v0, :cond_1

    add-int/lit8 p1, p1, 0x1

    .line 661
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mInitCount:I

    .line 662
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mScreenSize:Lorg/opencv/core/Point;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->width()I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p1, Lorg/opencv/core/Point;->x:D

    .line 663
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mScreenSize:Lorg/opencv/core/Point;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->height()I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p1, Lorg/opencv/core/Point;->y:D

    .line 664
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    sget-object v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->POSITION_0_0:Lorg/opencv/core/Point;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mScreenSize:Lorg/opencv/core/Point;

    sget-object v2, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->COLOR_BLACK:Lorg/opencv/core/Scalar;

    const/4 v3, -0x1

    invoke-static {p1, v0, v1, v2, v3}, Lorg/opencv/imgproc/Imgproc;->rectangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V

    .line 668
    :cond_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->sensorAndCamera()V

    .line 671
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {p1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getGuideMode()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 673
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    iget-wide v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->PAPER_DETECT_VALUE:D

    .line 674
    invoke-virtual {p1, v0, v1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->isTerminalAngleInRange(D)Z

    move-result p1

    .line 675
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    invoke-direct {p0, v0, p1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->drawSensorPos(Lorg/opencv/core/Mat;Z)V

    .line 678
    :cond_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    return-object p1

    .line 657
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    return-object p1
.end method

.method public onCameraViewStarted(II)V
    .locals 2

    .line 314
    new-instance v0, Lorg/opencv/core/Mat;

    sget v1, Lorg/opencv/core/CvType;->CV_8UC4:I

    invoke-direct {v0, p1, p2, v1}, Lorg/opencv/core/Mat;-><init>(III)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    .line 316
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->savePictureSizeList()V

    .line 317
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->checkPictureSize()V

    return-void
.end method

.method public onCameraViewStopped()V
    .locals 1

    .line 388
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mRgba:Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 391
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mCameraPictureLowResolutionDialog:Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    if-eqz v0, :cond_0

    .line 392
    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->dismiss()V

    const/4 v0, 0x0

    .line 393
    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mCameraPictureLowResolutionDialog:Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 684
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 685
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    const/4 p1, 0x0

    .line 686
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .line 880
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mDisableSettingsButton:Z

    if-nez v0, :cond_0

    const/high16 v0, 0x7f0b0000

    .line 881
    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 884
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    const v1, 0x7f0a0067

    .line 184
    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x1

    .line 188
    invoke-virtual {p0, p2}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->setHasOptionsMenu(Z)V

    .line 190
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->setButtonListener(Landroid/view/View;)V

    const v1, 0x7f080385

    .line 191
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/epson/cameracopy/device/OpenCvCameraView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    .line 192
    invoke-static {}, Lcom/epson/cameracopy/device/CameraPreviewControl;->hasBackCamera()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    iput-boolean p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mHasCamera:Z

    .line 194
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {p2, p0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->setCvCameraViewListener(Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewListener2;)V

    goto :goto_0

    .line 196
    :cond_0
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mHasCamera:Z

    .line 199
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhtographButton:Landroid/widget/Button;

    invoke-static {p2, v0}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    if-nez p3, :cond_1

    .line 204
    iget-boolean p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->noCameraDialog:Z

    if-eqz p2, :cond_1

    .line 206
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->noCameraDialog:Z

    const p2, 0x7f0e02b1

    .line 208
    invoke-static {p2}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->newInstance(I)Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    move-result-object p2

    .line 209
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p3

    const-string v1, "error-dialog"

    invoke-virtual {p2, p3, v1}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 213
    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {p2, v0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->setVisibility(I)V

    .line 217
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    const-string p3, "window"

    invoke-virtual {p2, p3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/WindowManager;

    .line 218
    invoke-interface {p2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/Display;->getRotation()I

    move-result p2

    iput p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mDisplayRotation:I

    .line 219
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    iget p3, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mDisplayRotation:I

    invoke-virtual {p2, p3}, Lcom/epson/cameracopy/device/AptSensorAdapter;->setDisplayRotation(I)V

    .line 221
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    .line 222
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p3

    const-string v0, "sensor"

    invoke-virtual {p3, v0}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/hardware/SensorManager;

    .line 221
    invoke-virtual {p2, p3}, Lcom/epson/cameracopy/device/AptSensorAdapter;->setSensorManager(Landroid/hardware/SensorManager;)V

    .line 223
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    const-wide v0, 0x3fdccccccccccccdL    # 0.45

    invoke-virtual {p2, v0, v1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->setReferenceAccelaration(D)V

    const p2, 0x7f08015b

    .line 226
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mFunctionBarView:Landroid/view/View;

    .line 228
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    .line 229
    iget p2, p2, Landroid/util/DisplayMetrics;->density:F

    iput p2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mDisplayDensity:F

    return-object p1
.end method

.method public onDestroy()V
    .locals 3

    .line 768
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    if-eqz v0, :cond_0

    .line 769
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->cancelAutofocus()V

    .line 770
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->disableView()V

    .line 771
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    const/4 v1, 0x0

    move-object v2, v1

    check-cast v2, Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewListener2;

    invoke-virtual {v0, v2}, Lcom/epson/cameracopy/device/OpenCvCameraView;->setCvCameraViewListener(Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewListener2;)V

    .line 772
    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    .line 775
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 890
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0800af

    if-eq v0, v1, :cond_0

    .line 900
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 892
    :cond_0
    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    return p1

    .line 896
    :cond_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->startSettingActivity()V

    const/4 p1, 0x1

    return p1
.end method

.method public onPause()V
    .locals 2

    const/4 v0, 0x0

    .line 749
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->isFragmentVisible:Z

    .line 751
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    invoke-virtual {v1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->stop()V

    .line 752
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mTakePictureTask:Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;

    if-eqz v1, :cond_0

    .line 753
    invoke-virtual {v1, v0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->cancel(Z)Z

    const/4 v1, 0x0

    .line 754
    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mTakePictureTask:Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;

    .line 756
    :cond_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    if-eqz v1, :cond_1

    .line 757
    invoke-virtual {v1}, Lcom/epson/cameracopy/device/OpenCvCameraView;->cancelAutofocus()V

    .line 758
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {v1}, Lcom/epson/cameracopy/device/OpenCvCameraView;->disableView()V

    .line 760
    :cond_1
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    .line 762
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 5

    .line 695
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 697
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mHasCamera:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 699
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->enableAllButton()V

    .line 704
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->checkBackCamera()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->enableView()V

    .line 708
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/AptSensorAdapter;->start()V

    const-wide/16 v3, 0x0

    .line 709
    iput-wide v3, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPAfTime:J

    goto :goto_0

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPhtographButton:Landroid/widget/Button;

    invoke-static {v0, v2}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    .line 719
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->disableSettingButton(Z)V

    const v0, 0x7f0e02b1

    .line 722
    invoke-static {v0}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->newInstance(I)Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    move-result-object v0

    .line 723
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "error-dialog"

    invoke-virtual {v0, v3, v4}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 727
    :goto_0
    iput-boolean v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    .line 729
    :cond_1
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mInitCount:I

    .line 731
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPreviewActivityStarted:Z

    if-eqz v0, :cond_2

    .line 733
    iput-boolean v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPreviewActivityStarted:Z

    .line 735
    iput-boolean v2, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    .line 736
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->enableAllButton()V

    .line 740
    :cond_2
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getAutoPictureMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mAutoPhotoMode:Z

    .line 742
    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->isFragmentVisible:Z

    return-void
.end method

.method public setAppInitFlag(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 167
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->noCameraDialog:Z

    :cond_0
    return-void
.end method

.method public takePicture()V
    .locals 2

    .line 785
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 788
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mPictureTaking:Z

    .line 789
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPreviewFragment$3;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$3;-><init>(Lcom/epson/cameracopy/ui/CameraPreviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 798
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mSensorEventListener:Lcom/epson/cameracopy/device/AptSensorAdapter;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/AptSensorAdapter;->stop()V

    .line 800
    new-instance v0, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mOpenCvCameraView:Lcom/epson/cameracopy/device/OpenCvCameraView;

    invoke-direct {v0, p0, v1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;-><init>(Lcom/epson/cameracopy/ui/CameraPreviewFragment;Lcom/epson/cameracopy/device/OpenCvCameraView;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mTakePictureTask:Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;

    .line 801
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPreviewFragment;->mTakePictureTask:Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPreviewFragment$TakePictureTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.class Lcom/epson/cameracopy/ui/CropImageActivity$9;
.super Ljava/lang/Object;
.source "CropImageActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;->executeMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V
    .locals 0

    .line 1118
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$9;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 1136
    :pswitch_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$9;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const-wide/high16 v0, 0x405e000000000000L    # 120.0

    invoke-virtual {p1, v0, v1}, Lepson/colorcorrection/ImageCollect;->SetResolutionThreshold(D)V

    goto :goto_0

    .line 1133
    :pswitch_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$9;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const-wide v0, 0x4066800000000000L    # 180.0

    invoke-virtual {p1, v0, v1}, Lepson/colorcorrection/ImageCollect;->SetResolutionThreshold(D)V

    goto :goto_0

    .line 1130
    :pswitch_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$9;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const-wide/high16 v0, 0x406e000000000000L    # 240.0

    invoke-virtual {p1, v0, v1}, Lepson/colorcorrection/ImageCollect;->SetResolutionThreshold(D)V

    goto :goto_0

    .line 1127
    :pswitch_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$9;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const-wide v0, 0x4072c00000000000L    # 300.0

    invoke-virtual {p1, v0, v1}, Lepson/colorcorrection/ImageCollect;->SetResolutionThreshold(D)V

    goto :goto_0

    .line 1124
    :pswitch_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$9;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const-wide v0, 0x4076800000000000L    # 360.0

    invoke-virtual {p1, v0, v1}, Lepson/colorcorrection/ImageCollect;->SetResolutionThreshold(D)V

    .line 1139
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$9;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$1202(Lcom/epson/cameracopy/ui/CropImageActivity;I)I

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

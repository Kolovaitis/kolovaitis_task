.class Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;
.super Landroid/os/AsyncTask;
.source "CameraPrintSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V
    .locals 0

    .line 930
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 930
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 942
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const-string v0, "printer"

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 944
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnectSimpleAP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 930
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3

    .line 956
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const-class v1, Lepson/print/screen/SearchPrinterScr;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 959
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "PRINTER_ID"

    .line 960
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$200(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PRINTER_IP"

    .line 961
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$000(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PRINTER_EMAIL_ADDRESS"

    .line 962
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$300(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PRINTER_LOCATION"

    .line 963
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$400(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "simpleap"

    .line 964
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$100(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 967
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 971
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v0, 0x7f08027e

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 935
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const v1, 0x7f08027e

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.class Lcom/epson/cameracopy/ui/CameraPrintProgress$8;
.super Ljava/lang/Object;
.source "CameraPrintProgress.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CameraPrintProgress;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 851
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$8;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 p1, 0x0

    .line 856
    :try_start_0
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-interface {p2, p1}, Lepson/print/service/IEpsonService;->confirmContinueable(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 858
    invoke-virtual {p2}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_0
    const-string p2, "Epson"

    const-string v0, "user click str_cancel button"

    .line 861
    invoke-static {p2, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    sput p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    .line 864
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$8;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p2, p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$602(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 865
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$8;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p2, p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->removeDialog(I)V

    .line 866
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$8;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

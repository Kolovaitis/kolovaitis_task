.class Lcom/epson/cameracopy/ui/MenuControl;
.super Ljava/lang/Object;
.source "FolderSelectActivity.java"


# static fields
.field static final MENU_HIDE:I = 0x0

.field static final MENU_NEW:I = 0x2

.field static final MENU_OK:I = 0x1

.field static final MENU_OK_CANCEL:I = 0x4

.field static final MENU_OK_NEW:I = 0x3


# instance fields
.field private final mActivity:Lcom/epson/cameracopy/ui/FolderSelectActivity;

.field private final mMenuInflater:Landroid/view/MenuInflater;

.field private mMenuMode:I


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/FolderSelectActivity;I)V
    .locals 1

    .line 358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 353
    iput v0, p0, Lcom/epson/cameracopy/ui/MenuControl;->mMenuMode:I

    .line 359
    iput-object p1, p0, Lcom/epson/cameracopy/ui/MenuControl;->mActivity:Lcom/epson/cameracopy/ui/FolderSelectActivity;

    .line 361
    iget-object p1, p0, Lcom/epson/cameracopy/ui/MenuControl;->mActivity:Lcom/epson/cameracopy/ui/FolderSelectActivity;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/MenuControl;->mMenuInflater:Landroid/view/MenuInflater;

    .line 362
    iput p2, p0, Lcom/epson/cameracopy/ui/MenuControl;->mMenuMode:I

    return-void
.end method


# virtual methods
.method public ChangeMenu(I)V
    .locals 1

    .line 375
    iget v0, p0, Lcom/epson/cameracopy/ui/MenuControl;->mMenuMode:I

    if-eq v0, p1, :cond_0

    .line 376
    iput p1, p0, Lcom/epson/cameracopy/ui/MenuControl;->mMenuMode:I

    .line 377
    iget-object p1, p0, Lcom/epson/cameracopy/ui/MenuControl;->mActivity:Lcom/epson/cameracopy/ui/FolderSelectActivity;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/FolderSelectActivity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method public CreateMenu(Landroid/view/Menu;)V
    .locals 2

    .line 366
    iget v0, p0, Lcom/epson/cameracopy/ui/MenuControl;->mMenuMode:I

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/epson/cameracopy/ui/MenuControl;->mMenuInflater:Landroid/view/MenuInflater;

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :cond_0
    return-void
.end method

.class public Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;
.super Landroid/arch/lifecycle/LiveData;
.source "ImageFolderFileViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageFolderFileLiveData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/arch/lifecycle/LiveData<",
        "Lcom/epson/cameracopy/ui/ImageFolderFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;->this$0:Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;

    invoke-direct {p0}, Landroid/arch/lifecycle/LiveData;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;Ljava/lang/Object;)V
    .locals 0

    .line 27
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public setImageFolderFile(Lcom/epson/cameracopy/ui/ImageFolderFile;)V
    .locals 2
    .param p1    # Lcom/epson/cameracopy/ui/ImageFolderFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 29
    new-instance v0, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;

    invoke-direct {v0, p0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;-><init>(Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;Lcom/epson/cameracopy/ui/ImageFolderFile;)V

    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 45
    invoke-virtual {v0, p1, v1}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

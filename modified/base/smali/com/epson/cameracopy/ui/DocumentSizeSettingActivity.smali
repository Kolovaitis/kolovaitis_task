.class public Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;
.super Lepson/print/ActivityIACommon;
.source "DocumentSizeSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;,
        Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;
    }
.end annotation


# instance fields
.field private ID_DIALOG_DELETE:I

.field private ID_DIALOG_RESET:I

.field private mAdd:Landroid/widget/Button;

.field private mClear:Landroid/widget/Button;

.field private mDelete:Landroid/widget/Button;

.field private mEdit:Landroid/widget/Button;

.field private mListView:Landroid/widget/ListView;

.field private mSelect:Landroid/widget/Button;

.field mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

.field private mSelectPos:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x1

    .line 45
    iput v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->ID_DIALOG_DELETE:I

    const/4 v0, 0x2

    .line 46
    iput v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->ID_DIALOG_RESET:I

    return-void
.end method

.method private addNewDocumentSize()V
    .locals 6

    .line 146
    new-instance v0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    invoke-direct {v0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;-><init>()V

    .line 149
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 150
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, -0x2

    const v5, 0x7f0e03da

    if-nez v2, :cond_1

    sget-object v2, Ljava/util/Locale;->CANADA:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 161
    :cond_0
    invoke-virtual {p0, v5}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setDocSizeName(Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 162
    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setScaleId(I)V

    const-wide v1, 0x406a400000000000L    # 210.0

    .line 163
    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setWidth(D)V

    const-wide v1, 0x4072900000000000L    # 297.0

    .line 164
    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setHeight(D)V

    .line 165
    invoke-virtual {v0, v4}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setPaperId(I)V

    .line 166
    invoke-virtual {v0, v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setDocSizeNameId(I)V

    goto :goto_1

    .line 151
    :cond_1
    :goto_0
    invoke-virtual {p0, v5}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setDocSizeName(Ljava/lang/String;)V

    const/4 v1, 0x2

    .line 152
    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setScaleId(I)V

    const-wide/high16 v1, 0x4021000000000000L    # 8.5

    .line 153
    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setWidth(D)V

    const-wide/high16 v1, 0x4026000000000000L    # 11.0

    .line 154
    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setHeight(D)V

    .line 155
    invoke-virtual {v0, v4}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setPaperId(I)V

    .line 156
    invoke-virtual {v0, v3}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->setDocSizeNameId(I)V

    .line 170
    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "DocumentSize"

    .line 171
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "DocumentSizePos"

    const/4 v2, -0x1

    .line 172
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 173
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private deleteDocumentSize()V
    .locals 3

    .line 194
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "message"

    const v2, 0x7f0e0315

    .line 195
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "id"

    .line 196
    iget v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->ID_DIALOG_DELETE:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 199
    new-instance v1, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;

    invoke-direct {v1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;-><init>()V

    .line 200
    invoke-virtual {v1, v0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 201
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "notify-dialog"

    invoke-virtual {v1, v0, v2}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private editDocumentSize(I)V
    .locals 3

    .line 185
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 186
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/epson/cameracopy/ui/DocumentSizeEditActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "DocumentSize"

    .line 187
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "DocumentSizePos"

    .line 188
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 189
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private resetDocumentSizeList()V
    .locals 3

    .line 206
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "message"

    const v2, 0x7f0e044b

    .line 207
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "id"

    .line 208
    iget v2, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->ID_DIALOG_RESET:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 211
    new-instance v1, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;

    invoke-direct {v1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;-><init>()V

    .line 212
    invoke-virtual {v1, v0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 213
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "notify-dialog"

    invoke-virtual {v1, v0, v2}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private selectDocumentSize(I)V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->select(I)V

    .line 179
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->finish()V

    return-void
.end method


# virtual methods
.method public doNegativeClick(I)V
    .locals 0

    return-void
.end method

.method public doPositiveClick(I)V
    .locals 2

    .line 334
    iget v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->ID_DIALOG_RESET:I

    if-ne p1, v0, :cond_0

    .line 335
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->reset()V

    goto :goto_0

    .line 339
    :cond_0
    iget v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->ID_DIALOG_DELETE:I

    if-ne p1, v0, :cond_1

    .line 340
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    iget v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectPos:I

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    .line 341
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    iget v1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectPos:I

    invoke-virtual {v0, p1, v1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->delete(Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;I)V

    .line 342
    iget p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectPos:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectPos:I

    :cond_1
    :goto_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 96
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 98
    :sswitch_0
    iget p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectPos:I

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->selectDocumentSize(I)V

    goto :goto_0

    .line 101
    :sswitch_1
    iget p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectPos:I

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->editDocumentSize(I)V

    goto :goto_0

    .line 104
    :sswitch_2
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->deleteDocumentSize()V

    goto :goto_0

    .line 110
    :sswitch_3
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->resetDocumentSizeList()V

    goto :goto_0

    .line 107
    :sswitch_4
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->addNewDocumentSize()V

    :goto_0
    const p1, 0x7f080115

    .line 113
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080057 -> :sswitch_4
        0x7f0800c2 -> :sswitch_3
        0x7f080103 -> :sswitch_2
        0x7f080121 -> :sswitch_1
        0x7f0802f0 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 50
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0028

    .line 51
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->setContentView(I)V

    const p1, 0x7f0e0320

    const/4 v0, 0x1

    .line 54
    invoke-virtual {p0, p1, v0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->setActionBar(IZ)V

    const p1, 0x7f080111

    .line 57
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mListView:Landroid/widget/ListView;

    .line 58
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mListView:Landroid/widget/ListView;

    new-instance v0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$1;-><init>(Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 64
    new-instance p1, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    invoke-direct {p1, p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;-><init>(Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    .line 65
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const p1, 0x7f0802f0

    .line 68
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelect:Landroid/widget/Button;

    .line 69
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelect:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080121

    .line 72
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mEdit:Landroid/widget/Button;

    .line 73
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mEdit:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080103

    .line 76
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mDelete:Landroid/widget/Button;

    .line 77
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mDelete:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080057

    .line 80
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mAdd:Landroid/widget/Button;

    .line 81
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mAdd:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0800c2

    .line 84
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mClear:Landroid/widget/Button;

    .line 85
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mClear:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onSelectDocsizeList(Ljava/lang/Object;Landroid/view/View;I)V
    .locals 3

    .line 119
    iput p3, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectPos:I

    const p2, 0x7f080115

    .line 125
    invoke-virtual {p0, p2}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p3

    invoke-virtual {p3}, Landroid/view/View;->getVisibility()I

    move-result p3

    if-eqz p3, :cond_2

    .line 126
    check-cast p1, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;

    const p3, 0x7f080217

    .line 127
    invoke-virtual {p0, p3}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    invoke-virtual {p1, p0}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getDocSizeName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result p3

    const/4 v0, -0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p3, v0, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 130
    :goto_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mEdit:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 131
    invoke-virtual {p1}, Lcom/epson/cameracopy/printlayout/DocumentSizeInfo;->getPaperId()I

    move-result p1

    const/4 p3, -0x1

    if-ne p1, p3, :cond_1

    const/4 v1, 0x0

    .line 132
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mDelete:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 133
    invoke-virtual {p0, p2}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 138
    :cond_2
    invoke-virtual {p0, p2}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 90
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStart()V

    .line 91
    iget-object v0, p0, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;->mSelectListAdapter:Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$SelectListAdapter;->reload()V

    return-void
.end method

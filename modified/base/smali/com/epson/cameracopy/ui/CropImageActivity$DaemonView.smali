.class Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;
.super Landroid/view/View;
.source "CropImageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DaemonView"
.end annotation


# instance fields
.field private mHandlerAvtivity:Landroid/os/Handler;

.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;Landroid/content/Context;)V
    .locals 0

    .line 1907
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 1908
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 1905
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->mHandlerAvtivity:Landroid/os/Handler;

    .line 1909
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->Init()V

    return-void
.end method

.method public constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1912
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 1913
    invoke-direct {p0, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 1905
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->mHandlerAvtivity:Landroid/os/Handler;

    .line 1914
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->Init()V

    return-void
.end method

.method public constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 1917
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    .line 1918
    invoke-direct {p0, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 1905
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->mHandlerAvtivity:Landroid/os/Handler;

    .line 1919
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->Init()V

    return-void
.end method

.method private Init()V
    .locals 2

    const/4 v0, 0x4

    .line 1928
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->setVisibility(I)V

    const/4 v0, 0x0

    .line 1930
    invoke-static {v0, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->setBackgroundColor(I)V

    .line 1931
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public SetHandler(Landroid/os/Handler;)V
    .locals 0

    .line 1923
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->mHandlerAvtivity:Landroid/os/Handler;

    const/4 p1, 0x0

    .line 1924
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->setVisibility(I)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1938
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    and-int/lit16 p1, p1, 0xff

    if-eqz p1, :cond_0

    goto :goto_0

    .line 1942
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->mHandlerAvtivity:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const/16 v0, 0x137

    .line 1943
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 p1, 0x0

    .line 1944
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->mHandlerAvtivity:Landroid/os/Handler;

    const/4 p1, 0x4

    .line 1945
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity$DaemonView;->setVisibility(I)V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

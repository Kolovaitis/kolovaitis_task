.class public Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;
.super Ljava/lang/Object;
.source "CropImageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CorrectionParamSet"
.end annotation


# instance fields
.field private mAfterPWCtype:I

.field private mImageType:I

.field private mParamArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V
    .locals 0

    .line 1974
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1975
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mParamArray:Ljava/util/List;

    const/4 p1, 0x0

    .line 1976
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mImageType:I

    .line 1977
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mAfterPWCtype:I

    return-void
.end method


# virtual methods
.method public AddParam(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;)V
    .locals 1

    .line 1988
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mParamArray:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public Clone()Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;
    .locals 4

    .line 2012
    new-instance v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {v0, v1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    .line 2013
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2014
    iget-object v2, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mParamArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;

    .line 2015
    invoke-virtual {v3}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->Clone()Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2017
    :cond_0
    iput-object v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mParamArray:Ljava/util/List;

    .line 2018
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mImageType:I

    iput v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mImageType:I

    .line 2019
    iget v1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mAfterPWCtype:I

    iput v1, v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mAfterPWCtype:I

    return-object v0
.end method

.method public GetAfterPWC()I
    .locals 1

    .line 2000
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mAfterPWCtype:I

    return v0
.end method

.method public GetImageType()I
    .locals 1

    .line 1984
    iget v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mImageType:I

    return v0
.end method

.method public GetParam(I)Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;
    .locals 1

    .line 1992
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mParamArray:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;

    return-object p1
.end method

.method public Reset()V
    .locals 1

    .line 2004
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mParamArray:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public SetAfterPWC(I)V
    .locals 0

    .line 1996
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mAfterPWCtype:I

    return-void
.end method

.method public SetImageType(I)V
    .locals 0

    .line 1980
    iput p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mImageType:I

    return-void
.end method

.method public Size()I
    .locals 1

    .line 2008
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->mParamArray:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

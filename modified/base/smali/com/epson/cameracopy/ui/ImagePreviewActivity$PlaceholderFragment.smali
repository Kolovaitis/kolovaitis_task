.class public Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;
.super Landroid/support/v4/app/Fragment;
.source "ImagePreviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImagePreviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlaceholderFragment"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final KEY_PREVIEW_FILE:Ljava/lang/String; = "preview-file"

.field private static final PARAM_CAMERA_FILE:Ljava/lang/String; = "camera-file"

.field private static final PARAM_FILE_NAME:Ljava/lang/String; = "file_name"


# instance fields
.field mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

.field private mImageView:Lepson/common/ScalableImageView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSaveButton:Landroid/widget/Button;

.field private mShowSetDisplaySizeMessageOnPrintPreview:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 197
    const-class v0, Lcom/epson/cameracopy/ui/ImagePreviewActivity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 197
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;)Z
    .locals 0

    .line 197
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->checkProgress()Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;Lcom/epson/cameracopy/ui/ImageFolderFile;)V
    .locals 0

    .line 197
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->localFolderFileViewModelPrepared(Lcom/epson/cameracopy/ui/ImageFolderFile;)V

    return-void
.end method

.method private checkProgress()Z
    .locals 1

    .line 499
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static getInstance(Ljava/lang/String;Lcom/epson/cameracopy/device/CameraFile;)Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;
    .locals 3

    .line 218
    new-instance v0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    invoke-direct {v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;-><init>()V

    .line 220
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "file_name"

    .line 221
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string p0, "camera-file"

    .line 222
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 223
    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private localFolderFileViewModelPrepared(Lcom/epson/cameracopy/ui/ImageFolderFile;)V
    .locals 2
    .param p1    # Lcom/epson/cameracopy/ui/ImageFolderFile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 506
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 507
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    .line 508
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mSaveButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    invoke-interface {v0}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->needSaveButton()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    .line 509
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->restorePreview()V

    return-void
.end method

.method private observeImageConvertFromArgument()V
    .locals 2

    .line 418
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v1, "file_name"

    .line 425
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 427
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->observeImageFolderFile(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private observeImageFolderFile(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    .line 444
    :cond_0
    new-instance v0, Lcom/epson/cameracopy/ui/ImageFolderFile;

    invoke-direct {v0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFile;-><init>(Ljava/lang/String;)V

    .line 445
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFile;->needsConvert(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 446
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->requestWritePermissionIfNeeded()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 452
    :cond_1
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/Fragment;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v1, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;

    .line 453
    invoke-virtual {p1, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;

    .line 455
    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->setOriginalData(Lcom/epson/cameracopy/ui/ImageFolderFile;)V

    .line 457
    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel;->getData()Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$4;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$4;-><init>(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;)V

    invoke-virtual {p1, p0, v0}, Lcom/epson/cameracopy/ui/ImageFolderFileViewModel$ImageFolderFileLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    return-void
.end method

.method private requestWritePermissionIfNeeded()Z
    .locals 7

    .line 473
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 475
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    .line 476
    new-array v3, v2, [Ljava/lang/String;

    const v4, 0x7f0e0422

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 477
    new-array v2, v2, [Ljava/lang/String;

    const v4, 0x7f0e0420

    .line 478
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 479
    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f0e0424

    invoke-virtual {p0, v6}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v4, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    .line 485
    new-instance v4, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v6, v0, v1

    invoke-direct {v4, v6, v3, v2}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 487
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    .line 488
    invoke-static {p0, v4, v0}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/support/v4/app/Fragment;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return v5

    :cond_0
    return v1
.end method

.method private setPreview(Lepson/common/ScalableImageView;Ljava/lang/String;)V
    .locals 3

    const/16 v0, 0x4b0

    const/4 v1, 0x1

    .line 230
    invoke-static {p2, v0, v0, v1, v1}, Lepson/common/ImageUtil;->loadBitmap(Ljava/lang/String;IIZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 232
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lepson/common/ScalableImageView;->SetImageBitmap(Landroid/graphics/Bitmap;Landroid/content/res/Resources;)V

    .line 235
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, p2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string p2, "Orientation"

    .line 236
    invoke-virtual {v0, p2, v1}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result p2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_2

    const/4 v0, 0x6

    if-eq p2, v0, :cond_1

    const/16 v0, 0x8

    if-eq p2, v0, :cond_0

    const/4 p2, 0x0

    .line 249
    invoke-virtual {p1, p2}, Lepson/common/ScalableImageView;->SetRotate(F)V

    goto :goto_0

    :cond_0
    const/high16 p2, 0x43870000    # 270.0f

    .line 246
    invoke-virtual {p1, p2}, Lepson/common/ScalableImageView;->SetRotate(F)V

    goto :goto_0

    :cond_1
    const/high16 p2, 0x42b40000    # 90.0f

    .line 240
    invoke-virtual {p1, p2}, Lepson/common/ScalableImageView;->SetRotate(F)V

    goto :goto_0

    :cond_2
    const/high16 p2, 0x43340000    # 180.0f

    .line 243
    invoke-virtual {p1, p2}, Lepson/common/ScalableImageView;->SetRotate(F)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :goto_0
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    return-void

    :pswitch_0
    if-eq p2, v0, :cond_1

    .line 649
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 651
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    .line 655
    :cond_1
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->observeImageConvertFromArgument()V

    return-void

    :pswitch_1
    if-eq p2, v0, :cond_2

    goto :goto_0

    .line 639
    :cond_2
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->saveImage()V

    :goto_0
    return-void

    :pswitch_2
    if-ne p2, v0, :cond_3

    const-string p1, "corrected_file_name"

    .line 620
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 623
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    invoke-interface {p2}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->deleteTemporaryFile()V

    .line 624
    new-instance p2, Lcom/epson/cameracopy/device/CameraFile;

    new-instance p3, Ljava/util/Date;

    invoke-direct {p3}, Ljava/util/Date;-><init>()V

    invoke-direct {p2, p3, p1}, Lcom/epson/cameracopy/device/CameraFile;-><init>(Ljava/util/Date;Ljava/lang/String;)V

    .line 625
    new-instance p1, Lcom/epson/cameracopy/ui/CameraFileAdapter;

    invoke-direct {p1, p2}, Lcom/epson/cameracopy/ui/CameraFileAdapter;-><init>(Lcom/epson/cameracopy/device/CameraFile;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    .line 626
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->setPreview()V

    .line 627
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mSaveButton:Landroid/widget/Button;

    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    .line 628
    invoke-interface {p2}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->needSaveButton()Z

    move-result p2

    .line 627
    invoke-static {p1, p2}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    const/4 p1, 0x0

    .line 629
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mShowSetDisplaySizeMessageOnPrintPreview:Z

    .line 631
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->startPrintPreviewActivity()V

    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 297
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "preview-file"

    .line 301
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/ui/ImagePreviewFile;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    goto :goto_0

    .line 303
    :cond_0
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    :cond_1
    const-string v0, "camera-file"

    .line 313
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/cameracopy/device/CameraFile;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    .line 315
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mShowSetDisplaySizeMessageOnPrintPreview:Z

    .line 316
    new-instance v0, Lcom/epson/cameracopy/ui/CameraFileAdapter;

    invoke-direct {v0, p1}, Lcom/epson/cameracopy/ui/CameraFileAdapter;-><init>(Lcom/epson/cameracopy/device/CameraFile;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    .line 318
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mShowSetDisplaySizeMessageOnPrintPreview:Z

    :goto_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const/4 p3, 0x0

    const v0, 0x7f0a006b

    .line 327
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f080266

    .line 330
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lepson/common/ScalableImageView;

    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImageView:Lepson/common/ScalableImageView;

    const p2, 0x7f080291

    .line 332
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ProgressBar;

    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const p2, 0x7f0802cc

    .line 334
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mSaveButton:Landroid/widget/Button;

    .line 335
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mSaveButton:Landroid/widget/Button;

    new-instance v0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;-><init>(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;)V

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f080268

    .line 377
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 378
    new-instance v0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$2;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$2;-><init>(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;)V

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f0802ae

    .line 388
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 389
    new-instance v0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$3;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$3;-><init>(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;)V

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 399
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    if-eqz p2, :cond_0

    .line 401
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 402
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->setPreview()V

    .line 403
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mSaveButton:Landroid/widget/Button;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    invoke-interface {p3}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->needSaveButton()Z

    move-result p3

    invoke-static {p2, p3}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    goto :goto_0

    .line 405
    :cond_0
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p2, p3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 406
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->observeImageConvertFromArgument()V

    :goto_0
    return-object p1
.end method

.method public onPause()V
    .locals 1

    .line 522
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->releasePreviewBitmap()V

    .line 524
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    invoke-interface {v0}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->deleteTemporaryFile()V

    .line 527
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 541
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "preview-file"

    .line 542
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    check-cast v1, Ljava/io/Serializable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    .line 514
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 516
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->restorePreview()V

    return-void
.end method

.method public onStop()V
    .locals 0

    .line 533
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->releasePreviewBitmap()V

    .line 535
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    return-void
.end method

.method public releasePreviewBitmap()V
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImageView:Lepson/common/ScalableImageView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 290
    invoke-virtual {v0, v1, v1}, Lepson/common/ScalableImageView;->SetImageBitmap(Landroid/graphics/Bitmap;Landroid/content/res/Resources;)V

    :cond_0
    return-void
.end method

.method public restorePreview()V
    .locals 2

    .line 273
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImageView:Lepson/common/ScalableImageView;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    if-nez v1, :cond_0

    goto :goto_0

    .line 277
    :cond_0
    invoke-virtual {v0}, Lepson/common/ScalableImageView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    return-void

    .line 282
    :cond_1
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->setPreview()V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method public saveImage()V
    .locals 3

    .line 548
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mSaveButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    .line 551
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object v0

    .line 552
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getSaveDirecotry()Ljava/lang/String;

    move-result-object v0

    .line 553
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    invoke-interface {v1, v0}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->doSave(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mSaveButton:Landroid/widget/Button;

    invoke-static {v0, v1}, Lcom/epson/cameracopy/alt/UiCommon;->setButtonEnabled(Landroid/widget/Button;Z)V

    const v0, 0x7f0e0372

    .line 559
    invoke-static {v0}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->newInstance(I)Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;

    move-result-object v0

    .line 560
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "error-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/ui/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    .line 565
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    invoke-interface {v2}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->getPrintFileName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 567
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lepson/common/ImageUtil;->galleryAddPic(Landroid/content/Context;Ljava/io/File;)V

    .line 571
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 570
    invoke-static {v1, v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;->newInstance(ILjava/lang/String;)Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;

    move-result-object v0

    .line 572
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "ok-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public setPreview()V
    .locals 2

    .line 263
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    invoke-interface {v0}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->getPrintFileName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImageView:Lepson/common/ScalableImageView;

    invoke-direct {p0, v1, v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->setPreview(Lepson/common/ScalableImageView;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public startImageCollectActivity()V
    .locals 4

    .line 598
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    .line 599
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->getImageProcFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 606
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "image_file_name"

    .line 607
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x1

    .line 609
    invoke-virtual {p0, v1, v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public startPrintPreviewActivity()V
    .locals 4

    .line 578
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mImagePreviewFile:Lcom/epson/cameracopy/ui/ImagePreviewFile;

    invoke-interface {v0}, Lcom/epson/cameracopy/ui/ImagePreviewFile;->getPrintFileName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 585
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 586
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 587
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v0, "imageList"

    .line 588
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 590
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->mShowSetDisplaySizeMessageOnPrintPreview:Z

    if-eqz v0, :cond_1

    const-string v0, "display-sizeset-message"

    .line 591
    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 594
    :cond_1
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

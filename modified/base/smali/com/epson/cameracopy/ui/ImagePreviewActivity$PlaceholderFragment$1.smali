.class Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;
.super Ljava/lang/Object;
.source "ImagePreviewActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;)V
    .locals 0

    .line 335
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .line 338
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->access$000(Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 343
    :cond_0
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 344
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x2

    .line 345
    new-array v1, v0, [Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    const v3, 0x7f0e0422

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 346
    new-array v2, v0, [Ljava/lang/String;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    const v6, 0x7f0e0420

    .line 347
    invoke-virtual {v5, v6}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    .line 348
    invoke-virtual {v5, v6}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    const v8, 0x7f0e0424

    invoke-virtual {v7, v8}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    .line 354
    new-instance v3, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v4, p1, v4

    invoke-direct {v3, v4, v1, v2}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 356
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    invoke-virtual {v1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 357
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    invoke-static {p1, v3, v0}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/support/v4/app/Fragment;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 362
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment$1;->this$0:Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$PlaceholderFragment;->saveImage()V

    return-void
.end method

.class public Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;
.super Lepson/print/ActivityIACommon;
.source "CameraPreviewOptionActivity.java"

# interfaces
.implements Lcom/epson/cameracopy/ui/PictureResolutionDialogFragment$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;
    }
.end annotation


# static fields
.field private static final CODE_DIRECTORY_SELECT:I = 0x1


# instance fields
.field mFragment:Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 44
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a001d

    .line 45
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;->setContentView(I)V

    const v0, 0x7f0e02ed

    const/4 v1, 0x1

    .line 48
    invoke-virtual {p0, v0, v1}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;->setActionBar(IZ)V

    if-nez p1, :cond_0

    .line 51
    new-instance p1, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;

    invoke-direct {p1}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;->mFragment:Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;

    .line 52
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    const v0, 0x7f0800d8

    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;->mFragment:Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;

    .line 53
    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    return-void
.end method

.method public onItemSelected(ZILandroid/graphics/Point;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    .line 68
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity;->mFragment:Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;

    if-eqz p1, :cond_1

    .line 69
    invoke-virtual {p1, p2, p3}, Lcom/epson/cameracopy/ui/CameraPreviewOptionActivity$PlaceholderFragment;->changeSelectedCameraPictureResolution(ILandroid/graphics/Point;)V

    :cond_1
    return-void
.end method

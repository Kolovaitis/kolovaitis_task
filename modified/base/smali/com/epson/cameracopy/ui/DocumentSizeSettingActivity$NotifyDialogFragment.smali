.class public Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "DocumentSizeSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NotifyDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 294
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .line 298
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 301
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 302
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 305
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 306
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 307
    new-instance p1, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment$1;

    invoke-direct {p1, p0, v0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment$1;-><init>(Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;I)V

    const v2, 0x7f0e0548

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 315
    new-instance p1, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment$2;

    invoke-direct {p1, p0, v0}, Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment$2;-><init>(Lcom/epson/cameracopy/ui/DocumentSizeSettingActivity$NotifyDialogFragment;I)V

    const v0, 0x7f0e03f2

    invoke-virtual {v1, v0, p1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 324
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 325
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p1
.end method

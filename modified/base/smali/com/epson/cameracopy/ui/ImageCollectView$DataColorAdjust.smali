.class Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;
.super Ljava/lang/Object;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageCollectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataColorAdjust"
.end annotation


# instance fields
.field public mPointArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation
.end field

.field public mPointEdgeArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation
.end field

.field public mPointMoveArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImageCollectView;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 97
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    .line 98
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    .line 99
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    .line 102
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    .line 103
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    .line 104
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    return-void
.end method

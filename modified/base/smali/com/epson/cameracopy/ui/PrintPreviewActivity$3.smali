.class Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;
.super Ljava/lang/Object;
.source "PrintPreviewActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/PrintPreviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 913
    const-class v0, Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    return-void
.end method

.method constructor <init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 913
    iput-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5

    .line 916
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/16 v1, 0x64

    if-eq v0, v1, :cond_1

    packed-switch v0, :pswitch_data_0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 954
    :pswitch_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$800(Lcom/epson/cameracopy/ui/PrintPreviewActivity;D)V

    return v2

    .line 936
    :pswitch_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$600(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/EPImageList;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/EPImageList;->size()I

    move-result p1

    if-gtz p1, :cond_0

    .line 937
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$400(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    .line 941
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    const v0, 0x7f0e0373

    .line 942
    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 941
    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 943
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 922
    :pswitch_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$200(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    goto :goto_0

    .line 918
    :pswitch_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$100(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    goto :goto_0

    .line 948
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {v0, p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$700(Lcom/epson/cameracopy/ui/PrintPreviewActivity;Landroid/os/Message;)V

    goto :goto_0

    .line 930
    :cond_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$400(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    .line 932
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$500(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    goto :goto_0

    .line 926
    :cond_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$3;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$300(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    :goto_0
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

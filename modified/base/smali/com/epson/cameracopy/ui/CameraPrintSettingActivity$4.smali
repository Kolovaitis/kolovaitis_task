.class Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "CameraPrintSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;)V
    .locals 0

    .line 2327
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2340
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$002(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "IEpsonServiceCallback"

    .line 2349
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNotifyContinueable code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 2351
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isInfoAvai:Z

    .line 2354
    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getPrinterLang()I

    move-result v0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 2357
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2358
    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public onNotifyEndJob(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string p1, "IEpsonServiceCallback"

    .line 2364
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onNotifyError errorCode = "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2379
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->isInfoAvai:Z

    .line 2380
    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->getPrinterLang()I

    move-result p2

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->access$5602(Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;I)I

    .line 2381
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity$4;->this$0:Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;

    const/4 p2, 0x0

    iput-boolean p2, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->undoFlag:Z

    .line 2382
    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onNotifyProgress(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

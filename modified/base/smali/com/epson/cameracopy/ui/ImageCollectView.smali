.class public Lcom/epson/cameracopy/ui/ImageCollectView;
.super Landroid/view/View;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/ImageCollectView$EPxLog;,
        Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;,
        Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;,
        Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageTextMessage;,
        Lcom/epson/cameracopy/ui/ImageCollectView$Data;,
        Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;,
        Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;,
        Lcom/epson/cameracopy/ui/ImageCollectView$DataEnhanceText;,
        Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;
    }
.end annotation


# static fields
.field private static final DRAG_EDGE_STROKE_WIDTH:F = 1.0f

.field private static final MESSAGE_TEXT_SIZE:F = 10.0f

.field private static final RADIUS_CROP_CIRCLE:F = 16.0f

.field private static final SCALE_RANGE_MAX:F = 4.0f

.field private static final SCALE_RANGE_MIN:F = 1.0f

.field private static final STROKE_WIDTH_DRAG_PATH:F = 20.0f

.field private static final TOUCH_STATE_DOWN_MOVE:I = 0x0

.field private static final TOUCH_STATE_IDOLE:I = -0x1

.field private static final TOUCH_STATE_UP:I = 0x1

.field private static final VIEW_MARGINE:F = 6.0f

.field public static final VIEW_MODE_COLOR_ADJUSTMENT_ALL:I = 0x2

.field public static final VIEW_MODE_COLOR_ADJUSTMENT_PART:I = 0x3

.field public static final VIEW_MODE_CROP_IMAGE:I = 0x0

.field public static final VIEW_MODE_ENHANCE_TEXT:I = 0x1


# instance fields
.field private INVERT:[I

.field private mBitmapImage:Landroid/graphics/Bitmap;

.field private mBitmapMask:Landroid/graphics/Bitmap;

.field private mCanvasMask:Landroid/graphics/Canvas;

.field private mContext:Landroid/content/Context;

.field private mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGetCropPoint:Z

.field private mHandlerAvtivity:Landroid/os/Handler;

.field private mOrient:I

.field private mPathEffectData:Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mViewMode:I

.field private final onGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private final onScaleGestureDetector:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 250
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 232
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    const/4 v1, 0x0

    .line 233
    iput v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    const/4 v2, 0x2

    .line 234
    new-array v2, v2, [I

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    .line 235
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    .line 236
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    .line 237
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mCanvasMask:Landroid/graphics/Canvas;

    .line 238
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mPathEffectData:Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

    .line 240
    iput v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    .line 241
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mContext:Landroid/content/Context;

    .line 242
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mHandlerAvtivity:Landroid/os/Handler;

    .line 243
    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGetCropPoint:Z

    .line 246
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 247
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 1505
    new-instance v0, Lcom/epson/cameracopy/ui/ImageCollectView$1;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$1;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->onScaleGestureDetector:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

    .line 1573
    new-instance v0, Lcom/epson/cameracopy/ui/ImageCollectView$2;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$2;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->onGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 253
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->init(Landroid/content/Context;)V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 259
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 232
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    const/4 v0, 0x0

    .line 233
    iput v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    const/4 v1, 0x2

    .line 234
    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    .line 235
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    .line 236
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    .line 237
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mCanvasMask:Landroid/graphics/Canvas;

    .line 238
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mPathEffectData:Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

    .line 240
    iput v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    .line 241
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mContext:Landroid/content/Context;

    .line 242
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mHandlerAvtivity:Landroid/os/Handler;

    .line 243
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGetCropPoint:Z

    .line 246
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 247
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 1505
    new-instance p2, Lcom/epson/cameracopy/ui/ImageCollectView$1;

    invoke-direct {p2, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$1;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->onScaleGestureDetector:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

    .line 1573
    new-instance p2, Lcom/epson/cameracopy/ui/ImageCollectView$2;

    invoke-direct {p2, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$2;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->onGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 262
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->isInEditMode()Z

    move-result p2

    if-nez p2, :cond_0

    .line 263
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->init(Landroid/content/Context;)V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 268
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 232
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    const/4 p3, 0x0

    .line 233
    iput p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    const/4 v0, 0x2

    .line 234
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    .line 235
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    .line 236
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    .line 237
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mCanvasMask:Landroid/graphics/Canvas;

    .line 238
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mPathEffectData:Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

    .line 240
    iput p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    .line 241
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mContext:Landroid/content/Context;

    .line 242
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mHandlerAvtivity:Landroid/os/Handler;

    .line 243
    iput-boolean p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGetCropPoint:Z

    .line 246
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 247
    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 1505
    new-instance p2, Lcom/epson/cameracopy/ui/ImageCollectView$1;

    invoke-direct {p2, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$1;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->onScaleGestureDetector:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

    .line 1573
    new-instance p2, Lcom/epson/cameracopy/ui/ImageCollectView$2;

    invoke-direct {p2, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$2;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->onGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 271
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->isInEditMode()Z

    move-result p2

    if-nez p2, :cond_0

    .line 272
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->init(Landroid/content/Context;)V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x0
    .end array-data
.end method

.method private ClontPointArray(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;)",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation

    .line 653
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_1

    .line 655
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 656
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 657
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 658
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 660
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private DrawColorAdjustmentAll(Landroid/graphics/Canvas;)V
    .locals 3

    .line 787
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImage(Landroid/graphics/Canvas;)V

    .line 788
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mPathEffectData:Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->BlinkingIndex(Landroid/graphics/RectF;)Landroid/graphics/PathEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 789
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-void
.end method

.method private DrawColorAdjustmentPart(Landroid/graphics/Canvas;)V
    .locals 12

    .line 794
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImage(Landroid/graphics/Canvas;)V

    .line 798
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    .line 799
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    .line 800
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    .line 801
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mStrokeWidth:F

    mul-float v4, v4, v2

    .line 802
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 803
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 805
    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOn:Landroid/graphics/Paint;

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 808
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v7

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/4 v8, 0x0

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 809
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 810
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    .line 812
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-le v9, v3, :cond_0

    .line 813
    iget v9, v8, Landroid/graphics/PointF;->x:F

    add-float/2addr v9, v0

    iget v10, v8, Landroid/graphics/PointF;->y:F

    add-float/2addr v10, v1

    invoke-virtual {v5, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 814
    iget v9, v8, Landroid/graphics/PointF;->x:F

    div-float/2addr v9, v2

    iget v8, v8, Landroid/graphics/PointF;->y:F

    div-float/2addr v8, v2

    invoke-virtual {v6, v9, v8}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v8, 0x1

    .line 815
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_0

    .line 816
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    .line 817
    iget v10, v9, Landroid/graphics/PointF;->x:F

    add-float/2addr v10, v0

    iget v11, v9, Landroid/graphics/PointF;->y:F

    add-float/2addr v11, v1

    invoke-virtual {v5, v10, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 818
    iget v10, v9, Landroid/graphics/PointF;->x:F

    div-float/2addr v10, v2

    iget v9, v9, Landroid/graphics/PointF;->y:F

    div-float/2addr v9, v2

    invoke-virtual {v6, v10, v9}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 824
    :cond_1
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTouchState:I

    if-nez v2, :cond_3

    .line 827
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v4

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 829
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v6

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    if-le v2, v3, :cond_2

    .line 831
    iget v6, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v0

    iget v4, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v1

    invoke-virtual {v5, v6, v4}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_1
    if-ge v3, v2, :cond_2

    .line 833
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v6

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 834
    iget v6, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v0

    iget v4, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v1

    invoke-virtual {v5, v6, v4}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 840
    :cond_2
    invoke-virtual {v5}, Landroid/graphics/Path;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 841
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOn:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 846
    :cond_3
    invoke-virtual {v5}, Landroid/graphics/Path;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 849
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTouchState:I

    if-ne v2, v3, :cond_5

    .line 850
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    const/4 v0, -0x1

    iput v0, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTouchState:I

    .line 851
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mCanvasMask:Landroid/graphics/Canvas;

    if-eqz p1, :cond_8

    .line 852
    invoke-virtual {v6}, Landroid/graphics/Path;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    .line 853
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mCanvasMask:Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragMask:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 855
    :cond_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mHandlerAvtivity:Landroid/os/Handler;

    const/16 v0, 0x12d

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_4

    .line 859
    :cond_5
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v4

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v4

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 903
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 904
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mPathEffectData:Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->BlinkingIndex(Landroid/graphics/RectF;)Landroid/graphics/PathEffect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 905
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 906
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_7

    .line 907
    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    .line 908
    iget v7, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v7, v0

    .line 909
    iget v6, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v1

    .line 910
    invoke-virtual {v2, v7, v6}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v9, 0x1

    .line 911
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_6

    .line 912
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    .line 913
    iget v11, v10, Landroid/graphics/PointF;->x:F

    add-float/2addr v11, v0

    iget v10, v10, Landroid/graphics/PointF;->y:F

    add-float/2addr v10, v1

    invoke-virtual {v2, v11, v10}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 915
    :cond_6
    invoke-virtual {v2, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 917
    :cond_7
    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 918
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    goto :goto_2

    :cond_8
    :goto_4
    return-void
.end method

.method private DrawCropImage(Landroid/graphics/Canvas;)V
    .locals 10

    .line 696
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImage(Landroid/graphics/Canvas;)V

    .line 699
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetPreviewCropPoint()V

    .line 702
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    .line 705
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget v5, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    aget-object v0, v0, v9

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v7, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 707
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v5, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    const/4 v9, 0x2

    aget-object v0, v0, v9

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v7, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 709
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v5, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v7, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 711
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v5, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget v7, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :goto_0
    const/4 v0, 0x4

    if-ge v2, v0, :cond_1

    .line 716
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    if-ne v2, v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mRadiusCropCircle:F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleSelect:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 722
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mRadiusCropCircle:F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleDeselected:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private DrawEnhanceText(Landroid/graphics/Canvas;)V
    .locals 0

    .line 782
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImage(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private DrawPreviewImage(Landroid/graphics/Canvas;)V
    .locals 3

    .line 928
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 929
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_0

    .line 931
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectBitmap:Landroid/graphics/RectF;

    invoke-direct {p0, v0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImageGetDrawable(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 932
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v1, v1, v2

    iput-object v0, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    :cond_0
    if-eqz v0, :cond_1

    .line 936
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mMatrixBitmap:Landroid/graphics/Matrix;

    invoke-direct {p0, v1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImageGetMatrix(Landroid/graphics/Matrix;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 937
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 938
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mMatrixBitmap:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 939
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 940
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    return-void
.end method

.method private DrawPreviewImageCalcMatrix(Landroid/graphics/Matrix;)V
    .locals 7

    .line 1106
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 1110
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    aget v2, v2, v1

    mul-float v0, v0, v2

    .line 1111
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1138
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointInitOffset:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointInitOffset:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1143
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectBitmap:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectBitmap:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1144
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1147
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    .line 1148
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v5

    sub-float/2addr v0, v3

    neg-float v0, v0

    .line 1150
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    sub-float/2addr v3, v5

    div-float/2addr v3, v2

    .line 1151
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v5

    sub-float/2addr v3, v2

    neg-float v2, v3

    .line 1153
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v3, v3, v1

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v0

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v2

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1155
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectBitmap:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectBitmap:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1156
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    return-void
.end method

.method private DrawPreviewImageGetDrawable(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 4

    if-eqz p1, :cond_0

    .line 966
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 968
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p2, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 969
    new-instance p2, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    const/4 v2, 0x0

    invoke-direct {p2, v2, v2, v1, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private DrawPreviewImageGetMatrix(Landroid/graphics/Matrix;)Z
    .locals 2

    .line 977
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImageCalcMatrix(Landroid/graphics/Matrix;)V

    .line 979
    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 981
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->access$000(Lcom/epson/cameracopy/ui/ImageCollectView$Data;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImagePointCheck(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 983
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImageCalcMatrix(Landroid/graphics/Matrix;)V

    :cond_0
    return v1
.end method

.method private DrawPreviewImagePointCheck(Z)Z
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_8

    .line 1004
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p1

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    cmpg-float p1, p1, v3

    if-gez p1, :cond_0

    .line 1006
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    sub-float/2addr v3, v5

    div-float/2addr v3, v4

    add-float/2addr p1, v3

    .line 1007
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v3

    goto/16 :goto_1

    .line 1009
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    cmpl-float p1, p1, v3

    if-lez p1, :cond_1

    .line 1010
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v3

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1012
    :goto_0
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpg-float v3, v3, v5

    if-gez v3, :cond_2

    .line 1013
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr p1, v3

    .line 1018
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpg-float v3, v3, v5

    if-gez v3, :cond_3

    .line 1020
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v5, v6

    div-float/2addr v5, v4

    add-float/2addr v3, v5

    .line 1021
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    goto/16 :goto_3

    .line 1023
    :cond_3
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 1024
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    .line 1026
    :goto_2
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_5

    .line 1027
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    :cond_5
    :goto_3
    cmpl-float v4, p1, v0

    if-nez v4, :cond_7

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_6

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 1033
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v4, v4, v2

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v5, v5, v2

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, p1

    iput v5, v4, Landroid/graphics/PointF;->x:F

    iput v5, v0, Landroid/graphics/PointF;->x:F

    .line 1034
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v3

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iput v1, p1, Landroid/graphics/PointF;->y:F

    goto/16 :goto_a

    .line 1045
    :cond_8
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p1

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    cmpg-float p1, p1, v3

    if-gez p1, :cond_9

    .line 1047
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    iput v0, p1, Landroid/graphics/PointF;->x:F

    const/4 p1, 0x1

    goto :goto_6

    .line 1053
    :cond_9
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    cmpl-float p1, p1, v3

    if-lez p1, :cond_a

    const/4 p1, 0x1

    goto :goto_5

    :cond_a
    const/4 p1, 0x0

    .line 1056
    :goto_5
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_b

    const/4 p1, 0x1

    .line 1062
    :cond_b
    :goto_6
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_c

    .line 1064
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v3, v3, v1

    iput v0, v3, Landroid/graphics/PointF;->y:F

    const/4 v0, 0x1

    goto :goto_8

    .line 1070
    :cond_c
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_d

    const/4 v0, 0x1

    goto :goto_7

    :cond_d
    const/4 v0, 0x0

    .line 1073
    :goto_7
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mRectDrawScale:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_e

    const/4 v0, 0x1

    :cond_e
    :goto_8
    if-eqz p1, :cond_f

    .line 1084
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v3, v3, v1

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iput v3, p1, Landroid/graphics/PointF;->x:F

    const/4 p1, 0x1

    goto :goto_9

    .line 1089
    :cond_f
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iput v3, p1, Landroid/graphics/PointF;->x:F

    const/4 p1, 0x0

    :goto_9
    if-eqz v0, :cond_10

    .line 1093
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v2

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p1, Landroid/graphics/PointF;->y:F

    goto :goto_a

    .line 1098
    :cond_10
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    move v2, p1

    :goto_a
    return v2
.end method

.method private GetPreviewCropPoint()V
    .locals 9

    .line 1729
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    if-eqz v0, :cond_4

    .line 1731
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 1734
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_4

    .line 1738
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    const/4 v3, 0x0

    const/4 v4, 0x1

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 1741
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    aget v6, v6, v4

    mul-float v5, v5, v6

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v6

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 1744
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    aget v6, v6, v4

    mul-float v5, v5, v6

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, v6

    iput v5, v2, Landroid/graphics/PointF;->y:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    .line 1751
    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v6, v6, v0

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v7, v7, v8

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v7, v7, v0

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v7}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x1

    .line 1764
    :goto_2
    iget-boolean v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGetCropPoint:Z

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 1765
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v3

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 1766
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v3

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 1767
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v4

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 1768
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v4

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 1769
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    const/4 v2, 0x2

    aget-object v0, v0, v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iput v3, v0, Landroid/graphics/PointF;->x:F

    .line 1770
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 1771
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 1772
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    :cond_4
    return-void
.end method

.method private GetPreviewImage()V
    .locals 15

    .line 1644
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 1645
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 1646
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 1647
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v0

    .line 1648
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v3, v1

    cmpg-float v4, v2, v3

    if-gez v4, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    .line 1654
    :goto_0
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    const/4 v4, 0x1

    aput v2, v3, v4

    .line 1658
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    mul-float v0, v0, v2

    mul-float v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v3, v2, v2, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1659
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    .line 1660
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    .line 1661
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v5, v6

    div-float/2addr v5, v3

    add-float/2addr v2, v5

    .line 1659
    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1662
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 1665
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float/2addr v0, v3

    .line 1666
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v1, v3

    .line 1667
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    const v3, 0x3d4ccccd    # 0.05f

    mul-float v2, v2, v3

    .line 1668
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v0

    .line 1669
    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v1

    .line 1674
    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    iget-object v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    iget-object v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v9, v9, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v9, v9, v10

    iget-object v9, v9, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v10, v10, v11

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v0

    sub-float/2addr v10, v2

    iget-object v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v11, v11, v12

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v11, v1

    sub-float/2addr v11, v2

    invoke-virtual {v6, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1679
    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v8

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v6, v6, v4

    iget-object v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    sub-float/2addr v8, v0

    add-float/2addr v8, v2

    iget-object v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v9, v9, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v9, v9, v10

    iget-object v9, v9, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v10, v10, v11

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    iget-object v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v11, v11, v12

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v11, v1

    sub-float/2addr v11, v2

    invoke-virtual {v6, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1684
    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v8

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    const/4 v8, 0x2

    aget-object v6, v6, v8

    iget-object v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v9, v9, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v9, v9, v10

    iget-object v9, v9, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    sub-float/2addr v9, v0

    add-float/2addr v9, v2

    iget-object v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v10, v10, v11

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v10, v1

    add-float/2addr v10, v2

    iget-object v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v11, v11, v12

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    iget-object v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v12, v12, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v13, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v12, v12, v13

    iget-object v12, v12, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v6, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1689
    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v9

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    const/4 v9, 0x3

    aget-object v6, v6, v9

    iget-object v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v10, v10, v11

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    iget-object v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v11, v11, v12

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v11, v1

    add-float/2addr v11, v2

    iget-object v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v12, v12, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v13, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v12, v12, v13

    iget-object v12, v12, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->left:F

    add-float/2addr v12, v0

    sub-float/2addr v12, v2

    iget-object v13, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v13, v13, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v14, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v13, v13, v14

    iget-object v13, v13, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v13, v13, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v6, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1696
    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v10

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    aget-object v6, v6, v7

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v7, v7, v10

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget-object v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v10, v10, v11

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    iget-object v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v11, v11, v12

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    add-float/2addr v11, v0

    sub-float/2addr v11, v2

    iget-object v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v12, v12, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v13, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v12, v12, v13

    iget-object v12, v12, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->top:F

    add-float/2addr v12, v1

    sub-float/2addr v12, v2

    invoke-virtual {v6, v7, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1701
    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    aget-object v4, v6, v4

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    sub-float/2addr v6, v0

    add-float/2addr v6, v2

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v7, v7, v10

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v10, v10, v11

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    iget-object v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v12, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v11, v11, v12

    iget-object v11, v11, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v11, v1

    sub-float/2addr v11, v2

    invoke-virtual {v4, v6, v7, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1706
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v6

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    aget-object v4, v4, v8

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    sub-float/2addr v6, v0

    add-float/2addr v6, v2

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v7, v7, v8

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v7, v1

    add-float/2addr v7, v2

    iget-object v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v8, v8, v10

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    iget-object v10, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v11, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v10, v10, v11

    iget-object v10, v10, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v6, v7, v8, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1711
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v6

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    aget-object v4, v4, v9

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v7, v7, v8

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v7, v1

    add-float/2addr v7, v2

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v8

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v0

    sub-float/2addr v1, v2

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v8

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v6, v7, v1, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1716
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    sub-float v1, v3, v2

    sub-float v4, v5, v2

    add-float/2addr v3, v2

    add-float/2addr v5, v2

    invoke-virtual {v0, v1, v4, v3, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1721
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointInitOffset:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1722
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointInitOffset:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    :cond_1
    return-void
.end method

.method private ResetScalePos(Z)V
    .locals 6

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez p1, :cond_0

    .line 1550
    iget p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    if-eq p1, v3, :cond_1

    .line 1551
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v4

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v4

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    invoke-virtual {p1, v2, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1552
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v4

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v4

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v3

    invoke-virtual {p1, v2, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1553
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v4

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v4

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    aput v0, v4, v3

    aput v0, p1, v1

    .line 1556
    :cond_1
    iget p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    if-nez p1, :cond_2

    .line 1558
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v4

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    invoke-virtual {p1, v2, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1559
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v4

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v3

    invoke-virtual {p1, v2, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1560
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v2

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v4

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    aput v0, v2, v3

    aput v0, p1, v1

    :cond_2
    return-void
.end method

.method private SetPreviewBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 411
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->ReleaseBitmap()V

    .line 414
    :cond_0
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    const/4 p1, 0x0

    .line 415
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->ResetScalePos(Z)V

    .line 416
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->invalidate()V

    return-void
.end method

.method private SetScale(F)V
    .locals 4

    .line 1532
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    const/4 v3, 0x0

    aput v1, v0, v3

    .line 1535
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    aget v0, v0, v2

    mul-float v0, v0, p1

    const/high16 p1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, p1

    if-gez v1, :cond_0

    .line 1538
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    aput p1, v0, v2

    goto :goto_0

    :cond_0
    const/high16 p1, 0x40800000    # 4.0f

    cmpl-float v1, v0, p1

    if-lez v1, :cond_1

    .line 1540
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    aput p1, v0, v2

    goto :goto_0

    .line 1542
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v1

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v1

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    aput v0, p1, v2

    :goto_0
    return-void
.end method

.method private TouchEventColorAdjustmentAll(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method private TouchEventColorAdjustmentPart(Landroid/view/MotionEvent;)V
    .locals 9

    .line 1441
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 1445
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1446
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 1447
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    .line 1448
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    sub-float/2addr p1, v3

    const/4 v4, 0x0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 1463
    :pswitch_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1464
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->invalidate()V

    goto/16 :goto_1

    .line 1469
    :pswitch_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x1

    if-lez p1, :cond_4

    .line 1470
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 1472
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 1473
    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectPreviewImage:Landroid/graphics/RectF;

    iget v7, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v7, v2

    iget v8, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v8, v3

    invoke-virtual {v6, v7, v8}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1475
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1478
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 1479
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 1482
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    .line 1483
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 1484
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 1485
    new-instance v3, Landroid/graphics/PointF;

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v5, v1, v4

    sub-float v6, v2, v4

    invoke-direct {v3, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1486
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v1, v6}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1487
    new-instance v3, Landroid/graphics/PointF;

    add-float v7, v1, v4

    invoke-direct {v3, v7, v6}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1488
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v7, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1489
    new-instance v3, Landroid/graphics/PointF;

    add-float/2addr v4, v2

    invoke-direct {v3, v7, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1490
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v1, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1491
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v5, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1492
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v5, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1494
    :cond_2
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1496
    :cond_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v1

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 1499
    :cond_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iput v0, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTouchState:I

    .line 1500
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->invalidate()V

    goto :goto_1

    .line 1457
    :pswitch_2
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iput v4, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTouchState:I

    .line 1458
    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1459
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointMoveArray:Ljava/util/List;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private TouchEventCropImage(Landroid/view/MotionEvent;)V
    .locals 6

    .line 1273
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/4 v1, -0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 1300
    :pswitch_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    if-le v0, v1, :cond_5

    .line 1302
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1303
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 1306
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewTouch:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float v2, v1, v2

    .line 1307
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewTouch:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v3, p1, v3

    .line 1310
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v4, v4, v0

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v2

    .line 1311
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v3

    .line 1312
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v3, v3, v0

    invoke-virtual {v3, v4, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1314
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/RectF;->left:F

    cmpg-float v3, v4, v3

    if-gez v3, :cond_0

    .line 1316
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v3, v3, v0

    iget v4, v3, Landroid/graphics/RectF;->left:F

    .line 1318
    :cond_0
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/RectF;->top:F

    cmpg-float v3, v2, v3

    if-gez v3, :cond_1

    .line 1320
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/RectF;->top:F

    .line 1322
    :cond_1
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/RectF;->right:F

    cmpl-float v3, v4, v3

    if-lez v3, :cond_2

    .line 1324
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/RectF;->right:F

    move v4, v3

    .line 1326
    :cond_2
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    cmpl-float v3, v2, v3

    if-lez v3, :cond_3

    .line 1328
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    .line 1332
    :cond_3
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v3, v3, v0

    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 1333
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v0, v3, v0

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 1334
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewTouch:Landroid/graphics/PointF;

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1335
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewTouch:Landroid/graphics/PointF;

    iput p1, v0, Landroid/graphics/PointF;->y:F

    .line 1336
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->invalidate()V

    goto/16 :goto_0

    .line 1342
    :pswitch_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    if-le p1, v1, :cond_5

    .line 1343
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTimerCropImageCropCircle:Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;

    invoke-virtual {p1, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->TimerOn(Landroid/view/View;)V

    goto :goto_0

    .line 1278
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1279
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 1280
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTimerCropImageCropCircle:Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;

    invoke-virtual {v2}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->TimerOff()V

    const/4 v2, 0x1

    .line 1283
    invoke-direct {p0, v0, p1, v2}, Lcom/epson/cameracopy/ui/ImageCollectView;->TouchEventCropImageHitCheck(FFZ)I

    move-result v2

    if-le v2, v1, :cond_4

    .line 1285
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewTouch:Landroid/graphics/PointF;

    invoke-virtual {v1, v0, p1}, Landroid/graphics/PointF;->set(FF)V

    .line 1286
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iput v2, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    goto :goto_0

    .line 1288
    :cond_4
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    if-le v2, v1, :cond_5

    const/4 v2, 0x0

    .line 1289
    invoke-direct {p0, v0, p1, v2}, Lcom/epson/cameracopy/ui/ImageCollectView;->TouchEventCropImageHitCheck(FFZ)I

    move-result v2

    if-le v2, v1, :cond_5

    .line 1291
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTimerCropImageCropCircle:Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;

    invoke-virtual {v1, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->TimerOn(Landroid/view/View;)V

    .line 1292
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewTouch:Landroid/graphics/PointF;

    invoke-virtual {v1, v0, p1}, Landroid/graphics/PointF;->set(FF)V

    .line 1293
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iput v2, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    :cond_5
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private TouchEventCropImageHitCheck(FFZ)I
    .locals 8

    const/4 v0, 0x3

    :goto_0
    const/4 v1, -0x1

    if-le v0, v1, :cond_3

    .line 1356
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1357
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 1358
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v4, v4, v0

    iget v4, v4, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mRadiusCropCircle:F

    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    const/4 v3, 0x1

    .line 1359
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1360
    new-instance v3, Landroid/graphics/Region;

    invoke-direct {v3}, Landroid/graphics/Region;-><init>()V

    .line 1361
    new-instance v4, Landroid/graphics/Region;

    iget v5, v1, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iget v6, v1, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    iget v7, v1, Landroid/graphics/RectF;->right:F

    float-to-int v7, v7

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    invoke-direct {v4, v5, v6, v7, v1}, Landroid/graphics/Region;-><init>(IIII)V

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 1362
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Region;->contains(II)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p3, :cond_0

    .line 1367
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    goto :goto_1

    .line 1369
    :cond_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_0

    :cond_3
    :goto_2
    return v0
.end method

.method private TouchEventEnhanceText(Landroid/view/MotionEvent;)V
    .locals 8

    .line 1382
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 1383
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    if-eq v0, v2, :cond_0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 1397
    :pswitch_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->access$000(Lcom/epson/cameracopy/ui/ImageCollectView$Data;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1398
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1399
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1402
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMovePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float v4, v0, v4

    .line 1403
    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMovePoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float v5, v2, v5

    .line 1405
    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v6, v6, v3

    iget v7, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v7, v4

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 1406
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v6

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v6

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v4, v4, v3

    iget v6, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v5

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1408
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMovePoint:Landroid/graphics/PointF;

    iput v0, v4, Landroid/graphics/PointF;->x:F

    .line 1409
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMovePoint:Landroid/graphics/PointF;

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 1410
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->invalidate()V

    goto :goto_0

    .line 1416
    :pswitch_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->access$002(Lcom/epson/cameracopy/ui/ImageCollectView$Data;Z)Z

    goto :goto_0

    .line 1390
    :pswitch_2
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->access$000(Lcom/epson/cameracopy/ui/ImageCollectView$Data;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1391
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1392
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1393
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMovePoint:Landroid/graphics/PointF;

    invoke-virtual {v4, v0, v2}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1387
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    invoke-static {v0, v3}, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->access$002(Lcom/epson/cameracopy/ui/ImageCollectView$Data;Z)Z

    :cond_1
    :goto_0
    if-ne v1, v3, :cond_2

    .line 1422
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_2
    const/4 v0, 0x2

    if-lt v1, v0, :cond_3

    .line 1430
    :try_start_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 1432
    invoke-virtual {p1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :cond_3
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/ui/ImageCollectView;F)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetScale(F)V

    return-void
.end method

.method static synthetic access$1100(Lcom/epson/cameracopy/ui/ImageCollectView;)I
    .locals 0

    .line 37
    iget p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    return p0
.end method

.method static synthetic access$200(Lcom/epson/cameracopy/ui/ImageCollectView;Z)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->ResetScalePos(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/epson/cameracopy/ui/ImageCollectView;)Lcom/epson/cameracopy/ui/ImageCollectView$Data;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    return-object p0
.end method

.method private init(Landroid/content/Context;)V
    .locals 10

    .line 277
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mContext:Landroid/content/Context;

    .line 279
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 280
    move-object v1, p1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 282
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41800000    # 16.0f

    mul-float v1, v1, v0

    const/high16 v2, 0x40c00000    # 6.0f

    mul-float v2, v2, v0

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float v3, v3, v0

    const/high16 v4, 0x41a00000    # 20.0f

    mul-float v4, v4, v0

    .line 290
    new-instance v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    invoke-direct {v5, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$Data;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    .line 291
    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iput v4, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mStrokeWidth:F

    .line 292
    iput v1, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mRadiusCropCircle:F

    .line 293
    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointViewMargine:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    add-float/2addr v1, v2

    invoke-direct {v6, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v1, 0x0

    aput-object v6, v5, v1

    .line 294
    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointViewMargine:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v7, 0x1

    aput-object v6, v5, v7

    .line 295
    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointViewMargine:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v8, 0x2

    aput-object v6, v5, v8

    .line 296
    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointViewMargine:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v2, 0x3

    aput-object v6, v5, v2

    .line 298
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintView:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 299
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintView:Landroid/graphics/Paint;

    const v5, -0xff01

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 300
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintView:Landroid/graphics/Paint;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 302
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintImageArea:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 303
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintImageArea:Landroid/graphics/Paint;

    const/16 v6, -0x100

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 305
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintPreviewImage:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 306
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintPreviewImage:Landroid/graphics/Paint;

    const v6, -0xffff01

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 308
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 309
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    const v6, -0xff0100

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 310
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 312
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleSelect:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 313
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleSelect:Landroid/graphics/Paint;

    const/high16 v5, -0x10000

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 314
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleDeselected:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 315
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleDeselected:Landroid/graphics/Paint;

    const/16 v6, 0xff

    const/16 v8, 0x4c

    invoke-static {v8, v6, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 317
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOn:Landroid/graphics/Paint;

    sget-object v9, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 318
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOn:Landroid/graphics/Paint;

    invoke-static {v8, v1, v6, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 319
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOn:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 320
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOn:Landroid/graphics/Paint;

    sget-object v9, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 322
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOff:Landroid/graphics/Paint;

    sget-object v9, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 324
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOff:Landroid/graphics/Paint;

    invoke-static {v8, v1, v1, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 325
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOff:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 326
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOff:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 328
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragMask:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 329
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragMask:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 330
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragMask:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 331
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragMask:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 333
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 334
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 335
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 338
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropTapArea:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 339
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropTapArea:Landroid/graphics/Paint;

    const v2, -0x118112

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 340
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropTapArea:Landroid/graphics/Paint;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 342
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropMovableArea:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 343
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropMovableArea:Landroid/graphics/Paint;

    const v2, -0xff0001

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 345
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintTextMessage:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/high16 v1, 0x41200000    # 10.0f

    mul-float v0, v0, v1

    .line 347
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintTextMessage:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 348
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintTextMessage:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 349
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintTextMessage:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 351
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintMessageArea:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 352
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintMessageArea:Landroid/graphics/Paint;

    const v1, -0x33000001    # -1.3421772E8f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 354
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    new-instance v1, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V

    iput-object v1, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTimerCropImageCropCircle:Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;

    .line 356
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->onGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 357
    new-instance v0, Landroid/view/ScaleGestureDetector;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->onScaleGestureDetector:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 359
    new-instance p1, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

    invoke-direct {p1, p0, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView;Landroid/view/View;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mPathEffectData:Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

    return-void
.end method


# virtual methods
.method public GetBcscPointArray()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation

    .line 420
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/ImageCollectView;->ClontPointArray(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public GetBcscScale()F
    .locals 2

    .line 424
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public GetBcscStrokeWidth()F
    .locals 1

    .line 428
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mStrokeWidth:F

    return v0
.end method

.method public GetCropPoint()[Landroid/graphics/PointF;
    .locals 10

    const/4 v0, 0x4

    .line 500
    new-array v1, v0, [Landroid/graphics/PointF;

    .line 501
    new-array v2, v0, [Landroid/graphics/PointF;

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x1

    if-ge v4, v0, :cond_0

    .line 503
    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    aput-object v6, v2, v4

    .line 504
    aget-object v6, v2, v4

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v7, v7, v8

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v7, v7, v4

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    aget v8, v8, v5

    div-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 507
    aget-object v6, v2, v4

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v7, v7, v8

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v7, v7, v4

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v9, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    aget v5, v8, v5

    div-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/PointF;->y:F

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 514
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    .line 515
    aget-object v6, v2, v3

    iget v6, v6, Landroid/graphics/PointF;->x:F

    const/4 v7, 0x0

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1

    aget-object v6, v2, v3

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 516
    :cond_1
    aget-object v6, v2, v3

    iget v6, v6, Landroid/graphics/PointF;->y:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_2

    aget-object v6, v2, v3

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 517
    :cond_2
    aget-object v6, v2, v5

    iget v6, v6, Landroid/graphics/PointF;->x:F

    cmpl-float v6, v6, v0

    if-lez v6, :cond_3

    aget-object v6, v2, v5

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 518
    :cond_3
    aget-object v6, v2, v5

    iget v6, v6, Landroid/graphics/PointF;->y:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_4

    aget-object v6, v2, v5

    iput v7, v6, Landroid/graphics/PointF;->y:F

    :cond_4
    const/4 v6, 0x2

    .line 519
    aget-object v8, v2, v6

    iget v8, v8, Landroid/graphics/PointF;->x:F

    cmpl-float v8, v8, v0

    if-lez v8, :cond_5

    aget-object v8, v2, v6

    iput v0, v8, Landroid/graphics/PointF;->x:F

    .line 520
    :cond_5
    aget-object v0, v2, v6

    iget v0, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_6

    aget-object v0, v2, v6

    iput v4, v0, Landroid/graphics/PointF;->y:F

    :cond_6
    const/4 v0, 0x3

    .line 521
    aget-object v8, v2, v0

    iget v8, v8, Landroid/graphics/PointF;->x:F

    cmpg-float v8, v8, v7

    if-gez v8, :cond_7

    aget-object v8, v2, v0

    iput v7, v8, Landroid/graphics/PointF;->x:F

    .line 522
    :cond_7
    aget-object v7, v2, v0

    iget v7, v7, Landroid/graphics/PointF;->y:F

    cmpl-float v7, v7, v4

    if-lez v7, :cond_8

    aget-object v7, v2, v0

    iput v4, v7, Landroid/graphics/PointF;->y:F

    .line 524
    :cond_8
    aget-object v4, v2, v3

    aput-object v4, v1, v3

    .line 525
    aget-object v3, v2, v0

    aput-object v3, v1, v5

    .line 526
    aget-object v3, v2, v6

    aput-object v3, v1, v6

    .line 527
    aget-object v2, v2, v5

    aput-object v2, v1, v0

    return-object v1
.end method

.method public GetMaskImage()Landroid/graphics/Bitmap;
    .locals 1

    .line 432
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public MessageOff()V
    .locals 1

    .line 455
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTimerCropImageCropCircle:Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;

    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;->TimerOff()V

    return-void
.end method

.method public ReleaseBitmap()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x2

    const/4 v3, 0x0

    if-ge v1, v2, :cond_2

    const/4 v2, 0x0

    .line 475
    :goto_1
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    array-length v4, v4

    if-ge v2, v4, :cond_1

    .line 476
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    aget-object v4, v4, v2

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_0

    .line 477
    iget-object v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    aget-object v4, v4, v2

    iput-object v3, v4, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 483
    :cond_2
    iput-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    return-void
.end method

.method public ReleaseMask()V
    .locals 1

    .line 492
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    .line 494
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    .line 495
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mCanvasMask:Landroid/graphics/Canvas;

    :cond_0
    return-void
.end method

.method public ReleaseResource()V
    .locals 1

    .line 460
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->ReleaseBitmap()V

    .line 461
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->ReleaseMask()V

    .line 462
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mPathEffectData:Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;

    if-eqz v0, :cond_0

    .line 463
    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/ImageCollectView$PathEffectData;->Cleanup()V

    :cond_0
    return-void
.end method

.method public SetHandler(Landroid/os/Handler;)V
    .locals 0

    .line 387
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mHandlerAvtivity:Landroid/os/Handler;

    return-void
.end method

.method public SetMaskEdge(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;)V"
        }
    .end annotation

    .line 437
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 439
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 440
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 441
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 442
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 446
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    mul-float v4, v4, v5

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v7

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    aget v5, v5, v6

    mul-float v2, v2, v5

    invoke-direct {v3, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 448
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 451
    :cond_2
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->invalidate()V

    return-void
.end method

.method public SetMessage(Ljava/lang/String;)V
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTextMessage:Ljava/lang/String;

    return-void
.end method

.method public SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 406
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2}, Landroid/graphics/PointF;->set(FF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 408
    :cond_0
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public SetPreviewBitmapAndCropData(Landroid/graphics/Bitmap;[Landroid/graphics/PointF;)V
    .locals 4

    const/4 v0, 0x1

    .line 391
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGetCropPoint:Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    .line 392
    array-length v3, p2

    if-ne v3, v1, :cond_0

    .line 393
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    aget-object v1, v1, v2

    aget-object v2, p2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 394
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    aget-object v1, v1, v0

    const/4 v2, 0x3

    aget-object v3, p2, v2

    invoke-virtual {v1, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 395
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    const/4 v3, 0x2

    aget-object v1, v1, v3

    aget-object v3, p2, v3

    invoke-virtual {v1, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 396
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    aget-object v1, v1, v2

    aget-object p2, p2, v0

    invoke-virtual {v1, p2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_1

    :cond_0
    :goto_0
    if-ge v2, v1, :cond_1

    .line 399
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p2, p2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    aget-object p2, p2, v2

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v0}, Landroid/graphics/PointF;->set(FF)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 402
    :cond_1
    :goto_1
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->SetPreviewBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public SetViewMode(I)V
    .locals 2

    .line 363
    iput p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    .line 364
    iget p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 365
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    if-eqz p1, :cond_1

    .line 366
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 367
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 369
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v0

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 372
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_3

    .line 373
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_2

    .line 374
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 376
    :cond_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    .line 377
    new-instance p1, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mBitmapMask:Landroid/graphics/Bitmap;

    invoke-direct {p1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mCanvasMask:Landroid/graphics/Canvas;

    :cond_3
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 668
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 669
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    if-eqz v0, :cond_3

    .line 672
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetPreviewImage()V

    .line 675
    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    if-nez v0, :cond_0

    .line 676
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawCropImage(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 678
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawEnhanceText(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 680
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawColorAdjustmentAll(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 682
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawColorAdjustmentPart(Landroid/graphics/Canvas;)V

    :cond_3
    :goto_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 9

    .line 538
    iget-object p4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mContext:Landroid/content/Context;

    const/4 v0, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p4, :cond_2

    .line 539
    check-cast p4, Landroid/app/Activity;

    invoke-virtual {p4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p4

    invoke-interface {p4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p4

    invoke-virtual {p4}, Landroid/view/Display;->getRotation()I

    move-result p4

    if-eq p4, v2, :cond_1

    if-ne p4, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p4, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p4, 0x1

    goto :goto_1

    :cond_2
    const/4 p4, 0x0

    :goto_1
    if-eqz p3, :cond_3

    .line 550
    iget p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    if-eq p3, p4, :cond_3

    const/4 p3, 0x1

    goto :goto_2

    :cond_3
    const/4 p3, 0x0

    .line 554
    :goto_2
    iput p4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    .line 557
    iget-object p4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    if-eqz p4, :cond_d

    iget-object p4, p4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    if-eqz p4, :cond_d

    iget-object p4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p4, p4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p4, p4, v3

    iget-object p4, p4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    if-eqz p4, :cond_d

    if-nez p3, :cond_4

    .line 562
    iget-object p4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p4, p4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p4, p4, v3

    iget-object p4, p4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget p4, p4, Landroid/graphics/RectF;->right:F

    int-to-float v3, p1

    cmpl-float p4, p4, v3

    if-nez p4, :cond_4

    iget-object p4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p4, p4, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p4, p4, v3

    iget-object p4, p4, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget p4, p4, Landroid/graphics/RectF;->bottom:F

    int-to-float v3, p2

    cmpl-float p4, p4, v3

    if-nez p4, :cond_4

    const/4 p4, 0x0

    goto :goto_3

    :cond_4
    const/4 p4, 0x1

    .line 566
    :goto_3
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    int-to-float p1, p1

    int-to-float p2, p2

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v4, p1, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 571
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointViewMargine:[Landroid/graphics/PointF;

    iget p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, p2

    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 572
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p2, p2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointViewMargine:[Landroid/graphics/PointF;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p2, p2, v3

    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 573
    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectImageArea:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, p1

    iget-object v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, p2

    iget-object v7, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v7, v7, v8

    iget-object v7, v7, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, p1

    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v8, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v8

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mRectView:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr p1, p2

    invoke-virtual {v3, v5, v6, v7, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 580
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    if-eqz p3, :cond_5

    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget p2, p2, v3

    goto :goto_4

    :cond_5
    iget p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    :goto_4
    aget-object p1, p1, p2

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    aget p1, p1, v2

    .line 581
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/ImageCollectView;->GetPreviewImage()V

    .line 582
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p2, p2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p2, p2, v3

    iget-object p2, p2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mBaseScale:[F

    aget p2, p2, v2

    div-float/2addr p2, p1

    if-eqz p3, :cond_8

    .line 585
    iget p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    if-nez p1, :cond_7

    .line 587
    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mGetCropPoint:Z

    if-eqz p1, :cond_8

    .line 588
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v2, v2, v3

    aget-object p1, p1, v2

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object p1, p1, v0

    iget p1, p1, Landroid/graphics/PointF;->y:F

    cmpl-float p1, p1, v4

    if-eqz p1, :cond_8

    :goto_5
    const/4 p1, 0x4

    if-ge v1, p1, :cond_6

    .line 590
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v2

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v3, v3, v5

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v2, v2, v1

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v5, v5, v6

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v3

    iput v2, p1, Landroid/graphics/PointF;->x:F

    .line 594
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v2

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v3, v3, v5

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v2, v2, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    aget-object v2, v2, v1

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v5, v5, v6

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->mPointPreviewImageOffset:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v3

    iput v2, p1, Landroid/graphics/PointF;->y:F

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    .line 598
    :cond_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v1

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v2, v2, v3

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dcm:Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;

    const/4 v2, -0x1

    iput v2, v1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    iput v2, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    goto/16 :goto_6

    :cond_7
    if-ne p1, v2, :cond_8

    .line 603
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v5, v5, v6

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    aget v3, v3, v2

    aput v3, p1, v2

    .line 604
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v5, v5, v6

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mScale:[F

    aget v3, v3, v1

    aput v3, p1, v1

    .line 605
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v5, v5, v6

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/PointF;->x:F

    mul-float v3, v3, p2

    iput v3, p1, Landroid/graphics/PointF;->x:F

    .line 606
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v2

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v5, v5, v6

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/PointF;->y:F

    mul-float v3, v3, p2

    iput v3, p1, Landroid/graphics/PointF;->y:F

    .line 607
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v5, v5, v6

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v3, v3, v1

    iget v3, v3, Landroid/graphics/PointF;->x:F

    mul-float v3, v3, p2

    iput v3, p1, Landroid/graphics/PointF;->x:F

    .line 608
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object p1, p1, v3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object p1, p1, v1

    iget-object v3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v6, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget v5, v5, v6

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->ddp:[Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;

    iget v5, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/epson/cameracopy/ui/ImageCollectView$DataDrawPreview;->mPointMoveDeff:[Landroid/graphics/PointF;

    aget-object v1, v3, v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    mul-float v1, v1, p2

    iput v1, p1, Landroid/graphics/PointF;->y:F

    .line 612
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImageCalcMatrix(Landroid/graphics/Matrix;)V

    .line 615
    invoke-direct {p0, v2}, Lcom/epson/cameracopy/ui/ImageCollectView;->DrawPreviewImagePointCheck(Z)Z

    :cond_8
    :goto_6
    if-eqz p4, :cond_d

    cmpl-float p1, p2, v4

    if-eqz p1, :cond_d

    .line 621
    iget p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    if-ne p1, v0, :cond_d

    if-eqz p3, :cond_9

    .line 625
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, p3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p3, p3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object p4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget p4, p4, v0

    aget-object p3, p3, p4

    iget-object p3, p3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p3, p3, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    invoke-direct {p0, p3}, Lcom/epson/cameracopy/ui/ImageCollectView;->ClontPointArray(Ljava/util/List;)Ljava/util/List;

    move-result-object p3

    iput-object p3, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    .line 626
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, p3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p3, p3, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget-object p4, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->INVERT:[I

    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget p4, p4, v0

    aget-object p3, p3, p4

    iget-object p3, p3, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p3, p3, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    invoke-direct {p0, p3}, Lcom/epson/cameracopy/ui/ImageCollectView;->ClontPointArray(Ljava/util/List;)Ljava/util/List;

    move-result-object p3

    iput-object p3, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    .line 630
    :cond_9
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, p3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    if-eqz p1, :cond_b

    .line 631
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, p3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointArray:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_a
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    .line 632
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_7
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_a

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Landroid/graphics/PointF;

    .line 633
    iget v0, p4, Landroid/graphics/PointF;->x:F

    mul-float v0, v0, p2

    iput v0, p4, Landroid/graphics/PointF;->x:F

    .line 634
    iget v0, p4, Landroid/graphics/PointF;->y:F

    mul-float v0, v0, p2

    iput v0, p4, Landroid/graphics/PointF;->y:F

    goto :goto_7

    .line 638
    :cond_b
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, p3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    if-eqz p1, :cond_d

    .line 639
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mData:Lcom/epson/cameracopy/ui/ImageCollectView$Data;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iget p3, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mOrient:I

    aget-object p1, p1, p3

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;->dca:Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/ImageCollectView$DataColorAdjust;->mPointEdgeArray:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_c
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_d

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    .line 640
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_8
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_c

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Landroid/graphics/PointF;

    .line 641
    iget v0, p4, Landroid/graphics/PointF;->x:F

    mul-float v0, v0, p2

    iput v0, p4, Landroid/graphics/PointF;->x:F

    .line 642
    iget v0, p4, Landroid/graphics/PointF;->y:F

    mul-float v0, v0, p2

    iput v0, p4, Landroid/graphics/PointF;->y:F

    goto :goto_8

    :cond_d
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1258
    iget v0, p0, Lcom/epson/cameracopy/ui/ImageCollectView;->mViewMode:I

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 1259
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->TouchEventCropImage(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_0
    if-ne v0, v1, :cond_1

    .line 1261
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->TouchEventEnhanceText(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 1263
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->TouchEventColorAdjustmentAll(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 1265
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageCollectView;->TouchEventColorAdjustmentPart(Landroid/view/MotionEvent;)V

    :cond_3
    :goto_0
    return v1
.end method

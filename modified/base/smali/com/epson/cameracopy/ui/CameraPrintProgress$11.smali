.class Lcom/epson/cameracopy/ui/CameraPrintProgress$11;
.super Ljava/lang/Object;
.source "CameraPrintProgress.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CameraPrintProgress;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 887
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$11;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    const/4 p1, 0x0

    .line 893
    :try_start_0
    sget p2, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    const/16 v0, 0x6d

    const/4 v1, 0x1

    if-ne p2, v0, :cond_0

    .line 894
    sput-boolean v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isBkRetry:Z

    .line 895
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$11;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$200(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    .line 897
    :cond_0
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-interface {p2, v1}, Lepson/print/service/IEpsonService;->confirmContinueable(Z)I

    .line 898
    sput p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    .line 900
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$11;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p2, p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$302(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 902
    invoke-virtual {p2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 904
    :goto_0
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$11;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p2, p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$602(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 905
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$11;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p2, p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->removeDialog(I)V

    return-void
.end method

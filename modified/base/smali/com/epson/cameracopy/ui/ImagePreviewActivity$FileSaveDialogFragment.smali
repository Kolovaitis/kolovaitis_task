.class public Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "ImagePreviewActivity.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImagePreviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FileSaveDialogFragment"
.end annotation


# static fields
.field private static final PARAM_FILENAME:Ljava/lang/String; = "filename"

.field private static final PARAM_TYPE:Ljava/lang/String; = "type"


# instance fields
.field private mFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 664
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static newInstance(ILjava/lang/String;)Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;
    .locals 3

    .line 670
    new-instance v0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;

    invoke-direct {v0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;-><init>()V

    .line 672
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "type"

    .line 673
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "filename"

    .line 674
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    invoke-virtual {v0, v1}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 682
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 684
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "type"

    .line 685
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    const-string v0, "filename"

    .line 686
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;->mFileName:Ljava/lang/String;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .line 691
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 693
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;->mFileName:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f0e0394

    invoke-virtual {p1, v1, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 696
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 697
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment$1;-><init>(Lcom/epson/cameracopy/ui/ImagePreviewActivity$FileSaveDialogFragment;)V

    const v1, 0x7f0e03ff

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 705
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

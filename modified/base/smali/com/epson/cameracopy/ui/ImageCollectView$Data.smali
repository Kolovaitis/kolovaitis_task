.class Lcom/epson/cameracopy/ui/ImageCollectView$Data;
.super Ljava/lang/Object;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageCollectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Data"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;
    }
.end annotation


# instance fields
.field public d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

.field private mFlagScale:Z

.field public mPaintCropCircleDeselected:Landroid/graphics/Paint;

.field public mPaintCropCircleSelect:Landroid/graphics/Paint;

.field public mPaintCropLine:Landroid/graphics/Paint;

.field public mPaintCropMovableArea:Landroid/graphics/Paint;

.field public mPaintCropTapArea:Landroid/graphics/Paint;

.field public mPaintDragEdge:Landroid/graphics/Paint;

.field public mPaintDragMask:Landroid/graphics/Paint;

.field public mPaintDragOff:Landroid/graphics/Paint;

.field public mPaintDragOn:Landroid/graphics/Paint;

.field public mPaintImageArea:Landroid/graphics/Paint;

.field public mPaintMessageArea:Landroid/graphics/Paint;

.field public mPaintPreviewImage:Landroid/graphics/Paint;

.field public mPaintTextMessage:Landroid/graphics/Paint;

.field public mPaintView:Landroid/graphics/Paint;

.field public mPointCrop:[Landroid/graphics/PointF;

.field public mPointViewMargine:[Landroid/graphics/PointF;

.field public mRadiusCropCircle:F

.field public mShowTextMessage:Z

.field mStrokeWidth:F

.field public mTextMessage:Ljava/lang/String;

.field public mTimerCropImageCropCircle:Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;

.field public mTouchState:I

.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImageCollectView;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V
    .locals 5

    .line 202
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 175
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    .line 176
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    .line 177
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintView:Landroid/graphics/Paint;

    .line 178
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintPreviewImage:Landroid/graphics/Paint;

    .line 179
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintImageArea:Landroid/graphics/Paint;

    .line 180
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    .line 181
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleSelect:Landroid/graphics/Paint;

    .line 182
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleDeselected:Landroid/graphics/Paint;

    .line 183
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOn:Landroid/graphics/Paint;

    .line 184
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOff:Landroid/graphics/Paint;

    .line 185
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragMask:Landroid/graphics/Paint;

    .line 186
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    .line 187
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropTapArea:Landroid/graphics/Paint;

    .line 188
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropMovableArea:Landroid/graphics/Paint;

    .line 190
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointViewMargine:[Landroid/graphics/PointF;

    .line 191
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTimerCropImageCropCircle:Lcom/epson/cameracopy/ui/ImageCollectView$TimerCropImageCropCircle;

    .line 192
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTextMessage:Ljava/lang/String;

    .line 193
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintTextMessage:Landroid/graphics/Paint;

    .line 195
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintMessageArea:Landroid/graphics/Paint;

    const/4 p1, 0x0

    .line 198
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mFlagScale:Z

    const/4 v0, 0x2

    .line 203
    new-array v1, v0, [Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    .line 204
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    new-instance v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView$Data;)V

    aput-object v2, v1, p1

    .line 205
    iget-object v1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->d:[Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    new-instance v2, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/ImageCollectView$Data$D;-><init>(Lcom/epson/cameracopy/ui/ImageCollectView$Data;)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v1, 0x4

    .line 207
    new-array v2, v1, [Landroid/graphics/PointF;

    iput-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    .line 208
    iget-object v2, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    aput-object v4, v2, p1

    .line 209
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    aput-object v2, p1, v3

    .line 210
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    aput-object v2, p1, v0

    .line 211
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointCrop:[Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    const/4 v2, 0x3

    aput-object v0, p1, v2

    .line 212
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintView:Landroid/graphics/Paint;

    .line 213
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintPreviewImage:Landroid/graphics/Paint;

    .line 214
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintImageArea:Landroid/graphics/Paint;

    .line 215
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropLine:Landroid/graphics/Paint;

    .line 216
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleSelect:Landroid/graphics/Paint;

    .line 217
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropCircleDeselected:Landroid/graphics/Paint;

    .line 218
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOn:Landroid/graphics/Paint;

    .line 219
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragOff:Landroid/graphics/Paint;

    .line 220
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragMask:Landroid/graphics/Paint;

    .line 221
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintDragEdge:Landroid/graphics/Paint;

    .line 222
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropTapArea:Landroid/graphics/Paint;

    .line 223
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintCropMovableArea:Landroid/graphics/Paint;

    .line 224
    new-array p1, v1, [Landroid/graphics/PointF;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPointViewMargine:[Landroid/graphics/PointF;

    const-string p1, ""

    .line 225
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTextMessage:Ljava/lang/String;

    .line 226
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintTextMessage:Landroid/graphics/Paint;

    .line 227
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mPaintMessageArea:Landroid/graphics/Paint;

    const/4 p1, -0x1

    .line 228
    iput p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mTouchState:I

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/ImageCollectView$Data;)Z
    .locals 0

    .line 132
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mFlagScale:Z

    return p0
.end method

.method static synthetic access$002(Lcom/epson/cameracopy/ui/ImageCollectView$Data;Z)Z
    .locals 0

    .line 132
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$Data;->mFlagScale:Z

    return p1
.end method

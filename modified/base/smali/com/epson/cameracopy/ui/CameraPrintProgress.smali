.class public Lcom/epson/cameracopy/ui/CameraPrintProgress;
.super Landroid/app/Activity;
.source "CameraPrintProgress.java"

# interfaces
.implements Lepson/print/CommonDefine;


# static fields
.field private static final DIALOG_CONFIRM:I = 0x1

.field private static final DIALOG_INFORM:I = 0x0

.field public static final END_PRINT:I = 0x4

.field public static final KEY_IMAGE_AND_LAYOUT:Ljava/lang/String; = "image_and_layout"

.field public static final PARAM_PRINT_PROGRESS:Ljava/lang/String; = "progress-params"

.field public static final PREFS_NAME:Ljava/lang/String; = "PrintSetting"

.field public static final RESULT_ERROR:I = 0x3e8

.field private static bRestartactivity:Z = false

.field static copies:I = 0x0

.field static curError:I = 0x0

.field static fileIndex:I = 0x0

.field static isBkRetry:Z = false

.field static isContinue:Z = false

.field static isDocument:Z = false

.field private static final lockBCancel:Ljava/lang/Object;

.field private static mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private static mEpsonConnection:Landroid/content/ServiceConnection;

.field private static mEpsonService:Lepson/print/service/IEpsonService;

.field private static final mLock:Ljava/lang/Object;

.field private static final mLockInit:Ljava/lang/Object;

.field static mProgressCopies:Landroid/widget/ProgressBar;

.field static mProgressPage:Landroid/widget/ProgressBar;

.field static mProgressPercent:Landroid/widget/ProgressBar;


# instance fields
.field private final CHECK_PRINTER:I

.field private final CHECK_PRINTER_CONTINUE:I

.field private final CONFIRM:I

.field private final CONNECTED_SIMPLEAP:I

.field private final CONTINUE_PRINT:I

.field private final ECC_LIB_ERROR:I

.field private final EPS_DUPLEX_NONE:I

.field private final EPS_JOB_CANCELED:I

.field private final ERROR:I

.field private final FINISH:I

.field private final PERCENT:Ljava/lang/String;

.field private final PRINT_NEXT_PAGE:I

.field private final SHOW_COMPLETE_DIALOG:I

.field private final START_PRINT:I

.field private final UPDATE_PERCENT:I

.field bCancel:Z

.field private bSearching:Z

.field private bSearchingPrinter:Z

.field private context:Landroid/content/Context;

.field private curCopy:I

.field private curSheet:I

.field duplex:I

.field private isDialogOpen:Z

.field private isRemotePrinter:Z

.field mCancelButton:Landroid/widget/Button;

.field mCanceled:Z

.field mCopies:Landroid/widget/TextView;

.field private volatile mError:Z

.field mHandler:Landroid/os/Handler;

.field private mImageAndLayoutList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/cameracopy/printlayout/ImageAndLayout;",
            ">;"
        }
    .end annotation
.end field

.field mPage:Landroid/widget/TextView;

.field mPercent:Landroid/widget/TextView;

.field private volatile mWaitEpsonServiceForFinish:Z

.field private percentString:Ljava/lang/String;

.field private printerIp:Ljava/lang/String;

.field private sheets:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 86
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mLock:Ljava/lang/Object;

    .line 87
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mLockInit:Ljava/lang/Object;

    .line 88
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->lockBCancel:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 91
    sput-boolean v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bRestartactivity:Z

    .line 92
    sput-boolean v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isBkRetry:Z

    const/4 v1, 0x0

    .line 381
    sput-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 755
    sput v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 84
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bCancel:Z

    .line 85
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCanceled:Z

    .line 90
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bSearchingPrinter:Z

    .line 94
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->EPS_DUPLEX_NONE:I

    const/16 v1, 0x28

    .line 95
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->EPS_JOB_CANCELED:I

    .line 97
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->duplex:I

    .line 103
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isRemotePrinter:Z

    const-string v1, ""

    .line 104
    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->percentString:Ljava/lang/String;

    const/4 v1, 0x0

    .line 105
    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->context:Landroid/content/Context;

    .line 106
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bSearching:Z

    .line 114
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mError:Z

    .line 118
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mWaitEpsonServiceForFinish:Z

    const-string v1, "PERCENT"

    .line 431
    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->PERCENT:Ljava/lang/String;

    .line 432
    iput v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->UPDATE_PERCENT:I

    const/4 v1, 0x1

    .line 433
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->PRINT_NEXT_PAGE:I

    const/4 v2, 0x2

    .line 434
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->START_PRINT:I

    const/4 v2, 0x3

    .line 435
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->CONTINUE_PRINT:I

    const/4 v2, 0x4

    .line 436
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->ERROR:I

    const/4 v2, 0x5

    .line 437
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->FINISH:I

    const/4 v2, 0x6

    .line 438
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->CHECK_PRINTER:I

    const/4 v2, 0x7

    .line 439
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->CONFIRM:I

    const/16 v2, 0x8

    .line 440
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->SHOW_COMPLETE_DIALOG:I

    const/16 v2, 0x9

    .line 441
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->ECC_LIB_ERROR:I

    const/16 v2, 0xa

    .line 442
    iput v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->CHECK_PRINTER_CONTINUE:I

    .line 447
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->CONNECTED_SIMPLEAP:I

    .line 449
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curSheet:I

    .line 450
    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curCopy:I

    .line 455
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$4;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    .line 754
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isDialogOpen:Z

    return-void
.end method

.method private CancelPrint()V
    .locals 1

    .line 987
    :try_start_0
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 988
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelPrint()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 992
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 994
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->showDialog(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/lang/String;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->printerIp:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$002(Lcom/epson/cameracopy/ui/CameraPrintProgress;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->printerIp:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .line 60
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->CancelPrint()V

    return-void
.end method

.method static synthetic access$1100(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Landroid/content/Context;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->context:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I
    .locals 0

    .line 60
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curSheet:I

    return p0
.end method

.method static synthetic access$1202(Lcom/epson/cameracopy/ui/CameraPrintProgress;I)I
    .locals 0

    .line 60
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curSheet:I

    return p1
.end method

.method static synthetic access$1208(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I
    .locals 2

    .line 60
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curSheet:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curSheet:I

    return v0
.end method

.method static synthetic access$1300(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I
    .locals 0

    .line 60
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curCopy:I

    return p0
.end method

.method static synthetic access$1302(Lcom/epson/cameracopy/ui/CameraPrintProgress;I)I
    .locals 0

    .line 60
    iput p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curCopy:I

    return p1
.end method

.method static synthetic access$1308(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I
    .locals 2

    .line 60
    iget v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curCopy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curCopy:I

    return v0
.end method

.method static synthetic access$1400(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/lang/String;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->percentString:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)I
    .locals 0

    .line 60
    iget p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->sheets:I

    return p0
.end method

.method static synthetic access$1600(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Ljava/util/ArrayList;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mImageAndLayoutList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$1700()Z
    .locals 1

    .line 60
    sget-boolean v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bRestartactivity:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mWaitEpsonServiceForFinish:Z

    return p0
.end method

.method static synthetic access$1802(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z
    .locals 0

    .line 60
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mWaitEpsonServiceForFinish:Z

    return p1
.end method

.method static synthetic access$1900()Landroid/content/ServiceConnection;
    .locals 1

    .line 60
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$200(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->triggerPrint()V

    return-void
.end method

.method static synthetic access$2000(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->sendPrintLog()V

    return-void
.end method

.method static synthetic access$2100()Ljava/lang/Object;
    .locals 1

    .line 60
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->lockBCancel:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mError:Z

    return p0
.end method

.method static synthetic access$302(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z
    .locals 0

    .line 60
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mError:Z

    return p1
.end method

.method static synthetic access$400(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isRemotePrinter:Z

    return p0
.end method

.method static synthetic access$500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bSearchingPrinter:Z

    return p0
.end method

.method static synthetic access$502(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z
    .locals 0

    .line 60
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bSearchingPrinter:Z

    return p1
.end method

.method static synthetic access$600(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isDialogOpen:Z

    return p0
.end method

.method static synthetic access$602(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z
    .locals 0

    .line 60
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isDialogOpen:Z

    return p1
.end method

.method static synthetic access$700(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bSearching:Z

    return p0
.end method

.method static synthetic access$702(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z
    .locals 0

    .line 60
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bSearching:Z

    return p1
.end method

.method static synthetic access$800()Lepson/print/service/IEpsonService;
    .locals 1

    .line 60
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object v0
.end method

.method static synthetic access$802(Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 60
    sput-object p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$900()Lepson/print/service/IEpsonServiceCallback;
    .locals 1

    .line 60
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object v0
.end method

.method private init()V
    .locals 3

    .line 121
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mLockInit:Ljava/lang/Object;

    monitor-enter v0

    .line 122
    :try_start_0
    sget-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v1, :cond_0

    .line 123
    monitor-exit v0

    return-void

    .line 125
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_1

    const-string v0, "Epson"

    const-string v1, "mEpsonService = null"

    .line 127
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    sput-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    .line 239
    new-instance v0, Lcom/epson/cameracopy/ui/CameraPrintProgress$2;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$2;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    sput-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonConnection:Landroid/content/ServiceConnection;

    const-string v0, "Epson"

    const-string v1, "bindService() call"

    .line 274
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/service/EpsonService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    const-string v0, "Epson"

    const-string v1, "bindService() finish"

    .line 278
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    .line 125
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static isPrintSuccess(I)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private sendPrintLog()V
    .locals 1

    .line 1042
    new-instance v0, Lcom/epson/cameracopy/ui/CameraCopyProgressParams;

    invoke-direct {v0}, Lcom/epson/cameracopy/ui/CameraCopyProgressParams;-><init>()V

    invoke-static {p0, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendPrintLog(Landroid/content/Context;Lepson/print/screen/PrintProgress$ProgressParams;)V

    return-void
.end method

.method private triggerPrint()V
    .locals 2

    .line 386
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 963
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 974
    :pswitch_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 970
    :pswitch_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/16 p2, 0xa

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    const-string v0, "Epson"

    const-string v1, "onBackPressed()call"

    .line 1002
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const-string v0, "Epson"

    const-string v1, "PrintProgress.java: onCreate() call"

    .line 286
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Epson"

    const-string v1, "addFlags : FLAG_KEEP_SCREEN_ON "

    .line 288
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 291
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 293
    invoke-static {p0, p1}, Lepson/common/Utils;->setFInishOnTOuchOutside(Landroid/app/Activity;Z)V

    .line 294
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mError:Z

    .line 295
    sput p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    .line 296
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mWaitEpsonServiceForFinish:Z

    .line 298
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mLockInit:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    const-string v1, "Epson"

    const-string v2, "PrintProgress.java: onCreate() call init() funciton"

    .line 299
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-direct {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->init()V

    .line 301
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PRINT_DOCUMENT"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isDocument:Z

    const/4 v0, 0x1

    .line 304
    sput-boolean v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isDocument:Z

    .line 306
    new-instance v1, Lepson/print/screen/PrintSetting;

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v1, p0, v2}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 307
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 308
    iget v1, v1, Lepson/print/screen/PrintSetting;->copiesValue:I

    sput v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->copies:I

    .line 313
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "image_and_layout"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mImageAndLayoutList:Ljava/util/ArrayList;

    .line 314
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mImageAndLayoutList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->finish()V

    return-void

    .line 319
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->sheets:I

    .line 320
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isRemotePrinter:Z

    const v1, 0x7f0a00af

    .line 322
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->setContentView(I)V

    .line 323
    iput-object p0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->context:Landroid/content/Context;

    const v1, 0x7f08029d

    .line 325
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    sput-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    const v1, 0x7f08025b

    .line 326
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPercent:Landroid/widget/TextView;

    const v1, 0x7f08029a

    .line 327
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    sput-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    const v1, 0x7f0800e0

    .line 328
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCopies:Landroid/widget/TextView;

    const v1, 0x7f08023f

    .line 329
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPage:Landroid/widget/TextView;

    const v1, 0x7f08029c

    .line 330
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    sput-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    .line 332
    iget v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->sheets:I

    const/4 v2, 0x4

    if-le v1, v0, :cond_1

    .line 333
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPage:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0e0403

    invoke-virtual {p0, v4}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curSheet:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->sheets:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    sget-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curSheet:I

    mul-int/lit8 v3, v3, 0x64

    iget v4, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->sheets:I

    div-int/2addr v3, v4

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 336
    :cond_1
    sget-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 337
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPage:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 340
    :goto_0
    sget v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->copies:I

    if-le v1, v0, :cond_2

    iget-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isRemotePrinter:Z

    if-nez v1, :cond_2

    .line 341
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCopies:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0e030c

    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curCopy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lcom/epson/cameracopy/ui/CameraPrintProgress;->copies:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    sget-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curCopy:I

    mul-int/lit8 v2, v2, 0x64

    sget v3, Lcom/epson/cameracopy/ui/CameraPrintProgress;->copies:I

    div-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_1

    .line 344
    :cond_2
    sget-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 345
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCopies:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 346
    sput v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->copies:I

    :goto_1
    const v1, 0x7f0800b3

    .line 349
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCancelButton:Landroid/widget/Button;

    .line 350
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCancelButton:Landroid/widget/Button;

    new-instance v2, Lcom/epson/cameracopy/ui/CameraPrintProgress$3;

    invoke-direct {v2, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$3;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->bSearching:Z

    .line 361
    iget-boolean v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isRemotePrinter:Z

    if-ne v1, v0, :cond_3

    const v1, 0x7f0e0361

    .line 362
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->percentString:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const v1, 0x7f0e043f

    .line 364
    invoke-virtual {p0, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->percentString:Ljava/lang/String;

    .line 366
    :goto_2
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->percentString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "         0%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    sget-object v1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {v1, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    const p1, 0x7f080129

    .line 370
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 372
    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isRemotePrinter:Z

    if-ne p1, v0, :cond_4

    .line 373
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_3

    .line 376
    :cond_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_3
    const-string p1, "Epson"

    const-string v0, "printProgress.java: onCreate() finish"

    .line 378
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception p1

    .line 301
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    return-object v0

    .line 762
    :pswitch_0
    iget-boolean p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isDialogOpen:Z

    if-ne p1, v2, :cond_0

    return-object v0

    .line 766
    :cond_0
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 767
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 768
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04de

    .line 769
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e052b

    .line 770
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04e6

    .line 806
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPrintProgress$5;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$5;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 815
    new-instance v0, Lcom/epson/cameracopy/ui/CameraPrintProgress$7;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$7;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 826
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    :pswitch_1
    const-string p1, "Epson"

    .line 829
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "show dialog: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " cancontinue: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v3, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isContinue:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    invoke-virtual {p0, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->removeDialog(I)V

    .line 831
    sget p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    if-nez p1, :cond_1

    const/4 p1, 0x3

    .line 833
    new-array p1, p1, [Ljava/lang/Integer;

    const v0, 0x7f0e023a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v1

    const v0, 0x7f0e023b

    .line 834
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    const/4 v0, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v0

    .line 842
    :cond_1
    sget v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p1, v0, v3}, Lepson/common/Utils;->replaceMessage([Ljava/lang/Integer;ILandroid/content/Context;)[Ljava/lang/String;

    move-result-object p1

    .line 844
    sget-boolean v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isContinue:Z

    const v3, 0x7f0e0476

    if-nez v0, :cond_2

    const-string v0, "Epson"

    const-string v4, "show str_cancel button"

    .line 845
    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 847
    invoke-virtual {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    aget-object v2, p1, v2

    .line 848
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    aget-object p1, p1, v1

    .line 849
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 850
    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPrintProgress$8;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$8;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 870
    new-instance v0, Lcom/epson/cameracopy/ui/CameraPrintProgress$9;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$9;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 880
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    .line 882
    :cond_2
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    aget-object v2, p1, v2

    .line 883
    invoke-virtual {v0, v2}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    aget-object p1, p1, v1

    .line 884
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 885
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0482

    .line 886
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPrintProgress$11;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$11;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 913
    invoke-virtual {p0, v3}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/cameracopy/ui/CameraPrintProgress$10;

    invoke-direct {v1, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$10;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 942
    new-instance v0, Lcom/epson/cameracopy/ui/CameraPrintProgress$12;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$12;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 953
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .line 391
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "Epson"

    const-string v1, "PrintProgress.java: onDestroy()call"

    .line 392
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Epson"

    const-string v1, "clearFlags : FLAG_KEEP_SCREEN_ON "

    .line 394
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 397
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 398
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 399
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 400
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const-string v0, "Epson"

    const-string v1, "PrintProgress.java: onDestroy() finish"

    .line 402
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const-string v0, "Epson"

    const-string v1, "onKeyDown() call"

    .line 1009
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result p2

    if-nez p2, :cond_0

    const/16 p2, 0x54

    if-ne p1, p2, :cond_0

    const-string p1, "Epson"

    const-string p2, "onKeyDown() KEYCODE_SEARCH key press"

    .line 1012
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method protected onPause()V
    .locals 0

    .line 426
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 428
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 417
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "Epson"

    const-string v1, "PrintProgress.java call onResume()"

    .line 418
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 421
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 407
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 409
    sget-object v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_0

    const-string v0, "Epson"

    const-string v1, "set bRestartactivity = true (2)"

    .line 410
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1021
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 1022
    invoke-virtual {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1023
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1024
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const p2, 0x7f0e03ff

    .line 1025
    invoke-virtual {p0, p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Lcom/epson/cameracopy/ui/CameraPrintProgress$13;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/ui/CameraPrintProgress$13;-><init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    invoke-virtual {p1, p2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1033
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

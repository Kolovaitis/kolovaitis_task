.class Lcom/epson/cameracopy/ui/FolderInformation;
.super Ljava/lang/Object;
.source "FolderSelectActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/ui/FolderInformation$FileNameSort;,
        Lcom/epson/cameracopy/ui/FolderInformation$DirectoryFileFilter;
    }
.end annotation


# static fields
.field private static DIRECTION_CURRENT:I = 0x0

.field private static DIRECTION_FORWARD:I = 0x1

.field private static DIRECTION_REVERS:I = -0x1


# instance fields
.field private mCurrentFolder:Ljava/io/File;

.field private mCurrentFolderName:Lcom/epson/cameracopy/ui/CurrentFolderName;

.field private mFileList:[Ljava/io/File;

.field private mFolderArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

.field private mItemDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/cameracopy/ui/ItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mLevel:I

.field private mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

.field private mSelectedPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILcom/epson/cameracopy/ui/ItemDataAdapter;Ljava/util/ArrayList;Lcom/epson/cameracopy/ui/CurrentFolderName;Lcom/epson/cameracopy/ui/MenuControl;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/epson/cameracopy/ui/ItemDataAdapter;",
            "Ljava/util/ArrayList<",
            "Lcom/epson/cameracopy/ui/ItemData;",
            ">;",
            "Lcom/epson/cameracopy/ui/CurrentFolderName;",
            "Lcom/epson/cameracopy/ui/MenuControl;",
            ")V"
        }
    .end annotation

    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 421
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolder:Ljava/io/File;

    .line 422
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFileList:[Ljava/io/File;

    .line 423
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFolderArray:Ljava/util/ArrayList;

    const/4 v1, 0x0

    .line 424
    iput v1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    .line 425
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolderName:Lcom/epson/cameracopy/ui/CurrentFolderName;

    .line 426
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

    const/4 v1, -0x1

    .line 427
    iput v1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mSelectedPosition:I

    .line 429
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mItemDataList:Ljava/util/ArrayList;

    .line 430
    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

    .line 434
    invoke-direct/range {p0 .. p6}, Lcom/epson/cameracopy/ui/FolderInformation;->init(Ljava/lang/String;ILcom/epson/cameracopy/ui/ItemDataAdapter;Ljava/util/ArrayList;Lcom/epson/cameracopy/ui/CurrentFolderName;Lcom/epson/cameracopy/ui/MenuControl;)V

    return-void
.end method

.method private GetFolder(Ljava/io/File;)V
    .locals 3

    .line 501
    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mItemDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 502
    iput-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolder:Ljava/io/File;

    .line 503
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFolderArray:Ljava/util/ArrayList;

    .line 505
    iget p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 506
    new-instance p1, Lcom/epson/cameracopy/ui/ItemData;

    invoke-direct {p1, v0}, Lcom/epson/cameracopy/ui/ItemData;-><init>(Ljava/lang/String;)V

    .line 507
    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mItemDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 508
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFolderArray:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolder:Ljava/io/File;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolder:Ljava/io/File;

    new-instance v1, Lcom/epson/cameracopy/ui/FolderInformation$DirectoryFileFilter;

    invoke-direct {v1, p0, v0}, Lcom/epson/cameracopy/ui/FolderInformation$DirectoryFileFilter;-><init>(Lcom/epson/cameracopy/ui/FolderInformation;Lcom/epson/cameracopy/ui/FolderInformation$1;)V

    invoke-virtual {p1, v1}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFileList:[Ljava/io/File;

    .line 512
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFileList:[Ljava/io/File;

    new-instance v1, Lcom/epson/cameracopy/ui/FolderInformation$FileNameSort;

    invoke-direct {v1, p0, v0}, Lcom/epson/cameracopy/ui/FolderInformation$FileNameSort;-><init>(Lcom/epson/cameracopy/ui/FolderInformation;Lcom/epson/cameracopy/ui/FolderInformation$1;)V

    invoke-static {p1, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 513
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFileList:[Ljava/io/File;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 514
    :goto_0
    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFileList:[Ljava/io/File;

    array-length v2, v1

    if-ge p1, v2, :cond_1

    .line 515
    new-instance v2, Lcom/epson/cameracopy/ui/ItemData;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/epson/cameracopy/ui/ItemData;-><init>(Ljava/lang/String;)V

    .line 516
    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mItemDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    iget-object v1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFolderArray:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFileList:[Ljava/io/File;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 521
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolderName:Lcom/epson/cameracopy/ui/CurrentFolderName;

    const/4 v1, 0x1

    if-eqz p1, :cond_3

    .line 522
    iget-object v2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolder:Ljava/io/File;

    invoke-virtual {p1, v2}, Lcom/epson/cameracopy/ui/CurrentFolderName;->SetFile(Ljava/io/File;)V

    .line 523
    iget p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    if-eqz p1, :cond_2

    .line 524
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolderName:Lcom/epson/cameracopy/ui/CurrentFolderName;

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/CurrentFolderName;->Show(Z)V

    goto :goto_1

    .line 526
    :cond_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolderName:Lcom/epson/cameracopy/ui/CurrentFolderName;

    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/CurrentFolderName;->Show(Z)V

    .line 530
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

    if-eqz p1, :cond_5

    .line 531
    iget v2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    if-eqz v2, :cond_4

    .line 532
    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/MenuControl;->ChangeMenu(I)V

    goto :goto_2

    .line 534
    :cond_4
    invoke-virtual {p1, v0}, Lcom/epson/cameracopy/ui/MenuControl;->ChangeMenu(I)V

    :cond_5
    :goto_2
    return-void
.end method

.method private UpdateFolder(II)V
    .locals 1

    .line 489
    sget v0, Lcom/epson/cameracopy/ui/FolderInformation;->DIRECTION_REVERS:I

    if-ne p2, v0, :cond_0

    .line 490
    iget p2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    add-int/lit8 p2, p2, -0x1

    iput p2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    .line 491
    iget-object p2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFolderArray:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/FolderInformation;->GetFolder(Ljava/io/File;)V

    goto :goto_0

    .line 492
    :cond_0
    sget v0, Lcom/epson/cameracopy/ui/FolderInformation;->DIRECTION_FORWARD:I

    if-ne p2, v0, :cond_1

    .line 493
    iget p2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    add-int/lit8 p2, p2, 0x1

    iput p2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    .line 494
    iget-object p2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mFolderArray:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/FolderInformation;->GetFolder(Ljava/io/File;)V

    goto :goto_0

    .line 496
    :cond_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolder:Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/FolderInformation;->GetFolder(Ljava/io/File;)V

    :goto_0
    return-void
.end method

.method private init(Ljava/lang/String;ILcom/epson/cameracopy/ui/ItemDataAdapter;Ljava/util/ArrayList;Lcom/epson/cameracopy/ui/CurrentFolderName;Lcom/epson/cameracopy/ui/MenuControl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/epson/cameracopy/ui/ItemDataAdapter;",
            "Ljava/util/ArrayList<",
            "Lcom/epson/cameracopy/ui/ItemData;",
            ">;",
            "Lcom/epson/cameracopy/ui/CurrentFolderName;",
            "Lcom/epson/cameracopy/ui/MenuControl;",
            ")V"
        }
    .end annotation

    .line 439
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolder:Ljava/io/File;

    .line 440
    iput-object p4, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mItemDataList:Ljava/util/ArrayList;

    .line 441
    iput-object p3, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

    .line 442
    iput p2, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    .line 443
    iput-object p5, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolderName:Lcom/epson/cameracopy/ui/CurrentFolderName;

    .line 444
    iput-object p6, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mMenuControl:Lcom/epson/cameracopy/ui/MenuControl;

    return-void
.end method


# virtual methods
.method public GetCurrentFolder()Ljava/io/File;
    .locals 1

    .line 485
    iget-object v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mCurrentFolder:Ljava/io/File;

    return-object v0
.end method

.method public IsFolder()Z
    .locals 1

    .line 477
    iget v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mSelectedPosition:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public IsFolder(I)Z
    .locals 0

    if-nez p1, :cond_0

    .line 465
    iget p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public SetSelectedPosition(I)V
    .locals 0

    .line 473
    iput p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mSelectedPosition:I

    return-void
.end method

.method public UpdateFolder(I)I
    .locals 1

    .line 452
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/FolderInformation;->IsFolder(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    sget v0, Lcom/epson/cameracopy/ui/FolderInformation;->DIRECTION_FORWARD:I

    invoke-direct {p0, p1, v0}, Lcom/epson/cameracopy/ui/FolderInformation;->UpdateFolder(II)V

    goto :goto_0

    .line 455
    :cond_0
    sget v0, Lcom/epson/cameracopy/ui/FolderInformation;->DIRECTION_REVERS:I

    invoke-direct {p0, p1, v0}, Lcom/epson/cameracopy/ui/FolderInformation;->UpdateFolder(II)V

    .line 459
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mItemDataAdapter:Lcom/epson/cameracopy/ui/ItemDataAdapter;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/ItemDataAdapter;->notifyDataSetInvalidated()V

    .line 461
    iget p1, p0, Lcom/epson/cameracopy/ui/FolderInformation;->mLevel:I

    return p1
.end method

.method public UpdateFolder()V
    .locals 2

    .line 448
    sget v0, Lcom/epson/cameracopy/ui/FolderInformation;->DIRECTION_CURRENT:I

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/epson/cameracopy/ui/FolderInformation;->UpdateFolder(II)V

    return-void
.end method

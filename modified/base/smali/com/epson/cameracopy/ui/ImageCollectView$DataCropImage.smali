.class Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;
.super Ljava/lang/Object;
.source "ImageCollectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/ImageCollectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataCropImage"
.end annotation


# instance fields
.field public mPointPreviewCrop:[Landroid/graphics/PointF;

.field public mPointPreviewTouch:Landroid/graphics/PointF;

.field public mRectCropMovableArea:[Landroid/graphics/RectF;

.field public mRectCropTapArea:[Landroid/graphics/RectF;

.field public mSelectArea:I

.field final synthetic this$0:Lcom/epson/cameracopy/ui/ImageCollectView;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/ImageCollectView;)V
    .locals 6

    .line 67
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->this$0:Lcom/epson/cameracopy/ui/ImageCollectView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 60
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    .line 61
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    .line 63
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    .line 64
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewTouch:Landroid/graphics/PointF;

    const/4 p1, 0x5

    .line 68
    new-array p1, p1, [Landroid/graphics/RectF;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    .line 69
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v1, 0x0

    aput-object v0, p1, v1

    .line 70
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v2, 0x1

    aput-object v0, p1, v2

    .line 71
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v3, 0x2

    aput-object v0, p1, v3

    .line 72
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v4, 0x3

    aput-object v0, p1, v4

    .line 73
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropTapArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v5, 0x4

    aput-object v0, p1, v5

    .line 75
    new-array p1, v5, [Landroid/graphics/RectF;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    .line 76
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    aput-object v0, p1, v1

    .line 77
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    aput-object v0, p1, v2

    .line 78
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    aput-object v0, p1, v3

    .line 79
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mRectCropMovableArea:[Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    aput-object v0, p1, v4

    .line 81
    new-array p1, v5, [Landroid/graphics/PointF;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    .line 82
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    aput-object v0, p1, v1

    .line 83
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    aput-object v0, p1, v2

    .line 84
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    aput-object v0, p1, v3

    .line 85
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewCrop:[Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    aput-object v0, p1, v4

    .line 86
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mPointPreviewTouch:Landroid/graphics/PointF;

    const/4 p1, -0x1

    .line 88
    iput p1, p0, Lcom/epson/cameracopy/ui/ImageCollectView$DataCropImage;->mSelectArea:I

    return-void
.end method

.class Lcom/epson/cameracopy/ui/CameraFileAdapter;
.super Ljava/lang/Object;
.source "ImagePreviewActivity.java"

# interfaces
.implements Lcom/epson/cameracopy/ui/ImagePreviewFile;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mCameraFile:Lcom/epson/cameracopy/device/CameraFile;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/device/CameraFile;)V
    .locals 0

    .line 745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 746
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraFileAdapter;->mCameraFile:Lcom/epson/cameracopy/device/CameraFile;

    return-void
.end method


# virtual methods
.method public deleteTemporaryFile()V
    .locals 1

    .line 761
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraFileAdapter;->mCameraFile:Lcom/epson/cameracopy/device/CameraFile;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraFile;->deleteTemporaryFile()V

    return-void
.end method

.method public doSave(Ljava/lang/String;)Z
    .locals 1

    .line 771
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraFileAdapter;->mCameraFile:Lcom/epson/cameracopy/device/CameraFile;

    invoke-virtual {v0, p1}, Lcom/epson/cameracopy/device/CameraFile;->moveTo(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public getImageProcFileName(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    .line 751
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraFileAdapter;->mCameraFile:Lcom/epson/cameracopy/device/CameraFile;

    invoke-virtual {p1}, Lcom/epson/cameracopy/device/CameraFile;->getValidFileName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getPrintFileName()Ljava/lang/String;
    .locals 1

    .line 756
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraFileAdapter;->mCameraFile:Lcom/epson/cameracopy/device/CameraFile;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraFile;->getValidFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public needSaveButton()Z
    .locals 1

    .line 766
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraFileAdapter;->mCameraFile:Lcom/epson/cameracopy/device/CameraFile;

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraFile;->isMovable()Z

    move-result v0

    return v0
.end method

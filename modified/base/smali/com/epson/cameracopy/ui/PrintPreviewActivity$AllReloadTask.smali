.class public Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "PrintPreviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/PrintPreviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AllReloadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 1145
    iput-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .line 1155
    invoke-static {}, Ljava/lang/System;->gc()V

    const/4 p1, 0x0

    const/4 v0, 0x0

    :cond_0
    const/4 v1, 0x1

    .line 1160
    :try_start_0
    iget-object v2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {v2}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$900(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonService;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1161
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1162
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_1
    const-wide/16 v2, 0x64

    .line 1164
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    add-int/2addr v0, v1

    const/16 v2, 0x64

    if-lt v0, v2, :cond_0

    .line 1173
    :cond_2
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1174
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 1176
    :cond_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$900(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 1178
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$1000(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 1180
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$900(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v0}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->EpsonConnectUpdatePrinterSettings(Ljava/lang/String;)I

    goto :goto_0

    .line 1185
    :cond_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$900(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v0}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->updatePrinterSettings(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1193
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    .line 1191
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1196
    :cond_5
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1143
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    .line 1203
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$1100(Lcom/epson/cameracopy/ui/PrintPreviewActivity;Z)V

    .line 1205
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$200(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    .line 1208
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    iget-boolean p1, p1, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->bAutoStartPrint:Z

    if-eqz p1, :cond_0

    .line 1210
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$1200(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->onClick(Landroid/view/View;)V

    .line 1211
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    iput-boolean v0, p1, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->bAutoStartPrint:Z

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1143
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 1150
    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$AllReloadTask;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$100(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V

    return-void
.end method

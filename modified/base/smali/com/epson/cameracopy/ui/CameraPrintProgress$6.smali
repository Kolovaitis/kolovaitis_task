.class Lcom/epson/cameracopy/ui/CameraPrintProgress$6;
.super Ljava/lang/Object;
.source "CameraPrintProgress.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CameraPrintProgress;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 771
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .line 774
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    const/4 p1, 0x0

    :try_start_0
    const-string v0, "Epson"

    const-string v1, "user choice cancel print from GUI"

    .line 776
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 779
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-interface {v0, p2}, Lepson/print/service/IEpsonService;->confirmCancel(Z)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 782
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$2100()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "Epson"

    const-string v2, "===> set bCancel = true"

    .line 783
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    .line 788
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 790
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {v0, p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->removeDialog(I)V

    .line 792
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iput-boolean p2, v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCanceled:Z

    .line 793
    iget-object v0, v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 794
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object v0, v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 797
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$700(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result v0

    if-ne v0, p2, :cond_2

    .line 798
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p2, p2, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 799
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$6;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p2, p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$702(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    :cond_2
    return-void
.end method

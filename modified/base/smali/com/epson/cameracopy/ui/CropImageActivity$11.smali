.class Lcom/epson/cameracopy/ui/CropImageActivity$11;
.super Ljava/lang/Object;
.source "CropImageActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;->executeMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V
    .locals 0

    .line 1155
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 1179
    :pswitch_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {p1, v0}, Lepson/colorcorrection/ImageCollect;->SetSmallPreviewRate(F)V

    goto :goto_0

    .line 1176
    :pswitch_1
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const/high16 v0, 0x40600000    # 3.5f

    invoke-virtual {p1, v0}, Lepson/colorcorrection/ImageCollect;->SetSmallPreviewRate(F)V

    goto :goto_0

    .line 1173
    :pswitch_2
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const/high16 v0, 0x40400000    # 3.0f

    invoke-virtual {p1, v0}, Lepson/colorcorrection/ImageCollect;->SetSmallPreviewRate(F)V

    goto :goto_0

    .line 1170
    :pswitch_3
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const/high16 v0, 0x40200000    # 2.5f

    invoke-virtual {p1, v0}, Lepson/colorcorrection/ImageCollect;->SetSmallPreviewRate(F)V

    goto :goto_0

    .line 1167
    :pswitch_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p1, v0}, Lepson/colorcorrection/ImageCollect;->SetSmallPreviewRate(F)V

    goto :goto_0

    .line 1164
    :pswitch_5
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-virtual {p1, v0}, Lepson/colorcorrection/ImageCollect;->SetSmallPreviewRate(F)V

    goto :goto_0

    .line 1161
    :pswitch_6
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$400(Lcom/epson/cameracopy/ui/CropImageActivity;)Lepson/colorcorrection/ImageCollect;

    move-result-object p1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lepson/colorcorrection/ImageCollect;->SetSmallPreviewRate(F)V

    .line 1182
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$11;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CropImageActivity;->access$1302(Lcom/epson/cameracopy/ui/CropImageActivity;I)I

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

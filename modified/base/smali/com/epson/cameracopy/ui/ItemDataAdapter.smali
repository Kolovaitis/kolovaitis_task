.class Lcom/epson/cameracopy/ui/ItemDataAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FolderSelectActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/epson/cameracopy/ui/ItemData;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List<",
            "Lcom/epson/cameracopy/ui/ItemData;",
            ">;)V"
        }
    .end annotation

    .line 586
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 587
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ItemDataAdapter;->mContext:Landroid/content/Context;

    .line 588
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ItemDataAdapter;->mContext:Landroid/content/Context;

    const-string p2, "layout_inflater"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ItemDataAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 594
    iget-object p2, p0, Lcom/epson/cameracopy/ui/ItemDataAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v0, 0x7f0a0062

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 597
    :cond_0
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/ui/ItemDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/ui/ItemData;

    if-eqz v0, :cond_1

    .line 599
    invoke-virtual {v0, p1, p2, p3}, Lcom/epson/cameracopy/ui/ItemData;->DrawItem(ILandroid/view/View;Landroid/view/ViewGroup;)V

    :cond_1
    return-object p2
.end method

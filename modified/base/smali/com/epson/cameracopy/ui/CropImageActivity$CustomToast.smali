.class Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;
.super Ljava/lang/Object;
.source "CropImageActivity.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ShowToast"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/CropImageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomToast"
.end annotation


# instance fields
.field private mBreak:Z

.field private mToast:Landroid/widget/Toast;

.field final synthetic this$0:Lcom/epson/cameracopy/ui/CropImageActivity;


# direct methods
.method private constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V
    .locals 0

    .line 1849
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->this$0:Lcom/epson/cameracopy/ui/CropImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 1850
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mToast:Landroid/widget/Toast;

    const/4 p1, 0x0

    .line 1851
    iput-boolean p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mBreak:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/cameracopy/ui/CropImageActivity;Lcom/epson/cameracopy/ui/CropImageActivity$1;)V
    .locals 0

    .line 1849
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const-string v0, "cancel"

    .line 1897
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1898
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mBreak:Z

    .line 1899
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    return-void
.end method

.method public makeText(Landroid/content/Context;Ljava/lang/CharSequence;II)V
    .locals 2

    const/4 v0, 0x0

    .line 1855
    iput-boolean v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mBreak:Z

    const/4 v1, 0x1

    .line 1856
    invoke-static {p1, p2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mToast:Landroid/widget/Toast;

    .line 1859
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mToast:Landroid/widget/Toast;

    const/16 p2, 0x11

    invoke-virtual {p1, p2, p3, p4}, Landroid/widget/Toast;->setGravity(III)V

    .line 1862
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {p1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object p1

    const p2, -0xbbbbbc

    .line 1863
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1864
    instance-of p2, p1, Landroid/view/ViewGroup;

    if-eqz p2, :cond_1

    .line 1865
    move-object p2, p1

    check-cast p2, Landroid/view/ViewGroup;

    .line 1866
    :goto_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p3

    if-ge v0, p3, :cond_1

    .line 1867
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    .line 1868
    instance-of p4, p3, Landroid/widget/TextView;

    if-eqz p4, :cond_0

    .line 1869
    check-cast p3, Landroid/widget/TextView;

    const/4 p4, -0x1

    invoke-virtual {p3, p4}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1875
    :cond_1
    new-instance p2, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast$1;

    invoke-direct {p2, p0}, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast$1;-><init>(Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public show()V
    .locals 1

    const-string v0, "show :"

    .line 1887
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 1888
    iget-boolean v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mBreak:Z

    if-nez v0, :cond_0

    const-string v0, "run:show"

    .line 1889
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 1890
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CropImageActivity$CustomToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_0
    const-string v0, "run:break"

    .line 1892
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

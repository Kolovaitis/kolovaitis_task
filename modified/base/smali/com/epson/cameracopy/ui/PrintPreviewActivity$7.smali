.class Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;
.super Ljava/lang/Object;
.source "PrintPreviewActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/ui/PrintPreviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)V
    .locals 0

    .line 1218
    iput-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    .line 1233
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p2}, Lepson/print/service/IEpsonService$Stub;->asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$902(Lcom/epson/cameracopy/ui/PrintPreviewActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    .line 1235
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$900(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1237
    :try_start_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$900(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p2}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$1300(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object p2

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->registerCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1239
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .line 1223
    :try_start_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$900(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$1300(Lcom/epson/cameracopy/ui/PrintPreviewActivity;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1225
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1228
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/PrintPreviewActivity$7;->this$0:Lcom/epson/cameracopy/ui/PrintPreviewActivity;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/PrintPreviewActivity;->access$902(Lcom/epson/cameracopy/ui/PrintPreviewActivity;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    return-void
.end method

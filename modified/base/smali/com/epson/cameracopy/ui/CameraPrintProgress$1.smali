.class Lcom/epson/cameracopy/ui/CameraPrintProgress$1;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "CameraPrintProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/ui/CameraPrintProgress;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 145
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$002(Lcom/epson/cameracopy/ui/CameraPrintProgress;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "Epson"

    const-string v1, "onNotifyContinueable() CALL"

    .line 160
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$100()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 163
    :try_start_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 166
    sput-boolean v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isBkRetry:Z

    .line 167
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$200(Lcom/epson/cameracopy/ui/CameraPrintProgress;)V

    goto :goto_0

    :cond_0
    const/16 v1, 0x28

    if-ne p1, v1, :cond_1

    .line 170
    sput v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    .line 171
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    const-wide/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$302(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 174
    sput p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    .line 175
    sput-boolean v0, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isContinue:Z

    .line 176
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    .line 163
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public onNotifyEndJob(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "PrintProgress"

    const-string v1, "onNotifyEndJob() call"

    .line 223
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_1

    .line 226
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$400(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 227
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 229
    :cond_0
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 182
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNotifyError() call from printprogress.java: status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " err code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " continue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$400(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    if-eqz p2, :cond_0

    .line 186
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/16 p3, 0x9

    .line 187
    iput p3, p1, Landroid/os/Message;->what:I

    .line 188
    iput p2, p1, Landroid/os/Message;->arg1:I

    .line 189
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p2, p2, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p2, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    if-eqz p2, :cond_6

    .line 195
    invoke-static {}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$100()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    const/16 p1, 0x66

    const/16 v3, -0x547

    const/16 v4, -0x44c

    const/16 v5, -0x514

    if-eq v4, p2, :cond_2

    if-eq v5, p2, :cond_2

    if-eq v3, p2, :cond_2

    if-ne p1, p2, :cond_3

    .line 196
    :cond_2
    :try_start_0
    iget-object v6, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {v6}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result v6

    if-ne v6, v1, :cond_3

    const/16 p2, -0x32c9

    goto :goto_0

    :cond_3
    if-eq v4, p2, :cond_4

    if-eq v5, p2, :cond_4

    if-eq v3, p2, :cond_4

    if-ne p1, p2, :cond_5

    .line 198
    :cond_4
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$500(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result p1

    if-nez p1, :cond_5

    const/16 p2, -0x514

    .line 201
    :cond_5
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$502(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 203
    sput-boolean p3, Lcom/epson/cameracopy/ui/CameraPrintProgress;->isContinue:Z

    .line 204
    sput p2, Lcom/epson/cameracopy/ui/CameraPrintProgress;->curError:I

    .line 205
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p1, v1}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$302(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    .line 206
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 201
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 208
    :cond_6
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$600(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result p2

    if-eqz p2, :cond_7

    .line 209
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-virtual {p2, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->removeDialog(I)V

    .line 210
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p2, v0}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$602(Lcom/epson/cameracopy/ui/CameraPrintProgress;Z)Z

    :cond_7
    const/4 p2, -0x1

    if-ne p1, p2, :cond_8

    xor-int/lit8 p1, p3, 0x1

    .line 214
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    invoke-static {p2}, Lcom/epson/cameracopy/ui/CameraPrintProgress;->access$700(Lcom/epson/cameracopy/ui/CameraPrintProgress;)Z

    move-result p2

    xor-int/2addr p2, v1

    and-int/2addr p1, p2

    if-eqz p1, :cond_8

    .line 215
    iget-object p1, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p1, p1, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_8
    :goto_1
    return-void
.end method

.method public onNotifyProgress(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 150
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p1

    const/4 v0, 0x0

    .line 151
    iput v0, p1, Landroid/os/Message;->what:I

    .line 152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "PERCENT"

    .line 153
    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 154
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 155
    iget-object p2, p0, Lcom/epson/cameracopy/ui/CameraPrintProgress$1;->this$0:Lcom/epson/cameracopy/ui/CameraPrintProgress;

    iget-object p2, p2, Lcom/epson/cameracopy/ui/CameraPrintProgress;->mHandler:Landroid/os/Handler;

    const-wide/16 v0, 0x64

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

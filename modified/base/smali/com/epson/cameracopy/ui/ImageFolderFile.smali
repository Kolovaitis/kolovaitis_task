.class Lcom/epson/cameracopy/ui/ImageFolderFile;
.super Ljava/lang/Object;
.source "ImageFolderFile.java"

# interfaces
.implements Lcom/epson/cameracopy/ui/ImagePreviewFile;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mConvertFilename:Ljava/lang/String;

.field private mImageFolderFileName:Ljava/lang/String;

.field private mTemporaryFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mImageFolderFileName:Ljava/lang/String;

    return-void
.end method

.method private getHeifToJpegFile(Landroid/content/Context;)Ljava/io/File;
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 116
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lcom/epson/cameracopy/alt/FileUtils;->getCameracopyTempDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object p1

    const-string v1, "heif_to_jpeg.jpg"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private localDeleteFile(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    .line 79
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 81
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    return-void
.end method


# virtual methods
.method public deleteTemporaryFile()V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mTemporaryFileName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/epson/cameracopy/ui/ImageFolderFile;->localDeleteFile(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mTemporaryFileName:Ljava/lang/String;

    .line 70
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/epson/cameracopy/ui/ImageFolderFile;->getHeifToJpegFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/epson/cameracopy/ui/ImageFolderFile;->localDeleteFile(Ljava/lang/String;)V

    .line 71
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mConvertFilename:Ljava/lang/String;

    return-void
.end method

.method public doSave(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public getImageProcFileName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 47
    invoke-static {p1}, Lcom/epson/cameracopy/alt/FileUtils;->getNewTemporaryJpegFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageFolderFile;->getPrintFileName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/epson/cameracopy/alt/FileUtils;->copy(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    return-object p1

    .line 57
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mTemporaryFileName:Ljava/lang/String;

    .line 58
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mTemporaryFileName:Ljava/lang/String;

    return-object p1
.end method

.method public getPrintFileName()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mConvertFilename:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mImageFolderFileName:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public needSaveButton()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public needsConvert(Landroid/content/Context;)Z
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 111
    iget-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mImageFolderFileName:Ljava/lang/String;

    invoke-static {p1}, Lepson/common/ImageUtil;->isHEIFFormat(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method prepareImage(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 91
    iput-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mConvertFilename:Ljava/lang/String;

    .line 92
    invoke-virtual {p0}, Lcom/epson/cameracopy/ui/ImageFolderFile;->deleteTemporaryFile()V

    .line 94
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mImageFolderFileName:Ljava/lang/String;

    invoke-static {v0}, Lepson/common/ImageUtil;->isHEIFFormat(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/ui/ImageFolderFile;->getHeifToJpegFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    .line 96
    iget-object v0, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mImageFolderFileName:Ljava/lang/String;

    invoke-static {v0, p1}, Lepson/common/ImageUtil;->convertHEIFtoJPEG(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 99
    :cond_0
    iput-object p1, p0, Lcom/epson/cameracopy/ui/ImageFolderFile;->mConvertFilename:Ljava/lang/String;

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

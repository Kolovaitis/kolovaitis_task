.class public Lcom/epson/cameracopy/alt/UiCommon;
.super Ljava/lang/Object;
.source "UiCommon.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setButtonEnabled(Landroid/widget/Button;Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 28
    invoke-virtual {p0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 30
    invoke-virtual {p0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method public static setImageButtonEnabled(Landroid/widget/ImageButton;Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 41
    invoke-virtual {p0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 43
    invoke-virtual {p0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    :goto_0
    return-void
.end method

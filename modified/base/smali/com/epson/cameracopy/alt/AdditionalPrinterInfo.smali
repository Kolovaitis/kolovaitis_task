.class public Lcom/epson/cameracopy/alt/AdditionalPrinterInfo;
.super Ljava/lang/Object;
.source "AdditionalPrinterInfo.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentPrintModeResolution(Landroid/content/Context;)I
    .locals 1

    .line 18
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p0

    .line 19
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getLang()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    const/16 p0, 0x168

    return p0

    :cond_0
    const/16 p0, 0x12c

    return p0
.end method

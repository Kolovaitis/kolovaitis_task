.class public Lcom/epson/cameracopy/alt/SimpleBmpMerger;
.super Ljava/lang/Object;
.source "SimpleBmpMerger.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 2

    .line 98
    new-instance p0, Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    .line 99
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo0.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 100
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo1.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 101
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo2.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 102
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo3.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 103
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo4.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 104
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo5.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 105
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo6.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 106
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo7.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 107
    new-instance v0, Ljava/io/File;

    const-string v1, "c:/tmp/bo8.bmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    const-string v0, "c:/tmp/tbo-all.bmp"

    .line 109
    invoke-virtual {p0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/epson/cameracopy/alt/SimpleBmpMerger;->margeBmpFile(Ljava/lang/String;Ljava/util/Iterator;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 111
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "marge ok."

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_0
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "marge failed"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static margeBmpFile(Ljava/lang/String;Ljava/util/Iterator;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Iterator<",
            "Ljava/io/File;",
            ">;)Z"
        }
    .end annotation

    .line 31
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 32
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string p1, "no bmp file"

    invoke-virtual {p0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return v1

    .line 36
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 38
    new-instance v2, Lcom/epson/cameracopy/alt/BmpFileInfo;

    invoke-direct {v2}, Lcom/epson/cameracopy/alt/BmpFileInfo;-><init>()V

    .line 39
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/alt/BmpFileInfo;->readParams(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 40
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "> bmp format error"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return v1

    .line 45
    :cond_1
    invoke-virtual {v2}, Lcom/epson/cameracopy/alt/BmpFileInfo;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Lcom/epson/cameracopy/alt/BmpFileInfo;->getBitPerPixel()I

    move-result v3

    .line 44
    invoke-static {v0, v3, p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->createH0Instance(IILjava/lang/String;)Lcom/epson/cameracopy/alt/BmpFileInfo;

    move-result-object p0

    .line 49
    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->prepareAppendData()Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 52
    :cond_2
    invoke-virtual {v2, p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->transferImageData(Lcom/epson/cameracopy/alt/BmpFileInfo;)Z

    move-result v0

    if-nez v0, :cond_3

    return v1

    .line 58
    :cond_3
    :goto_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 59
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 60
    new-instance v2, Lcom/epson/cameracopy/alt/BmpFileInfo;

    invoke-direct {v2}, Lcom/epson/cameracopy/alt/BmpFileInfo;-><init>()V

    .line 61
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/epson/cameracopy/alt/BmpFileInfo;->readParams(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 62
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "> bmp format error"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->closeWriteableChannel()V

    return v1

    .line 66
    :cond_4
    :try_start_1
    invoke-virtual {v2}, Lcom/epson/cameracopy/alt/BmpFileInfo;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->getWidth()I

    move-result v4

    if-eq v3, v4, :cond_5

    .line 67
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "width not correspond"

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->closeWriteableChannel()V

    return v1

    .line 71
    :cond_5
    :try_start_2
    invoke-virtual {v2, p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->transferImageData(Lcom/epson/cameracopy/alt/BmpFileInfo;)Z

    move-result v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v2, :cond_6

    .line 83
    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->closeWriteableChannel()V

    return v1

    .line 76
    :cond_6
    :try_start_3
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 79
    :cond_7
    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->updateHeader()Z

    move-result p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez p1, :cond_8

    .line 83
    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->closeWriteableChannel()V

    return v1

    :cond_8
    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->closeWriteableChannel()V

    .line 86
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string p1, "convert done."

    invoke-virtual {p0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 p0, 0x1

    return p0

    :catchall_0
    move-exception p1

    .line 83
    invoke-virtual {p0}, Lcom/epson/cameracopy/alt/BmpFileInfo;->closeWriteableChannel()V

    throw p1
.end method

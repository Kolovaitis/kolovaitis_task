.class Lcom/epson/cameracopy/device/CameraSizeList$1;
.super Ljava/lang/Object;
.source "CameraSizeList.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/cameracopy/device/CameraSizeList;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Lcom/epson/cameracopy/device/SizeWrapper;",
        ">;"
    }
.end annotation


# instance fields
.field mIndex:I

.field final synthetic this$0:Lcom/epson/cameracopy/device/CameraSizeList;


# direct methods
.method constructor <init>(Lcom/epson/cameracopy/device/CameraSizeList;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->this$0:Lcom/epson/cameracopy/device/CameraSizeList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 49
    iput p1, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->mIndex:I

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .line 53
    iget v0, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->mIndex:I

    iget-object v1, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->this$0:Lcom/epson/cameracopy/device/CameraSizeList;

    iget-object v1, v1, Lcom/epson/cameracopy/device/CameraSizeList;->mCameraSizeList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public next()Lcom/epson/cameracopy/device/SizeWrapper;
    .locals 2

    .line 61
    iget v0, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->mIndex:I

    iget-object v1, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->this$0:Lcom/epson/cameracopy/device/CameraSizeList;

    iget-object v1, v1, Lcom/epson/cameracopy/device/CameraSizeList;->mCameraSizeList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->this$0:Lcom/epson/cameracopy/device/CameraSizeList;

    iget-object v0, v0, Lcom/epson/cameracopy/device/CameraSizeList;->mCameraSizeList:Ljava/util/List;

    iget v1, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->mIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 66
    iget v1, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->mIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/epson/cameracopy/device/CameraSizeList$1;->mIndex:I

    .line 67
    new-instance v1, Lcom/epson/cameracopy/device/CameraSizeList$CameraSizeWarpper;

    invoke-direct {v1, v0}, Lcom/epson/cameracopy/device/CameraSizeList$CameraSizeWarpper;-><init>(Landroid/hardware/Camera$Size;)V

    return-object v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 48
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/CameraSizeList$1;->next()Lcom/epson/cameracopy/device/SizeWrapper;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 0

    return-void
.end method

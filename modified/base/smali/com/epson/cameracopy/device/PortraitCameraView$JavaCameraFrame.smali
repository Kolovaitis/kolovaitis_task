.class Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;
.super Ljava/lang/Object;
.source "PortraitCameraView.java"

# interfaces
.implements Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewFrame;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/device/PortraitCameraView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "JavaCameraFrame"
.end annotation


# instance fields
.field private mHeight:I

.field private mRgba:Lorg/opencv/core/Mat;

.field private mRotated:Lorg/opencv/core/Mat;

.field private mWidth:I

.field private mYuvFrameData:Lorg/opencv/core/Mat;

.field final synthetic this$0:Lcom/epson/cameracopy/device/PortraitCameraView;


# direct methods
.method public constructor <init>(Lcom/epson/cameracopy/device/PortraitCameraView;Lorg/opencv/core/Mat;II)V
    .locals 0

    .line 810
    iput-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    .line 811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812
    iput p3, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mWidth:I

    .line 813
    iput p4, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mHeight:I

    .line 814
    iput-object p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mYuvFrameData:Lorg/opencv/core/Mat;

    .line 815
    new-instance p1, Lorg/opencv/core/Mat;

    invoke-direct {p1}, Lorg/opencv/core/Mat;-><init>()V

    iput-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRgba:Lorg/opencv/core/Mat;

    return-void
.end method


# virtual methods
.method public gray()Lorg/opencv/core/Mat;
    .locals 4

    .line 793
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mYuvFrameData:Lorg/opencv/core/Mat;

    iget v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mWidth:I

    iget v2, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mHeight:I

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v3, v2}, Lorg/opencv/core/Mat;->submat(IIII)Lorg/opencv/core/Mat;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    .line 795
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->t()Lorg/opencv/core/Mat;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    .line 796
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    const/4 v1, 0x1

    invoke-static {v0, v0, v1}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 797
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    return-object v0
.end method

.method public release()V
    .locals 1

    .line 819
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRgba:Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 820
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    :cond_0
    return-void
.end method

.method public rgba()Lorg/opencv/core/Mat;
    .locals 4

    .line 803
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mYuvFrameData:Lorg/opencv/core/Mat;

    iget-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRgba:Lorg/opencv/core/Mat;

    const/16 v2, 0x60

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V

    .line 804
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 805
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRgba:Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->t()Lorg/opencv/core/Mat;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    .line 806
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    const/4 v1, 0x1

    invoke-static {v0, v0, v1}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 807
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->mRotated:Lorg/opencv/core/Mat;

    return-object v0
.end method

.class public Lcom/epson/cameracopy/device/PaperContourDetector;
.super Ljava/lang/Object;
.source "PaperContourDetector.java"


# static fields
.field private static final DETECT_AREA_RATIO:D = 0.8


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static detectPaperContour(Lorg/opencv/core/Mat;)Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            ")",
            "Landroid/util/Pair<",
            "[",
            "Lorg/opencv/core/Point;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-static {p0}, Lcom/epson/cameracopy/device/PaperContourDetector;->findPaperContourCandidate(Lorg/opencv/core/Mat;)Ljava/util/List;

    move-result-object v0

    .line 42
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 45
    :cond_0
    invoke-static {v0}, Lcom/epson/cameracopy/device/PaperContourDetector;->findMax(Ljava/util/List;)Lorg/opencv/core/MatOfPoint;

    move-result-object v0

    .line 47
    invoke-static {p0, v0}, Lcom/epson/cameracopy/device/PaperContourDetector;->findPaperRect(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfPoint;)Landroid/util/Pair;

    move-result-object p0

    return-object p0
.end method

.method private static findMax(Ljava/util/List;)Lorg/opencv/core/MatOfPoint;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;)",
            "Lorg/opencv/core/MatOfPoint;"
        }
    .end annotation

    .line 80
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    .line 81
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 82
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/MatOfPoint;

    .line 83
    invoke-virtual {v3}, Lorg/opencv/core/MatOfPoint;->cols()I

    move-result v4

    invoke-virtual {v3}, Lorg/opencv/core/MatOfPoint;->rows()I

    move-result v5

    mul-int v4, v4, v5

    const/4 v5, 0x4

    if-lt v4, v5, :cond_0

    .line 84
    invoke-static {v3}, Lorg/opencv/imgproc/Imgproc;->contourArea(Lorg/opencv/core/Mat;)D

    move-result-wide v4

    cmpl-double v6, v4, v0

    if-lez v6, :cond_0

    move-object v2, v3

    move-wide v0, v4

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public static findPaperContourCandidate(Lorg/opencv/core/Mat;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            ")",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;"
        }
    .end annotation

    .line 57
    new-instance v6, Lorg/opencv/core/Mat;

    invoke-direct {v6}, Lorg/opencv/core/Mat;-><init>()V

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x405e000000000000L    # 120.0

    move-object v0, p0

    move-object v1, v6

    .line 58
    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->Canny(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DD)V

    .line 61
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    .line 62
    new-instance v4, Lorg/opencv/core/Point;

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    invoke-direct {v4, v0, v1, v0, v1}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 v2, 0x4

    const/4 v5, 0x1

    move-object v0, v6

    move-object v1, v6

    move-object v3, p0

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->morphologyEx(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Point;I)V

    .line 64
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->release()V

    .line 67
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 68
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    const/4 v1, 0x1

    .line 69
    invoke-static {v6, p0, v0, v1, v2}, Lorg/opencv/imgproc/Imgproc;->findContours(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Mat;II)V

    .line 71
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 72
    invoke-virtual {v6}, Lorg/opencv/core/Mat;->release()V

    return-object p0
.end method

.method private static findPaperRect(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfPoint;)Landroid/util/Pair;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Lorg/opencv/core/MatOfPoint;",
            ")",
            "Landroid/util/Pair<",
            "[",
            "Lorg/opencv/core/Point;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 106
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/MatOfPoint;->rows()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_f

    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/MatOfPoint;->cols()I

    move-result v1

    if-ge v1, v2, :cond_1

    goto/16 :goto_6

    .line 111
    :cond_1
    new-instance v1, Lorg/opencv/core/MatOfPoint2f;

    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/MatOfPoint;->toArray()[Lorg/opencv/core/Point;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    .line 112
    new-instance v3, Lorg/opencv/core/MatOfPoint2f;

    invoke-direct {v3}, Lorg/opencv/core/MatOfPoint2f;-><init>()V

    const/16 v4, 0x12c

    :goto_0
    const/16 v5, 0x64

    const/4 v6, 0x4

    if-lt v4, v5, :cond_3

    int-to-double v7, v4

    .line 114
    invoke-static {v1, v3, v7, v8, v2}, Lorg/opencv/imgproc/Imgproc;->approxPolyDP(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/MatOfPoint2f;DZ)V

    .line 115
    invoke-virtual {v3}, Lorg/opencv/core/MatOfPoint2f;->rows()I

    move-result v5

    invoke-virtual {v3}, Lorg/opencv/core/MatOfPoint2f;->cols()I

    move-result v7

    mul-int v5, v5, v7

    if-lt v5, v6, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, -0x64

    goto :goto_0

    .line 119
    :cond_3
    :goto_1
    invoke-virtual {v1}, Lorg/opencv/core/MatOfPoint2f;->release()V

    .line 122
    invoke-virtual {v3}, Lorg/opencv/core/MatOfPoint2f;->toArray()[Lorg/opencv/core/Point;

    move-result-object v1

    .line 123
    new-instance v4, Lorg/opencv/core/MatOfPoint;

    invoke-direct {v4, v1}, Lorg/opencv/core/MatOfPoint;-><init>([Lorg/opencv/core/Point;)V

    .line 124
    array-length v5, v1

    if-ne v5, v6, :cond_e

    .line 125
    invoke-virtual {v3}, Lorg/opencv/core/MatOfPoint2f;->release()V

    .line 126
    invoke-static {v4}, Lorg/opencv/imgproc/Imgproc;->isContourConvex(Lorg/opencv/core/MatOfPoint;)Z

    move-result v3

    .line 127
    invoke-virtual {v4}, Lorg/opencv/core/MatOfPoint;->release()V

    if-eqz v3, :cond_d

    .line 131
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    .line 132
    iget-wide v3, v0, Lorg/opencv/core/Size;->width:D

    .line 134
    iget-wide v5, v0, Lorg/opencv/core/Size;->height:D

    .line 136
    array-length v7, v1

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-wide v11, v5

    move-wide v13, v8

    move-wide v4, v3

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v7, :cond_9

    aget-object v6, v1, v3

    move/from16 p0, v3

    .line 137
    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    cmpg-double v17, v2, v15

    if-gtz v17, :cond_4

    iget-wide v2, v0, Lorg/opencv/core/Size;->height:D

    cmpg-double v17, v2, v15

    if-gtz v17, :cond_4

    const/4 v2, 0x0

    goto :goto_3

    .line 144
    :cond_4
    iget-wide v2, v6, Lorg/opencv/core/Point;->x:D

    cmpl-double v15, v4, v2

    if-lez v15, :cond_5

    .line 145
    iget-wide v2, v6, Lorg/opencv/core/Point;->x:D

    move-wide v4, v2

    .line 147
    :cond_5
    iget-wide v2, v6, Lorg/opencv/core/Point;->x:D

    cmpg-double v15, v8, v2

    if-gez v15, :cond_6

    .line 148
    iget-wide v2, v6, Lorg/opencv/core/Point;->x:D

    move-wide v8, v2

    .line 150
    :cond_6
    iget-wide v2, v6, Lorg/opencv/core/Point;->y:D

    cmpl-double v15, v11, v2

    if-lez v15, :cond_7

    .line 151
    iget-wide v2, v6, Lorg/opencv/core/Point;->y:D

    move-wide v11, v2

    .line 153
    :cond_7
    iget-wide v2, v6, Lorg/opencv/core/Point;->y:D

    cmpg-double v15, v13, v2

    if-gez v15, :cond_8

    .line 154
    iget-wide v2, v6, Lorg/opencv/core/Point;->y:D

    move-wide v13, v2

    :cond_8
    add-int/lit8 v3, p0, 0x1

    const/4 v2, 0x1

    goto :goto_2

    :cond_9
    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_c

    sub-double/2addr v8, v4

    .line 158
    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    div-double/2addr v8, v2

    const-wide v2, 0x3fe999999999999aL    # 0.8

    cmpl-double v4, v8, v2

    if-gez v4, :cond_b

    sub-double/2addr v13, v11

    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    div-double/2addr v13, v4

    cmpl-double v0, v13, v2

    if-ltz v0, :cond_a

    goto :goto_4

    :cond_a
    const/4 v2, 0x0

    goto :goto_5

    :cond_b
    :goto_4
    const/4 v2, 0x1

    .line 161
    :cond_c
    :goto_5
    new-instance v0, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_d
    return-object v0

    .line 164
    :cond_e
    array-length v1, v1

    return-object v0

    :cond_f
    :goto_6
    return-object v0
.end method

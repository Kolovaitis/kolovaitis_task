.class public Lcom/epson/cameracopy/device/CameraPreviewControl;
.super Ljava/lang/Object;
.source "CameraPreviewControl.java"


# static fields
.field private static DEFAULT_SAVE_DIRECTORY:Ljava/lang/String; = "/EpsonCameraCapture"
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation
.end field

.field private static final KEY_AUTO_PICTURE_MODE:Ljava/lang/String; = "auto_picture_mode"

.field private static final KEY_GUIDE_MODE:Ljava/lang/String; = "guide_mode"

.field private static final KEY_PICTURE_HEIGHT:Ljava/lang/String; = "picture_size_height"

.field private static final KEY_PICTURE_RESOLUTION_MODE:Ljava/lang/String; = "picture_resolution_mode"

.field private static final KEY_PICTURE_SIZE_LIST:Ljava/lang/String; = "picture_size_list"

.field private static final KEY_PICTURE_WIDTH:Ljava/lang/String; = "picture_size_width"

.field private static final KEY_SAVE_DIRECTORY:Ljava/lang/String; = "save_directory"

.field public static final PICTURE_RESOLUTION_MODE_AUTO:I = 0x0

.field public static final PICTURE_RESOLUTION_MODE_MANUAL:I = 0x1

.field private static final PREFS_NAME:Ljava/lang/String; = "camera_preview_option"

.field private static sCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;
    .locals 1

    .line 66
    sget-object v0, Lcom/epson/cameracopy/device/CameraPreviewControl;->sCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lcom/epson/cameracopy/device/CameraPreviewControl;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/device/CameraPreviewControl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/cameracopy/device/CameraPreviewControl;->sCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    .line 69
    :cond_0
    sget-object p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->sCameraPreviewControl:Lcom/epson/cameracopy/device/CameraPreviewControl;

    return-object p0
.end method

.method public static getPictureSizeString(II)Ljava/lang/String;
    .locals 7

    .line 325
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%4.1fM %dx%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    mul-int v3, p1, p0

    int-to-double v3, v3

    const-wide v5, 0x412e848000000000L    # 1000000.0

    div-double/2addr v3, v5

    .line 328
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 330
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 v3, 0x1

    aput-object p0, v2, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 p1, 0x2

    aput-object p0, v2, p1

    .line 325
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static hasBackCamera()Z
    .locals 5

    .line 81
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    .line 83
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    .line 85
    invoke-static {v3, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 87
    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method private setPictureResolutionMode(I)V
    .locals 3

    .line 244
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    .line 245
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 246
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "picture_resolution_mode"

    .line 247
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method public getAutoPictureMode()Z
    .locals 3

    .line 134
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "auto_picture_mode"

    .line 136
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getCameraPictureSizes()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "picture_size_list"

    const-string v3, ""

    .line 297
    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    .line 299
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 301
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 303
    array-length v3, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v0, v4

    const-string v6, ","

    .line 304
    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 305
    array-length v6, v5

    const/4 v7, 0x2

    if-lt v6, v7, :cond_0

    .line 306
    aget-object v6, v5, v2

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x1

    .line 307
    aget-object v5, v5, v7

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-lez v6, :cond_0

    if-lez v5, :cond_0

    .line 309
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7, v6, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 310
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getGuideMode()Z
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "guide_mode"

    const/4 v2, 0x1

    .line 117
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPictureResolutionMode()I
    .locals 3

    .line 228
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "picture_resolution_mode"

    .line 230
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getPictureSize()Landroid/graphics/Point;
    .locals 4

    .line 211
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 213
    iget-object v1, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v2, "camera_preview_option"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "picture_size_width"

    const/4 v3, -0x1

    .line 215
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Landroid/graphics/Point;->x:I

    const-string v2, "picture_size_height"

    .line 216
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    return-object v0
.end method

.method public getSaveDirecotry()Ljava/lang/String;
    .locals 4

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/epson/cameracopy/device/CameraPreviewControl;->DEFAULT_SAVE_DIRECTORY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v2, "camera_preview_option"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "save_directory"

    .line 155
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasAutoPictureHardware(Landroid/content/Context;)Z
    .locals 2

    .line 103
    invoke-static {}, Lcom/epson/cameracopy/device/CameraPreviewControl;->hasBackCamera()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 107
    :cond_0
    invoke-static {p1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->hasAptSensor(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public setAutoPictureMode(Ljava/lang/Boolean;)V
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    .line 143
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 144
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "auto_picture_mode"

    .line 145
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setCameraPictureSizes(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 269
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 272
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    .line 273
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_1

    const-string v2, ":"

    .line 274
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    :cond_1
    iget v2, v1, Landroid/hardware/Camera$Size;->width:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    .line 278
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget v1, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 282
    :cond_2
    iget-object p1, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    .line 283
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 284
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v1, "picture_size_list"

    .line 285
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setGuideMode(Z)V
    .locals 3

    .line 123
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    .line 124
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "guide_mode"

    .line 126
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setPictureResolutionModeAuto()V
    .locals 1

    const/4 v0, 0x0

    .line 255
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setPictureResolutionMode(I)V

    return-void
.end method

.method public setPictureSize(Landroid/graphics/Point;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    .line 195
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 196
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "picture_size_width"

    .line 198
    iget v2, p1, Landroid/graphics/Point;->x:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "picture_size_height"

    .line 199
    iget p1, p1, Landroid/graphics/Point;->y:I

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 201
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 p1, 0x1

    .line 203
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setPictureResolutionMode(I)V

    return-void
.end method

.method public setPictureSize(Landroid/hardware/Camera$Size;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 179
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    iget v1, p1, Landroid/hardware/Camera$Size;->width:I

    iget p1, p1, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v0, v1, p1}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->setPictureSize(Landroid/graphics/Point;)V

    return-void
.end method

.method public setSaveDirecotry(Ljava/lang/String;)V
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraPreviewControl;->mContext:Landroid/content/Context;

    const-string v1, "camera_preview_option"

    const/4 v2, 0x0

    .line 162
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 163
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "save_directory"

    .line 164
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

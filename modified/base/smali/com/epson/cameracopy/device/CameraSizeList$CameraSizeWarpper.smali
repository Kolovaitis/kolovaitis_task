.class public Lcom/epson/cameracopy/device/CameraSizeList$CameraSizeWarpper;
.super Ljava/lang/Object;
.source "CameraSizeList.java"

# interfaces
.implements Lcom/epson/cameracopy/device/SizeWrapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/device/CameraSizeList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CameraSizeWarpper"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field mCameraSize:Landroid/hardware/Camera$Size;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    const-class v0, Lcom/epson/cameracopy/device/CameraSizeList;

    return-void
.end method

.method public constructor <init>(Landroid/hardware/Camera$Size;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/epson/cameracopy/device/CameraSizeList$CameraSizeWarpper;->mCameraSize:Landroid/hardware/Camera$Size;

    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraSizeList$CameraSizeWarpper;->mCameraSize:Landroid/hardware/Camera$Size;

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraSizeList$CameraSizeWarpper;->mCameraSize:Landroid/hardware/Camera$Size;

    iget v0, v0, Landroid/hardware/Camera$Size;->width:I

    return v0
.end method

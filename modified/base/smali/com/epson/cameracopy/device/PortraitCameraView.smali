.class public Lcom/epson/cameracopy/device/PortraitCameraView;
.super Lorg/opencv/android/CameraBridgeViewBase;
.source "PortraitCameraView.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;,
        Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;,
        Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraSizeAccessor;
    }
.end annotation


# static fields
.field private static final FAVORITE_FOCUS_MODES:[Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field

.field private static final MAGIC_TEXTURE_ID:I = 0xa

.field private static final TAG:Ljava/lang/String; = "portCameraView"


# instance fields
.field private mBuffer:[B

.field protected mCamera:Landroid/hardware/Camera;

.field protected mCameraFrame:[Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

.field private mCameraFrameReady:Z

.field private mChainIdx:I

.field private mFrameChain:[Lorg/opencv/core/Mat;

.field private mStopThread:Z

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "continuous-video"

    const-string v1, "continuous-picture"

    const-string v2, "auto"

    .line 332
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/cameracopy/device/PortraitCameraView;->FAVORITE_FOCUS_MODES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .line 66
    invoke-direct {p0, p1, p2}, Lorg/opencv/android/CameraBridgeViewBase;-><init>(Landroid/content/Context;I)V

    const/4 p1, 0x0

    .line 43
    iput p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mChainIdx:I

    .line 726
    iput-boolean p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrameReady:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 70
    invoke-direct {p0, p1, p2}, Lorg/opencv/android/CameraBridgeViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 43
    iput p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mChainIdx:I

    .line 726
    iput-boolean p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrameReady:Z

    return-void
.end method

.method private OLDselectPreviewSizeFromPictureSize(DLjava/util/List;)Landroid/graphics/Point;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;)",
            "Landroid/graphics/Point;"
        }
    .end annotation

    if-eqz p3, :cond_9

    .line 373
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_9

    const-wide/16 v0, 0x0

    cmpg-double v2, p1, v0

    if-gtz v2, :cond_0

    goto/16 :goto_2

    .line 378
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 381
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-wide v3, 0x3fa999999999999aL    # 0.05

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    .line 382
    iget v5, v2, Landroid/hardware/Camera$Size;->height:I

    iget v6, v2, Landroid/hardware/Camera$Size;->width:I

    mul-int v5, v5, v6

    const v6, 0x12c00

    if-ge v5, v6, :cond_2

    goto :goto_0

    .line 386
    :cond_2
    iget v5, v2, Landroid/hardware/Camera$Size;->width:I

    int-to-double v5, v5

    iget v7, v2, Landroid/hardware/Camera$Size;->height:I

    int-to-double v7, v7

    div-double/2addr v5, v7

    sub-double/2addr v5, p1

    .line 388
    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    cmpg-double v7, v5, v3

    if-gez v7, :cond_1

    .line 389
    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    iget v4, v0, Landroid/graphics/Point;->x:I

    if-le v3, v4, :cond_1

    .line 390
    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    iput v3, v0, Landroid/graphics/Point;->x:I

    .line 391
    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    iput v2, v0, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 396
    :cond_3
    iget v1, v0, Landroid/graphics/Point;->x:I

    if-lez v1, :cond_4

    return-object v0

    :cond_4
    const/4 v1, 0x0

    .line 405
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    .line 406
    iget v5, v2, Landroid/hardware/Camera$Size;->width:I

    iput v5, v0, Landroid/graphics/Point;->x:I

    .line 407
    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    iput v2, v0, Landroid/graphics/Point;->y:I

    const-wide v5, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 411
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_5
    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    .line 412
    iget v7, v2, Landroid/hardware/Camera$Size;->height:I

    if-gtz v7, :cond_6

    goto :goto_1

    .line 415
    :cond_6
    iget v7, v2, Landroid/hardware/Camera$Size;->width:I

    int-to-double v7, v7

    iget v9, v2, Landroid/hardware/Camera$Size;->height:I

    int-to-double v9, v9

    div-double/2addr v7, v9

    sub-double/2addr v7, p1

    .line 416
    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v7

    add-double v9, v7, v3

    cmpl-double v11, v5, v9

    if-lez v11, :cond_7

    .line 419
    iget v1, v2, Landroid/hardware/Camera$Size;->width:I

    iget v5, v2, Landroid/hardware/Camera$Size;->height:I

    mul-int v1, v1, v5

    .line 420
    iget v5, v2, Landroid/hardware/Camera$Size;->width:I

    iput v5, v0, Landroid/graphics/Point;->x:I

    .line 421
    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    iput v2, v0, Landroid/graphics/Point;->y:I

    move-wide v5, v7

    goto :goto_1

    :cond_7
    sub-double v9, v7, v3

    cmpl-double v11, v5, v9

    if-lez v11, :cond_5

    .line 425
    iget v9, v2, Landroid/hardware/Camera$Size;->width:I

    iget v10, v2, Landroid/hardware/Camera$Size;->height:I

    mul-int v9, v9, v10

    if-ge v1, v9, :cond_5

    .line 429
    iget v1, v2, Landroid/hardware/Camera$Size;->width:I

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 430
    iget v1, v2, Landroid/hardware/Camera$Size;->height:I

    iput v1, v0, Landroid/graphics/Point;->y:I

    move-wide v5, v7

    move v1, v9

    goto :goto_1

    :cond_8
    return-object v0

    :cond_9
    :goto_2
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic access$100(Lcom/epson/cameracopy/device/PortraitCameraView;)Z
    .locals 0

    .line 36
    iget-boolean p0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrameReady:Z

    return p0
.end method

.method static synthetic access$102(Lcom/epson/cameracopy/device/PortraitCameraView;Z)Z
    .locals 0

    .line 36
    iput-boolean p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrameReady:Z

    return p1
.end method

.method static synthetic access$200(Lcom/epson/cameracopy/device/PortraitCameraView;)Z
    .locals 0

    .line 36
    iget-boolean p0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mStopThread:Z

    return p0
.end method

.method static synthetic access$300(Lcom/epson/cameracopy/device/PortraitCameraView;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mChainIdx:I

    return p0
.end method

.method static synthetic access$302(Lcom/epson/cameracopy/device/PortraitCameraView;I)I
    .locals 0

    .line 36
    iput p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mChainIdx:I

    return p1
.end method

.method static synthetic access$400(Lcom/epson/cameracopy/device/PortraitCameraView;)[Lorg/opencv/core/Mat;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    return-object p0
.end method

.method static synthetic access$500(Lcom/epson/cameracopy/device/PortraitCameraView;Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewFrame;)V
    .locals 0

    .line 36
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/device/PortraitCameraView;->deliverAndDrawFrame(Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewFrame;)V

    return-void
.end method

.method static getMaxIndex([D)I
    .locals 7

    if-eqz p0, :cond_3

    .line 606
    array-length v0, p0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 610
    aget-wide v1, p0, v0

    move-wide v2, v1

    const/4 v1, 0x0

    .line 612
    :goto_0
    array-length v4, p0

    if-ge v0, v4, :cond_2

    .line 613
    aget-wide v4, p0, v0

    cmpg-double v6, v2, v4

    if-gez v6, :cond_1

    move v1, v0

    move-wide v2, v4

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    :goto_1
    const/4 p0, -0x1

    return p0
.end method

.method static getRate(DD)D
    .locals 5

    const-wide/16 v0, 0x0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, p0, v0

    if-lez v4, :cond_2

    cmpg-double v4, p2, v0

    if-gtz v4, :cond_0

    goto :goto_0

    :cond_0
    div-double/2addr p2, p0

    cmpl-double p0, p2, v2

    if-lez p0, :cond_1

    div-double p2, v2, p2

    :cond_1
    sub-double/2addr p2, v2

    const-wide/high16 p0, 0x4024000000000000L    # 10.0

    mul-double p2, p2, p0

    const-wide/high16 p0, 0x4000000000000000L    # 2.0

    .line 643
    invoke-static {p2, p3, p0, p1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p0

    add-double/2addr p0, v2

    div-double/2addr v2, p0

    return-wide v2

    :cond_2
    :goto_0
    return-wide v2
.end method

.method private isInRange(DDD)Z
    .locals 3

    sub-double v0, p1, p3

    cmpg-double v2, v0, p5

    if-gtz v2, :cond_0

    add-double/2addr p1, p3

    cmpl-double p3, p1, p5

    if-ltz p3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private static openBackCamera()Landroid/hardware/Camera;
    .locals 10

    .line 294
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    .line 295
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, v3

    const/4 v4, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 297
    invoke-static {v2, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 298
    iget v6, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v6, :cond_0

    .line 300
    :try_start_0
    invoke-static {v2}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v6

    const-string v7, "portCameraView"

    .line 303
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Camera #"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, "failed to open: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/epson/cameracopy/device/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    if-eqz v4, :cond_0

    return-object v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method private selectPreviewSizeFromPictureSize(DLjava/util/List;)Lcom/epson/cameracopy/device/SizeWrapper;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;)",
            "Lcom/epson/cameracopy/device/SizeWrapper;"
        }
    .end annotation

    if-eqz p3, :cond_2

    const-wide/16 v0, 0x0

    cmpg-double v2, p1, v0

    if-gtz v2, :cond_0

    goto :goto_0

    .line 449
    :cond_0
    new-instance v0, Lcom/epson/cameracopy/device/CameraSizeList;

    invoke-direct {v0, p3}, Lcom/epson/cameracopy/device/CameraSizeList;-><init>(Ljava/util/List;)V

    .line 451
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->getHeight()I

    move-result p3

    .line 452
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->getWidth()I

    move-result v1

    .line 453
    invoke-static {p1, p2, v0, p3, v1}, Lcom/epson/cameracopy/device/PreviewResolutionSelector;->selectSize01(DLjava/lang/Iterable;II)Lcom/epson/cameracopy/device/SizeWrapper;

    move-result-object v2

    if-eqz v2, :cond_1

    return-object v2

    .line 460
    :cond_1
    invoke-static {p1, p2, v0, p3, v1}, Lcom/epson/cameracopy/device/PreviewResolutionSelector;->selectSize02(DLjava/lang/Iterable;II)Lcom/epson/cameracopy/device/SizeWrapper;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static selectSize(Landroid/graphics/Point;Ljava/util/List;)Landroid/graphics/Point;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Point;",
            "Ljava/util/List<",
            "Landroid/graphics/Point;",
            ">;)",
            "Landroid/graphics/Point;"
        }
    .end annotation

    .line 573
    iget v0, p0, Landroid/graphics/Point;->x:I

    int-to-double v0, v0

    iget p0, p0, Landroid/graphics/Point;->y:I

    int-to-double v2, p0

    div-double/2addr v0, v2

    const/4 p0, 0x0

    if-nez p1, :cond_0

    return-object p0

    .line 580
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [D

    const/4 v3, 0x0

    .line 583
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 584
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    .line 585
    iget v5, v4, Landroid/graphics/Point;->x:I

    int-to-double v5, v5

    iget v7, v4, Landroid/graphics/Point;->y:I

    int-to-double v7, v7

    div-double/2addr v5, v7

    invoke-static {v0, v1, v5, v6}, Lcom/epson/cameracopy/device/PortraitCameraView;->getRate(DD)D

    move-result-wide v5

    .line 587
    iget v7, v4, Landroid/graphics/Point;->x:I

    int-to-double v7, v7

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-double v9, v4

    mul-double v7, v7, v9

    mul-double v7, v7, v5

    .line 588
    aput-wide v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 591
    :cond_1
    invoke-static {v2}, Lcom/epson/cameracopy/device/PortraitCameraView;->getMaxIndex([D)I

    move-result v0

    if-ltz v0, :cond_3

    .line 592
    array-length v1, v2

    if-lt v0, v1, :cond_2

    goto :goto_1

    .line 596
    :cond_2
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/Point;

    return-object p0

    :cond_3
    :goto_1
    return-object p0
.end method

.method private setPictureAndPreviewResolutionManualMode(Landroid/hardware/Camera$Parameters;)Z
    .locals 8

    .line 474
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object v0

    .line 475
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getPictureResolutionMode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    return v2

    .line 479
    :cond_0
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getPictureSize()Landroid/graphics/Point;

    move-result-object v0

    .line 480
    iget v1, v0, Landroid/graphics/Point;->x:I

    if-lez v1, :cond_3

    iget v1, v0, Landroid/graphics/Point;->y:I

    if-gtz v1, :cond_1

    goto :goto_0

    .line 484
    :cond_1
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v1, v2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 486
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v1

    .line 487
    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-double v4, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-double v6, v0

    div-double/2addr v4, v6

    invoke-direct {p0, v4, v5, v1}, Lcom/epson/cameracopy/device/PortraitCameraView;->selectPreviewSizeFromPictureSize(DLjava/util/List;)Lcom/epson/cameracopy/device/SizeWrapper;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 490
    invoke-interface {v0}, Lcom/epson/cameracopy/device/SizeWrapper;->getWidth()I

    move-result v1

    invoke-interface {v0}, Lcom/epson/cameracopy/device/SizeWrapper;->getHeight()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    :cond_2
    return v3

    :cond_3
    :goto_0
    return v2
.end method

.method static setPictureSize(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Parameters;)V
    .locals 5

    .line 502
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v0

    .line 504
    iget v1, p0, Landroid/hardware/Camera$Size;->width:I

    int-to-double v1, v1

    iget v3, p0, Landroid/hardware/Camera$Size;->height:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    .line 505
    invoke-static {v0, v1, v2}, Lcom/epson/cameracopy/device/PortraitCameraView;->setPictureSize01(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 507
    iget p0, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p1, p0, v0}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    return-void

    .line 511
    :cond_0
    invoke-static {p0, p1}, Lcom/epson/cameracopy/device/PortraitCameraView;->setPictureSize02(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Parameters;)V

    return-void
.end method

.method static setPictureSize01(Ljava/util/List;D)Landroid/hardware/Camera$Size;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;D)",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .line 524
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 525
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 526
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    .line 527
    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    int-to-double v3, v3

    iget v5, v2, Landroid/hardware/Camera$Size;->height:I

    int-to-double v5, v5

    div-double/2addr v3, v5

    sub-double/2addr v3, p1

    .line 528
    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    const-wide v5, 0x3fa999999999999aL    # 0.05

    cmpg-double v7, v3, v5

    if-gez v7, :cond_0

    .line 530
    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    if-ge v0, v3, :cond_0

    .line 531
    iget v0, v2, Landroid/hardware/Camera$Size;->width:I

    move-object v1, v2

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method static setPictureSize02(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Parameters;)V
    .locals 5

    .line 545
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v0

    .line 546
    new-instance v1, Ljava/util/ArrayList;

    .line 547
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 549
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 550
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 551
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    .line 552
    new-instance v3, Landroid/graphics/Point;

    iget v4, v2, Landroid/hardware/Camera$Size;->width:I

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v3, v4, v2}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 555
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    iget v2, p0, Landroid/hardware/Camera$Size;->width:I

    iget p0, p0, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v0, v2, p0}, Landroid/graphics/Point;-><init>(II)V

    .line 558
    invoke-static {v0, v1}, Lcom/epson/cameracopy/device/PortraitCameraView;->selectSize(Landroid/graphics/Point;Ljava/util/List;)Landroid/graphics/Point;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 560
    iget v0, p0, Landroid/graphics/Point;->x:I

    iget p0, p0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0, p0}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected calculateCameraFrameSize(Ljava/util/List;Lorg/opencv/android/CameraBridgeViewBase$ListItemAccessor;II)Lorg/opencv/core/Size;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "*>;",
            "Lorg/opencv/android/CameraBridgeViewBase$ListItemAccessor;",
            "II)",
            "Lorg/opencv/core/Size;"
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v8, p2

    .line 87
    iget v0, v7, Lcom/epson/cameracopy/device/PortraitCameraView;->mMaxWidth:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, v7, Lcom/epson/cameracopy/device/PortraitCameraView;->mMaxWidth:I

    move/from16 v2, p3

    if-ge v0, v2, :cond_1

    iget v0, v7, Lcom/epson/cameracopy/device/PortraitCameraView;->mMaxWidth:I

    move v9, v0

    goto :goto_0

    :cond_0
    move/from16 v2, p3

    :cond_1
    move v9, v2

    .line 88
    :goto_0
    iget v0, v7, Lcom/epson/cameracopy/device/PortraitCameraView;->mMaxHeight:I

    if-eq v0, v1, :cond_2

    iget v0, v7, Lcom/epson/cameracopy/device/PortraitCameraView;->mMaxHeight:I

    move/from16 v1, p4

    if-ge v0, v1, :cond_3

    iget v0, v7, Lcom/epson/cameracopy/device/PortraitCameraView;->mMaxHeight:I

    move v10, v0

    goto :goto_1

    :cond_2
    move/from16 v1, p4

    :cond_3
    move v10, v1

    :goto_1
    const/4 v12, 0x0

    .line 95
    :goto_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v12, v0, :cond_6

    move-object/from16 v13, p1

    .line 96
    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 97
    invoke-interface {v8, v0}, Lorg/opencv/android/CameraBridgeViewBase$ListItemAccessor;->getWidth(Ljava/lang/Object;)I

    move-result v1

    .line 98
    invoke-interface {v8, v0}, Lorg/opencv/android/CameraBridgeViewBase$ListItemAccessor;->getHeight(Ljava/lang/Object;)I

    move-result v0

    const/16 v2, 0x28a

    if-ge v1, v2, :cond_4

    const/16 v2, 0x12c

    if-lt v1, v2, :cond_4

    const-wide v2, 0x3ff5555555555555L    # 1.3333333333333333

    const-wide v4, 0x3fa47ae147ae147bL    # 0.04

    int-to-double v14, v1

    int-to-double v0, v0

    div-double v16, v14, v0

    move/from16 p4, v12

    move-wide v11, v0

    move-object/from16 v0, p0

    move-wide v1, v2

    move-wide v3, v4

    move-wide/from16 v5, v16

    .line 102
    invoke-direct/range {v0 .. v6}, Lcom/epson/cameracopy/device/PortraitCameraView;->isInRange(DDD)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 103
    new-instance v0, Lorg/opencv/core/Size;

    invoke-direct {v0, v14, v15, v11, v12}, Lorg/opencv/core/Size;-><init>(DD)V

    return-object v0

    :cond_4
    move/from16 p4, v12

    :cond_5
    add-int/lit8 v12, p4, 0x1

    goto :goto_2

    :cond_6
    move-object/from16 v13, p1

    .line 108
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :cond_7
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 109
    invoke-interface {v8, v3}, Lorg/opencv/android/CameraBridgeViewBase$ListItemAccessor;->getWidth(Ljava/lang/Object;)I

    move-result v4

    .line 110
    invoke-interface {v8, v3}, Lorg/opencv/android/CameraBridgeViewBase$ListItemAccessor;->getHeight(Ljava/lang/Object;)I

    move-result v3

    if-gt v4, v9, :cond_7

    if-gt v3, v10, :cond_7

    if-lt v4, v1, :cond_7

    if-lt v3, v2, :cond_7

    move v2, v3

    move v1, v4

    goto :goto_3

    .line 120
    :cond_8
    new-instance v0, Lorg/opencv/core/Size;

    int-to-double v3, v1

    int-to-double v1, v2

    invoke-direct {v0, v3, v4, v1, v2}, Lorg/opencv/core/Size;-><init>(DD)V

    return-object v0
.end method

.method public checkBackCamera()Z
    .locals 1

    .line 319
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->disconnectCamera()V

    .line 320
    invoke-static {}, Lcom/epson/cameracopy/device/PortraitCameraView;->openBackCamera()Landroid/hardware/Camera;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 325
    :cond_0
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const/4 v0, 0x1

    return v0
.end method

.method protected connectCamera(II)Z
    .locals 2

    const-string v0, "portCameraView"

    const-string v1, "Connecting to camera"

    .line 735
    invoke-static {v0, v1}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    invoke-virtual {p0, p1, p2}, Lcom/epson/cameracopy/device/PortraitCameraView;->initializeCamera(II)Z

    move-result p1

    const/4 p2, 0x0

    if-nez p1, :cond_0

    return p2

    .line 739
    :cond_0
    iput-boolean p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrameReady:Z

    const-string p1, "portCameraView"

    const-string v0, "Starting processing thread"

    .line 742
    invoke-static {p1, v0}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    iput-boolean p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mStopThread:Z

    .line 744
    new-instance p1, Ljava/lang/Thread;

    new-instance p2, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;

    const/4 v0, 0x0

    invoke-direct {p2, p0, v0}, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;-><init>(Lcom/epson/cameracopy/device/PortraitCameraView;Lcom/epson/cameracopy/device/PortraitCameraView$1;)V

    const-string v0, "CameraWorker"

    invoke-direct {p1, p2, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mThread:Ljava/lang/Thread;

    .line 745
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mThread:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    const/4 p1, 0x1

    return p1
.end method

.method protected disconnectCamera()V
    .locals 3

    const-string v0, "portCameraView"

    const-string v1, "Disconnecting from camera"

    .line 755
    invoke-static {v0, v1}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 757
    :try_start_0
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mStopThread:Z

    const-string v0, "portCameraView"

    const-string v2, "Notify thread"

    .line 758
    invoke-static {v0, v2}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 760
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 761
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v0, "portCameraView"

    const-string v2, "Wating for thread"

    .line 762
    invoke-static {v0, v2}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 761
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 766
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 768
    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mThread:Ljava/lang/Thread;

    .line 772
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->releaseCamera()V

    const/4 v0, 0x0

    .line 774
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrameReady:Z

    return-void

    .line 768
    :goto_1
    iput-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mThread:Ljava/lang/Thread;

    throw v0
.end method

.method public getCameraPictureSize()Landroid/hardware/Camera$Size;
    .locals 2

    .line 660
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 665
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 667
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-object v1

    :catch_0
    return-object v1
.end method

.method public getPictureSizeList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation

    .line 686
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 692
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    .line 696
    :cond_1
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v1
.end method

.method getScreenSize()Landroid/graphics/Point;
    .locals 1

    .line 180
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 181
    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 182
    invoke-static {v0}, Lepson/common/ImageUtil;->getDisplaySize(Landroid/view/Display;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method protected initializeCamera(II)Z
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const-string v0, "portCameraView"

    const-string v1, "Initialize java camera"

    .line 190
    invoke-static {v0, v1}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    monitor-enter p0

    .line 193
    :try_start_0
    invoke-static {}, Lcom/epson/cameracopy/device/PortraitCameraView;->openBackCamera()Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    .line 195
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v1

    .line 199
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    const-string v2, "portCameraView"

    const-string v3, "getSupportedPreviewSizes()"

    .line 200
    invoke-static {v2, v3}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_5

    const/16 v4, 0x11

    .line 204
    invoke-virtual {v0, v4}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    .line 208
    invoke-direct {p0, v0}, Lcom/epson/cameracopy/device/PortraitCameraView;->setPictureAndPreviewResolutionManualMode(Landroid/hardware/Camera$Parameters;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 209
    invoke-virtual {p0, v2, v0}, Lcom/epson/cameracopy/device/PortraitCameraView;->setPreviewSize(Ljava/util/List;Landroid/hardware/Camera$Parameters;)Z

    .line 212
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/epson/cameracopy/device/PortraitCameraView;->setPictureSize(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Parameters;)V

    :cond_1
    const/16 v2, 0x5f

    .line 223
    invoke-virtual {v0, v2}, Landroid/hardware/Camera$Parameters;->setJpegQuality(I)V

    .line 226
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 230
    invoke-virtual {p0, v0}, Lcom/epson/cameracopy/device/PortraitCameraView;->setForcusMode(Landroid/hardware/Camera$Parameters;)V

    const/16 v2, 0x5a

    .line 233
    invoke-virtual {v0, v2}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    .line 235
    iget-object v2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 236
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    iput v2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameWidth:I

    .line 239
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    iget v2, v2, Landroid/hardware/Camera$Size;->width:I

    iput v2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameHeight:I

    .line 241
    iget v2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameHeight:I

    .line 242
    iget v4, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameWidth:I

    .line 244
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v5, v6, :cond_2

    int-to-float p2, p2

    .line 245
    iget v5, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameHeight:I

    int-to-float v5, v5

    div-float/2addr p2, v5

    int-to-float p1, p1

    iget v5, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameWidth:I

    int-to-float v5, v5

    div-float/2addr p1, v5

    invoke-static {p2, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    iput p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mScale:F

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 247
    iput p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mScale:F

    .line 249
    :goto_0
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFpsMeter:Lorg/opencv/android/FpsMeter;

    if-eqz p1, :cond_3

    .line 250
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFpsMeter:Lorg/opencv/android/FpsMeter;

    iget p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameWidth:I

    iget v5, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameHeight:I

    invoke-virtual {p1, p2, v5}, Lorg/opencv/android/FpsMeter;->setResolution(II)V

    .line 253
    :cond_3
    iget p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameWidth:I

    iget p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameHeight:I

    mul-int p1, p1, p2

    .line 254
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result p2

    invoke-static {p2}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result p2

    mul-int p1, p1, p2

    div-int/lit8 p1, p1, 0x8

    .line 255
    new-array p1, p1, [B

    iput-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mBuffer:[B

    .line 257
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    iget-object p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mBuffer:[B

    invoke-virtual {p1, p2}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 258
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p1, p0}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    const/4 p1, 0x2

    .line 260
    new-array p2, p1, [Lorg/opencv/core/Mat;

    iput-object p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    .line 261
    iget-object p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    new-instance v0, Lorg/opencv/core/Mat;

    div-int/lit8 v5, v4, 0x2

    add-int/2addr v5, v4

    sget v6, Lorg/opencv/core/CvType;->CV_8UC1:I

    invoke-direct {v0, v5, v2, v6}, Lorg/opencv/core/Mat;-><init>(III)V

    aput-object v0, p2, v1

    .line 262
    iget-object p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    new-instance v0, Lorg/opencv/core/Mat;

    div-int/lit8 v5, v4, 0x2

    add-int/2addr v4, v5

    sget v5, Lorg/opencv/core/CvType;->CV_8UC1:I

    invoke-direct {v0, v4, v2, v5}, Lorg/opencv/core/Mat;-><init>(III)V

    aput-object v0, p2, v3

    .line 264
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->AllocateCache()V

    .line 266
    new-array p1, p1, [Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    iput-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrame:[Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    .line 267
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrame:[Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    new-instance p2, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v1

    iget v2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameWidth:I

    iget v4, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameHeight:I

    invoke-direct {p2, p0, v0, v2, v4}, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;-><init>(Lcom/epson/cameracopy/device/PortraitCameraView;Lorg/opencv/core/Mat;II)V

    aput-object p2, p1, v1

    .line 268
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrame:[Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    new-instance p2, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v3

    iget v2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameWidth:I

    iget v4, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameHeight:I

    invoke-direct {p2, p0, v0, v2, v4}, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;-><init>(Lcom/epson/cameracopy/device/PortraitCameraView;Lorg/opencv/core/Mat;II)V

    aput-object p2, p1, v3

    .line 270
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0xb

    if-lt p1, p2, :cond_4

    .line 271
    new-instance p1, Landroid/graphics/SurfaceTexture;

    const/16 p2, 0xa

    invoke-direct {p1, p2}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 272
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    iget-object p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p1, p2}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    goto :goto_1

    .line 274
    :cond_4
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    :goto_1
    const-string p1, "portCameraView"

    const-string p2, "startPreview"

    .line 277
    invoke-static {p1, p2}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p1}, Landroid/hardware/Camera;->startPreview()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 284
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 286
    :cond_5
    :goto_2
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 1

    .line 780
    monitor-enter p0

    .line 781
    :try_start_0
    iget-object p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    iget v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mChainIdx:I

    aget-object p2, p2, v0

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v0, p1}, Lorg/opencv/core/Mat;->put(II[B)I

    const/4 p1, 0x1

    .line 782
    iput-boolean p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrameReady:Z

    .line 783
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 784
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 785
    iget-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    if-eqz p1, :cond_0

    .line 786
    iget-object p2, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mBuffer:[B

    invoke-virtual {p1, p2}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    .line 784
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method protected releaseCamera()V
    .locals 3

    .line 707
    monitor-enter p0

    .line 708
    :try_start_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 710
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 712
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 714
    :cond_0
    iput-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCamera:Landroid/hardware/Camera;

    .line 715
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 716
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 717
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mFrameChain:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 719
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrame:[Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    if-eqz v0, :cond_2

    .line 720
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrame:[Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->release()V

    .line 721
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrame:[Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;->release()V

    .line 723
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method selectPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;D)",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .line 137
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    .line 138
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 139
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/Camera$Size;

    .line 140
    iget v4, v3, Landroid/hardware/Camera$Size;->width:I

    int-to-double v4, v4

    iget v6, v3, Landroid/hardware/Camera$Size;->height:I

    int-to-double v6, v6

    div-double/2addr v4, v6

    sub-double/2addr v4, p2

    .line 141
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3fa999999999999aL    # 0.05

    cmpg-double v8, v4, v6

    if-gez v8, :cond_0

    .line 143
    iget v4, v3, Landroid/hardware/Camera$Size;->width:I

    iget v5, v3, Landroid/hardware/Camera$Size;->height:I

    mul-int v4, v4, v5

    int-to-double v4, v4

    const-wide v6, 0x4112c00000000000L    # 307200.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v8, v4, v6

    if-lez v8, :cond_1

    div-double v4, v6, v4

    :cond_1
    cmpl-double v6, v4, v1

    if-lez v6, :cond_0

    move-object v0, v3

    move-wide v1, v4

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method setForcusMode(Landroid/hardware/Camera$Parameters;)V
    .locals 6

    .line 339
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 340
    invoke-static {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getInstance(Landroid/content/Context;)Lcom/epson/cameracopy/device/CameraPreviewControl;

    move-result-object v0

    .line 341
    invoke-virtual {v0}, Lcom/epson/cameracopy/device/CameraPreviewControl;->getAutoPictureMode()Z

    move-result v0

    .line 344
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    if-nez v0, :cond_1

    const-string v0, "auto"

    .line 351
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "auto"

    .line 352
    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    return-void

    .line 357
    :cond_1
    sget-object v0, Lcom/epson/cameracopy/device/PortraitCameraView;->FAVORITE_FOCUS_MODES:[Ljava/lang/String;

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v4, v0, v3

    .line 358
    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 359
    invoke-virtual {p1, v4}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    return-void

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method protected setPreviewSize(Ljava/util/List;Landroid/hardware/Camera$Parameters;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "Landroid/hardware/Camera$Parameters;",
            ")Z"
        }
    .end annotation

    .line 166
    invoke-virtual {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->getScreenSize()Landroid/graphics/Point;

    move-result-object v0

    .line 167
    iget v1, v0, Landroid/graphics/Point;->y:I

    int-to-double v1, v1

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-double v3, v0

    div-double/2addr v1, v3

    invoke-virtual {p0, p1, v1, v2}, Lcom/epson/cameracopy/device/PortraitCameraView;->selectPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 175
    :cond_0
    iget v0, p1, Landroid/hardware/Camera$Size;->width:I

    iget p1, p1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p2, v0, p1}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    const/4 p1, 0x1

    return p1
.end method

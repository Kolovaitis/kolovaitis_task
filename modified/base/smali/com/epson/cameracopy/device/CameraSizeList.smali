.class public Lcom/epson/cameracopy/device/CameraSizeList;
.super Ljava/lang/Object;
.source "CameraSizeList.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/device/CameraSizeList$CameraSizeWarpper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/epson/cameracopy/device/SizeWrapper;",
        ">;"
    }
.end annotation


# instance fields
.field mCameraSizeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/Camera$Size;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/epson/cameracopy/device/CameraSizeList;->mCameraSizeList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/epson/cameracopy/device/SizeWrapper;",
            ">;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/epson/cameracopy/device/CameraSizeList$1;

    invoke-direct {v0, p0}, Lcom/epson/cameracopy/device/CameraSizeList$1;-><init>(Lcom/epson/cameracopy/device/CameraSizeList;)V

    return-object v0
.end method

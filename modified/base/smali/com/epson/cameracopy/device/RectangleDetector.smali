.class public Lcom/epson/cameracopy/device/RectangleDetector;
.super Ljava/lang/Object;
.source "RectangleDetector.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "opencv_java3"

    .line 7
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const-string v0, "rectdetector"

    .line 8
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native bcsCorrection(JJJDDD)I
.end method

.method public static native detectAndDrawRectangle(JZ)I
.end method

.method public static native detectAndDrawRectangle2(JIIZ)I
.end method

.method public static native getDefaultPaperWhiteCorrection()Landroid/graphics/Point;
.end method

.method public static native paperWhiteCorrection(JJ)I
.end method

.method public static native paperWhiteCorrection(JJLandroid/graphics/Point;)I
.end method

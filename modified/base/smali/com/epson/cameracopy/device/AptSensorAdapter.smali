.class public Lcom/epson/cameracopy/device/AptSensorAdapter;
.super Ljava/lang/Object;
.source "AptSensorAdapter.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private m2BaseValue:F

.field private mDisplayRotation:I

.field private mIsValidValue:Z

.field private mLastOverTime:J

.field private mLastOverTime2:J

.field private mPosX:F

.field private mPosY:F

.field private mPreD0:F

.field private mPreD1:F

.field private mPreD2:F

.field private mReferenceAccelaration:D

.field private final mSensorDelay:I

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 34
    iput v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mSensorDelay:I

    const-wide v0, 0x3fd999999999999aL    # 0.4

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 43
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->m2BaseValue:F

    return-void
.end method

.method public static getRangeCompValue(I)D
    .locals 4

    int-to-double v0, p0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double v0, v0, v2

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    .line 111
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    const-wide v2, 0x40239d0140000000L    # 9.806650161743164

    mul-double v0, v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static hasAptSensor(Landroid/content/Context;)Z
    .locals 1

    const-string v0, "sensor"

    .line 63
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/hardware/SensorManager;

    .line 65
    invoke-static {p0}, Lcom/epson/cameracopy/device/AptSensorAdapter;->hasAptSensor(Landroid/hardware/SensorManager;)Z

    move-result p0

    return p0
.end method

.method public static hasAptSensor(Landroid/hardware/SensorManager;)Z
    .locals 3

    const/4 v0, 0x1

    .line 51
    invoke-virtual {p0, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    :cond_0
    const/16 v1, 0xa

    .line 54
    invoke-virtual {p0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object p0

    if-nez p0, :cond_1

    return v2

    :cond_1
    return v0
.end method

.method private updateAccValue(Landroid/hardware/SensorEvent;)V
    .locals 8

    const/4 v0, 0x1

    .line 215
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mIsValidValue:Z

    .line 217
    iget v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mDisplayRotation:I

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 231
    :pswitch_0
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v0

    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosX:F

    .line 232
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    neg-float v1, v1

    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosY:F

    goto :goto_0

    .line 227
    :pswitch_1
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    neg-float v1, v1

    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosX:F

    .line 228
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v0

    neg-float v1, v1

    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosY:F

    goto :goto_0

    .line 223
    :pswitch_2
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v0

    neg-float v1, v1

    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosX:F

    .line 224
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosY:F

    goto :goto_0

    .line 219
    :pswitch_3
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosX:F

    .line 220
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v0

    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosY:F

    .line 236
    :goto_0
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    .line 237
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v2, v0

    .line 238
    iget-object p1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x2

    aget p1, p1, v2

    .line 239
    iget v2, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPreD0:F

    sub-float/2addr v2, v1

    float-to-double v2, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget v6, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPreD1:F

    sub-float/2addr v6, v0

    float-to-double v6, v6

    .line 240
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v2, v6

    iget v6, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPreD2:F

    sub-float/2addr v6, p1

    float-to-double v6, v6

    .line 241
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v2, v4

    double-to-float v2, v2

    .line 242
    iput v1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPreD0:F

    .line 243
    iput v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPreD1:F

    .line 244
    iput p1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPreD2:F

    .line 246
    iget p1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->m2BaseValue:F

    cmpl-float p1, v2, p1

    if-lez p1, :cond_0

    .line 247
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime2:J

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateLinearAccelValue(Landroid/hardware/SensorEvent;)V
    .locals 6

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 198
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v1, v1

    .line 199
    iget-wide v3, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mReferenceAccelaration:D

    cmpl-double v5, v1, v3

    if-lez v5, :cond_0

    .line 201
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime:J

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_1
    :goto_1
    iget-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime:J

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-nez p1, :cond_2

    .line 209
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime:J

    :cond_2
    return-void
.end method


# virtual methods
.method public getLastAccelOverTime()J
    .locals 2

    .line 270
    iget-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime:J

    return-wide v0
.end method

.method public getLastOverTime2()J
    .locals 2

    .line 260
    iget-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime2:J

    return-wide v0
.end method

.method public getPosX()F
    .locals 1

    .line 168
    iget v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosX:F

    return v0
.end method

.method public getPosY()F
    .locals 1

    .line 177
    iget v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosY:F

    return v0
.end method

.method public isTerminalAngleInRange(D)Z
    .locals 4

    .line 150
    iget-boolean v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mIsValidValue:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 154
    :cond_0
    iget v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosX:F

    mul-float v0, v0, v0

    iget v2, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mPosY:F

    mul-float v2, v2, v2

    add-float/2addr v0, v2

    float-to-double v2, v0

    cmpl-double v0, p1, v2

    if-ltz v0, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2

    .line 183
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    return-void

    .line 188
    :cond_0
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->updateLinearAccelValue(Landroid/hardware/SensorEvent;)V

    return-void

    .line 185
    :cond_1
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/device/AptSensorAdapter;->updateAccValue(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method public resetTime()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 138
    iput-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime:J

    return-void
.end method

.method public setDisplayRotation(I)V
    .locals 0

    .line 124
    iput p1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mDisplayRotation:I

    return-void
.end method

.method public setReferenceAccelaration(D)V
    .locals 0

    .line 133
    iput-wide p1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mReferenceAccelaration:D

    const-wide/16 p1, 0x0

    .line 134
    iput-wide p1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime:J

    return-void
.end method

.method public setSensorManager(Landroid/hardware/SensorManager;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    return-void
.end method

.method public start()V
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    .line 83
    iget-object v2, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v2, p0, v0, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v2, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v2, p0, v0, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_1
    const-wide/16 v0, 0x0

    .line 94
    iput-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime:J

    .line 95
    iput-wide v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mLastOverTime2:J

    return-void
.end method

.method public stop()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/epson/cameracopy/device/AptSensorAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method

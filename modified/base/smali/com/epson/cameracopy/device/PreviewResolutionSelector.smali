.class public Lcom/epson/cameracopy/device/PreviewResolutionSelector;
.super Ljava/lang/Object;
.source "PreviewResolutionSelector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getAspectScore(DD)D
    .locals 1

    sub-double/2addr p2, p0

    .line 176
    invoke-static {p2, p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide p0

    const-wide/high16 p2, -0x4020000000000000L    # -0.5

    mul-double p0, p0, p2

    const-wide/high16 p2, 0x3ff0000000000000L    # 1.0

    add-double/2addr p0, p2

    const-wide/16 p2, 0x0

    cmpg-double v0, p0, p2

    if-gez v0, :cond_0

    move-wide p0, p2

    :cond_0
    return-wide p0
.end method

.method static getScore01(II)I
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x190

    if-ge p0, v1, :cond_0

    return v0

    :cond_0
    if-gt p0, p1, :cond_3

    const/16 v0, 0x258

    if-gt p0, v0, :cond_1

    sub-int/2addr p0, v1

    mul-int/lit8 p0, p0, 0x5

    add-int/lit16 p0, p0, 0x2328

    return p0

    :cond_1
    const/16 v1, 0x2710

    if-gt p1, v0, :cond_2

    return v1

    :cond_2
    sub-int/2addr p0, v0

    mul-int/lit16 p0, p0, -0x3e8

    sub-int/2addr p1, v0

    .line 84
    div-int/2addr p0, p1

    add-int/2addr p0, v1

    return p0

    :cond_3
    return v0
.end method

.method static getScore02(II)I
    .locals 2

    .line 153
    invoke-static {p0, p1}, Lcom/epson/cameracopy/device/PreviewResolutionSelector;->getScore01(II)I

    move-result p1

    if-lez p1, :cond_0

    return p1

    :cond_0
    const/4 p1, 0x0

    const/16 v0, 0xc8

    if-ge p0, v0, :cond_1

    return p1

    :cond_1
    const/16 v1, 0x190

    if-ge p0, v1, :cond_2

    sub-int/2addr p0, v0

    mul-int/lit8 p0, p0, 0x5a

    .line 163
    div-int/lit8 p0, p0, 0x2

    return p0

    :cond_2
    return p1
.end method

.method static getScore03(IIIID)D
    .locals 2

    if-gt p0, p2, :cond_0

    .line 136
    invoke-static {p1, p3}, Lcom/epson/cameracopy/device/PreviewResolutionSelector;->getScore02(II)I

    move-result p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-gtz p2, :cond_1

    const-wide/16 p0, 0x0

    return-wide p0

    :cond_1
    int-to-double v0, p0

    int-to-double p0, p1

    div-double/2addr v0, p0

    .line 143
    invoke-static {v0, v1, p4, p5}, Lcom/epson/cameracopy/device/PreviewResolutionSelector;->getAspectScore(DD)D

    move-result-wide p0

    int-to-double p2, p2

    mul-double p2, p2, p0

    return-wide p2
.end method

.method static selectSize01(DLjava/lang/Iterable;II)Lcom/epson/cameracopy/device/SizeWrapper;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/lang/Iterable<",
            "Lcom/epson/cameracopy/device/SizeWrapper;",
            ">;II)",
            "Lcom/epson/cameracopy/device/SizeWrapper;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_6

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    cmpg-double v3, p0, v1

    if-gez v3, :cond_0

    goto :goto_1

    .line 27
    :cond_0
    new-instance v1, Lcom/epson/cameracopy/device/SimpleSize;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2}, Lcom/epson/cameracopy/device/SimpleSize;-><init>(II)V

    .line 31
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/epson/cameracopy/device/SizeWrapper;

    .line 32
    invoke-interface {v3}, Lcom/epson/cameracopy/device/SizeWrapper;->getWidth()I

    move-result v4

    .line 33
    invoke-interface {v3}, Lcom/epson/cameracopy/device/SizeWrapper;->getHeight()I

    move-result v5

    if-le v4, p3, :cond_2

    goto :goto_0

    :cond_2
    if-gtz v5, :cond_3

    goto :goto_0

    :cond_3
    int-to-double v6, v4

    int-to-double v8, v5

    div-double/2addr v6, v8

    sub-double/2addr v6, p0

    .line 44
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x3fa999999999999aL    # 0.05

    cmpg-double v4, v6, v8

    if-gez v4, :cond_1

    .line 45
    invoke-static {v5, p4}, Lcom/epson/cameracopy/device/PreviewResolutionSelector;->getScore01(II)I

    move-result v4

    if-ge v2, v4, :cond_1

    move-object v1, v3

    move v2, v4

    goto :goto_0

    .line 54
    :cond_4
    invoke-interface {v1}, Lcom/epson/cameracopy/device/SizeWrapper;->getWidth()I

    move-result p0

    if-gtz p0, :cond_5

    return-object v0

    :cond_5
    return-object v1

    :cond_6
    :goto_1
    return-object v0
.end method

.method static selectSize02(DLjava/lang/Iterable;II)Lcom/epson/cameracopy/device/SizeWrapper;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/lang/Iterable<",
            "Lcom/epson/cameracopy/device/SizeWrapper;",
            ">;II)",
            "Lcom/epson/cameracopy/device/SizeWrapper;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    cmpg-double v3, p0, v1

    if-gez v3, :cond_0

    goto :goto_1

    .line 103
    :cond_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 104
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    return-object v0

    .line 108
    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/device/SizeWrapper;

    .line 111
    invoke-interface {v0}, Lcom/epson/cameracopy/device/SizeWrapper;->getWidth()I

    move-result v1

    invoke-interface {v0}, Lcom/epson/cameracopy/device/SizeWrapper;->getHeight()I

    move-result v2

    move v3, p3

    move v4, p4

    move-wide v5, p0

    invoke-static/range {v1 .. v6}, Lcom/epson/cameracopy/device/PreviewResolutionSelector;->getScore03(IIIID)D

    move-result-wide v1

    .line 114
    :cond_2
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 115
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/epson/cameracopy/device/SizeWrapper;

    .line 116
    invoke-interface {v3}, Lcom/epson/cameracopy/device/SizeWrapper;->getWidth()I

    move-result v4

    invoke-interface {v3}, Lcom/epson/cameracopy/device/SizeWrapper;->getHeight()I

    move-result v5

    move v6, p3

    move v7, p4

    move-wide v8, p0

    invoke-static/range {v4 .. v9}, Lcom/epson/cameracopy/device/PreviewResolutionSelector;->getScore03(IIIID)D

    move-result-wide v4

    cmpg-double v6, v1, v4

    if-gez v6, :cond_2

    move-object v0, v3

    move-wide v1, v4

    goto :goto_0

    :cond_3
    return-object v0

    :cond_4
    :goto_1
    return-object v0
.end method

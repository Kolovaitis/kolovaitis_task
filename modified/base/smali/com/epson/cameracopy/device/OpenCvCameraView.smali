.class public Lcom/epson/cameracopy/device/OpenCvCameraView;
.super Lcom/epson/cameracopy/device/PortraitCameraView;
.source "OpenCvCameraView.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;
.implements Landroid/hardware/Camera$ShutterCallback;
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;,
        Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "OpenCvCameraView"


# instance fields
.field private mAutoFocusCallBack:Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;

.field private mHasAutoFocus:Z

.field private volatile mInAutofocus:Z

.field private mPictureFileName:Ljava/lang/String;

.field private mPictureTakeCallback:Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;

.field private mPictureTaking:Z

.field private mTakePictuerAutofocus:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/epson/cameracopy/device/PortraitCameraView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p2, "android.hardware.camera.autofocus"

    .line 71
    invoke-virtual {p1, p2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mHasAutoFocus:Z

    :cond_0
    return-void
.end method

.method private localAutofocus()V
    .locals 2

    .line 186
    iget-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mHasAutoFocus:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 187
    iput-boolean v1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    return-void

    .line 192
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p0}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 195
    :catch_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p0, v1, v0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->onAutoFocus(ZLandroid/hardware/Camera;)V

    :goto_0
    return-void
.end method

.method private localCancelAutofocus()V
    .locals 1

    .line 201
    iget-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mHasAutoFocus:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 202
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_1

    return-void

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    return-void
.end method


# virtual methods
.method public cancelAutofocus()V
    .locals 2

    .line 280
    invoke-direct {p0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->localCancelAutofocus()V

    const/4 v0, 0x0

    .line 282
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    const/4 v1, 0x0

    .line 283
    iput-object v1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mAutoFocusCallBack:Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;

    .line 284
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mTakePictuerAutofocus:Z

    .line 285
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTaking:Z

    .line 286
    iput-object v1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTakeCallback:Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;

    return-void
.end method

.method public declared-synchronized doAutofocus(Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;)V
    .locals 2

    monitor-enter p0

    .line 260
    :try_start_0
    iget-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    iget-boolean v1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTaking:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    or-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 263
    monitor-exit p0

    return-void

    .line 267
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mAutoFocusCallBack:Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;

    const/4 p1, 0x1

    .line 268
    iput-boolean p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    .line 269
    invoke-direct {p0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->localAutofocus()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public enableView()V
    .locals 1

    .line 292
    invoke-super {p0}, Lcom/epson/cameracopy/device/PortraitCameraView;->enableView()V

    const/4 v0, 0x0

    .line 294
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    .line 295
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTaking:Z

    return-void
.end method

.method public getCamera()Landroid/hardware/Camera;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method public getScale()F
    .locals 2

    .line 302
    iget v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mScale:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    return v0

    .line 306
    :cond_0
    iget v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mScale:F

    return v0
.end method

.method public isInAutofocus()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    return v0
.end method

.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 2

    .line 229
    iget-boolean p2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mTakePictuerAutofocus:Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    .line 230
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mTakePictuerAutofocus:Z

    .line 241
    iget-object p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p1, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 242
    iget-object p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p1, p0, v1, p0}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    goto :goto_0

    .line 244
    :cond_0
    iget-object p2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mAutoFocusCallBack:Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;

    if-eqz p2, :cond_1

    .line 245
    invoke-interface {p2, p1}, Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;->onAutoFocusCompleted(Z)V

    .line 246
    iput-object v1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mAutoFocusCallBack:Lcom/epson/cameracopy/device/OpenCvCameraView$AutoFocusCallBack;

    .line 250
    :cond_1
    :goto_0
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    return-void
.end method

.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 3

    .line 96
    iget-object p2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p2}, Landroid/hardware/Camera;->startPreview()V

    .line 97
    iget-object p2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p2, p0}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    const/4 p2, 0x0

    const/4 v0, 0x0

    .line 102
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureFileName:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 115
    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 118
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    const/4 p1, 0x1

    goto :goto_3

    :catch_1
    move-exception p1

    goto :goto_1

    :catchall_0
    move-exception p1

    move-object v1, p2

    goto :goto_4

    :catch_2
    move-exception p1

    move-object v1, p2

    .line 106
    :goto_1
    :try_start_3
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 108
    new-instance p1, Ljava/io/File;

    iget-object v2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureFileName:Ljava/lang/String;

    invoke-direct {p1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    .line 115
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    .line 118
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :cond_1
    :goto_2
    const/4 p1, 0x0

    .line 123
    :goto_3
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureFileName:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 124
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 p1, 0x0

    .line 130
    :cond_2
    iget-object v1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTakeCallback:Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;

    if-eqz v1, :cond_3

    .line 131
    iget-object v2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureFileName:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;->onMyPictureTaken(ZLjava/lang/String;)V

    .line 132
    iput-object p2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTakeCallback:Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;

    .line 137
    :cond_3
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTaking:Z

    return-void

    :catchall_1
    move-exception p1

    :goto_4
    if-eqz v1, :cond_4

    .line 115
    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_5

    :catch_4
    move-exception p2

    .line 118
    invoke-virtual {p2}, Ljava/io/IOException;->printStackTrace()V

    .line 119
    :cond_4
    :goto_5
    throw p1
.end method

.method public onShutter()V
    .locals 0

    return-void
.end method

.method public setDisplayOriantation(I)V
    .locals 0

    return-void
.end method

.method public declared-synchronized takePicture(Ljava/lang/String;Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;)V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    .line 146
    :try_start_0
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTaking:Z

    .line 149
    iget-boolean v1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 151
    monitor-exit p0

    return-void

    .line 154
    :cond_0
    :try_start_1
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z

    .line 157
    iput-object p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureFileName:Ljava/lang/String;

    .line 158
    iput-object p2, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTakeCallback:Lcom/epson/cameracopy/device/OpenCvCameraView$MyPictureTakeCallback;

    .line 170
    iget-boolean p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mHasAutoFocus:Z

    if-nez p1, :cond_1

    .line 171
    iget-object p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 172
    iget-object p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p1, p0, p2, p0}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    const/4 p1, 0x0

    .line 174
    iput-boolean p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mPictureTaking:Z

    .line 175
    iput-boolean p1, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mInAutofocus:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    monitor-exit p0

    return-void

    .line 180
    :cond_1
    :try_start_2
    iput-boolean v0, p0, Lcom/epson/cameracopy/device/OpenCvCameraView;->mTakePictuerAutofocus:Z

    .line 181
    invoke-direct {p0}, Lcom/epson/cameracopy/device/OpenCvCameraView;->localAutofocus()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 182
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

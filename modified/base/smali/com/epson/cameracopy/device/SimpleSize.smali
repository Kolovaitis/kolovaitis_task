.class public Lcom/epson/cameracopy/device/SimpleSize;
.super Ljava/lang/Object;
.source "SimpleSize.java"

# interfaces
.implements Lcom/epson/cameracopy/device/SizeWrapper;


# instance fields
.field private mHeight:I

.field private mWidth:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/epson/cameracopy/device/SimpleSize;->mWidth:I

    .line 14
    iput p2, p0, Lcom/epson/cameracopy/device/SimpleSize;->mHeight:I

    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/epson/cameracopy/device/SimpleSize;->mHeight:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/epson/cameracopy/device/SimpleSize;->mWidth:I

    return v0
.end method

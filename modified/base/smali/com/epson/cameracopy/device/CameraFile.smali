.class public Lcom/epson/cameracopy/device/CameraFile;
.super Ljava/lang/Object;
.source "CameraFile.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mDstFile:Ljava/io/File;

.field private final mFileCreateDate:Ljava/util/Date;

.field private mTempFileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/util/Date;Ljava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/epson/cameracopy/device/CameraFile;->mFileCreateDate:Ljava/util/Date;

    .line 44
    iput-object p2, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public deleteTemporaryFile()V
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 172
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 173
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    const/4 v0, 0x0

    .line 177
    iput-object v0, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 57
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 58
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 59
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result p1

    if-nez p1, :cond_0

    return-object v1

    .line 63
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result p1

    if-nez p1, :cond_1

    return-object v1

    .line 67
    :cond_1
    new-instance p1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMddHHmm"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {p1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 68
    iget-object v2, p0, Lcom/epson/cameracopy/device/CameraFile;->mFileCreateDate:Ljava/util/Date;

    invoke-virtual {p1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/16 v4, 0x64

    if-ge v3, v4, :cond_3

    .line 71
    new-instance v4, Ljava/io/File;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s_%02d.jpg"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v2

    .line 72
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x1

    aput-object v8, v7, v9

    .line 71
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 73
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_2

    .line 74
    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-object v1
.end method

.method public getValidFileName()Ljava/lang/String;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraFile;->mDstFile:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 144
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public isMovable()Z
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public isMoved()Z
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraFile;->mDstFile:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public moveTo(Ljava/lang/String;)Z
    .locals 5

    .line 89
    iget-object v0, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 93
    :cond_0
    invoke-virtual {p0, p1}, Lcom/epson/cameracopy/device/CameraFile;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    return v1

    .line 99
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 100
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 101
    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result p1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    .line 103
    iput-object v2, p0, Lcom/epson/cameracopy/device/CameraFile;->mDstFile:Ljava/io/File;

    .line 106
    iput-object v4, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    return v3

    .line 112
    :cond_2
    :try_start_0
    invoke-static {v0, v2}, Lcom/epson/cameracopy/alt/FileUtils;->copy(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 117
    iput-object v2, p0, Lcom/epson/cameracopy/device/CameraFile;->mDstFile:Ljava/io/File;

    .line 120
    iput-object v4, p0, Lcom/epson/cameracopy/device/CameraFile;->mTempFileName:Ljava/lang/String;

    return v3

    :catch_0
    return v1
.end method

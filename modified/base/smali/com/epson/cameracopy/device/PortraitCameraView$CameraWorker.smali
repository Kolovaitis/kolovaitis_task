.class Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;
.super Ljava/lang/Object;
.source "PortraitCameraView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/cameracopy/device/PortraitCameraView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/cameracopy/device/PortraitCameraView;


# direct methods
.method private constructor <init>(Lcom/epson/cameracopy/device/PortraitCameraView;)V
    .locals 0

    .line 830
    iput-object p1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/cameracopy/device/PortraitCameraView;Lcom/epson/cameracopy/device/PortraitCameraView$1;)V
    .locals 0

    .line 830
    invoke-direct {p0, p1}, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;-><init>(Lcom/epson/cameracopy/device/PortraitCameraView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    monitor-enter v0

    .line 838
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v1}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$100(Lcom/epson/cameracopy/device/PortraitCameraView;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v1}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$200(Lcom/epson/cameracopy/device/PortraitCameraView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 839
    iget-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    .line 842
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 844
    :cond_1
    iget-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v1}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$100(Lcom/epson/cameracopy/device/PortraitCameraView;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    .line 846
    iget-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    iget-object v4, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v4}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$300(Lcom/epson/cameracopy/device/PortraitCameraView;)I

    move-result v4

    rsub-int/lit8 v4, v4, 0x1

    invoke-static {v1, v4}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$302(Lcom/epson/cameracopy/device/PortraitCameraView;I)I

    .line 847
    iget-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v1, v2}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$102(Lcom/epson/cameracopy/device/PortraitCameraView;Z)Z

    const/4 v2, 0x1

    .line 850
    :cond_2
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 852
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v0}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$200(Lcom/epson/cameracopy/device/PortraitCameraView;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz v2, :cond_3

    .line 853
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v0}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$400(Lcom/epson/cameracopy/device/PortraitCameraView;)[Lorg/opencv/core/Mat;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v1}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$300(Lcom/epson/cameracopy/device/PortraitCameraView;)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 854
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    iget-object v1, v0, Lcom/epson/cameracopy/device/PortraitCameraView;->mCameraFrame:[Lcom/epson/cameracopy/device/PortraitCameraView$JavaCameraFrame;

    iget-object v2, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v2}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$300(Lcom/epson/cameracopy/device/PortraitCameraView;)I

    move-result v2

    sub-int/2addr v3, v2

    aget-object v1, v1, v3

    invoke-static {v0, v1}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$500(Lcom/epson/cameracopy/device/PortraitCameraView;Lorg/opencv/android/CameraBridgeViewBase$CvCameraViewFrame;)V

    .line 856
    :cond_3
    iget-object v0, p0, Lcom/epson/cameracopy/device/PortraitCameraView$CameraWorker;->this$0:Lcom/epson/cameracopy/device/PortraitCameraView;

    invoke-static {v0}, Lcom/epson/cameracopy/device/PortraitCameraView;->access$200(Lcom/epson/cameracopy/device/PortraitCameraView;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "portCameraView"

    const-string v1, "Finish processing thread"

    .line 857
    invoke-static {v0, v1}, Lcom/epson/cameracopy/device/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 850
    :goto_1
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

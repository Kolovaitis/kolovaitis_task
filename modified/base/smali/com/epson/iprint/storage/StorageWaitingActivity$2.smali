.class Lcom/epson/iprint/storage/StorageWaitingActivity$2;
.super Ljava/lang/Object;
.source "StorageWaitingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageWaitingActivity;->createCancelDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageWaitingActivity;)V
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$2;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 192
    iget-object p2, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$2;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    iget-object p2, p2, Lcom/epson/iprint/storage/StorageWaitingActivity;->mCancelButton:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 193
    iget-object p2, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$2;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    iget-object p2, p2, Lcom/epson/iprint/storage/StorageWaitingActivity;->mMessageText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$2;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    const v1, 0x7f0e0219

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    new-instance p2, Landroid/content/Intent;

    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->CANCEL_REQUEST_ACTION:Ljava/lang/String;

    invoke-direct {p2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 196
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_REQUEST_CODE:Ljava/lang/String;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$2;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    iget v1, v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->mRequestCode:I

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 197
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$2;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    invoke-virtual {v0, p2}, Lcom/epson/iprint/storage/StorageWaitingActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 198
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    return-void
.end method

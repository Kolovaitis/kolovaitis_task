.class public final enum Lcom/epson/iprint/storage/StorageItem$ItemType;
.super Ljava/lang/Enum;
.source "StorageItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/iprint/storage/StorageItem$ItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/iprint/storage/StorageItem$ItemType;

.field public static final enum DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

.field public static final enum FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

.field public static final enum LOADMORE:Lcom/epson/iprint/storage/StorageItem$ItemType;

.field public static final enum PHOTO:Lcom/epson/iprint/storage/StorageItem$ItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 59
    new-instance v0, Lcom/epson/iprint/storage/StorageItem$ItemType;

    const-string v1, "DIRECTORY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/iprint/storage/StorageItem$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 66
    new-instance v0, Lcom/epson/iprint/storage/StorageItem$ItemType;

    const-string v1, "FILE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/iprint/storage/StorageItem$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 73
    new-instance v0, Lcom/epson/iprint/storage/StorageItem$ItemType;

    const-string v1, "PHOTO"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/iprint/storage/StorageItem$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/StorageItem$ItemType;->PHOTO:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 80
    new-instance v0, Lcom/epson/iprint/storage/StorageItem$ItemType;

    const-string v1, "LOADMORE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/epson/iprint/storage/StorageItem$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/StorageItem$ItemType;->LOADMORE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    const/4 v0, 0x4

    .line 53
    new-array v0, v0, [Lcom/epson/iprint/storage/StorageItem$ItemType;

    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->PHOTO:Lcom/epson/iprint/storage/StorageItem$ItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->LOADMORE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/epson/iprint/storage/StorageItem$ItemType;->$VALUES:[Lcom/epson/iprint/storage/StorageItem$ItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/iprint/storage/StorageItem$ItemType;
    .locals 1

    .line 53
    const-class v0, Lcom/epson/iprint/storage/StorageItem$ItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/iprint/storage/StorageItem$ItemType;

    return-object p0
.end method

.method public static values()[Lcom/epson/iprint/storage/StorageItem$ItemType;
    .locals 1

    .line 53
    sget-object v0, Lcom/epson/iprint/storage/StorageItem$ItemType;->$VALUES:[Lcom/epson/iprint/storage/StorageItem$ItemType;

    invoke-virtual {v0}, [Lcom/epson/iprint/storage/StorageItem$ItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/iprint/storage/StorageItem$ItemType;

    return-object v0
.end method

.class Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;
.super Ljava/lang/Object;
.source "StorageProcessDownloadActivity.java"

# interfaces
.implements Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;)V
    .locals 0

    .line 460
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;",
            "Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;",
            ")V"
        }
    .end annotation

    .line 467
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iput-object p1, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->enumeratedItems:Ljava/util/List;

    .line 468
    iget-object p1, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$700(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Ljava/util/Stack;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Stack;->size()I

    move-result p1

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 470
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$700(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Ljava/util/Stack;

    move-result-object v0

    add-int/lit8 p1, p1, -0x2

    invoke-virtual {v0, p1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/storage/StorageItem;

    .line 471
    new-instance v0, Lcom/epson/iprint/storage/StorageItem;

    const-string v1, ".."

    invoke-direct {v0, v1}, Lcom/epson/iprint/storage/StorageItem;-><init>(Ljava/lang/String;)V

    .line 472
    iget-object v1, p1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    iput-object v1, v0, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 473
    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object v1, v0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 474
    iget-object p1, p1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    iput-object p1, v0, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    .line 475
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object p1, p1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->enumeratedItems:Ljava/util/List;

    if-nez p1, :cond_0

    .line 476
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->enumeratedItems:Ljava/util/List;

    .line 478
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object p1, p1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->enumeratedItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 487
    :cond_1
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object p1, p1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;

    invoke-direct {v0, p0, p2}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

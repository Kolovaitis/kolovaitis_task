.class Lcom/epson/iprint/storage/StorageWaitingActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "StorageWaitingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageWaitingActivity;->registerBroadcastReceiver(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageWaitingActivity;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$1;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .line 122
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    .line 128
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_REQUEST_CODE:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 129
    iget-object v1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$1;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    iget v1, v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->mRequestCode:I

    if-eq v1, v0, :cond_0

    return-void

    .line 136
    :cond_0
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->DISMISS_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$1;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->finish()V

    return-void

    .line 144
    :cond_1
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->CHANGE_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 145
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$1;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    invoke-virtual {p1, p2}, Lcom/epson/iprint/storage/StorageWaitingActivity;->setMessage(Landroid/content/Intent;)V

    return-void

    .line 152
    :cond_2
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->REGISTER_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 153
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity$1;->this$0:Lcom/epson/iprint/storage/StorageWaitingActivity;

    invoke-virtual {p1, p2}, Lcom/epson/iprint/storage/StorageWaitingActivity;->setCancelRequest(Landroid/content/Intent;)V

    return-void

    :cond_3
    return-void
.end method

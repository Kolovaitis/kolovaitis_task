.class public Lcom/epson/iprint/storage/StorageItem;
.super Ljava/lang/Object;
.source "StorageItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageItem$NameComparator;,
        Lcom/epson/iprint/storage/StorageItem$ItemType;
    }
.end annotation


# static fields
.field static final LOGGERINFO_MYPOCKET_FILE:I = 0x2

.field static final LOGGERINFO_MYPOCKET_PHOTO:I = 0x1

.field private static final LOGGERINFO_NO_INFO:I


# instance fields
.field mLoggerInfo:I

.field public name:Ljava/lang/String;

.field public path:Ljava/lang/String;

.field public type:Lcom/epson/iprint/storage/StorageItem$ItemType;

.field public userInfo:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lcom/epson/iprint/storage/StorageItem;->mLoggerInfo:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lcom/epson/iprint/storage/StorageItem;->mLoggerInfo:I

    .line 90
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageItem$ItemType;)V
    .locals 1

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lcom/epson/iprint/storage/StorageItem;->mLoggerInfo:I

    .line 101
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 103
    iput-object p3, p0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageItem$ItemType;Ljava/lang/Object;)V
    .locals 1

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lcom/epson/iprint/storage/StorageItem;->mLoggerInfo:I

    .line 107
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    .line 108
    iput-object p2, p0, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 109
    iput-object p3, p0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 110
    iput-object p4, p0, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    return-void
.end method

.method public static endsWith(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .line 43
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 p0, 0x0

    return p0

    .line 46
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v4, v0, v1

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 47
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    move-object v2, p0

    move-object v5, p1

    invoke-virtual/range {v2 .. v7}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result p0

    return p0
.end method


# virtual methods
.method getDownloadLocalPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 121
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getDownloadDir()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_0

    .line 123
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 125
    :cond_0
    new-instance p1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    invoke-direct {p1, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

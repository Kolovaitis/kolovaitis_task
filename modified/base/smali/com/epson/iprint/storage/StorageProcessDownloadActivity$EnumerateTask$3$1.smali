.class Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;
.super Ljava/lang/Object;
.source "StorageProcessDownloadActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;

.field final synthetic val$error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

.field final synthetic val$items:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;Ljava/util/List;)V
    .locals 0

    .line 515
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;

    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->val$error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    iput-object p3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->val$items:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 523
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->val$error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-ne v0, v1, :cond_0

    .line 524
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$800(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;

    if-eqz v0, :cond_2

    .line 526
    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->val$items:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->addLoaedMoreItems(Ljava/util/List;)V

    .line 527
    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->val$error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 531
    :cond_1
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const v1, 0x7f0e053b

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->showErrorDialog(I)V

    .line 538
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$100(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Z)V

    .line 539
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->dismiss()V

    return-void
.end method

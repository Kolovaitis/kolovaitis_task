.class Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$2;
.super Ljava/lang/Object;
.source "StorageProcessDownloadActivity.java"

# interfaces
.implements Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;)V
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    .locals 1

    if-eqz p2, :cond_0

    .line 291
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iput-object p2, p1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->localPath:Ljava/lang/String;

    .line 293
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object p1, p1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    new-instance p2, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    invoke-direct {p2, v0, p3}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    invoke-virtual {p1, p2}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.class public Lcom/epson/iprint/storage/LocalProgressDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "LocalProgressDialog.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "MyProgress"

.field private static final PARAM_CANCELABLE:Ljava/lang/String; = "cancelable"

.field private static final PARAM_CANCEL_CONFIRM_DIALOG_TAG:Ljava/lang/String; = "tag_confirm-cancel"

.field private static final PARAM_MESSAGE_ID:Ljava/lang/String; = "message-id"


# instance fields
.field private mCancelConfirmDialogTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/LocalProgressDialog;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/epson/iprint/storage/LocalProgressDialog;->showCancelConfirmDialog()V

    return-void
.end method

.method public static newInstance(ZILjava/lang/String;)Lcom/epson/iprint/storage/LocalProgressDialog;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    if-eqz p0, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    .line 47
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0

    .line 50
    :cond_1
    :goto_0
    new-instance v0, Lcom/epson/iprint/storage/LocalProgressDialog;

    invoke-direct {v0}, Lcom/epson/iprint/storage/LocalProgressDialog;-><init>()V

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "cancelable"

    .line 53
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string p0, "message-id"

    .line 54
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "tag_confirm-cancel"

    .line 55
    invoke-virtual {v1, p0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/LocalProgressDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private showCancelConfirmDialog()V
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/epson/iprint/storage/LocalProgressDialog;->mCancelConfirmDialogTag:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {p0}, Lcom/epson/iprint/storage/LocalProgressDialog;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0e04de

    .line 135
    invoke-static {v1}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->newInstance(I)Lcom/epson/iprint/storage/ConfirmCancelDialog;

    move-result-object v1

    .line 136
    iget-object v2, p0, Lcom/epson/iprint/storage/LocalProgressDialog;->mCancelConfirmDialogTag:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    .line 127
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 119
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    const/4 v0, 0x1

    .line 120
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    return-object p1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 64
    invoke-virtual {p0}, Lcom/epson/iprint/storage/LocalProgressDialog;->getArguments()Landroid/os/Bundle;

    move-result-object p3

    const-string v0, "cancelable"

    const/4 v1, 0x0

    .line 65
    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "message-id"

    .line 66
    invoke-virtual {p3, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "tag_confirm-cancel"

    .line 67
    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/epson/iprint/storage/LocalProgressDialog;->mCancelConfirmDialogTag:Ljava/lang/String;

    const p3, 0x7f0a0094

    .line 69
    invoke-virtual {p1, p3, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f0800b0

    .line 70
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 71
    new-instance p3, Lcom/epson/iprint/storage/LocalProgressDialog$1;

    invoke-direct {p3, p0}, Lcom/epson/iprint/storage/LocalProgressDialog$1;-><init>(Lcom/epson/iprint/storage/LocalProgressDialog;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f0e0476

    .line 77
    invoke-virtual {p2, p3}, Landroid/widget/Button;->setText(I)V

    const/16 p3, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    .line 78
    :goto_0
    invoke-virtual {p2, v0}, Landroid/widget/Button;->setVisibility(I)V

    const p2, 0x7f080219

    .line 80
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 82
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    if-nez v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 p3, 0x0

    .line 84
    :goto_1
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setVisibility(I)V

    const p2, 0x7f080109

    .line 86
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    .line 87
    new-instance p3, Lcom/epson/iprint/storage/LocalProgressDialog$2;

    invoke-direct {p3, p0}, Lcom/epson/iprint/storage/LocalProgressDialog$2;-><init>(Lcom/epson/iprint/storage/LocalProgressDialog;)V

    invoke-virtual {p2, p3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p2, 0x1

    .line 97
    invoke-virtual {p1, p2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 98
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 99
    new-instance p2, Lcom/epson/iprint/storage/LocalProgressDialog$3;

    invoke-direct {p2, p0}, Lcom/epson/iprint/storage/LocalProgressDialog$3;-><init>(Lcom/epson/iprint/storage/LocalProgressDialog;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 111
    invoke-virtual {p0, v1}, Lcom/epson/iprint/storage/LocalProgressDialog;->setCancelable(Z)V

    return-object p1
.end method

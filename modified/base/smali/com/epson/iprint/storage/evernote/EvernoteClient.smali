.class public Lcom/epson/iprint/storage/evernote/EvernoteClient;
.super Lcom/epson/iprint/storage/StorageServiceClient;
.source "EvernoteClient.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageServiceClient;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/evernote/EvernoteClient;Lcom/epson/iprint/storage/StorageItem;)Z
    .locals 0

    .line 18
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/evernote/EvernoteClient;->isPrintableFileTypes(Lcom/epson/iprint/storage/StorageItem;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
    .locals 1

    .line 111
    new-instance v0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;-><init>(Lcom/epson/iprint/storage/evernote/EvernoteClient;Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
    .locals 1

    .line 152
    new-instance v0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;-><init>(Lcom/epson/iprint/storage/evernote/EvernoteClient;Landroid/content/Context;)V

    return-object v0
.end method

.method public getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    const-string p1, "Evernote"

    return-object p1
.end method

.method public getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
    .locals 0

    .line 66
    new-instance p2, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    invoke-direct {p2, p0, p1, p3, p4}, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;-><init>(Lcom/epson/iprint/storage/evernote/EvernoteClient;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method

.method public isSignedIn(Landroid/content/Context;)Z
    .locals 0

    .line 22
    invoke-static {p1}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession;->isLoggedIn()Z

    move-result p1

    return p1
.end method

.method public isSupportedUploadType(Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public isValidUploadName(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "."

    .line 42
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "UTF-8"

    .line 47
    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 48
    array-length v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x100

    if-le v0, v2, :cond_1

    return v1

    :cond_1
    const-string v0, "\\/:,;*?<>|#\u3001\""

    .line 56
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget-char v5, v2, v4

    .line 57
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const/4 v6, -0x1

    if-le v5, v6, :cond_2

    return v1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 61
    :cond_3
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient;->isValidUploadName(Ljava/lang/String;)Z

    move-result p1

    return p1

    :catch_0
    return v1
.end method

.method public revokeSignedInData(Landroid/app/Activity;)Z
    .locals 0

    .line 32
    invoke-static {p1}, Lepson/server/service/EvernoteClient;->logout(Landroid/content/Context;)V

    const/4 p1, 0x1

    return p1
.end method

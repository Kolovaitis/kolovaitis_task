.class public Lcom/epson/iprint/storage/evernote/EvernoteSignInActivity;
.super Lcom/epson/iprint/storage/StorageSignInActivity;
.source "EvernoteSignInActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EvernoteSignInActivity"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public getBasicSignIn()Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    const/16 p3, 0x3836

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_1

    .line 26
    invoke-static {p0}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession;->isLoggedIn()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "EvernoteSignInActivity"

    const-string p2, "REQUEST_CODE_OAUTH RESULT_OK"

    .line 27
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0}, Lcom/epson/iprint/storage/evernote/EvernoteSignInActivity;->finish()V

    return-void

    .line 32
    :cond_1
    invoke-virtual {p0}, Lcom/epson/iprint/storage/evernote/EvernoteSignInActivity;->showLoginErrorAndFinish()V

    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/epson/iprint/storage/evernote/EvernoteSignInActivity;->revokeUserData()V

    .line 50
    invoke-virtual {p0}, Lcom/epson/iprint/storage/evernote/EvernoteSignInActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 17
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    invoke-static {p0}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/evernote/client/android/EvernoteSession;->authenticate(Landroid/app/Activity;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .line 41
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;->onDestroy()V

    return-void
.end method

.method revokeUserData()V
    .locals 0

    .line 54
    invoke-static {p0}, Lepson/server/service/EvernoteClient;->logout(Landroid/content/Context;)V

    return-void
.end method

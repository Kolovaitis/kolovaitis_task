.class Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;
.super Landroid/os/AsyncTask;
.source "EvernoteClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/evernote/EvernoteClient$2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/evernote/EvernoteClient$2;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .line 118
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    .line 121
    :cond_0
    :try_start_0
    iget-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    iget-object p1, p1, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->downloader:Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;

    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    iget-object v0, v0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->val$item:Lcom/epson/iprint/storage/StorageItem;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    iget-object v1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    iget-object v1, v1, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->val$localPath:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;->download(Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 123
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    iget-object v0, v0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->handleException(Ljava/lang/Exception;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    move-result-object p1

    .line 125
    :goto_0
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->RETRY:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    iget-object v0, v0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->downloaded:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    iget-object v1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    iget-object v1, v1, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->val$item:Lcom/epson/iprint/storage/StorageItem;

    iget-object v2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$2;

    iget-object v2, v2, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->val$localPath:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p1}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    const/4 p1, 0x0

    return-object p1
.end method

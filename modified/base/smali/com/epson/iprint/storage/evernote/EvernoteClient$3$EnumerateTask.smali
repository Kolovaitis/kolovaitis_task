.class Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;
.super Landroid/os/AsyncTask;
.source "EvernoteClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/evernote/EvernoteClient$3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EnumerateTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/epson/iprint/storage/StorageItem;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$3;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/evernote/EvernoteClient$3;)V
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$3;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 167
    check-cast p1, [Lcom/epson/iprint/storage/StorageItem;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;->doInBackground([Lcom/epson/iprint/storage/StorageItem;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lcom/epson/iprint/storage/StorageItem;)Ljava/lang/Void;
    .locals 4

    .line 169
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    const/4 v0, 0x0

    .line 170
    aget-object p1, p1, v0

    const/4 v0, 0x0

    move-object v1, v0

    .line 175
    :cond_0
    :try_start_0
    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    .line 176
    iget-object v3, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$3;

    iget-object v3, v3, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {v3, p1}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->getItemList(Lcom/epson/iprint/storage/StorageItem;)Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 178
    iget-object v3, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$3;

    iget-object v3, v3, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {v3, v2}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->handleException(Ljava/lang/Exception;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    move-result-object v2

    .line 180
    :goto_0
    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->RETRY:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-virtual {v3, v2}, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v1, :cond_1

    .line 183
    iget-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$3;

    invoke-virtual {p1, v1}, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->getPrintableItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 185
    :cond_1
    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    .line 187
    :goto_1
    iget-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$3;

    iget-object p1, p1, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->enumerated:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    invoke-interface {p1, v1, v2}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-object v0
.end method

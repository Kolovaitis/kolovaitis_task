.class Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;
.super Ljava/lang/Object;
.source "EvernoteRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/evernote/EvernoteRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UploadHandler"
.end annotation


# instance fields
.field private canceled:Z

.field final synthetic this$0:Lcom/epson/iprint/storage/evernote/EvernoteRequest;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/evernote/EvernoteRequest;)V
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;->this$0:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 200
    iput-boolean p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;->canceled:Z

    return-void
.end method


# virtual methods
.method cancel()V
    .locals 2

    const/4 v0, 0x1

    .line 242
    iput-boolean v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;->canceled:Z

    const-string v0, "EvernoteRequest"

    const-string v1, "Upload Canceled\uff01\uff01"

    .line 243
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method upload(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 203
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;->this$0:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-static {v0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->access$000(Lcom/epson/iprint/storage/evernote/EvernoteRequest;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object v0

    .line 206
    iget-object p1, p1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast p1, Lcom/evernote/edam/type/Notebook;

    .line 208
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209
    new-instance p2, Lcom/evernote/edam/type/Resource;

    invoke-direct {p2}, Lcom/evernote/edam/type/Resource;-><init>()V

    .line 211
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->access$100(Ljava/lang/String;)Lcom/evernote/edam/type/Data;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/evernote/edam/type/Resource;->setData(Lcom/evernote/edam/type/Data;)V

    .line 212
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lepson/common/Utils;->getExtention(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lepson/common/Utils;->getExtType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/evernote/edam/type/Resource;->setMime(Ljava/lang/String;)V

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\"><en-note><p>Epson iPrint scan data:</p><en-media type=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-virtual {p2}, Lcom/evernote/edam/type/Resource;->getMime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\" hash=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    invoke-virtual {p2}, Lcom/evernote/edam/type/Resource;->getData()Lcom/evernote/edam/type/Data;

    move-result-object v2

    invoke-virtual {v2}, Lcom/evernote/edam/type/Data;->getBodyHash()[B

    move-result-object v2

    invoke-static {v2}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->access$200([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\"/>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "</en-note>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 220
    new-instance v2, Lcom/evernote/edam/type/ResourceAttributes;

    invoke-direct {v2}, Lcom/evernote/edam/type/ResourceAttributes;-><init>()V

    .line 221
    invoke-virtual {v2, p3}, Lcom/evernote/edam/type/ResourceAttributes;->setFileName(Ljava/lang/String;)V

    .line 222
    invoke-virtual {p2, v2}, Lcom/evernote/edam/type/Resource;->setAttributes(Lcom/evernote/edam/type/ResourceAttributes;)V

    .line 223
    new-instance v2, Lcom/evernote/edam/type/Note;

    invoke-direct {v2}, Lcom/evernote/edam/type/Note;-><init>()V

    .line 224
    invoke-virtual {v2, p3}, Lcom/evernote/edam/type/Note;->setTitle(Ljava/lang/String;)V

    .line 225
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/evernote/edam/type/Note;->setCreated(J)V

    .line 226
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/evernote/edam/type/Note;->setUpdated(J)V

    const/4 p3, 0x1

    .line 227
    invoke-virtual {v2, p3}, Lcom/evernote/edam/type/Note;->setActive(Z)V

    .line 228
    invoke-virtual {v2, p3}, Lcom/evernote/edam/type/Note;->setResourcesIsSet(Z)V

    .line 229
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->getGuid()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/evernote/edam/type/Note;->setNotebookGuid(Ljava/lang/String;)V

    .line 230
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    .line 231
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    invoke-virtual {v2, p1}, Lcom/evernote/edam/type/Note;->setResources(Ljava/util/List;)V

    .line 233
    invoke-virtual {v2, v1}, Lcom/evernote/edam/type/Note;->setContent(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->createNote(Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;

    .line 237
    iget-boolean p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;->canceled:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    :goto_0
    return-object p1
.end method

.class Lcom/epson/iprint/storage/evernote/EvernoteClient$3;
.super Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.source "EvernoteClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/evernote/EvernoteClient;->getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;
    }
.end annotation


# instance fields
.field enumerated:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

.field request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

.field final synthetic this$0:Lcom/epson/iprint/storage/evernote/EvernoteClient;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/evernote/EvernoteClient;Landroid/content/Context;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->this$0:Lcom/epson/iprint/storage/evernote/EvernoteClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->val$context:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    .line 153
    new-instance p1, Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    iget-object p2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->val$context:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    return-void
.end method


# virtual methods
.method public enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 2

    .line 199
    iput-object p2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->enumerated:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    .line 200
    new-instance p2, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;

    invoke-direct {p2, p0}, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;-><init>(Lcom/epson/iprint/storage/evernote/EvernoteClient$3;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/epson/iprint/storage/StorageItem;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p2, v0}, Lcom/epson/iprint/storage/evernote/EvernoteClient$3$EnumerateTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method getPrintableItems(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;"
        }
    .end annotation

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 158
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/iprint/storage/StorageItem;

    .line 160
    iget-object v2, v1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    sget-object v3, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->this$0:Lcom/epson/iprint/storage/evernote/EvernoteClient;

    invoke-static {v2, v1}, Lcom/epson/iprint/storage/evernote/EvernoteClient;->access$000(Lcom/epson/iprint/storage/evernote/EvernoteClient;Lcom/epson/iprint/storage/StorageItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public getRootItem()Lcom/epson/iprint/storage/StorageItem;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$3;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->getFeedItem()Lcom/epson/iprint/storage/StorageItem;

    move-result-object v0

    return-object v0
.end method

.class Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;
.super Ljava/lang/Object;
.source "EvernoteRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/evernote/EvernoteRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadHandler"
.end annotation


# instance fields
.field private canceled:Z

.field final synthetic this$0:Lcom/epson/iprint/storage/evernote/EvernoteRequest;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/evernote/EvernoteRequest;)V
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;->this$0:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 249
    iput-boolean p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;->canceled:Z

    return-void
.end method


# virtual methods
.method cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 284
    iput-boolean v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;->canceled:Z

    return-void
.end method

.method download(Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 258
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;->this$0:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-static {v0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->access$000(Lcom/epson/iprint/storage/evernote/EvernoteRequest;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getResource(Ljava/lang/String;ZZZZ)Lcom/evernote/edam/type/Resource;

    move-result-object p1

    .line 262
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->getAttributes()Lcom/evernote/edam/type/ResourceAttributes;

    move-result-object v0

    .line 263
    invoke-virtual {v0}, Lcom/evernote/edam/type/ResourceAttributes;->getFileName()Ljava/lang/String;

    .line 264
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->getData()Lcom/evernote/edam/type/Data;

    move-result-object p1

    .line 266
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 267
    new-instance p2, Ljava/io/FileOutputStream;

    invoke-direct {p2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 269
    :try_start_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/Data;->getBody()[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    :try_start_1
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 280
    :catch_0
    iget-boolean p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;->canceled:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    :goto_0
    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 271
    :try_start_2
    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 274
    :goto_1
    :try_start_3
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 277
    :catch_2
    throw p1
.end method

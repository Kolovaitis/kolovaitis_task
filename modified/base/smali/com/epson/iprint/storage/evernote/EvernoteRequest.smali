.class public Lcom/epson/iprint/storage/evernote/EvernoteRequest;
.super Ljava/lang/Object;
.source "EvernoteRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;,
        Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;
    }
.end annotation


# static fields
.field private static final NOTE_PREFIX:Ljava/lang/String; = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\"><en-note>"

.field private static final NOTE_SUFFIX:Ljava/lang/String; = "</en-note>"

.field private static final TAG:Ljava/lang/String; = "EvernoteRequest"

.field private static final URI_FILE_DEMI:Ljava/lang/String; = "/"

.field private static final URI_FILE_FEED:Ljava/lang/String; = "root"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 55
    iput-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->mContext:Landroid/content/Context;

    .line 58
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/evernote/EvernoteRequest;)Landroid/content/Context;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$100(Ljava/lang/String;)Lcom/evernote/edam/type/Data;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .line 43
    invoke-static {p0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->readFileAsData(Ljava/lang/String;)Lcom/evernote/edam/type/Data;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200([B)Ljava/lang/String;
    .locals 0

    .line 43
    invoke-static {p0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->bytesToHex([B)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static bytesToHex([B)Ljava/lang/String;
    .locals 5

    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 360
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-byte v3, p0, v2

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0x10

    if-ge v3, v4, :cond_0

    const/16 v4, 0x30

    .line 363
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 365
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 367
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static readFileAsData(Ljava/lang/String;)Lcom/evernote/edam/type/Data;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .line 333
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 334
    new-instance p0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x2800

    .line 335
    new-array v1, v1, [B

    .line 338
    :goto_0
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v3, 0x0

    .line 339
    invoke-virtual {p0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 345
    :cond_0
    :goto_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catchall_0
    move-exception p0

    goto :goto_3

    :catch_0
    move-exception v1

    .line 342
    :try_start_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 350
    :catch_1
    :goto_2
    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    .line 351
    new-instance v0, Lcom/evernote/edam/type/Data;

    invoke-direct {v0}, Lcom/evernote/edam/type/Data;-><init>()V

    .line 352
    array-length v1, p0

    invoke-virtual {v0, v1}, Lcom/evernote/edam/type/Data;->setSize(I)V

    const-string v1, "MD5"

    .line 353
    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/type/Data;->setBodyHash([B)V

    .line 354
    invoke-virtual {v0, p0}, Lcom/evernote/edam/type/Data;->setBody([B)V

    return-object v0

    .line 345
    :goto_3
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 348
    :catch_2
    throw p0
.end method


# virtual methods
.method getDownloadHandler()Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;
    .locals 1

    .line 322
    new-instance v0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;-><init>(Lcom/epson/iprint/storage/evernote/EvernoteRequest;)V

    return-object v0
.end method

.method public getFeedItem()Lcom/epson/iprint/storage/StorageItem;
    .locals 2

    .line 67
    new-instance v0, Lcom/epson/iprint/storage/StorageItem;

    invoke-direct {v0}, Lcom/epson/iprint/storage/StorageItem;-><init>()V

    const-string v1, "root"

    .line 68
    iput-object v1, v0, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 69
    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object v1, v0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    return-object v0
.end method

.method public getItemList(Lcom/epson/iprint/storage/StorageItem;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/iprint/storage/StorageItem;",
            ")",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object v0

    .line 82
    iget-object v1, p1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    sget-object v2, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    if-ne v1, v2, :cond_6

    .line 86
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "root"

    .line 88
    iget-object v3, p1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->listNotebooks()Ljava/util/List;

    move-result-object p1

    .line 90
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/edam/type/Notebook;

    .line 91
    new-instance v2, Lcom/epson/iprint/storage/StorageItem;

    invoke-direct {v2}, Lcom/epson/iprint/storage/StorageItem;-><init>()V

    .line 93
    invoke-virtual {v0}, Lcom/evernote/edam/type/Notebook;->getGuid()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 94
    invoke-virtual {v0}, Lcom/evernote/edam/type/Notebook;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    .line 95
    sget-object v3, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object v3, v2, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 96
    iput-object v0, v2, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    .line 98
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    :cond_0
    new-instance v2, Lcom/evernote/edam/notestore/NoteFilter;

    invoke-direct {v2}, Lcom/evernote/edam/notestore/NoteFilter;-><init>()V

    .line 102
    sget-object v3, Lcom/evernote/edam/type/NoteSortOrder;->UPDATED:Lcom/evernote/edam/type/NoteSortOrder;

    invoke-virtual {v3}, Lcom/evernote/edam/type/NoteSortOrder;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/evernote/edam/notestore/NoteFilter;->setOrder(I)V

    const/4 v3, 0x0

    .line 103
    invoke-virtual {v2, v3}, Lcom/evernote/edam/notestore/NoteFilter;->setAscending(Z)V

    .line 104
    iget-object v4, p1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/evernote/edam/notestore/NoteFilter;->setNotebookGuid(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findNoteCounts(Lcom/evernote/edam/notestore/NoteFilter;Z)Lcom/evernote/edam/notestore/NoteCollectionCounts;

    move-result-object v4

    .line 106
    invoke-virtual {v4}, Lcom/evernote/edam/notestore/NoteCollectionCounts;->getNotebookCounts()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 108
    iget-object p1, p1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_1

    .line 112
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    return-object p1

    .line 115
    :cond_1
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, v2, v3, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findNotes(Lcom/evernote/edam/notestore/NoteFilter;II)Lcom/evernote/edam/notestore/NoteList;

    move-result-object p1

    if-nez p1, :cond_2

    .line 120
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    return-object p1

    .line 123
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteList;->getNotes()Ljava/util/List;

    move-result-object p1

    .line 124
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/edam/type/Note;

    .line 125
    invoke-virtual {v0}, Lcom/evernote/edam/type/Note;->getResources()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 127
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/edam/type/Resource;

    .line 128
    invoke-virtual {v2}, Lcom/evernote/edam/type/Resource;->getAttributes()Lcom/evernote/edam/type/ResourceAttributes;

    move-result-object v3

    .line 129
    invoke-virtual {v3}, Lcom/evernote/edam/type/ResourceAttributes;->getFileName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    goto :goto_1

    .line 134
    :cond_4
    new-instance v3, Lcom/epson/iprint/storage/StorageItem;

    invoke-direct {v3}, Lcom/epson/iprint/storage/StorageItem;-><init>()V

    .line 136
    invoke-virtual {v2}, Lcom/evernote/edam/type/Resource;->getGuid()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 137
    invoke-virtual {v2}, Lcom/evernote/edam/type/Resource;->getAttributes()Lcom/evernote/edam/type/ResourceAttributes;

    move-result-object v4

    invoke-virtual {v4}, Lcom/evernote/edam/type/ResourceAttributes;->getFileName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    .line 138
    sget-object v4, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object v4, v3, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 139
    iput-object v2, v3, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    .line 141
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    return-object v1

    .line 83
    :cond_6
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Invalid ItemType"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method getUploadFolder()Lcom/epson/iprint/storage/StorageItem;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 158
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->listNotebooks()Ljava/util/List;

    move-result-object v0

    .line 163
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/edam/type/Notebook;

    const-string v3, "Epson iPrint"

    .line 164
    invoke-virtual {v2}, Lcom/evernote/edam/type/Notebook;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 165
    new-instance v1, Lcom/epson/iprint/storage/StorageItem;

    invoke-direct {v1}, Lcom/epson/iprint/storage/StorageItem;-><init>()V

    .line 166
    invoke-virtual {v2}, Lcom/evernote/edam/type/Notebook;->getGuid()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 167
    invoke-virtual {v2}, Lcom/evernote/edam/type/Notebook;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    .line 168
    sget-object v3, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object v3, v1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 169
    iput-object v2, v1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method getUploadHandler()Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;
    .locals 1

    .line 314
    new-instance v0, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;-><init>(Lcom/epson/iprint/storage/evernote/EvernoteRequest;)V

    return-object v0
.end method

.method public handleException(Ljava/lang/Exception;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    .locals 2

    const-string v0, "EvernoteRequest"

    const-string v1, "handleException"

    .line 294
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 296
    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 297
    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    const-string p1, "EvernoteRequest"

    .line 300
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_1
    iget-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lepson/server/service/EvernoteClient;->logout(Landroid/content/Context;)V

    .line 306
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    return-object p1
.end method

.method newUploadFolder()Lcom/epson/iprint/storage/StorageItem;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 182
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object v0

    .line 184
    new-instance v1, Lcom/evernote/edam/type/Notebook;

    invoke-direct {v1}, Lcom/evernote/edam/type/Notebook;-><init>()V

    const-string v2, "Epson iPrint"

    .line 185
    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/Notebook;->setName(Ljava/lang/String;)V

    .line 187
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->createNotebook(Lcom/evernote/edam/type/Notebook;)Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    .line 189
    new-instance v1, Lcom/epson/iprint/storage/StorageItem;

    invoke-direct {v1}, Lcom/epson/iprint/storage/StorageItem;-><init>()V

    .line 190
    invoke-virtual {v0}, Lcom/evernote/edam/type/Notebook;->getGuid()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 191
    invoke-virtual {v0}, Lcom/evernote/edam/type/Notebook;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    .line 192
    sget-object v2, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object v2, v1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 193
    iput-object v0, v1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    return-object v1
.end method

.class Lcom/epson/iprint/storage/evernote/EvernoteClient$1;
.super Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
.source "EvernoteClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/evernote/EvernoteClient;->getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;
    }
.end annotation


# instance fields
.field request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

.field final synthetic this$0:Lcom/epson/iprint/storage/evernote/EvernoteClient;

.field uploaded:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

.field uploader:Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$localPath:Ljava/lang/String;

.field final synthetic val$uploadFilename:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/evernote/EvernoteClient;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->this$0:Lcom/epson/iprint/storage/evernote/EvernoteClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->val$localPath:Ljava/lang/String;

    iput-object p4, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->val$uploadFilename:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    .line 67
    new-instance p1, Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    iget-object p2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->val$context:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    .line 68
    iget-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->getUploadHandler()Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->uploader:Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->uploader:Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;->cancel()V

    return-void
.end method

.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V
    .locals 1

    .line 98
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->uploaded:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    .line 99
    new-instance p1, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;

    invoke-direct {p1, p0}, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;-><init>(Lcom/epson/iprint/storage/evernote/EvernoteClient$1;)V

    const/4 v0, 0x0

    .line 100
    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

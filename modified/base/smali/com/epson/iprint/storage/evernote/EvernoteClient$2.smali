.class Lcom/epson/iprint/storage/evernote/EvernoteClient$2;
.super Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
.source "EvernoteClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/evernote/EvernoteClient;->getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;
    }
.end annotation


# instance fields
.field downloaded:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

.field downloader:Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;

.field request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

.field final synthetic this$0:Lcom/epson/iprint/storage/evernote/EvernoteClient;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$item:Lcom/epson/iprint/storage/StorageItem;

.field final synthetic val$localPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/evernote/EvernoteClient;Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->this$0:Lcom/epson/iprint/storage/evernote/EvernoteClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->val$item:Lcom/epson/iprint/storage/StorageItem;

    iput-object p4, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->val$localPath:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Downloader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    .line 112
    new-instance p1, Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    iget-object p2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->val$context:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    .line 113
    iget-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->getDownloadHandler()Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->downloader:Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->downloader:Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/evernote/EvernoteRequest$DownloadHandler;->cancel()V

    return-void
.end method

.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V
    .locals 1

    .line 139
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$2;->downloaded:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    .line 140
    new-instance p1, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;

    invoke-direct {p1, p0}, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;-><init>(Lcom/epson/iprint/storage/evernote/EvernoteClient$2;)V

    const/4 v0, 0x0

    .line 141
    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/evernote/EvernoteClient$2$DownloadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

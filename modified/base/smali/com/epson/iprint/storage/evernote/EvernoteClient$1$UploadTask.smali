.class Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "EvernoteClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/evernote/EvernoteClient$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UploadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/evernote/EvernoteClient$1;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 71
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .line 74
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    .line 78
    :cond_0
    :try_start_0
    iget-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object p1, p1, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->getUploadFolder()Lcom/epson/iprint/storage/StorageItem;

    move-result-object p1

    if-nez p1, :cond_1

    .line 80
    iget-object p1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object p1, p1, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->newUploadFolder()Lcom/epson/iprint/storage/StorageItem;

    move-result-object p1

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object v0, v0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->uploader:Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;

    iget-object v1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object v1, v1, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->val$localPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object v2, v2, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->val$uploadFilename:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, Lcom/epson/iprint/storage/evernote/EvernoteRequest$UploadHandler;->upload(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 84
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object v0, v0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->request:Lcom/epson/iprint/storage/evernote/EvernoteRequest;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/evernote/EvernoteRequest;->handleException(Ljava/lang/Exception;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    move-result-object p1

    .line 86
    :goto_0
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->RETRY:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object v0, v0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->uploaded:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    iget-object v1, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object v1, v1, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->val$localPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/iprint/storage/evernote/EvernoteClient$1$UploadTask;->this$1:Lcom/epson/iprint/storage/evernote/EvernoteClient$1;

    iget-object v2, v2, Lcom/epson/iprint/storage/evernote/EvernoteClient$1;->val$uploadFilename:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p1}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    const/4 p1, 0x0

    return-object p1
.end method

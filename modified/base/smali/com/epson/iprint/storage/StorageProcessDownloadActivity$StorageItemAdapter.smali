.class Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;
.super Landroid/widget/ArrayAdapter;
.source "StorageProcessDownloadActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageProcessDownloadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StorageItemAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/epson/iprint/storage/StorageItem;",
        ">;"
    }
.end annotation


# instance fields
.field final COMPONENT_GONE:I

.field layoutInflater:Landroid/view/LayoutInflater;

.field storageItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;"
        }
    .end annotation
.end field

.field taskRunningNow:Z

.field final synthetic this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

.field viewCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/epson/iprint/storage/StorageItem;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;)V"
        }
    .end annotation

    .line 701
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    .line 702
    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0061

    invoke-direct {p0, v0, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const/16 v0, -0x3039

    .line 628
    iput v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->COMPONENT_GONE:I

    .line 703
    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "layout_inflater"

    .line 704
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 705
    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->storageItems:Ljava/util/List;

    .line 706
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->viewCache:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method addLoaedMoreItems(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;)V"
        }
    .end annotation

    .line 765
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->storageItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 766
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->storageItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .line 639
    iget-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->taskRunningNow:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method getIconView(Landroid/view/View;)Landroid/widget/ImageView;
    .locals 1

    const v0, 0x7f080141

    .line 778
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    return-object p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p3    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 658
    iget-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->storageItems:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/storage/StorageItem;

    .line 659
    iget-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->viewCache:Ljava/util/HashMap;

    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    if-nez p2, :cond_2

    .line 661
    iget-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const p3, 0x7f0a0061

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 662
    sget-object p3, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$1;->$SwitchMap$com$epson$iprint$storage$StorageItem$ItemType:[I

    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageItem$ItemType;->ordinal()I

    move-result v0

    aget p3, p3, v0

    const/16 v0, -0x3039

    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 686
    :pswitch_0
    iget-object p3, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    const v1, 0x7f0700b5

    invoke-virtual {p0, p2, p3, v1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->setViewComponents(Landroid/view/View;Ljava/lang/String;II)V

    .line 687
    iget-object p3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {p3}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$600(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    move-result-object p3

    invoke-virtual {p0, p2}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->getIconView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p3, p1, v0}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;->setThumbnailInBackground(Lcom/epson/iprint/storage/StorageItem;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 678
    :pswitch_1
    iget-object p3, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    const v1, 0x7f07009f

    invoke-virtual {p0, p2, p3, v1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->setViewComponents(Landroid/view/View;Ljava/lang/String;II)V

    goto :goto_0

    .line 671
    :pswitch_2
    iget-object p3, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    if-nez p3, :cond_0

    .line 673
    iget-object p3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const v1, 0x7f0e046d

    invoke-virtual {p3, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 675
    :cond_0
    invoke-virtual {p0, p2, p3, v0, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->setViewComponents(Landroid/view/View;Ljava/lang/String;II)V

    goto :goto_0

    .line 664
    :pswitch_3
    iget-object p3, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    const-string v0, ".."

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    const v0, 0x7f0700e9

    if-eqz p3, :cond_1

    .line 665
    iget-object p3, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    const v1, 0x7f070103

    invoke-virtual {p0, p2, p3, v1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->setViewComponents(Landroid/view/View;Ljava/lang/String;II)V

    goto :goto_0

    .line 667
    :cond_1
    iget-object p3, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    const v1, 0x7f0700a0

    invoke-virtual {p0, p2, p3, v1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->setViewComponents(Landroid/view/View;Ljava/lang/String;II)V

    .line 692
    :goto_0
    iget-object p3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->viewCache:Ljava/util/HashMap;

    invoke-virtual {p3, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object p2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isEnabled(I)Z
    .locals 0

    .line 647
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->areAllItemsEnabled()Z

    move-result p1

    return p1
.end method

.method setTaskingStatus(Z)V
    .locals 0

    .line 753
    iput-boolean p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->taskRunningNow:Z

    return-void
.end method

.method setViewComponents(Landroid/view/View;Ljava/lang/String;II)V
    .locals 3

    const v0, 0x7f080142

    .line 722
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 723
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f080141

    .line 728
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    const/16 v0, 0x8

    const/4 v1, 0x0

    const/16 v2, -0x3039

    if-ne p3, v2, :cond_0

    .line 730
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 732
    :cond_0
    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 733
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    const p2, 0x7f080084

    .line 739
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    if-ne p4, v2, :cond_1

    .line 741
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 743
    :cond_1
    invoke-virtual {p1, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 744
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void
.end method

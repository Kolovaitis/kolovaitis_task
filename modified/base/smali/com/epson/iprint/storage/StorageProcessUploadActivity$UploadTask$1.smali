.class Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$1;
.super Ljava/lang/Object;
.source "StorageProcessUploadActivity.java"

# interfaces
.implements Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;)V
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$1;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelRequest(Landroid/content/Intent;)V
    .locals 1

    .line 207
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$1;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iget-object p1, p1, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploader:Lcom/epson/iprint/storage/StorageServiceClient$Uploader;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$1;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iget-object p1, p1, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploader:Lcom/epson/iprint/storage/StorageServiceClient$Uploader;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;->isCancelable()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 208
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$1;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iget-object p1, p1, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploader:Lcom/epson/iprint/storage/StorageServiceClient$Uploader;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;->cancel()V

    .line 210
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$1;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->canceled:Z

    return-void
.end method

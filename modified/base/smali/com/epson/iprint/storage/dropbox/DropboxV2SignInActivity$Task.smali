.class Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;
.super Landroid/os/AsyncTask;
.source "DropboxV2SignInActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Task"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;->this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1

    .line 66
    iget-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;->this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->getTokenAndInitClient(Landroid/content/Context;)V

    .line 68
    invoke-static {}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->getClient()Lcom/dropbox/core/v2/DbxClientV2;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 70
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 76
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/dropbox/core/v2/DbxClientV2;->users()Lcom/dropbox/core/v2/users/DbxUserUsersRequests;

    move-result-object p1

    invoke-virtual {p1}, Lcom/dropbox/core/v2/users/DbxUserUsersRequests;->getCurrentAccount()Lcom/dropbox/core/v2/users/FullAccount;
    :try_end_0
    .catch Lcom/dropbox/core/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    .line 82
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 78
    invoke-virtual {p1}, Lcom/dropbox/core/DbxException;->printStackTrace()V

    .line 79
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 0

    if-eqz p1, :cond_1

    .line 57
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 61
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;->this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;->finish()V

    return-void

    .line 58
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;->this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;

    invoke-static {p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;->access$000(Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 54
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

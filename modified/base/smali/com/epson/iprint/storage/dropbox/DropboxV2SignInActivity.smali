.class public Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;
.super Lcom/epson/iprint/storage/StorageSignInActivity;
.source "DropboxV2SignInActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "DropboxV2SignInActivity"


# instance fields
.field private mAuthActivityStarted:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;)V
    .locals 0

    .line 12
    invoke-virtual {p0}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;->showLoginErrorAndFinish()V

    return-void
.end method

.method private startAuthActivity()V
    .locals 0

    .line 26
    invoke-static {p0}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->init(Landroid/content/Context;)V

    .line 27
    invoke-static {p0}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->startAuthentication(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getBasicSignIn()Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 19
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 33
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;->onResume()V

    .line 35
    iget-boolean v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;->mAuthActivityStarted:Z

    if-nez v0, :cond_0

    .line 36
    invoke-direct {p0}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;->startAuthActivity()V

    const/4 v0, 0x1

    .line 37
    iput-boolean v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;->mAuthActivityStarted:Z

    goto :goto_0

    .line 40
    :cond_0
    new-instance v0, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;-><init>(Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity$Task;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void
.end method

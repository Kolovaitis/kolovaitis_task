.class public Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;
.super Ljava/lang/Object;
.source "DropboxClientFactory.java"


# static fields
.field private static final CLIENT_IDENTIFIER:Ljava/lang/String; = "Epson-iPrint/6.3"

.field private static sDbxClient:Lcom/dropbox/core/v2/DbxClientV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteClient()V
    .locals 1

    const/4 v0, 0x0

    .line 37
    sput-object v0, Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;->sDbxClient:Lcom/dropbox/core/v2/DbxClientV2;

    return-void
.end method

.method public static getClient()Lcom/dropbox/core/v2/DbxClientV2;
    .locals 1

    .line 29
    sget-object v0, Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;->sDbxClient:Lcom/dropbox/core/v2/DbxClientV2;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public static init(Ljava/lang/String;)V
    .locals 3

    .line 16
    sget-object v0, Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;->sDbxClient:Lcom/dropbox/core/v2/DbxClientV2;

    if-nez v0, :cond_0

    const-string v0, "Epson-iPrint/6.3"

    .line 17
    invoke-static {v0}, Lcom/dropbox/core/DbxRequestConfig;->newBuilder(Ljava/lang/String;)Lcom/dropbox/core/DbxRequestConfig$Builder;

    move-result-object v0

    new-instance v1, Lcom/dropbox/core/http/OkHttp3Requestor;

    .line 18
    invoke-static {}, Lcom/dropbox/core/http/OkHttp3Requestor;->defaultOkHttpClient()Lokhttp3/OkHttpClient;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/core/http/OkHttp3Requestor;-><init>(Lokhttp3/OkHttpClient;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/core/DbxRequestConfig$Builder;->withHttpRequestor(Lcom/dropbox/core/http/HttpRequestor;)Lcom/dropbox/core/DbxRequestConfig$Builder;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/dropbox/core/DbxRequestConfig$Builder;->build()Lcom/dropbox/core/DbxRequestConfig;

    move-result-object v0

    .line 21
    new-instance v1, Lcom/dropbox/core/v2/DbxClientV2;

    invoke-direct {v1, v0, p0}, Lcom/dropbox/core/v2/DbxClientV2;-><init>(Lcom/dropbox/core/DbxRequestConfig;Ljava/lang/String;)V

    sput-object v1, Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;->sDbxClient:Lcom/dropbox/core/v2/DbxClientV2;

    :cond_0
    return-void
.end method

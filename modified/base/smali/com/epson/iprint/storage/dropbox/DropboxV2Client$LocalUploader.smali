.class Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;
.super Ljava/lang/Object;
.source "DropboxV2Client.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/dropbox/DropboxV2Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocalUploader"
.end annotation


# instance fields
.field private mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

.field private mOrgFilePath:Ljava/lang/String;

.field private mUploadFilename:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V
    .locals 0
    .param p3    # Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    iput-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    .line 333
    iput-object p2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mUploadFilename:Ljava/lang/String;

    .line 334
    iput-object p3, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 339
    iget-object v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mUploadFilename:Ljava/lang/String;

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 345
    :cond_0
    invoke-static {}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->getClient()Lcom/dropbox/core/v2/DbxClientV2;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 347
    iget-object v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v0, v1, v1, v2}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void

    .line 351
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/Epson iPrint/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mUploadFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 357
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dropbox/core/DbxException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 359
    :try_start_1
    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxClientV2;->files()Lcom/dropbox/core/v2/files/DbxUserFilesRequests;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/dropbox/core/v2/files/DbxUserFilesRequests;->uploadBuilder(Ljava/lang/String;)Lcom/dropbox/core/v2/files/UploadBuilder;

    move-result-object v0

    sget-object v1, Lcom/dropbox/core/v2/files/WriteMode;->ADD:Lcom/dropbox/core/v2/files/WriteMode;

    invoke-virtual {v0, v1}, Lcom/dropbox/core/v2/files/UploadBuilder;->withMode(Lcom/dropbox/core/v2/files/WriteMode;)Lcom/dropbox/core/v2/files/UploadBuilder;

    move-result-object v0

    const/4 v1, 0x1

    .line 360
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/core/v2/files/UploadBuilder;->withAutorename(Ljava/lang/Boolean;)Lcom/dropbox/core/v2/files/UploadBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/dropbox/core/v2/files/UploadBuilder;->uploadAndFinish(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/core/v2/files/FileMetadata;
    :try_end_1
    .catch Lcom/dropbox/core/DbxException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    if-nez v0, :cond_2

    .line 377
    iget-object v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    iget-object v1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mUploadFilename:Ljava/lang/String;

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v0, v1, v2, v3}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    .line 380
    :cond_2
    iget-object v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    iget-object v1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mUploadFilename:Ljava/lang/String;

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v0, v1, v2, v3}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void

    :catchall_0
    move-exception v0

    move-object v1, v3

    goto :goto_1

    :catch_1
    move-object v1, v3

    goto :goto_0

    :catchall_1
    move-exception v0

    goto :goto_1

    .line 362
    :catch_2
    :goto_0
    :try_start_3
    iget-object v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    iget-object v2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    iget-object v3, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mUploadFilename:Ljava/lang/String;

    sget-object v4, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v0, v2, v3, v4}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_3

    .line 368
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    :cond_3
    return-void

    :goto_1
    if-eqz v1, :cond_4

    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 371
    :catch_4
    :cond_4
    throw v0

    .line 340
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    iget-object v1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;->mUploadFilename:Ljava/lang/String;

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v0, v1, v2, v3}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void
.end method

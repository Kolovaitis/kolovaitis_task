.class public Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;
.super Ljava/lang/Object;
.source "DropboxV2Adapter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearToken(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 112
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object p0

    const-string v0, "FolderViewer.DROPBOX_V2_TOKEN"

    .line 113
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    .line 115
    invoke-static {}, Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;->deleteClient()V

    return-void
.end method

.method private static eraseOldData()V
    .locals 5

    .line 64
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object v0

    const-string v1, "FolderViewer.DROPBOX_ACCESS_KEY"

    const-string v2, "FolderViewer.DROPBOX_ACCESS_SECRET"

    const-string v3, "FolderViewer.ROPBOX_PASSWORD"

    const-string v4, "FolderViewer.DROPBOX_USERNAME"

    .line 66
    filled-new-array {v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v1

    .line 74
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    .line 75
    invoke-virtual {v0, v4}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static getClient()Lcom/dropbox/core/v2/DbxClientV2;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 108
    invoke-static {}, Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;->getClient()Lcom/dropbox/core/v2/DbxClientV2;

    move-result-object v0

    return-object v0
.end method

.method private static getSavedToken(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 85
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object p0

    const-string v0, "FolderViewer.DROPBOX_V2_TOKEN"

    .line 86
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageSecureStore;->fetch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getTokenAndInitClient(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 50
    invoke-static {}, Lcom/dropbox/core/android/Auth;->getOAuth2Token()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 53
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object v0

    const-string v1, "FolderViewer.DROPBOX_V2_TOKEN"

    .line 54
    sget-object v2, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->OVERWRITE_IF_EXIST:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    invoke-virtual {v0, v1, p0, v2}, Lcom/epson/iprint/storage/StorageSecureStore;->put(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;)Z

    .line 57
    invoke-static {}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->eraseOldData()V

    .line 59
    invoke-static {p0}, Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;->init(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static hasToken(Landroid/content/Context;)Z
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 80
    invoke-static {p0}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->getSavedToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 34
    invoke-static {p0}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->getSavedToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 36
    invoke-static {p0}, Lcom/epson/iprint/storage/dropbox/DropboxClientFactory;->init(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static startAuthentication(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 96
    new-instance v0, Lcom/epson/iprint/storage/SecureKeyStore;

    invoke-direct {v0}, Lcom/epson/iprint/storage/SecureKeyStore;-><init>()V

    invoke-virtual {v0, p0}, Lcom/epson/iprint/storage/SecureKeyStore;->getApiKeyA(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/dropbox/core/android/Auth;->startOAuth2Authentication(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

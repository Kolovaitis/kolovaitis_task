.class Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;
.super Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
.source "DropboxV2Client.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/dropbox/DropboxV2Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DropboxDownloader"
.end annotation


# instance fields
.field private mStorageItem:Lcom/epson/iprint/storage/StorageItem;

.field private mWriteFilename:Ljava/lang/String;

.field final synthetic this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2Client;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/dropbox/DropboxV2Client;Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;->this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2Client;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Downloader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    .line 217
    iput-object p3, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;->mStorageItem:Lcom/epson/iprint/storage/StorageItem;

    .line 218
    iput-object p4, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;->mWriteFilename:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V
    .locals 3

    .line 235
    new-instance v0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;

    invoke-direct {v0, p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;-><init>(Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;->mStorageItem:Lcom/epson/iprint/storage/StorageItem;

    const/4 v2, 0x0

    aput-object v1, p1, v2

    iget-object v1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;->mWriteFilename:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, p1, v2

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

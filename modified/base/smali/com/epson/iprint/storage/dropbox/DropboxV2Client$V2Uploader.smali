.class Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;
.super Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
.source "DropboxV2Client.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/dropbox/DropboxV2Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "V2Uploader"
.end annotation


# instance fields
.field private mOrgFilePath:Ljava/lang/String;

.field private mUploadPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2Client;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/dropbox/DropboxV2Client;Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;->this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2Client;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    .line 308
    iput-object p4, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;->mOrgFilePath:Ljava/lang/String;

    .line 309
    iput-object p5, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;->mUploadPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V
    .locals 4

    .line 319
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;

    iget-object v2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;->mOrgFilePath:Ljava/lang/String;

    iget-object v3, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;->mUploadPath:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

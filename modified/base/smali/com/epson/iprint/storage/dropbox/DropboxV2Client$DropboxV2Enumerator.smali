.class Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxV2Enumerator;
.super Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.source "DropboxV2Client.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/dropbox/DropboxV2Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DropboxV2Enumerator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2Client;


# direct methods
.method private constructor <init>(Lcom/epson/iprint/storage/dropbox/DropboxV2Client;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxV2Enumerator;->this$0:Lcom/epson/iprint/storage/dropbox/DropboxV2Client;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/iprint/storage/dropbox/DropboxV2Client;Lcom/epson/iprint/storage/dropbox/DropboxV2Client$1;)V
    .locals 0

    .line 113
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxV2Enumerator;-><init>(Lcom/epson/iprint/storage/dropbox/DropboxV2Client;)V

    return-void
.end method

.method private convertToStorageItem(Lcom/dropbox/core/v2/files/Metadata;)Lcom/epson/iprint/storage/StorageItem;
    .locals 4
    .param p1    # Lcom/dropbox/core/v2/files/Metadata;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 182
    instance-of v0, p1, Lcom/dropbox/core/v2/files/FileMetadata;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 183
    invoke-virtual {p1}, Lcom/dropbox/core/v2/files/Metadata;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/iprint/storage/StorageServiceClient;->isPrintableFilename(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object v1

    .line 187
    :cond_0
    sget-object v0, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    goto :goto_0

    .line 188
    :cond_1
    instance-of v0, p1, Lcom/dropbox/core/v2/files/FolderMetadata;

    if-eqz v0, :cond_2

    .line 189
    sget-object v0, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    .line 194
    :goto_0
    new-instance v1, Lcom/epson/iprint/storage/StorageItem;

    invoke-virtual {p1}, Lcom/dropbox/core/v2/files/Metadata;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/core/v2/files/Metadata;->getPathLower()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0, p1}, Lcom/epson/iprint/storage/StorageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageItem$ItemType;Ljava/lang/Object;)V

    return-object v1

    :cond_2
    return-object v1
.end method


# virtual methods
.method public cleanUpResources()V
    .locals 0

    return-void
.end method

.method public enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 4

    .line 129
    invoke-static {}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->getClient()Lcom/dropbox/core/v2/DbxClientV2;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 131
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {p2, v1, p1}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void

    .line 135
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 137
    iget-object p1, p1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 140
    :try_start_0
    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxClientV2;->files()Lcom/dropbox/core/v2/files/DbxUserFilesRequests;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/dropbox/core/v2/files/DbxUserFilesRequests;->listFolder(Ljava/lang/String;)Lcom/dropbox/core/v2/files/ListFolderResult;

    move-result-object p1
    :try_end_0
    .catch Lcom/dropbox/core/DbxException; {:try_start_0 .. :try_end_0} :catch_1

    .line 148
    :goto_0
    invoke-virtual {p1}, Lcom/dropbox/core/v2/files/ListFolderResult;->getEntries()Ljava/util/List;

    move-result-object v1

    .line 149
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dropbox/core/v2/files/Metadata;

    .line 150
    invoke-direct {p0, v3}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxV2Enumerator;->convertToStorageItem(Lcom/dropbox/core/v2/files/Metadata;)Lcom/epson/iprint/storage/StorageItem;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 152
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 156
    :cond_2
    invoke-virtual {p1}, Lcom/dropbox/core/v2/files/ListFolderResult;->getHasMore()Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_2

    .line 161
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxClientV2;->files()Lcom/dropbox/core/v2/files/DbxUserFilesRequests;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/core/v2/files/ListFolderResult;->getCursor()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/dropbox/core/v2/files/DbxUserFilesRequests;->listFolderContinue(Ljava/lang/String;)Lcom/dropbox/core/v2/files/ListFolderResult;

    move-result-object p1
    :try_end_1
    .catch Lcom/dropbox/core/DbxException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 170
    :catch_0
    :goto_2
    new-instance p1, Lcom/epson/iprint/storage/StorageItem$NameComparator;

    invoke-direct {p1}, Lcom/epson/iprint/storage/StorageItem$NameComparator;-><init>()V

    invoke-static {v2, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 172
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {p2, v2, p1}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void

    :catch_1
    move-exception p1

    .line 142
    invoke-virtual {p1}, Lcom/dropbox/core/DbxException;->printStackTrace()V

    .line 143
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {p2, v1, p1}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void
.end method

.method public getRootItem()Lcom/epson/iprint/storage/StorageItem;
    .locals 5

    .line 199
    new-instance v0, Lcom/epson/iprint/storage/StorageItem;

    const-string v1, ""

    const-string v2, ""

    sget-object v3, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/epson/iprint/storage/StorageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageItem$ItemType;Ljava/lang/Object;)V

    return-object v0
.end method

.method public setThumbnailInBackground(Lcom/epson/iprint/storage/StorageItem;Landroid/widget/ImageView;)V
    .locals 0

    return-void
.end method

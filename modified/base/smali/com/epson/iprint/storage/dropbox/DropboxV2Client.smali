.class public Lcom/epson/iprint/storage/dropbox/DropboxV2Client;
.super Lcom/epson/iprint/storage/StorageServiceClient;
.source "DropboxV2Client.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/dropbox/DropboxV2Client$LocalUploader;,
        Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;,
        Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;,
        Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;,
        Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxV2Enumerator;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageServiceClient;-><init>()V

    return-void
.end method


# virtual methods
.method public getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
    .locals 1

    .line 40
    new-instance v0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxDownloader;-><init>(Lcom/epson/iprint/storage/dropbox/DropboxV2Client;Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
    .locals 1

    .line 45
    new-instance p1, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxV2Enumerator;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DropboxV2Enumerator;-><init>(Lcom/epson/iprint/storage/dropbox/DropboxV2Client;Lcom/epson/iprint/storage/dropbox/DropboxV2Client$1;)V

    return-object p1
.end method

.method public getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    const v0, 0x7f0e0328

    .line 53
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
    .locals 7

    .line 58
    new-instance v6, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$V2Uploader;-><init>(Lcom/epson/iprint/storage/dropbox/DropboxV2Client;Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method public isSignedIn(Landroid/content/Context;)Z
    .locals 0

    .line 64
    invoke-static {p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->init(Landroid/content/Context;)V

    .line 66
    invoke-static {p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->hasToken(Landroid/content/Context;)Z

    move-result p1

    return p1
.end method

.method public isSupportedUploadType(Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method protected isValidUploadName(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "."

    .line 78
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "UTF-8"

    .line 83
    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 84
    array-length v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x100

    if-le v0, v2, :cond_1

    return v1

    :cond_1
    const-string v0, "\\/:,;*?<>|#\u3001\""

    .line 92
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget-char v5, v2, v4

    .line 93
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const/4 v6, -0x1

    if-le v5, v6, :cond_2

    return v1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 98
    :cond_3
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient;->isValidUploadName(Ljava/lang/String;)Z

    move-result p1

    return p1

    :catch_0
    return v1
.end method

.method public revokeSignedInData(Landroid/app/Activity;)Z
    .locals 0

    .line 103
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->clearToken(Landroid/content/Context;)V

    const/4 p1, 0x0

    return p1
.end method

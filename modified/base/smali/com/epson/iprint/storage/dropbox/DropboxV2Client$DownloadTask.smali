.class Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;
.super Landroid/os/AsyncTask;
.source "DropboxV2Client.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/dropbox/DropboxV2Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DownloadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mDownloadCompletion:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V
    .locals 0
    .param p1    # Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 242
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 243
    iput-object p1, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;->mDownloadCompletion:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 239
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 6

    const/4 v0, 0x0

    .line 258
    :try_start_0
    array-length v1, p1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_2

    const/4 v1, 0x0

    aget-object v2, p1, v1

    instance-of v2, v2, Lcom/epson/iprint/storage/StorageItem;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    aget-object v3, p1, v2

    instance-of v3, v3, Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 262
    aget-object v1, p1, v1

    check-cast v1, Lcom/epson/iprint/storage/StorageItem;
    :try_end_0
    .catch Lcom/dropbox/core/DbxException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 263
    :try_start_1
    iget-object v3, v1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    instance-of v3, v3, Lcom/dropbox/core/v2/files/FileMetadata;

    if-eqz v3, :cond_1

    .line 266
    iget-object v3, v1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast v3, Lcom/dropbox/core/v2/files/FileMetadata;

    .line 268
    invoke-static {}, Lcom/epson/iprint/storage/dropbox/DropboxV2Adapter;->getClient()Lcom/dropbox/core/v2/DbxClientV2;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 273
    aget-object p1, p1, v2

    check-cast p1, Ljava/lang/String;
    :try_end_1
    .catch Lcom/dropbox/core/DbxException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 274
    :try_start_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 278
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 280
    invoke-virtual {v4}, Lcom/dropbox/core/v2/DbxClientV2;->files()Lcom/dropbox/core/v2/files/DbxUserFilesRequests;

    move-result-object v2

    invoke-virtual {v3}, Lcom/dropbox/core/v2/files/FileMetadata;->getPathLower()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/core/v2/files/FileMetadata;->getRev()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/dropbox/core/v2/files/DbxUserFilesRequests;->download(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/core/DbxDownloader;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/dropbox/core/DbxDownloader;->download(Ljava/io/OutputStream;)Ljava/lang/Object;
    :try_end_2
    .catch Lcom/dropbox/core/DbxException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3

    .line 290
    iget-object v2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;->mDownloadCompletion:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v2, v1, p1, v3}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-object v0

    .line 270
    :cond_0
    :try_start_3
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 264
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
    :try_end_3
    .catch Lcom/dropbox/core/DbxException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-object p1, v0

    goto :goto_0

    :catch_1
    move-object p1, v0

    goto :goto_1

    .line 259
    :cond_2
    :try_start_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
    :try_end_4
    .catch Lcom/dropbox/core/DbxException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-object p1, v0

    move-object v1, p1

    .line 286
    :catch_3
    :goto_0
    iget-object v2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;->mDownloadCompletion:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v2, v1, p1, v3}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-object v0

    :catch_4
    move-object p1, v0

    move-object v1, p1

    .line 283
    :catch_5
    :goto_1
    iget-object v2, p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client$DownloadTask;->mDownloadCompletion:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v2, v1, p1, v3}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-object v0
.end method

.class public Lcom/epson/iprint/storage/OneButtonDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "OneButtonDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/OneButtonDialogFragment$DialogCallback;
    }
.end annotation


# static fields
.field private static final PARAM_DIALOG_ID:Ljava/lang/String; = "dialog_id"

.field private static final PARAM_MESSAGE_RESOURCE_ID:Ljava/lang/String; = "message_id"


# instance fields
.field private mDialogId:Ljava/lang/String;

.field private mMessageId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/OneButtonDialogFragment;)Ljava/lang/String;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/epson/iprint/storage/OneButtonDialogFragment;->mDialogId:Ljava/lang/String;

    return-object p0
.end method

.method public static newInstance(ILjava/lang/String;)Lcom/epson/iprint/storage/OneButtonDialogFragment;
    .locals 3

    .line 34
    new-instance v0, Lcom/epson/iprint/storage/OneButtonDialogFragment;

    invoke-direct {v0}, Lcom/epson/iprint/storage/OneButtonDialogFragment;-><init>()V

    .line 36
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message_id"

    .line 37
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "dialog_id"

    .line 38
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 39
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/OneButtonDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 46
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/epson/iprint/storage/OneButtonDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "message_id"

    .line 49
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/storage/OneButtonDialogFragment;->mMessageId:I

    const-string v0, "dialog_id"

    .line 50
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/OneButtonDialogFragment;->mDialogId:Ljava/lang/String;

    const/4 p1, 0x0

    .line 52
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/OneButtonDialogFragment;->setCancelable(Z)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 58
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 60
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/epson/iprint/storage/OneButtonDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v0, p0, Lcom/epson/iprint/storage/OneButtonDialogFragment;->mMessageId:I

    .line 61
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lcom/epson/iprint/storage/OneButtonDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/OneButtonDialogFragment$1;-><init>(Lcom/epson/iprint/storage/OneButtonDialogFragment;)V

    const v1, 0x7f0e04f2

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 74
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

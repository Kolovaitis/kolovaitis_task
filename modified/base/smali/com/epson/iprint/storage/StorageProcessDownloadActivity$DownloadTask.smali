.class Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "StorageProcessDownloadActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageProcessDownloadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field item:Lcom/epson/iprint/storage/StorageItem;

.field localPath:Ljava/lang/String;

.field progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

.field final synthetic this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Lcom/epson/iprint/storage/StorageItem;)V
    .locals 1

    .line 351
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    const/4 v0, 0x0

    .line 258
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    .line 259
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->item:Lcom/epson/iprint/storage/StorageItem;

    .line 260
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->localPath:Ljava/lang/String;

    .line 352
    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->item:Lcom/epson/iprint/storage/StorageItem;

    .line 354
    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/epson/iprint/storage/StorageServiceClient;->getDownloadLocalPath(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->localPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 257
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    .line 271
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->item:Lcom/epson/iprint/storage/StorageItem;

    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->localPath:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, Lcom/epson/iprint/storage/StorageServiceClient;->getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;

    move-result-object p1

    .line 273
    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageServiceClient$Downloader;->isCancelable()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 274
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$1;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$1;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;Lcom/epson/iprint/storage/StorageServiceClient$Downloader;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 284
    :goto_0
    new-instance v2, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    iget-object v3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-direct {v2, v3}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;-><init>(Lcom/epson/iprint/storage/StorageActivity;)V

    iput-object v2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    .line 285
    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    iget-object v3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const v4, 0x7f0e0327

    invoke-virtual {v3, v4}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->show(Ljava/lang/String;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V

    .line 286
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$2;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$2;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;)V

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageServiceClient$Downloader;->start(Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V

    return-object v1
.end method

.method protected onPreExecute()V
    .locals 2

    .line 263
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$100(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Z)V

    return-void
.end method

.class public Lcom/epson/iprint/storage/StorageItem$NameComparator;
.super Ljava/lang/Object;
.source "StorageItem.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NameComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/epson/iprint/storage/StorageItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageItem;)I
    .locals 3

    .line 139
    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    const/4 v1, -0x1

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_1

    .line 144
    :cond_0
    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    iget-object v2, p2, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 150
    :cond_1
    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    if-nez v0, :cond_2

    goto :goto_0

    .line 155
    :cond_2
    iget-object p1, p1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iget-object p2, p2, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    invoke-virtual {p1, p2}, Lcom/epson/iprint/storage/StorageItem$ItemType;->compareTo(Ljava/lang/Enum;)I

    move-result p1

    return p1

    :cond_3
    :goto_0
    return v1

    :cond_4
    :goto_1
    return v1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 133
    check-cast p1, Lcom/epson/iprint/storage/StorageItem;

    check-cast p2, Lcom/epson/iprint/storage/StorageItem;

    invoke-virtual {p0, p1, p2}, Lcom/epson/iprint/storage/StorageItem$NameComparator;->compare(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageItem;)I

    move-result p1

    return p1
.end method

.class Lcom/epson/iprint/storage/StorageActivity$3;
.super Ljava/lang/Object;
.source "StorageActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageActivity;->bindClearButton(IILcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/StorageActivity;

.field final synthetic val$clearButton:Landroid/widget/Button;

.field final synthetic val$cleared:Lcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;

.field final synthetic val$editText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageActivity;Landroid/widget/EditText;Landroid/widget/Button;Lcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;)V
    .locals 0

    .line 309
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$3;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    iput-object p2, p0, Lcom/epson/iprint/storage/StorageActivity$3;->val$editText:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/epson/iprint/storage/StorageActivity$3;->val$clearButton:Landroid/widget/Button;

    iput-object p4, p0, Lcom/epson/iprint/storage/StorageActivity$3;->val$cleared:Lcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 313
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$3;->val$editText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 314
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$3;->val$clearButton:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 316
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$3;->val$clearButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 318
    :goto_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$3;->val$cleared:Lcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;

    if-eqz p1, :cond_1

    .line 319
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageActivity$3;->val$clearButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;->onVisibilityChanged(I)V

    :cond_1
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

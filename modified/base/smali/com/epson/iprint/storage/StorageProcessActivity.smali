.class public abstract Lcom/epson/iprint/storage/StorageProcessActivity;
.super Lcom/epson/iprint/storage/StorageActivity;
.source "StorageProcessActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;,
        Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;
    }
.end annotation


# static fields
.field public static final EXTRA_UPLOADFILE_LIST:Ljava/lang/String; = "Extra.Uploadfile.List"

.field private static final StorageServiceNameKey:Ljava/lang/String; = "storage.service.name"


# instance fields
.field private bEnableSighIn:Z

.field private mServerName:Ljava/lang/String;

.field private mStorageServiceClient:Lcom/epson/iprint/storage/StorageServiceClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 40
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageActivity;-><init>()V

    const/4 v0, 0x1

    .line 45
    iput-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->bEnableSighIn:Z

    return-void
.end method

.method public static getProcessIntent(Landroid/content/Context;Ljava/lang/String;Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;)Landroid/content/Intent;
    .locals 1

    .line 86
    sget-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;->DOWNLOAD:Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;

    if-ne p2, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 93
    :cond_0
    const-class p2, Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    .line 96
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 97
    invoke-static {v0, p1}, Lcom/epson/iprint/storage/StorageProcessActivity;->setCommonExtra(Landroid/content/Intent;Ljava/lang/String;)V

    return-object v0
.end method

.method private init()V
    .locals 2

    .line 184
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "storage.service.name"

    .line 185
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 186
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mServerName:Ljava/lang/String;

    .line 187
    invoke-static {v0}, Lcom/epson/iprint/storage/StorageServiceClient;->getClient(Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/StorageServiceClient;

    return-void
.end method

.method private onSignIn()V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mServerName:Ljava/lang/String;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/StorageServiceClient;

    .line 167
    invoke-virtual {v1, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 166
    invoke-static {p0, v0, v1}, Lcom/epson/iprint/storage/StorageSignInActivity;->getStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 168
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onSignOut()V
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/StorageServiceClient;

    invoke-virtual {v0, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->revokeSignedInData(Landroid/app/Activity;)Z

    .line 177
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->updateSignInStatus()V

    return-void
.end method

.method protected static setCommonExtra(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 1

    const-string v0, "storage.service.name"

    .line 102
    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method

.method private updateSignInRequest(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V
    .locals 2

    const v0, 0x1020004

    .line 195
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 196
    sget-object v1, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_IN:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    if-ne p1, v1, :cond_0

    const/16 p1, 0x8

    .line 200
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const p1, 0x7f0e0464

    .line 205
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    const/4 v1, 0x1

    .line 206
    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    const/high16 p1, 0x41900000    # 18.0f

    .line 207
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    const/4 p1, 0x0

    .line 208
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/StorageServiceClient;

    return-object v0
.end method

.method public bridge synthetic isConnected()Z
    .locals 1

    .line 40
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageActivity;->isConnected()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 107
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->onCreate(Landroid/os/Bundle;)V

    .line 108
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->init()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .line 276
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0007

    .line 277
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 280
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/StorageServiceClient;

    invoke-virtual {v0, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->isSignedIn(Landroid/content/Context;)Z

    move-result v0

    const v1, 0x7f080215

    const v2, 0x7f080216

    if-eqz v0, :cond_0

    .line 281
    invoke-interface {p1, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 284
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->bEnableSighIn:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 287
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 290
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->bEnableSighIn:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 293
    :goto_0
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 261
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    .line 270
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 266
    :pswitch_0
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->onSignOut()V

    return v1

    .line 263
    :pswitch_1
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->onSignIn()V

    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x7f080215
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .line 135
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageActivity;->onResume()V

    .line 136
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->updateSignInStatus()V

    return-void
.end method

.method abstract onSignInStatus(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V
.end method

.method protected onStart()V
    .locals 1

    .line 116
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageActivity;->onStart()V

    .line 120
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->updateSignInStatus()V

    .line 125
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/StorageServiceClient;

    invoke-virtual {v0, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->isSignedIn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->onUpdateProcessView()V

    :cond_0
    return-void
.end method

.method abstract onUpdateProcessView()V
.end method

.method setSignInButtonEnabled(Z)V
    .locals 0

    .line 254
    iput-boolean p1, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->bEnableSighIn:Z

    .line 255
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public updateSignInStatus()V
    .locals 1

    const/4 v0, 0x0

    .line 219
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 224
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/StorageServiceClient;

    invoke-virtual {v0, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->isSignedIn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    sget-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_IN:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    goto :goto_0

    .line 227
    :cond_0
    sget-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_OUT:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    .line 233
    :goto_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->invalidateOptionsMenu()V

    .line 238
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/StorageProcessActivity;->updateSignInRequest(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V

    .line 243
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessActivity;->onSignInStatus(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V

    return-void
.end method

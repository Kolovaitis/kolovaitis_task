.class Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;
.super Ljava/lang/Object;
.source "StorageProcessUploadActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

.field final synthetic val$progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;)V
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->val$progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 344
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    invoke-static {v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->access$000(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    move-result-object v0

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-ne v0, v1, :cond_0

    .line 346
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->val$progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->dismiss()V

    .line 347
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->finish()V

    goto :goto_0

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->setEnabledSaveButton(Z)V

    .line 354
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    invoke-static {v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->access$000(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    move-result-object v0

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-ne v0, v1, :cond_1

    .line 355
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    const v1, 0x7f0e053e

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->showErrorDialog(I)V

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;->val$progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->dismiss()V

    :goto_0
    return-void
.end method

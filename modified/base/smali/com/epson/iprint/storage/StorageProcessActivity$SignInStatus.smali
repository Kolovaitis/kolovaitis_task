.class public final enum Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;
.super Ljava/lang/Enum;
.source "StorageProcessActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageProcessActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SignInStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

.field public static final enum SIGNED_IN:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

.field public static final enum SIGNED_OUT:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 59
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    const-string v1, "SIGNED_IN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_IN:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    .line 60
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    const-string v1, "SIGNED_OUT"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_OUT:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    const/4 v0, 0x2

    .line 58
    new-array v0, v0, [Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    sget-object v1, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_IN:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_OUT:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->$VALUES:[Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;
    .locals 1

    .line 58
    const-class v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    return-object p0
.end method

.method public static values()[Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;
    .locals 1

    .line 58
    sget-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->$VALUES:[Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    invoke-virtual {v0}, [Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    return-object v0
.end method

.class Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;
.super Ljava/lang/Object;
.source "StorageProcessUploadActivity.java"

# interfaces
.implements Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

.field final synthetic val$taskLock:Ljava/util/concurrent/locks/Lock;

.field final synthetic val$waitUploadTaskComplete:Ljava/util/concurrent/locks/Condition;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;Ljava/util/concurrent/locks/Lock;Ljava/util/concurrent/locks/Condition;)V
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->val$taskLock:Ljava/util/concurrent/locks/Lock;

    iput-object p3, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->val$waitUploadTaskComplete:Ljava/util/concurrent/locks/Condition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    .locals 1

    .line 297
    :try_start_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->val$taskLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 298
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    invoke-static {p1, p3}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->access$002(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    .line 299
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploaded:Z

    .line 300
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->val$waitUploadTaskComplete:Ljava/util/concurrent/locks/Condition;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    :goto_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->val$taskLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    :try_start_1
    const-string p2, "Storage"

    .line 302
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onUploadComplete() exception <"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ">"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :goto_1
    return-void

    .line 304
    :goto_2
    iget-object p2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;->val$taskLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

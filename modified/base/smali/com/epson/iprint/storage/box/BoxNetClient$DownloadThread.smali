.class Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;
.super Ljava/lang/Thread;
.source "BoxNetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/box/BoxNetClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DownloadThread"
.end annotation


# instance fields
.field private final mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

.field private volatile mCanceled:Z

.field private final mDownloadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

.field private final mLocalPath:Ljava/lang/String;

.field private final mOnlineStorageItem:Lcom/epson/iprint/storage/StorageItem;


# direct methods
.method private constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/box/BoxNetClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/storage/StorageItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "download-box"

    .line 479
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 480
    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    .line 481
    iput-object p2, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mDownloadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    .line 482
    iput-object p3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mOnlineStorageItem:Lcom/epson/iprint/storage/StorageItem;

    .line 483
    iput-object p4, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mLocalPath:Ljava/lang/String;

    const/4 p1, 0x0

    .line 484
    iput-boolean p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mCanceled:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/box/BoxNetClient$1;)V
    .locals 0

    .line 470
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 506
    iput-boolean v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mCanceled:Z

    return-void
.end method

.method public run()V
    .locals 5

    const/4 v0, 0x0

    .line 490
    :try_start_0
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    iget-object v2, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mOnlineStorageItem:Lcom/epson/iprint/storage/StorageItem;

    iget-object v2, v2, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mLocalPath:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/epson/iprint/storage/box/BoxNetClient;->access$400(Lcom/epson/iprint/storage/box/BoxNetClient;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-boolean v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mCanceled:Z

    if-eqz v1, :cond_0

    .line 492
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mDownloadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v1, v0, v0, v2}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    goto :goto_0

    .line 494
    :cond_0
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mDownloadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    iget-object v2, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mOnlineStorageItem:Lcom/epson/iprint/storage/StorageItem;

    iget-object v3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mLocalPath:Ljava/lang/String;

    sget-object v4, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v1, v2, v3, v4}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/box/androidsdk/content/BoxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 497
    iget-boolean v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mCanceled:Z

    if-eqz v1, :cond_1

    .line 498
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mDownloadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v1, v0, v0, v2}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    goto :goto_0

    .line 500
    :cond_1
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->mDownloadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v1, v0, v0, v2}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    :goto_0
    return-void
.end method

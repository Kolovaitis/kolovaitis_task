.class public Lcom/epson/iprint/storage/box/BoxNetClient;
.super Lcom/epson/iprint/storage/StorageServiceClient;
.source "BoxNetClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;,
        Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;,
        Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;
    }
.end annotation


# static fields
.field private static sBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;


# instance fields
.field private mBoxApiFile:Lcom/box/androidsdk/content/BoxApiFile;

.field private mBoxApiFolder:Lcom/box/androidsdk/content/BoxApiFolder;

.field private mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 59
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageServiceClient;-><init>()V

    .line 60
    new-instance v0, Lcom/epson/iprint/storage/box/BoxAuthStorage;

    invoke-direct {v0}, Lcom/epson/iprint/storage/box/BoxAuthStorage;-><init>()V

    .line 61
    invoke-static {}, Lcom/epson/iprint/storage/box/BoxAuthStorage;->deleteOldData()V

    .line 63
    invoke-static {}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getInstance()Lcom/box/androidsdk/content/auth/BoxAuthentication;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->setAuthStorage(Lcom/box/androidsdk/content/auth/BoxAuthentication$AuthStorage;)V

    .line 64
    invoke-direct {p0}, Lcom/epson/iprint/storage/box/BoxNetClient;->configureClient()V

    .line 65
    invoke-virtual {p0}, Lcom/epson/iprint/storage/box/BoxNetClient;->initSession()V

    .line 67
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/box/BoxNetClient;->isSignedIn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 68
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/box/BoxNetClient;->setLoginStatus(Z)V

    :cond_0
    return-void
.end method

.method static synthetic access$300(Lcom/epson/iprint/storage/box/BoxNetClient;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/box/BoxNetClient;->listFolder(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/epson/iprint/storage/box/BoxNetClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/box/BoxNetClient;->downloadFile(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/epson/iprint/storage/box/BoxNetClient;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/iprint/storage/box/BoxNetClient;->uploadFileToFolder(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private configureClient()V
    .locals 3

    .line 73
    new-instance v0, Lcom/epson/iprint/storage/SecureKeyStore;

    invoke-direct {v0}, Lcom/epson/iprint/storage/SecureKeyStore;-><init>()V

    .line 74
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/SecureKeyStore;->getApiKeyC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/box/androidsdk/content/BoxConfig;->CLIENT_ID:Ljava/lang/String;

    .line 76
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/SecureKeyStore;->getSecKeyC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/BoxConfig;->CLIENT_SECRET:Ljava/lang/String;

    return-void
.end method

.method private createFolder(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFolder;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 370
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFolder:Lcom/box/androidsdk/content/BoxApiFolder;

    const-string v1, "0"

    invoke-virtual {v0, v1, p1}, Lcom/box/androidsdk/content/BoxApiFolder;->getCreateRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$CreateFolder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$CreateFolder;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/models/BoxFolder;

    return-object p1
.end method

.method private downloadFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 432
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFile:Lcom/box/androidsdk/content/BoxApiFile;

    if-eqz v0, :cond_0

    .line 436
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 437
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 439
    iget-object p2, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFile:Lcom/box/androidsdk/content/BoxApiFile;

    invoke-virtual {p2, v0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getDownloadRequest(Ljava/io/File;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;

    move-result-object p1

    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;->send()Lcom/box/androidsdk/content/models/BoxObject;

    return-void

    .line 433
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method private filterBoxItem(Lcom/box/androidsdk/content/models/BoxItem;)Z
    .locals 1
    .param p1    # Lcom/box/androidsdk/content/models/BoxItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 427
    instance-of v0, p1, Lcom/box/androidsdk/content/models/BoxFolder;

    if-nez v0, :cond_1

    .line 428
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxItem;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/iprint/storage/StorageServiceClient;->isPrintableFilename(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private findOrCreateFolder(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 299
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/box/BoxNetClient;->searchRootFolder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 304
    :cond_0
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/box/BoxNetClient;->createFolder(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFolder;

    move-result-object p1

    if-nez p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    .line 305
    :cond_1
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxFolder;->getId()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public static declared-synchronized getInstance()Lcom/epson/iprint/storage/box/BoxNetClient;
    .locals 2

    const-class v0, Lcom/epson/iprint/storage/box/BoxNetClient;

    monitor-enter v0

    .line 49
    :try_start_0
    sget-object v1, Lcom/epson/iprint/storage/box/BoxNetClient;->sBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    if-nez v1, :cond_0

    .line 50
    new-instance v1, Lcom/epson/iprint/storage/box/BoxNetClient;

    invoke-direct {v1}, Lcom/epson/iprint/storage/box/BoxNetClient;-><init>()V

    sput-object v1, Lcom/epson/iprint/storage/box/BoxNetClient;->sBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    .line 53
    :cond_0
    sget-object v1, Lcom/epson/iprint/storage/box/BoxNetClient;->sBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private listFolder(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 402
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFolder:Lcom/box/androidsdk/content/BoxApiFolder;

    if-eqz v0, :cond_3

    .line 405
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 407
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFolder:Lcom/box/androidsdk/content/BoxApiFolder;

    invoke-virtual {v1, p1}, Lcom/box/androidsdk/content/BoxApiFolder;->getItemsRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;

    move-result-object p1

    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/models/BoxIteratorItems;

    .line 409
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxIteratorItems;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/box/androidsdk/content/models/BoxItem;

    .line 410
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxItem;->getName()Ljava/lang/String;

    move-result-object v2

    .line 411
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxItem;->getId()Ljava/lang/String;

    move-result-object v3

    .line 412
    invoke-direct {p0, v1}, Lcom/epson/iprint/storage/box/BoxNetClient;->filterBoxItem(Lcom/box/androidsdk/content/models/BoxItem;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 413
    new-instance v4, Lcom/epson/iprint/storage/StorageItem;

    instance-of v1, v1, Lcom/box/androidsdk/content/models/BoxFolder;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    :goto_1
    invoke-direct {v4, v2, v3, v1}, Lcom/epson/iprint/storage/StorageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageItem$ItemType;)V

    .line 416
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0

    .line 403
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method private searchFolderInRoot(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "^"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " .$"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 319
    new-instance v1, Lcom/box/androidsdk/content/BoxApiSearch;

    iget-object v2, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v1, v2}, Lcom/box/androidsdk/content/BoxApiSearch;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 v2, 0x0

    .line 324
    :goto_0
    invoke-virtual {v1, v0}, Lcom/box/androidsdk/content/BoxApiSearch;->getSearchRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    move-result-object v3

    .line 325
    invoke-virtual {v3, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->setOffset(I)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->setLimit(I)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    move-result-object v3

    const-string v4, "folder"

    .line 326
    invoke-virtual {v3, v4}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitType(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    move-result-object v3

    const-string v4, "0"

    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v4

    .line 327
    invoke-virtual {v3, v4}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitAncestorFolderIds([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    move-result-object v3

    .line 328
    invoke-virtual {v3}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v3

    check-cast v3, Lcom/box/androidsdk/content/models/BoxIteratorItems;

    .line 330
    invoke-virtual {v3}, Lcom/box/androidsdk/content/models/BoxIteratorItems;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 331
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 335
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 336
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/box/androidsdk/content/models/BoxItem;

    .line 337
    invoke-virtual {v4}, Lcom/box/androidsdk/content/models/BoxItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 338
    invoke-virtual {v4}, Lcom/box/androidsdk/content/models/BoxItem;->getId()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    add-int/lit8 v2, v2, 0x3

    goto :goto_0
.end method

.method private searchRootFolder(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 353
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFolder:Lcom/box/androidsdk/content/BoxApiFolder;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/box/androidsdk/content/BoxApiFolder;->getItemsRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;

    move-result-object v0

    invoke-virtual {v0}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxIteratorItems;

    .line 354
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxIteratorItems;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/box/androidsdk/content/models/BoxItem;

    .line 355
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxItem;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "folder"

    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxItem;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxItem;->getId()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private uploadFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 266
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 p1, 0x0

    .line 267
    :try_start_0
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFile:Lcom/box/androidsdk/content/BoxApiFile;

    invoke-virtual {v1, v0, p2, p3}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadRequest(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;

    move-result-object p2

    .line 269
    invoke-virtual {p2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;->send()Lcom/box/androidsdk/content/models/BoxObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_0
    move-exception p2

    goto :goto_0

    :catch_0
    move-exception p1

    .line 266
    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-eqz p1, :cond_0

    .line 270
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception p3

    invoke-virtual {p1, p3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :goto_1
    throw p2
.end method

.method private uploadFileToFolder(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 283
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    if-eqz v0, :cond_1

    .line 290
    invoke-direct {p0, p2}, Lcom/epson/iprint/storage/box/BoxNetClient;->findOrCreateFolder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 294
    invoke-direct {p0, p1, p3, p2}, Lcom/epson/iprint/storage/box/BoxNetClient;->uploadOrUpdate(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 292
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "box folder can not create"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 287
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Box session not initialized"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private uploadNewVersion(Ljava/io/File;Lcom/box/androidsdk/content/models/BoxFile;)V
    .locals 2
    .param p1    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/box/androidsdk/content/models/BoxFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 274
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 p1, 0x0

    .line 275
    :try_start_0
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFile:Lcom/box/androidsdk/content/BoxApiFile;

    .line 276
    invoke-virtual {p2}, Lcom/box/androidsdk/content/models/BoxFile;->getId()Ljava/lang/String;

    move-result-object p2

    .line 275
    invoke-virtual {v1, v0, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadNewVersionRequest(Ljava/io/InputStream;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;

    move-result-object p2

    .line 277
    invoke-virtual {p2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;->send()Lcom/box/androidsdk/content/models/BoxObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_0
    move-exception p2

    goto :goto_0

    :catch_0
    move-exception p1

    .line 274
    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-eqz p1, :cond_0

    .line 278
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {p1, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :goto_1
    throw p2
.end method

.method private uploadOrUpdate(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 379
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/iprint/storage/box/BoxNetClient;->uploadFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/box/androidsdk/content/BoxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 381
    invoke-virtual {p2}, Lcom/box/androidsdk/content/BoxException;->getAsBoxError()Lcom/box/androidsdk/content/models/BoxError;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 382
    invoke-virtual {p3}, Lcom/box/androidsdk/content/models/BoxError;->getStatus()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x199

    if-ne v0, v1, :cond_1

    .line 383
    invoke-virtual {p3}, Lcom/box/androidsdk/content/models/BoxError;->getContextInfo()Lcom/box/androidsdk/content/models/BoxError$ErrorContext;

    move-result-object p3

    invoke-virtual {p3}, Lcom/box/androidsdk/content/models/BoxError$ErrorContext;->getConflicts()Ljava/util/ArrayList;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 384
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/box/androidsdk/content/models/BoxFile;

    if-eqz v1, :cond_0

    .line 385
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/box/androidsdk/content/models/BoxFile;

    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/box/BoxNetClient;->uploadNewVersion(Ljava/io/File;Lcom/box/androidsdk/content/models/BoxFile;)V

    :goto_0
    return-void

    .line 387
    :cond_0
    throw p2

    .line 390
    :cond_1
    throw p2
.end method


# virtual methods
.method public authenticate()V
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    if-eqz v0, :cond_0

    .line 238
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/box/androidsdk/content/models/BoxSession;->authenticate(Landroid/content/Context;)Lcom/box/androidsdk/content/BoxFutureTask;

    return-void

    .line 236
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public boxLogout()V
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->logout()Lcom/box/androidsdk/content/BoxFutureTask;

    move-result-object v0

    .line 214
    :try_start_0
    invoke-virtual {v0}, Lcom/box/androidsdk/content/BoxFutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
    .locals 0

    if-eqz p2, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    new-instance p1, Lcom/epson/iprint/storage/box/BoxNetClient$2;

    invoke-direct {p1, p0, p2, p3}, Lcom/epson/iprint/storage/box/BoxNetClient$2;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 143
    new-instance p1, Lcom/epson/iprint/storage/box/BoxNetClient$3;

    invoke-direct {p1, p0}, Lcom/epson/iprint/storage/box/BoxNetClient$3;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;)V

    return-object p1
.end method

.method public getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    const v0, 0x7f0e02b7

    .line 205
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
    .locals 0

    .line 82
    new-instance p1, Lcom/epson/iprint/storage/box/BoxNetClient$1;

    invoke-direct {p1, p0, p3, p4}, Lcom/epson/iprint/storage/box/BoxNetClient$1;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public initSession()V
    .locals 2

    .line 221
    new-instance v0, Lcom/box/androidsdk/content/models/BoxSession;

    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/box/androidsdk/content/models/BoxSession;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    return-void
.end method

.method public isSignedIn(Landroid/content/Context;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 191
    :cond_0
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    if-eqz v1, :cond_1

    .line 192
    invoke-static {}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getInstance()Lcom/box/androidsdk/content/auth/BoxAuthentication;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getLastAuthenticatedUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public isSupportedUploadType(Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public revokeSignedInData(Landroid/app/Activity;)Z
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 198
    invoke-static {p1}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->getLogoutIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 p1, 0x1

    return p1
.end method

.method public setLoginStatus(Z)V
    .locals 1

    if-eqz p1, :cond_1

    .line 243
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    if-nez p1, :cond_0

    return-void

    .line 249
    :cond_0
    new-instance v0, Lcom/box/androidsdk/content/BoxApiFolder;

    invoke-direct {v0, p1}, Lcom/box/androidsdk/content/BoxApiFolder;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFolder:Lcom/box/androidsdk/content/BoxApiFolder;

    .line 250
    new-instance p1, Lcom/box/androidsdk/content/BoxApiFile;

    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {p1, v0}, Lcom/box/androidsdk/content/BoxApiFile;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFile:Lcom/box/androidsdk/content/BoxApiFile;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 252
    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFolder:Lcom/box/androidsdk/content/BoxApiFolder;

    .line 253
    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxApiFile:Lcom/box/androidsdk/content/BoxApiFile;

    :goto_0
    return-void
.end method

.method public setSessionAuthListener(Lcom/box/androidsdk/content/auth/BoxAuthentication$AuthListener;)V
    .locals 1
    .param p1    # Lcom/box/androidsdk/content/auth/BoxAuthentication$AuthListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 228
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient;->mBoxSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-virtual {v0, p1}, Lcom/box/androidsdk/content/models/BoxSession;->setSessionAuthListener(Lcom/box/androidsdk/content/auth/BoxAuthentication$AuthListener;)V

    return-void
.end method

.class Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;
.super Ljava/lang/Thread;
.source "BoxNetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/box/BoxNetClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UploadThread"
.end annotation


# instance fields
.field private final mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

.field private final mOrigLocalFilePath:Ljava/lang/String;

.field private final mUploadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

.field private final mUploadOnlineName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/box/BoxNetClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "upload-box"

    .line 518
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 519
    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    .line 520
    iput-object p2, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mUploadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    .line 521
    iput-object p3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mOrigLocalFilePath:Ljava/lang/String;

    .line 522
    iput-object p4, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mUploadOnlineName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/box/BoxNetClient$1;)V
    .locals 0

    .line 510
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 527
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mOrigLocalFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 529
    :try_start_0
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    const-string v2, "Epson iPrint"

    iget-object v3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mUploadOnlineName:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/epson/iprint/storage/box/BoxNetClient;->access$500(Lcom/epson/iprint/storage/box/BoxNetClient;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mUploadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mOrigLocalFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mUploadOnlineName:Ljava/lang/String;

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v0, v1, v2, v3}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    :try_end_0
    .catch Lcom/box/androidsdk/content/BoxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 532
    :catch_0
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->mUploadEndListener:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v2, v1}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    :goto_0
    return-void
.end method

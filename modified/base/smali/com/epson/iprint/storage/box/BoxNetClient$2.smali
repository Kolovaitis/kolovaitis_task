.class Lcom/epson/iprint/storage/box/BoxNetClient$2;
.super Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
.source "BoxNetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/box/BoxNetClient;->getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDownloadThread:Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;

.field final synthetic this$0:Lcom/epson/iprint/storage/box/BoxNetClient;

.field final synthetic val$localPath:Ljava/lang/String;

.field final synthetic val$storageItem:Lcom/epson/iprint/storage/StorageItem;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->this$0:Lcom/epson/iprint/storage/box/BoxNetClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->val$storageItem:Lcom/epson/iprint/storage/StorageItem;

    iput-object p3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->val$localPath:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Downloader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 131
    monitor-enter p0

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->mDownloadThread:Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->mDownloadThread:Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->cancel()V

    .line 135
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V
    .locals 7

    if-nez p1, :cond_0

    return-void

    .line 123
    :cond_0
    monitor-enter p0

    .line 124
    :try_start_0
    new-instance v6, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;

    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->this$0:Lcom/epson/iprint/storage/box/BoxNetClient;

    iget-object v3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->val$storageItem:Lcom/epson/iprint/storage/StorageItem;

    iget-object v4, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->val$localPath:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/box/BoxNetClient$1;)V

    iput-object v6, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->mDownloadThread:Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;

    .line 125
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$2;->mDownloadThread:Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/box/BoxNetClient$DownloadThread;->start()V

    return-void

    :catchall_0
    move-exception p1

    .line 125
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.class Lcom/epson/iprint/storage/box/BoxNetClient$1;
.super Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
.source "BoxNetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/box/BoxNetClient;->getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/box/BoxNetClient;

.field final synthetic val$localPath:Ljava/lang/String;

.field final synthetic val$uploadFilename:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$1;->this$0:Lcom/epson/iprint/storage/box/BoxNetClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/box/BoxNetClient$1;->val$localPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$1;->val$uploadFilename:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    return-void
.end method


# virtual methods
.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V
    .locals 7

    .line 90
    new-instance v6, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;

    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$1;->this$0:Lcom/epson/iprint/storage/box/BoxNetClient;

    iget-object v3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$1;->val$localPath:Ljava/lang/String;

    iget-object v4, p0, Lcom/epson/iprint/storage/box/BoxNetClient$1;->val$uploadFilename:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/box/BoxNetClient$1;)V

    .line 91
    invoke-virtual {v6}, Lcom/epson/iprint/storage/box/BoxNetClient$UploadThread;->start()V

    return-void
.end method

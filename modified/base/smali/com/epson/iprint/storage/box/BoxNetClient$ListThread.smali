.class Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;
.super Ljava/lang/Thread;
.source "BoxNetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/box/BoxNetClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ListThread"
.end annotation


# instance fields
.field private final mBaseFolderId:Ljava/lang/String;

.field private final mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

.field private final mCompleteListener:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;


# direct methods
.method private constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/box/BoxNetClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "list-box"

    .line 450
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 451
    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    .line 452
    iput-object p2, p0, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;->mCompleteListener:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    .line 453
    iput-object p3, p0, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;->mBaseFolderId:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;Ljava/lang/String;Lcom/epson/iprint/storage/box/BoxNetClient$1;)V
    .locals 0

    .line 443
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 459
    :try_start_0
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;->mBaseFolderId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/box/BoxNetClient;->access$300(Lcom/epson/iprint/storage/box/BoxNetClient;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 460
    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;->mCompleteListener:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v1, v0, v2}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    :try_end_0
    .catch Lcom/box/androidsdk/content/BoxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 462
    :catch_0
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;->mCompleteListener:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    const/4 v1, 0x0

    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {v0, v1, v2}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    :goto_0
    return-void
.end method

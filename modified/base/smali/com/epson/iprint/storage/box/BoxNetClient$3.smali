.class Lcom/epson/iprint/storage/box/BoxNetClient$3;
.super Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.source "BoxNetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/box/BoxNetClient;->getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/box/BoxNetClient;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/box/BoxNetClient;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$3;->this$0:Lcom/epson/iprint/storage/box/BoxNetClient;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    return-void
.end method


# virtual methods
.method public enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 3

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    .line 161
    :cond_0
    iget-object p1, p1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    if-nez p1, :cond_1

    const-string p1, "0"

    .line 169
    :cond_1
    new-instance v0, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;

    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetClient$3;->this$0:Lcom/epson/iprint/storage/box/BoxNetClient;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, p1, v2}, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;-><init>(Lcom/epson/iprint/storage/box/BoxNetClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;Ljava/lang/String;Lcom/epson/iprint/storage/box/BoxNetClient$1;)V

    invoke-virtual {v0}, Lcom/epson/iprint/storage/box/BoxNetClient$ListThread;->start()V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method public getRootItem()Lcom/epson/iprint/storage/StorageItem;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 147
    new-instance v0, Lcom/epson/iprint/storage/StorageItem;

    const-string v1, ""

    const-string v2, "0"

    sget-object v3, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    invoke-direct {v0, v1, v2, v3}, Lcom/epson/iprint/storage/StorageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageItem$ItemType;)V

    return-object v0
.end method

.class public Lcom/epson/iprint/storage/box/BoxAuthStorage;
.super Lcom/box/androidsdk/content/auth/BoxAuthentication$AuthStorage;
.source "BoxAuthStorage.java"


# static fields
.field private static final SECURE_STORE_KEY:Ljava/lang/String; = "FolderViewer.BOXNET_AUTO_INFO_V4"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/box/androidsdk/content/auth/BoxAuthentication$AuthStorage;-><init>()V

    return-void
.end method

.method public static deleteOldData()V
    .locals 2

    .line 94
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object v0

    const-string v1, "FolderViewer.BOXNET_TOKEN"

    .line 95
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    const-string v1, "FolderViewer.BOXNET_USERNAME"

    .line 96
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    const-string v1, "FolderViewer.BOXNET_PASSWORD"

    .line 97
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    return-void
.end method


# virtual methods
.method protected clearAuthInfoMap(Landroid/content/Context;)V
    .locals 1

    .line 36
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object p1

    const-string v0, "FolderViewer.BOXNET_AUTO_INFO_V4"

    .line 37
    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    return-void
.end method

.method protected getLastAuthentictedUserId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 53
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object p1

    const-string v0, "FolderViewer.BOXNET_LAST_USER_ID"

    .line 54
    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageSecureStore;->fetch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected loadAuthInfoMap(Landroid/content/Context;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;",
            ">;"
        }
    .end annotation

    .line 62
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 64
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object v0

    const-string v1, "FolderViewer.BOXNET_AUTO_INFO_V4"

    .line 65
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageSecureStore;->fetch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    return-object p1

    .line 70
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 71
    new-instance v1, Lcom/box/androidsdk/content/models/BoxEntity;

    invoke-direct {v1}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>()V

    .line 72
    invoke-virtual {v1, v0}, Lcom/box/androidsdk/content/models/BoxEntity;->createFromJson(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxEntity;->getPropertiesKeySet()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 74
    invoke-virtual {v1, v2}, Lcom/box/androidsdk/content/models/BoxEntity;->getPropertyValue(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;

    move-result-object v3

    const/4 v4, 0x0

    .line 76
    invoke-virtual {v3}, Lcom/eclipsesource/json/JsonValue;->isString()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 77
    new-instance v4, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;

    invoke-direct {v4}, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;-><init>()V

    .line 78
    invoke-virtual {v3}, Lcom/eclipsesource/json/JsonValue;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;->createFromJson(Ljava/lang/String;)V

    goto :goto_1

    .line 79
    :cond_1
    invoke-virtual {v3}, Lcom/eclipsesource/json/JsonValue;->isObject()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 80
    new-instance v4, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;

    invoke-direct {v4}, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;-><init>()V

    .line 81
    invoke-virtual {v3}, Lcom/eclipsesource/json/JsonValue;->asObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    .line 83
    :cond_2
    :goto_1
    invoke-virtual {p1, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    return-object p1
.end method

.method protected storeAuthInfoMap(Ljava/util/Map;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .line 24
    new-instance p2, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {p2}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    .line 25
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 26
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;->toJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    goto :goto_0

    .line 28
    :cond_0
    new-instance p1, Lcom/box/androidsdk/content/models/BoxEntity;

    invoke-direct {p1, p2}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    .line 30
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object p2

    const-string v0, "FolderViewer.BOXNET_AUTO_INFO_V4"

    .line 31
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxEntity;->toJson()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->OVERWRITE_IF_EXIST:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    invoke-virtual {p2, v0, p1, v1}, Lcom/epson/iprint/storage/StorageSecureStore;->put(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;)Z

    return-void
.end method

.method protected storeLastAuthenticatedUserId(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .line 42
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object p2

    if-eqz p1, :cond_1

    .line 43
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "FolderViewer.BOXNET_LAST_USER_ID"

    .line 46
    sget-object v1, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->OVERWRITE_IF_EXIST:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    invoke-virtual {p2, v0, p1, v1}, Lcom/epson/iprint/storage/StorageSecureStore;->put(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;)Z

    goto :goto_1

    :cond_1
    :goto_0
    const-string p1, "FolderViewer.BOXNET_LAST_USER_ID"

    .line 44
    invoke-virtual {p2, p1}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    :goto_1
    return-void
.end method

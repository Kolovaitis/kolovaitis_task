.class Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;
.super Landroid/os/AsyncTask;
.source "BoxNetSignInActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/box/BoxNetSignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LogoutTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final mBoxNetClient:Lcom/epson/iprint/storage/box/BoxNetClient;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/epson/iprint/storage/box/BoxNetClient;)V
    .locals 1
    .param p2    # Lcom/epson/iprint/storage/box/BoxNetClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 127
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 128
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    .line 129
    iput-object p2, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;->mBoxNetClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    .line 135
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;->mBoxNetClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/box/BoxNetClient;->boxLogout()V

    const/4 p1, 0x0

    return-object p1
.end method

.class public Lcom/epson/iprint/storage/box/BoxNetSignInActivity;
.super Lcom/epson/iprint/storage/StorageSignInActivity;
.source "BoxNetSignInActivity.java"

# interfaces
.implements Lcom/box/androidsdk/content/auth/BoxAuthentication$AuthListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;
    }
.end annotation


# static fields
.field private static final PARAM_KEY_LOGOUT:Ljava/lang/String; = "logout"


# instance fields
.field private mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

.field private mModeLogout:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;-><init>()V

    return-void
.end method

.method public static getLogoutIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "logout"

    const/4 v1, 0x1

    .line 35
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method private getMode(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string v1, "logout"

    .line 72
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private startLogoutTask()V
    .locals 3

    .line 76
    iget-object v0, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->finish()V

    .line 83
    :cond_0
    new-instance v0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;

    iget-object v1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    invoke-direct {v0, p0, v1}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;-><init>(Landroid/app/Activity;Lcom/epson/iprint/storage/box/BoxNetClient;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity$LogoutTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public getBasicSignIn()Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onAuthCreated(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;)V
    .locals 1

    .line 100
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/box/BoxNetClient;->setLoginStatus(Z)V

    .line 101
    invoke-virtual {p0}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->finish()V

    return-void
.end method

.method public onAuthFailure(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;Ljava/lang/Exception;)V
    .locals 0

    .line 106
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/epson/iprint/storage/box/BoxNetClient;->setLoginStatus(Z)V

    .line 111
    invoke-virtual {p0}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->showLoginErrorAndFinish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 41
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->getMode(Landroid/content/Intent;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mModeLogout:Z

    .line 44
    iget-boolean p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mModeLogout:Z

    if-eqz p1, :cond_0

    const p1, 0x7f0a00c2

    .line 47
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->setContentView(I)V

    const p1, 0x7f080219

    .line 48
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x7f0800b0

    .line 49
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 52
    :cond_0
    invoke-static {}, Lcom/epson/iprint/storage/box/BoxNetClient;->getInstance()Lcom/epson/iprint/storage/box/BoxNetClient;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    .line 53
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    invoke-virtual {p1, p0}, Lcom/epson/iprint/storage/box/BoxNetClient;->setSessionAuthListener(Lcom/box/androidsdk/content/auth/BoxAuthentication$AuthListener;)V

    .line 55
    iget-boolean p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mModeLogout:Z

    if-eqz p1, :cond_1

    .line 56
    invoke-direct {p0}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->startLogoutTask()V

    goto :goto_0

    .line 58
    :cond_1
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/box/BoxNetClient;->authenticate()V

    :goto_0
    return-void
.end method

.method public onLoggedOut(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;Ljava/lang/Exception;)V
    .locals 0

    .line 116
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/epson/iprint/storage/box/BoxNetClient;->setLoginStatus(Z)V

    .line 117
    invoke-virtual {p0}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->finish()V

    return-void
.end method

.method public onRefreshed(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;)V
    .locals 1

    .line 94
    iget-object p1, p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->mBoxClient:Lcom/epson/iprint/storage/box/BoxNetClient;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/box/BoxNetClient;->setLoginStatus(Z)V

    .line 95
    invoke-virtual {p0}, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;->finish()V

    return-void
.end method

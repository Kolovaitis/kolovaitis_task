.class Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;
.super Ljava/lang/Object;
.source "StorageProcessUploadActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageProcessUploadActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

.field final synthetic val$saveButton:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity;Landroid/widget/Button;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->val$saveButton:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 75
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getFilename()Ljava/lang/String;

    move-result-object p1

    .line 76
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/StorageServiceClient;->isValidUploadName(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->val$saveButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 88
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;

    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-direct {v0, v2}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;-><init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity;)V

    .line 93
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    iget-object v3, v3, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFileLocalPathList:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->localPaths:Ljava/util/List;

    .line 95
    iput-object p1, v0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploadFilename:Ljava/lang/String;

    .line 96
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getFiletype()Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    move-result-object p1

    iput-object p1, v0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->fileType:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    .line 97
    new-array p1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 99
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->showConnectionError()V

    goto :goto_0

    .line 102
    :cond_1
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    const v0, 0x7f0e04b4

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->showErrorDialog(I)V

    :goto_0
    return-void
.end method

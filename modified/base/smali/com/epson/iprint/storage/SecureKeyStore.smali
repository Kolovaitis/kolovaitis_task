.class public Lcom/epson/iprint/storage/SecureKeyStore;
.super Ljava/lang/Object;
.source "SecureKeyStore.java"

# interfaces
.implements Lcom/epson/iprint/storage/ISecureKeyStore;


# static fields
.field private static final A_KEY:Ljava/lang/String; = "2T6hSEbWBnkQ4r8KmkdQDg=="

.field public static final B_KEY:Ljava/lang/String; = "oKHwWs_UUc1LchvNPq3Dd_nHCB5gUSpSiq2zkIHj2fE="

.field public static final B_SEC:Ljava/lang/String; = "FJrqtZTOjGHKXjQ49Q2N5R3qAzBeZvWDd_Tqfjxi3aE="

.field public static final C_KEY:Ljava/lang/String; = "gtV0nQIxV5Mo5JvWmF5ymXXG5gfJeHqp9OdwHjAm6H0d6gMwXmb1g3f06n48Yt2h"

.field public static final C_SEC:Ljava/lang/String; = "XYnA5S0klACNMsBO7UXye-V9YNbI9xypH1H4ty-l_T0d6gMwXmb1g3f06n48Yt2h"

.field public static final D_KEY:Ljava/lang/String; = "NTqY6KeSXNYF9434almiWR3qAzBeZvWDd_Tqfjxi3aE="


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApiKeyA(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 39
    invoke-static {p1}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    move-result-object p1

    const-string v0, "2T6hSEbWBnkQ4r8KmkdQDg=="

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->Decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getApiKeyB(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 44
    invoke-static {p1}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    move-result-object p1

    const-string v0, "oKHwWs_UUc1LchvNPq3Dd_nHCB5gUSpSiq2zkIHj2fE="

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->Decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getApiKeyC(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 54
    invoke-static {p1}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    move-result-object p1

    const-string v0, "gtV0nQIxV5Mo5JvWmF5ymXXG5gfJeHqp9OdwHjAm6H0d6gMwXmb1g3f06n48Yt2h"

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->Decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getApiKeyD(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 64
    invoke-static {p1}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    move-result-object p1

    const-string v0, "NTqY6KeSXNYF9434almiWR3qAzBeZvWDd_Tqfjxi3aE="

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->Decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSecKeyB(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 49
    invoke-static {p1}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    move-result-object p1

    const-string v0, "FJrqtZTOjGHKXjQ49Q2N5R3qAzBeZvWDd_Tqfjxi3aE="

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->Decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSecKeyC(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 59
    invoke-static {p1}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    move-result-object p1

    const-string v0, "XYnA5S0klACNMsBO7UXye-V9YNbI9xypH1H4ty-l_T0d6gMwXmb1g3f06n48Yt2h"

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->Decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

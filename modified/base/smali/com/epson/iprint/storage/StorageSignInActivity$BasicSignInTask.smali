.class Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;
.super Landroid/os/AsyncTask;
.source "StorageSignInActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageSignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BasicSignInTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mPassword:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;

.field progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

.field final synthetic this$0:Lcom/epson/iprint/storage/StorageSignInActivity;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageSignInActivity;)V
    .locals 1

    .line 188
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->this$0:Lcom/epson/iprint/storage/StorageSignInActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const v0, 0x7f0801ff

    .line 189
    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->mUserName:Ljava/lang/String;

    const v0, 0x7f0801fd

    .line 190
    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->mPassword:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2

    .line 198
    new-instance p1, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->this$0:Lcom/epson/iprint/storage/StorageSignInActivity;

    invoke-direct {p1, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;-><init>(Lcom/epson/iprint/storage/StorageActivity;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    .line 199
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->show()V

    .line 200
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->this$0:Lcom/epson/iprint/storage/StorageSignInActivity;

    invoke-static {p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->access$000(Lcom/epson/iprint/storage/StorageSignInActivity;)Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->mUserName:Ljava/lang/String;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->mPassword:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;->signInBackground(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 0

    .line 209
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 210
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->this$0:Lcom/epson/iprint/storage/StorageSignInActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->showLoginError()V

    .line 211
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->dismiss()V

    goto :goto_0

    .line 216
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->dismiss()V

    .line 217
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->this$0:Lcom/epson/iprint/storage/StorageSignInActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->finish()V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 183
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

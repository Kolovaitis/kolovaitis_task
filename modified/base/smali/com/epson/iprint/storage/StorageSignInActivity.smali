.class public abstract Lcom/epson/iprint/storage/StorageSignInActivity;
.super Lcom/epson/iprint/storage/StorageActivity;
.source "StorageSignInActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;,
        Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;
    }
.end annotation


# static fields
.field private static final EXTRA_SERVER_NAME:Ljava/lang/String; = "StorageSignInActivity.ServerName"


# instance fields
.field private final DEFAULT_INPUT_MAX_LENGTH:I

.field private mBasicSignIn:Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;

.field private mProgressUntilOnDestroy:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageActivity;-><init>()V

    const/16 v0, 0x100

    .line 34
    iput v0, p0, Lcom/epson/iprint/storage/StorageSignInActivity;->DEFAULT_INPUT_MAX_LENGTH:I

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/StorageSignInActivity;)Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/epson/iprint/storage/StorageSignInActivity;->mBasicSignIn:Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;

    return-object p0
.end method

.method private static getActivityClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 48
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_GOOGLEDRIVE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-class p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;

    goto :goto_0

    :cond_0
    const-string v0, "dropbox"

    .line 50
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const-class p0, Lcom/epson/iprint/storage/dropbox/DropboxV2SignInActivity;

    goto :goto_0

    .line 52
    :cond_1
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_BOX:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    const-class p0, Lcom/epson/iprint/storage/box/BoxNetSignInActivity;

    goto :goto_0

    .line 54
    :cond_2
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_EVERNOTE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55
    const-class p0, Lcom/epson/iprint/storage/evernote/EvernoteSignInActivity;

    goto :goto_0

    .line 56
    :cond_3
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_ONEDRIVE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 57
    const-class p0, Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity;

    goto :goto_0

    :cond_4
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private getMaxLengthUsername()I
    .locals 1

    const/16 v0, 0x100

    return v0
.end method

.method public static getStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .line 64
    invoke-static {p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->getActivityClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    .line 65
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "StorageSignInActivity.ServerName"

    .line 66
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public abstract getBasicSignIn()Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;
.end method

.method protected getMaxLengthPassword()I
    .locals 1

    const/16 v0, 0x100

    return v0
.end method

.method public bridge synthetic isConnected()Z
    .locals 1

    .line 30
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageActivity;->isConnected()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 96
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;->getBasicSignIn()Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 104
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->onInitBasicSignIn(Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;)V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 238
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0007

    .line 239
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f080216

    .line 240
    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 242
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onDestroy()V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageSignInActivity;->mProgressUntilOnDestroy:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->dismiss()V

    const/4 v0, 0x0

    .line 112
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSignInActivity;->mProgressUntilOnDestroy:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    .line 114
    :cond_0
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageActivity;->onDestroy()V

    return-void
.end method

.method protected onInitBasicSignIn(Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;)V
    .locals 3

    .line 122
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSignInActivity;->mBasicSignIn:Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;

    const p1, 0x7f0a0086

    .line 127
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->setContentView(I)V

    .line 132
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "StorageSignInActivity.ServerName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    .line 133
    invoke-virtual {p0, p1, v0}, Lcom/epson/iprint/storage/StorageSignInActivity;->setActionBar(Ljava/lang/String;Z)V

    const p1, 0x7f0801ff

    .line 138
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;->getMaxLengthUsername()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/storage/StorageSignInActivity;->setInputMaxLength(Landroid/widget/EditText;I)V

    const v0, 0x7f0801fd

    .line 139
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;->getMaxLengthPassword()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/epson/iprint/storage/StorageSignInActivity;->setInputMaxLength(Landroid/widget/EditText;I)V

    const v1, 0x7f0800c3

    .line 144
    invoke-virtual {p0, p1, v1}, Lcom/epson/iprint/storage/StorageSignInActivity;->bindClearButton(II)V

    const p1, 0x7f0800c6

    .line 145
    invoke-virtual {p0, v0, p1}, Lcom/epson/iprint/storage/StorageSignInActivity;->bindClearButton(II)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 226
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080215

    if-eq v0, v1, :cond_0

    .line 232
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 228
    :cond_0
    new-instance p1, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;

    invoke-direct {p1, p0}, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;-><init>(Lcom/epson/iprint/storage/StorageSignInActivity;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignInTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 p1, 0x1

    return p1
.end method

.method protected showLoginError()V
    .locals 1

    const v0, 0x7f0e02ad

    .line 168
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageSignInActivity;->showErrorDialog(I)V

    return-void
.end method

.method protected showLoginErrorAndFinish()V
    .locals 1

    const v0, 0x7f0e02ad

    .line 177
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageSignInActivity;->showErrorDialogAndFinish(Ljava/lang/String;)V

    return-void
.end method

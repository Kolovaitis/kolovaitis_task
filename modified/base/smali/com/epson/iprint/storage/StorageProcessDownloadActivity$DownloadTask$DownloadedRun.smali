.class Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;
.super Ljava/lang/Object;
.source "StorageProcessDownloadActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadedRun"
.end annotation


# instance fields
.field error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

.field final synthetic this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 312
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$202(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Z)Z

    .line 313
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-ne v0, v2, :cond_0

    .line 314
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    .line 315
    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v2, v2, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->localPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v3, v3, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    .line 316
    invoke-static {v3}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$300(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v3

    .line 315
    invoke-static {v2, v0, v3}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getPreviewIntent(Ljava/lang/String;Landroid/content/Context;Lcom/epson/iprint/prtlogger/PrintLog;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000000

    .line 318
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 324
    iget-object v3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v3, v3, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    iget-object v4, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v4, v4, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->localPath:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$400(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Ljava/lang/String;)V

    .line 330
    iget-object v3, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v3, v3, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {v3}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$500(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 331
    invoke-virtual {v0, v2, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-ne v0, v2, :cond_1

    goto :goto_0

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const v2, 0x7f0e0323

    invoke-virtual {v0, v2}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->showErrorDialog(I)V

    .line 342
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$100(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Z)V

    .line 343
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask$DownloadedRun;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->dismiss()V

    return-void
.end method

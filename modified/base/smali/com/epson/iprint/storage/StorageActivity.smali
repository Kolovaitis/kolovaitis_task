.class Lcom/epson/iprint/storage/StorageActivity;
.super Lepson/print/ActivityIACommon;
.source "StorageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;,
        Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;,
        Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method


# virtual methods
.method bindClearButton(II)V
    .locals 1

    const/4 v0, 0x0

    .line 292
    invoke-virtual {p0, p1, p2, v0}, Lcom/epson/iprint/storage/StorageActivity;->bindClearButton(IILcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;)V

    return-void
.end method

.method bindClearButton(IILcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;)V
    .locals 1

    .line 302
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    .line 303
    invoke-virtual {p0, p2}, Lcom/epson/iprint/storage/StorageActivity;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 309
    new-instance v0, Lcom/epson/iprint/storage/StorageActivity$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/epson/iprint/storage/StorageActivity$3;-><init>(Lcom/epson/iprint/storage/StorageActivity;Landroid/widget/EditText;Landroid/widget/Button;Lcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 327
    new-instance p3, Lcom/epson/iprint/storage/StorageActivity$4;

    invoke-direct {p3, p0, p1}, Lcom/epson/iprint/storage/StorageActivity$4;-><init>(Lcom/epson/iprint/storage/StorageActivity;Landroid/widget/EditText;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x0

    .line 337
    invoke-virtual {p2, p1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public isConnected()Z
    .locals 2

    .line 347
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 348
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 350
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 42
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/iprint/storage/StorageSecureStore;->initSharedSecureStore(Landroid/content/Context;)V

    return-void
.end method

.method setInputMaxLength(Landroid/widget/EditText;I)V
    .locals 2

    const/4 v0, 0x1

    .line 361
    new-array v0, v0, [Landroid/text/InputFilter;

    .line 362
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, p2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 p2, 0x0

    aput-object v1, v0, p2

    .line 363
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method setTitle(Ljava/lang/String;)V
    .locals 0

    .line 371
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showConnectionError()V
    .locals 3

    const v0, 0x7f0e03d8

    .line 232
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.WIRELESS_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/storage/StorageActivity;->showErrorDialog(Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method showErrorDialog(I)V
    .locals 0

    .line 225
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->showErrorDialog(Ljava/lang/String;)V

    return-void
.end method

.method protected showErrorDialog(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 217
    invoke-virtual {p0, p1, v0}, Lcom/epson/iprint/storage/StorageActivity;->showErrorDialog(Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method showErrorDialog(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 2

    .line 244
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 245
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 p1, 0x0

    .line 246
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const p1, 0x7f0e04f2

    .line 247
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/epson/iprint/storage/StorageActivity$1;

    invoke-direct {v1, p0, p2}, Lcom/epson/iprint/storage/StorageActivity$1;-><init>(Lcom/epson/iprint/storage/StorageActivity;Landroid/content/Intent;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 256
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 257
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method showErrorDialogAndFinish(Ljava/lang/String;)V
    .locals 2

    .line 266
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 267
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 p1, 0x0

    .line 268
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const p1, 0x7f0e04f2

    .line 269
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/epson/iprint/storage/StorageActivity$2;

    invoke-direct {v1, p0}, Lcom/epson/iprint/storage/StorageActivity$2;-><init>(Lcom/epson/iprint/storage/StorageActivity;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 275
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 276
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

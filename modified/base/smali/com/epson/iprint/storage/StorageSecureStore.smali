.class public Lcom/epson/iprint/storage/StorageSecureStore;
.super Ljava/lang/Object;
.source "StorageSecureStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;,
        Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;
    }
.end annotation


# static fields
.field static mInstance:Lcom/epson/iprint/storage/StorageSecureStore;


# instance fields
.field final COLUMN_KEY:Ljava/lang/String;

.field final COLUMN_VALUE:Ljava/lang/String;

.field final CREATE_KEYVALUE:Ljava/lang/String;

.field final DELETE_KEYVALUE:Ljava/lang/String;

.field final DROP_KEYVALUE:Ljava/lang/String;

.field final ENCRYPT_ALGORITHM:Ljava/lang/String;

.field final ENCRYPT_KEY_BYTES:I

.field final INDEX_KEY:I

.field final INDEX_VALUE:I

.field final KEYVALUE_DBNAME:Ljava/lang/String;

.field final KEYVALUE_TBLNAME:Ljava/lang/String;

.field final KEYVALUE_VERSION:I

.field final SELECT_KEYVALUE:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mKeyValueMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 44
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mKeyValueMap:Ljava/util/Map;

    .line 45
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mContext:Landroid/content/Context;

    const-string v0, "storage.data"

    .line 50
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->KEYVALUE_DBNAME:Ljava/lang/String;

    const-string v0, "keyvalue_tbl"

    .line 51
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->KEYVALUE_TBLNAME:Ljava/lang/String;

    const-string v0, "key"

    .line 52
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->COLUMN_KEY:Ljava/lang/String;

    const-string v0, "value"

    .line 53
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->COLUMN_VALUE:Ljava/lang/String;

    const-string v0, "key=?"

    .line 54
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->DELETE_KEYVALUE:Ljava/lang/String;

    const-string v0, "SELECT * FROM keyvalue_tbl;"

    .line 55
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->SELECT_KEYVALUE:Ljava/lang/String;

    const-string v0, "CREATE TABLE IF NOT EXISTS keyvalue_tbl (key TEXT PRIMARY KEY, value BLOB);"

    .line 56
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->CREATE_KEYVALUE:Ljava/lang/String;

    const-string v0, "DROP TABLE IF EXISTS keyvalue_tbl;"

    .line 58
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->DROP_KEYVALUE:Ljava/lang/String;

    const/4 v0, 0x0

    .line 59
    iput v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->INDEX_KEY:I

    const/4 v0, 0x1

    .line 60
    iput v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->INDEX_VALUE:I

    .line 61
    iput v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->KEYVALUE_VERSION:I

    const/16 v0, 0x10

    .line 62
    iput v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->ENCRYPT_KEY_BYTES:I

    const-string v0, "AES"

    .line 63
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->ENCRYPT_ALGORITHM:Ljava/lang/String;

    .line 222
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageSecureStore;->reloadCache()V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 44
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mKeyValueMap:Ljava/util/Map;

    .line 45
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mContext:Landroid/content/Context;

    const-string v0, "storage.data"

    .line 50
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->KEYVALUE_DBNAME:Ljava/lang/String;

    const-string v0, "keyvalue_tbl"

    .line 51
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->KEYVALUE_TBLNAME:Ljava/lang/String;

    const-string v0, "key"

    .line 52
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->COLUMN_KEY:Ljava/lang/String;

    const-string v0, "value"

    .line 53
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->COLUMN_VALUE:Ljava/lang/String;

    const-string v0, "key=?"

    .line 54
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->DELETE_KEYVALUE:Ljava/lang/String;

    const-string v0, "SELECT * FROM keyvalue_tbl;"

    .line 55
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->SELECT_KEYVALUE:Ljava/lang/String;

    const-string v0, "CREATE TABLE IF NOT EXISTS keyvalue_tbl (key TEXT PRIMARY KEY, value BLOB);"

    .line 56
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->CREATE_KEYVALUE:Ljava/lang/String;

    const-string v0, "DROP TABLE IF EXISTS keyvalue_tbl;"

    .line 58
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->DROP_KEYVALUE:Ljava/lang/String;

    const/4 v0, 0x0

    .line 59
    iput v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->INDEX_KEY:I

    const/4 v0, 0x1

    .line 60
    iput v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->INDEX_VALUE:I

    .line 61
    iput v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->KEYVALUE_VERSION:I

    const/16 v0, 0x10

    .line 62
    iput v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->ENCRYPT_KEY_BYTES:I

    const-string v0, "AES"

    .line 63
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->ENCRYPT_ALGORITHM:Ljava/lang/String;

    .line 214
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mContext:Landroid/content/Context;

    .line 215
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageSecureStore;->reloadCache()V

    return-void
.end method

.method public static getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;
    .locals 1

    .line 70
    sget-object v0, Lcom/epson/iprint/storage/StorageSecureStore;->mInstance:Lcom/epson/iprint/storage/StorageSecureStore;

    return-object v0
.end method

.method public static initSharedSecureStore(Landroid/content/Context;)V
    .locals 1

    .line 143
    sget-object v0, Lcom/epson/iprint/storage/StorageSecureStore;->mInstance:Lcom/epson/iprint/storage/StorageSecureStore;

    if-nez v0, :cond_0

    .line 144
    new-instance v0, Lcom/epson/iprint/storage/StorageSecureStore;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/StorageSecureStore;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/iprint/storage/StorageSecureStore;->mInstance:Lcom/epson/iprint/storage/StorageSecureStore;

    :cond_0
    return-void
.end method

.method public static invalidateCache()V
    .locals 1

    const/4 v0, 0x0

    .line 238
    sput-object v0, Lcom/epson/iprint/storage/StorageSecureStore;->mInstance:Lcom/epson/iprint/storage/StorageSecureStore;

    return-void
.end method


# virtual methods
.method decodeAes([BLjava/lang/String;)[B
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 180
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    const/16 v1, 0x10

    .line 181
    invoke-virtual {p0, v1, p2}, Lcom/epson/iprint/storage/StorageSecureStore;->makeKeyBytes(I[B)[B

    move-result-object p2

    const-string v1, "AES"

    .line 182
    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 183
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "AES"

    invoke-direct {v2, p2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const/4 p2, 0x2

    .line 184
    invoke-virtual {v1, p2, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 185
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    return-object v0

    :cond_1
    :goto_0
    return-object v0
.end method

.method encodeAes([BLjava/lang/String;)[B
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 159
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    const/16 v1, 0x10

    .line 160
    invoke-virtual {p0, v1, p2}, Lcom/epson/iprint/storage/StorageSecureStore;->makeKeyBytes(I[B)[B

    move-result-object p2

    const-string v1, "AES"

    .line 161
    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 162
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "AES"

    invoke-direct {v2, p2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const/4 p2, 0x1

    .line 163
    invoke-virtual {v1, p2, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 164
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    return-object v0

    :cond_1
    :goto_0
    return-object v0
.end method

.method public fetch(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mKeyValueMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method makeKeyBytes(I[B)[B
    .locals 3

    .line 198
    new-array p1, p1, [B

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 199
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 200
    array-length v2, p2

    if-ge v1, v2, :cond_0

    .line 201
    aget-byte v2, p2, v1

    aput-byte v2, p1, v1

    goto :goto_1

    .line 203
    :cond_0
    aput-byte v0, p1, v1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object p1
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .line 80
    new-instance v0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;-><init>(Lcom/epson/iprint/storage/StorageSecureStore;Landroid/content/Context;)V

    .line 81
    invoke-virtual {v0, p1, p2}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->insert(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    .line 82
    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->close()V

    .line 86
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageSecureStore;->reloadCache()V

    return p1
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;)Z
    .locals 1

    .line 100
    sget-object v0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->OVERWRITE_IF_EXIST:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    if-ne p3, v0, :cond_0

    .line 101
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageSecureStore;->fetch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 103
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    .line 106
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/epson/iprint/storage/StorageSecureStore;->put(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method reloadCache()V
    .locals 2

    .line 229
    new-instance v0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;-><init>(Lcom/epson/iprint/storage/StorageSecureStore;Landroid/content/Context;)V

    .line 230
    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->select()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mKeyValueMap:Ljava/util/Map;

    .line 231
    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->close()V

    return-void
.end method

.method public revoke(Ljava/lang/String;)Z
    .locals 2

    .line 128
    new-instance v0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageSecureStore;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;-><init>(Lcom/epson/iprint/storage/StorageSecureStore;Landroid/content/Context;)V

    .line 129
    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->delete(Ljava/lang/String;)Z

    move-result p1

    .line 130
    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->close()V

    .line 134
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageSecureStore;->reloadCache()V

    return p1
.end method

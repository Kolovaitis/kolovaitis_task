.class public Lcom/epson/iprint/storage/StorageProcessUploadActivity;
.super Lcom/epson/iprint/storage/StorageProcessActivity;
.source "StorageProcessUploadActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;
    }
.end annotation


# instance fields
.field final UPLOAD_EDITING_FILENAME:Ljava/lang/String;

.field mUploadFileLocalPathList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mUploadFilename:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 45
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;-><init>()V

    const-string v0, "Upload.Editing.Filename"

    .line 53
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->UPLOAD_EDITING_FILENAME:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method generateUploadFilename(I)Ljava/lang/String;
    .locals 3

    .line 469
    invoke-static {p0}, Lepson/print/ScanFileNumber;->getCount(Landroid/content/Context;)I

    move-result v0

    .line 470
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EPSON"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Lepson/print/Util/Utils;

    invoke-direct {v2}, Lepson/print/Util/Utils;-><init>()V

    add-int/2addr v0, p1

    invoke-virtual {v2, v0}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getFilename()Ljava/lang/String;
    .locals 1

    const v0, 0x7f08037a

    .line 435
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getFiletype()Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;
    .locals 2

    const v0, 0x7f080148

    .line 451
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 452
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f08025a

    if-ne v0, v1, :cond_0

    .line 453
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;->PDF:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;->JPEG:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    :goto_0
    return-object v0
.end method

.method getTotalFileSize(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    .line 480
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 482
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 483
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 484
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 486
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-double v0, v0

    const-wide v2, 0x407f400000000000L    # 500.0

    const/4 p1, 0x2

    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    .line 488
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/high16 v3, 0x4090000000000000L    # 1024.0

    div-double/2addr v0, v3

    invoke-static {v0, v1, p1}, Lepson/server/utils/MyUtility;->mathRound(DI)D

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p1, "MB"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 490
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1, p1}, Lepson/server/utils/MyUtility;->mathRound(DI)D

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p1, "KB"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 57
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageProcessActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a0064

    .line 58
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->setContentView(I)V

    .line 61
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->setActionBar(Ljava/lang/String;Z)V

    const v0, 0x7f0802cc

    .line 67
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0e053d

    .line 68
    invoke-virtual {p0, v1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 69
    new-instance v1, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;

    invoke-direct {v1, p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$1;-><init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity;Landroid/widget/Button;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$2;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$2;-><init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity;)V

    const v1, 0x7f08037a

    const v2, 0x7f0800c5

    invoke-virtual {p0, v1, v2, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->bindClearButton(IILcom/epson/iprint/storage/StorageActivity$OnVisibilityListener;)V

    const/4 v0, 0x0

    .line 119
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->generateUploadFilename(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFilename:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "Upload.Editing.Filename"

    .line 121
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 123
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFilename:Ljava/lang/String;

    .line 126
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFilename:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->setFilename(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "Extra.Uploadfile.List"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFileLocalPathList:Ljava/util/List;

    .line 133
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object p1

    instance-of p1, p1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;

    if-eqz p1, :cond_1

    .line 136
    invoke-static {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->getPlayServiceCheckIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 155
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageProcessActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "Upload.Editing.Filename"

    .line 156
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFilename:Ljava/lang/String;

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 145
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageProcessActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 146
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getFilename()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFilename:Ljava/lang/String;

    const-string v0, "Upload.Editing.Filename"

    .line 147
    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFilename:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method onSignInStatus(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V
    .locals 0

    .line 165
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->updateUploadSection(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V

    .line 166
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->updateSaveButton(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V

    return-void
.end method

.method onUpdateProcessView()V
    .locals 0

    return-void
.end method

.method setEnabledSaveButton(Z)V
    .locals 1

    const v0, 0x7f0802cc

    .line 422
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 424
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 426
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method setFilename(Ljava/lang/String;)V
    .locals 1

    const v0, 0x7f08037a

    .line 443
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 444
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method updateSaveButton(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V
    .locals 1

    .line 414
    sget-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_IN:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->setEnabledSaveButton(Z)V

    return-void
.end method

.method updateUploadSection(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V
    .locals 2

    const v0, 0x7f08037e

    .line 381
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 382
    sget-object v1, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_IN:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    if-ne p1, v1, :cond_1

    const/4 p1, 0x0

    .line 383
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    const p1, 0x7f080148

    .line 388
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioGroup;

    const v0, 0x7f08025a

    .line 390
    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 395
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFileLocalPathList:Ljava/util/List;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 396
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFilename:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->setFilename(Ljava/lang/String;)V

    :cond_0
    const p1, 0x7f080145

    .line 402
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 403
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->mUploadFileLocalPathList:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getTotalFileSize(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x4

    .line 405
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void
.end method

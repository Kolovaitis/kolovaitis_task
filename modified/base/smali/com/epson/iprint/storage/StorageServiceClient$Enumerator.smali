.class public abstract Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.super Ljava/lang/Object;
.source "StorageServiceClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageServiceClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "Enumerator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/StorageServiceClient;


# direct methods
.method public constructor <init>(Lcom/epson/iprint/storage/StorageServiceClient;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;->this$0:Lcom/epson/iprint/storage/StorageServiceClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cleanUpResources()V
    .locals 0

    return-void
.end method

.method public abstract enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
.end method

.method public enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;)V
    .locals 0

    return-void
.end method

.method public abstract getRootItem()Lcom/epson/iprint/storage/StorageItem;
.end method

.method public setThumbnailInBackground(Lcom/epson/iprint/storage/StorageItem;Landroid/widget/ImageView;)V
    .locals 0

    return-void
.end method

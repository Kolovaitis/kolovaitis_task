.class public final enum Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;
.super Ljava/lang/Enum;
.source "StorageSecureStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageSecureStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EXEC_MODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

.field public static final enum NONE:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

.field public static final enum OVERWRITE_IF_EXIST:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 39
    new-instance v0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->NONE:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    .line 40
    new-instance v0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    const-string v1, "OVERWRITE_IF_EXIST"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->OVERWRITE_IF_EXIST:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    const/4 v0, 0x2

    .line 38
    new-array v0, v0, [Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    sget-object v1, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->NONE:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->OVERWRITE_IF_EXIST:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    aput-object v1, v0, v3

    sput-object v0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->$VALUES:[Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;
    .locals 1

    .line 38
    const-class v0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    return-object p0
.end method

.method public static values()[Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;
    .locals 1

    .line 38
    sget-object v0, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->$VALUES:[Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    invoke-virtual {v0}, [Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    return-object v0
.end method

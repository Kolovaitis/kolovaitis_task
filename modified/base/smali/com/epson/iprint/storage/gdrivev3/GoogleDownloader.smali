.class public Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;
.super Ljava/lang/Object;
.source "GoogleDownloader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;
    }
.end annotation


# static fields
.field private static final ROOT_ID:Ljava/lang/String; = "root"

.field private static final TARGET_SCOPE:Ljava/lang/String; = "https://www.googleapis.com/auth/drive"


# instance fields
.field private final mDrive:Lcom/google/api/services/drive/Drive;

.field private final mFolderManager:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;


# direct methods
.method private constructor <init>(Lcom/google/api/services/drive/Drive;)V
    .locals 1
    .param p1    # Lcom/google/api/services/drive/Drive;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;

    invoke-direct {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mFolderManager:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;

    .line 42
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mDrive:Lcom/google/api/services/drive/Drive;

    return-void
.end method

.method public static createGoogleDownloader(Landroid/content/Context;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 98
    invoke-static {}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->getTargetScope()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 97
    invoke-static {p0, v0}, Lcom/google/api/client/googleapis/extensions/android/gms/auth/GoogleAccountCredential;->usingOAuth2(Landroid/content/Context;Ljava/util/Collection;)Lcom/google/api/client/googleapis/extensions/android/gms/auth/GoogleAccountCredential;

    move-result-object v0

    .line 99
    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->getAccount()Landroid/accounts/Account;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/api/client/googleapis/extensions/android/gms/auth/GoogleAccountCredential;->setSelectedAccount(Landroid/accounts/Account;)Lcom/google/api/client/googleapis/extensions/android/gms/auth/GoogleAccountCredential;

    .line 100
    new-instance p1, Lcom/google/api/services/drive/Drive$Builder;

    .line 102
    invoke-static {}, Lcom/google/api/client/extensions/android/http/AndroidHttp;->newCompatibleTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v1

    new-instance v2, Lcom/google/api/client/json/gson/GsonFactory;

    invoke-direct {v2}, Lcom/google/api/client/json/gson/GsonFactory;-><init>()V

    invoke-direct {p1, v1, v2, v0}, Lcom/google/api/services/drive/Drive$Builder;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/HttpRequestInitializer;)V

    .line 105
    invoke-static {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->getDriveApplicationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/google/api/services/drive/Drive$Builder;->setApplicationName(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object p0

    .line 106
    invoke-virtual {p0}, Lcom/google/api/services/drive/Drive$Builder;->build()Lcom/google/api/services/drive/Drive;

    move-result-object p0

    .line 108
    new-instance p1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    invoke-direct {p1, p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;-><init>(Lcom/google/api/services/drive/Drive;)V

    return-object p1
.end method

.method private static getAppName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 132
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 133
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 134
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v1, :cond_0

    return-object p0

    .line 138
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getDriveApplicationName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 113
    invoke-static {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->getAppName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    return-object v0
.end method

.method private getFileInputStream(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Ljava/io/InputStream;
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 187
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->getId()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 192
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mDrive:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/api/services/drive/Drive$Files$Get;->executeMediaAsInputStream()Ljava/io/InputStream;

    move-result-object p1

    return-object p1

    .line 189
    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "getId() return null"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static getTargetScope()Ljava/lang/String;
    .locals 1

    const-string v0, "https://www.googleapis.com/auth/drive"

    return-object v0
.end method


# virtual methods
.method public downloadFile(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Ljava/io/File;Lcom/epson/iprint/storage/gdrivev3/Canceller;)Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
    .locals 6
    .param p3    # Lcom/epson/iprint/storage/gdrivev3/Canceller;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 81
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/DownloadFile;

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/epson/iprint/storage/gdrivev3/DownloadFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->getFileInputStream(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Ljava/io/InputStream;

    move-result-object p1

    const/4 v1, 0x0

    .line 83
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/16 p2, 0x400

    .line 84
    :try_start_1
    new-array p2, p2, [B

    .line 86
    :goto_0
    invoke-virtual {p1, p2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-ltz v3, :cond_0

    invoke-interface {p3}, Lcom/epson/iprint/storage/gdrivev3/Canceller;->canceled()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    .line 87
    invoke-virtual {v2, p2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 89
    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    :cond_1
    return-object v0

    :catchall_0
    move-exception p2

    move-object p3, v1

    goto :goto_1

    :catch_0
    move-exception p2

    .line 82
    :try_start_3
    throw p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p3

    move-object v5, p3

    move-object p3, p2

    move-object p2, v5

    :goto_1
    if-eqz p3, :cond_2

    .line 89
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_5
    invoke-virtual {p3, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    :goto_2
    throw p2
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p2

    goto :goto_3

    :catch_2
    move-exception p2

    move-object v1, p2

    .line 82
    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :goto_3
    if-eqz p1, :cond_4

    if-eqz v1, :cond_3

    .line 89
    :try_start_7
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_4

    :catch_3
    move-exception p1

    invoke-virtual {v1, p1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    :cond_4
    :goto_4
    throw p2
.end method

.method public downloadPdf(Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;Ljava/io/File;)Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 67
    invoke-virtual {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->getId()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 72
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 73
    :try_start_0
    iget-object v2, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mDrive:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v2}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v2

    const-string v3, "application/pdf"

    invoke-virtual {v2, p1, v3}, Lcom/google/api/services/drive/Drive$Files;->export(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Export;

    move-result-object p1

    .line 74
    invoke-virtual {p1, v0}, Lcom/google/api/services/drive/Drive$Files$Export;->executeMediaAndDownloadTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 76
    new-instance p1, Lcom/epson/iprint/storage/gdrivev3/DownloadFile;

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    const-string v0, "application/pdf"

    invoke-direct {p1, p2, v0}, Lcom/epson/iprint/storage/gdrivev3/DownloadFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    move-object v1, p1

    .line 72
    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-eqz v1, :cond_0

    .line 75
    :try_start_2
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception p2

    invoke-virtual {v1, p2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :goto_1
    throw p1

    .line 69
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public isRootFolder()Z
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mFolderManager:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;

    invoke-static {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->access$000(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;)Z

    move-result v0

    return v0
.end method

.method public listFile(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Ljava/util/ArrayList;
    .locals 6
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 153
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mDrive:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mFolderManager:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;

    invoke-static {v1, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->access$100(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V

    .line 155
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mFolderManager:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->getCurrentFolderId()Ljava/lang/String;

    move-result-object p1

    .line 157
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    .line 162
    :cond_0
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive$Files;->list()Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\' in parents and trashed = false and (mimeType contains \'image/\' or mimeType contains \'application/\' or mimeType contains \'text/\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/api/services/drive/Drive$Files$List;->setQ(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v3

    const-string v4, "nextPageToken, files(id, name, mimeType, trashed)"

    .line 165
    invoke-virtual {v3, v4}, Lcom/google/api/services/drive/Drive$Files$List;->setFields(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v3

    .line 166
    invoke-virtual {v3, v2}, Lcom/google/api/services/drive/Drive$Files$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v2

    .line 169
    invoke-virtual {v2}, Lcom/google/api/services/drive/Drive$Files$List;->execute()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/drive/model/FileList;

    .line 171
    iget-object v3, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->mFolderManager:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;

    invoke-static {v3}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->access$000(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 172
    new-instance v3, Lcom/epson/iprint/storage/gdrivev3/ParentFolder;

    invoke-direct {v3}, Lcom/epson/iprint/storage/gdrivev3/ParentFolder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    :cond_1
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getFiles()Ljava/util/List;

    move-result-object v3

    .line 176
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/drive/model/File;

    .line 177
    new-instance v5, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;

    invoke-direct {v5, v4}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;-><init>(Lcom/google/api/services/drive/model/File;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    :cond_2
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    return-object v1
.end method

.method public startDownload(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;)Lcom/epson/iprint/storage/gdrivev3/DownloadTask;
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 47
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1, p0, p3}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;-><init>(Landroid/content/Context;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;)V

    const/4 p1, 0x1

    .line 48
    new-array p1, p1, [Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    const/4 p3, 0x0

    aput-object p2, p1, p3

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-object v0
.end method

.method public startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;)V
    .locals 2
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 55
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;

    invoke-direct {v0, p0, p2}, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;-><init>(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;)V

    const/4 p2, 0x1

    new-array p2, p2, [Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    const/4 v1, 0x0

    aput-object p1, p2, v1

    invoke-virtual {v0, p2}, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.class public Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;
.super Lepson/print/ActivityIACommon;
.source "ListAndDownloadActivity.java"

# interfaces
.implements Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
.implements Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;
.implements Lcom/epson/iprint/storage/OneButtonDialogFragment$DialogCallback;
.implements Lcom/epson/iprint/storage/ConfirmCancelDialog$DialogCancelListener;


# static fields
.field private static final DIALOG_TAG_CONFIRM_CANCEL:Ljava/lang/String; = "confirm-cancel"

.field private static final DIALOG_TAG_DOWNLOAD_FAILED:Ljava/lang/String; = "download_failed"

.field private static final DIALOG_TAG_OFFLINE_ERROR:Ljava/lang/String; = "offline_error"

.field private static final DIALOG_TAG_SIGN_IN_FAILED:Ljava/lang/String; = "sign-in_failed"

.field private static final FRAGMENT_TAG_PROGRESS:Ljava/lang/String; = "fragment-progress"

.field private static final REQUEST_CODE_CHECK_PLAY_SERVICE:I = 0xb

.field private static final REQUEST_CODE_DOWNLOAD_PREVIEW_END:I = 0xc

.field private static final REQUEST_CODE_SIGN_IN:I = 0xa

.field private static final REQUEST_CODE_SIGN_IN_ADD_SCOPE:I = 0xd


# instance fields
.field private mActivityForeground:Z

.field private mFileListAdapter:Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;

.field private mForegroundRunnableList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

.field private mIsDownloadInterruption:Z

.field private mListView:Landroid/widget/ListView;

.field private mPosition:I

.field private mSignInMessage:Landroid/widget/TextView;

.field private mSignInOutButtonEnabled:Z

.field private mSignInOutButtonType:I

.field private mSignInStartActivityCallback:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 42
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 436
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mIsDownloadInterruption:Z

    .line 437
    iput v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mPosition:I

    return-void
.end method

.method static synthetic access$002(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;I)I
    .locals 0

    .line 42
    iput p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mPosition:I

    return p1
.end method

.method static synthetic access$100(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;I)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->localOnItemClick(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;Ljava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->dismissDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;ILjava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->showErrorDialog(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;ZI)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->showProgressFragment(ZI)V

    return-void
.end method

.method static synthetic access$500(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->removeProgressIfExists()V

    return-void
.end method

.method private clearFragments()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->removeProgressIfExists()V

    .line 126
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->deleteAllDialog()V

    return-void
.end method

.method private deleteAllDialog()V
    .locals 4

    const-string v0, "sign-in_failed"

    const-string v1, "download_failed"

    const-string v2, "offline_error"

    const-string v3, "confirm-cancel"

    .line 130
    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    .line 137
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 138
    invoke-direct {p0, v3}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->dismissDialog(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 143
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mActivityForeground:Z

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    new-instance v1, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$2;-><init>(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    return-void

    .line 153
    :cond_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 154
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 156
    check-cast p1, Landroid/support/v4/app/DialogFragment;

    .line 157
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_1
    return-void
.end method

.method private localOnItemClick(I)V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mFileListAdapter:Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->getDriveItem(I)Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->itemSelected(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V

    return-void
.end method

.method private processForegroundList()V
    .locals 2

    .line 236
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 237
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 239
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 240
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private removeProgressIfExists()V
    .locals 2

    .line 466
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mActivityForeground:Z

    if-nez v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    new-instance v1, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$5;

    invoke-direct {v1, p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$5;-><init>(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    return-void

    .line 476
    :cond_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "fragment-progress"

    .line 477
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 479
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 480
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_1
    return-void
.end method

.method private showErrorDialog(ILjava/lang/String;)V
    .locals 2

    .line 302
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mActivityForeground:Z

    if-nez v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    new-instance v1, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$3;-><init>(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    return-void

    .line 312
    :cond_0
    invoke-static {p1, p2}, Lcom/epson/iprint/storage/OneButtonDialogFragment;->newInstance(ILjava/lang/String;)Lcom/epson/iprint/storage/OneButtonDialogFragment;

    move-result-object p1

    .line 313
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/epson/iprint/storage/OneButtonDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showProgressFragment(ZI)V
    .locals 2

    .line 345
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mActivityForeground:Z

    if-nez v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    new-instance v1, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$4;-><init>(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;ZI)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    return-void

    .line 355
    :cond_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "confirm-cancel"

    .line 356
    invoke-static {p1, p2, v1}, Lcom/epson/iprint/storage/LocalProgressDialog;->newInstance(ZILjava/lang/String;)Lcom/epson/iprint/storage/LocalProgressDialog;

    move-result-object p1

    .line 359
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p2

    const v0, 0x1020002

    const-string v1, "fragment-progress"

    .line 360
    invoke-virtual {p2, v0, p1, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    .line 361
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method


# virtual methods
.method public buttonPressed(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, -0x1

    .line 415
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x28c3f354

    if-eq v1, v2, :cond_3

    const v2, 0x95a9fd4

    if-eq v1, v2, :cond_2

    const v2, 0x15dfd887

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "sign-in_failed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v1, "download_failed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const-string v1, "offline_error"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x2

    :cond_4
    :goto_0
    packed-switch v0, :pswitch_data_0

    return-void

    .line 423
    :pswitch_0
    new-instance p1, Landroid/content/Intent;

    const-string v0, "android.settings.WIRELESS_SETTINGS"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->startActivity(Landroid/content/Intent;)V

    .line 424
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->finish()V

    return-void

    :pswitch_1
    return-void

    :pswitch_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public cancel()V
    .locals 1

    .line 431
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->cancelDownload()V

    return-void
.end method

.method public changeSignInOutButton(I)V
    .locals 4

    .line 291
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInMessage:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-nez p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mListView:Landroid/widget/ListView;

    if-nez p1, :cond_1

    const/16 v1, 0x8

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 297
    iput p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInOutButtonType:I

    .line 298
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public clearListItems()V
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mFileListAdapter:Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->clearItems()V

    return-void
.end method

.method public dismissProgress()V
    .locals 1

    const-string v0, "confirm-cancel"

    .line 376
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->dismissDialog(Ljava/lang/String;)V

    .line 377
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->removeProgressIfExists()V

    const/4 v0, 0x1

    .line 378
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInOutButtonEnabled:Z

    .line 379
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public finishActivity()V
    .locals 0

    .line 403
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->finish()V

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    return-object p0
.end method

.method public getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    return-object p0
.end method

.method public getPlayServiceRequestCode()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method

.method public getSingInAddScopeRequestCode()I
    .locals 1

    const/16 v0, 0xd

    return v0
.end method

.method public listFiles(Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 385
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->clearListItems()V

    const p1, 0x7f0e053b

    const-string v0, "download_failed"

    .line 387
    invoke-direct {p0, p1, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->showErrorDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mFileListAdapter:Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->setDriveFile(Ljava/util/ArrayList;)V

    :goto_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    return-void

    .line 218
    :pswitch_0
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    const/4 p3, -0x1

    if-ne p2, p3, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->onAddScopeCompleted(Z)V

    return-void

    .line 222
    :pswitch_1
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->onPreviewActivityEnd()V

    return-void

    :pswitch_2
    return-void

    .line 211
    :pswitch_3
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInStartActivityCallback:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;

    if-eqz p1, :cond_1

    .line 212
    invoke-interface {p1, p2, p3}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;->onActivityResult(ILandroid/content/Intent;)V

    const/4 p1, 0x0

    .line 213
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInStartActivityCallback:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->backKeyPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 86
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a0035

    .line 87
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->setContentView(I)V

    const v0, 0x7f0e038e

    .line 89
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->setActionBar(Ljava/lang/String;Z)V

    .line 91
    invoke-static {p0}, Lcom/epson/iprint/storage/gdrivev3/Injection;->provideDownloadPresenter(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;)Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    .line 92
    iput-boolean v1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInOutButtonEnabled:Z

    .line 94
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    const v0, 0x102000a

    .line 97
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mListView:Landroid/widget/ListView;

    .line 98
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 99
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;

    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mFileListAdapter:Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;

    .line 100
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mFileListAdapter:Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 101
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$1;

    invoke-direct {v2, p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity$1;-><init>(Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x1020004

    .line 109
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInMessage:Landroid/widget/TextView;

    .line 111
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInMessage:Landroid/widget/TextView;

    const v2, 0x7f0e0464

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 112
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInMessage:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 113
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInMessage:Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    if-eqz p1, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->clearFragments()V

    .line 121
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->initView()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .line 186
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0007

    .line 187
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 189
    iget v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInOutButtonType:I

    const v1, 0x7f080216

    const v2, 0x7f080215

    if-nez v0, :cond_0

    .line 192
    invoke-interface {p1, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 193
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInOutButtonEnabled:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 195
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 196
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInOutButtonEnabled:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 172
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    .line 181
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 177
    :pswitch_0
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->signInOrDisconnectClicked()V

    return v1

    .line 174
    :pswitch_1
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->signInOrDisconnectClicked()V

    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x7f080215
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .line 246
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 247
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->activityOnPause()V

    const/4 v0, 0x0

    .line 248
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mActivityForeground:Z

    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .line 229
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPostResume()V

    const/4 v0, 0x1

    .line 230
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mActivityForeground:Z

    .line 231
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->processForegroundList()V

    .line 232
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->activityOnResume()V

    return-void
.end method

.method protected onRestart()V
    .locals 1

    .line 454
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onRestart()V

    .line 455
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mIsDownloadInterruption:Z

    if-eqz v0, :cond_0

    .line 457
    iget v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mPosition:I

    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->localOnItemClick(I)V

    :cond_0
    const/4 v0, 0x0

    .line 459
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mIsDownloadInterruption:Z

    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 441
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    .line 443
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mGoogleDownloadPresenter:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;->isDownloading()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    .line 444
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mIsDownloadInterruption:Z

    .line 446
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->cancel()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 448
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mIsDownloadInterruption:Z

    :goto_0
    return-void
.end method

.method public requestSignIn(Landroid/content/Intent;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 274
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInStartActivityCallback:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;

    const/16 p2, 0xa

    .line 275
    invoke-virtual {p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public setSignInButtonEnabled(Z)V
    .locals 0

    .line 286
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public showDownloadErrorDialog()V
    .locals 2

    const-string v0, "download_failed"

    const v1, 0x7f0e0323

    .line 323
    invoke-direct {p0, v1, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->showErrorDialog(ILjava/lang/String;)V

    return-void
.end method

.method public showDownloadProgress()V
    .locals 2

    const/4 v0, 0x0

    .line 366
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInOutButtonEnabled:Z

    .line 367
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->invalidateOptionsMenu()V

    const/4 v0, 0x1

    const v1, 0x7f0e0327

    .line 370
    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->showProgressFragment(ZI)V

    return-void
.end method

.method public showOfflineErrorDialog()V
    .locals 2

    const-string v0, "offline_error"

    const v1, 0x7f0e03d8

    .line 328
    invoke-direct {p0, v1, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->showErrorDialog(ILjava/lang/String;)V

    return-void
.end method

.method public showProgress()V
    .locals 1

    const/4 v0, 0x0

    .line 338
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->mSignInOutButtonEnabled:Z

    .line 339
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->invalidateOptionsMenu()V

    .line 341
    invoke-direct {p0, v0, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->showProgressFragment(ZI)V

    return-void
.end method

.method public showSignInFailDialog()V
    .locals 2

    const-string v0, "sign-in_failed"

    const v1, 0x7f0e02ad

    .line 318
    invoke-direct {p0, v1, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->showErrorDialog(ILjava/lang/String;)V

    return-void
.end method

.method public startPreviewActivity(Ljava/lang/String;)V
    .locals 2

    .line 395
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    .line 396
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getPreviewIntent(Ljava/lang/String;Landroid/content/Context;Lcom/epson/iprint/prtlogger/PrintLog;)Landroid/content/Intent;

    move-result-object p1

    const/16 v0, 0xc

    .line 398
    invoke-virtual {p0, p1, v0}, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

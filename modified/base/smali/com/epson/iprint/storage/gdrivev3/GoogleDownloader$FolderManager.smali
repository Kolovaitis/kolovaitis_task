.class Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;
.super Ljava/lang/Object;
.source "GoogleDownloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FolderManager"
.end annotation


# instance fields
.field private mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

.field private final mParentDirList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mParentDirList:Ljava/util/LinkedList;

    const/4 v0, 0x0

    .line 207
    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;)Z
    .locals 0

    .line 199
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->isRootFolder()Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V
    .locals 0

    .line 199
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->changeCurrentFolder(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V

    return-void
.end method

.method private changeCurrentFolder(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V
    .locals 2
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    .line 219
    :cond_0
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->isFolder()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 227
    :cond_1
    instance-of v0, p1, Lcom/epson/iprint/storage/gdrivev3/ParentFolder;

    if-eqz v0, :cond_2

    .line 229
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->gotoParent()V

    goto :goto_0

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    if-eqz v0, :cond_3

    .line 234
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mParentDirList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 236
    :cond_3
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    :goto_0
    return-void
.end method

.method private gotoParent()V
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mParentDirList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 253
    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    .line 254
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->gotoRoot()V

    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mParentDirList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    if-nez v0, :cond_1

    .line 260
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->gotoRoot()V

    return-void

    .line 264
    :cond_1
    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    .line 265
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->getId()Ljava/lang/String;

    return-void
.end method

.method private gotoRoot()V
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mParentDirList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    const/4 v0, 0x0

    .line 245
    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    return-void
.end method

.method private isRootFolder()Z
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method getCurrentFolderId()Ljava/lang/String;
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader$FolderManager;->mCurrentFolder:Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    if-nez v0, :cond_0

    const-string v0, "root"

    return-object v0

    .line 272
    :cond_0
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

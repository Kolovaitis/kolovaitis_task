.class public interface abstract Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;
.super Ljava/lang/Object;
.source "GoogleDownloadContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserActionListener"
.end annotation


# virtual methods
.method public abstract activityOnPause()V
.end method

.method public abstract activityOnResume()V
.end method

.method public abstract backKeyPressed()V
.end method

.method public abstract cancelDownload()V
.end method

.method public abstract initView()V
.end method

.method public abstract isDownloading()Z
.end method

.method public abstract itemSelected(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onAddScopeCompleted(Z)V
.end method

.method public abstract onPreviewActivityEnd()V
.end method

.method public abstract signInOrDisconnectClicked()V
.end method

.method public abstract startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
.end method

.class public Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;
.super Ljava/lang/Object;
.source "DownloaderWrapper.java"


# instance fields
.field private mDownloadTask:Lcom/epson/iprint/storage/gdrivev3/DownloadTask;

.field private mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelDownload()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mDownloadTask:Lcom/epson/iprint/storage/gdrivev3/DownloadTask;

    if-nez v0, :cond_0

    return-void

    .line 61
    :cond_0
    invoke-virtual {v0}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->cancelTask()V

    return-void
.end method

.method public createDownloader(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)Z
    .locals 0
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 16
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->createGoogleDownloader(Landroid/content/Context;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    .line 17
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public invalidateDownloader()V
    .locals 1

    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    return-void
.end method

.method public isDownloadValid()Z
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isRootFolder()Z
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->isRootFolder()Z

    move-result v0

    return v0
.end method

.method public onDownloadPreviewEnd()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mDownloadTask:Lcom/epson/iprint/storage/gdrivev3/DownloadTask;

    if-nez v0, :cond_0

    return-void

    .line 71
    :cond_0
    invoke-virtual {v0}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->deleteDownloadFile()V

    return-void
.end method

.method public startDownload(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;)V
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 29
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0, p1, p2, p3}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->startDownload(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;)Lcom/epson/iprint/storage/gdrivev3/DownloadTask;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mDownloadTask:Lcom/epson/iprint/storage/gdrivev3/DownloadTask;

    return-void

    .line 30
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;)V
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 39
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;)V

    return-void

    .line 40
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

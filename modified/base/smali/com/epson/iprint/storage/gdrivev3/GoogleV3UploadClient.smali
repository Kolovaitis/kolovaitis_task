.class public Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;
.super Lcom/epson/iprint/storage/StorageServiceClient;
.source "GoogleV3UploadClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;,
        Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;,
        Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;
    }
.end annotation


# static fields
.field public static final UPLOAD_FOLDER_NAME:Ljava/lang/String; = "Epson iPrint"

.field private static sCheckSignIn:Z


# instance fields
.field private mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageServiceClient;-><init>()V

    .line 48
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-direct {v0}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    const/4 v0, 0x0

    .line 49
    sput-boolean v0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;->sCheckSignIn:Z

    return-void
.end method

.method public static isCheckSignIn()Z
    .locals 1

    .line 39
    sget-boolean v0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;->sCheckSignIn:Z

    return v0
.end method

.method public static setSingInValue(Z)V
    .locals 0

    .line 43
    sput-boolean p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;->sCheckSignIn:Z

    return-void
.end method


# virtual methods
.method public getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
    .locals 0

    .line 59
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 65
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    const v0, 0x7f0e038e

    .line 95
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
    .locals 2

    .line 54
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;

    new-instance v1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;

    invoke-direct {v1, p2, p3, p4}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;-><init>(Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;-><init>(Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;Landroid/content/Context;Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;)V

    return-object v0
.end method

.method public isSignedIn(Landroid/content/Context;)Z
    .locals 2

    .line 77
    invoke-static {}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;->isCheckSignIn()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;->UPLOAD:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;

    invoke-virtual {v0, p1, v1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->isSignInValid(Landroid/content/Context;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Z

    move-result p1

    return p1
.end method

.method public isSupportedUploadType(Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public revokeSignedInData(Landroid/app/Activity;)Z
    .locals 1

    .line 87
    invoke-static {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->getDisconnectIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 p1, 0x0

    return p1
.end method

.class public Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FileListAdapter.java"


# instance fields
.field private mDriveFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;"
        }
    .end annotation
.end field

.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 21
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 22
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public clearItems()V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->mDriveFiles:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 98
    :cond_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->mDriveFiles:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 36
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getDriveItem(I)Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->mDriveFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    return-object p1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 58
    iget-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a0061

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 61
    :cond_0
    iget-object p3, p0, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->mDriveFiles:Ljava/util/ArrayList;

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    .line 62
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->getName()Ljava/lang/String;

    move-result-object p3

    const v1, 0x7f07009f

    .line 67
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->isFolder()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 68
    instance-of p1, p1, Lcom/epson/iprint/storage/gdrivev3/ParentFolder;

    if-eqz p1, :cond_1

    const p1, 0x7f070103

    const v1, 0x7f070103

    goto :goto_0

    :cond_1
    const p1, 0x7f0700a0

    const v1, 0x7f0700a0

    :goto_0
    const p1, 0x7f0700e9

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    const v2, 0x7f080142

    .line 76
    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 77
    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f080141

    .line 79
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 80
    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const p3, 0x7f080084

    .line 83
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    if-eqz p1, :cond_3

    .line 85
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_3
    const/16 p1, 0x8

    .line 88
    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    return-object p2
.end method

.method public setDriveFile(Ljava/util/ArrayList;)V
    .locals 0
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;)V"
        }
    .end annotation

    .line 27
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->mDriveFiles:Ljava/util/ArrayList;

    .line 28
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/FileListAdapter;->notifyDataSetChanged()V

    return-void
.end method

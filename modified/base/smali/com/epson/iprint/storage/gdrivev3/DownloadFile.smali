.class public Lcom/epson/iprint/storage/gdrivev3/DownloadFile;
.super Ljava/lang/Object;
.source "DownloadFile.java"

# interfaces
.implements Lcom/epson/iprint/storage/gdrivev3/OnlineFile;


# instance fields
.field private final mMimeType:Ljava/lang/String;

.field private final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadFile;->mName:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadFile;->mMimeType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadFile;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadFile;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public isDisplayFile()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isFolder()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

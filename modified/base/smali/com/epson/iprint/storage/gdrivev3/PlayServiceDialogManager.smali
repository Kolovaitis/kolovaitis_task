.class public Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;
.super Ljava/lang/Object;
.source "PlayServiceDialogManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;
    }
.end annotation


# instance fields
.field private mGooglePlayServiceDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkPlayService(Landroid/app/Activity;ILcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;)Z
    .locals 3
    .param p1    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 42
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->mGooglePlayServiceDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->mGooglePlayServiceDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 48
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    .line 49
    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isUserResolvableError(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 52
    new-instance v2, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$1;

    invoke-direct {v2, p0, p3}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$1;-><init>(Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;)V

    invoke-virtual {v0, p1, v1, p2, v2}, Lcom/google/android/gms/common/GoogleApiAvailability;->getErrorDialog(Landroid/app/Activity;IILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->mGooglePlayServiceDialog:Landroid/app/Dialog;

    .line 59
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->mGooglePlayServiceDialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    const/4 p1, 0x1

    return p1

    .line 64
    :cond_1
    invoke-interface {p3}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;->playServiceInstallCancel()V

    :cond_2
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public checkPlayService(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;)Z
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 73
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getPlayServiceRequestCode()I

    move-result p1

    invoke-direct {p0, v0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->checkPlayService(Landroid/app/Activity;ILcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;)Z

    move-result p1

    return p1
.end method

.method public onActivityPaused()V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->mGooglePlayServiceDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->mGooglePlayServiceDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

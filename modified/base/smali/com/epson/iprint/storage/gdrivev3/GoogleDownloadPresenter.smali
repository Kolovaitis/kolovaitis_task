.class public Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;
.super Ljava/lang/Object;
.source "GoogleDownloadPresenter.java"

# interfaces
.implements Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$UserActionListener;
.implements Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;
.implements Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;
.implements Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;
.implements Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;,
        Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;,
        Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;
    }
.end annotation


# instance fields
.field private final mDownloadViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;",
            ">;"
        }
    .end annotation
.end field

.field private final mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

.field private final mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

.field private mIsDownloading:Z

.field private final mMisc:Lcom/epson/iprint/storage/Network;

.field private final mPlayServiceDialogManager:Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

.field private mSignInStatus:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

.field private mStartListDone:Z

.field private mTaskRunning:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;


# direct methods
.method public constructor <init>(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;Lcom/epson/iprint/storage/Network;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;)V
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/epson/iprint/storage/Network;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 247
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIsDownloading:Z

    .line 53
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    .line 54
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mPlayServiceDialogManager:Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

    .line 55
    iput-object p3, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    .line 56
    iput-object p4, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mMisc:Lcom/epson/iprint/storage/Network;

    .line 57
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mDownloadViewReference:Ljava/lang/ref/WeakReference;

    .line 59
    sget-object p1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_OUT:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mSignInStatus:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    .line 60
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mStartListDone:Z

    .line 61
    sget-object p1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;->STOP:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mTaskRunning:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;

    return-void
.end method

.method private changeSignInStatus(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;)V
    .locals 1

    .line 230
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mSignInStatus:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    .line 231
    sget-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_IN:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    if-ne p2, v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->changeSignInOutButton(I)V

    return-void
.end method

.method private getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mDownloadViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    return-object v0
.end method

.method private onTaskEnd(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;)V
    .locals 1

    .line 323
    sget-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;->STOP:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mTaskRunning:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;

    .line 325
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->dismissProgress()V

    const/4 v0, 0x1

    .line 326
    invoke-interface {p1, v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->setSignInButtonEnabled(Z)V

    return-void
.end method

.method private onTaskStart(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;)V
    .locals 1

    .line 330
    sget-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;->RUNNING:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mTaskRunning:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;

    .line 332
    sget-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;->DOWNLOAD:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;

    if-ne p2, v0, :cond_0

    .line 333
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->showDownloadProgress()V

    goto :goto_0

    .line 335
    :cond_0
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->showProgress()V

    :goto_0
    const/4 p2, 0x0

    .line 338
    invoke-interface {p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->setSignInButtonEnabled(Z)V

    return-void
.end method

.method private start1stListTask(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    .locals 1
    .param p2    # Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    .line 208
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->createDownloader(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)Z

    const/4 p1, 0x0

    .line 210
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V

    return-void

    .line 206
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method


# virtual methods
.method public activityOnPause()V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mPlayServiceDialogManager:Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->onActivityPaused()V

    return-void
.end method

.method public activityOnResume()V
    .locals 4

    .line 84
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mMisc:Lcom/epson/iprint/storage/Network;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/epson/iprint/storage/Network;->selectSimpleAp(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Z)V

    .line 91
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mMisc:Lcom/epson/iprint/storage/Network;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/epson/iprint/storage/Network;->isOnline(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 92
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->showOfflineErrorDialog()V

    return-void

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mPlayServiceDialogManager:Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->checkPlayService(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    .line 102
    :cond_2
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mTaskRunning:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;

    sget-object v2, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;->STOP:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskState;

    if-eq v1, v2, :cond_3

    return-void

    :cond_3
    const/4 v1, 0x1

    .line 106
    invoke-interface {v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->setSignInButtonEnabled(Z)V

    .line 110
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v2

    invoke-interface {v2}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget-object v3, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;->DOWNLOAD:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;

    invoke-virtual {v1, v2, v3}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->isSignInValid(Landroid/content/Context;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 112
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_IN:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->changeSignInStatus(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;)V

    .line 114
    iget-boolean v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mStartListDone:Z

    if-nez v1, :cond_5

    .line 115
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v2

    invoke-interface {v2}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getSignInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->start1stListTask(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    goto :goto_0

    .line 118
    :cond_4
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_OUT:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->changeSignInStatus(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public backKeyPressed()V
    .locals 3

    .line 272
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mSignInStatus:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    sget-object v2, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_IN:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    if-ne v1, v2, :cond_3

    .line 278
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-virtual {v1}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->isDownloadValid()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 281
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-virtual {v1}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->isRootFolder()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 283
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->finishActivity()V

    goto :goto_0

    .line 286
    :cond_1
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/ParentFolder;

    invoke-direct {v0}, Lcom/epson/iprint/storage/gdrivev3/ParentFolder;-><init>()V

    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V

    goto :goto_0

    .line 279
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 289
    :cond_3
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->finishActivity()V

    :goto_0
    return-void
.end method

.method public cancelDownload()V
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->cancelDownload()V

    return-void
.end method

.method public initView()V
    .locals 2

    .line 71
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 76
    :cond_0
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_OUT:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->changeSignInStatus(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;)V

    const/4 v1, 0x0

    .line 78
    invoke-interface {v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->setSignInButtonEnabled(Z)V

    .line 79
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->dismissProgress()V

    return-void
.end method

.method public isDownloading()Z
    .locals 1

    .line 127
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIsDownloading:Z

    return v0
.end method

.method public itemSelected(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V
    .locals 2
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 251
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 256
    :cond_0
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-virtual {v1}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->isDownloadValid()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 260
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 261
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V

    goto :goto_0

    .line 263
    :cond_1
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;->DOWNLOAD:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;

    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->onTaskStart(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;)V

    const/4 v1, 0x1

    .line 264
    iput-boolean v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIsDownloading:Z

    .line 265
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v0

    invoke-virtual {v1, v0, p1, p0}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->startDownload(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;)V

    :goto_0
    return-void

    .line 257
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public listComplete(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;)V"
        }
    .end annotation

    .line 237
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 242
    :cond_0
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->onTaskEnd(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;)V

    .line 244
    invoke-interface {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->listFiles(Ljava/util/ArrayList;)V

    return-void
.end method

.method public onAddScopeCompleted(Z)V
    .locals 2

    .line 179
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    .line 186
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v0

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getSignInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 184
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->onSignInComplete(ZLcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    return-void
.end method

.method public onDisconnectCompleted()V
    .locals 2

    .line 192
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-virtual {v1}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->invalidateDownloader()V

    .line 199
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_OUT:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->changeSignInStatus(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;)V

    const/4 v1, 0x1

    .line 200
    invoke-interface {v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->setSignInButtonEnabled(Z)V

    .line 201
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->clearListItems()V

    return-void
.end method

.method public onDownloadComplete(ZLjava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 305
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    const/4 v1, 0x0

    .line 306
    iput-boolean v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIsDownloading:Z

    if-nez v0, :cond_0

    return-void

    .line 312
    :cond_0
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->onTaskEnd(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;)V

    if-eqz p2, :cond_1

    .line 314
    invoke-interface {v0, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->startPreviewActivity(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    .line 317
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->showDownloadErrorDialog()V

    :cond_2
    :goto_0
    return-void
.end method

.method public onPreviewActivityEnd()V
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->onDownloadPreviewEnd()V

    return-void
.end method

.method public onSignInComplete(ZLcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    .locals 2

    .line 157
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 162
    :cond_0
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->clearListItems()V

    if-eqz p1, :cond_1

    .line 163
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_IN:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_OUT:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->changeSignInStatus(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;)V

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 169
    invoke-direct {p0, v0, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->start1stListTask(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    goto :goto_1

    .line 167
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_3
    const/4 p1, 0x1

    .line 171
    invoke-interface {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->setSignInButtonEnabled(Z)V

    .line 172
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->dismissProgress()V

    .line 173
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->showSignInFailDialog()V

    :goto_1
    return-void
.end method

.method public playServiceInstallCancel()V
    .locals 2

    .line 131
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 136
    invoke-interface {v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->setSignInButtonEnabled(Z)V

    .line 137
    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->showSignInFailDialog()V

    return-void
.end method

.method public signInOrDisconnectClicked()V
    .locals 3

    .line 142
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 147
    invoke-interface {v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->setSignInButtonEnabled(Z)V

    .line 148
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mSignInStatus:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    sget-object v2, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_IN:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    if-ne v1, v2, :cond_1

    .line 149
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->disconnectAccount(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V

    goto :goto_0

    .line 151
    :cond_1
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-interface {v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;->getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;

    move-result-object v0

    sget-object v2, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;->DOWNLOAD:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;

    invoke-virtual {v1, p0, v0, v2}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->startSignIn(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)V

    :goto_0
    return-void
.end method

.method public startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V
    .locals 2
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 215
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->getDownloadView()Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 220
    :cond_0
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;->FILE_LIST:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;

    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->onTaskStart(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$TaskType;)V

    const/4 v0, 0x1

    .line 221
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mStartListDone:Z

    .line 223
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;->mGoogleDownloader:Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-virtual {v0, p1, p0}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;->startListTask(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;)V

    return-void
.end method

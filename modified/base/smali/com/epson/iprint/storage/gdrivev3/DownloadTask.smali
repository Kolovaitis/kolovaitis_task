.class public Lcom/epson/iprint/storage/gdrivev3/DownloadTask;
.super Landroid/os/AsyncTask;
.source "DownloadTask.java"

# interfaces
.implements Lcom/epson/iprint/storage/gdrivev3/Canceller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
        "Ljava/lang/Void;",
        "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
        ">;",
        "Lcom/epson/iprint/storage/gdrivev3/Canceller;"
    }
.end annotation


# static fields
.field private static final DOWNLOAD_TMP_FILE_NAME_BODY:Ljava/lang/String; = "google_v3"


# instance fields
.field private final mApplicationContext:Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field

.field private volatile mDownloadCancelled:Z

.field private final mDownloadCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;

.field private volatile mDownloadFile:Ljava/io/File;

.field private final mMyDrive:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

.field private mThreadException:Ljava/lang/Exception;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 45
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 46
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCancelled:Z

    .line 47
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mApplicationContext:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mMyDrive:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    .line 49
    iput-object p3, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;

    const/4 p1, 0x0

    .line 50
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadFile:Ljava/io/File;

    return-void
.end method

.method static getExtensionFromMimeType(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    .line 103
    :cond_0
    invoke-interface {p0}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-static {v0}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    invoke-interface {p0}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lepson/common/Utils;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 108
    invoke-static {p0}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private static getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 115
    invoke-static {p0}, Lepson/common/Utils;->getMimeExt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method

.method private getOutFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 174
    :try_start_0
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->prepareOutDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "google_v3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private prepareOutDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 188
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getDownloadDir()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 189
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_1

    .line 190
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 191
    :cond_0
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1

    .line 194
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 196
    array-length v1, p1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    aget-object v3, p1, v2

    .line 197
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    return-object v0
.end method


# virtual methods
.method public cancelTask()V
    .locals 1

    const/4 v0, 0x1

    .line 159
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCancelled:Z

    .line 160
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->cancel(Z)Z

    return-void
.end method

.method public canceled()Z
    .locals 1

    .line 155
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCancelled:Z

    return v0
.end method

.method public deleteDownloadFile()V
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadFile:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    const/4 v0, 0x0

    .line 213
    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadFile:Ljava/io/File;

    :cond_0
    return-void
.end method

.method protected varargs doInBackground([Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Lcom/epson/iprint/storage/gdrivev3/OnlineFile;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 58
    aget-object p1, p1, v1

    if-nez p1, :cond_1

    return-object v0

    .line 64
    :cond_1
    :try_start_0
    instance-of v1, p1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;

    if-eqz v1, :cond_3

    move-object v1, p1

    check-cast v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;

    .line 65
    invoke-virtual {v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->isGoogleDocuments()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 68
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mApplicationContext:Landroid/content/Context;

    const-string v2, ".pdf"

    invoke-direct {p0, v1, v2}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->getOutFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 69
    iput-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadFile:Ljava/io/File;

    if-nez v1, :cond_2

    return-object v0

    .line 74
    :cond_2
    iget-object v2, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mMyDrive:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    check-cast p1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;

    invoke-virtual {v2, p1, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->downloadPdf(Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;Ljava/io/File;)Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    move-result-object p1

    return-object p1

    .line 76
    :cond_3
    invoke-static {p1}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->getExtensionFromMimeType(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Ljava/lang/String;

    move-result-object v1

    .line 77
    iget-object v2, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mApplicationContext:Landroid/content/Context;

    invoke-direct {p0, v2, v1}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->getOutFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 78
    iput-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadFile:Ljava/io/File;

    if-nez v1, :cond_4

    return-object v0

    .line 84
    :cond_4
    iget-object v2, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mMyDrive:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    invoke-virtual {v2, p1, v1, p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->downloadFile(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;Ljava/io/File;Lcom/epson/iprint/storage/gdrivev3/Canceller;)Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 88
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mThreadException:Ljava/lang/Exception;

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, [Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->doInBackground([Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled()V
    .locals 3

    .line 146
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->deleteDownloadFile()V

    .line 147
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;->onDownloadComplete(ZLjava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V
    .locals 3

    .line 124
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCancelled:Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;

    invoke-interface {v0, v1, v2}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;->onDownloadComplete(ZLjava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    if-eqz p1, :cond_2

    .line 129
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->getName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 131
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;

    invoke-interface {v0, v1, p1}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;->onDownloadComplete(ZLjava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_1
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;

    invoke-interface {p1, v0, v2}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;->onDownloadComplete(ZLjava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_2
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mThreadException:Ljava/lang/Exception;

    .line 139
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->deleteDownloadFile()V

    .line 140
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->mDownloadCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;

    invoke-interface {p1, v0, v2}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask$DownloadCompleteListener;->onDownloadComplete(ZLjava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/DownloadTask;->onPostExecute(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)V

    return-void
.end method

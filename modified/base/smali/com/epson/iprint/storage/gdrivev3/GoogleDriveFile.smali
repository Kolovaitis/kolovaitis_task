.class public Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;
.super Ljava/lang/Object;
.source "GoogleDriveFile.java"

# interfaces
.implements Lcom/epson/iprint/storage/gdrivev3/OnlineFile;


# static fields
.field private static sGoogleDocumentsTypeSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDriveFile:Lcom/google/api/services/drive/model/File;


# direct methods
.method public constructor <init>(Lcom/google/api/services/drive/model/File;)V
    .locals 0
    .param p1    # Lcom/google/api/services/drive/model/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->mDriveFile:Lcom/google/api/services/drive/model/File;

    return-void
.end method

.method private isIprintFile()Z
    .locals 2

    .line 61
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->localGetTrashed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 65
    :cond_0
    invoke-static {}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->prepareFileTypeSet()V

    .line 66
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->sGoogleDocumentsTypeSet:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/iprint/storage/StorageServiceClient;->isPrintableFilename(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private localGetTrashed()Z
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->mDriveFile:Lcom/google/api/services/drive/model/File;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/File;->getTrashed()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static declared-synchronized prepareFileTypeSet()V
    .locals 3

    const-class v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;

    monitor-enter v0

    .line 86
    :try_start_0
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->sGoogleDocumentsTypeSet:Ljava/util/HashSet;

    if-nez v1, :cond_0

    .line 87
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->sGoogleDocumentsTypeSet:Ljava/util/HashSet;

    .line 88
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->sGoogleDocumentsTypeSet:Ljava/util/HashSet;

    const-string v2, "application/vnd.google-apps.document"

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->sGoogleDocumentsTypeSet:Ljava/util/HashSet;

    const-string v2, "application/vnd.google-apps.spreadsheet"

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 90
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->sGoogleDocumentsTypeSet:Ljava/util/HashSet;

    const-string v2, "application/vnd.google-apps.drawing"

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->sGoogleDocumentsTypeSet:Ljava/util/HashSet;

    const-string v2, "application/vnd.google-apps.presentation"

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->mDriveFile:Lcom/google/api/services/drive/model/File;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/File;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->mDriveFile:Lcom/google/api/services/drive/model/File;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->mDriveFile:Lcom/google/api/services/drive/model/File;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/File;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDisplayFile()Z
    .locals 3

    .line 45
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->localGetTrashed()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->isFolder()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    .line 53
    :cond_1
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->isIprintFile()Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    return v1
.end method

.method public isFolder()Z
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->mDriveFile:Lcom/google/api/services/drive/model/File;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "application/vnd.google-apps.folder"

    .line 29
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isGoogleDocuments()Z
    .locals 2

    .line 75
    invoke-static {}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->prepareFileTypeSet()V

    .line 77
    sget-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->sGoogleDocumentsTypeSet:Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleDriveFile;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

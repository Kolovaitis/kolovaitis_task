.class public interface abstract Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;
.super Ljava/lang/Object;
.source "GoogleDownloadContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract changeSignInOutButton(I)V
.end method

.method public abstract clearListItems()V
.end method

.method public abstract dismissProgress()V
.end method

.method public abstract finishActivity()V
.end method

.method public abstract getActivityWrapper()Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract listFiles(Ljava/util/ArrayList;)V
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setSignInButtonEnabled(Z)V
.end method

.method public abstract showDownloadErrorDialog()V
.end method

.method public abstract showDownloadProgress()V
.end method

.method public abstract showOfflineErrorDialog()V
.end method

.method public abstract showProgress()V
.end method

.method public abstract showSignInFailDialog()V
.end method

.method public abstract startPreviewActivity(Ljava/lang/String;)V
.end method

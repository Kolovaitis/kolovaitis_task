.class Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;
.super Landroid/os/AsyncTask;
.source "GoogleV3UploadClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UploadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mUploadNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V
    .locals 0

    .line 133
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 134
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->mContext:Landroid/content/Context;

    .line 135
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->mUploadNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    return-void
.end method

.method private notifyUploadEnd(Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->mUploadNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    if-eqz v0, :cond_0

    .line 166
    iget-object v1, p1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;->originalLocalPath:Ljava/lang/String;

    iget-object p1, p1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;->uploadFilename:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 129
    check-cast p1, [Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->doInBackground([Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;)Ljava/lang/Void;
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    const/4 v1, 0x0

    .line 140
    aget-object v2, p1, v1

    if-nez v2, :cond_0

    goto :goto_1

    .line 144
    :cond_0
    aget-object p1, p1, v1

    .line 146
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/epson/iprint/storage/gdrivev3/DriveWriter;->createDriveWriter(Landroid/content/Context;)Lcom/epson/iprint/storage/gdrivev3/DriveWriter;

    move-result-object v1

    :try_start_0
    const-string v2, "Epson iPrint"

    .line 148
    invoke-virtual {v1, v2}, Lcom/epson/iprint/storage/gdrivev3/DriveWriter;->findOrCreateFolder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 150
    iget-object v3, p1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;->originalLocalPath:Ljava/lang/String;

    iget-object v4, p1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;->uploadFilename:Ljava/lang/String;

    iget-object v5, p1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;->fileType:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    sget-object v6, Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;->PDF:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    if-ne v5, v6, :cond_1

    const-string v5, "application/pdf"

    goto :goto_0

    :cond_1
    const-string v5, "image/jpeg"

    :goto_0
    invoke-virtual {v1, v3, v4, v5, v2}, Lcom/epson/iprint/storage/gdrivev3/DriveWriter;->uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-direct {p0, p1, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->notifyUploadEnd(Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-object v0

    .line 155
    :catch_0
    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-direct {p0, p1, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->notifyUploadEnd(Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-object v0

    .line 141
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->mUploadNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {p1, v0, v0, v1}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-object v0
.end method

.class Lcom/epson/iprint/storage/gdrivev3/Injection;
.super Ljava/lang/Object;
.source "Injection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/gdrivev3/Injection$DownloadPresenterFactory;
    }
.end annotation


# static fields
.field private static volatile sDownloadPresenterFactory:Lcom/epson/iprint/storage/gdrivev3/Injection$DownloadPresenterFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideDownloadPresenter(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;)Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;
    .locals 7
    .param p0    # Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 24
    new-instance v6, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;

    new-instance v1, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-direct {v1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;-><init>()V

    new-instance v2, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

    invoke-direct {v2}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;-><init>()V

    new-instance v3, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;

    invoke-direct {v3}, Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;-><init>()V

    new-instance v4, Lcom/epson/iprint/storage/Network;

    invoke-direct {v4}, Lcom/epson/iprint/storage/Network;-><init>()V

    move-object v0, v6

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;-><init>(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;Lcom/epson/iprint/storage/gdrivev3/DownloaderWrapper;Lcom/epson/iprint/storage/Network;Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadContract$View;)V

    return-object v6
.end method

.method static setDownloadPresenterFactory(Lcom/epson/iprint/storage/gdrivev3/Injection$DownloadPresenterFactory;)V
    .locals 0
    .param p0    # Lcom/epson/iprint/storage/gdrivev3/Injection$DownloadPresenterFactory;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.class public Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;
.super Ljava/lang/Object;
.source "IprintGoogleSignIn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;,
        Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;,
        Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Intent;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V
    .locals 0

    .line 21
    invoke-static {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->handleLogin(Landroid/content/Intent;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V

    return-void
.end method

.method private addTargetScope(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)V
    .locals 4
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 84
    invoke-static {p2}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getTargetScope(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Lcom/google/android/gms/common/api/Scope;

    move-result-object p2

    .line 85
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getSignInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v0

    .line 87
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getSingInAddScopeRequestCode()I

    move-result p1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/common/api/Scope;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, p1, v0, v2}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->requestPermissions(Landroid/app/Activity;ILcom/google/android/gms/auth/api/signin/GoogleSignInAccount;[Lcom/google/android/gms/common/api/Scope;)V

    return-void
.end method

.method private static getGoogleSignInClient(Landroid/app/Activity;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;
    .locals 2
    .param p0    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 100
    invoke-static {p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getTargetScope(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Lcom/google/android/gms/common/api/Scope;

    move-result-object p1

    .line 102
    new-instance v0, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;

    sget-object v1, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;->DEFAULT_SIGN_IN:Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;-><init>(Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)V

    .line 104
    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;->requestEmail()Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/gms/common/api/Scope;

    .line 105
    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;->requestScopes(Lcom/google/android/gms/common/api/Scope;[Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;->build()Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    move-result-object p1

    .line 107
    invoke-static {p0, p1}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getClient(Landroid/app/Activity;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    move-result-object p0

    return-object p0
.end method

.method private static getSignInIntent(Landroid/app/Activity;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Landroid/content/Intent;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 93
    invoke-static {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getGoogleSignInClient(Landroid/app/Activity;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    move-result-object p0

    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;->getSignInIntent()Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method private static getTargetScope(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Lcom/google/android/gms/common/api/Scope;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 113
    sget-object v0, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;->DOWNLOAD:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;

    if-ne p0, v0, :cond_0

    .line 114
    new-instance p0, Lcom/google/android/gms/common/api/Scope;

    invoke-static {}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->getTargetScope()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_0
    new-instance p0, Lcom/google/android/gms/common/api/Scope;

    invoke-static {}, Lcom/epson/iprint/storage/gdrivev3/DriveWriter;->getWriteScope()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method private static handleLogin(Landroid/content/Intent;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V
    .locals 1

    .line 43
    invoke-static {p0}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getSignedInAccountFromIntent(Landroid/content/Intent;)Lcom/google/android/gms/tasks/Task;

    move-result-object p0

    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$2;

    invoke-direct {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$2;-><init>(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V

    .line 44
    invoke-virtual {p0, v0}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    move-result-object p0

    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$1;

    invoke-direct {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$1;-><init>(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V

    .line 49
    invoke-virtual {p0, v0}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method


# virtual methods
.method public disconnectAccount(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V
    .locals 2
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 124
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;->DOWNLOAD:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getGoogleSignInClient(Landroid/app/Activity;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;->revokeAccess()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object p1

    new-instance v1, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$4;

    invoke-direct {v1, p0, p2}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$4;-><init>(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Landroid/app/Activity;Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public getSignInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 157
    invoke-static {p1}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getLastSignedInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object p1

    return-object p1
.end method

.method public isSignInValid(Landroid/content/Context;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Z
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 140
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getSignInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 145
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->getGrantedScopes()Ljava/util/Set;

    move-result-object p1

    .line 146
    invoke-static {p2}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getTargetScope(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Lcom/google/android/gms/common/api/Scope;

    move-result-object p2

    .line 147
    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public startSignIn(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)V
    .locals 1
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 64
    invoke-interface {p2}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getSignInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    invoke-direct {p0, p2, p3}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->addTargetScope(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)V

    return-void

    .line 70
    :cond_0
    invoke-interface {p2}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->getSignInIntent(Landroid/app/Activity;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)Landroid/content/Intent;

    move-result-object p3

    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$3;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$3;-><init>(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V

    invoke-interface {p2, p3, v0}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->requestSignIn(Landroid/content/Intent;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;)V

    return-void
.end method

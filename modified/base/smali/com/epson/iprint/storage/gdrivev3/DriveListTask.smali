.class public Lcom/epson/iprint/storage/gdrivev3/DriveListTask;
.super Landroid/os/AsyncTask;
.source "DriveListTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList<",
        "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mException:Ljava/lang/Exception;

.field private final mListTaskCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;

.field private final mMyDrive:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;)V
    .locals 0
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 24
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->mMyDrive:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    .line 26
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->mListTaskCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;

    return-void
.end method

.method private filterFiles(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 53
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    return-object p1

    .line 56
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 57
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    .line 58
    invoke-interface {v1}, Lcom/epson/iprint/storage/gdrivev3/OnlineFile;->isDisplayFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 59
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, [Lcom/epson/iprint/storage/gdrivev3/OnlineFile;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->doInBackground([Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    .line 33
    aget-object p1, p1, v1

    goto :goto_0

    :cond_0
    move-object p1, v0

    .line 36
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->mMyDrive:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;

    invoke-virtual {v1, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloader;->listFile(Lcom/epson/iprint/storage/gdrivev3/OnlineFile;)Ljava/util/ArrayList;

    move-result-object p1

    .line 37
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->filterFiles(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 39
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 40
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->mException:Ljava/lang/Exception;

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/epson/iprint/storage/gdrivev3/OnlineFile;",
            ">;)V"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/DriveListTask;->mListTaskCompleteListener:Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;

    invoke-interface {v0, p1}, Lcom/epson/iprint/storage/gdrivev3/DriveListTask$ListTaskCompleteListener;->listComplete(Ljava/util/ArrayList;)V

    return-void
.end method

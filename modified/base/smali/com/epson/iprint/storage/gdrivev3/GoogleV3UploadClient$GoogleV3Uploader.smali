.class Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;
.super Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
.source "GoogleV3UploadClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GoogleV3Uploader"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mUploadInfo:Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;

.field final synthetic this$0:Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;


# direct methods
.method public constructor <init>(Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;Landroid/content/Context;Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;)V
    .locals 0
    .param p2    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 112
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;->this$0:Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    .line 113
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;->mContext:Landroid/content/Context;

    .line 114
    iput-object p3, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;->mUploadInfo:Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;

    return-void
.end method


# virtual methods
.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V
    .locals 4

    .line 125
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;

    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;-><init>(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V

    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;

    iget-object v2, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$GoogleV3Uploader;->mUploadInfo:Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, p1, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

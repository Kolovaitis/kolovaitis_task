.class public Lcom/epson/iprint/storage/gdrivev3/GoogleDriveMimeType;
.super Ljava/lang/Object;
.source "GoogleDriveMimeType.java"


# static fields
.field public static final MIME_TYPE_GOOGLE_DOC:Ljava/lang/String; = "application/vnd.google-apps.document"

.field public static final MIME_TYPE_GOOGLE_DRAWING:Ljava/lang/String; = "application/vnd.google-apps.drawing"

.field public static final MIME_TYPE_GOOGLE_FOLDER:Ljava/lang/String; = "application/vnd.google-apps.folder"

.field public static final MIME_TYPE_GOOGLE_SHEETS:Ljava/lang/String; = "application/vnd.google-apps.spreadsheet"

.field public static final MIME_TYPE_GOOGLE_SLIDES:Ljava/lang/String; = "application/vnd.google-apps.presentation"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

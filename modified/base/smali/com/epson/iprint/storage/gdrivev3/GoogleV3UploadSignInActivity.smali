.class public Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "GoogleV3UploadSignInActivity.java"

# interfaces
.implements Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
.implements Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;
.implements Lcom/epson/iprint/storage/OneButtonDialogFragment$DialogCallback;
.implements Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;


# static fields
.field private static final DIALOG_TAG_OFFLINE_ERROR:Ljava/lang/String; = "offline_error"

.field private static final DIALOG_TAG_SIGN_IN_FAILED:Ljava/lang/String; = "sign-in_failed"

.field private static final PARAM_KEY_CHECK_SERVICE_ONLY:Ljava/lang/String; = "check-service_only"

.field private static final PARAM_KEY_FOR_DISCONNECT:Ljava/lang/String; = "for-disconnect"

.field private static final REQUEST_CODE_CHECK_PLAY_SERVICE:I = 0xb

.field private static final REQUEST_CODE_SIGN_IN:I = 0xa

.field private static final REQUEST_CODE_SIGN_IN_ADD_SCOPE:I = 0xc


# instance fields
.field private mActivityForeground:Z

.field private mCheckServiceOnly:Z

.field private mForegroundRunnableList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

.field private mMisc:Lcom/epson/iprint/storage/Network;

.field private mPlayServiceDialogManager:Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

.field private mSignInStartActivityCallback:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;

.field private mStartForDisconnect:Z

.field private mWaitSingInProcess:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;ILjava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->showErrorDialog(ILjava/lang/String;)V

    return-void
.end method

.method private finishWithResult(Z)V
    .locals 0

    .line 278
    invoke-static {p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;->setSingInValue(Z)V

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 279
    :goto_0
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->setResult(I)V

    .line 280
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->finish()V

    return-void
.end method

.method public static getDisconnectIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "for-disconnect"

    const/4 v1, 0x1

    .line 73
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method public static getPlayServiceCheckIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 86
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "check-service_only"

    const/4 v1, 0x1

    .line 87
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method private processForegroundList()V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 165
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 167
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 168
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private showErrorDialog(ILjava/lang/String;)V
    .locals 2

    .line 109
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mActivityForeground:Z

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    new-instance v1, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity$1;-><init>(Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    return-void

    .line 119
    :cond_0
    invoke-static {p1, p2}, Lcom/epson/iprint/storage/OneButtonDialogFragment;->newInstance(ILjava/lang/String;)Lcom/epson/iprint/storage/OneButtonDialogFragment;

    move-result-object p1

    .line 120
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/epson/iprint/storage/OneButtonDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public buttonPressed(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, -0x1

    .line 222
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x28c3f354

    const/4 v3, 0x0

    if-eq v1, v2, :cond_2

    const v2, 0x15dfd887

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "sign-in_failed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v1, "offline_error"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :cond_3
    :goto_0
    packed-switch v0, :pswitch_data_0

    return-void

    .line 229
    :pswitch_0
    new-instance p1, Landroid/content/Intent;

    const-string v0, "android.settings.WIRELESS_SETTINGS"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->startActivity(Landroid/content/Intent;)V

    .line 230
    invoke-direct {p0, v3}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->finishWithResult(Z)V

    return-void

    .line 224
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->finishWithResult(Z)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    return-object p0
.end method

.method public getPlayServiceRequestCode()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method

.method public getSingInAddScopeRequestCode()I
    .locals 1

    const/16 v0, 0xc

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    const/4 p1, 0x1

    .line 254
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->finishWithResult(Z)V

    return-void

    :cond_0
    const p1, 0x7f0e02ad

    const-string p2, "sign-in_failed"

    .line 257
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->showErrorDialog(ILjava/lang/String;)V

    goto :goto_0

    :pswitch_1
    return-void

    .line 244
    :pswitch_2
    iget-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mSignInStartActivityCallback:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;

    if-eqz p1, :cond_1

    .line 245
    invoke-interface {p1, p2, p3}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;->onActivityResult(ILandroid/content/Intent;)V

    const/4 p1, 0x0

    .line 246
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mSignInStartActivityCallback:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;

    :cond_1
    return-void

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 94
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    new-instance p1, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

    invoke-direct {p1}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;-><init>()V

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mPlayServiceDialogManager:Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

    .line 97
    new-instance p1, Lcom/epson/iprint/storage/Network;

    invoke-direct {p1}, Lcom/epson/iprint/storage/Network;-><init>()V

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mMisc:Lcom/epson/iprint/storage/Network;

    .line 98
    new-instance p1, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-direct {p1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;-><init>()V

    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    const/4 p1, 0x0

    .line 99
    iput-boolean p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mWaitSingInProcess:Z

    .line 100
    iput-boolean p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mActivityForeground:Z

    .line 101
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mForegroundRunnableList:Ljava/util/LinkedList;

    .line 103
    invoke-virtual {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "for-disconnect"

    .line 104
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mStartForDisconnect:Z

    const-string v1, "check-service_only"

    .line 105
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mCheckServiceOnly:Z

    return-void
.end method

.method public onDisconnectCompleted()V
    .locals 1

    const/4 v0, 0x1

    .line 274
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->finishWithResult(Z)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 174
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 175
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mPlayServiceDialogManager:Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->onActivityPaused()V

    const/4 v0, 0x0

    .line 176
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mActivityForeground:Z

    return-void
.end method

.method protected onPostResume()V
    .locals 2

    .line 126
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPostResume()V

    .line 128
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mMisc:Lcom/epson/iprint/storage/Network;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/epson/iprint/storage/Network;->selectSimpleAp(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Z)V

    const/4 v0, 0x1

    .line 130
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mActivityForeground:Z

    .line 131
    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->processForegroundList()V

    .line 133
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mMisc:Lcom/epson/iprint/storage/Network;

    invoke-virtual {v1, p0}, Lcom/epson/iprint/storage/Network;->isOnline(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;)Z

    move-result v1

    if-nez v1, :cond_0

    const v0, 0x7f0e03d8

    const-string v1, "offline_error"

    .line 134
    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->showErrorDialog(ILjava/lang/String;)V

    return-void

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mPlayServiceDialogManager:Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;

    invoke-virtual {v1, p0, p0}, Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager;->checkPlayService(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/PlayServiceDialogManager$SingInCancelNotifier;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    .line 144
    :cond_1
    iget-boolean v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mCheckServiceOnly:Z

    if-eqz v1, :cond_2

    .line 145
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->finishWithResult(Z)V

    return-void

    .line 149
    :cond_2
    iget-boolean v1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mWaitSingInProcess:Z

    if-eqz v1, :cond_3

    return-void

    .line 155
    :cond_3
    iput-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mWaitSingInProcess:Z

    .line 156
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mStartForDisconnect:Z

    if-eqz v0, :cond_4

    .line 157
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    invoke-virtual {v0, p0, p0}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->disconnectAccount(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;)V

    goto :goto_0

    .line 159
    :cond_4
    iget-object v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mIprintGoogleSignIn:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;

    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;->UPLOAD:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;

    invoke-virtual {v0, p0, p0, v1}, Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn;->startSignIn(Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$SignInListener;Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$OperationType;)V

    :goto_0
    return-void
.end method

.method public onSignInComplete(ZLcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 265
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->finishWithResult(Z)V

    return-void

    :cond_0
    const p1, 0x7f0e02ad

    const-string p2, "sign-in_failed"

    .line 269
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->showErrorDialog(ILjava/lang/String;)V

    return-void
.end method

.method public playServiceInstallCancel()V
    .locals 2

    .line 204
    iget-boolean v0, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mCheckServiceOnly:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 205
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->finishWithResult(Z)V

    return-void

    :cond_0
    const v0, 0x7f0e02ad

    const-string v1, "sign-in_failed"

    .line 209
    invoke-direct {p0, v0, v1}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->showErrorDialog(ILjava/lang/String;)V

    return-void
.end method

.method public requestSignIn(Landroid/content/Intent;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 197
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->mSignInStartActivityCallback:Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;

    const/16 p2, 0xa

    .line 198
    invoke-virtual {p0, p1, p2}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadSignInActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.class public interface abstract Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
.super Ljava/lang/Object;
.source "ActivityWrapper.java"


# virtual methods
.method public abstract getActivity()Landroid/app/Activity;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getPlayServiceRequestCode()I
.end method

.method public abstract getSingInAddScopeRequestCode()I
.end method

.method public abstract requestSignIn(Landroid/content/Intent;Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;)V
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/storage/gdrivev3/IprintGoogleSignIn$StartActivityResultCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.class public Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;
.super Ljava/lang/Object;
.source "GoogleV3UploadClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UploadInfo"
.end annotation


# instance fields
.field fileType:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

.field originalLocalPath:Ljava/lang/String;

.field uploadFilename:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iput-object p1, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;->fileType:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    .line 180
    iput-object p2, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;->originalLocalPath:Ljava/lang/String;

    .line 181
    iput-object p3, p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient$UploadInfo;->uploadFilename:Ljava/lang/String;

    return-void
.end method

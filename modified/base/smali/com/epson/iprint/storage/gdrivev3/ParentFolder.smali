.class public Lcom/epson/iprint/storage/gdrivev3/ParentFolder;
.super Ljava/lang/Object;
.source "ParentFolder.java"

# interfaces
.implements Lcom/epson/iprint/storage/gdrivev3/OnlineFile;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    const-string v0, ".."

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    const-string v0, "application/vnd.google-apps.folder"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, ".."

    return-object v0
.end method

.method public isDisplayFile()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isFolder()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.class final enum Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;
.super Ljava/lang/Enum;
.source "GoogleDownloadPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SignInState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

.field public static final enum SIGN_IN:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

.field public static final enum SIGN_OUT:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 24
    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    const-string v1, "SIGN_IN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_IN:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    new-instance v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    const-string v1, "SIGN_OUT"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_OUT:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    const/4 v0, 0x2

    .line 23
    new-array v0, v0, [Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_IN:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->SIGN_OUT:Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->$VALUES:[Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;
    .locals 1

    .line 23
    const-class v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    return-object p0
.end method

.method public static values()[Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;
    .locals 1

    .line 23
    sget-object v0, Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->$VALUES:[Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    invoke-virtual {v0}, [Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/iprint/storage/gdrivev3/GoogleDownloadPresenter$SignInState;

    return-object v0
.end method

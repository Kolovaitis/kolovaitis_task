.class public Lcom/epson/iprint/storage/Network;
.super Ljava/lang/Object;
.source "Network.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isOnline(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;)Z
    .locals 0
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 16
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isConnecting(Landroid/content/Context;)Z

    move-result p1

    return p1
.end method

.method public selectSimpleAp(Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;Z)V
    .locals 0
    .param p1    # Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 23
    invoke-interface {p1}, Lcom/epson/iprint/storage/gdrivev3/ActivityWrapper;->getActivity()Landroid/app/Activity;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    return-void
.end method

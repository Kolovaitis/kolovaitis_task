.class Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;
.super Landroid/os/AsyncTask;
.source "StorageProcessUploadActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageProcessUploadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UploadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field canceled:Z

.field fileType:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

.field localPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile taskError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

.field final synthetic this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

.field uploadFilename:Ljava/lang/String;

.field uploaded:Z

.field uploader:Lcom/epson/iprint/storage/StorageServiceClient$Uploader;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity;)V
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->taskError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    return-object p0
.end method

.method static synthetic access$002(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->taskError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    return-object p1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 188
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 14

    .line 204
    new-instance p1, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-direct {p1, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;-><init>(Lcom/epson/iprint/storage/StorageActivity;)V

    .line 205
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    const v1, 0x7f0e0541

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$1;

    invoke-direct {v2, p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$1;-><init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;)V

    invoke-virtual {p1, v0, v2}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->show(Ljava/lang/String;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V

    const-string v0, ""

    .line 222
    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;->PDF:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    iget-object v3, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->fileType:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    invoke-virtual {v2, v3}, Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->localPaths:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploadFilename:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".pdf"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 231
    new-instance v2, Lepson/scan/lib/libHaru;

    invoke-direct {v2}, Lepson/scan/lib/libHaru;-><init>()V

    .line 232
    iget-object v4, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->localPaths:Ljava/util/List;

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v0}, Lepson/scan/lib/libHaru;->createPDF(Ljava/util/ArrayList;Ljava/lang/String;)I

    .line 234
    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->localPaths:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 235
    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->localPaths:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, ".pdf"

    goto :goto_0

    .line 239
    :cond_0
    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;->JPEG:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    iget-object v4, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->fileType:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    invoke-virtual {v2, v4}, Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, ".jpg"

    .line 249
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploadFilename:Ljava/lang/String;

    iget-object v4, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {v4, v3}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->generateUploadFilename(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x1

    xor-int/2addr v2, v4

    .line 255
    iget-object v5, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->localPaths:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v5, :cond_8

    .line 257
    iget-object v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->localPaths:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 258
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploadFilename:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    if-le v5, v4, :cond_3

    const/4 v8, 0x2

    if-eqz v2, :cond_2

    const-string v9, "%s%03d%s"

    const/4 v10, 0x3

    .line 265
    new-array v10, v10, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploadFilename:Ljava/lang/String;

    aput-object v11, v10, v3

    add-int/lit8 v11, v6, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v4

    aput-object v0, v10, v8

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_2
    const-string v9, "%s%s"

    .line 267
    new-array v8, v8, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {v10, v6}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->generateUploadFilename(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v3

    aput-object v0, v8, v4

    invoke-static {v9, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 269
    :goto_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {v10, v1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v10, v6, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->changeMessage(Ljava/lang/String;)V

    .line 276
    :cond_3
    new-instance v9, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v9}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 277
    invoke-interface {v9}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v10

    .line 282
    :try_start_0
    invoke-interface {v9}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 283
    iput-boolean v3, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploaded:Z

    .line 284
    sget-object v11, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    iput-object v11, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->taskError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    .line 289
    iget-object v11, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    invoke-virtual {v11}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object v11

    iget-object v12, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    iget-object v13, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->fileType:Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;

    invoke-virtual {v11, v12, v13, v7, v8}, Lcom/epson/iprint/storage/StorageServiceClient;->getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;

    move-result-object v7

    iput-object v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploader:Lcom/epson/iprint/storage/StorageServiceClient$Uploader;

    .line 290
    iget-object v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploader:Lcom/epson/iprint/storage/StorageServiceClient$Uploader;

    new-instance v8, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;

    invoke-direct {v8, p0, v9, v10}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$2;-><init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;Ljava/util/concurrent/locks/Lock;Ljava/util/concurrent/locks/Condition;)V

    invoke-virtual {v7, v8}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;->start(Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V

    .line 312
    :goto_3
    iget-boolean v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->uploaded:Z

    if-nez v7, :cond_4

    .line 313
    invoke-interface {v10}, Ljava/util/concurrent/locks/Condition;->await()V

    goto :goto_3

    .line 321
    :cond_4
    iget-boolean v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->canceled:Z

    if-nez v7, :cond_5

    iget-object v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->taskError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    sget-object v8, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-eq v7, v8, :cond_7

    .line 322
    :cond_5
    iget-boolean v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->canceled:Z

    if-eqz v7, :cond_6

    .line 323
    sget-object v7, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    iput-object v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->taskError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    :cond_6
    invoke-interface {v9}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_5

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception v7

    .line 328
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 329
    sget-object v7, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    iput-object v7, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->taskError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332
    :cond_7
    invoke-interface {v9}, Ljava/util/concurrent/locks/Lock;->unlock()V

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    :goto_4
    invoke-interface {v9}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1

    .line 342
    :cond_8
    :goto_5
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;->this$0:Lcom/epson/iprint/storage/StorageProcessUploadActivity;

    new-instance v1, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;

    invoke-direct {v1, p0, p1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask$3;-><init>(Lcom/epson/iprint/storage/StorageProcessUploadActivity$UploadTask;Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;)V

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    const/4 p1, 0x0

    return-object p1
.end method

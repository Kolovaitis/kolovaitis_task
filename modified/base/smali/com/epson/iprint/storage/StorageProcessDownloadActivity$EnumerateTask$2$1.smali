.class Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;
.super Ljava/lang/Object;
.source "StorageProcessDownloadActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

.field final synthetic val$error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V
    .locals 0

    .line 487
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->val$error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 489
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->val$error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->enumeratedItems:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 490
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    iget-object v1, v1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v1, v1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    iget-object v2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    iget-object v2, v2, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v2, v2, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->enumeratedItems:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Ljava/util/List;)V

    .line 492
    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    iget-object v1, v1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v1, v1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$800(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->val$error:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 496
    :cond_1
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const v1, 0x7f0e053b

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->showErrorDialog(I)V

    .line 503
    :goto_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$100(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Z)V

    .line 504
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2$1;->this$2:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;->this$1:Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->dismiss()V

    return-void
.end method

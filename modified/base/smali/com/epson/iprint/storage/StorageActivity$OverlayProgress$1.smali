.class Lcom/epson/iprint/storage/StorageActivity$OverlayProgress$1;
.super Landroid/content/BroadcastReceiver;
.source "StorageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->registerCancelRequestReceiver(Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

.field final synthetic val$callback:Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress$1;->this$1:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    iput-object p2, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress$1;->val$callback:Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .line 200
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    .line 201
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_REQUEST_CODE:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 202
    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->CANCEL_REQUEST_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress$1;->this$1:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    iget p1, p1, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->mRequestCode:I

    if-ne p1, v0, :cond_0

    .line 203
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress$1;->val$callback:Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;

    invoke-interface {p1, p2}, Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;->onCancelRequest(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

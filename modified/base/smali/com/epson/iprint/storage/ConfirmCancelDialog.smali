.class public Lcom/epson/iprint/storage/ConfirmCancelDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ConfirmCancelDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/ConfirmCancelDialog$DialogCancelListener;
    }
.end annotation


# static fields
.field private static final PARAM_MESSAGE_RESOURCE_ID:Ljava/lang/String; = "message_id"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/ConfirmCancelDialog;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->doCancel()V

    return-void
.end method

.method private doCancel()V
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 66
    :cond_0
    check-cast v0, Lcom/epson/iprint/storage/ConfirmCancelDialog$DialogCancelListener;

    .line 67
    invoke-interface {v0}, Lcom/epson/iprint/storage/ConfirmCancelDialog$DialogCancelListener;->cancel()V

    return-void
.end method

.method public static newInstance(I)Lcom/epson/iprint/storage/ConfirmCancelDialog;
    .locals 3

    .line 22
    new-instance v0, Lcom/epson/iprint/storage/ConfirmCancelDialog;

    invoke-direct {v0}, Lcom/epson/iprint/storage/ConfirmCancelDialog;-><init>()V

    .line 24
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message_id"

    .line 25
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 26
    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 35
    invoke-virtual {p0}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "message_id"

    .line 36
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x0

    .line 38
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->setCancelable(Z)V

    .line 40
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e052b

    .line 42
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/iprint/storage/ConfirmCancelDialog$2;

    invoke-direct {v1, p0}, Lcom/epson/iprint/storage/ConfirmCancelDialog$2;-><init>(Lcom/epson/iprint/storage/ConfirmCancelDialog;)V

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04e6

    .line 48
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/ConfirmCancelDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/iprint/storage/ConfirmCancelDialog$1;

    invoke-direct {v1, p0}, Lcom/epson/iprint/storage/ConfirmCancelDialog$1;-><init>(Lcom/epson/iprint/storage/ConfirmCancelDialog;)V

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

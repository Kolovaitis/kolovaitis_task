.class public Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;
.super Ljava/lang/Object;
.source "StorageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OverlayProgress"
.end annotation


# instance fields
.field mCancelReceiver:Landroid/content/BroadcastReceiver;

.field mRequestCode:I

.field final synthetic this$0:Lcom/epson/iprint/storage/StorageActivity;


# direct methods
.method public constructor <init>(Lcom/epson/iprint/storage/StorageActivity;)V
    .locals 1

    .line 81
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance p1, Ljava/util/Random;

    invoke-direct {p1}, Ljava/util/Random;-><init>()V

    const v0, 0x7fffffff

    invoke-virtual {p1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result p1

    iput p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->mRequestCode:I

    const/4 p1, 0x0

    .line 83
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->setCancelReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method


# virtual methods
.method changeMessage(Ljava/lang/String;)V
    .locals 3

    .line 153
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->CHANGE_ACTION:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 154
    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_REQUEST_CODE:Ljava/lang/String;

    iget v2, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->mRequestCode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 155
    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_MESSAGE:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public dismiss()V
    .locals 3

    .line 176
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->DISMISS_ACTION:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 177
    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_REQUEST_CODE:Ljava/lang/String;

    iget v2, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->mRequestCode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 178
    iget-object v1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    invoke-virtual {v1, v0}, Lcom/epson/iprint/storage/StorageActivity;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    .line 179
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->setCancelReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method registerCancelCallback(Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V
    .locals 3

    .line 164
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->REGISTER_ACTION:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 165
    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_REQUEST_CODE:Ljava/lang/String;

    iget v2, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->mRequestCode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 166
    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_CANCEL_ENABLED:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 167
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->registerCancelRequestReceiver(Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V

    .line 168
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method registerCancelRequestReceiver(Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V
    .locals 3

    .line 198
    new-instance v0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress$1;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress$1;-><init>(Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V

    .line 207
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/epson/iprint/storage/StorageWaitingActivity;->CANCEL_REQUEST_ACTION:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Lcom/epson/iprint/storage/StorageActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 208
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->setCancelReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method setCancelReceiver(Landroid/content/BroadcastReceiver;)V
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->mCancelReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    invoke-virtual {v1, v0}, Lcom/epson/iprint/storage/StorageActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 190
    :cond_0
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->mCancelReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public show()V
    .locals 1

    const/4 v0, 0x0

    .line 90
    invoke-virtual {p0, v0, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->show(Ljava/lang/String;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V

    return-void
.end method

.method show(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 98
    invoke-virtual {p0, p1, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->show(Ljava/lang/String;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V

    return-void
.end method

.method show(Ljava/lang/String;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V
    .locals 1

    const/4 v0, 0x1

    .line 107
    invoke-virtual {p0, p1, p2, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->show(Ljava/lang/String;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;Z)V

    return-void
.end method

.method show(Ljava/lang/String;Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;Z)V
    .locals 3

    .line 110
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    const-class v2, Lcom/epson/iprint/storage/StorageWaitingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 111
    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_REQUEST_CODE:Ljava/lang/String;

    iget v2, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->mRequestCode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz p1, :cond_0

    .line 113
    sget-object v1, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_MESSAGE:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    if-eqz p2, :cond_1

    .line 116
    sget-object p1, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_CANCEL_ENABLED:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 117
    invoke-virtual {p0, p2}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->registerCancelRequestReceiver(Lcom/epson/iprint/storage/StorageActivity$CancelRequestCallback;)V

    .line 124
    :cond_1
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->this$0:Lcom/epson/iprint/storage/StorageActivity;

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    if-eqz p3, :cond_2

    .line 128
    sget-boolean p1, Lcom/epson/iprint/storage/StorageWaitingActivity;->bShowing:Z

    if-nez p1, :cond_2

    const-wide/16 p1, 0xc8

    .line 130
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 133
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_2
    return-void
.end method

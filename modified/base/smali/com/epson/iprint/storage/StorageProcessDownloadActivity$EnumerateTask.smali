.class Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "StorageProcessDownloadActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageProcessDownloadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EnumerateTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field enumeratedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;"
        }
    .end annotation
.end field

.field item:Lcom/epson/iprint/storage/StorageItem;

.field final onEnumerated:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

.field onLoadedMore:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

.field progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

.field final synthetic this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Lcom/epson/iprint/storage/StorageItem;)V
    .locals 1

    .line 545
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    .line 408
    new-instance p1, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-direct {p1, v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;-><init>(Lcom/epson/iprint/storage/StorageActivity;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    .line 460
    new-instance p1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;

    invoke-direct {p1, p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$2;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->onEnumerated:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    .line 513
    new-instance p1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;

    invoke-direct {p1, p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$3;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->onLoadedMore:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    .line 546
    iput-object p2, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->item:Lcom/epson/iprint/storage/StorageItem;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 407
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .line 425
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageServiceClient;->isNeedSignin()Z

    move-result p1

    if-nez p1, :cond_0

    .line 427
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->progress:Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageActivity$OverlayProgress;->show()V

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->item:Lcom/epson/iprint/storage/StorageItem;

    iget-object v0, v0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->LOADMORE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    if-ne v0, v1, :cond_1

    .line 434
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$600(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->item:Lcom/epson/iprint/storage/StorageItem;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->onLoadedMore:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    invoke-virtual {p1, v0, v1}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;->enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    .line 441
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$600(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->item:Lcom/epson/iprint/storage/StorageItem;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->onEnumerated:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    invoke-virtual {p1, v0, v1}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;->enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    goto :goto_0

    .line 445
    :cond_2
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-static {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$600(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->item:Lcom/epson/iprint/storage/StorageItem;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->onEnumerated:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    new-instance v2, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$1;

    invoke-direct {v2, p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask$1;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;->enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;)V

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected onPreExecute()V
    .locals 2

    .line 413
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->this$0:Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->access$100(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Z)V

    return-void
.end method

.class public abstract Lcom/epson/iprint/storage/StorageServiceClient;
.super Ljava/lang/Object;
.source "StorageServiceClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;,
        Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;,
        Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;,
        Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;,
        Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;,
        Lcom/epson/iprint/storage/StorageServiceClient$Downloader;,
        Lcom/epson/iprint/storage/StorageServiceClient$Uploader;,
        Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;,
        Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    }
.end annotation


# static fields
.field public static final EPSON_IPRINT_FOLDER:Ljava/lang/String; = "Epson iPrint"

.field public static STORAGE_BOX:Ljava/lang/String; = "box"

.field public static final STORAGE_DROPBOX:Ljava/lang/String; = "dropbox"

.field public static STORAGE_EVERNOTE:Ljava/lang/String; = "evernote"

.field public static STORAGE_GOOGLEDRIVE:Ljava/lang/String; = "googledrive"

.field public static STORAGE_ONEDRIVE:Ljava/lang/String; = "onedrive"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getClient(Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient;
    .locals 1

    .line 70
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_GOOGLEDRIVE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    new-instance p0, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;

    invoke-direct {p0}, Lcom/epson/iprint/storage/gdrivev3/GoogleV3UploadClient;-><init>()V

    goto :goto_0

    :cond_0
    const-string v0, "dropbox"

    .line 72
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    new-instance p0, Lcom/epson/iprint/storage/dropbox/DropboxV2Client;

    invoke-direct {p0}, Lcom/epson/iprint/storage/dropbox/DropboxV2Client;-><init>()V

    goto :goto_0

    .line 74
    :cond_1
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_BOX:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    invoke-static {}, Lcom/epson/iprint/storage/box/BoxNetClient;->getInstance()Lcom/epson/iprint/storage/box/BoxNetClient;

    move-result-object p0

    goto :goto_0

    .line 76
    :cond_2
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_EVERNOTE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    new-instance p0, Lcom/epson/iprint/storage/evernote/EvernoteClient;

    invoke-direct {p0}, Lcom/epson/iprint/storage/evernote/EvernoteClient;-><init>()V

    goto :goto_0

    .line 78
    :cond_3
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_ONEDRIVE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 79
    new-instance p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-direct {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;-><init>()V

    goto :goto_0

    :cond_4
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static isPrintableFilename(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 299
    :cond_0
    invoke-static {p0}, Lepson/common/Utils;->isPhotoFile(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    return v2

    :cond_1
    const-string v1, ".pdf"

    .line 303
    invoke-static {p0, v1}, Lcom/epson/iprint/storage/StorageItem;->endsWith(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    return v2

    .line 307
    :cond_2
    invoke-static {p0}, Lepson/common/Utils;->isGConvertFile(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    return v2

    :cond_3
    return v0
.end method


# virtual methods
.method protected getDownloadLocalPath(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;)Ljava/lang/String;
    .locals 0

    .line 322
    invoke-virtual {p2, p1}, Lcom/epson/iprint/storage/StorageItem;->getDownloadLocalPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public abstract getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
.end method

.method public abstract getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.end method

.method public abstract getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
.end method

.method protected isNeedSignin()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected isPrintableFileTypes(Lcom/epson/iprint/storage/StorageItem;)Z
    .locals 0

    .line 288
    iget-object p1, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    .line 290
    invoke-static {p1}, Lcom/epson/iprint/storage/StorageServiceClient;->isPrintableFilename(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public abstract isSignedIn(Landroid/content/Context;)Z
.end method

.method public abstract isSupportedUploadType(Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;)Z
.end method

.method protected isValidUploadName(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "UTF-8"

    .line 253
    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_2

    .line 254
    array-length v1, v1

    const/16 v2, 0x40

    if-le v1, v2, :cond_0

    goto :goto_0

    .line 259
    :cond_0
    invoke-static {p1}, Lepson/print/fileBrower;->isAvailableFileName(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_1

    return v0

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_0
    return v0

    :catch_0
    return v0
.end method

.method public abstract revokeSignedInData(Landroid/app/Activity;)Z
.end method

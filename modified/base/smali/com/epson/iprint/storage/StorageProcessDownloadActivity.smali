.class public Lcom/epson/iprint/storage/StorageProcessDownloadActivity;
.super Lcom/epson/iprint/storage/StorageProcessActivity;
.source "StorageProcessDownloadActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;,
        Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemClickListener;,
        Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;,
        Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;
    }
.end annotation


# static fields
.field public static final PARAM_KEY_ONLINE_STORAGE_PRINT_LOG:Ljava/lang/String; = "print_log"


# instance fields
.field private final DOWNLOADED_FILE_PATH:Ljava/lang/String;

.field private final DOWNLOAD_PREFERENCE:Ljava/lang/String;

.field private final ITEMNAME_PARENT:Ljava/lang/String;

.field private final REQUEST_CODE_DELETE_DOWNLOADED_FILE:I

.field private mEnumerator:Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

.field private mFolderStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDownloadInterruption:Z

.field private mIsDownloading:Z

.field private mListView:Landroid/widget/ListView;

.field private mPosition:I

.field private mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

.field task:Lepson/print/Util/AsyncTaskExecutor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepson/print/Util/AsyncTaskExecutor<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 46
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;-><init>()V

    const/4 v0, 0x0

    .line 57
    iput v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->REQUEST_CODE_DELETE_DOWNLOADED_FILE:I

    const-string v1, "StorageProcessDownloadActivity.Preference"

    .line 66
    iput-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->DOWNLOAD_PREFERENCE:Ljava/lang/String;

    const-string v1, "Downloaded.File.Path"

    .line 67
    iput-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->DOWNLOADED_FILE_PATH:Ljava/lang/String;

    const-string v1, ".."

    .line 70
    iput-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->ITEMNAME_PARENT:Ljava/lang/String;

    .line 73
    iput-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloading:Z

    .line 74
    iput-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloadInterruption:Z

    .line 75
    iput v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mPosition:I

    const/4 v0, 0x0

    .line 76
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->task:Lepson/print/Util/AsyncTaskExecutor;

    return-void
.end method

.method static synthetic access$100(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Z)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->taskInvoked(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;I)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->itemClick(I)V

    return-void
.end method

.method static synthetic access$202(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Z)Z
    .locals 0

    .line 46
    iput-boolean p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloading:Z

    return p1
.end method

.method static synthetic access$300(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    return-object p0
.end method

.method static synthetic access$400(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->saveDownloadedFilePath(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Z
    .locals 0

    .line 46
    iget-boolean p0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloadInterruption:Z

    return p0
.end method

.method static synthetic access$600(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getEnumerator()Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Ljava/util/Stack;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    return-object p0
.end method

.method static synthetic access$800(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;)Landroid/widget/ListView;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$902(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;I)I
    .locals 0

    .line 46
    iput p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mPosition:I

    return p1
.end method

.method private fetchDownloadedFilePath()Ljava/lang/String;
    .locals 3

    const-string v0, "StorageProcessDownloadActivity.Preference"

    const/4 v1, 0x0

    .line 814
    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "Downloaded.File.Path"

    const/4 v2, 0x0

    .line 815
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getEnumerator()Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
    .locals 1

    .line 823
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mEnumerator:Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    if-nez v0, :cond_0

    .line 824
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mEnumerator:Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    .line 826
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mEnumerator:Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    return-object v0
.end method

.method private static getOnlineStorageUiRouteValue(Ljava/lang/String;)I
    .locals 2

    const/4 v0, -0x1

    if-nez p0, :cond_0

    return v0

    .line 845
    :cond_0
    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_BOX:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 p0, 0x103

    return p0

    :cond_1
    const-string v1, "dropbox"

    .line 847
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 p0, 0x102

    return p0

    .line 849
    :cond_2
    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_EVERNOTE:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 p0, 0x100

    return p0

    .line 851
    :cond_3
    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_GOOGLEDRIVE:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 p0, 0x101

    return p0

    .line 853
    :cond_4
    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_ONEDRIVE:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    const/16 p0, 0x104

    return p0

    :cond_5
    return v0
.end method

.method public static getPreviewIntent(Ljava/lang/String;Landroid/content/Context;Lcom/epson/iprint/prtlogger/PrintLog;)Landroid/content/Intent;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/prtlogger/PrintLog;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 365
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "from"

    const/4 v2, 0x4

    .line 368
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, ".pdf"

    .line 370
    invoke-static {p0, v1}, Lcom/epson/iprint/storage/StorageItem;->endsWith(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, Lepson/common/Utils;->isGConvertFile(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 376
    :cond_0
    invoke-static {p0}, Lepson/common/Utils;->isPhotoFile(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const-string v1, "android.intent.action.SEND"

    .line 377
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    .line 378
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 379
    invoke-static {p0}, Lepson/common/Utils;->getMimeExt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "typeprint"

    .line 380
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 381
    const-class v1, Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "FROM_EPSON"

    .line 383
    invoke-virtual {v0, p1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 384
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/PrintLog;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, p2, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    const-string p0, "print_log"

    .line 385
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1

    .line 390
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 391
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "print_web"

    .line 392
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "typeprint"

    .line 393
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 394
    const-class v1, Lepson/print/ActivityPrintWeb;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 396
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/PrintLog;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, p2, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    const-string p0, "print_log"

    .line 397
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1

    :cond_2
    :goto_0
    const-string v1, "send document"

    .line 371
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    const-class v1, Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 374
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/PrintLog;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, p2, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    const-string p0, "print_log"

    .line 375
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :goto_1
    return-object v0
.end method

.method public static getStartIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 832
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 833
    new-instance p0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {p0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    .line 834
    invoke-static {p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getOnlineStorageUiRouteValue(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    const-string v1, "print_log"

    .line 835
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 837
    invoke-static {v0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->setCommonExtra(Landroid/content/Intent;Ljava/lang/String;)V

    return-object v0
.end method

.method private itemClick(I)V
    .locals 2

    const/4 v0, 0x1

    .line 565
    iput-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloading:Z

    .line 566
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    .line 567
    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->task:Lepson/print/Util/AsyncTaskExecutor;

    .line 568
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/storage/StorageItem;

    .line 573
    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    if-ne v0, v1, :cond_2

    .line 579
    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    const-string v1, ".."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 582
    :cond_0
    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/iprint/storage/StorageItem;

    iget-object v1, v1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 583
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 589
    :cond_1
    :goto_0
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Lcom/epson/iprint/storage/StorageItem;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->task:Lepson/print/Util/AsyncTaskExecutor;

    .line 591
    iget p1, p1, Lcom/epson/iprint/storage/StorageItem;->mLoggerInfo:I

    const/4 v0, 0x2

    goto :goto_1

    .line 606
    :cond_2
    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    sget-object v1, Lcom/epson/iprint/storage/StorageItem$ItemType;->LOADMORE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    if-ne v0, v1, :cond_3

    .line 607
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Lcom/epson/iprint/storage/StorageItem;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->task:Lepson/print/Util/AsyncTaskExecutor;

    goto :goto_1

    .line 609
    :cond_3
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$DownloadTask;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Lcom/epson/iprint/storage/StorageItem;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->task:Lepson/print/Util/AsyncTaskExecutor;

    .line 613
    :goto_1
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->task:Lepson/print/Util/AsyncTaskExecutor;

    if-eqz p1, :cond_5

    const/4 v0, 0x0

    .line 614
    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lepson/print/Util/AsyncTaskExecutor;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2

    .line 617
    :cond_4
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->showConnectionError()V

    :cond_5
    :goto_2
    return-void
.end method

.method private saveDownloadedFilePath(Ljava/lang/String;)V
    .locals 2

    const-string v0, "StorageProcessDownloadActivity.Preference"

    const/4 v1, 0x0

    .line 805
    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 806
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "Downloaded.File.Path"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private taskInvoked(Z)V
    .locals 1

    .line 788
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;

    if-eqz v0, :cond_0

    .line 790
    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->setTaskingStatus(Z)V

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    .line 797
    :cond_1
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->setSignInButtonEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    if-nez p1, :cond_1

    .line 242
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->fetchDownloadedFilePath()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 244
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 246
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_0
    const/4 p1, 0x0

    .line 249
    iput-boolean p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloadInterruption:Z

    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;

    if-eqz v0, :cond_3

    .line 147
    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemAdapter;->areAllItemsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 166
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 167
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/iprint/storage/StorageItem;

    invoke-direct {v0, p0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Lcom/epson/iprint/storage/StorageItem;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->showConnectionError()V

    goto :goto_0

    .line 173
    :cond_2
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->finish()V

    :goto_0
    return-void

    .line 155
    :cond_3
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 89
    invoke-super {p0, p1}, Lcom/epson/iprint/storage/StorageProcessActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0063

    .line 90
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->setContentView(I)V

    .line 91
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->setActionBar(Ljava/lang/String;Z)V

    .line 96
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/epson/iprint/storage/StorageServiceClient;->getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mEnumerator:Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    .line 101
    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    const p1, 0x102000a

    .line 107
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    .line 108
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    new-instance v0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemClickListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$StorageItemClickListener;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Lcom/epson/iprint/storage/StorageProcessDownloadActivity$1;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 109
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 112
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "print_log"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/prtlogger/PrintLog;

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :catch_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez p1, :cond_0

    .line 117
    new-instance p1, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {p1}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    :cond_0
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .line 209
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->onRestart()V

    .line 210
    iget-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloadInterruption:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloading:Z

    if-nez v0, :cond_0

    .line 212
    iget v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mPosition:I

    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->itemClick(I)V

    :cond_0
    const/4 v0, 0x0

    .line 214
    iput-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloadInterruption:Z

    return-void
.end method

.method onSignInStatus(Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;)V
    .locals 1

    .line 186
    sget-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;->SIGNED_IN:Lcom/epson/iprint/storage/StorageProcessActivity$SignInStatus;

    if-ne p1, v0, :cond_0

    .line 187
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 189
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 190
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 191
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mEnumerator:Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    if-eqz p1, :cond_1

    .line 192
    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;->cleanUpResources()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 199
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageProcessActivity;->onStop()V

    .line 201
    iget-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloading:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    .line 202
    iput-boolean v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mIsDownloadInterruption:Z

    .line 203
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->task:Lepson/print/Util/AsyncTaskExecutor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/print/Util/AsyncTaskExecutor;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method onUpdateProcessView()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 224
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getEnumerator()Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;->getRootItem()Lcom/epson/iprint/storage/StorageItem;

    move-result-object v0

    .line 226
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    .line 227
    iget-object v1, p0, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->mFolderStack:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    new-instance v1, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;

    invoke-direct {v1, p0, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;-><init>(Lcom/epson/iprint/storage/StorageProcessDownloadActivity;Lcom/epson/iprint/storage/StorageItem;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity$EnumerateTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 230
    :cond_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->showConnectionError()V

    :cond_1
    :goto_0
    return-void
.end method

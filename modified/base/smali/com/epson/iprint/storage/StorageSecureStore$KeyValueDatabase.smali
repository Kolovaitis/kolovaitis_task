.class Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;
.super Ljava/lang/Object;
.source "StorageSecureStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/StorageSecureStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KeyValueDatabase"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;
    }
.end annotation


# instance fields
.field mSecretText:Ljava/lang/String;

.field mSqlDB:Landroid/database/sqlite/SQLiteDatabase;

.field mSqlHelper:Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;

.field final synthetic this$0:Lcom/epson/iprint/storage/StorageSecureStore;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/StorageSecureStore;Landroid/content/Context;)V
    .locals 6

    .line 249
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->this$0:Lcom/epson/iprint/storage/StorageSecureStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 245
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlHelper:Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;

    .line 246
    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 250
    new-instance p1, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;

    const-string v3, "storage.data"

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p1

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;-><init>(Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlHelper:Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;

    .line 251
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlHelper:Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 252
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSecretText:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method declared-synchronized close()V
    .locals 1

    monitor-enter p0

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 326
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlHelper:Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase$DatabaseHelper;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 328
    :catch_0
    :goto_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized delete(Ljava/lang/String;)Z
    .locals 6

    monitor-enter p0

    const/4 v0, 0x0

    .line 286
    :try_start_0
    iget-object v1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "keyvalue_tbl"

    const-string v3, "key=?"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/String;

    aput-object p1, v5, v0

    invoke-virtual {v1, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    .line 291
    :catch_0
    :cond_0
    :goto_0
    monitor-exit p0

    return v0
.end method

.method declared-synchronized insert(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    monitor-enter p0

    const-wide/16 v0, -0x1

    .line 264
    :try_start_0
    iget-object v2, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->this$0:Lcom/epson/iprint/storage/StorageSecureStore;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    iget-object v3, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSecretText:Ljava/lang/String;

    invoke-virtual {v2, p2, v3}, Lcom/epson/iprint/storage/StorageSecureStore;->encodeAes([BLjava/lang/String;)[B

    move-result-object p2

    if-eqz p2, :cond_0

    .line 266
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "key"

    .line 267
    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "value"

    .line 268
    invoke-virtual {v2, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 269
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string p2, "keyvalue_tbl"

    const-string v3, ""

    invoke-virtual {p1, p2, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v0, p1

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 272
    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1

    :catch_0
    :cond_0
    :goto_0
    const-wide/16 p1, 0x0

    cmp-long v2, v0, p1

    if-ltz v2, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 274
    :goto_1
    monitor-exit p0

    return p1
.end method

.method declared-synchronized select()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 299
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    .line 302
    :try_start_1
    iget-object v2, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSqlDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "SELECT * FROM keyvalue_tbl;"

    invoke-virtual {v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 303
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 304
    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 305
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    .line 306
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 307
    iget-object v6, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->this$0:Lcom/epson/iprint/storage/StorageSecureStore;

    iget-object v7, p0, Lcom/epson/iprint/storage/StorageSecureStore$KeyValueDatabase;->mSecretText:Ljava/lang/String;

    invoke-virtual {v6, v5, v7}, Lcom/epson/iprint/storage/StorageSecureStore;->decodeAes([BLjava/lang/String;)[B

    move-result-object v5

    if-eqz v5, :cond_0

    .line 309
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v0, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    .line 314
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 317
    :cond_2
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 314
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;
.super Ljava/lang/Object;
.source "OneDriveClient.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/ICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getRootFolder(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/concurrency/ICallback<",
        "Lcom/onedrive/sdk/extensions/Item;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

.field final synthetic val$client:Lcom/onedrive/sdk/extensions/IOneDriveClient;

.field final synthetic val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 0

    .line 432
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->val$client:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    iput-object p3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lcom/onedrive/sdk/core/ClientException;)V
    .locals 2

    .line 443
    invoke-virtual {p1}, Lcom/onedrive/sdk/core/ClientException;->printStackTrace()V

    .line 444
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$100(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {p1, v0, v1}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void
.end method

.method public success(Lcom/onedrive/sdk/extensions/Item;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 436
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->val$client:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    iget-object p1, p1, Lcom/onedrive/sdk/extensions/Item;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    invoke-static {v0, v1, p1, v2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$400(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    goto :goto_0

    .line 438
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$100(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {p1, v0, v1}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0

    .line 432
    check-cast p1, Lcom/onedrive/sdk/extensions/Item;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;->success(Lcom/onedrive/sdk/extensions/Item;)V

    return-void
.end method

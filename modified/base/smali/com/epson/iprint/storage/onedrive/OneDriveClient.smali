.class public Lcom/epson/iprint/storage/onedrive/OneDriveClient;
.super Lcom/epson/iprint/storage/StorageServiceClient;
.source "OneDriveClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;,
        Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;,
        Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;,
        Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;
    }
.end annotation


# static fields
.field private static final EXTENSIONS_IPRINT_SPEC:[Ljava/lang/String;

.field private static final EXTENSIONS_ONEDRIVE_SPEC:[Ljava/lang/String;

.field private static final MIMETYPE_IPRINT_SPEC:[Ljava/lang/String;

.field protected static final TAG:Ljava/lang/String; = "OneDriveClient"


# instance fields
.field private mStorageItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;"
        }
    .end annotation
.end field

.field private mUploadProcessError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

.field private oneDriveClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;


# direct methods
.method static constructor <clinit>()V
    .locals 21

    const-string v0, "doc"

    const-string v1, "docx"

    const-string v2, "epub"

    const-string v3, "eml"

    const-string v4, "htm"

    const-string v5, "html"

    const-string v6, "md"

    const-string v7, "msg"

    const-string v8, "odp"

    const-string v9, "ods"

    const-string v10, "odt"

    const-string v11, "pps"

    const-string v12, "ppsx"

    const-string v13, "ppt"

    const-string v14, "pptx"

    const-string v15, "rtf"

    const-string v16, "tif"

    const-string v17, "tiff"

    const-string v18, "xls"

    const-string v19, "xlsm"

    const-string v20, "xlsx"

    .line 43
    filled-new-array/range {v0 .. v20}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->EXTENSIONS_ONEDRIVE_SPEC:[Ljava/lang/String;

    const-string v1, "doc"

    const-string v2, "docx"

    const-string v3, "rtf"

    const-string v4, "xls"

    const-string v5, "xlsm"

    const-string v6, "xlsx"

    const-string v7, "ppt"

    const-string v8, "pptx"

    const-string v9, "pps"

    const-string v10, "ppsx"

    .line 49
    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->EXTENSIONS_IPRINT_SPEC:[Ljava/lang/String;

    const-string v1, "application/msword"

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v3, "application/vnd.ms-powerpoint"

    const-string v4, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    const-string v5, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    const-string v6, "application/rtf"

    const-string v7, "application/vnd.ms-excel"

    const-string v8, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    const-string v9, "application/vnd.ms-excel.sheet.macroEnabled.12"

    .line 54
    filled-new-array/range {v1 .. v9}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->MIMETYPE_IPRINT_SPEC:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageServiceClient;-><init>()V

    .line 69
    invoke-virtual {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->resetUploadProcessError()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getRootFolder(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Ljava/util/List;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->mStorageItemList:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/Item;)Z
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isConvertPdfExtension(Lcom/onedrive/sdk/extensions/Item;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1100(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/Item;)Z
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isConvertPdfMimeType(Lcom/onedrive/sdk/extensions/Item;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getDownloadInputStreamPdf(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1400(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getDownloadInputStream(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1600(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getConvertPdfName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1702(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->mUploadProcessError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Z
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isExpired(Lcom/onedrive/sdk/extensions/IOneDriveClient;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1900(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Lcom/onedrive/sdk/extensions/UploadSession;
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getUploadSession(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Lcom/onedrive/sdk/extensions/UploadSession;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/UploadSession;Ljava/io/InputStream;ILcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->uploadLargeFile(Lcom/onedrive/sdk/extensions/UploadSession;Ljava/io/InputStream;ILcom/onedrive/sdk/concurrency/IProgressCallback;)V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->oneDriveClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    return-object p0
.end method

.method static synthetic access$302(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->oneDriveClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    return-object p1
.end method

.method static synthetic access$400(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getItems(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    return-void
.end method

.method static synthetic access$500(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getPrintableItems(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/Item;)Lcom/epson/iprint/storage/StorageItem;
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getStorageItem(Lcom/onedrive/sdk/extensions/Item;)Lcom/epson/iprint/storage/StorageItem;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;)Lcom/onedrive/sdk/concurrency/ICallback;
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getCallbackCollectionPage(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;)Lcom/onedrive/sdk/concurrency/ICallback;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$800(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IItemCollectionPage;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getNextPage(Lcom/onedrive/sdk/extensions/IItemCollectionPage;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method private debug_list(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;)V"
        }
    .end annotation

    .line 188
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/iprint/storage/StorageItem;

    .line 190
    iget-object v0, v0, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast v0, Lcom/onedrive/sdk/extensions/Item;

    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getItemPhotos(Lcom/onedrive/sdk/extensions/Item;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getCallbackCollectionPage(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;)Lcom/onedrive/sdk/concurrency/ICallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;",
            ")",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
            ">;"
        }
    .end annotation

    .line 214
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;)V

    return-object v0
.end method

.method private getCallbackItemCollectionPage(Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)Lcom/onedrive/sdk/concurrency/ICallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;",
            ")",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
            ">;"
        }
    .end annotation

    .line 173
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$3;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$3;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getCallbackCollectionPage(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;)Lcom/onedrive/sdk/concurrency/ICallback;

    move-result-object p1

    return-object p1
.end method

.method private getClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->oneDriveClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    if-nez v0, :cond_0

    .line 396
    invoke-virtual {p0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getOneDriveClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 399
    invoke-interface {p2, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;->onNotifyOneDriveClient(Lcom/onedrive/sdk/extensions/IOneDriveClient;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private getConvertPdfName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    if-eqz p1, :cond_1

    .line 477
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "."

    .line 478
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 480
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".pdf"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 482
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".pdf"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0
.end method

.method private getDownloadInputStream(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 0

    .line 342
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getDrive()Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;

    move-result-object p1

    .line 343
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;->getItems(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;

    move-result-object p1

    .line 344
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemRequestBuilder;->getContent()Lcom/onedrive/sdk/extensions/IItemStreamRequestBuilder;

    move-result-object p1

    .line 345
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemStreamRequestBuilder;->buildRequest()Lcom/onedrive/sdk/extensions/IItemStreamRequest;

    move-result-object p1

    .line 346
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemStreamRequest;->get()Ljava/io/InputStream;

    move-result-object p1

    return-object p1
.end method

.method private getDownloadInputStreamPdf(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3

    .line 353
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "format"

    const-string v2, "pdf"

    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getDrive()Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;

    move-result-object p1

    .line 356
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;->getItems(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;

    move-result-object p1

    .line 357
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemRequestBuilder;->getContent()Lcom/onedrive/sdk/extensions/IItemStreamRequestBuilder;

    move-result-object p1

    .line 358
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/onedrive/sdk/extensions/IItemStreamRequestBuilder;->buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IItemStreamRequest;

    move-result-object p1

    .line 359
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemStreamRequest;->get()Ljava/io/InputStream;

    move-result-object p1

    return-object p1
.end method

.method private getItem(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
            ">;)V"
        }
    .end annotation

    .line 286
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getDrive()Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;

    move-result-object p1

    .line 287
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;->getItems(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;

    move-result-object p1

    .line 288
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemRequestBuilder;->getChildren()Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;

    move-result-object p1

    .line 289
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;->buildRequest()Lcom/onedrive/sdk/extensions/IItemCollectionRequest;

    move-result-object p1

    .line 290
    invoke-interface {p1, p3}, Lcom/onedrive/sdk/extensions/IItemCollectionRequest;->get(Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method private getItemDetails(Lcom/onedrive/sdk/extensions/Item;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    if-eqz p1, :cond_15

    .line 516
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 517
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " name: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 518
    :cond_0
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->size:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 519
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " size: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->size:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 520
    :cond_1
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    if-eqz v1, :cond_2

    .line 521
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/File;->mimeType:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 522
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " mimeType: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/File;->mimeType:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 524
    :cond_2
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->folder:Lcom/onedrive/sdk/extensions/Folder;

    if-eqz v1, :cond_3

    .line 525
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->folder:Lcom/onedrive/sdk/extensions/Folder;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Folder;->childCount:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 526
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " childCount: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->folder:Lcom/onedrive/sdk/extensions/Folder;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Folder;->childCount:Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 528
    :cond_3
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->image:Lcom/onedrive/sdk/extensions/Image;

    if-eqz v1, :cond_5

    .line 529
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->image:Lcom/onedrive/sdk/extensions/Image;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Image;->width:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 530
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " width: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->image:Lcom/onedrive/sdk/extensions/Image;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Image;->width:Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 531
    :cond_4
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->image:Lcom/onedrive/sdk/extensions/Image;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Image;->height:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 532
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " height: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->image:Lcom/onedrive/sdk/extensions/Image;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Image;->height:Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 534
    :cond_5
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->audio:Lcom/onedrive/sdk/extensions/Audio;

    if-eqz v1, :cond_8

    .line 535
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->audio:Lcom/onedrive/sdk/extensions/Audio;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Audio;->title:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " title: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->audio:Lcom/onedrive/sdk/extensions/Audio;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Audio;->title:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 537
    :cond_6
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->audio:Lcom/onedrive/sdk/extensions/Audio;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Audio;->artist:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 538
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " artist: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->audio:Lcom/onedrive/sdk/extensions/Audio;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Audio;->artist:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 539
    :cond_7
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->audio:Lcom/onedrive/sdk/extensions/Audio;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Audio;->bitrate:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 540
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " bitrate: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->audio:Lcom/onedrive/sdk/extensions/Audio;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Audio;->bitrate:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 542
    :cond_8
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    if-eqz v1, :cond_f

    .line 543
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Photo;->cameraMake:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 544
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " cameraMake: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Photo;->cameraMake:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 545
    :cond_9
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Photo;->cameraModel:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 546
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " cameraModel: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Photo;->cameraModel:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 547
    :cond_a
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Photo;->exposureDenominator:Ljava/lang/Double;

    if-eqz v1, :cond_b

    .line 548
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " exposureDenominator: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Photo;->exposureDenominator:Ljava/lang/Double;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 549
    :cond_b
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Photo;->exposureNumerator:Ljava/lang/Double;

    if-eqz v1, :cond_c

    .line 550
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " exposureNumerator: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Photo;->exposureNumerator:Ljava/lang/Double;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 551
    :cond_c
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Photo;->fNumber:Ljava/lang/Double;

    if-eqz v1, :cond_d

    .line 552
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " fNumber: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Photo;->fNumber:Ljava/lang/Double;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 553
    :cond_d
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Photo;->focalLength:Ljava/lang/Double;

    if-eqz v1, :cond_e

    .line 554
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " focalLength: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Photo;->focalLength:Ljava/lang/Double;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 555
    :cond_e
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Photo;->iso:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " iso: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Photo;->iso:Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 558
    :cond_f
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->video:Lcom/onedrive/sdk/extensions/Video;

    if-eqz v1, :cond_11

    .line 559
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->video:Lcom/onedrive/sdk/extensions/Video;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Video;->width:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 560
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " width: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->video:Lcom/onedrive/sdk/extensions/Video;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Video;->width:Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 561
    :cond_10
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->video:Lcom/onedrive/sdk/extensions/Video;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Video;->height:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " height: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->video:Lcom/onedrive/sdk/extensions/Video;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Video;->height:Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 564
    :cond_11
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->createdBy:Lcom/onedrive/sdk/extensions/IdentitySet;

    if-eqz v1, :cond_12

    .line 565
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->createdBy:Lcom/onedrive/sdk/extensions/IdentitySet;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/IdentitySet;->application:Lcom/onedrive/sdk/extensions/Identity;

    if-eqz v1, :cond_12

    .line 566
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->createdBy:Lcom/onedrive/sdk/extensions/IdentitySet;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/IdentitySet;->application:Lcom/onedrive/sdk/extensions/Identity;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Identity;->displayName:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 567
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " createdBy-displayName: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->createdBy:Lcom/onedrive/sdk/extensions/IdentitySet;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/IdentitySet;->application:Lcom/onedrive/sdk/extensions/Identity;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Identity;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 570
    :cond_12
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->lastModifiedBy:Lcom/onedrive/sdk/extensions/IdentitySet;

    if-eqz v1, :cond_13

    .line 571
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->lastModifiedBy:Lcom/onedrive/sdk/extensions/IdentitySet;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/IdentitySet;->application:Lcom/onedrive/sdk/extensions/Identity;

    if-eqz v1, :cond_13

    .line 572
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->lastModifiedBy:Lcom/onedrive/sdk/extensions/IdentitySet;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/IdentitySet;->application:Lcom/onedrive/sdk/extensions/Identity;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/Identity;->displayName:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 573
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " lastModifiedBy-displayName: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->lastModifiedBy:Lcom/onedrive/sdk/extensions/IdentitySet;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/IdentitySet;->application:Lcom/onedrive/sdk/extensions/Identity;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/Identity;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 576
    :cond_13
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->parentReference:Lcom/onedrive/sdk/extensions/ItemReference;

    if-eqz v1, :cond_14

    .line 577
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->parentReference:Lcom/onedrive/sdk/extensions/ItemReference;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/ItemReference;->path:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 578
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " parentReference-path: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->parentReference:Lcom/onedrive/sdk/extensions/ItemReference;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/ItemReference;->path:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 580
    :cond_14
    iget-object p1, p1, Lcom/onedrive/sdk/extensions/Item;->shared:Lcom/onedrive/sdk/extensions/Shared;

    :cond_15
    return-object v0
.end method

.method private getItemPhotos(Lcom/onedrive/sdk/extensions/Item;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    if-eqz p1, :cond_6

    .line 491
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 492
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " name: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 493
    :cond_0
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->size:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 494
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " size: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->size:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 495
    :cond_1
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    if-eqz v1, :cond_3

    .line 496
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/File;->mimeType:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 497
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " mimeType: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/File;->mimeType:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 498
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " -FILE- "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 500
    :cond_3
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->folder:Lcom/onedrive/sdk/extensions/Folder;

    if-eqz v1, :cond_4

    .line 501
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " -FOLDER- "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 503
    :cond_4
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->image:Lcom/onedrive/sdk/extensions/Image;

    if-eqz v1, :cond_5

    .line 504
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " -IMAGE- "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 506
    :cond_5
    iget-object p1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    if-eqz p1, :cond_6

    .line 507
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " -PHOTO- "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_6
    return-object v0
.end method

.method private getItems(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 1

    .line 168
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->mStorageItemList:Ljava/util/List;

    .line 169
    invoke-direct {p0, p3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getCallbackItemCollectionPage(Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)Lcom/onedrive/sdk/concurrency/ICallback;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getItem(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method private getNextPage(Lcom/onedrive/sdk/extensions/IItemCollectionPage;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
            ">;)V"
        }
    .end annotation

    .line 295
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemCollectionPage;->getNextPage()Lcom/onedrive/sdk/http/IRequestBuilder;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;

    .line 296
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;->buildRequest()Lcom/onedrive/sdk/extensions/IItemCollectionRequest;

    move-result-object p1

    .line 297
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/extensions/IItemCollectionRequest;->get(Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method private getPrintableItems(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;"
        }
    .end annotation

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 197
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/iprint/storage/StorageItem;

    .line 198
    iget-object v2, v1, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    sget-object v3, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    if-ne v2, v3, :cond_1

    .line 199
    invoke-virtual {p0, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isPrintableFileTypes(Lcom/epson/iprint/storage/StorageItem;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast v2, Lcom/onedrive/sdk/extensions/Item;

    .line 200
    invoke-direct {p0, v2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isConvertPdfExtension(Lcom/onedrive/sdk/extensions/Item;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast v2, Lcom/onedrive/sdk/extensions/Item;

    .line 201
    invoke-direct {p0, v2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isConvertPdfMimeType(Lcom/onedrive/sdk/extensions/Item;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private getRoot(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;)V"
        }
    .end annotation

    .line 278
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getDrive()Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;

    move-result-object p1

    .line 279
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;->getRoot()Lcom/onedrive/sdk/extensions/IItemRequestBuilder;

    move-result-object p1

    .line 280
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemRequestBuilder;->buildRequest()Lcom/onedrive/sdk/extensions/IItemRequest;

    move-result-object p1

    .line 281
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/extensions/IItemRequest;->get(Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method private getRootFolder(Landroid/app/Activity;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 1

    .line 417
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$6;

    invoke-direct {v0, p0, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$6;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    invoke-direct {p0, p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V

    return-void
.end method

.method private getRootFolder(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 1

    .line 432
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;

    invoke-direct {v0, p0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$7;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    invoke-direct {p0, p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getRoot(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method private getStorageItem(Lcom/onedrive/sdk/extensions/Item;)Lcom/epson/iprint/storage/StorageItem;
    .locals 3

    .line 253
    new-instance v0, Lcom/epson/iprint/storage/StorageItem;

    invoke-direct {v0}, Lcom/epson/iprint/storage/StorageItem;-><init>()V

    .line 254
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/epson/iprint/storage/StorageItem;->name:Ljava/lang/String;

    .line 255
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    .line 256
    iput-object p1, v0, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    .line 257
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->folder:Lcom/onedrive/sdk/extensions/Folder;

    if-eqz v1, :cond_0

    .line 258
    sget-object p1, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object p1, v0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    goto :goto_0

    .line 260
    :cond_0
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    if-eqz v1, :cond_1

    .line 261
    sget-object p1, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object p1, v0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    goto :goto_0

    .line 263
    :cond_1
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->image:Lcom/onedrive/sdk/extensions/Image;

    if-eqz v1, :cond_2

    .line 264
    sget-object p1, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object p1, v0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    goto :goto_0

    .line 266
    :cond_2
    iget-object p1, p1, Lcom/onedrive/sdk/extensions/Item;->photo:Lcom/onedrive/sdk/extensions/Photo;

    if-eqz p1, :cond_3

    .line 267
    sget-object p1, Lcom/epson/iprint/storage/StorageItem$ItemType;->FILE:Lcom/epson/iprint/storage/StorageItem$ItemType;

    iput-object p1, v0, Lcom/epson/iprint/storage/StorageItem;->type:Lcom/epson/iprint/storage/StorageItem$ItemType;

    :goto_0
    return-object v0

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method private getUploadSession(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Lcom/onedrive/sdk/extensions/UploadSession;
    .locals 0

    .line 302
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getDrive()Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;

    move-result-object p1

    .line 303
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;->getRoot()Lcom/onedrive/sdk/extensions/IItemRequestBuilder;

    move-result-object p1

    .line 304
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/extensions/IItemRequestBuilder;->getItemWithPath(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;

    move-result-object p1

    new-instance p2, Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;

    invoke-direct {p2}, Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;-><init>()V

    .line 305
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/extensions/IItemRequestBuilder;->getCreateSession(Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)Lcom/onedrive/sdk/extensions/ICreateSessionRequestBuilder;

    move-result-object p1

    .line 306
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/ICreateSessionRequestBuilder;->buildRequest()Lcom/onedrive/sdk/extensions/ICreateSessionRequest;

    move-result-object p1

    .line 307
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/ICreateSessionRequest;->post()Lcom/onedrive/sdk/extensions/UploadSession;

    move-result-object p1

    return-object p1
.end method

.method private isConvertPdfExtension(Lcom/onedrive/sdk/extensions/Item;)Z
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 450
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 451
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_1

    .line 453
    iget-object p1, p1, Lcom/onedrive/sdk/extensions/Item;->name:Ljava/lang/String;

    const/4 v2, 0x1

    add-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 454
    sget-object v1, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->EXTENSIONS_IPRINT_SPEC:[Ljava/lang/String;

    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v1, v4

    .line 455
    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    return v2

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private isConvertPdfMimeType(Lcom/onedrive/sdk/extensions/Item;)Z
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 465
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    iget-object v1, v1, Lcom/onedrive/sdk/extensions/File;->mimeType:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 466
    sget-object v1, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->MIMETYPE_IPRINT_SPEC:[Ljava/lang/String;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 467
    iget-object v5, p1, Lcom/onedrive/sdk/extensions/Item;->file:Lcom/onedrive/sdk/extensions/File;

    iget-object v5, v5, Lcom/onedrive/sdk/extensions/File;->mimeType:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private isExpired(Lcom/onedrive/sdk/extensions/IOneDriveClient;)Z
    .locals 0

    .line 389
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object p1

    .line 390
    invoke-interface {p1}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object p1

    .line 391
    invoke-interface {p1}, Lcom/onedrive/sdk/authentication/IAccountInfo;->isExpired()Z

    move-result p1

    return p1
.end method

.method private logOut(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 364
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object p1

    .line 365
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/authentication/IAuthenticator;->logout(Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method public static saveToken(Lcom/onedrive/sdk/authentication/IAccountInfo;)V
    .locals 3

    if-eqz p0, :cond_0

    .line 371
    invoke-interface {p0}, Lcom/onedrive/sdk/authentication/IAccountInfo;->getAccessToken()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 372
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object v0

    const-string v1, "FolderViewer.ONEDRIVE_V2_TOKEN"

    .line 374
    sget-object v2, Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;->OVERWRITE_IF_EXIST:Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;

    invoke-virtual {v0, v1, p0, v2}, Lcom/epson/iprint/storage/StorageSecureStore;->put(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageSecureStore$EXEC_MODE;)Z

    :cond_0
    return-void
.end method

.method private uploadLargeFile(Lcom/onedrive/sdk/extensions/UploadSession;Ljava/io/InputStream;ILcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/UploadSession;",
            "Ljava/io/InputStream;",
            "I",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;)V"
        }
    .end annotation

    .line 323
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "@name.conflictBehavior"

    const-string v2, "rename"

    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 328
    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->oneDriveClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    const-class v2, Lcom/onedrive/sdk/extensions/Item;

    invoke-virtual {p1, v1, p2, p3, v2}, Lcom/onedrive/sdk/extensions/UploadSession;->createUploadProvider(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/io/InputStream;ILjava/lang/Class;)Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 331
    :try_start_0
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    const/4 p3, 0x2

    new-array p3, p3, [I

    const/4 v0, 0x0

    const/high16 v1, 0xa0000

    aput v1, p3, v0

    const/4 v0, 0x1

    const/4 v1, 0x5

    aput v1, p3, v0

    invoke-virtual {p1, p2, p4, p3}, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->upload(Ljava/util/List;Lcom/onedrive/sdk/concurrency/IProgressCallback;[I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 334
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public getDownloader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
    .locals 1

    .line 87
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
    .locals 1

    .line 92
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/content/Context;)V

    return-object v0
.end method

.method getOneDriveClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V
    .locals 1

    .line 405
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$5;

    invoke-direct {v0, p0, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$5;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V

    invoke-static {p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator;->getOneDriveClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V

    return-void
.end method

.method public getStorageServiceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    const v0, 0x7f0e0400

    .line 164
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUploadProcessError()Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->mUploadProcessError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    return-object v0
.end method

.method public getUploader(Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
    .locals 7

    .line 82
    new-instance v6, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method protected isNeedSignin()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isSessionExpired()Z
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->oneDriveClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    if-eqz v0, :cond_0

    .line 382
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isExpired(Lcom/onedrive/sdk/extensions/IOneDriveClient;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public isSignedIn(Landroid/content/Context;)Z
    .locals 1

    .line 137
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object p1

    const-string v0, "FolderViewer.ONEDRIVE_V2_TOKEN"

    .line 138
    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageSecureStore;->fetch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isSupportedUploadType(Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public resetUploadProcessError()V
    .locals 1

    .line 77
    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->mUploadProcessError:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    return-void
.end method

.method public revokeSignedInData(Landroid/app/Activity;)Z
    .locals 1

    .line 144
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object p1

    const-string v0, "FolderViewer.ONEDRIVE_V2_TOKEN"

    .line 145
    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    .line 146
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->oneDriveClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    if-eqz p1, :cond_0

    .line 147
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$2;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$2;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)V

    invoke-direct {p0, p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->logOut(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/onedrive/sdk/concurrency/ICallback;)V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.class public Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator;
.super Ljava/lang/Object;
.source "OneDriveAuthenticator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "OneDriveAuthenticator"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getMSAAuthenticator(Landroid/content/Context;)Lcom/onedrive/sdk/authentication/MSAAuthenticator;
    .locals 1

    .line 50
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$2;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$2;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static getOneDriveClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V
    .locals 2

    .line 23
    invoke-static {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator;->getMSAAuthenticator(Landroid/content/Context;)Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    move-result-object v0

    invoke-static {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->createWithAuthenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/onedrive/sdk/core/IClientConfig;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    invoke-direct {v1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;-><init>()V

    .line 25
    invoke-virtual {v1, v0}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->fromConfig(Lcom/onedrive/sdk/core/IClientConfig;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$1;

    invoke-direct {v1, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$1;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V

    .line 26
    invoke-virtual {v0, p0, v1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->loginAndBuildClient(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

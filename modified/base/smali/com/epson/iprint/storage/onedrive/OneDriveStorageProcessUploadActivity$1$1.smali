.class Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;
.super Ljava/lang/Object;
.source "OneDriveStorageProcessUploadActivity.java"

# interfaces
.implements Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyOneDriveClient(Lcom/onedrive/sdk/extensions/IOneDriveClient;)V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    iget-object v0, v0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    const v1, 0x7f080151

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-nez p1, :cond_0

    .line 52
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    iget-object p1, p1, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    invoke-static {p1}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->access$000(Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;)Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    iget-object v0, v0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->revokeSignedInData(Landroid/app/Activity;)Z

    .line 53
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    iget-object p1, p1, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->updateSignInStatus()V

    goto :goto_0

    .line 57
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    iget-object p1, p1, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    iget-boolean p1, p1, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mExpired:Z

    if-eqz p1, :cond_1

    .line 58
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    iget-object p1, p1, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    iput-boolean v1, p1, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mExpired:Z

    .line 59
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    iget-object p1, p1, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    invoke-static {p1}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->access$000(Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;)Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->resetUploadProcessError()V

    .line 60
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    iget-object p1, p1, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    const v0, 0x7f0802cc

    invoke-virtual {p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    .line 61
    invoke-virtual {p1}, Landroid/widget/Button;->performClick()Z

    :cond_1
    :goto_0
    return-void
.end method

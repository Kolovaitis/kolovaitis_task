.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$2;
.super Ljava/lang/Object;
.source "OneDriveClient.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/ICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient;->revokeSignedInData(Landroid/app/Activity;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/concurrency/ICallback<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$2;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lcom/onedrive/sdk/core/ClientException;)V
    .locals 1

    .line 154
    invoke-virtual {p1}, Lcom/onedrive/sdk/core/ClientException;->printStackTrace()V

    .line 155
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$2;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$302(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0

    .line 147
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$2;->success(Ljava/lang/Void;)V

    return-void
.end method

.method public success(Ljava/lang/Void;)V
    .locals 1

    .line 150
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$2;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$302(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    return-void
.end method

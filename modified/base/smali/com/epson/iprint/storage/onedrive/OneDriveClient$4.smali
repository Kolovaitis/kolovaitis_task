.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;
.super Ljava/lang/Object;
.source "OneDriveClient.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/ICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getCallbackCollectionPage(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;)Lcom/onedrive/sdk/concurrency/ICallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/concurrency/ICallback<",
        "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

.field final synthetic val$callback:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;)V
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->val$callback:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private result(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;)V"
        }
    .end annotation

    .line 245
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->val$callback:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;

    if-eqz v0, :cond_0

    .line 246
    invoke-interface {v0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;->onNotifyOneDriveClientCollectionPage(Ljava/util/List;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public failure(Lcom/onedrive/sdk/core/ClientException;)V
    .locals 0

    .line 241
    invoke-virtual {p1}, Lcom/onedrive/sdk/core/ClientException;->printStackTrace()V

    const/4 p1, 0x0

    .line 242
    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->result(Ljava/util/List;)V

    return-void
.end method

.method public success(Lcom/onedrive/sdk/extensions/IItemCollectionPage;)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 218
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemCollectionPage;->getCurrentPage()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    .line 220
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 221
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/onedrive/sdk/extensions/Item;

    .line 222
    iget-object v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v3, v2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$600(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/Item;)Lcom/epson/iprint/storage/StorageItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 224
    iget-object v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$100(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_1
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IItemCollectionPage;->getNextPage()Lcom/onedrive/sdk/http/IRequestBuilder;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 228
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->val$callback:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$700(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;)Lcom/onedrive/sdk/concurrency/ICallback;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$800(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IItemCollectionPage;Lcom/onedrive/sdk/concurrency/ICallback;)V

    goto :goto_1

    .line 230
    :cond_2
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$100(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->result(Ljava/util/List;)V

    goto :goto_1

    .line 233
    :cond_3
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->result(Ljava/util/List;)V

    goto :goto_1

    .line 236
    :cond_4
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->result(Ljava/util/List;)V

    :goto_1
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0

    .line 214
    check-cast p1, Lcom/onedrive/sdk/extensions/IItemCollectionPage;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$4;->success(Lcom/onedrive/sdk/extensions/IItemCollectionPage;)V

    return-void
.end method

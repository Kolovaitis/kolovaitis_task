.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;
.super Ljava/lang/Thread;
.source "OneDriveClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadThread"
.end annotation


# instance fields
.field private volatile bCanceled:Z

.field private bConvertPdf:Z

.field private downloaded:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

.field private inputStream:Ljava/io/InputStream;

.field final synthetic this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V
    .locals 0

    .line 623
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 624
    iput-object p2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->downloaded:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    return-void
.end method

.method private closeStream()V
    .locals 1

    .line 708
    :try_start_0
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x0

    .line 710
    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 713
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 718
    iput-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bCanceled:Z

    return-void
.end method

.method public run()V
    .locals 7

    const/4 v0, 0x0

    .line 630
    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;

    const/4 v1, 0x0

    .line 631
    iput-boolean v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bCanceled:Z

    .line 634
    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    iget-object v2, v2, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-static {v3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->access$900(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Lcom/epson/iprint/storage/StorageItem;

    move-result-object v3

    iget-object v3, v3, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast v3, Lcom/onedrive/sdk/extensions/Item;

    invoke-static {v2, v3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1000(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/Item;)Z

    move-result v2

    .line 635
    iget-object v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    iget-object v3, v3, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v4, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-static {v4}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->access$900(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Lcom/epson/iprint/storage/StorageItem;

    move-result-object v4

    iget-object v4, v4, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast v4, Lcom/onedrive/sdk/extensions/Item;

    invoke-static {v3, v4}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1100(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/Item;)Z

    move-result v3

    const/4 v4, 0x1

    if-nez v2, :cond_1

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 638
    :goto_1
    iput-boolean v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bConvertPdf:Z

    .line 640
    iget-boolean v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bConvertPdf:Z

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bCanceled:Z

    if-nez v2, :cond_5

    .line 643
    :try_start_0
    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    iget-object v2, v2, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    iget-object v3, v3, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v3

    iget-object v5, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-static {v5}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->access$1200(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;
    :try_end_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v2

    .line 646
    invoke-virtual {v2}, Lcom/onedrive/sdk/core/ClientException;->printStackTrace()V

    .line 647
    sget-object v3, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->NotSupported:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-virtual {v2, v3}, Lcom/onedrive/sdk/core/ClientException;->isError(Lcom/onedrive/sdk/core/OneDriveErrorCodes;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 650
    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    iput-boolean v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bConvertPdf:Z

    goto :goto_4

    .line 653
    :cond_3
    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    iput-boolean v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bConvertPdf:Z

    .line 659
    :cond_5
    :goto_4
    iget-boolean v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bConvertPdf:Z

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bCanceled:Z

    if-nez v2, :cond_6

    .line 662
    :try_start_1
    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    iget-object v2, v2, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    iget-object v3, v3, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v3

    iget-object v5, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-static {v5}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->access$1200(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1400(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;
    :try_end_1
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception v2

    .line 665
    invoke-virtual {v2}, Lcom/onedrive/sdk/core/ClientException;->printStackTrace()V

    .line 666
    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;

    .line 669
    :cond_6
    :goto_5
    iget-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bConvertPdf:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    iget-object v0, v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-static {v2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->access$1500(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1600(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_7
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-static {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->access$1500(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Ljava/lang/String;

    move-result-object v0

    .line 671
    :goto_6
    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;

    if-eqz v2, :cond_a

    iget-boolean v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bCanceled:Z

    if-nez v2, :cond_a

    .line 673
    :try_start_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 674
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_8

    .line 675
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    .line 676
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 678
    :cond_8
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    const/high16 v2, 0x100000

    .line 680
    new-array v2, v2, [B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 684
    :goto_7
    :try_start_3
    iget-object v5, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v5, v2}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_9

    iget-boolean v6, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bCanceled:Z

    if-nez v6, :cond_9

    .line 686
    invoke-virtual {v3, v2, v1, v5}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_7

    .line 688
    :cond_9
    iget-boolean v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bCanceled:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    xor-int/2addr v1, v4

    goto :goto_8

    :catch_2
    move-exception v2

    .line 690
    :try_start_4
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 693
    :goto_8
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_9

    :catch_3
    move-exception v2

    .line 695
    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_9

    :catch_4
    move-exception v2

    .line 698
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 701
    :cond_a
    :goto_9
    invoke-direct {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->closeStream()V

    .line 703
    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->downloaded:Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;

    iget-object v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;

    invoke-static {v3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->access$900(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Lcom/epson/iprint/storage/StorageItem;

    move-result-object v3

    if-eqz v1, :cond_b

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    goto :goto_a

    :cond_b
    iget-boolean v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->bCanceled:Z

    if-eqz v1, :cond_c

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    goto :goto_a

    :cond_c
    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    :goto_a
    invoke-interface {v2, v3, v0, v1}, Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;->onDownloadComplete(Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void
.end method

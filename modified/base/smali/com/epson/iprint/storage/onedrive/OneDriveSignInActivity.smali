.class public Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity;
.super Lcom/epson/iprint/storage/StorageSignInActivity;
.source "OneDriveSignInActivity.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "OneDriveSignInActivity"


# instance fields
.field private mAuthActivityStarted:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity;)V
    .locals 0

    .line 6
    invoke-virtual {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity;->showLoginErrorAndFinish()V

    return-void
.end method


# virtual methods
.method public getBasicSignIn()Lcom/epson/iprint/storage/StorageSignInActivity$BasicSignIn;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onResume()V
    .locals 1

    .line 12
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageSignInActivity;->onResume()V

    .line 14
    iget-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity;->mAuthActivityStarted:Z

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity$1;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity$1;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity;)V

    invoke-static {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator;->getOneDriveClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V

    const/4 v0, 0x1

    .line 25
    iput-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity;->mAuthActivityStarted:Z

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveSignInActivity;->finish()V

    :goto_0
    return-void
.end method

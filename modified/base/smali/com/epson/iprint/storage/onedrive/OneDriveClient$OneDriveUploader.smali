.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;
.super Lcom/epson/iprint/storage/StorageServiceClient$Uploader;
.source "OneDriveClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OneDriveUploader"
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mLocalUploader:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

.field private mOrgFilePath:Ljava/lang/String;

.field private mUploadPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/content/Context;Lcom/epson/iprint/storage/StorageServiceClient$UploadFileType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 734
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    .line 735
    check-cast p2, Landroid/app/Activity;

    iput-object p2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mActivity:Landroid/app/Activity;

    .line 736
    iput-object p4, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mOrgFilePath:Ljava/lang/String;

    .line 737
    iput-object p5, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mUploadPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 759
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageServiceClient$Uploader;->cancel()V

    .line 760
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mLocalUploader:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->cancel()V

    return-void
.end method

.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V
    .locals 7

    .line 747
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1702(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    .line 748
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1800(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 749
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mOrgFilePath:Ljava/lang/String;

    iget-object v5, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mUploadPath:Ljava/lang/String;

    move-object v1, v0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mLocalUploader:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

    .line 750
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->mLocalUploader:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 753
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->RETRY:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-static {v0, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1702(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    const-string v0, ""

    const-string v1, ""

    .line 754
    sget-object v2, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->RETRY:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {p1, v0, v1, v2}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    :goto_0
    return-void
.end method

.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;
.super Lcom/epson/iprint/storage/StorageServiceClient$Downloader;
.source "OneDriveClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OneDriveDownloader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;
    }
.end annotation


# instance fields
.field private mItemid:Ljava/lang/String;

.field private mStorageItem:Lcom/epson/iprint/storage/StorageItem;

.field private mThread:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;

.field private mWriteFilename:Ljava/lang/String;

.field final synthetic this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/content/Context;Lcom/epson/iprint/storage/StorageItem;Ljava/lang/String;)V
    .locals 0

    .line 592
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Downloader;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    .line 593
    iput-object p3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mStorageItem:Lcom/epson/iprint/storage/StorageItem;

    .line 594
    iget-object p1, p3, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast p1, Lcom/onedrive/sdk/extensions/Item;

    iget-object p1, p1, Lcom/onedrive/sdk/extensions/Item;->id:Ljava/lang/String;

    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mItemid:Ljava/lang/String;

    .line 595
    iput-object p4, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mWriteFilename:Ljava/lang/String;

    const/4 p1, 0x0

    .line 596
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mThread:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;

    return-void
.end method

.method static synthetic access$1200(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Ljava/lang/String;
    .locals 0

    .line 586
    iget-object p0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mItemid:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Ljava/lang/String;
    .locals 0

    .line 586
    iget-object p0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mWriteFilename:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$900(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;)Lcom/epson/iprint/storage/StorageItem;
    .locals 0

    .line 586
    iget-object p0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mStorageItem:Lcom/epson/iprint/storage/StorageItem;

    return-object p0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 612
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mThread:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;

    if-eqz v0, :cond_0

    .line 613
    invoke-virtual {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->cancel()V

    :cond_0
    return-void
.end method

.method public isCancelable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public start(Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V
    .locals 1

    .line 606
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;

    invoke-direct {v0, p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;Lcom/epson/iprint/storage/StorageServiceClient$DownloadCompletion;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mThread:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;

    .line 607
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader;->mThread:Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;

    invoke-virtual {p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$OneDriveDownloader$DownloadThread;->start()V

    return-void
.end method

.class final Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$1;
.super Ljava/lang/Object;
.source "OneDriveAuthenticator.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/ICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator;->getOneDriveClient(Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/concurrency/ICallback<",
        "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$1;->val$callback:Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lcom/onedrive/sdk/core/ClientException;)V
    .locals 1

    .line 41
    invoke-virtual {p1}, Lcom/onedrive/sdk/core/ClientException;->printStackTrace()V

    .line 42
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$1;->val$callback:Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 43
    invoke-interface {p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;->onNotifyOneDriveClient(Lcom/onedrive/sdk/extensions/IOneDriveClient;)V

    :cond_0
    return-void
.end method

.method public success(Lcom/onedrive/sdk/extensions/IOneDriveClient;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 30
    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 32
    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->saveToken(Lcom/onedrive/sdk/authentication/IAccountInfo;)V

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$1;->val$callback:Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;

    if-eqz v0, :cond_1

    .line 36
    invoke-interface {v0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;->onNotifyOneDriveClient(Lcom/onedrive/sdk/extensions/IOneDriveClient;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$1;->success(Lcom/onedrive/sdk/extensions/IOneDriveClient;)V

    return-void
.end method

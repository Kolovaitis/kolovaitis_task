.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$3;
.super Ljava/lang/Object;
.source "OneDriveClient.java"

# interfaces
.implements Lcom/epson/iprint/storage/onedrive/OneDriveClient$OnOneDriveClientListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getCallbackItemCollectionPage(Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)Lcom/onedrive/sdk/concurrency/ICallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

.field final synthetic val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$3;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$3;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyOneDriveClientCollectionPage(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/epson/iprint/storage/StorageItem;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 179
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$3;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$500(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$3;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    if-eqz p1, :cond_1

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    :goto_0
    invoke-interface {v0, p1, v1}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void
.end method

.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;
.super Ljava/lang/Object;
.source "OneDriveClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalUploader"
.end annotation


# instance fields
.field private volatile bCanceled:Z

.field private fileInputStream:Ljava/io/FileInputStream;

.field private mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

.field private mOrgFilePath:Ljava/lang/String;

.field private mUploadFilename:Ljava/lang/String;

.field private result:Z

.field final synthetic this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

.field private uploadSession:Lcom/onedrive/sdk/extensions/UploadSession;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 773
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x1

    .line 768
    iput-boolean p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->result:Z

    const/4 p1, 0x0

    .line 769
    iput-boolean p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->bCanceled:Z

    .line 774
    iput-object p3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    .line 775
    iput-object p4, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mUploadFilename:Ljava/lang/String;

    .line 776
    iput-object p5, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    return-void
.end method

.method static synthetic access$2002(Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;Z)Z
    .locals 0

    .line 764
    iput-boolean p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->result:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;)Z
    .locals 0

    .line 764
    iget-boolean p0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->bCanceled:Z

    return p0
.end method

.method static synthetic access$2200(Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;)V
    .locals 0

    .line 764
    invoke-direct {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->uploadcancel()V

    return-void
.end method

.method private closefile()V
    .locals 1

    .line 845
    :try_start_0
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->fileInputStream:Ljava/io/FileInputStream;

    if-eqz v0, :cond_0

    .line 846
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->fileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 849
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method private upload(I)V
    .locals 4

    const/4 v0, 0x0

    .line 808
    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->uploadSession:Lcom/onedrive/sdk/extensions/UploadSession;

    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/Epson iPrint/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mUploadFilename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 812
    :try_start_0
    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$1900(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;)Lcom/onedrive/sdk/extensions/UploadSession;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->uploadSession:Lcom/onedrive/sdk/extensions/UploadSession;
    :try_end_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 814
    invoke-virtual {v0}, Lcom/onedrive/sdk/core/ClientException;->printStackTrace()V

    .line 817
    :goto_0
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->uploadSession:Lcom/onedrive/sdk/extensions/UploadSession;

    if-eqz v0, :cond_0

    .line 818
    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->fileInputStream:Ljava/io/FileInputStream;

    new-instance v3, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader$1;

    invoke-direct {v3, p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader$1;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;)V

    invoke-static {v1, v0, v2, p1, v3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$2300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/UploadSession;Ljava/io/InputStream;ILcom/onedrive/sdk/concurrency/IProgressCallback;)V

    :cond_0
    return-void
.end method

.method private uploadcancel()V
    .locals 0

    .line 840
    invoke-direct {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->closefile()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 854
    iput-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->bCanceled:Z

    return-void
.end method

.method public run()V
    .locals 4

    const/4 v0, 0x0

    .line 781
    iput-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->bCanceled:Z

    .line 782
    iput-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->result:Z

    .line 783
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mUploadFilename:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 784
    iget-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->bCanceled:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 785
    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->fileInputStream:Ljava/io/FileInputStream;

    .line 788
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->fileInputStream:Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 790
    :try_start_1
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->fileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v0

    if-lez v0, :cond_0

    .line 791
    iget-boolean v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->bCanceled:Z

    if-nez v1, :cond_0

    .line 792
    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->upload(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 795
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    .line 798
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 800
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->closefile()V

    goto :goto_2

    :goto_1
    invoke-direct {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->closefile()V

    throw v0

    .line 804
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mNotifier:Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;

    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mOrgFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->mUploadFilename:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->result:Z

    if-eqz v3, :cond_2

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->NONE:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    goto :goto_3

    :cond_2
    iget-boolean v3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->bCanceled:Z

    if-eqz v3, :cond_3

    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->CANCELED:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    goto :goto_3

    :cond_3
    sget-object v3, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    :goto_3
    invoke-interface {v0, v1, v2, v3}, Lcom/epson/iprint/storage/StorageServiceClient$UploadCompletion;->onUploadComplete(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    return-void
.end method

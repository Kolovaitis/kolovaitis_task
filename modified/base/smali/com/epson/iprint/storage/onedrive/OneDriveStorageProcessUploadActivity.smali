.class public Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;
.super Lcom/epson/iprint/storage/StorageProcessUploadActivity;
.source "OneDriveStorageProcessUploadActivity.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "OneDriveStorageProcessUploadActivity"


# instance fields
.field mExpired:Z

.field private mStorageServiceClient:Lcom/epson/iprint/storage/onedrive/OneDriveClient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;)Lcom/epson/iprint/storage/onedrive/OneDriveClient;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    return-object p0
.end method

.method private checkSession()V
    .locals 2

    .line 38
    invoke-virtual {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->getStorageClient()Lcom/epson/iprint/storage/StorageServiceClient;

    move-result-object v0

    check-cast v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iput-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    .line 40
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-virtual {v0, p0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isSignedIn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-virtual {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->isSessionExpired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity$1;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;)V

    .line 68
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const v0, 0x7f080151

    .line 69
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public static getProcessIntent(Landroid/content/Context;Ljava/lang/String;Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;)Landroid/content/Intent;
    .locals 1

    .line 77
    new-instance p2, Landroid/content/Intent;

    const-class v0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;

    invoke-direct {p2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    invoke-static {p2, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->setCommonExtra(Landroid/content/Intent;Ljava/lang/String;)V

    return-object p2
.end method


# virtual methods
.method protected onResume()V
    .locals 2

    .line 28
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->onResume()V

    const/4 v0, 0x0

    .line 29
    iput-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mExpired:Z

    .line 31
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mStorageServiceClient:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getUploadProcessError()Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    move-result-object v0

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->RETRY:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-virtual {v0, v1}, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 32
    iput-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mExpired:Z

    .line 33
    invoke-direct {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->checkSession()V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 21
    invoke-super {p0}, Lcom/epson/iprint/storage/StorageProcessUploadActivity;->onStart()V

    const/4 v0, 0x0

    .line 22
    iput-boolean v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->mExpired:Z

    .line 23
    invoke-direct {p0}, Lcom/epson/iprint/storage/onedrive/OneDriveStorageProcessUploadActivity;->checkSession()V

    return-void
.end method

.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;
.super Ljava/lang/Object;
.source "OneDriveClient.java"

# interfaces
.implements Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;->enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;

.field final synthetic val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

.field final synthetic val$signin_notifier:Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;

    iput-object p2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;->val$signin_notifier:Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;

    iput-object p3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyOneDriveClient(Lcom/onedrive/sdk/extensions/IOneDriveClient;)V
    .locals 2

    if-eqz p1, :cond_1

    .line 109
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;->val$signin_notifier:Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;

    if-eqz v0, :cond_0

    .line 111
    invoke-interface {v0}, Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;->onSigninCompletion()V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;

    iget-object v0, v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    invoke-static {v0, p1, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$000(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    goto :goto_0

    .line 117
    :cond_1
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;->val$notifier:Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;

    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;

    iget-object v0, v0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$100(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;->ERROR:Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;

    invoke-interface {p1, v0, v1}, Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;->onEnumerateComplete(Ljava/util/List;Lcom/epson/iprint/storage/StorageServiceClient$ProcessError;)V

    :goto_0
    return-void
.end method

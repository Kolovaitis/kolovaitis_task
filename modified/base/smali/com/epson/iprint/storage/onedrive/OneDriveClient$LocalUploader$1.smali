.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader$1;
.super Ljava/lang/Object;
.source "OneDriveClient.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/IProgressCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->upload(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
        "Lcom/onedrive/sdk/extensions/Item;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;)V
    .locals 0

    .line 818
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lcom/onedrive/sdk/core/ClientException;)V
    .locals 0

    .line 825
    invoke-virtual {p1}, Lcom/onedrive/sdk/core/ClientException;->printStackTrace()V

    return-void
.end method

.method public progress(JJ)V
    .locals 0

    .line 829
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

    invoke-static {p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->access$2100(Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 830
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

    invoke-static {p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->access$2200(Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;)V

    :cond_0
    return-void
.end method

.method public success(Lcom/onedrive/sdk/extensions/Item;)V
    .locals 1

    .line 821
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader$1;->this$1:Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;->access$2002(Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader;Z)Z

    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0

    .line 818
    check-cast p1, Lcom/onedrive/sdk/extensions/Item;

    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$LocalUploader$1;->success(Lcom/onedrive/sdk/extensions/Item;)V

    return-void
.end method

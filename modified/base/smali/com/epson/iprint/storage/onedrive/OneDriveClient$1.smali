.class Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;
.super Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.source "OneDriveClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveClient;->getEnumerator(Landroid/content/Context;)Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/content/Context;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iput-object p2, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/epson/iprint/storage/StorageServiceClient$Enumerator;-><init>(Lcom/epson/iprint/storage/StorageServiceClient;)V

    return-void
.end method


# virtual methods
.method public enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V
    .locals 0

    return-void
.end method

.method public enumerate(Lcom/epson/iprint/storage/StorageItem;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;)V
    .locals 2

    .line 102
    iget-object v0, p1, Lcom/epson/iprint/storage/StorageItem;->path:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;->val$context:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;

    invoke-direct {v1, p0, p3, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1$1;-><init>(Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;Lcom/epson/iprint/storage/StorageServiceClient$SigninCompletion;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    invoke-static {p1, v0, v1}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$200(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Landroid/app/Activity;Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$OnOneDriveAuthenticatorListener;)V

    goto :goto_0

    .line 123
    :cond_0
    iget-object p1, p1, Lcom/epson/iprint/storage/StorageItem;->userInfo:Ljava/lang/Object;

    check-cast p1, Lcom/onedrive/sdk/extensions/Item;

    iget-object p1, p1, Lcom/onedrive/sdk/extensions/Item;->id:Ljava/lang/String;

    .line 124
    iget-object p3, p0, Lcom/epson/iprint/storage/onedrive/OneDriveClient$1;->this$0:Lcom/epson/iprint/storage/onedrive/OneDriveClient;

    invoke-static {p3}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$300(Lcom/epson/iprint/storage/onedrive/OneDriveClient;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-static {p3, v0, p1, p2}, Lcom/epson/iprint/storage/onedrive/OneDriveClient;->access$400(Lcom/epson/iprint/storage/onedrive/OneDriveClient;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/lang/String;Lcom/epson/iprint/storage/StorageServiceClient$EnumerateCompletion;)V

    :goto_0
    return-void
.end method

.method public getRootItem()Lcom/epson/iprint/storage/StorageItem;
    .locals 5

    .line 95
    new-instance v0, Lcom/epson/iprint/storage/StorageItem;

    const-string v1, ""

    const-string v2, ""

    sget-object v3, Lcom/epson/iprint/storage/StorageItem$ItemType;->DIRECTORY:Lcom/epson/iprint/storage/StorageItem$ItemType;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/epson/iprint/storage/StorageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/epson/iprint/storage/StorageItem$ItemType;Ljava/lang/Object;)V

    return-object v0
.end method

.class final Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$2;
.super Lcom/onedrive/sdk/authentication/MSAAuthenticator;
.source "OneDriveAuthenticator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator;->getMSAAuthenticator(Landroid/content/Context;)Lcom/onedrive/sdk/authentication/MSAAuthenticator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;-><init>()V

    return-void
.end method

.method private getMicrosoftAccountForOneDrive(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 79
    new-instance v0, Lcom/epson/iprint/storage/SecureKeyStore;

    invoke-direct {v0}, Lcom/epson/iprint/storage/SecureKeyStore;-><init>()V

    invoke-virtual {v0, p1}, Lcom/epson/iprint/storage/SecureKeyStore;->getApiKeyD(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$2;->val$context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/epson/iprint/storage/onedrive/OneDriveAuthenticator$2;->getMicrosoftAccountForOneDrive(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScopes()[Ljava/lang/String;
    .locals 2

    const-string v0, "onedrive.readwrite"

    const-string v1, "offline_access"

    .line 73
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/epson/iprint/storage/StorageWaitingActivity;
.super Landroid/app/Activity;
.source "StorageWaitingActivity.java"


# static fields
.field static CANCEL_REQUEST_ACTION:Ljava/lang/String; = "StorageProgressActivity.CancelRequestAction"

.field static CHANGE_ACTION:Ljava/lang/String; = "StorageProgressActivity.ChangeAction"

.field static DISMISS_ACTION:Ljava/lang/String; = "StorageProgressActivity.DismissAction"

.field static EXTRA_CANCEL_ENABLED:Ljava/lang/String; = "StorageProgressActivity.ExtraCancelEnabled"

.field static EXTRA_MESSAGE:Ljava/lang/String; = "StorageProgressActivity.ExtraMessage"

.field static EXTRA_REQUEST_CODE:Ljava/lang/String; = "StorageProgressActivity.ExtraRequestCode"

.field static REGISTER_ACTION:Ljava/lang/String; = "StorageProgressActivity.RegisterAction"

.field public static bShowing:Z = false


# instance fields
.field mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field mCancelButton:Landroid/widget/Button;

.field mMessageText:Landroid/widget/TextView;

.field mRequestCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method createCancelDialog()Landroid/app/Dialog;
    .locals 3

    .line 174
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e04de

    .line 175
    invoke-virtual {p0, v1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    .line 176
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e052b

    .line 190
    invoke-virtual {p0, v1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/iprint/storage/StorageWaitingActivity$2;

    invoke-direct {v2, p0}, Lcom/epson/iprint/storage/StorageWaitingActivity$2;-><init>(Lcom/epson/iprint/storage/StorageWaitingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e04e6

    .line 205
    invoke-virtual {p0, v1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/iprint/storage/StorageWaitingActivity$3;

    invoke-direct {v2, p0}, Lcom/epson/iprint/storage/StorageWaitingActivity$3;-><init>(Lcom/epson/iprint/storage/StorageWaitingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 211
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 89
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00c2

    .line 90
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->setContentView(I)V

    .line 92
    invoke-virtual {p0}, Lcom/epson/iprint/storage/StorageWaitingActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 93
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->registerBroadcastReceiver(Landroid/content/Intent;)V

    .line 94
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->setMessage(Landroid/content/Intent;)V

    .line 95
    invoke-virtual {p0, p1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->setCancelRequest(Landroid/content/Intent;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageWaitingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 105
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 258
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, 0x0

    .line 259
    sput-boolean v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->bShowing:Z

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 252
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x1

    .line 253
    sput-boolean v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->bShowing:Z

    return-void
.end method

.method registerBroadcastReceiver(Landroid/content/Intent;)V
    .locals 2

    .line 119
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_REQUEST_CODE:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mRequestCode:I

    .line 120
    new-instance p1, Lcom/epson/iprint/storage/StorageWaitingActivity$1;

    invoke-direct {p1, p0}, Lcom/epson/iprint/storage/StorageWaitingActivity$1;-><init>(Lcom/epson/iprint/storage/StorageWaitingActivity;)V

    iput-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 162
    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    .line 163
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->DISMISS_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 164
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->CHANGE_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 165
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->REGISTER_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, p1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method setCancelRequest(Landroid/content/Intent;)V
    .locals 2

    const v0, 0x7f0800b0

    .line 219
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageWaitingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mCancelButton:Landroid/widget/Button;

    .line 220
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mCancelButton:Landroid/widget/Button;

    const v1, 0x7f0e0476

    invoke-virtual {p0, v1}, Lcom/epson/iprint/storage/StorageWaitingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 221
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_CANCEL_ENABLED:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 223
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mCancelButton:Landroid/widget/Button;

    new-instance v0, Lcom/epson/iprint/storage/StorageWaitingActivity$4;

    invoke-direct {v0, p0}, Lcom/epson/iprint/storage/StorageWaitingActivity$4;-><init>(Lcom/epson/iprint/storage/StorageWaitingActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 231
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mCancelButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method setMessage(Landroid/content/Intent;)V
    .locals 1

    const v0, 0x7f080219

    .line 240
    invoke-virtual {p0, v0}, Lcom/epson/iprint/storage/StorageWaitingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mMessageText:Landroid/widget/TextView;

    .line 241
    sget-object v0, Lcom/epson/iprint/storage/StorageWaitingActivity;->EXTRA_MESSAGE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mMessageText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mMessageText:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 246
    :cond_0
    iget-object p1, p0, Lcom/epson/iprint/storage/StorageWaitingActivity;->mMessageText:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

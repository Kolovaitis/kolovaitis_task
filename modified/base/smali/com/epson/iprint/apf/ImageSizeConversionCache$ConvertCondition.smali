.class public Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;
.super Ljava/lang/Object;
.source "ImageSizeConversionCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/apf/ImageSizeConversionCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConvertCondition"
.end annotation


# instance fields
.field public mColorValue:I

.field public mHeight:I

.field public mWidth:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput p1, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mWidth:I

    .line 100
    iput p2, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mHeight:I

    .line 101
    iput p3, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mColorValue:I

    return-void
.end method


# virtual methods
.method public myEquals(Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;)Z
    .locals 2

    .line 105
    iget v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mWidth:I

    iget v1, p1, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mWidth:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mHeight:I

    iget v1, p1, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mHeight:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mColorValue:I

    iget p1, p1, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->mColorValue:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

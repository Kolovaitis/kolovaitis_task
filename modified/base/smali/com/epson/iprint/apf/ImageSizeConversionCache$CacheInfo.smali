.class public Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;
.super Ljava/lang/Object;
.source "ImageSizeConversionCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/apf/ImageSizeConversionCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CacheInfo"
.end annotation


# instance fields
.field mCacheCondition:Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;

.field mFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;Ljava/lang/String;)V
    .locals 0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;->mCacheCondition:Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;

    .line 77
    iput-object p2, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;->mFileName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFilename()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public isValid(Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;)Z
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;->mCacheCondition:Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;

    invoke-virtual {v0, p1}, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;->myEquals(Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;)Z

    move-result p1

    return p1
.end method

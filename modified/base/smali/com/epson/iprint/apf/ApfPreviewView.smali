.class public Lcom/epson/iprint/apf/ApfPreviewView;
.super Ljava/lang/Object;
.source "ApfPreviewView.java"

# interfaces
.implements Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;


# static fields
.field public static final APF_RESIZE_FILE_PREFIX:Ljava/lang/String; = "rsapf_"


# instance fields
.field protected mApfAdapter:Lcom/epson/iprint/apf/ApfAdapter;

.field private mApfMode:I

.field protected mApplicationContext:Landroid/content/Context;

.field private mClearlyVividMode:I

.field protected mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

.field private mResizeCache:Lcom/epson/iprint/apf/ImageSizeConversionCache;

.field private mSharpnessValue:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/epson/iprint/apf/ImageSizeConversionCache;

    invoke-direct {v0}, Lcom/epson/iprint/apf/ImageSizeConversionCache;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mResizeCache:Lcom/epson/iprint/apf/ImageSizeConversionCache;

    return-void
.end method

.method public static deleteWorkingDirectory(Landroid/content/Context;)V
    .locals 0

    .line 81
    invoke-static {p0}, Lcom/epson/iprint/apf/ApfAdapter;->clearDirectory(Landroid/content/Context;)V

    return-void
.end method

.method private getApfResizeCacheFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .line 313
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    .line 314
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    .line 315
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rsapf_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getApfMode()I
    .locals 1

    .line 122
    iget v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mApfMode:I

    return v0
.end method

.method protected getBitmapConventionally(Lepson/print/EPImage;Ljava/lang/String;III)Landroid/graphics/Bitmap;
    .locals 7

    .line 244
    new-instance v6, Lepson/print/EPImage;

    const/4 v0, 0x0

    invoke-direct {v6, p2, v0}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    .line 264
    new-instance v0, Lepson/print/EPImageCreator;

    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lepson/print/EPImageCreator;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x1

    move-object v1, v6

    move v2, p3

    move v3, p4

    move v4, p5

    .line 266
    invoke-virtual/range {v0 .. v5}, Lepson/print/EPImageCreator;->createPreviewImage(Lepson/print/EPImage;IIIZ)Ljava/lang/String;

    .line 271
    iget-object p3, v6, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    const/4 p4, 0x0

    if-nez p3, :cond_0

    return-object p4

    .line 275
    :cond_0
    new-instance p3, Ljava/io/File;

    iget-object v0, v6, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    invoke-direct {p3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 276
    invoke-virtual {p3}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_1

    return-object p4

    .line 282
    :cond_1
    iget v0, v6, Lepson/print/EPImage;->srcWidth:I

    iput v0, p1, Lepson/print/EPImage;->srcWidth:I

    .line 283
    iget v0, v6, Lepson/print/EPImage;->srcHeight:I

    iput v0, p1, Lepson/print/EPImage;->srcHeight:I

    .line 284
    iget v0, v6, Lepson/print/EPImage;->decodeWidth:I

    iput v0, p1, Lepson/print/EPImage;->decodeWidth:I

    .line 285
    iget v0, v6, Lepson/print/EPImage;->decodeHeight:I

    iput v0, p1, Lepson/print/EPImage;->decodeHeight:I

    .line 286
    iget v0, v6, Lepson/print/EPImage;->previewWidth:I

    iput v0, p1, Lepson/print/EPImage;->previewWidth:I

    .line 287
    iget v0, v6, Lepson/print/EPImage;->previewHeight:I

    iput v0, p1, Lepson/print/EPImage;->previewHeight:I

    .line 290
    iput-object p4, p1, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 292
    iget-object p4, v6, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    invoke-static {p4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p4

    .line 295
    invoke-direct {p0, p3, p2}, Lcom/epson/iprint/apf/ApfPreviewView;->getApfResizeCacheFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 296
    invoke-virtual {p3, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result p3

    if-eqz p3, :cond_2

    .line 298
    iget-object p3, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mResizeCache:Lcom/epson/iprint/apf/ImageSizeConversionCache;

    new-instance v1, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;

    iget v2, p1, Lepson/print/EPImage;->previewWidth:I

    iget p1, p1, Lepson/print/EPImage;->previewHeight:I

    invoke-direct {v1, v2, p1, p5}, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;-><init>(III)V

    .line 301
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    .line 298
    invoke-virtual {p3, p2, v1, p1}, Lcom/epson/iprint/apf/ImageSizeConversionCache;->setFilename(Ljava/lang/String;Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;Ljava/lang/String;)V

    :cond_2
    return-object p4
.end method

.method public getClearlyVividMode()I
    .locals 1

    .line 130
    iget v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mClearlyVividMode:I

    return v0
.end method

.method public getImageList()Lepson/print/EPImageList;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreview;->getImageList()Lepson/print/EPImageList;

    move-result-object v0

    return-object v0
.end method

.method public getIsPaperLandscape()Z
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreview;->getIsPaperLandscape()Z

    move-result v0

    return v0
.end method

.method public getPreviewBitmap(Lepson/print/EPImage;IIIZ)Landroid/graphics/Bitmap;
    .locals 6

    .line 190
    iget v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mApfMode:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mSharpnessValue:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mApfAdapter:Lcom/epson/iprint/apf/ApfAdapter;

    iget-object v2, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    iget v3, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mApfMode:I

    iget v4, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mSharpnessValue:I

    iget v5, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mClearlyVividMode:I

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/epson/iprint/apf/ApfAdapter;->getApfResult(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    if-nez p5, :cond_2

    .line 203
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mResizeCache:Lcom/epson/iprint/apf/ImageSizeConversionCache;

    new-instance v3, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;

    iget v4, p1, Lepson/print/EPImage;->previewWidth:I

    iget v5, p1, Lepson/print/EPImage;->previewHeight:I

    invoke-direct {v3, v4, v5, p4}, Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;-><init>(III)V

    invoke-virtual {v0, v2, v3}, Lcom/epson/iprint/apf/ImageSizeConversionCache;->getFilename(Ljava/lang/String;Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 207
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 210
    :cond_1
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "in getPreviewBitmap()"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 218
    invoke-virtual/range {v0 .. v5}, Lcom/epson/iprint/apf/ApfPreviewView;->getBitmapConventionally(Lepson/print/EPImage;Ljava/lang/String;III)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    return-object v0

    .line 221
    :cond_3
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "in getPreviewBitmap()"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_4
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "in getPreviewBitmap()"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSharpnessValue()I
    .locals 1

    .line 126
    iget v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mSharpnessValue:I

    return v0
.end method

.method public init(Landroid/content/Context;Lepson/print/phlayout/PhotoPreview;)V
    .locals 1

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mApplicationContext:Landroid/content/Context;

    .line 69
    new-instance p1, Lcom/epson/iprint/apf/ApfAdapter;

    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mApplicationContext:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/epson/iprint/apf/ApfAdapter;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mApfAdapter:Lcom/epson/iprint/apf/ApfAdapter;

    .line 71
    iput-object p2, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    .line 72
    iget-object p1, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {p1, p0}, Lepson/print/phlayout/PhotoPreview;->setPreviewBitmapMaker(Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;)V

    return-void
.end method

.method public invalidate()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreview;->invalidate()V

    return-void
.end method

.method public invalidateImageNo()V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreview;->invalidateImageNo()V

    return-void
.end method

.method public invalidatePreview()V
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mResizeCache:Lcom/epson/iprint/apf/ImageSizeConversionCache;

    invoke-virtual {v0}, Lcom/epson/iprint/apf/ImageSizeConversionCache;->invalidateCache()V

    .line 167
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreview;->invalidatePreview()V

    return-void
.end method

.method public postInvalidate()V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreview;->postInvalidate()V

    return-void
.end method

.method public setApfLibParams(III)V
    .locals 0

    .line 115
    iput p1, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mApfMode:I

    .line 116
    iput p2, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mSharpnessValue:I

    .line 117
    iput p3, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mClearlyVividMode:I

    return-void
.end method

.method public setColor(I)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0, p1}, Lepson/print/phlayout/PhotoPreview;->setColor(I)V

    return-void
.end method

.method public setDrawEndHandler(Landroid/os/Handler;)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0, p1}, Lepson/print/phlayout/PhotoPreview;->setDrawEndHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public setImage(IILandroid/app/Activity;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .line 175
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0, p1, p2, p3}, Lepson/print/phlayout/PhotoPreview;->setImage(IILandroid/app/Activity;)Z

    move-result p1

    return p1
.end method

.method public setImageList(Lepson/print/phlayout/PhotoPreviewImageList;)V
    .locals 1
    .param p1    # Lepson/print/phlayout/PhotoPreviewImageList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 137
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0, p1}, Lepson/print/phlayout/PhotoPreview;->setImageList(Lepson/print/phlayout/PhotoPreviewImageList;)V

    return-void
.end method

.method public setIsPaperLandscape(ZLandroid/app/Activity;)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0, p1, p2}, Lepson/print/phlayout/PhotoPreview;->setIsPaperLandscape(ZLandroid/app/Activity;)V

    return-void
.end method

.method public setLayout(II)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0, p1, p2}, Lepson/print/phlayout/PhotoPreview;->setLayout(II)V

    return-void
.end method

.method public setPaper(I)Z
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0, p1}, Lepson/print/phlayout/PhotoPreview;->setPaper(I)Z

    move-result p1

    return p1
.end method

.method public setPreviewImageView(Lepson/print/phlayout/PhotoPreview;)V
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    return-void
.end method

.method public setZoomControlHandler(Landroid/os/Handler;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfPreviewView;->mEPPreviewImageView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {v0, p1}, Lepson/print/phlayout/PhotoPreview;->setZoomControlHandler(Landroid/os/Handler;)V

    return-void
.end method

.class public Lcom/epson/iprint/apf/ApfEpImageAdapter;
.super Ljava/lang/Object;
.source "ApfEpImageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/apf/ApfEpImageAdapter$ProgressCallback;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static copyPositionParams(Lepson/print/EPImage;Lepson/print/EPImage;)V
    .locals 1

    .line 91
    iget v0, p0, Lepson/print/EPImage;->previewPaperRectLeft:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 92
    iget v0, p0, Lepson/print/EPImage;->previewPaperRectTop:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 93
    iget v0, p0, Lepson/print/EPImage;->previewPaperRectRight:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 94
    iget v0, p0, Lepson/print/EPImage;->previewPaperRectBottom:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 96
    iget v0, p0, Lepson/print/EPImage;->previewImageRectLeft:F

    iput v0, p1, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 97
    iget v0, p0, Lepson/print/EPImage;->previewImageRectTop:F

    iput v0, p1, Lepson/print/EPImage;->previewImageRectTop:F

    .line 98
    iget v0, p0, Lepson/print/EPImage;->previewImageRectRight:F

    iput v0, p1, Lepson/print/EPImage;->previewImageRectRight:F

    .line 99
    iget v0, p0, Lepson/print/EPImage;->previewImageRectBottom:F

    iput v0, p1, Lepson/print/EPImage;->previewImageRectBottom:F

    .line 101
    iget v0, p0, Lepson/print/EPImage;->previewWidth:I

    iput v0, p1, Lepson/print/EPImage;->previewWidth:I

    .line 102
    iget v0, p0, Lepson/print/EPImage;->previewHeight:I

    iput v0, p1, Lepson/print/EPImage;->previewHeight:I

    .line 104
    iget-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    iput-boolean v0, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 105
    iget v0, p0, Lepson/print/EPImage;->scaleFactor:F

    iput v0, p1, Lepson/print/EPImage;->scaleFactor:F

    .line 106
    iget v0, p0, Lepson/print/EPImage;->previewImageRectCenterX:I

    iput v0, p1, Lepson/print/EPImage;->previewImageRectCenterX:I

    .line 107
    iget p0, p0, Lepson/print/EPImage;->previewImageRectCenterY:I

    iput p0, p1, Lepson/print/EPImage;->previewImageRectCenterY:I

    return-void
.end method

.method public static getApfImageList(Landroid/content/Context;Lepson/print/EPImageList;Lepson/print/service/PrintService;)Lepson/print/EPImageList;
    .locals 19

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const/4 v2, 0x0

    if-eqz v0, :cond_9

    .line 35
    iget-object v3, v0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    goto/16 :goto_4

    .line 38
    :cond_0
    new-instance v3, Lcom/epson/iprint/apf/ApfAdapter;

    move-object/from16 v4, p0

    invoke-direct {v3, v4}, Lcom/epson/iprint/apf/ApfAdapter;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v10, Lepson/print/EPImageList;

    invoke-direct {v10}, Lepson/print/EPImageList;-><init>()V

    .line 42
    iget-object v4, v0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 43
    iget v4, v0, Lepson/print/EPImageList;->apfModeInPrinting:I

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ne v4, v6, :cond_1

    const/4 v12, 0x1

    goto :goto_0

    :cond_1
    const/4 v12, 0x0

    .line 44
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lepson/print/EPImageList;->getSharpness()I

    move-result v4

    if-ne v4, v6, :cond_2

    const/4 v13, 0x1

    goto :goto_1

    :cond_2
    const/4 v13, 0x0

    .line 45
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lepson/print/EPImageList;->getClearlyVivid()I

    move-result v4

    if-ne v4, v6, :cond_3

    const/4 v14, 0x1

    goto :goto_2

    :cond_3
    const/4 v14, 0x0

    .line 47
    :goto_2
    iget-object v4, v0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    const/4 v9, 0x0

    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Lepson/print/EPImage;

    if-eqz v1, :cond_4

    mul-int/lit8 v4, v9, 0x64

    .line 49
    div-int/2addr v4, v11

    .line 50
    invoke-interface {v1, v4}, Lepson/print/service/PrintService;->updateApfProgress(I)Z

    move-result v4

    if-nez v4, :cond_4

    return-object v2

    .line 55
    :cond_4
    iget-object v4, v8, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    iget v5, v0, Lepson/print/EPImageList;->apfModeInPrinting:I

    .line 56
    invoke-virtual/range {p1 .. p1}, Lepson/print/EPImageList;->getSharpness()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lepson/print/EPImageList;->getClearlyVivid()I

    move-result v7

    .line 55
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/epson/iprint/apf/ApfAdapter;->getApfFilename(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v7

    .line 57
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_5

    .line 61
    iget-object v5, v8, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    move-object v4, v3

    move-object v6, v7

    move-object/from16 v16, v7

    move v7, v12

    move-object/from16 v17, v8

    move v8, v13

    move/from16 v18, v9

    move v9, v14

    invoke-virtual/range {v4 .. v9}, Lcom/epson/iprint/apf/ApfAdapter;->processApf(Ljava/lang/String;Ljava/lang/String;ZZZ)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v4, :cond_6

    return-object v2

    :cond_5
    move-object/from16 v16, v7

    move-object/from16 v17, v8

    move/from16 v18, v9

    .line 73
    :cond_6
    new-instance v4, Lepson/print/EPImage;

    move-object/from16 v5, v16

    move/from16 v6, v18

    invoke-direct {v4, v5, v6}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    add-int/lit8 v9, v6, 0x1

    move-object/from16 v6, v17

    .line 75
    invoke-static {v6, v4}, Lcom/epson/iprint/apf/ApfEpImageAdapter;->copyPositionParams(Lepson/print/EPImage;Lepson/print/EPImage;)V

    .line 76
    iget-object v6, v6, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lepson/print/EPImage;->setOrgName(Ljava/lang/String;)V

    .line 77
    iput-object v5, v4, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 79
    invoke-virtual {v10, v4}, Lepson/print/EPImageList;->add(Lepson/print/EPImage;)Z

    goto :goto_3

    :catch_0
    return-object v2

    :catch_1
    return-object v2

    :cond_7
    const/16 v0, 0x64

    .line 82
    invoke-interface {v1, v0}, Lepson/print/service/PrintService;->updateApfProgress(I)Z

    move-result v0

    if-nez v0, :cond_8

    return-object v2

    :cond_8
    return-object v10

    :cond_9
    :goto_4
    return-object v2
.end method

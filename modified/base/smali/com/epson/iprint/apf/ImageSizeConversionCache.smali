.class Lcom/epson/iprint/apf/ImageSizeConversionCache;
.super Ljava/lang/Object;
.source "ImageSizeConversionCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;,
        Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;
    }
.end annotation


# instance fields
.field private mHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache;->mHash:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public getFilename(Ljava/lang/String;Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;)Ljava/lang/String;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache;->mHash:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 29
    :cond_0
    invoke-virtual {v0, p2}, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;->isValid(Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 30
    invoke-virtual {v0}, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;->getFilename()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 32
    :cond_1
    invoke-virtual {v0}, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;->getFilename()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 34
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 37
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 40
    :cond_2
    iget-object p2, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache;->mHash:Ljava/util/HashMap;

    invoke-virtual {p2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method

.method public invalidateCache()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache;->mHash:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public setFilename(Ljava/lang/String;Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;Ljava/lang/String;)V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/epson/iprint/apf/ImageSizeConversionCache;->mHash:Ljava/util/HashMap;

    new-instance v1, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;

    invoke-direct {v1, p2, p3}, Lcom/epson/iprint/apf/ImageSizeConversionCache$CacheInfo;-><init>(Lcom/epson/iprint/apf/ImageSizeConversionCache$ConvertCondition;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

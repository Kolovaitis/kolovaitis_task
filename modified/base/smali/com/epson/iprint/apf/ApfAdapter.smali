.class public Lcom/epson/iprint/apf/ApfAdapter;
.super Ljava/lang/Object;
.source "ApfAdapter.java"


# static fields
.field protected static final APF_IMAGE_DIR:Ljava/lang/String; = "apf_image"

.field public static final BMP_EXTENSION:Ljava/lang/String; = ".bmp"

.field protected static final FILE_PREFIX_APF:Ljava/lang/String; = "apf"

.field protected static final FILE_PREFIX_TMP1:Ljava/lang/String; = "tmp1_"

.field protected static final FILE_PREFIX_TMP2:Ljava/lang/String; = "tmp2_"

.field protected static final mApfLock:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field protected mApfImageDir:Ljava/io/File;

.field protected mApplicationContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/epson/iprint/apf/ApfAdapter;->mApfLock:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApplicationContext:Landroid/content/Context;

    .line 58
    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object v0

    const-string v1, "apf_image"

    invoke-direct {p1, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApfImageDir:Ljava/io/File;

    .line 59
    iget-object p1, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApplicationContext:Landroid/content/Context;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApfImageDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    return-void
.end method

.method public static clearDirectory(Landroid/content/Context;)V
    .locals 2

    .line 68
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p0

    invoke-virtual {p0}, Lepson/common/ExternalFileUtils;->getCacheDir()Ljava/lang/String;

    move-result-object p0

    const-string v1, "apf_image"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-static {v0}, Lcom/epson/iprint/apf/ApfAdapter;->deleteDirectory(Ljava/io/File;)V

    return-void
.end method

.method public static deleteDirectory(Ljava/io/File;)V
    .locals 4

    .line 78
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 81
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 84
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p0

    .line 85
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_4

    aget-object v2, p0, v1

    .line 86
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 87
    invoke-static {v2}, Lcom/epson/iprint/apf/ApfAdapter;->deleteDirectory(Ljava/io/File;)V

    .line 89
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 90
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    :goto_1
    return-void
.end method

.method public static getCurrentPrinterId(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 409
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p0

    .line 410
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    const-string v0, "(?i)^epson +"

    const-string v1, ""

    .line 416
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getExifRotationValue(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    .line 104
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string p0, "Orientation"

    .line 107
    invoke-virtual {v1, p0, v0}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    return v0
.end method

.method public static getMimeTypeWithBitmapFactory(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 193
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 194
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 197
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 199
    iget-object p0, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    return-object p0
.end method

.method public static isEpsonColor(Landroid/content/Context;IIZ)Z
    .locals 1

    .line 424
    new-instance v0, Lcom/epson/sd/common/apf/apflib;

    invoke-direct {v0}, Lcom/epson/sd/common/apf/apflib;-><init>()V

    .line 426
    invoke-static {p0}, Lcom/epson/iprint/apf/ApfAdapter;->getCurrentPrinterId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    .line 427
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/epson/sd/common/apf/apflib;->isDspEpsonColorLogo(Ljava/lang/String;IIZ)Z

    move-result p0

    return p0
.end method

.method public static rotateWithExifValue(ILjava/io/File;Ljava/io/File;)Ljava/io/File;
    .locals 2

    .line 126
    new-instance v0, Lepson/print/EPImageUtil;

    invoke-direct {v0}, Lepson/print/EPImageUtil;-><init>()V

    const/4 v1, 0x3

    if-eq p0, v1, :cond_2

    const/4 v1, 0x6

    if-eq p0, v1, :cond_1

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    return-object p1

    .line 136
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lepson/print/EPImageUtil;->rotateR270Image(Ljava/lang/String;Ljava/lang/String;)I

    return-object p2

    .line 130
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lepson/print/EPImageUtil;->rotateR90Image(Ljava/lang/String;Ljava/lang/String;)I

    return-object p2

    .line 133
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lepson/print/EPImageUtil;->rotate180Image(Ljava/lang/String;Ljava/lang/String;)I

    return-object p2
.end method


# virtual methods
.method protected canUseApfResult(Ljava/lang/String;)Z
    .locals 1

    .line 184
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    return p1
.end method

.method protected convertJpegToBmp(Ljava/lang/String;)Ljava/io/File;
    .locals 5

    const/4 v0, 0x0

    .line 349
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v2, "tmp1_"

    invoke-virtual {p0, p1, v2}, Lcom/epson/iprint/apf/ApfAdapter;->getTmpBmpFilename(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 350
    :try_start_1
    new-instance v2, Lepson/print/EPImageUtil;

    invoke-direct {v2}, Lepson/print/EPImageUtil;-><init>()V

    .line 351
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, p1, v3, v4}, Lepson/print/EPImageUtil;->jpg2bmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_1

    .line 361
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 362
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    return-object v0

    .line 357
    :cond_1
    :try_start_2
    invoke-static {p1}, Lcom/epson/iprint/apf/ApfAdapter;->getExifRotationValue(Ljava/lang/String;)I

    move-result v2

    .line 358
    new-instance v3, Ljava/io/File;

    const-string v4, "tmp2_"

    invoke-virtual {p0, p1, v4}, Lcom/epson/iprint/apf/ApfAdapter;->getTmpBmpFilename(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 359
    :try_start_3
    invoke-static {v2, v1, v3}, Lcom/epson/iprint/apf/ApfAdapter;->rotateWithExifValue(ILjava/io/File;Ljava/io/File;)Ljava/io/File;

    move-result-object p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eq p1, v1, :cond_2

    .line 361
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_2
    if-eq p1, v3, :cond_3

    .line 364
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    :cond_3
    return-object p1

    :catchall_0
    move-exception p1

    move-object v0, v3

    goto :goto_0

    :catchall_1
    move-exception p1

    goto :goto_0

    :catchall_2
    move-exception p1

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_4

    if-eqz v1, :cond_4

    .line 361
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 362
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_4
    if-eqz v0, :cond_5

    if-eqz v0, :cond_5

    .line 364
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 365
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_5
    throw p1
.end method

.method protected convertPngToBmp(Ljava/lang/String;)Ljava/io/File;
    .locals 6

    .line 309
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApplicationContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lepson/common/ImageUtil;->png2jpeg(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    return-object v2

    .line 316
    :cond_0
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApplicationContext:Landroid/content/Context;

    invoke-static {v5}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v5

    invoke-virtual {v5}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 317
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v2

    .line 323
    :cond_1
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v4, "tmp1_"

    invoke-virtual {p0, p1, v4}, Lcom/epson/iprint/apf/ApfAdapter;->getTmpBmpFilename(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 324
    new-instance p1, Lepson/print/EPImageUtil;

    invoke-direct {p1}, Lepson/print/EPImageUtil;-><init>()V

    .line 325
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5, v1}, Lepson/print/EPImageUtil;->jpg2bmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_2

    .line 331
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    return-object v2

    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    return-object v0

    :catchall_0
    move-exception p1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    throw p1
.end method

.method public getApfFilename(Ljava/lang/String;III)Ljava/lang/String;
    .locals 5

    .line 402
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s%ds%dc%d_"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "apf"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 403
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 p3, 0x2

    aput-object p2, v2, p3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 p3, 0x3

    aput-object p2, v2, p3

    .line 402
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 404
    invoke-virtual {p0, p1, p2}, Lcom/epson/iprint/apf/ApfAdapter;->getTmpBmpFilename(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getApfResult(Ljava/lang/String;III)Ljava/lang/String;
    .locals 8

    .line 156
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/epson/iprint/apf/ApfAdapter;->getApfFilename(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v6

    .line 157
    invoke-virtual {p0, v6}, Lcom/epson/iprint/apf/ApfAdapter;->canUseApfResult(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v6

    :cond_0
    const/4 v7, 0x0

    .line 164
    :try_start_0
    sget-object v0, Lcom/epson/iprint/apf/ApfAdapter;->mApfLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-ne p3, v1, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-ne p4, v1, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, v6

    .line 166
    :try_start_1
    invoke-virtual/range {v0 .. v5}, Lcom/epson/iprint/apf/ApfAdapter;->processApf(Ljava/lang/String;Ljava/lang/String;ZZZ)Z

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p1, :cond_4

    .line 170
    :try_start_2
    sget-object p1, Lcom/epson/iprint/apf/ApfAdapter;->mApfLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v7

    :cond_4
    sget-object p1, Lcom/epson/iprint/apf/ApfAdapter;->mApfLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v6

    :catchall_0
    move-exception p1

    sget-object p2, Lcom/epson/iprint/apf/ApfAdapter;->mApfLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw p1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return-object v7
.end method

.method protected getTmpBmpFilename(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 381
    iget-object v0, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApfImageDir:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 386
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object p1

    .line 387
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/epson/iprint/apf/ApfAdapter;->mApfImageDir:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".bmp"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 389
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public processApf(Ljava/lang/String;Ljava/lang/String;ZZZ)Z
    .locals 12

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 224
    :try_start_0
    invoke-static {p1}, Lcom/epson/iprint/apf/ApfAdapter;->getMimeTypeWithBitmapFactory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "image/bmp"

    .line 225
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v7, p1

    const/4 v6, 0x0

    goto :goto_0

    :cond_0
    const-string v3, "image/png"

    .line 229
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 231
    invoke-virtual {p0, p1}, Lcom/epson/iprint/apf/ApfAdapter;->convertPngToBmp(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 236
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v7, v2

    const/4 v6, 0x0

    goto :goto_0

    .line 233
    :cond_1
    new-instance v2, Ljava/lang/OutOfMemoryError;

    const-string v3, "in convertPngToBmp()"

    invoke-direct {v2, v3}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const-string v3, "image/jpeg"

    .line 238
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 240
    invoke-virtual {p0, p1}, Lcom/epson/iprint/apf/ApfAdapter;->convertJpegToBmp(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 245
    invoke-static {p1}, Lcom/epson/iprint/apf/ApfAdapter;->getExifRotationValue(Ljava/lang/String;)I

    move-result v2

    .line 246
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    move v6, v2

    move-object v7, v3

    .line 263
    :goto_0
    new-instance v4, Lcom/epson/sd/common/apf/apflib;

    invoke-direct {v4}, Lcom/epson/sd/common/apf/apflib;-><init>()V

    .line 264
    invoke-virtual {v4}, Lcom/epson/sd/common/apf/apflib;->interrupt()V

    move-object v5, p1

    move-object v8, p2

    move v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    .line 267
    invoke-virtual/range {v4 .. v11}, Lcom/epson/sd/common/apf/apflib;->apfFile(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZ)Z

    move-result v2

    const-string v3, "ApfAdapter"

    .line 269
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "apf returns <"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_5

    .line 276
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x2000

    invoke-static {v2, v3}, Lepson/print/Util/BmpFileSize;->checkDiskCapacity(Ljava/lang/String;I)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_4

    if-eqz v1, :cond_3

    .line 292
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 294
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_3
    const/4 v0, 0x1

    return v0

    .line 278
    :cond_4
    :try_start_1
    new-instance v2, Ljava/lang/OutOfMemoryError;

    const-string v3, "in apflib.apfFile() size check"

    invoke-direct {v2, v3}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v2

    .line 271
    :cond_5
    new-instance v2, Ljava/lang/OutOfMemoryError;

    const-string v3, "in apflib.apfFile()"

    invoke-direct {v2, v3}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v2

    .line 242
    :cond_6
    new-instance v2, Ljava/lang/OutOfMemoryError;

    const-string v3, "in convertJpegToBmp()"

    invoke-direct {v2, v3}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_7
    return v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    nop

    if-eqz v1, :cond_8

    .line 292
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 294
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_8
    return v0

    :catch_1
    move-exception v0

    .line 283
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    if-eqz v1, :cond_9

    .line 292
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 294
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_9
    throw v0
.end method

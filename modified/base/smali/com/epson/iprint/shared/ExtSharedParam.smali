.class public Lcom/epson/iprint/shared/ExtSharedParam;
.super Ljava/lang/Object;
.source "ExtSharedParam.java"


# instance fields
.field private file_name:Ljava/lang/String;

.field private folder_name:Ljava/lang/String;

.field private isValid:Z

.field private package_name:Ljava/lang/String;

.field private pixel_main:I

.field private pixel_sub:I

.field private res_main:I

.field private res_sub:I

.field private scan_mode:I

.field private scan_type:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 8
    iput-boolean v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->isValid:Z

    .line 9
    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->scan_type:I

    .line 10
    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->pixel_main:I

    .line 11
    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->pixel_sub:I

    .line 12
    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->res_main:I

    .line 13
    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->res_sub:I

    const/4 v1, 0x0

    .line 14
    iput-object v1, p0, Lcom/epson/iprint/shared/ExtSharedParam;->folder_name:Ljava/lang/String;

    .line 15
    iput-object v1, p0, Lcom/epson/iprint/shared/ExtSharedParam;->file_name:Ljava/lang/String;

    .line 16
    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->scan_mode:I

    .line 17
    iput-object v1, p0, Lcom/epson/iprint/shared/ExtSharedParam;->package_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFile_name()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->file_name:Ljava/lang/String;

    return-object v0
.end method

.method public getFolder_name()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->folder_name:Ljava/lang/String;

    return-object v0
.end method

.method public getPackage_name()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->package_name:Ljava/lang/String;

    return-object v0
.end method

.method public getPixel_main()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->pixel_main:I

    return v0
.end method

.method public getPixel_sub()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->pixel_sub:I

    return v0
.end method

.method public getRes_main()I
    .locals 1

    .line 71
    iget v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->res_main:I

    return v0
.end method

.method public getRes_sub()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->res_sub:I

    return v0
.end method

.method public getScan_mode()I
    .locals 1

    .line 87
    iget v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->scan_mode:I

    return v0
.end method

.method public getScan_type()I
    .locals 1

    .line 59
    iget v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->scan_type:I

    return v0
.end method

.method public isAvailable()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->isValid:Z

    return v0
.end method

.method public setParam(Landroid/content/Intent;)V
    .locals 1

    .line 38
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "SCAN_TYPE"

    .line 40
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->scan_type:I

    const-string v0, "PIXEL_MAIN"

    .line 41
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->pixel_main:I

    const-string v0, "PIXEL_SUB"

    .line 42
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->pixel_sub:I

    const-string v0, "RES_MAIN"

    .line 43
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->res_main:I

    const-string v0, "RES_SUB"

    .line 44
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->res_sub:I

    const-string v0, "FOLDER_NAME"

    .line 45
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->folder_name:Ljava/lang/String;

    const-string v0, "FILE_NAME"

    .line 46
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->file_name:Ljava/lang/String;

    const-string v0, "SCAN_MODE"

    .line 47
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->scan_mode:I

    const-string v0, "PACKAGE_NAME"

    .line 48
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/shared/ExtSharedParam;->package_name:Ljava/lang/String;

    const/4 p1, 0x1

    .line 50
    iput-boolean p1, p0, Lcom/epson/iprint/shared/ExtSharedParam;->isValid:Z

    return-void
.end method

.method public setParam(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "SCAN_TYPE"

    .line 23
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->scan_type:I

    const-string v0, "PIXEL_MAIN"

    .line 24
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->pixel_main:I

    const-string v0, "PIXEL_SUB"

    .line 25
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->pixel_sub:I

    const-string v0, "RES_MAIN"

    .line 26
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->res_main:I

    const-string v0, "RES_SUB"

    .line 27
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->res_sub:I

    const-string v0, "FOLDER_NAME"

    .line 28
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->folder_name:Ljava/lang/String;

    const-string v0, "FILE_NAME"

    .line 29
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->file_name:Ljava/lang/String;

    const-string v0, "SCAN_MODE"

    .line 30
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/ExtSharedParam;->scan_mode:I

    const-string v0, "PACKAGE_NAME"

    .line 31
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/shared/ExtSharedParam;->package_name:Ljava/lang/String;

    const/4 p1, 0x1

    .line 33
    iput-boolean p1, p0, Lcom/epson/iprint/shared/ExtSharedParam;->isValid:Z

    return-void
.end method

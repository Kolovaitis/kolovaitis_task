.class public Lcom/epson/iprint/shared/SharedParamPhoto;
.super Lcom/epson/iprint/shared/SharedParam;
.source "SharedParamPhoto.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private arrayFileFullPath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private arrayFilePath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private color_mode:I

.field private file_type:I

.field private folder_name:Ljava/lang/String;

.field private layout_type:I

.field private media_size:I

.field private media_type:I

.field private print_mode:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 13
    invoke-direct {p0}, Lcom/epson/iprint/shared/SharedParam;-><init>()V

    const/4 v0, 0x0

    .line 67
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->file_type:I

    const/4 v1, 0x0

    .line 68
    iput-object v1, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->folder_name:Ljava/lang/String;

    .line 69
    iput-object v1, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFilePath:Ljava/util/ArrayList;

    .line 70
    iput-object v1, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFileFullPath:Ljava/util/ArrayList;

    .line 72
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->print_mode:I

    .line 73
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_type:I

    .line 74
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_size:I

    .line 75
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->color_mode:I

    const/4 v0, 0x2

    .line 76
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->layout_type:I

    return-void
.end method

.method private setFullPathName()V
    .locals 4

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFileFullPath:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 147
    :goto_0
    iget-object v1, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFilePath:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFileFullPath:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->folder_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFilePath:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public clearArrayFileFullPath()V
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFileFullPath:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getArrayFileFullPath()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 184
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFileFullPath:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getArrayFilePath()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 180
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFilePath:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getColor_mode()I
    .locals 1

    .line 212
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->color_mode:I

    return v0
.end method

.method public getFile_type()I
    .locals 1

    .line 154
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->file_type:I

    return v0
.end method

.method public getFolder_name()Ljava/lang/String;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->folder_name:Ljava/lang/String;

    return-object v0
.end method

.method public getLayout_type()I
    .locals 1

    .line 216
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->layout_type:I

    return v0
.end method

.method public getMedia_size()I
    .locals 1

    .line 208
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_size:I

    return v0
.end method

.method public getMedia_type()I
    .locals 1

    .line 204
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_type:I

    return v0
.end method

.method public bridge synthetic getPackage_name()Ljava/lang/String;
    .locals 1

    .line 13
    invoke-super {p0}, Lcom/epson/iprint/shared/SharedParam;->getPackage_name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrint_mode()I
    .locals 1

    .line 200
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->print_mode:I

    return v0
.end method

.method public bridge synthetic isAvailable()Z
    .locals 1

    .line 13
    invoke-super {p0}, Lcom/epson/iprint/shared/SharedParam;->isAvailable()Z

    move-result v0

    return v0
.end method

.method public isFileTypeValid()Z
    .locals 1

    .line 169
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->file_type:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isTIFF()Z
    .locals 2

    .line 161
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->file_type:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setArrayFileFullPath(ILjava/lang/String;)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFileFullPath:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setArrayFileFullPath(Ljava/lang/String;)V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFileFullPath:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected setParam(Landroid/os/Bundle;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/iprint/shared/SharedDataException;
        }
    .end annotation

    const-string v0, "FILE_TYPE"

    const/4 v1, 0x2

    .line 79
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->file_type:I

    .line 80
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->file_type:I

    if-ltz v0, :cond_c

    const/4 v2, 0x4

    if-gt v0, v2, :cond_c

    const-string v0, "FOLDER_NAME"

    .line 84
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->folder_name:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->folder_name:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "FILE_NAME"

    .line 89
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFilePath:Ljava/util/ArrayList;

    .line 90
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->arrayFilePath:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "PRINT_MODE"

    const/4 v3, 0x0

    .line 94
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->print_mode:I

    .line 95
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->print_mode:I

    if-ltz v0, :cond_9

    const/4 v4, 0x1

    if-gt v0, v4, :cond_9

    const-string v0, "MEDIA_TYPE"

    .line 99
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_type:I

    .line 100
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_type:I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v5

    if-eq v0, v5, :cond_1

    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_type:I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 101
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v5

    if-eq v0, v5, :cond_1

    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_type:I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HAGAKIRECL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 102
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v5

    if-eq v0, v5, :cond_1

    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_type:I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HAGAKIINKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 103
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v5

    if-ne v0, v5, :cond_0

    goto :goto_0

    .line 105
    :cond_0
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "MEDIA_TYPE error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    const-string v0, "MEDIA_SIZE"

    .line 108
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_size:I

    .line 109
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_size:I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    if-eq v0, v5, :cond_3

    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_size:I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 110
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    if-eq v0, v5, :cond_3

    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_size:I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 111
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    if-eq v0, v5, :cond_3

    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->media_size:I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_POSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 112
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    if-ne v0, v5, :cond_2

    goto :goto_1

    .line 113
    :cond_2
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "MEDIA_SIZE error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    const-string v0, "COLOR_MODE"

    .line 116
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->color_mode:I

    .line 117
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->color_mode:I

    if-eqz v0, :cond_5

    if-ne v0, v4, :cond_4

    goto :goto_2

    .line 118
    :cond_4
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "COLOR_MODE error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_2
    const-string v0, "LAYOUT_TYPE"

    .line 122
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->layout_type:I

    .line 123
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->layout_type:I

    if-eq v0, v4, :cond_7

    if-ne v0, v1, :cond_6

    goto :goto_3

    .line 124
    :cond_6
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "LAYOUT_TYPE error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_3
    const-string v0, "PACKAGE_NAME"

    .line 134
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->package_name:Ljava/lang/String;

    .line 135
    iget-object p1, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->package_name:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    .line 140
    invoke-direct {p0}, Lcom/epson/iprint/shared/SharedParamPhoto;->setFullPathName()V

    .line 142
    iput-boolean v4, p0, Lcom/epson/iprint/shared/SharedParamPhoto;->isValid:Z

    return-void

    .line 136
    :cond_8
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "PACKAGE_NAME error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 96
    :cond_9
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "PRINT_MODE error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 91
    :cond_a
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "FILE_NAME error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 86
    :cond_b
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "FOLDER_NAME error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 81
    :cond_c
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "FILE_TYPE errorr"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

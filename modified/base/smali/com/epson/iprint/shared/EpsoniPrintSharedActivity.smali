.class public Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "EpsoniPrintSharedActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final ACTION_CONFIRM_PRINTER_SCANNER_SELECTION:Ljava/lang/String; = "com.epson.iprint.isSetDevice"

.field public static final ACTION_PHOTO_PRINT:Ljava/lang/String; = "com.epson.iprint.photo"

.field public static final ACTION_SCAN:Ljava/lang/String; = "com.epson.iprint.scan"

.field public static final ACTION_WEB_PRINT:Ljava/lang/String; = "com.epson.iprint.web"

.field public static final ACTIVITY_GET_PRINTER_INFORMATION:Ljava/lang/String; = "epson.maintain.activity.GetPrinterReplyData"

.field private static final ACTIVITY_INTENT_PARAMETER_ERROR_:I = 0x1

.field public static final ACTIVITY_SCAN:Ljava/lang/String; = "epson.scan.activity.ScanActivity"

.field public static final DIALOG_WAIT:Ljava/lang/String; = "dialog_wait"

.field public static final GET_PRINTER_INFORMATION:Ljava/lang/String; = "com.epson.iprint.status"

.field public static final INK_STATUS_ERROR:I = -0x1

.field public static final INK_STATUS_INK_END:I = 0x2

.field public static final INK_STATUS_INK_LOW:I = 0x1

.field public static final INK_STATUS_NORMAL:I = 0x0

.field private static final ISSETDEVICE_DEVICE_NOT_SET:I = 0x0

.field private static final ISSETDEVICE_LOCAL_DEVICE_SET:I = -0x1

.field private static final ISSETDEVICE_REMOTE_DEVICE_SET:I = -0x2

.field private static final LOG_TAG:Ljava/lang/String; = "iPrintSharedActivity"

.field public static final PACKAGE_PANASONIC_FAX:Ljava/lang/String; = "com.panasonic.psn.fax"

.field public static final PARAM_KEY_CALLER_PACKAGE_NAME:Ljava/lang/String; = "PACKAGE_NAME"

.field private static final REQUEST_CODE_LICENSE_CHECK:I = 0x65

.field private static final REQUEST_PHOTO_CUSTOM:I = 0xa

.field private static final REQUEST_RUNTIMEPERMMISSION:I = 0x64

.field private static final REQUEST_SCAN_CUSTOM:I = 0x14

.field private static final REQUEST_STATUS_CUSTOM:I = 0x63

.field private static final REQUEST_WEB_CUSTOM:I = 0x1e

.field public static final RESULT_NO_SD_ERROR:I = 0x1

.field public static final RESULT_PARAM_ERROR:I


# instance fields
.field mHandler:Landroid/os/Handler;

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private thread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 40
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 235
    new-instance v0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity$1;

    invoke-direct {v0, p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity$1;-><init>(Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;)V

    iput-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private CleanUp()V
    .locals 2

    .line 680
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->thread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 681
    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    .line 683
    iput-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->thread:Ljava/lang/Thread;

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_wait"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method private InitTempFolder()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 713
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempSharedDir()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setWait()V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->CleanUp()V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->backgroundJob()V

    return-void
.end method

.method private backgroundJob()V
    .locals 11

    const-string v0, "iPrintSharedActivity"

    const-string v1, "backgroundJob"

    .line 293
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 297
    invoke-virtual {p0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 298
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void

    .line 302
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 303
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-nez v3, :cond_1

    .line 305
    invoke-virtual {p0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 306
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void

    .line 316
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.epson.iprint.photo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 318
    new-instance v4, Lcom/epson/iprint/shared/SharedParamPhoto;

    invoke-direct {v4}, Lcom/epson/iprint/shared/SharedParamPhoto;-><init>()V

    .line 319
    invoke-virtual {v4, v3}, Lcom/epson/iprint/shared/SharedParamPhoto;->setParam(Landroid/os/Bundle;)V

    .line 325
    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamPhoto;->getPackage_name()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.panasonic.psn.fax"

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_2

    .line 327
    new-instance v3, Lcom/epson/iprint/shared/ManagePreferences;

    invoke-direct {v3}, Lcom/epson/iprint/shared/ManagePreferences;-><init>()V

    const-string v5, "com.panasonic.psn.fax"

    .line 328
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/epson/iprint/shared/ManagePreferences;->setPreferences(Ljava/lang/String;Landroid/content/Context;)Z

    goto :goto_0

    .line 331
    :cond_2
    new-instance v5, Lcom/epson/iprint/shared/ManagePreferences;

    invoke-direct {v5}, Lcom/epson/iprint/shared/ManagePreferences;-><init>()V

    .line 332
    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamPhoto;->getMedia_type()I

    move-result v6

    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamPhoto;->getMedia_size()I

    move-result v7

    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamPhoto;->getColor_mode()I

    move-result v8

    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamPhoto;->getLayout_type()I

    move-result v9

    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual/range {v5 .. v10}, Lcom/epson/iprint/shared/ManagePreferences;->setPreferences(IIIILandroid/content/Context;)Z

    .line 336
    :goto_0
    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamPhoto;->isTIFF()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 337
    invoke-direct {p0, v4}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->convertTiff(Lcom/epson/iprint/shared/SharedParamPhoto;)V

    .line 341
    :cond_3
    const-class v3, Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v2, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 342
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "extParam"

    .line 343
    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "print_log"

    .line 345
    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamPhoto;->getPackage_name()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1001

    invoke-direct {p0, v3, v4}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getPhotoPrintLog(Ljava/lang/String;I)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v3

    .line 344
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/16 v0, 0xa

    goto/16 :goto_1

    .line 350
    :cond_4
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.epson.iprint.scan"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 352
    new-instance v4, Lcom/epson/iprint/shared/SharedParamScan;

    invoke-direct {v4}, Lcom/epson/iprint/shared/SharedParamScan;-><init>()V

    .line 353
    invoke-virtual {v4, v3}, Lcom/epson/iprint/shared/SharedParamScan;->setParam(Landroid/os/Bundle;)V

    .line 356
    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_main()I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamScan;->getPixel_sub()I

    move-result v3

    if-lez v3, :cond_5

    .line 357
    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_main()I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual {v4}, Lcom/epson/iprint/shared/SharedParamScan;->getRes_sub()I

    move-result v3

    if-lez v3, :cond_5

    .line 363
    const-class v3, Lepson/scan/activity/ScanActivity;

    invoke-virtual {v2, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 364
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "extParam"

    .line 365
    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "scan-log"

    .line 367
    invoke-direct {p0, v2}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getCallerPackage(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getScanLog(Ljava/lang/String;)Lcom/epson/iprint/prtlogger/CommonLog;

    move-result-object v3

    .line 366
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/16 v0, 0x14

    goto :goto_1

    .line 358
    :cond_5
    new-instance v0, Lcom/epson/iprint/shared/SharedDataException;

    const-string v2, "resolution or pixel error."

    invoke-direct {v0, v2}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 371
    :cond_6
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.epson.iprint.web"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "URL"

    .line 375
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 376
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    .line 381
    const-class v5, Lepson/print/WebviewActivity;

    invoke-virtual {v2, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 382
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.extra.TEXT"

    .line 383
    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "print_log"

    const-string v4, "PACKAGE_NAME"

    .line 385
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1004

    invoke-direct {p0, v3, v4}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getPhotoPrintLog(Ljava/lang/String;I)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v3

    .line 384
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/16 v0, 0x1e

    .line 397
    :goto_1
    invoke-virtual {p0, v2, v0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 398
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    .line 377
    :cond_7
    new-instance v0, Lcom/epson/iprint/shared/SharedDataException;

    const-string v2, "URL error"

    invoke-direct {v0, v2}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_8
    invoke-virtual {p0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 393
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V
    :try_end_0
    .catch Lcom/epson/iprint/shared/SharedDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 408
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 409
    invoke-virtual {p0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 410
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void

    :catch_1
    move-exception v0

    .line 402
    invoke-virtual {v0}, Lcom/epson/iprint/shared/SharedDataException;->printStackTrace()V

    .line 403
    invoke-virtual {p0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 404
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void
.end method

.method private checkPrinterSelection()I
    .locals 4

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 456
    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "PRINTER_NAME"

    const-string v3, ""

    .line 457
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 458
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    return v1

    .line 468
    :cond_0
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x2

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method private checkScannerSelection()I
    .locals 3

    .line 484
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v2, "SCAN_REFS_SCANNER_MODEL"

    invoke-static {v0, v1, v2}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e04d6

    .line 492
    invoke-virtual {p0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ""

    .line 493
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private convertTiff(Lcom/epson/iprint/shared/SharedParamPhoto;)V
    .locals 9

    .line 530
    new-instance v0, Lepson/print/EPImageUtil;

    invoke-direct {v0}, Lepson/print/EPImageUtil;-><init>()V

    .line 532
    invoke-virtual {p1}, Lcom/epson/iprint/shared/SharedParamPhoto;->getArrayFileFullPath()Ljava/util/ArrayList;

    move-result-object v1

    .line 533
    invoke-virtual {p1}, Lcom/epson/iprint/shared/SharedParamPhoto;->getArrayFilePath()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    .line 535
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 536
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    new-instance v5, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v6

    invoke-virtual {v6}, Lepson/common/ExternalFileUtils;->getTempSharedDir()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 537
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Lepson/common/Utils;->getPreffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".bmp"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 536
    invoke-virtual {v0, v4, v5}, Lepson/print/EPImageUtil;->tiff2bmp(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    new-instance v4, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v5

    invoke-virtual {v5}, Lepson/common/ExternalFileUtils;->getTempSharedDir()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 539
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Lepson/common/Utils;->getPreffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".bmp"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 538
    invoke-virtual {p1, v3, v4}, Lcom/epson/iprint/shared/SharedParamPhoto;->setArrayFileFullPath(ILjava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 568
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 570
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private getCallerPackage(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "PACKAGE_NAME"

    .line 418
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 424
    :cond_0
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getPhotoPrintLog(Ljava/lang/String;I)Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 1

    .line 436
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    .line 437
    iput p2, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    .line 438
    iput-object p1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    .line 439
    iget-object p1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    if-nez p1, :cond_0

    .line 440
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method private getScanLog(Ljava/lang/String;)Lcom/epson/iprint/prtlogger/CommonLog;
    .locals 2

    .line 428
    new-instance v0, Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/CommonLog;-><init>()V

    const/16 v1, 0x1101

    .line 429
    iput v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 430
    iput-object p1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->callerPackage:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic lambda$onCreate$0(Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;Ljava/util/Deque;)V
    .locals 2

    .line 111
    iget-object p1, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 113
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 114
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 117
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-direct {p0, v0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 121
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 122
    invoke-direct {p0, v0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private nextOnCreate()V
    .locals 4

    .line 148
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 151
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 159
    :try_start_0
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->InitTempFolder()V

    .line 162
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->CheckAction(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/epson/iprint/shared/SharedDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    new-instance v1, Lcom/epson/iprint/shared/ManagePreferences;

    invoke-direct {v1}, Lcom/epson/iprint/shared/ManagePreferences;-><init>()V

    .line 181
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PrintSetting"

    invoke-virtual {v1, v2, v3}, Lcom/epson/iprint/shared/ManagePreferences;->resetSavedFlag(Landroid/content/Context;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/epson/iprint/shared/ManagePreferences;->pushPreferrences(Landroid/content/Context;)Z

    .line 186
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.epson.iprint.status"

    .line 187
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 190
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "epson.maintain.activity.GetPrinterReplyData"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x63

    .line 191
    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_0
    const-string v1, "com.epson.iprint.isSetDevice"

    .line 193
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setPrinterScannerSelectionInfo()V

    goto :goto_0

    .line 197
    :cond_1
    invoke-static {p0}, Lepson/common/IprintLicenseInfo;->isAgreedCurrentVersion(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 198
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_2

    .line 156
    :cond_3
    :try_start_1
    new-instance v0, Lcom/epson/iprint/shared/SharedDataException;

    const-string v1, "bundle null error"

    invoke-direct {v0, v1}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/epson/iprint/shared/SharedDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 173
    :goto_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const/4 v0, 0x2

    .line 174
    invoke-virtual {p0, v0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 175
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void

    .line 166
    :goto_2
    invoke-virtual {v0}, Lcom/epson/iprint/shared/SharedDataException;->printStackTrace()V

    const/4 v0, 0x1

    .line 167
    invoke-virtual {p0, v0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 168
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void
.end method

.method private setPrinterScannerSelectionInfo()V
    .locals 3

    .line 506
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DEVICE_TYPE"

    const/4 v2, -0x1

    .line 508
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x1

    goto :goto_0

    .line 515
    :pswitch_0
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->checkScannerSelection()I

    move-result v0

    goto :goto_0

    .line 512
    :pswitch_1
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->checkPrinterSelection()I

    move-result v0

    .line 520
    :goto_0
    invoke-virtual {p0, v0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 521
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setWait()V
    .locals 7

    .line 252
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_wait"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    .line 254
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity$2;

    invoke-direct {v1, p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity$2;-><init>(Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->thread:Ljava/lang/Thread;

    .line 262
    :try_start_0
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.epson.iprint.photo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "FILE_TYPE"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 265
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    .line 266
    new-array v1, v3, [Ljava/lang/String;

    const v2, 0x7f0e0422

    invoke-virtual {p0, v2}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v1, v5

    invoke-virtual {p0, v2}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 267
    new-array v2, v3, [Ljava/lang/String;

    const v3, 0x7f0e0420

    .line 268
    invoke-virtual {p0, v3}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 269
    invoke-virtual {p0, v3}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v6, 0x7f0e0424

    invoke-virtual {p0, v6}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v3, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 275
    new-instance v3, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v4, v0, v5

    invoke-direct {v3, v4, v1, v2}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 277
    invoke-static {p0, v0}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x64

    .line 278
    invoke-static {p0, v3, v0}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 284
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 3

    .line 551
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x0

    const v2, 0x16b5a0c

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_wait"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const v0, 0x7f0e051b

    .line 553
    invoke-virtual {p0, v0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    .line 559
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 560
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method private startLicenseCheckActivity()V
    .locals 3

    .line 141
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/common/IprintLicenseInfo;->beforeLicenseCheck(Landroid/content/Context;)V

    .line 142
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lepson/common/IprintLicenseInfo;

    invoke-direct {v1}, Lepson/common/IprintLicenseInfo;-><init>()V

    new-instance v2, Lepson/common/IprintUserSurveyInfo;

    invoke-direct {v2}, Lepson/common/IprintUserSurveyInfo;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;Lcom/epson/mobilephone/common/license/UserSurveyInfo;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x65

    .line 144
    invoke-virtual {p0, v0, v1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method CheckAction(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/iprint/shared/SharedDataException;
        }
    .end annotation

    const-string v0, "com.epson.iprint.photo"

    .line 211
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.epson.iprint.scan"

    .line 212
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.epson.iprint.web"

    .line 213
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.epson.iprint.status"

    .line 214
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.epson.iprint.isSetDevice"

    .line 215
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 220
    :cond_0
    new-instance p1, Lcom/epson/iprint/shared/SharedDataException;

    const-string v0, "action error"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 577
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    const-string v0, "iPrintSharedActivity"

    .line 579
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult::requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "/requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xa

    if-eq p1, v0, :cond_7

    const/16 v0, 0x14

    if-eq p1, v0, :cond_5

    const/16 v0, 0x1e

    if-eq p1, v0, :cond_4

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    if-eq p2, v0, :cond_0

    .line 635
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void

    .line 638
    :cond_0
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->nextOnCreate()V

    return-void

    :pswitch_1
    if-eq p2, v0, :cond_1

    const/4 p1, 0x0

    .line 627
    invoke-virtual {p0, p1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 628
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    goto/16 :goto_0

    .line 624
    :cond_1
    iget-object p1, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->thread:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 600
    :pswitch_2
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    if-eqz p3, :cond_3

    const-string v0, "extStatus"

    .line 602
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p3

    check-cast p3, Lcom/epson/iprint/shared/SharedParamStatus;

    if-eqz p3, :cond_2

    const-string v0, "PRINTER_NAME"

    .line 604
    invoke-virtual {p3}, Lcom/epson/iprint/shared/SharedParamStatus;->getPrinter_name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "INK_NAME"

    .line 605
    invoke-virtual {p3}, Lcom/epson/iprint/shared/SharedParamStatus;->getArrayOutInkName()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "INK_REMAIN"

    .line 606
    invoke-virtual {p3}, Lcom/epson/iprint/shared/SharedParamStatus;->getArrayOutInkRemain()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "STATUS_VALUE"

    .line 607
    invoke-virtual {p3}, Lcom/epson/iprint/shared/SharedParamStatus;->getPrinter_status()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "DEVICE_ID"

    .line 608
    invoke-virtual {p3}, Lcom/epson/iprint/shared/SharedParamStatus;->getPrinter_name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "INK_STATUS"

    .line 609
    invoke-virtual {p3}, Lcom/epson/iprint/shared/SharedParamStatus;->getExternInkStatus()Ljava/util/ArrayList;

    move-result-object p3

    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 610
    invoke-virtual {p0, p2, p1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 612
    :cond_2
    invoke-virtual {p0, p2}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    goto :goto_0

    .line 615
    :cond_3
    invoke-virtual {p0, p2}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    goto :goto_0

    .line 596
    :cond_4
    invoke-virtual {p0, p2}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    goto :goto_0

    .line 587
    :cond_5
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    if-eqz p3, :cond_6

    .line 589
    invoke-static {p3}, Lepson/scan/activity/ScanActivity;->getScanFileListFromReturnIntent(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object p3

    const-string v0, "FILE_NAME"

    .line 590
    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 592
    :cond_6
    invoke-virtual {p0, p2, p1}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 583
    :cond_7
    invoke-virtual {p0, p2}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->setResult(I)V

    .line 646
    :goto_0
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void

    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "iPrintSharedActivity"

    const-string v1, "onCreate"

    .line 101
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lepson/common/DialogProgressViewModel;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lepson/common/DialogProgressViewModel;

    iput-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 106
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {v0}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object v0

    new-instance v1, Lcom/epson/iprint/shared/-$$Lambda$EpsoniPrintSharedActivity$couNCphGmMyZrQmSevVBCIRq2NM;

    invoke-direct {v1, p0}, Lcom/epson/iprint/shared/-$$Lambda$EpsoniPrintSharedActivity$couNCphGmMyZrQmSevVBCIRq2NM;-><init>(Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 127
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->stopLoggerIfNotAgreed(Landroid/content/Context;)V

    if-nez p1, :cond_0

    .line 130
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->startLicenseCheckActivity()V

    goto :goto_0

    .line 132
    :cond_0
    invoke-static {p0}, Lepson/common/IprintLicenseInfo;->isAgreedCurrentVersion(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 133
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->finish()V

    return-void

    .line 136
    :cond_1
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->nextOnCreate()V

    :goto_0
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .line 655
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 658
    new-instance v0, Lcom/epson/iprint/shared/ManagePreferences;

    invoke-direct {v0}, Lcom/epson/iprint/shared/ManagePreferences;-><init>()V

    .line 659
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "PrintSetting"

    invoke-virtual {v0, v1, v2}, Lcom/epson/iprint/shared/ManagePreferences;->getSavedFlag(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "iPrintSharedActivity"

    const-string v2, "Preference \u623b\u3059"

    .line 660
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    invoke-virtual {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/iprint/shared/ManagePreferences;->popPreferrences(Landroid/content/Context;)Z

    .line 665
    :cond_0
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->CleanUp()V

    .line 669
    :try_start_0
    invoke-direct {p0}, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->InitTempFolder()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public run()V
    .locals 2

    const-string v0, "iPrintSharedActivity"

    const-string v1, "run"

    .line 224
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, 0x64

    .line 227
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string v0, "iPrintSharedActivity"

    const-string v1, "run::1"

    .line 231
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/epson/iprint/shared/EpsoniPrintSharedActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.class Lcom/epson/iprint/shared/ParametersErrorException;
.super Lcom/epson/iprint/shared/SharedDataException;
.source "SharedDataException.java"


# instance fields
.field private ParaName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "parameter error"

    .line 30
    invoke-direct {p0, v0}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public getParaName()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/epson/iprint/shared/ParametersErrorException;->ParaName:Ljava/lang/String;

    return-object v0
.end method

.method public setParaName(Ljava/lang/String;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/epson/iprint/shared/ParametersErrorException;->ParaName:Ljava/lang/String;

    return-void
.end method

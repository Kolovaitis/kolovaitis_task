.class public Lcom/epson/iprint/shared/SharedParamStatus;
.super Lcom/epson/iprint/shared/SharedParam;
.source "SharedParamStatus.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x192096777f1fd8b1L


# instance fields
.field private arrayOutInkName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private arrayOutInkRemain:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mEscprLibInkStatus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mcsModelName:Ljava/lang/String;

.field private miReturnStatus:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 11
    invoke-direct {p0}, Lcom/epson/iprint/shared/SharedParam;-><init>()V

    const/4 v0, 0x0

    .line 15
    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->mcsModelName:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkName:Ljava/util/ArrayList;

    .line 17
    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkRemain:Ljava/util/ArrayList;

    .line 19
    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->mEscprLibInkStatus:Ljava/util/ArrayList;

    const/4 v0, 0x4

    .line 20
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->miReturnStatus:I

    return-void
.end method


# virtual methods
.method public clearArrayOutInkName()V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkName:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public clearArrayOutInkRemain()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkRemain:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getArrayOutInkName()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkName:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getArrayOutInkRemain()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkRemain:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExternInkStatus()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->mEscprLibInkStatus:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 83
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 87
    :goto_0
    iget-object v3, p0, Lcom/epson/iprint/shared/SharedParamStatus;->mEscprLibInkStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 88
    iget-object v3, p0, Lcom/epson/iprint/shared/SharedParamStatus;->mEscprLibInkStatus:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x3

    const/4 v5, 0x2

    if-eq v3, v4, :cond_2

    packed-switch v3, :pswitch_data_0

    const/4 v3, -0x1

    .line 118
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 94
    :pswitch_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    const/4 v3, 0x1

    .line 90
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 105
    :pswitch_2
    iget-object v3, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkRemain:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_1

    .line 108
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 110
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 101
    :cond_2
    :pswitch_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic getPackage_name()Ljava/lang/String;
    .locals 1

    .line 11
    invoke-super {p0}, Lcom/epson/iprint/shared/SharedParam;->getPackage_name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrinter_name()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->mcsModelName:Ljava/lang/String;

    return-object v0
.end method

.method public getPrinter_status()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamStatus;->miReturnStatus:I

    return v0
.end method

.method public bridge synthetic isAvailable()Z
    .locals 1

    .line 11
    invoke-super {p0}, Lcom/epson/iprint/shared/SharedParam;->isAvailable()Z

    move-result v0

    return v0
.end method

.method public setArrayOutInkName(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 51
    iput-object p1, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkName:Ljava/util/ArrayList;

    return-void
.end method

.method public setArrayOutInkRemain(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 64
    iput-object p1, p0, Lcom/epson/iprint/shared/SharedParamStatus;->arrayOutInkRemain:Ljava/util/ArrayList;

    return-void
.end method

.method public setEscprlibInkStatus(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 129
    iput-object p1, p0, Lcom/epson/iprint/shared/SharedParamStatus;->mEscprLibInkStatus:Ljava/util/ArrayList;

    return-void
.end method

.method public setParam(Landroid/os/Bundle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/iprint/shared/SharedDataException;
        }
    .end annotation

    return-void
.end method

.method public setPrinter_name(Ljava/lang/String;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/epson/iprint/shared/SharedParamStatus;->mcsModelName:Ljava/lang/String;

    return-void
.end method

.method public setPrinter_status(I)V
    .locals 0

    .line 32
    iput p1, p0, Lcom/epson/iprint/shared/SharedParamStatus;->miReturnStatus:I

    return-void
.end method

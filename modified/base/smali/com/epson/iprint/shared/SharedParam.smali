.class abstract Lcom/epson/iprint/shared/SharedParam;
.super Ljava/lang/Object;
.source "SharedParam.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field protected isValid:Z

.field protected package_name:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/epson/iprint/shared/SharedParam;->isValid:Z

    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParam;->package_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPackage_name()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParam;->package_name:Ljava/lang/String;

    return-object v0
.end method

.method public isAvailable()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/epson/iprint/shared/SharedParam;->isValid:Z

    return v0
.end method

.method protected abstract setParam(Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/iprint/shared/SharedDataException;
        }
    .end annotation
.end method

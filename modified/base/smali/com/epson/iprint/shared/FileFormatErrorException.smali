.class Lcom/epson/iprint/shared/FileFormatErrorException;
.super Lcom/epson/iprint/shared/SharedDataException;
.source "SharedDataException.java"


# instance fields
.field private fileName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "\u30d5\u30a1\u30a4\u30eb\u30d5\u30a9\u30fc\u30de\u30c3\u30c8\u30a8\u30e9\u30fc"

    .line 69
    invoke-direct {p0, v0}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/epson/iprint/shared/SharedDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/epson/iprint/shared/FileFormatErrorException;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/epson/iprint/shared/FileFormatErrorException;->fileName:Ljava/lang/String;

    return-void
.end method

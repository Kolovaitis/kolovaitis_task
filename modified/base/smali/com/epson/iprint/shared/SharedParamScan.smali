.class public Lcom/epson/iprint/shared/SharedParamScan;
.super Lcom/epson/iprint/shared/SharedParam;
.source "SharedParamScan.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x192096777f1fd8b1L


# instance fields
.field private arrayOutFilePath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private file_name:Ljava/lang/String;

.field private folder_name:Ljava/lang/String;

.field private pixel_main:I

.field private pixel_sub:I

.field private res_main:I

.field private res_sub:I

.field private scan_mode:I

.field private scan_type:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 9
    invoke-direct {p0}, Lcom/epson/iprint/shared/SharedParam;-><init>()V

    const/4 v0, 0x0

    .line 33
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->scan_type:I

    .line 34
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->pixel_main:I

    .line 35
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->pixel_sub:I

    .line 36
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->res_main:I

    .line 37
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->res_sub:I

    const/4 v1, 0x0

    .line 38
    iput-object v1, p0, Lcom/epson/iprint/shared/SharedParamScan;->folder_name:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lcom/epson/iprint/shared/SharedParamScan;->file_name:Ljava/lang/String;

    .line 40
    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->scan_mode:I

    .line 43
    iput-object v1, p0, Lcom/epson/iprint/shared/SharedParamScan;->arrayOutFilePath:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public clearArrayOutFilePath()V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->arrayOutFilePath:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getArrayOutFilePath()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->arrayOutFilePath:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFile_name()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->file_name:Ljava/lang/String;

    return-object v0
.end method

.method public getFolder_name()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->folder_name:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getPackage_name()Ljava/lang/String;
    .locals 1

    .line 9
    invoke-super {p0}, Lcom/epson/iprint/shared/SharedParam;->getPackage_name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPixel_main()I
    .locals 1

    .line 78
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->pixel_main:I

    return v0
.end method

.method public getPixel_sub()I
    .locals 1

    .line 82
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->pixel_sub:I

    return v0
.end method

.method public getRes_main()I
    .locals 1

    .line 86
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->res_main:I

    return v0
.end method

.method public getRes_sub()I
    .locals 1

    .line 90
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->res_sub:I

    return v0
.end method

.method public getScan_mode()I
    .locals 1

    .line 102
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->scan_mode:I

    return v0
.end method

.method public getScan_type()I
    .locals 1

    .line 74
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->scan_type:I

    return v0
.end method

.method public bridge synthetic isAvailable()Z
    .locals 1

    .line 9
    invoke-super {p0}, Lcom/epson/iprint/shared/SharedParam;->isAvailable()Z

    move-result v0

    return v0
.end method

.method public setArrayOutFilePath(Ljava/lang/String;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->arrayOutFilePath:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected setParam(Landroid/os/Bundle;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/iprint/shared/SharedDataException;
        }
    .end annotation

    const-string v0, "SCAN_TYPE"

    .line 51
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->scan_type:I

    .line 52
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->scan_type:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    const/4 v2, 0x3

    if-gt v0, v2, :cond_1

    const-string v0, "PIXEL_MAIN"

    .line 56
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->pixel_main:I

    .line 57
    iget v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->pixel_main:I

    if-ltz v0, :cond_0

    const-string v0, "PIXEL_SUB"

    .line 62
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->pixel_sub:I

    const-string v0, "RES_MAIN"

    .line 63
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->res_main:I

    const-string v0, "RES_SUB"

    .line 64
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->res_sub:I

    const-string v0, "FOLDER_NAME"

    .line 65
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->folder_name:Ljava/lang/String;

    const-string v0, "FILE_NAME"

    .line 66
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->file_name:Ljava/lang/String;

    const-string v0, "SCAN_MODE"

    .line 67
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/iprint/shared/SharedParamScan;->scan_mode:I

    const-string v0, "PACKAGE_NAME"

    .line 68
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/shared/SharedParamScan;->package_name:Ljava/lang/String;

    .line 69
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/iprint/shared/SharedParamScan;->arrayOutFilePath:Ljava/util/ArrayList;

    .line 70
    iput-boolean v1, p0, Lcom/epson/iprint/shared/SharedParamScan;->isValid:Z

    return-void

    .line 58
    :cond_0
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "PIXEL_MAIN errorr"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 53
    :cond_1
    new-instance p1, Lcom/epson/iprint/shared/ParametersErrorException;

    const-string v0, "SCAN_TYPE errorr"

    invoke-direct {p1, v0}, Lcom/epson/iprint/shared/ParametersErrorException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

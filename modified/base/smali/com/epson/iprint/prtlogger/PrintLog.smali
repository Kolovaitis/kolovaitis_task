.class public Lcom/epson/iprint/prtlogger/PrintLog;
.super Ljava/lang/Object;
.source "PrintLog.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final ACTION_COPY:I = 0x2004

.field public static final ACTION_EXTERNAL_APP_SCAN:I = 0x1101

.field public static final ACTION_ID_MEMORY_CARD_COPY_TO_MOBILE_DEVICE:I = 0x2002

.field public static final ACTION_ID_MEMORY_CARD_COPY_TO_PRINTER:I = 0x2001

.field public static final ACTION_PHOTO_TRANSFER:I = 0x2003

.field public static final ACTION_REPEAT_COPY:I = 0x2006

.field public static final ACTION_SCAN:I = 0x2005

.field public static final PREVIEW_TYPE_DOCUMENT:I = 0x2

.field public static final PREVIEW_TYPE_NOT_SET:I = 0x0

.field public static final PREVIEW_TYPE_PHOTO:I = 0x1

.field public static final PREVIEW_TYPE_WEB:I = 0x3

.field public static final PRINT_SOURCE_BOX:I = 0x103

.field public static final PRINT_SOURCE_CAMERACOPY:I = 0x5

.field public static final PRINT_SOURCE_DOCUMENT:I = 0x2

.field public static final PRINT_SOURCE_DROPBOX:I = 0x102

.field public static final PRINT_SOURCE_EVERNOTE:I = 0x100

.field public static final PRINT_SOURCE_EXTERNAL_APP_DOCUMENT:I = 0x1002

.field public static final PRINT_SOURCE_EXTERNAL_APP_PHOTO:I = 0x1001

.field public static final PRINT_SOURCE_EXTERNAL_APP_WEB:I = 0x1004

.field public static final PRINT_SOURCE_GOOGLEDRIVE:I = 0x101

.field public static final PRINT_SOURCE_MYPOCKET:I = 0x105

.field public static final PRINT_SOURCE_ONEDRIVE:I = 0x104

.field public static final PRINT_SOURCE_PHOTO:I = 0x1

.field public static final PRINT_SOURCE_SCAN_PRINT:I = 0x4

.field public static final PRINT_SOURCE_WEB:I = 0x3


# instance fields
.field public callerPackage:Ljava/lang/String;

.field public originalFileExtension:Ljava/lang/String;

.field public previewType:I

.field public uiRoute:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFileExtension(Ljava/io/File;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 74
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x2e

    .line 75
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-ltz v0, :cond_1

    .line 76
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 80
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 89
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/PrintLog;->getFileExtension(Ljava/io/File;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

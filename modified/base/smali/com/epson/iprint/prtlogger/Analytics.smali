.class public Lcom/epson/iprint/prtlogger/Analytics;
.super Ljava/lang/Object;
.source "Analytics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;,
        Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;
    }
.end annotation


# static fields
.field public static final ACTION_ID_HOME_BUY_INK:Ljava/lang/String; = "Home-BuyInk"

.field public static final ACTION_ID_HOME_CAMERACOPY:Ljava/lang/String; = "Home-CameraCopy"

.field public static final ACTION_ID_HOME_COPY:Ljava/lang/String; = "Home-Copy"

.field public static final ACTION_ID_HOME_DOCUMENT:Ljava/lang/String; = "Home-Document"

.field public static final ACTION_ID_HOME_INFO:Ljava/lang/String; = "Home-Info"

.field public static final ACTION_ID_HOME_MAINTAIN:Ljava/lang/String; = "Home-Maintain"

.field public static final ACTION_ID_HOME_MEMORYCARD_ACCESS:Ljava/lang/String; = "Home-MemoryCardAccess"

.field public static final ACTION_ID_HOME_ONLINE_STORAGE:Ljava/lang/String; = "Home-OnlineStorage"

.field public static final ACTION_ID_HOME_PHOTO:Ljava/lang/String; = "Home-Photo"

.field public static final ACTION_ID_HOME_PHOTO_TRANSFER:Ljava/lang/String; = "Home-PhotoTransfer"

.field public static final ACTION_ID_HOME_READY_INK:Ljava/lang/String; = "Home-ReadyInk"

.field public static final ACTION_ID_HOME_SCAN:Ljava/lang/String; = "Home-Scan"

.field public static final ACTION_ID_HOME_WEB:Ljava/lang/String; = "Home-Web"

.field public static final ACTION_ID_MAINTENANCE_BUY_INK:Ljava/lang/String; = "Maintenance-BuyInk"

.field public static final ACTION_ID_MAINTENANCE_FIRMWARE_UPDATE:Ljava/lang/String; = "Firmware Update"

.field public static final ACTION_ID_MAINTENANCE_HEAD_CLEANING:Ljava/lang/String; = "HeadCleaning"

.field public static final ACTION_ID_MAINTENANCE_NOZZLE_CHECK:Ljava/lang/String; = "NozzlleCheck"

.field public static final ACTION_ID_MAINTENANCE_READY_INK:Ljava/lang/String; = "Maintenance-ReadyInk"

.field public static final ACTION_ID_SETUP_BLE:Ljava/lang/String; = "PrinterSetup-BLE"

.field public static final CONNECTION_TYPE_NO_CONNECTION_TYPE:I = -0x1

.field public static final CONNECTION_TYPE_OTG:I = 0xb

.field public static final CONNECTION_TYPE_REMOTE:I = 0xa

.field private static final CUSTOM_DIMENSION_BLE_PATH:I = 0x1

.field private static final CUSTOM_DIMENSION_BLE_RESULT:I = 0x2

.field private static final CUSTOM_DIMENSION_DEBUG:I = 0x1c

.field private static final CUSTOM_DIMENSION_DEBUG_VALUE:Ljava/lang/String; = "develop"

.field public static final EXTENSION_STRING_WEB:Ljava/lang/String; = "http"

.field public static final SCREEN_ID_HOME:Ljava/lang/String; = "Home"

.field private static final TAG:Ljava/lang/String; = "Analytics"

.field public static final WIFI_DIRECT_TYPE_NORMAL_WIFI_DIRECT:I = 0x1

.field public static final WIFI_DIRECT_TYPE_NOT_WIFI_DIRECT:I = 0x0

.field public static final WIFI_DIRECT_TYPE_SIMPLE_AP:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cleanPrintNumber(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 552
    invoke-static {p0, v0}, Lcom/epson/iprint/prtlogger/AnalyticsPreferences;->savePrintNumber(Landroid/content/Context;I)V

    return-void
.end method

.method public static countScreen(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 572
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    const-string p0, "Analytics"

    const-string p1, "ERROR: countScreen() screenId == null"

    .line 577
    invoke-static {p0, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 581
    :cond_1
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object p0

    invoke-virtual {p0}, Lepson/print/IprintApplication;->getDefaultTracker()Lcom/google/android/gms/analytics/Tracker;

    move-result-object p0

    .line 582
    invoke-virtual {p0, p1}, Lcom/google/android/gms/analytics/Tracker;->setScreenName(Ljava/lang/String;)V

    .line 583
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->createScreenViewBuilder()Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    move-result-object p1

    .line 584
    invoke-virtual {p1}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;->build()Ljava/util/Map;

    move-result-object p1

    .line 583
    invoke-virtual {p0, p1}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    return-void
.end method

.method private static createEventBuilder()Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
    .locals 3

    .line 631
    new-instance v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    .line 632
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->isAnalyticsDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x1c

    const-string v2, "develop"

    .line 633
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    :cond_0
    return-object v0
.end method

.method private static createScreenViewBuilder()Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;
    .locals 3

    .line 640
    new-instance v0, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;-><init>()V

    .line 641
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->isAnalyticsDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x1c

    const-string v2, "develop"

    .line 642
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    :cond_0
    return-object v0
.end method

.method private static getActionString(I)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x1004

    if-eq p0, v0, :cond_1

    const/16 v0, 0x1101

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    packed-switch p0, :pswitch_data_2

    packed-switch p0, :pswitch_data_3

    const-string p0, ""

    return-object p0

    :pswitch_0
    const-string p0, "RepeatCopy"

    return-object p0

    :pswitch_1
    const-string p0, "Scan"

    return-object p0

    :pswitch_2
    const-string p0, "Copy"

    return-object p0

    :pswitch_3
    const-string p0, "Send Photo"

    return-object p0

    :pswitch_4
    const-string p0, "Memory Access"

    return-object p0

    :pswitch_5
    const-string p0, "Intent-Documents"

    return-object p0

    :pswitch_6
    const-string p0, "Intent-Photos"

    return-object p0

    :pswitch_7
    const-string p0, "MyPocket"

    return-object p0

    :pswitch_8
    const-string p0, "OneDrive"

    return-object p0

    :pswitch_9
    const-string p0, "Box"

    return-object p0

    :pswitch_a
    const-string p0, "Dropbox"

    return-object p0

    :pswitch_b
    const-string p0, "Google Drive"

    return-object p0

    :pswitch_c
    const-string p0, "Evernote"

    return-object p0

    :pswitch_d
    const-string p0, "Document Capture"

    return-object p0

    :pswitch_e
    const-string p0, "Scan-Print"

    return-object p0

    :pswitch_f
    const-string p0, "Web"

    return-object p0

    :pswitch_10
    const-string p0, "Saved Documents"

    return-object p0

    :pswitch_11
    const-string p0, "Photos"

    return-object p0

    :cond_0
    const-string p0, "Intent-Scan"

    return-object p0

    :cond_1
    const-string p0, "Intent-Web"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x100
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1001
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2001
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getActionString(Lcom/epson/iprint/prtlogger/PrintLog;)Ljava/lang/String;
    .locals 0

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    .line 264
    :cond_0
    iget p0, p0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->getActionString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getConnectionString(Landroid/content/Context;)Ljava/lang/String;
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 237
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/AnalyticsPreferences;->getConnectionPath(Landroid/content/Context;)I

    move-result p0

    .line 238
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionTypeString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getConnectionType(Landroid/content/Context;)I
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 556
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 p0, 0xa

    return p0

    .line 560
    :cond_0
    invoke-static {p0}, Lepson/print/MyPrinter;->isPrinterRouteOtg(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 p0, 0xb

    return p0

    .line 564
    :cond_1
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->getWifiDirectTypeIprintV4(Landroid/content/Context;)I

    move-result p0

    return p0
.end method

.method private static getConnectionTypeString(I)Ljava/lang/String;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    const-string p0, ""

    return-object p0

    :pswitch_0
    const-string p0, "OTG"

    return-object p0

    :pswitch_1
    const-string p0, "Remote"

    return-object p0

    :pswitch_2
    const-string p0, "SimpleAP"

    return-object p0

    :pswitch_3
    const-string p0, "Wi-Fi Direct"

    return-object p0

    :pswitch_4
    const-string p0, "Infra"

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getDateString(I)Ljava/lang/String;
    .locals 0

    if-nez p0, :cond_0

    const-string p0, "OFF"

    goto :goto_0

    :cond_0
    const-string p0, "ON"

    :goto_0
    return-object p0
.end method

.method public static getDefaultPrinterName(Landroid/content/Context;)Ljava/lang/String;
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 231
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p0

    .line 232
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getLayoutValue(Lepson/print/screen/PrintSetting;)I
    .locals 1

    .line 186
    iget v0, p0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    if-eqz v0, :cond_3

    const/high16 p0, 0x10000

    if-eq v0, p0, :cond_2

    const/high16 p0, 0x20000

    if-eq v0, p0, :cond_1

    const/high16 p0, 0x40000

    if-eq v0, p0, :cond_0

    const/4 p0, 0x2

    goto :goto_0

    :cond_0
    const/16 p0, 0x400

    goto :goto_0

    :cond_1
    const/16 p0, 0x200

    goto :goto_0

    :cond_2
    const/16 p0, 0x100

    goto :goto_0

    .line 205
    :cond_3
    iget p0, p0, Lepson/print/screen/PrintSetting;->layoutValue:I

    :goto_0
    return p0
.end method

.method private static getOnOff(Z)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    :goto_0
    return-object p0
.end method

.method private static getScanColorTypeString(I)Ljava/lang/String;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const-string p0, ""

    return-object p0

    :pswitch_0
    const-string p0, "Monochrome"

    return-object p0

    :pswitch_1
    const-string p0, "Grayscale"

    return-object p0

    :pswitch_2
    const-string p0, "Color"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getScanDeviceString(I)Ljava/lang/String;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const-string p0, ""

    return-object p0

    :pswitch_0
    const-string p0, "ADF"

    return-object p0

    :pswitch_1
    const-string p0, "DocumentTable"

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getScanGammaString(I)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    const-string p0, "1"

    return-object p0

    :cond_0
    const-string p0, "0"

    return-object p0
.end method

.method static getSizeLimitString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    .line 221
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le p1, v0, :cond_1

    move p1, v0

    :cond_1
    const/4 v0, 0x0

    .line 227
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getWifiDirectTypeIprintV4(Landroid/content/Context;)I
    .locals 2

    .line 673
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isWifiDirectFY13(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 676
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isWifiDirectP2P(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 679
    :cond_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x2

    return p0

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method private static isAnalyticsDebug()Z
    .locals 2

    .line 651
    invoke-static {}, Lepson/print/IprintApplication;->isConnectStaging()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static savePrintInfo(Landroid/content/Context;I)V
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 541
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  @@@@@@@@@@@@@     printSheet = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 542
    invoke-static {p0, p1}, Lcom/epson/iprint/prtlogger/AnalyticsPreferences;->savePrintNumber(Landroid/content/Context;I)V

    .line 544
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result p1

    .line 545
    invoke-static {p0, p1}, Lcom/epson/iprint/prtlogger/AnalyticsPreferences;->saveConnectionPath(Landroid/content/Context;I)V

    return-void
.end method

.method public static sendAction(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 591
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    const-string p0, "Analytics"

    const-string p1, "ERROR: sendAction() actionId == null"

    .line 596
    invoke-static {p0, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 600
    :cond_1
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object p0

    invoke-virtual {p0}, Lepson/print/IprintApplication;->getDefaultTracker()Lcom/google/android/gms/analytics/Tracker;

    move-result-object p0

    .line 601
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->createEventBuilder()Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    const-string v1, "tap"

    .line 602
    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCategory(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    .line 603
    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setAction(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object p1

    .line 604
    invoke-virtual {p1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object p1

    .line 601
    invoke-virtual {p0, p1}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    return-void
.end method

.method public static sendCommonLog(Landroid/content/Context;Lcom/epson/iprint/prtlogger/CommonLog;)V
    .locals 1

    .line 476
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    if-nez p0, :cond_1

    goto :goto_0

    .line 483
    :cond_1
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->createEventBuilder()Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object p0

    .line 484
    invoke-static {p1, p0}, Lcom/epson/iprint/prtlogger/Analytics;->setCommonData(Lcom/epson/iprint/prtlogger/CommonLog;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    .line 486
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->sendData(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method public static sendCopyLog(Landroid/content/Context;Lepson/print/copy/Component/ecopycomponent/CopyParams;Lcom/epson/iprint/prtlogger/CommonLog;)V
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    .line 404
    :cond_0
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_1

    return-void

    .line 408
    :cond_1
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->createEventBuilder()Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object p0

    .line 409
    invoke-static {p2, p0}, Lcom/epson/iprint/prtlogger/Analytics;->setCommonData(Lcom/epson/iprint/prtlogger/CommonLog;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    .line 410
    invoke-static {p1, p0}, Lcom/epson/iprint/prtlogger/Analytics;->setCopyData(Lepson/print/copy/Component/ecopycomponent/CopyParams;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    .line 412
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->sendData(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    return-void
.end method

.method private static sendData(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V
    .locals 1
    .param p0    # Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 511
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/IprintApplication;->getDefaultTracker()Lcom/google/android/gms/analytics/Tracker;

    move-result-object v0

    .line 513
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    return-void
.end method

.method private static sendDataIfNonNull(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;ILjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    .line 468
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    :cond_0
    return-void
.end method

.method public static sendPrintLog(Landroid/content/Context;Lepson/print/screen/PrintProgress$ProgressParams;)V
    .locals 6
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lepson/print/screen/PrintProgress$ProgressParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 112
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 116
    :cond_0
    invoke-interface {p1}, Lepson/print/screen/PrintProgress$ProgressParams;->getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 118
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    .line 121
    :cond_1
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/AnalyticsPreferences;->getPrintNumber(Landroid/content/Context;)I

    move-result v1

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    **************     "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-gtz v1, :cond_2

    return-void

    .line 127
    :cond_2
    invoke-interface {p1, p0}, Lepson/print/screen/PrintProgress$ProgressParams;->getPrintSetting(Landroid/content/Context;)Lepson/print/screen/PrintSetting;

    move-result-object v2

    .line 128
    invoke-virtual {v2}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 130
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->createEventBuilder()Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v3

    const-string v4, "function"

    .line 131
    invoke-virtual {v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCategory(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v3

    .line 132
    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getActionString(Lcom/epson/iprint/prtlogger/PrintLog;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setAction(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v3

    iget v4, v2, Lepson/print/screen/PrintSetting;->copiesValue:I

    mul-int v1, v1, v4

    int-to-long v4, v1

    .line 133
    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setValue(J)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v1

    .line 135
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->getDefaultPrinterName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 137
    invoke-virtual {v1, v3}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setLabel(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    .line 141
    :cond_3
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    const/4 v3, 0x2

    .line 143
    invoke-virtual {v1, v3, p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/4 v3, 0x3

    iget v4, v2, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 144
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/4 v3, 0x4

    iget v4, v2, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 145
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/4 v3, 0x5

    iget v4, v2, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 146
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/4 v3, 0x6

    .line 147
    invoke-static {v2}, Lcom/epson/iprint/prtlogger/Analytics;->getLayoutValue(Lepson/print/screen/PrintSetting;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/4 v3, 0x7

    iget v4, v2, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 148
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/16 v3, 0x8

    iget v4, v2, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 149
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/16 v3, 0x9

    iget v4, v2, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 150
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 153
    iget p0, v0, Lcom/epson/iprint/prtlogger/PrintLog;->previewType:I

    const/4 v3, 0x1

    if-ne p0, v3, :cond_5

    .line 154
    invoke-interface {p1}, Lepson/print/screen/PrintProgress$ProgressParams;->getApfMode()Z

    move-result p0

    if-eqz p0, :cond_4

    const-string p0, "ON"

    goto :goto_0

    :cond_4
    const-string p0, "OFF"

    :goto_0
    const/16 p1, 0xb

    .line 155
    invoke-virtual {v1, p1, p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/16 p1, 0xa

    iget v2, v2, Lepson/print/screen/PrintSetting;->printdate:I

    .line 157
    invoke-static {v2}, Lcom/epson/iprint/prtlogger/Analytics;->getDateString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 161
    :cond_5
    iget-object p0, v0, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    const/16 p1, 0x96

    invoke-static {p0, p1}, Lcom/epson/iprint/prtlogger/Analytics;->getSizeLimitString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    .line 162
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 163
    invoke-virtual {v1, v3, p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 167
    :cond_6
    iget-object p0, v0, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/epson/iprint/prtlogger/Analytics;->getSizeLimitString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    .line 168
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_7

    const/16 p1, 0xc

    .line 169
    invoke-virtual {v1, p1, p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 172
    :cond_7
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object p0

    invoke-virtual {p0}, Lepson/print/IprintApplication;->getDefaultTracker()Lcom/google/android/gms/analytics/Tracker;

    move-result-object p0

    .line 174
    invoke-virtual {v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    return-void
.end method

.method public static sendRepeatCopyLog(Landroid/content/Context;Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;Lcom/epson/iprint/prtlogger/CommonLog;)V
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/iprint/prtlogger/CommonLog;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    .line 438
    :cond_0
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_1

    return-void

    .line 442
    :cond_1
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->createEventBuilder()Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object p0

    .line 443
    invoke-static {p2, p0}, Lcom/epson/iprint/prtlogger/Analytics;->setCommonData(Lcom/epson/iprint/prtlogger/CommonLog;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    .line 444
    invoke-static {p1, p0}, Lcom/epson/iprint/prtlogger/Analytics;->setRepeatCopyData(Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    .line 446
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->sendData(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    return-void
.end method

.method public static sendScanI1Log(Landroid/content/Context;Lepson/scan/lib/I1ScanParams;Lcom/epson/iprint/prtlogger/CommonLog;Z)V
    .locals 0

    if-eqz p2, :cond_2

    if-eqz p0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    .line 339
    :cond_0
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_1

    return-void

    .line 343
    :cond_1
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->createEventBuilder()Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object p0

    .line 344
    invoke-static {p2, p0}, Lcom/epson/iprint/prtlogger/Analytics;->setCommonData(Lcom/epson/iprint/prtlogger/CommonLog;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    .line 345
    invoke-static {p1, p0, p3}, Lcom/epson/iprint/prtlogger/Analytics;->setScanDataFromI1(Lepson/scan/lib/I1ScanParams;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;Z)V

    .line 347
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->sendData(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method public static sendSetup(Ljava/lang/String;Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;)V
    .locals 2

    .line 616
    invoke-static {}, Lcom/epson/iprint/prtlogger/Analytics;->createEventBuilder()Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    const-string v1, "setup"

    .line 617
    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCategory(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    const-string v1, "PrinterSetup-BLE"

    .line 618
    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setAction(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    .line 619
    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setLabel(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object p0

    .line 621
    invoke-virtual {p1}, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 622
    invoke-virtual {p2}, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x2

    invoke-virtual {p0, p2, p1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 625
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/IprintApplication;->getDefaultTracker()Lcom/google/android/gms/analytics/Tracker;

    move-result-object p1

    .line 626
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    return-void
.end method

.method private static setCommonData(Lcom/epson/iprint/prtlogger/CommonLog;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V
    .locals 3
    .param p0    # Lcom/epson/iprint/prtlogger/CommonLog;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "function"

    .line 491
    invoke-virtual {p1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCategory(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    iget v1, p0, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 492
    invoke-static {v1}, Lcom/epson/iprint/prtlogger/Analytics;->getActionString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setAction(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    iget v1, p0, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    int-to-long v1, v1

    .line 493
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setValue(J)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    .line 494
    iget v0, p0, Lcom/epson/iprint/prtlogger/CommonLog;->connectionType:I

    if-ltz v0, :cond_0

    const/4 v1, 0x2

    .line 496
    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionTypeString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/epson/iprint/prtlogger/CommonLog;->printerName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lcom/epson/iprint/prtlogger/CommonLog;->printerName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setLabel(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    .line 503
    :cond_1
    iget-object v0, p0, Lcom/epson/iprint/prtlogger/CommonLog;->callerPackage:Ljava/lang/String;

    if-eqz v0, :cond_2

    const/16 v0, 0xc

    .line 504
    iget-object v1, p0, Lcom/epson/iprint/prtlogger/CommonLog;->callerPackage:Ljava/lang/String;

    const/16 v2, 0x96

    invoke-static {v1, v2}, Lcom/epson/iprint/prtlogger/Analytics;->getSizeLimitString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 507
    :cond_2
    invoke-static {p0, p1}, Lcom/epson/iprint/prtlogger/Analytics;->setDirectionData(Lcom/epson/iprint/prtlogger/CommonLog;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V

    return-void
.end method

.method private static setCopyData(Lepson/print/copy/Component/ecopycomponent/CopyParams;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V
    .locals 2
    .param p0    # Lepson/print/copy/Component/ecopycomponent/CopyParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 417
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->colorMode:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->density:Ljava/lang/String;

    const/16 v1, 0x15

    .line 418
    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->magnification:Ljava/lang/String;

    const/16 v1, 0x16

    .line 419
    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->paperSize:Ljava/lang/String;

    const/16 v1, 0x17

    .line 420
    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->paperType:Ljava/lang/String;

    const/16 v1, 0x18

    .line 421
    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->printDevice:Ljava/lang/String;

    const/16 v1, 0x19

    .line 422
    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->copyType:Ljava/lang/String;

    const/16 v1, 0x1a

    .line 423
    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object p0, p0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->copyQuality:Ljava/lang/String;

    const/16 v0, 0x1b

    .line 424
    invoke-virtual {p1, v0, p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    return-void
.end method

.method private static setDirectionData(Lcom/epson/iprint/prtlogger/CommonLog;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V
    .locals 1

    .line 522
    iget p0, p0, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    packed-switch p0, :pswitch_data_0

    return-void

    :pswitch_0
    const-string p0, "toSmartPhone"

    goto :goto_0

    :pswitch_1
    const-string p0, "toPrinter"

    :goto_0
    const/4 v0, 0x1

    .line 533
    invoke-virtual {p1, v0, p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    return-void

    :pswitch_data_0
    .packed-switch 0x2001
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static setRepeatCopyData(Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;)V
    .locals 3
    .param p0    # Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 451
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->colorMode:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->density:Ljava/lang/String;

    const/16 v2, 0x1f

    .line 452
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->repeatLayout:Ljava/lang/String;

    const/16 v2, 0x20

    .line 453
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->paperSize:Ljava/lang/String;

    const/16 v2, 0x21

    .line 454
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->paperType:Ljava/lang/String;

    const/16 v2, 0x22

    .line 455
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->printDevice:Ljava/lang/String;

    const/16 v2, 0x23

    .line 456
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->copyQuality:Ljava/lang/String;

    const/16 v2, 0x24

    .line 457
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->removeBackground:Ljava/lang/String;

    const/16 v2, 0x25

    .line 458
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    .line 460
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->XCutLine:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-static {p1, v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendDataIfNonNull(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;ILjava/lang/String;)V

    .line 461
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->XCutLineStyle:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-static {p1, v1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendDataIfNonNull(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;ILjava/lang/String;)V

    .line 462
    iget-object p0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->XCutLineWeight:Ljava/lang/String;

    const/16 v0, 0x28

    invoke-static {p1, v0, p0}, Lcom/epson/iprint/prtlogger/Analytics;->sendDataIfNonNull(Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;ILjava/lang/String;)V

    return-void
.end method

.method private static setScanDataFromI1(Lepson/scan/lib/I1ScanParams;Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;Z)V
    .locals 2
    .param p0    # Lepson/scan/lib/I1ScanParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 354
    iget v0, p0, Lepson/scan/lib/I1ScanParams;->inputDevice:I

    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getScanDeviceString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget v0, p0, Lepson/scan/lib/I1ScanParams;->colorType:I

    .line 355
    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getScanColorTypeString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget v0, p0, Lepson/scan/lib/I1ScanParams;->resolution:I

    .line 356
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    iget v0, p0, Lepson/scan/lib/I1ScanParams;->twoSide:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 357
    :goto_0
    invoke-static {v0}, Lcom/epson/iprint/prtlogger/Analytics;->getOnOff(Z)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/16 v0, 0x12

    iget v1, p0, Lepson/scan/lib/I1ScanParams;->gamma:I

    .line 358
    invoke-static {v1}, Lcom/epson/iprint/prtlogger/Analytics;->getScanGammaString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/16 v0, 0x13

    iget p0, p0, Lepson/scan/lib/I1ScanParams;->brightness:I

    .line 359
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v0, p0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    const/16 p1, 0x29

    .line 360
    invoke-static {p2}, Lcom/epson/iprint/prtlogger/Analytics;->getOnOff(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCustomDimension(ILjava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$HitBuilder;

    return-void
.end method

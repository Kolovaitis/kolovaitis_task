.class public Lcom/epson/iprint/prtlogger/NewLoggerController;
.super Ljava/lang/Object;
.source "NewLoggerController.java"


# static fields
.field private static final CURRENT_USER_SURVEY_DOCUMENT_VERSION:I = 0xeb8c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkAnalyticsInvitationAndDeleteOldData(Landroid/content/Context;)Z
    .locals 0
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 33
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->doesAnsweredCurrentInvitation(Landroid/content/Context;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method public static doesAnsweredCurrentInvitation(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 44
    invoke-static {}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getInstance()Lcom/epson/iprint/prtlogger/LoggerRecord;

    move-result-object v0

    .line 45
    invoke-virtual {v0, p0}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getInvitationVersion(Landroid/content/Context;)I

    move-result p0

    const v0, 0xeb8c

    if-lt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static doesAnsweredInvitation(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 54
    invoke-static {}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getInstance()Lcom/epson/iprint/prtlogger/LoggerRecord;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p0}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getInvitationVersion(Landroid/content/Context;)I

    move-result p0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isLoggerEnabled(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 75
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->doesAnsweredCurrentInvitation(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 81
    :cond_0
    invoke-static {}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getInstance()Lcom/epson/iprint/prtlogger/LoggerRecord;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getAnswer(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method public static setAnswer(Landroid/content/Context;Z)V
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 63
    invoke-static {}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getInstance()Lcom/epson/iprint/prtlogger/LoggerRecord;

    move-result-object v0

    .line 64
    invoke-virtual {v0, p0, p1}, Lcom/epson/iprint/prtlogger/LoggerRecord;->setAnswer(Landroid/content/Context;Z)V

    const v1, 0xeb8c

    .line 65
    invoke-virtual {v0, p0, v1}, Lcom/epson/iprint/prtlogger/LoggerRecord;->setInvitationVersion(Landroid/content/Context;I)V

    .line 67
    invoke-static {p0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object p0

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/analytics/GoogleAnalytics;->setAppOptOut(Z)V

    return-void
.end method

.method public static stopLoggerIfNotAgreed(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 89
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->isLoggerEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    invoke-static {p0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object p0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->setAppOptOut(Z)V

    :cond_0
    return-void
.end method

.class public final enum Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;
.super Ljava/lang/Enum;
.source "Analytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/prtlogger/Analytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SetUpPath"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

.field public static final enum Button:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

.field public static final enum Home:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 87
    new-instance v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    const-string v1, "Home"

    const-string v2, "HomePopup"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->Home:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    .line 88
    new-instance v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    const-string v1, "Button"

    const-string v2, "SetUpButton"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->Button:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    const/4 v0, 0x2

    .line 86
    new-array v0, v0, [Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    sget-object v1, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->Home:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->Button:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    aput-object v1, v0, v4

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->$VALUES:[Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 92
    iput-object p3, p0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->name:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;
    .locals 1

    .line 86
    const-class v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    return-object p0
.end method

.method public static values()[Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;
    .locals 1

    .line 86
    sget-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->$VALUES:[Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    invoke-virtual {v0}, [Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->name:Ljava/lang/String;

    return-object v0
.end method

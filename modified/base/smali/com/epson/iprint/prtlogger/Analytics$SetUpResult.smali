.class public final enum Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;
.super Ljava/lang/Enum;
.source "Analytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/iprint/prtlogger/Analytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SetUpResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

.field public static final enum Error:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

.field public static final enum Error106:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

.field public static final enum Error119:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

.field public static final enum Fail:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

.field public static final enum Success:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 69
    new-instance v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    const-string v1, "Success"

    const-string v2, "Success"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Success:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    .line 70
    new-instance v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    const-string v1, "Fail"

    const-string v2, "Fail"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Fail:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    .line 71
    new-instance v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    const-string v1, "Error"

    const-string v2, "Error"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Error:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    .line 72
    new-instance v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    const-string v1, "Error106"

    const-string v2, "Error-106"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Error106:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    .line 73
    new-instance v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    const-string v1, "Error119"

    const-string v2, "Error-119"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Error119:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    const/4 v0, 0x5

    .line 68
    new-array v0, v0, [Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    sget-object v1, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Success:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Fail:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Error:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Error106:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Error119:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    aput-object v1, v0, v7

    sput-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->$VALUES:[Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput-object p3, p0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->name:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;
    .locals 1

    .line 68
    const-class v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    return-object p0
.end method

.method public static values()[Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;
    .locals 1

    .line 68
    sget-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->$VALUES:[Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    invoke-virtual {v0}, [Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->name:Ljava/lang/String;

    return-object v0
.end method

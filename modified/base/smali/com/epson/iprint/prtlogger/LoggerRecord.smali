.class public Lcom/epson/iprint/prtlogger/LoggerRecord;
.super Ljava/lang/Object;
.source "LoggerRecord.java"


# static fields
.field private static final ANALYTICS_PREFS_NAME:Ljava/lang/String; = "logger6_3"

.field private static final PREFS_KEY_LOGGER_ANSWER:Ljava/lang/String; = "answer"

.field private static final PREFS_KEY_LOGGER_VERSION:Ljava/lang/String; = "logger_version"

.field private static sLoggerRecord:Lcom/epson/iprint/prtlogger/LoggerRecord;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/epson/iprint/prtlogger/LoggerRecord;
    .locals 2

    const-class v0, Lcom/epson/iprint/prtlogger/LoggerRecord;

    monitor-enter v0

    .line 20
    :try_start_0
    sget-object v1, Lcom/epson/iprint/prtlogger/LoggerRecord;->sLoggerRecord:Lcom/epson/iprint/prtlogger/LoggerRecord;

    if-nez v1, :cond_0

    .line 21
    new-instance v1, Lcom/epson/iprint/prtlogger/LoggerRecord;

    invoke-direct {v1}, Lcom/epson/iprint/prtlogger/LoggerRecord;-><init>()V

    sput-object v1, Lcom/epson/iprint/prtlogger/LoggerRecord;->sLoggerRecord:Lcom/epson/iprint/prtlogger/LoggerRecord;

    .line 23
    :cond_0
    sget-object v1, Lcom/epson/iprint/prtlogger/LoggerRecord;->sLoggerRecord:Lcom/epson/iprint/prtlogger/LoggerRecord;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "logger6_3"

    const/4 v1, 0x0

    .line 68
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    return-object p1
.end method

.method static replaceInstance(Lcom/epson/iprint/prtlogger/LoggerRecord;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 31
    sput-object p0, Lcom/epson/iprint/prtlogger/LoggerRecord;->sLoggerRecord:Lcom/epson/iprint/prtlogger/LoggerRecord;

    return-void
.end method


# virtual methods
.method public getAnswer(Landroid/content/Context;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 63
    invoke-direct {p0, p1}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "answer"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public getInvitationVersion(Landroid/content/Context;)I
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 40
    invoke-direct {p0, p1}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "logger_version"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method public setAnswer(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 54
    invoke-direct {p0, p1}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "answer"

    .line 55
    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setInvitationVersion(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 49
    invoke-direct {p0, p1}, Lcom/epson/iprint/prtlogger/LoggerRecord;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "logger_version"

    .line 50
    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

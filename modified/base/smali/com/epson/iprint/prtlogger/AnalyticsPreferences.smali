.class public Lcom/epson/iprint/prtlogger/AnalyticsPreferences;
.super Ljava/lang/Object;
.source "AnalyticsPreferences.java"


# static fields
.field private static final PREFS_KEY_CONNECTION_PATH:Ljava/lang/String; = "connection_path"

.field private static final PREFS_KEY_ONLINE_STORAGE_TYPE:Ljava/lang/String; = "online_storage_type"

.field private static final PREFS_KEY_PRINT_NUMBER:Ljava/lang/String; = "print_num"

.field public static final PREFS_NAME:Ljava/lang/String; = "analytics_prefs"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getConnectionPath(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "analytics_prefs"

    .line 28
    invoke-static {p0, v0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    const-string v0, "connection_path"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getInt(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method private static getOnlineStorageType(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "analytics_prefs"

    .line 46
    invoke-static {p0, v0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    const-string v0, "online_storage_type"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getInt(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static getPrintNumber(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "analytics_prefs"

    .line 37
    invoke-static {p0, v0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    const-string v0, "print_num"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getInt(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static saveConnectionPath(Landroid/content/Context;I)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "analytics_prefs"

    .line 23
    invoke-static {p0, v0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    invoke-virtual {p0}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->edit()Lepson/provider/SharedPreferencesProvider$Editor;

    move-result-object p0

    const-string v0, "connection_path"

    invoke-virtual {p0, v0, p1}, Lepson/provider/SharedPreferencesProvider$Editor;->putInt(Ljava/lang/String;I)Lepson/provider/SharedPreferencesProvider$Editor;

    move-result-object p0

    const-string p1, "analytics_prefs"

    invoke-virtual {p0, p1}, Lepson/provider/SharedPreferencesProvider$Editor;->apply(Ljava/lang/String;)V

    return-void
.end method

.method public static saveOnlineStorageType(Landroid/content/Context;I)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "analytics_prefs"

    .line 41
    invoke-static {p0, v0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    invoke-virtual {p0}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->edit()Lepson/provider/SharedPreferencesProvider$Editor;

    move-result-object p0

    const-string v0, "online_storage_type"

    invoke-virtual {p0, v0, p1}, Lepson/provider/SharedPreferencesProvider$Editor;->putInt(Ljava/lang/String;I)Lepson/provider/SharedPreferencesProvider$Editor;

    move-result-object p0

    const-string p1, "analytics_prefs"

    invoke-virtual {p0, p1}, Lepson/provider/SharedPreferencesProvider$Editor;->apply(Ljava/lang/String;)V

    return-void
.end method

.method public static savePrintNumber(Landroid/content/Context;I)V
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    printNumber = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const-string v0, "analytics_prefs"

    .line 33
    invoke-static {p0, v0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    invoke-virtual {p0}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->edit()Lepson/provider/SharedPreferencesProvider$Editor;

    move-result-object p0

    const-string v0, "print_num"

    invoke-virtual {p0, v0, p1}, Lepson/provider/SharedPreferencesProvider$Editor;->putInt(Ljava/lang/String;I)Lepson/provider/SharedPreferencesProvider$Editor;

    move-result-object p0

    const-string p1, "analytics_prefs"

    invoke-virtual {p0, p1}, Lepson/provider/SharedPreferencesProvider$Editor;->apply(Ljava/lang/String;)V

    return-void
.end method

.class public Lcom/epson/iprint/prtlogger/CommonLog;
.super Ljava/lang/Object;
.source "CommonLog.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public action:I

.field public callerPackage:Ljava/lang/String;

.field public connectionType:I

.field public numberOfSheet:I

.field public printerName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setConnectionType(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 31
    invoke-static {p1}, Lcom/epson/iprint/prtlogger/Analytics;->getConnectionType(Landroid/content/Context;)I

    move-result p1

    iput p1, p0, Lcom/epson/iprint/prtlogger/CommonLog;->connectionType:I

    return-void
.end method

.method public setPrinterName(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 26
    invoke-static {p1}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/iprint/prtlogger/CommonLog;->printerName:Ljava/lang/String;

    return-void
.end method

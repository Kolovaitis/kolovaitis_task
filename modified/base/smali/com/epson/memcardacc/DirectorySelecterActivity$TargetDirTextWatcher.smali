.class Lcom/epson/memcardacc/DirectorySelecterActivity$TargetDirTextWatcher;
.super Ljava/lang/Object;
.source "DirectorySelecterActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/DirectorySelecterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TargetDirTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/memcardacc/DirectorySelecterActivity;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/DirectorySelecterActivity;)V
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity$TargetDirTextWatcher;->this$0:Lcom/epson/memcardacc/DirectorySelecterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 140
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-gtz p1, :cond_0

    .line 141
    iget-object p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity$TargetDirTextWatcher;->this$0:Lcom/epson/memcardacc/DirectorySelecterActivity;

    invoke-virtual {p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->disableOkButton()V

    goto :goto_0

    .line 143
    :cond_0
    iget-object p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity$TargetDirTextWatcher;->this$0:Lcom/epson/memcardacc/DirectorySelecterActivity;

    invoke-virtual {p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->enableOkButton()V

    :goto_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

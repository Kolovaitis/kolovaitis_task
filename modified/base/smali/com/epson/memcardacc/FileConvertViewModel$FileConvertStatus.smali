.class public Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;
.super Ljava/lang/Object;
.source "FileConvertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/FileConvertViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FileConvertStatus"
.end annotation


# instance fields
.field private final mConvertFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mConvertStatus:Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;

.field private final mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    sget-object v0, Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;->TASK_NOT_STARTED:Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;-><init>(Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;)V

    return-void
.end method

.method public constructor <init>(Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;)V
    .locals 2

    .line 32
    sget-object v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->NO_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;-><init>(Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;Lcom/epson/memcardacc/FileConvertTask$ErrorType;Ljava/util/ArrayList;)V

    return-void
.end method

.method public constructor <init>(Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;Lcom/epson/memcardacc/FileConvertTask$ErrorType;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;",
            "Lcom/epson/memcardacc/FileConvertTask$ErrorType;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    .line 38
    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->mConvertStatus:Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;

    .line 39
    iput-object p3, p0, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->mConvertFileList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getConvertFileList()Ljava/util/ArrayList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->mConvertFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getConvertStatus()Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->mConvertStatus:Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;

    return-object v0
.end method

.method public getErrorType()Lcom/epson/memcardacc/FileConvertTask$ErrorType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    return-object v0
.end method

.class final enum Lcom/epson/memcardacc/FileConvertTask$ErrorType;
.super Ljava/lang/Enum;
.source "FileConvertTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/FileConvertTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/memcardacc/FileConvertTask$ErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/memcardacc/FileConvertTask$ErrorType;

.field public static final enum CONVERT_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

.field public static final enum FILE_SIZE_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

.field public static final enum NO_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 20
    new-instance v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    const-string v1, "NO_ERROR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/memcardacc/FileConvertTask$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->NO_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    new-instance v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    const-string v1, "FILE_SIZE_ERROR"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/memcardacc/FileConvertTask$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->FILE_SIZE_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    new-instance v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    const-string v1, "CONVERT_ERROR"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/memcardacc/FileConvertTask$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->CONVERT_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    const/4 v0, 0x3

    .line 19
    new-array v0, v0, [Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    sget-object v1, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->NO_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->FILE_SIZE_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->CONVERT_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->$VALUES:[Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/memcardacc/FileConvertTask$ErrorType;
    .locals 1

    .line 19
    const-class v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    return-object p0
.end method

.method public static values()[Lcom/epson/memcardacc/FileConvertTask$ErrorType;
    .locals 1

    .line 19
    sget-object v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->$VALUES:[Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    invoke-virtual {v0}, [Lcom/epson/memcardacc/FileConvertTask$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    return-object v0
.end method

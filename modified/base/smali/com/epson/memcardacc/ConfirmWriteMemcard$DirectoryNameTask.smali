.class Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;
.super Landroid/os/AsyncTask;
.source "ConfirmWriteMemcard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/ConfirmWriteMemcard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DirectoryNameTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V
    .locals 0

    .line 627
    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 627
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 0

    .line 654
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-virtual {p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->cifsGetTargetFoldername()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 627
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    .line 633
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->setWriteDirName(Ljava/lang/String;)V

    goto :goto_0

    .line 635
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->setWriteDirName(Ljava/lang/String;)V

    .line 637
    :goto_0
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-static {p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->access$300(Lcom/epson/memcardacc/ConfirmWriteMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object p1

    const-string v0, "dialog_target_directy_name_task"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 643
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->access$300(Lcom/epson/memcardacc/ConfirmWriteMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_target_directy_name_task"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 0

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 627
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

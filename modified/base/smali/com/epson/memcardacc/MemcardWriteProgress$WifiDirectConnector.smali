.class Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;
.super Ljava/lang/Object;
.source "MemcardWriteProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/MemcardWriteProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WifiDirectConnector"
.end annotation


# instance fields
.field private isTryConnectSimpleAp:Z

.field private mActivity:Landroid/app/Activity;

.field private mCheckWifiDirect:Z

.field final synthetic this$0:Lcom/epson/memcardacc/MemcardWriteProgress;


# direct methods
.method public constructor <init>(Lcom/epson/memcardacc/MemcardWriteProgress;Landroid/app/Activity;Z)V
    .locals 0

    .line 374
    iput-object p1, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->this$0:Lcom/epson/memcardacc/MemcardWriteProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375
    iput-boolean p3, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->mCheckWifiDirect:Z

    .line 376
    iput-object p2, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->mActivity:Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public tryConnectWifiDirect()V
    .locals 3

    .line 383
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->mCheckWifiDirect:Z

    if-nez v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->this$0:Lcom/epson/memcardacc/MemcardWriteProgress;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardWriteProgress;->access$000(Lcom/epson/memcardacc/MemcardWriteProgress;)V

    return-void

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->mActivity:Landroid/app/Activity;

    const-string v1, "printer"

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 391
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 392
    :cond_1
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->isTryConnectSimpleAp:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 394
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->isTryConnectSimpleAp:Z

    .line 395
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->mActivity:Landroid/app/Activity;

    const-string v1, "printer"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 401
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->this$0:Lcom/epson/memcardacc/MemcardWriteProgress;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardWriteProgress;->access$000(Lcom/epson/memcardacc/MemcardWriteProgress;)V

    return-void
.end method

.method public tryDisconnectWifiDirect()V
    .locals 3

    .line 409
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->mCheckWifiDirect:Z

    if-nez v0, :cond_0

    return-void

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->mActivity:Landroid/app/Activity;

    const-string v1, "printer"

    .line 414
    invoke-static {v0}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 413
    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.class Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;
.super Ljava/lang/Object;
.source "MemcardCheckTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/MemcardCheckTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyQueueData"
.end annotation


# static fields
.field public static final TYPE_AUTH_DATA:I = 0x2

.field public static final TYPE_CANCEL:I = 0x1


# instance fields
.field public mDataType:I

.field public mSmbAuthInfo:Lcom/epson/memcardacc/SmbAuthInfo;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    iput p1, p0, Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;->mDataType:I

    return-void
.end method

.method public static getCancelData()Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;
    .locals 2

    .line 262
    new-instance v0, Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public isCancel()Z
    .locals 2

    .line 270
    iget v0, p0, Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;->mDataType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.class public Lcom/epson/memcardacc/PhotoCopyWriteProgress;
.super Lcom/epson/memcardacc/MemcardWriteProgress;
.source "PhotoCopyWriteProgress.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;-><init>()V

    return-void
.end method

.method private getLogData()Lcom/epson/iprint/prtlogger/CommonLog;
    .locals 2

    .line 83
    new-instance v0, Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/CommonLog;-><init>()V

    const/4 v1, 0x0

    .line 84
    iput v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    const/16 v1, 0x2003

    .line 85
    iput v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 86
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->getDefaultPrinterName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->printerName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected execCopyTask()V
    .locals 9

    .line 74
    new-instance v8, Lcom/epson/memcardacc/CopyTask;

    iget v2, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mCifsAccessStorageType:I

    iget v3, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mMemcardStorageType:I

    iget-object v4, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mImageList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mWriteFileList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mTargetFolderName:Ljava/lang/String;

    .line 75
    invoke-direct {p0}, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->getLogData()Lcom/epson/iprint/prtlogger/CommonLog;

    move-result-object v7

    move-object v0, v8

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/epson/memcardacc/CopyTask;-><init>(Lcom/epson/memcardacc/MemcardWriteProgress;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Lcom/epson/iprint/prtlogger/CommonLog;)V

    iput-object v8, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mCopyTask:Lcom/epson/memcardacc/CopyTask;

    .line 77
    iget-object v0, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mCopyTask:Lcom/epson/memcardacc/CopyTask;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/epson/memcardacc/CopyTask;->setCheck(ZZ)V

    .line 79
    iget-object v0, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mCopyTask:Lcom/epson/memcardacc/CopyTask;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/CopyTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 62
    invoke-super {p0, p1}, Lcom/epson/memcardacc/MemcardWriteProgress;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected taskCancel()V
    .locals 2

    const-string v0, "PhotoCopyWriteProgress"

    const-string v1, "taskCancel()"

    .line 67
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/epson/memcardacc/PhotoCopyWriteProgress;->mCopyTask:Lcom/epson/memcardacc/CopyTask;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CopyTask;->taskCancelByFileSizeZero()V

    return-void
.end method

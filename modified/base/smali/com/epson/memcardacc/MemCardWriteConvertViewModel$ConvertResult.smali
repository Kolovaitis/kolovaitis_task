.class public Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;
.super Ljava/lang/Object;
.source "MemCardWriteConvertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/MemCardWriteConvertViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConvertResult"
.end annotation


# instance fields
.field public final filenameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public totalFileSize:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;->filenameList:Ljava/util/ArrayList;

    const-wide/16 v0, 0x0

    .line 23
    iput-wide v0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;->totalFileSize:J

    return-void
.end method


# virtual methods
.method public addFile(Ljava/lang/String;)V
    .locals 5

    .line 27
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    iget-wide v1, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;->totalFileSize:J

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;->totalFileSize:J

    .line 29
    iget-object v0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;->filenameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

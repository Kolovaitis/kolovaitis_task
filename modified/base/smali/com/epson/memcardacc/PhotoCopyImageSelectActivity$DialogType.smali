.class Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;
.super Ljava/lang/Object;
.source "PhotoCopyImageSelectActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DialogType"
.end annotation


# static fields
.field public static final DIALOG_COPY_RESULT_CANCELED:I = 0x6f

.field public static final DIALOG_FILE_SIZE_OVER:I = 0x71

.field public static final DIALOG_GENERIC_ERROR:I = 0x70

.field public static final DIALOG_NO_DIALOG:I


# instance fields
.field private mDialogCode:I

.field public mErrorCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    iput p1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;->mDialogCode:I

    return-void
.end method


# virtual methods
.method public getDialogFragment(Landroid/content/Context;)Lcom/epson/memcardacc/AltAlertDialogFragment;
    .locals 3

    const-string v0, ""

    .line 232
    iget v1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;->mDialogCode:I

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0e03a9

    .line 267
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :pswitch_1
    iget v0, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;->mErrorCode:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_1

    const v0, 0x7f0e03c1

    .line 258
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0e03aa

    .line 259
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0e03ac

    .line 247
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0e03ab

    .line 248
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0e0218

    .line 241
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0e0217

    .line 242
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0e03b7

    .line 236
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0e03b6

    .line 237
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const v0, 0x7f0e03b4

    .line 252
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0e03a8

    .line 253
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0e03bd

    .line 272
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 277
    :goto_0
    invoke-static {v2, v0}, Lcom/epson/memcardacc/AltAlertDialogFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/epson/memcardacc/AltAlertDialogFragment;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x6f
        :pswitch_5
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

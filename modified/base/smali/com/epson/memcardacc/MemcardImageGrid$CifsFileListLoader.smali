.class Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;
.super Landroid/os/AsyncTask;
.source "MemcardImageGrid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/MemcardImageGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CifsFileListLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final PROGRESS_UPDATE_DIRCTORY:I = 0x1

.field private static final PROGRESS_UPDATE_THUMBNAIL:I = 0x2

.field private static final RESULT_FILE_LIST_CANNOT_GET:I = 0x1

.field private static final RESULT_OK:I


# instance fields
.field mAdapterUpdateWaitDone:Z

.field mBaseDir:Ljava/lang/String;

.field mCanceling:Z

.field mFileList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/CifsFileInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/epson/memcardacc/MemcardImageGrid;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/MemcardImageGrid;)V
    .locals 0

    .line 693
    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private createThumbnail(Lcom/epson/memcardacc/CifsAccess;)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 882
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$800(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/MemcardBitmapCache;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/MemcardBitmapCache;->setCifsAccess(Lcom/epson/memcardacc/CifsAccess;)V

    .line 883
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    .line 884
    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$500(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/ImageFileListAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter;->getFileInfoIterator()Ljava/util/Iterator;

    move-result-object p1

    .line 885
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    .line 886
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mCanceling:Z

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    return v2

    .line 890
    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    .line 891
    invoke-virtual {v0}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->needThumbNail()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 892
    invoke-virtual {v0}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    .line 893
    invoke-virtual {v0, v3}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->setThumbNail(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 895
    :cond_2
    invoke-virtual {v0}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->getFileName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MemcardImageGrid"

    .line 896
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "name <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    invoke-virtual {v0}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->getFullPath()Ljava/lang/String;

    move-result-object v4

    .line 902
    iget-object v5, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v5}, Lcom/epson/memcardacc/MemcardImageGrid;->access$800(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/MemcardBitmapCache;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Lcom/epson/memcardacc/MemcardBitmapCache;->cacheOrMakeThumbnailBitmap(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 905
    invoke-virtual {v0, v3}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->setThumbNail(Landroid/graphics/Bitmap;)V

    .line 907
    :goto_1
    new-array v0, v1, [Ljava/lang/Integer;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    return v1
.end method


# virtual methods
.method public cancelTask()V
    .locals 2

    .line 825
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mCanceling:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "MemcardImageGrid"

    const-string v1, "task canceling..."

    .line 828
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 829
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mCanceling:Z

    .line 830
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->cancel()V

    return-void
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 8

    monitor-enter p0

    :try_start_0
    const-string v0, "MemcardImageGrid"

    const-string v1, "doInBackground() started"

    .line 748
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/epson/memcardacc/CifsAccess;->initDefault(Landroid/content/Context;I)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 751
    monitor-exit p0

    return-object v1

    .line 753
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    iget-object v3, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v3}, Lcom/epson/memcardacc/MemcardImageGrid;->access$300(Lcom/epson/memcardacc/MemcardImageGrid;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/epson/memcardacc/CifsAccess;->connectDefaultStorageWidthDefaultAthInfo(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 754
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 755
    monitor-exit p0

    return-object v1

    :cond_1
    const/4 v0, 0x0

    .line 758
    :try_start_2
    aget-object p1, p1, v0

    .line 759
    iget-object v3, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    .line 760
    invoke-static {v3}, Lcom/epson/memcardacc/MemcardImageGrid;->access$400(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/DirectoryCache;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/epson/memcardacc/DirectoryCache;->getFileList(Ljava/lang/String;)Ljava/util/LinkedList;

    move-result-object v3

    if-nez v3, :cond_4

    .line 763
    iget-object v3, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    iget-boolean v3, v3, Lcom/epson/memcardacc/MemcardImageGrid;->mStartDirectory:Z

    if-eqz v3, :cond_3

    const-string p1, ""

    .line 766
    iget-object v3, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v3}, Lcom/epson/memcardacc/MemcardImageGrid;->access$400(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/DirectoryCache;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/epson/memcardacc/DirectoryCache;->getFileList(Ljava/lang/String;)Ljava/util/LinkedList;

    move-result-object v3

    if-nez v3, :cond_4

    .line 770
    iget-object v3, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v3}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object v3

    invoke-virtual {v3}, Lcom/epson/memcardacc/CifsAccess;->getFreeUnit()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-gez v7, :cond_2

    .line 771
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 772
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 816
    :try_start_3
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 772
    monitor-exit p0

    return-object p1

    .line 777
    :cond_2
    :try_start_4
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    goto :goto_0

    .line 781
    :cond_3
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 784
    :cond_4
    :goto_0
    iget-object v4, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    iput-boolean v0, v4, Lcom/epson/memcardacc/MemcardImageGrid;->mStartDirectory:Z

    .line 785
    iget-object v4, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-virtual {v4, v3}, Lcom/epson/memcardacc/MemcardImageGrid;->selectFileList(Ljava/util/LinkedList;)V

    .line 787
    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mBaseDir:Ljava/lang/String;

    .line 788
    iput-object v3, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mFileList:Ljava/util/LinkedList;

    .line 791
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mAdapterUpdateWaitDone:Z

    .line 792
    new-array p1, v2, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v0

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->publishProgress([Ljava/lang/Object;)V

    .line 793
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->waiteAdapterUpdateAndCancelCheck()Z

    move-result p1

    if-eqz p1, :cond_5

    const-string p1, "MemcardImageGrid"

    const-string v3, "doInBackground() next createThumbnail"

    .line 794
    invoke-static {p1, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->createThumbnail(Lcom/epson/memcardacc/CifsAccess;)I

    :cond_5
    const-string p1, "MemcardImageGrid"

    const-string v3, "doInBackground() disconnectStorage()"

    .line 798
    invoke-static {p1, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 802
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    iget-boolean p1, p1, Lcom/epson/memcardacc/MemcardImageGrid;->discconectOnThreadFinish:Z

    if-ne p1, v2, :cond_6

    const-string p1, "MemcardImageGrid"

    const-string v2, "disconnectOnThreadFinish"

    .line 803
    invoke-static {p1, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    const-string v2, "printer"

    iget-object v3, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    .line 805
    invoke-static {v3}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 804
    invoke-static {p1, v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_6
    const-string p1, "MemcardImageGrid"

    const-string v2, "doInBackground() end"

    .line 808
    invoke-static {p1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 816
    :try_start_5
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 809
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 812
    :try_start_6
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const-string p1, "MemcardImageGrid"

    const-string v0, "exception in doInBackground()"

    .line 813
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 816
    :try_start_7
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 814
    monitor-exit p0

    return-object v1

    .line 816
    :goto_1
    :try_start_8
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 693
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    const-string v0, "MemcardImageGrid"

    const-string v1, "doInBackground() done."

    .line 855
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 858
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$600(Lcom/epson/memcardacc/MemcardImageGrid;)V

    .line 859
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    const-string v0, "dialog_file_read_error"

    invoke-static {p1, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$700(Lcom/epson/memcardacc/MemcardImageGrid;Ljava/lang/String;)V

    return-void

    .line 862
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 867
    :pswitch_0
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    const-string v0, "dialog_file_read_error"

    invoke-static {p1, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$700(Lcom/epson/memcardacc/MemcardImageGrid;Ljava/lang/String;)V

    goto :goto_0

    .line 864
    :pswitch_1
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$500(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/ImageFileListAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 693
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    const-string v0, "MemcardImageGrid"

    const-string v1, "doInBackground() onPreExecute()."

    .line 718
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3

    const/4 v0, 0x0

    .line 839
    aget-object p1, p1, v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 840
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$500(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/ImageFileListAdapter;

    move-result-object p1

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mBaseDir:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mFileList:Ljava/util/LinkedList;

    invoke-virtual {p1, v1, v2}, Lcom/epson/memcardacc/ImageFileListAdapter;->setFileList(Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 841
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$500(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/ImageFileListAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter;->notifyDataSetChanged()V

    .line 842
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mAdapterUpdateWaitDone:Z

    .line 844
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-virtual {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->updateSelectedNumber()V

    .line 846
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$600(Lcom/epson/memcardacc/MemcardImageGrid;)V

    goto :goto_0

    .line 848
    :cond_0
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$500(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/ImageFileListAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 693
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method waiteAdapterUpdateAndCancelCheck()Z
    .locals 4

    .line 727
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mCanceling:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 730
    :cond_0
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mAdapterUpdateWaitDone:Z

    if-nez v0, :cond_1

    const-wide/16 v2, 0xc8

    .line 732
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 736
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->mCanceling:Z

    if-eqz v0, :cond_0

    return v1

    :catch_0
    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

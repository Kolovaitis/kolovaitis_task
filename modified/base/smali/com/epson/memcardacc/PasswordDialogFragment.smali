.class public Lcom/epson/memcardacc/PasswordDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PasswordDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/PasswordDialogFragment$Callback;
    }
.end annotation


# static fields
.field private static final ARG_KEY_FIRST_AUTHENTICATION:Ljava/lang/String; = "first_authentication"


# instance fields
.field private mMessage:Landroid/widget/TextView;

.field private mPasswordEdit:Landroid/widget/EditText;

.field private mUserNameEdit:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/PasswordDialogFragment;)Landroid/widget/EditText;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/epson/memcardacc/PasswordDialogFragment;->mUserNameEdit:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$100(Lcom/epson/memcardacc/PasswordDialogFragment;)Landroid/widget/EditText;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/epson/memcardacc/PasswordDialogFragment;->mPasswordEdit:Landroid/widget/EditText;

    return-object p0
.end method

.method public static newInstance(Z)Lcom/epson/memcardacc/PasswordDialogFragment;
    .locals 3

    .line 35
    new-instance v0, Lcom/epson/memcardacc/PasswordDialogFragment;

    invoke-direct {v0}, Lcom/epson/memcardacc/PasswordDialogFragment;-><init>()V

    .line 36
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "first_authentication"

    .line 37
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/PasswordDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public cancelAuthentication()V
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/epson/memcardacc/PasswordDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/epson/memcardacc/PasswordDialogFragment$Callback;

    invoke-interface {v0}, Lcom/epson/memcardacc/PasswordDialogFragment$Callback;->onNegativeClicked()V

    return-void
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/epson/memcardacc/PasswordDialogFragment;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/epson/memcardacc/PasswordDialogFragment;->mUserNameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .line 50
    invoke-virtual {p0}, Lcom/epson/memcardacc/PasswordDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    const-string v1, "first_authentication"

    .line 52
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 55
    :cond_0
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/epson/memcardacc/PasswordDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 57
    invoke-virtual {p0}, Lcom/epson/memcardacc/PasswordDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0a0071

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080258

    .line 59
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/epson/memcardacc/PasswordDialogFragment;->mMessage:Landroid/widget/TextView;

    .line 60
    iget-object v2, p0, Lcom/epson/memcardacc/PasswordDialogFragment;->mMessage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    const v0, 0x7f0e03cc

    goto :goto_0

    :cond_1
    const v0, 0x7f0e03cb

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f08020e

    .line 64
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/epson/memcardacc/PasswordDialogFragment;->mUserNameEdit:Landroid/widget/EditText;

    const v0, 0x7f08020d

    .line 65
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/epson/memcardacc/PasswordDialogFragment;->mPasswordEdit:Landroid/widget/EditText;

    const v0, 0x7f080191

    .line 67
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 68
    new-instance v2, Lcom/epson/memcardacc/PasswordDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/epson/memcardacc/PasswordDialogFragment$1;-><init>(Lcom/epson/memcardacc/PasswordDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080192

    .line 75
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 76
    new-instance v2, Lcom/epson/memcardacc/PasswordDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/epson/memcardacc/PasswordDialogFragment$2;-><init>(Lcom/epson/memcardacc/PasswordDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e04f2

    new-instance v2, Lcom/epson/memcardacc/PasswordDialogFragment$4;

    invoke-direct {v2, p0}, Lcom/epson/memcardacc/PasswordDialogFragment$4;-><init>(Lcom/epson/memcardacc/PasswordDialogFragment;)V

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0476

    new-instance v2, Lcom/epson/memcardacc/PasswordDialogFragment$3;

    invoke-direct {v2, p0}, Lcom/epson/memcardacc/PasswordDialogFragment$3;-><init>(Lcom/epson/memcardacc/PasswordDialogFragment;)V

    .line 105
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e049f

    .line 112
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 114
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 116
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 117
    invoke-virtual {p1, p0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object p1
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/epson/memcardacc/PasswordDialogFragment;->cancelAuthentication()V

    .line 139
    invoke-virtual {p0}, Lcom/epson/memcardacc/PasswordDialogFragment;->dismiss()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

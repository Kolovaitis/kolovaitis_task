.class public Lcom/epson/memcardacc/MemcardCheckTask;
.super Landroid/os/AsyncTask;
.source "MemcardCheckTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;,
        Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final ERROR_CODE_ACTIVITY_ON_PAUSE_CANCEL:I = 0x6

.field public static final ERROR_CODE_CANCEL:I = 0x5

.field public static final ERROR_MEM_CARD_NOT_FOUND:I = 0x1

.field public static final ERROR_PRINTER_BUSY:I = 0x3

.field public static final ERROR_PRINTER_CONNECTION:I = 0x2

.field public static final ERROR_PRINTER_ERROR:I = 0x4

.field public static final NO_ERROR:I


# instance fields
.field protected mActivityOnPauseCancel:Z

.field mCallback:Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;

.field mCifsStorageSetType:I

.field mContext:Landroid/content/Context;

.field mErrorCode:I

.field private mQueue:Ljava/util/concurrent/SynchronousQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/SynchronousQueue<",
            "Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 74
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mActivityOnPauseCancel:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;I)V
    .locals 1

    .line 63
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mActivityOnPauseCancel:Z

    .line 64
    new-instance v0, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v0}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mQueue:Ljava/util/concurrent/SynchronousQueue;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mContext:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mCallback:Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;

    .line 67
    iput p3, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mCifsStorageSetType:I

    return-void
.end method


# virtual methods
.method public activityOnPauseCancel()V
    .locals 1

    const/4 v0, 0x1

    .line 197
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mActivityOnPauseCancel:Z

    .line 198
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardCheckTask;->cancel(Z)Z

    return-void
.end method

.method protected checkEscprLib(Landroid/content/Context;I)I
    .locals 2

    .line 230
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardCheckTask;->newEscprLibPrinter()Lcom/epson/memcardacc/EscprLibPrinter;

    move-result-object v0

    const/4 v1, 0x1

    .line 231
    invoke-virtual {v0, p1, p2, v1}, Lcom/epson/memcardacc/EscprLibPrinter;->init(Landroid/content/Context;IZ)I

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    return p1

    .line 234
    :cond_0
    invoke-virtual {v0}, Lcom/epson/memcardacc/EscprLibPrinter;->getStatus()I

    move-result p1

    .line 235
    invoke-virtual {v0}, Lcom/epson/memcardacc/EscprLibPrinter;->release()V

    if-eqz p1, :cond_1

    const/4 p2, 0x4

    if-eq p1, p2, :cond_1

    const/4 p1, 0x3

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 7

    .line 87
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardCheckTask;->isCancelled()Z

    move-result p1

    const/4 v0, 0x5

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 88
    iput v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    .line 89
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 92
    :cond_0
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mContext:Landroid/content/Context;

    const/16 v2, 0x3c

    invoke-virtual {p0, p1, v2}, Lcom/epson/memcardacc/MemcardCheckTask;->checkEscprLib(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    .line 93
    iget p1, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    if-eqz p1, :cond_1

    .line 94
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardCheckTask;->getCifsAccessInstance()Lcom/epson/memcardacc/CifsAccess;

    move-result-object p1

    .line 100
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardCheckTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 101
    iput v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    .line 102
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v0

    .line 104
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mCifsStorageSetType:I

    invoke-virtual {p1, v2, v3}, Lcom/epson/memcardacc/CifsAccess;->initDefault(Landroid/content/Context;I)I

    move-result v2

    if-nez v2, :cond_3

    .line 107
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    .line 108
    iget-object v2, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mCifsStorageSetType:I

    invoke-virtual {p1, v2, v3}, Lcom/epson/memcardacc/CifsAccess;->initDefault(Landroid/content/Context;I)I

    move-result v2

    if-nez v2, :cond_3

    .line 110
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v0

    :cond_3
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    :cond_4
    :goto_0
    if-nez v3, :cond_8

    .line 116
    :try_start_2
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardCheckTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 117
    iput v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    .line 118
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 155
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v0

    .line 121
    :cond_5
    :try_start_3
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->checkStorage()I

    move-result v3

    if-nez v3, :cond_4

    .line 123
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->getErrorCode()I

    move-result v5

    const/16 v6, -0x4b5

    if-eq v5, v6, :cond_6

    .line 125
    iput v2, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    .line 126
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 155
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v0

    .line 129
    :cond_6
    :try_start_4
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardCheckTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 130
    iput v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    .line 131
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 155
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v0

    .line 134
    :cond_7
    :try_start_5
    iget-object v5, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mCallback:Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;

    invoke-interface {v5, p0, v4}, Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;->onAuthInfoRequired(Lcom/epson/memcardacc/MemcardCheckTask;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 141
    :try_start_6
    iget-object v4, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mQueue:Ljava/util/concurrent/SynchronousQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/SynchronousQueue;->take()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v4, 0x0

    goto :goto_0

    .line 146
    :catch_0
    :try_start_7
    iput v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    .line 147
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 155
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v0

    :cond_8
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    .line 158
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception v0

    .line 155
    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    throw v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardCheckTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected getCifsAccessInstance()Lcom/epson/memcardacc/CifsAccess;
    .locals 1

    .line 163
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getInstance()Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    return-object v0
.end method

.method protected newEscprLibPrinter()Lcom/epson/memcardacc/EscprLibPrinter;
    .locals 1

    .line 246
    new-instance v0, Lcom/epson/memcardacc/EscprLibPrinter;

    invoke-direct {v0}, Lcom/epson/memcardacc/EscprLibPrinter;-><init>()V

    return-object v0
.end method

.method protected onCancelled()V
    .locals 3

    .line 185
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mActivityOnPauseCancel:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    .line 189
    :goto_0
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mCallback:Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;->onMemcardCheckEnd(Ljava/lang/Integer;I)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mCallback:Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;

    iget v1, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mErrorCode:I

    invoke-interface {v0, p1, v1}, Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;->onMemcardCheckEnd(Ljava/lang/Integer;I)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardCheckTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mCallback:Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;

    invoke-interface {v0}, Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;->onPreExecute()V

    return-void
.end method

.method public setAuthData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 p1, 0x0

    .line 211
    :goto_0
    :try_start_0
    iget-object p2, p0, Lcom/epson/memcardacc/MemcardCheckTask;->mQueue:Ljava/util/concurrent/SynchronousQueue;

    new-instance v0, Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/epson/memcardacc/MemcardCheckTask$MyQueueData;-><init>(I)V

    invoke-virtual {p2, v0}, Ljava/util/concurrent/SynchronousQueue;->offer(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p2, 0x4

    if-ge p1, p2, :cond_0

    const-wide/16 v0, 0x12c

    .line 215
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :catch_0
    :cond_0
    return-void
.end method

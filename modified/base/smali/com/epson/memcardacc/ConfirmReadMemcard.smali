.class public Lcom/epson/memcardacc/ConfirmReadMemcard;
.super Lepson/print/ActivityIACommon;
.source "ConfirmReadMemcard.java"

# interfaces
.implements Lepson/common/DialogProgress$DialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;,
        Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;
    }
.end annotation


# static fields
.field private static final ACTIVITY_DIRECTORY_SELECT:I = 0x2

.field private static final ACTIVITY_TYPE_PROGRESS:I = 0x1

.field private static final DIALOG_COMMUNICATION_ERROR:Ljava/lang/String; = "dialog_communication_error"

.field private static final DIALOG_COPY_RESULT_CANCEL:Ljava/lang/String; = "dialog_copy_result"

.field private static final DIALOG_COPY_RESULT_COMPLETE:Ljava/lang/String; = "dialog_copy_result_complete"

.field private static final DIALOG_COPY_RESULT_ERROR:Ljava/lang/String; = "dialog_copy_result_error"

.field private static final DIALOG_DISK_SPACE_SHORTAGE:Ljava/lang/String; = "dialog_disk_space_shortage"

.field private static final DIALOG_PRINTER_BUSY:Ljava/lang/String; = "dialog_printer_busy"

.field private static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field private static final DIALOG_WRITE_LOCAL_DIR_CANNOT_GET:Ljava/lang/String; = "dialog_write_local_dir_cannot_get"

.field static final ERROR_COMMUNICATION_ERROR:I = 0xb

.field static final ERROR_MEMCARD_BUSY:I = 0xa

.field public static final KEY_IMAGE_LIST:Ljava/lang/String; = "imagelist"

.field private static final LOG_TAG:Ljava/lang/String; = "ConfirmReadMemcard"

.field public static final MEMCARD_STORAGE_TYPE:Ljava/lang/String; = "memcard_storage_type"

.field public static final RESULT_BACK_TO_MEMCARD_TOP:I = 0x1


# instance fields
.field doTotalFileSizeCalc:Z

.field isKeepSimleApConnect:Z

.field isTryConnectSimpleAp:Z

.field mCheckAndNextActivityTask:Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;

.field private mCopyTargetFolderName:Ljava/lang/String;

.field private mCopyTargetPathText:Landroid/widget/TextView;

.field private mFileNumText:Landroid/widget/TextView;

.field private mFileSizeText:Landroid/widget/TextView;

.field private mImageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMemcardStorageType:I

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mTotalFileSize:J

.field private mTotalFileSizeCalcTask:Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;

.field private mWriteDirectory:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 36
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const-string v0, "eproll"

    .line 92
    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mWriteDirectory:Ljava/lang/String;

    const/4 v0, 0x0

    .line 97
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isTryConnectSimpleAp:Z

    .line 98
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isKeepSimleApConnect:Z

    .line 99
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->doTotalFileSizeCalc:Z

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/ConfirmReadMemcard;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mMemcardStorageType:I

    return p0
.end method

.method static synthetic access$100(Lcom/epson/memcardacc/ConfirmReadMemcard;)Ljava/util/ArrayList;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mImageList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$200(Lcom/epson/memcardacc/ConfirmReadMemcard;Ljava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->localShowDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/memcardacc/ConfirmReadMemcard;)Lepson/common/DialogProgressViewModel;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    return-object p0
.end method

.method static synthetic access$400(Lcom/epson/memcardacc/ConfirmReadMemcard;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/epson/memcardacc/ConfirmReadMemcard;)Z
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->checkFileSize()Z

    move-result p0

    return p0
.end method

.method static synthetic access$600(Lcom/epson/memcardacc/ConfirmReadMemcard;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->startCopyActivity()V

    return-void
.end method

.method private checkFileSize()Z
    .locals 6

    .line 296
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/epson/memcardacc/MemcardConfig;->BASE_DIRECTORY:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mWriteDirectory:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 298
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v2

    .line 299
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    .line 302
    iget-wide v4, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mTotalFileSize:J

    div-long/2addr v4, v0

    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v4, v0

    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 404
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 406
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private getTargetFolderName()Ljava/lang/String;
    .locals 9

    .line 246
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x64

    if-ge v2, v3, :cond_1

    .line 250
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s/%s/%04d%02d%02d%02d%02d%02d"

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Lcom/epson/memcardacc/MemcardConfig;->BASE_DIRECTORY:Ljava/lang/String;

    aput-object v6, v5, v1

    iget-object v6, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mWriteDirectory:Ljava/lang/String;

    const/4 v7, 0x1

    aput-object v6, v5, v7

    .line 255
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v8, 0x2

    aput-object v6, v5, v8

    const/4 v6, 0x3

    .line 256
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const/4 v7, 0x5

    .line 257
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v6

    const/16 v6, 0xb

    .line 258
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x6

    const/16 v7, 0xc

    .line 259
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x7

    .line 260
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 250
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 261
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 262
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 263
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v5

    .line 265
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    if-eqz v5, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method static getTotalFileSize(Ljava/util/ArrayList;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    const-wide/16 v0, 0x0

    if-nez p0, :cond_0

    return-wide v0

    .line 286
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 287
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 288
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    :cond_1
    return-wide v0
.end method

.method public static synthetic lambda$onCreate$0(Lcom/epson/memcardacc/ConfirmReadMemcard;Ljava/util/Deque;)V
    .locals 2

    .line 116
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 118
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 119
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 122
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 126
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 127
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private localShowDialog(Ljava/lang/String;)V
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {v0, p1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 11

    .line 362
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "dialog_printer_busy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_1
    const-string v0, "dialog_copy_result_complete"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_2
    const-string v0, "dialog_write_local_dir_cannot_get"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_4
    const-string v0, "dialog_copy_result_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_5
    const-string v0, "dialog_copy_result"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_6
    const-string v0, "dialog_disk_space_shortage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_7
    const-string v0, "dialog_communication_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    const v2, 0x7f0e03bd

    const v3, 0x7f0e04f2

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto/16 :goto_2

    :pswitch_0
    const/4 v5, 0x2

    const v0, 0x7f0e03b6

    .line 391
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v0, 0x7f0e03b7

    .line 392
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v4, p1

    .line 391
    invoke-static/range {v4 .. v10}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_1
    const/4 v2, 0x2

    const v0, 0x7f0e0217

    .line 387
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e0218

    .line 388
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v3, v0

    .line 387
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_2
    const/4 v0, 0x2

    .line 383
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v1, 0x7f0e03be

    .line 384
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p1

    move v2, v0

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    .line 383
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_3
    const/4 v0, 0x2

    .line 379
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 380
    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v1, p1

    move v2, v0

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    .line 379
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_4
    const/4 v2, 0x2

    const v0, 0x7f0e03ba

    .line 374
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 375
    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, p1

    move-object v3, v0

    .line 374
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_5
    const/4 v2, 0x2

    const v0, 0x7f0e03b3

    .line 369
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e03b4

    .line 370
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v3, v0

    .line 369
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_6
    const v0, 0x7f0e03b5

    .line 364
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 365
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    :goto_2
    if-eqz v0, :cond_1

    .line 396
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e289d18 -> :sswitch_7
        -0x77723c79 -> :sswitch_6
        -0x1e4d9850 -> :sswitch_5
        -0x3cce747 -> :sswitch_4
        -0x14b98bc -> :sswitch_3
        0x40559217 -> :sswitch_2
        0x4739a0e8 -> :sswitch_1
        0x552f8495 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private startCopyActivity()V
    .locals 3

    .line 321
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "imageList"

    .line 322
    iget-object v2, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "target_folder_name"

    .line 324
    iget-object v2, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "memcard_storage_type"

    .line 326
    iget v2, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mMemcardStorageType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    .line 327
    invoke-virtual {p0, v0, v1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->startActivityForResult(Landroid/content/Intent;I)V

    .line 329
    iput-boolean v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isKeepSimleApConnect:Z

    return-void
.end method


# virtual methods
.method public copy_button_clicked(Landroid/view/View;)V
    .locals 1

    .line 352
    new-instance p1, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;

    invoke-direct {p1, p0}, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;-><init>(Lcom/epson/memcardacc/ConfirmReadMemcard;)V

    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCheckAndNextActivityTask:Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;

    .line 353
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCheckAndNextActivityTask:Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public copy_dest_button_clicked(Landroid/view/View;)V
    .locals 2

    .line 338
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/epson/memcardacc/DirectorySelecterActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "baseDirectory"

    .line 339
    sget-object v1, Lcom/epson/memcardacc/MemcardConfig;->BASE_DIRECTORY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "default"

    const-string v1, "eproll"

    .line 341
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x2

    .line 343
    invoke-virtual {p0, p1, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_2

    const-string p1, "resultDirectory"

    .line 435
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 436
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->setTargetDirectory(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const/4 p1, 0x0

    .line 415
    iput-boolean p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isKeepSimleApConnect:Z

    const/4 p1, -0x2

    if-eq p2, p1, :cond_1

    if-eqz p2, :cond_0

    const-string p1, "dialog_copy_result_error"

    .line 430
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->localShowDialog(Ljava/lang/String;)V

    return-void

    :cond_0
    return-void

    :cond_1
    const/4 p1, 0x1

    .line 420
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->setResult(I)V

    .line 421
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->finish()V

    return-void

    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .line 212
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    const/4 v0, 0x1

    .line 214
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isKeepSimleApConnect:Z

    return-void
.end method

.method public onCancelDialog(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 106
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0022

    .line 107
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->setContentView(I)V

    .line 110
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgressViewModel;

    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 111
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/memcardacc/-$$Lambda$ConfirmReadMemcard$lvLHdxTxFsc3e4aCbADlWyp10yg;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/-$$Lambda$ConfirmReadMemcard$lvLHdxTxFsc3e4aCbADlWyp10yg;-><init>(Lcom/epson/memcardacc/ConfirmReadMemcard;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    const/4 p1, 0x1

    const v0, 0x7f0e03d2

    .line 133
    invoke-virtual {p0, v0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->setActionBar(IZ)V

    const v0, 0x7f08031f

    .line 135
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mFileNumText:Landroid/widget/TextView;

    const v0, 0x7f080320

    .line 136
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mFileSizeText:Landroid/widget/TextView;

    .line 137
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mFileSizeText:Landroid/widget/TextView;

    const-string v1, "?"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f08032e

    .line 138
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCopyTargetPathText:Landroid/widget/TextView;

    .line 140
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "imagelist"

    .line 141
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mImageList:Ljava/util/ArrayList;

    .line 142
    iget-object v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mImageList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 143
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mImageList:Ljava/util/ArrayList;

    :cond_0
    const-string v1, "memcard_storage_type"

    .line 145
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mMemcardStorageType:I

    .line 147
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 148
    iget-object v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mFileNumText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e03bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, p1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "eproll"

    .line 151
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->setTargetDirectory(Ljava/lang/String;)V

    .line 155
    iput-boolean p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->doTotalFileSizeCalc:Z

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 206
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mTotalFileSizeCalcTask:Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->cancel(Z)Z

    .line 207
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    return-void
.end method

.method public onNegativeClick(Ljava/lang/String;)V
    .locals 3

    .line 453
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x1e4d9850

    const/4 v2, 0x1

    if-eq v0, v1, :cond_1

    const v1, 0x4739a0e8

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_copy_result_complete"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "dialog_copy_result"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, -0x1

    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_2

    .line 460
    :pswitch_0
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mWriteDirectory:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->setTargetDirectory(Ljava/lang/String;)V

    goto :goto_2

    .line 455
    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmReadMemcard;->setResult(I)V

    .line 456
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->finish()V

    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onNeutralClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "ConfirmReadMemcard"

    const-string v1, "onPause()"

    .line 187
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    return-void
.end method

.method public onPositiveClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onResume()V
    .locals 4

    .line 160
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "printer"

    .line 163
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 165
    iput-boolean v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 166
    :cond_0
    iget-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isTryConnectSimpleAp:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 168
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isTryConnectSimpleAp:Z

    const-string v2, "printer"

    const/4 v3, -0x1

    .line 169
    invoke-static {p0, v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 170
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->doTotalFileSizeCalc:Z

    return-void

    .line 175
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isKeepSimleApConnect:Z

    .line 178
    iget-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->doTotalFileSizeCalc:Z

    if-eqz v0, :cond_2

    .line 179
    new-instance v0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;-><init>(Lcom/epson/memcardacc/ConfirmReadMemcard;)V

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mTotalFileSizeCalcTask:Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;

    .line 180
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mTotalFileSizeCalcTask:Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;

    new-array v2, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 181
    iput-boolean v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->doTotalFileSizeCalc:Z

    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "ConfirmReadMemcard"

    const-string v1, "onStop()"

    .line 193
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    .line 198
    iget-boolean v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->isKeepSimleApConnect:Z

    if-nez v0, :cond_0

    const-string v0, "printer"

    .line 199
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method public setTargetDirectory(Ljava/lang/String;)V
    .locals 4

    .line 230
    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mWriteDirectory:Ljava/lang/String;

    .line 231
    invoke-direct {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getTargetFolderName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    .line 232
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    if-nez p1, :cond_0

    const-string p1, "dialog_write_local_dir_cannot_get"

    .line 234
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->localShowDialog(Ljava/lang/String;)V

    const-string p1, ""

    :cond_0
    const-string v0, "ConfirmReadMemcard"

    .line 237
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "outdir: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mCopyTargetPathText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e03c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateFileSize(J)V
    .locals 5

    .line 311
    iput-wide p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mTotalFileSize:J

    .line 312
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mFileSizeText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0e03d3

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-wide v1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard;->mTotalFileSize:J

    long-to-double v1, v1

    const-wide v3, 0x412e848000000000L    # 1000000.0

    div-double/2addr v1, v3

    .line 313
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 312
    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.class public Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;
.super Landroid/arch/lifecycle/LiveData;
.source "MemCardWriteConvertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/MemCardWriteConvertViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConvertLiveData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/arch/lifecycle/LiveData<",
        "Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/memcardacc/MemCardWriteConvertViewModel;


# direct methods
.method public constructor <init>(Lcom/epson/memcardacc/MemCardWriteConvertViewModel;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->this$0:Lcom/epson/memcardacc/MemCardWriteConvertViewModel;

    invoke-direct {p0}, Landroid/arch/lifecycle/LiveData;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->initTempDirectory()V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;Ljava/util/ArrayList;)Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->convertAndGetFileSize(Ljava/util/ArrayList;)Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;Ljava/lang/Object;)V
    .locals 0

    .line 56
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method private declared-synchronized convertAndGetFileSize(Ljava/util/ArrayList;)Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;
    .locals 5
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;"
        }
    .end annotation

    monitor-enter p0

    .line 76
    :try_start_0
    new-instance v0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;

    invoke-direct {v0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->this$0:Lcom/epson/memcardacc/MemCardWriteConvertViewModel;

    invoke-static {v1}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->access$400(Lcom/epson/memcardacc/MemCardWriteConvertViewModel;)Landroid/app/Application;

    move-result-object v1

    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    .line 79
    invoke-direct {p0, v1}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->initTempDirectory(Lepson/common/ExternalFileUtils;)Ljava/io/File;

    move-result-object v1

    .line 81
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 82
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 83
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 84
    invoke-static {v2}, Lepson/common/ImageUtil;->isHEIFFormat(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 86
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1e

    .line 85
    invoke-static {v2, v3, v4}, Lepson/common/ExternalFileUtils;->getNotDuplicateFilename(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 87
    invoke-static {v2, v3}, Lepson/common/ImageUtil;->convertHEIFtoJPEG(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    const/4 p1, 0x0

    .line 89
    monitor-exit p0

    return-object p1

    :cond_0
    move-object v2, v3

    .line 93
    :cond_1
    :try_start_1
    invoke-virtual {v0, v2}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;->addFile(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 95
    :cond_2
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private initTempDirectory(Lepson/common/ExternalFileUtils;)Ljava/io/File;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 100
    sget-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->MEMORY_CARD_ACCESS:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    invoke-virtual {p1, v0}, Lepson/common/ExternalFileUtils;->initTempCacheDirectory(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method private declared-synchronized initTempDirectory()V
    .locals 1

    monitor-enter p0

    .line 105
    :try_start_0
    iget-object v0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->this$0:Lcom/epson/memcardacc/MemCardWriteConvertViewModel;

    invoke-static {v0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->access$400(Lcom/epson/memcardacc/MemCardWriteConvertViewModel;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    .line 106
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->initTempDirectory(Lepson/common/ExternalFileUtils;)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public setOrigFileList(Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 58
    new-instance v0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData$1;

    invoke-direct {v0, p0, p1}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData$1;-><init>(Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;Ljava/util/ArrayList;)V

    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 68
    invoke-virtual {v0, p1, v1}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

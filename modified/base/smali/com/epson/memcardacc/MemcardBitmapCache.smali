.class public Lcom/epson/memcardacc/MemcardBitmapCache;
.super Lcom/epson/memcardacc/BitmapCache;
.source "MemcardBitmapCache.java"


# static fields
.field protected static sBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;


# instance fields
.field protected mDirOk:Z


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/epson/memcardacc/BitmapCache;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/memcardacc/MemcardBitmapCache;
    .locals 5

    .line 19
    new-instance v0, Lcom/epson/memcardacc/MemcardBitmapCache;

    invoke-direct {v0}, Lcom/epson/memcardacc/MemcardBitmapCache;-><init>()V

    .line 23
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object p0

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return-object v1

    .line 27
    :cond_0
    new-instance v2, Ljava/io/File;

    const-string v3, "bindata01"

    invoke-direct {v2, p0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    const-string v4, "bindata02"

    invoke-direct {v3, p0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/epson/memcardacc/MemcardBitmapCache;->setDirectory(Ljava/io/File;Ljava/io/File;)Z

    move-result p0

    if-nez p0, :cond_1

    return-object v1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public checkEnv()Z
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardBitmapCache;->checkSettings()Z

    move-result v0

    return v0
.end method

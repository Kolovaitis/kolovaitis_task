.class Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;
.super Ljava/lang/Object;
.source "MemcardImageGrid.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/MemcardImageGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyGridLayoutListener"
.end annotation


# instance fields
.field mAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

.field private mGridView:Landroid/widget/GridView;

.field private mHorizontalSpacing:I


# direct methods
.method public constructor <init>(Landroid/widget/GridView;Lcom/epson/memcardacc/ImageFileListAdapter;I)V
    .locals 0

    .line 642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643
    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    .line 644
    iput-object p2, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    .line 645
    iput p3, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mHorizontalSpacing:I

    return-void
.end method

.method private setNewVerticalParams()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .line 682
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getHorizontalSpacing()I

    move-result v0

    .line 683
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 684
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getColumnWidth()I

    move-result v0

    .line 686
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {v1, v0, v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->setImageSize(II)V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .line 657
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getNumColumns()I

    move-result v0

    if-lez v0, :cond_1

    .line 661
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 662
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->setNewVerticalParams()V

    .line 665
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 667
    :cond_0
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mHorizontalSpacing:I

    add-int/2addr v1, v2

    .line 669
    div-int/2addr v1, v0

    sub-int/2addr v1, v2

    .line 671
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {v0, v1, v1}, Lcom/epson/memcardacc/ImageFileListAdapter;->setImageSize(II)V

    .line 674
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    :goto_0
    return-void
.end method

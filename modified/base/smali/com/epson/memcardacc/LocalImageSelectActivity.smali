.class public Lcom/epson/memcardacc/LocalImageSelectActivity;
.super Lepson/print/imgsel/ImageSelectActivity;
.source "LocalImageSelectActivity.java"


# static fields
.field private static final REQUEST_CODE_CONFIRM_WIRTE:I = 0x69


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;-><init>()V

    return-void
.end method

.method public static startAddImageList(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 74
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/LocalImageSelectActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    .line 76
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "selected_files"

    .line 77
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public getImageFinder()Lepson/print/imgsel/ImageFinder;
    .locals 2

    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    .line 24
    new-instance v0, Lepson/print/imgsel/JpegHeifImageFinder;

    invoke-direct {v0}, Lepson/print/imgsel/JpegHeifImageFinder;-><init>()V

    return-object v0

    .line 26
    :cond_0
    new-instance v0, Lepson/print/imgsel/JpegImageFinder;

    invoke-direct {v0}, Lepson/print/imgsel/JpegImageFinder;-><init>()V

    return-object v0
.end method

.method protected goNext()V
    .locals 2

    .line 53
    invoke-virtual {p0}, Lcom/epson/memcardacc/LocalImageSelectActivity;->getImageSelector()Lepson/print/imgsel/ImageSelector;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    return-void

    .line 61
    :cond_0
    invoke-static {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getStartIntent(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x69

    .line 62
    invoke-virtual {p0, v0, v1}, Lcom/epson/memcardacc/LocalImageSelectActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 38
    invoke-super {p0, p1, p2, p3}, Lepson/print/imgsel/ImageSelectActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 p3, 0x69

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    if-eq p2, p1, :cond_1

    :goto_0
    return-void

    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/LocalImageSelectActivity;->finish()V

    return-void
.end method

.method public singleImageMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

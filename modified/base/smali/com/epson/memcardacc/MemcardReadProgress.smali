.class public Lcom/epson/memcardacc/MemcardReadProgress;
.super Landroid/app/Activity;
.source "MemcardReadProgress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;
    }
.end annotation


# static fields
.field public static final KEY_IMAGE_LIST:Ljava/lang/String; = "imageList"

.field public static final KEY_WRITE_FOLDER_NAME:Ljava/lang/String; = "target_folder_name"

.field public static final LOG_TAG:Ljava/lang/String; = "MemcardReadProgress"

.field public static final MEMCARD_STORAGE_TYPE:Ljava/lang/String; = "memcard_storage_type"

.field public static final RESULT_COMPLETE:I = -0x2

.field public static final RESULT_ERROR:I = 0x3


# instance fields
.field discconectOnThreadFinish:Z

.field private mCancelButton:Landroid/widget/Button;

.field private mCopyTask:Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;

.field mEachProgress:Landroid/widget/ProgressBar;

.field private mImageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMemcardStorageType:I

.field private mTargetFolderName:Ljava/lang/String;

.field mTextView:Landroid/widget/TextView;

.field mTotalProgress:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 70
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->discconectOnThreadFinish:Z

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/MemcardReadProgress;)Ljava/util/ArrayList;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mImageList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$100(Lcom/epson/memcardacc/MemcardReadProgress;)I
    .locals 0

    .line 38
    iget p0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mMemcardStorageType:I

    return p0
.end method

.method static synthetic access$200(Lcom/epson/memcardacc/MemcardReadProgress;)Ljava/lang/String;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTargetFolderName:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public actionCancel()V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 130
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mCopyTask:Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->taskCancel()V

    return-void
.end method

.method public finishWithState(ILcom/epson/iprint/prtlogger/CommonLog;)V
    .locals 2
    .param p2    # Lcom/epson/iprint/prtlogger/CommonLog;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 149
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->discconectOnThreadFinish:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "printer"

    .line 150
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    if-eqz p2, :cond_1

    .line 153
    iget v0, p2, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    if-lez v0, :cond_1

    .line 154
    invoke-static {p0, p2}, Lcom/epson/iprint/prtlogger/Analytics;->sendCommonLog(Landroid/content/Context;Lcom/epson/iprint/prtlogger/CommonLog;)V

    .line 157
    :cond_1
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardReadProgress;->setResult(I)V

    .line 158
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardReadProgress;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 75
    invoke-static {p0, p1}, Lepson/common/Utils;->setFInishOnTOuchOutside(Landroid/app/Activity;Z)V

    const v0, 0x7f0a0091

    .line 79
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardReadProgress;->setContentView(I)V

    .line 81
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardReadProgress;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "imageList"

    .line 82
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mImageList:Ljava/util/ArrayList;

    const-string v1, "target_folder_name"

    .line 83
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTargetFolderName:Ljava/lang/String;

    const-string v1, "memcard_storage_type"

    const/4 v2, 0x1

    .line 84
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mMemcardStorageType:I

    const v0, 0x7f08029d

    .line 86
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardReadProgress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTotalProgress:Landroid/widget/ProgressBar;

    .line 87
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTotalProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const v0, 0x7f08029a

    .line 88
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardReadProgress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mEachProgress:Landroid/widget/ProgressBar;

    .line 89
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mEachProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const v0, 0x7f08025b

    .line 90
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardReadProgress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTextView:Landroid/widget/TextView;

    const v0, 0x7f0800fa

    .line 91
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardReadProgress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardReadProgress;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e03bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardReadProgress;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "imageList"

    .line 95
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mImageList:Ljava/util/ArrayList;

    const-string v1, "target_folder_name"

    .line 96
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTargetFolderName:Ljava/lang/String;

    const v0, 0x7f0800b3

    .line 98
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardReadProgress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mCancelButton:Landroid/widget/Button;

    .line 99
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mCancelButton:Landroid/widget/Button;

    new-instance v1, Lcom/epson/memcardacc/MemcardReadProgress$1;

    invoke-direct {v1, p0}, Lcom/epson/memcardacc/MemcardReadProgress$1;-><init>(Lcom/epson/memcardacc/MemcardReadProgress;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    new-instance v0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;-><init>(Lcom/epson/memcardacc/MemcardReadProgress;)V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mCopyTask:Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;

    .line 107
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mCopyTask:Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;

    new-array p1, p1, [Ljava/lang/Void;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "MemcardReadProgress"

    const-string v1, "onStop()"

    .line 115
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardReadProgress;->actionCancel()V

    const/4 v0, 0x1

    .line 122
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->discconectOnThreadFinish:Z

    .line 124
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public setProgress(II)V
    .locals 1

    .line 140
    invoke-virtual {p0, p1, p2}, Lcom/epson/memcardacc/MemcardReadProgress;->setProgressText(II)V

    .line 141
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTotalProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 142
    iget-object p2, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTotalProgress:Landroid/widget/ProgressBar;

    invoke-virtual {p2, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.method public setProgressText(II)V
    .locals 4

    .line 135
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress;->mTextView:Landroid/widget/TextView;

    const-string v1, "%d/%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 136
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v2, p2

    .line 135
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

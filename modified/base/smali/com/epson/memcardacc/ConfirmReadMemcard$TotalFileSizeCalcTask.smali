.class Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;
.super Landroid/os/AsyncTask;
.source "ConfirmReadMemcard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/ConfirmReadMemcard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TotalFileSizeCalcTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

.field final synthetic this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/ConfirmReadMemcard;)V
    .locals 0

    .line 471
    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 472
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getInstance()Lcom/epson/memcardacc/CifsAccess;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    return-void
.end method


# virtual methods
.method protected varargs declared-synchronized doInBackground([Ljava/lang/Void;)Ljava/lang/Long;
    .locals 9

    monitor-enter p0

    .line 476
    :try_start_0
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/epson/memcardacc/CifsAccess;->initDefault(Landroid/content/Context;I)I

    move-result p1

    const-wide/16 v0, -0x1

    if-nez p1, :cond_0

    .line 478
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit p0

    return-object p1

    .line 480
    :cond_0
    :try_start_1
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    iget-object v2, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {v2}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$000(Lcom/epson/memcardacc/ConfirmReadMemcard;)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/epson/memcardacc/CifsAccess;->connectDefaultStorageWidthDefaultAthInfo(I)I

    move-result p1

    if-nez p1, :cond_1

    .line 482
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    .line 483
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return-object p1

    .line 488
    :cond_1
    :try_start_2
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$100(Lcom/epson/memcardacc/ConfirmReadMemcard;)Ljava/util/ArrayList;

    move-result-object p1

    .line 489
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const-wide/16 v2, 0x0

    move-wide v4, v2

    .line 490
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 491
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 492
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 503
    :try_start_3
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 504
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 492
    monitor-exit p0

    return-object p1

    .line 495
    :cond_2
    :try_start_4
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 496
    iget-object v7, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v7, v6}, Lcom/epson/memcardacc/CifsAccess;->getFileSize(Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v8, v6, v2

    if-gtz v8, :cond_3

    .line 498
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 503
    :try_start_5
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 504
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 498
    monitor-exit p0

    return-object p1

    :cond_3
    add-long/2addr v4, v6

    goto :goto_0

    .line 503
    :cond_4
    :try_start_6
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 504
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {p1}, Lcom/epson/memcardacc/CifsAccess;->free()V

    .line 507
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 503
    :try_start_7
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 504
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 471
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 5

    if-eqz p1, :cond_2

    .line 514
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 515
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    const-string v0, "dialog_copy_result"

    invoke-static {p1, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$200(Lcom/epson/memcardacc/ConfirmReadMemcard;Ljava/lang/String;)V

    goto :goto_0

    .line 516
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    .line 517
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    const-string v0, "dialog_communication_error"

    invoke-static {p1, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$200(Lcom/epson/memcardacc/ConfirmReadMemcard;Ljava/lang/String;)V

    goto :goto_0

    .line 519
    :cond_1
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/epson/memcardacc/ConfirmReadMemcard;->updateFileSize(J)V

    .line 522
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$300(Lcom/epson/memcardacc/ConfirmReadMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object p1

    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 471
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 528
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$TotalFileSizeCalcTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$300(Lcom/epson/memcardacc/ConfirmReadMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

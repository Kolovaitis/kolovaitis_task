.class Lcom/epson/memcardacc/FileConvertViewModel$1;
.super Ljava/lang/Object;
.source "FileConvertViewModel.java"

# interfaces
.implements Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/FileConvertViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/memcardacc/FileConvertViewModel;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/FileConvertViewModel;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertViewModel$1;->this$0:Lcom/epson/memcardacc/FileConvertViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConvertEnd(Lcom/epson/memcardacc/FileConvertTask$ErrorType;Ljava/util/ArrayList;)V
    .locals 3
    .param p1    # Lcom/epson/memcardacc/FileConvertTask$ErrorType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/memcardacc/FileConvertTask$ErrorType;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/epson/memcardacc/FileConvertViewModel$1;->this$0:Lcom/epson/memcardacc/FileConvertViewModel;

    invoke-static {v0}, Lcom/epson/memcardacc/FileConvertViewModel;->access$000(Lcom/epson/memcardacc/FileConvertViewModel;)Landroid/arch/lifecycle/MutableLiveData;

    move-result-object v0

    new-instance v1, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;

    sget-object v2, Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;->TASK_END:Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;

    invoke-direct {v1, v2, p1, p2}, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;-><init>(Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;Lcom/epson/memcardacc/FileConvertTask$ErrorType;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

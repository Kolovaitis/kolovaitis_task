.class public Lcom/epson/memcardacc/DirectorySelecterActivity;
.super Landroid/app/Activity;
.source "DirectorySelecterActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/DirectorySelecterActivity$TargetDirTextWatcher;
    }
.end annotation


# static fields
.field private static final DIALOG_FILE_NAME_INVALID:I = 0x1

.field public static final PARAM_BASE_DIRECTORY:Ljava/lang/String; = "baseDirectory"

.field public static final PARAM_DEFAULT_DIR:Ljava/lang/String; = "default"

.field public static final RESULT_DIRECTORY:Ljava/lang/String; = "resultDirectory"


# instance fields
.field mBaseDirTextView:Landroid/widget/TextView;

.field mButtonActionProgress:Z

.field mOkButton:Landroid/widget/Button;

.field mTargetDir:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private localShowDialog(I)V
    .locals 0

    .line 64
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->showDialog(I)V

    return-void
.end method


# virtual methods
.method public cancel_button_clicked(Landroid/view/View;)V
    .locals 0

    .line 93
    iget-boolean p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mButtonActionProgress:Z

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x1

    .line 96
    iput-boolean p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mButtonActionProgress:Z

    const/4 p1, 0x0

    .line 97
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->setResult(I)V

    .line 98
    invoke-virtual {p0}, Lcom/epson/memcardacc/DirectorySelecterActivity;->finish()V

    return-void
.end method

.method disableOkButton()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mOkButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 123
    iget-object v0, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method enableOkButton()V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mOkButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 129
    iget-object v0, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    return-void
.end method

.method public ok_button_clicked(Landroid/view/View;)V
    .locals 5

    .line 68
    iget-boolean p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mButtonActionProgress:Z

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x1

    .line 71
    iput-boolean p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mButtonActionProgress:Z

    .line 72
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mTargetDir:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 74
    invoke-static {v1}, Lcom/epson/memcardacc/MemcardUtil;->checkFileName(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 75
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->localShowDialog(I)V

    .line 76
    iput-boolean v3, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mButtonActionProgress:Z

    return-void

    .line 79
    :cond_1
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mBaseDirTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_2

    .line 82
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->localShowDialog(I)V

    .line 83
    iput-boolean v3, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mButtonActionProgress:Z

    return-void

    :cond_2
    const-string p1, "resultDirectory"

    .line 86
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 87
    invoke-virtual {p0, p1, v0}, Lcom/epson/memcardacc/DirectorySelecterActivity;->setResult(ILandroid/content/Intent;)V

    .line 88
    invoke-virtual {p0}, Lcom/epson/memcardacc/DirectorySelecterActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0026

    .line 42
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lcom/epson/memcardacc/DirectorySelecterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0e03c4

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->setTitle(Ljava/lang/CharSequence;)V

    const p1, 0x7f08006e

    .line 45
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mBaseDirTextView:Landroid/widget/TextView;

    const p1, 0x7f08020e

    .line 46
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mTargetDir:Landroid/widget/EditText;

    const p1, 0x7f080234

    .line 48
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/DirectorySelecterActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mOkButton:Landroid/widget/Button;

    .line 50
    invoke-virtual {p0}, Lcom/epson/memcardacc/DirectorySelecterActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "baseDirectory"

    .line 51
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "default"

    .line 52
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 54
    iget-object v1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mBaseDirTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mTargetDir:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mTargetDir:Landroid/widget/EditText;

    new-instance v0, Lcom/epson/memcardacc/DirectorySelecterActivity$TargetDirTextWatcher;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/DirectorySelecterActivity$TargetDirTextWatcher;-><init>(Lcom/epson/memcardacc/DirectorySelecterActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 p1, 0x0

    .line 58
    iput-boolean p1, p0, Lcom/epson/memcardacc/DirectorySelecterActivity;->mButtonActionProgress:Z

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 106
    :cond_0
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0e04b4

    .line 107
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/DirectorySelecterActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    .line 108
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/DirectorySelecterActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/memcardacc/DirectorySelecterActivity$1;

    invoke-direct {v1, p0}, Lcom/epson/memcardacc/DirectorySelecterActivity$1;-><init>(Lcom/epson/memcardacc/DirectorySelecterActivity;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

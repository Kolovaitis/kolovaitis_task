.class public Lcom/epson/memcardacc/ConfirmWriteMemcard;
.super Lepson/print/ActivityIACommon;
.source "ConfirmWriteMemcard.java"

# interfaces
.implements Lepson/common/DialogProgress$DialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;,
        Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;
    }
.end annotation


# static fields
.field private static final ACTIVITY_TYPE_PROGRESS:I = 0x1

.field private static final DIALOG_CHECK_AND_NEXT:Ljava/lang/String; = "dialog_check_and_next"

.field private static final DIALOG_COMMUNICATION_ERROR:Ljava/lang/String; = "dialog_communication_error"

.field private static final DIALOG_COPY_RESULT_CANCEL:Ljava/lang/String; = "dialog_copy_result_cancel"

.field private static final DIALOG_COPY_RESULT_COMPLETE:Ljava/lang/String; = "dialog_copy_result_complete"

.field private static final DIALOG_COPY_RESULT_ERROR:Ljava/lang/String; = "dialog_copy_result_error"

.field private static final DIALOG_DISK_SPACE_SHORTAGE:Ljava/lang/String; = "dialog_disk_space_shortage"

.field private static final DIALOG_FILE_CONVERT:Ljava/lang/String; = "dialog_file_convert"

.field private static final DIALOG_FILE_WRITE_ERROR:Ljava/lang/String; = "dialog_file_write_error"

.field private static final DIALOG_PRINTER_BUSY:Ljava/lang/String; = "dialog_printer_busy"

.field private static final DIALOG_STORAGE_CONNECT_ERROR:Ljava/lang/String; = "dialog_storage_connect_error"

.field private static final DIALOG_TARGET_DIRECTORY_NAME_TASK:Ljava/lang/String; = "dialog_target_directy_name_task"

.field static final ERROR_COMMUNICATION_ERROR:I = 0xb

.field static final ERROR_FILE_WRITE_ERROR:I = 0xc

.field static final ERROR_MEMCARD_BUSY:I = 0xa

.field private static final KEY_IMAGE_LIST:Ljava/lang/String; = "imagelist"

.field private static final LOG_TAG:Ljava/lang/String; = "ConfirmWriteMemcard"

.field public static final RESULT_BACK_TO_MEMCARD_TOP:I = 0x1


# instance fields
.field doDirectoryNameTask:Z

.field isKeepSimleApConnect:Z

.field isTryConnectSimpleAp:Z

.field private mCheckAndNextActivity:Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;

.field private mCopyTargetFolderName:Ljava/lang/String;

.field private mCopyTargetPathText:Landroid/widget/TextView;

.field private mCopyTargetPrinter:Landroid/widget/TextView;

.field private mDirectoryNameTask:Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;

.field private mFileNumText:Landroid/widget/TextView;

.field private mFileSizeText:Landroid/widget/TextView;

.field private mImageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMemcardStorageType:I

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mTotalFileSize:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 99
    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mDirectoryNameTask:Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;

    const/4 v0, 0x0

    .line 104
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isTryConnectSimpleAp:Z

    .line 105
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isKeepSimleApConnect:Z

    .line 106
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->doDirectoryNameTask:Z

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/ConfirmWriteMemcard;Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->onUpdateViewModelData(Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;)V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/memcardacc/ConfirmWriteMemcard;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/epson/memcardacc/ConfirmWriteMemcard;)Landroid/widget/TextView;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetPathText:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/epson/memcardacc/ConfirmWriteMemcard;)Lepson/common/DialogProgressViewModel;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    return-object p0
.end method

.method static synthetic access$400(Lcom/epson/memcardacc/ConfirmWriteMemcard;)I
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->capacityCheck()I

    move-result p0

    return p0
.end method

.method static synthetic access$500(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->startCopyActivity()V

    return-void
.end method

.method private capacityCheck()I
    .locals 9

    const-string v0, "ConfirmWriteMemcard"

    const-string v1, "in capacityCheck()"

    .line 370
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getInstance()Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    const/4 v1, 0x1

    .line 375
    :try_start_0
    invoke-virtual {v0, p0, v1}, Lcom/epson/memcardacc/CifsAccess;->initDefault(Landroid/content/Context;I)I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, 0x2

    if-nez v2, :cond_0

    .line 385
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return v3

    .line 378
    :cond_0
    :try_start_1
    iget v2, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mMemcardStorageType:I

    invoke-virtual {v0, v2}, Lcom/epson/memcardacc/CifsAccess;->connectDefaultStorageWidthDefaultAthInfo(I)I

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_1

    .line 385
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return v3

    .line 381
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->getUnitSize()I

    move-result v2

    .line 382
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->getFreeUnit()J

    move-result-wide v3

    .line 383
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 385
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    const-string v0, "ConfirmWriteMemcard"

    .line 388
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unit :<"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "> free: <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, ">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-wide v5, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mTotalFileSize:J

    int-to-long v7, v2

    div-long/2addr v5, v7

    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-long v7, v0

    add-long/2addr v5, v7

    cmp-long v0, v5, v3

    if-lez v0, :cond_2

    return v1

    :cond_2
    const/4 v0, 0x0

    return v0

    :catchall_0
    move-exception v1

    .line 385
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    throw v1
.end method

.method private deleteTemporallyFile()V
    .locals 2

    .line 357
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;

    .line 358
    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;

    .line 359
    invoke-virtual {v0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->deleteTempFile()V

    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 589
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 591
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private getStartArgumentFileList(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v0, "imagelist"

    .line 200
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public static getStartIntent(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "imagelist"

    .line 115
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object v0
.end method

.method private getTargetFolderName(Lcom/epson/memcardacc/CifsAccess;)Ljava/lang/String;
    .locals 11

    .line 431
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const-string v1, "\\EPIMG"

    .line 434
    invoke-virtual {p1, v1}, Lcom/epson/memcardacc/CifsAccess;->createDirectory(Ljava/lang/String;)I

    move-result v1

    const/16 v2, -0x4b3

    const/4 v3, 0x0

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_0

    return-object v3

    :cond_0
    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    const/16 v5, 0x64

    if-ge v4, v5, :cond_2

    .line 446
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "\\EPIMG\\%04d%02d%02d%02d%02d%02d"

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x1

    .line 449
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v2

    const/4 v9, 0x2

    .line 450
    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v10

    add-int/2addr v10, v8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v8

    const/4 v8, 0x5

    .line 451
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    const/4 v9, 0x3

    const/16 v10, 0xb

    .line 452
    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    const/4 v9, 0x4

    const/16 v10, 0xc

    .line 453
    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    .line 454
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    .line 446
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 455
    invoke-virtual {p1, v5}, Lcom/epson/memcardacc/CifsAccess;->createDirectory(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    .line 459
    invoke-virtual {p1, v5}, Lcom/epson/memcardacc/CifsAccess;->deleteDirectory(Ljava/lang/String;)I

    return-object v5

    :cond_1
    const-string v5, "ConfirmWriteMemcard"

    .line 463
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CifsAccess.createDirectory() return <"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    return-object v3
.end method

.method public static synthetic lambda$onCreate$0(Lcom/epson/memcardacc/ConfirmWriteMemcard;Ljava/util/Deque;)V
    .locals 2

    .line 134
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 136
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 137
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 140
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 144
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 145
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private loggerCountUp()V
    .locals 1

    .line 520
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    return-void
.end method

.method private observeImageConvert(Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 204
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_file_convert"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    .line 205
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;

    .line 206
    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;

    .line 207
    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->setOrigFileList(Ljava/util/ArrayList;)V

    .line 209
    invoke-virtual {v0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->getData()Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/memcardacc/ConfirmWriteMemcard$1;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard$1;-><init>(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V

    invoke-virtual {p1, p0, v0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    return-void
.end method

.method private observeImageConvertFromIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->finish()V

    return-void

    .line 182
    :cond_0
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getStartArgumentFileList(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object p1

    if-nez p1, :cond_1

    .line 188
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->finish()V

    return-void

    .line 191
    :cond_1
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->updateFileInfo(Ljava/util/ArrayList;)V

    .line 193
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->observeImageConvert(Ljava/util/ArrayList;)V

    return-void
.end method

.method private onUpdateViewModelData(Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;)V
    .locals 2

    if-nez p1, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->finish()V

    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_file_convert"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    .line 225
    iget-wide v0, p1, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;->totalFileSize:J

    iput-wide v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mTotalFileSize:J

    .line 226
    new-instance v0, Ljava/util/ArrayList;

    iget-object p1, p1, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;->filenameList:Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mImageList:Ljava/util/ArrayList;

    .line 228
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mImageList:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->updateFileInfo(Ljava/util/ArrayList;)V

    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 10

    .line 535
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v0, "dialog_file_convert"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v0, "dialog_target_directy_name_task"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v0, "dialog_printer_busy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_3
    const-string v0, "dialog_copy_result_complete"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_4
    const-string v0, "dialog_file_write_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_5
    const-string v0, "dialog_storage_connect_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_6
    const-string v0, "dialog_check_and_next"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_7
    const-string v0, "dialog_disk_space_shortage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_8
    const-string v0, "dialog_copy_result_cancel"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_9
    const-string v0, "dialog_communication_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    const v2, 0x7f0e04f2

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto/16 :goto_2

    :pswitch_0
    const/4 v4, 0x2

    const v0, 0x7f0e03bd

    .line 576
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 577
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p1

    .line 576
    invoke-static/range {v3 .. v9}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_1
    const/4 v3, 0x2

    const v0, 0x7f0e03ba

    .line 570
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 571
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v2, p1

    .line 570
    invoke-static/range {v2 .. v8}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 572
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    goto/16 :goto_2

    :pswitch_2
    const/4 v3, 0x2

    const v0, 0x7f0e03b6

    .line 565
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f0e03b7

    .line 566
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p1

    .line 565
    invoke-static/range {v2 .. v8}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_3
    const/4 v0, 0x2

    const v1, 0x7f0e03c0

    .line 561
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v1, 0x7f0e03c1

    .line 562
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move v2, v0

    .line 561
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_4
    const/4 v0, 0x2

    const v1, 0x7f0e0217

    .line 557
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v1, 0x7f0e0218

    .line 558
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move v2, v0

    .line 557
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_5
    const/4 v0, 0x2

    const v1, 0x7f0e03c6

    .line 553
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v1, 0x7f0e03c7

    .line 554
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move v2, v0

    .line 553
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_6
    const/4 v0, 0x2

    const v1, 0x7f0e03b3

    .line 549
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v1, 0x7f0e03b4

    .line 550
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move v2, v0

    .line 549
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_7
    const v0, 0x7f0e03b5

    .line 545
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 546
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    goto :goto_2

    :pswitch_8
    const v0, 0x7f0e03b8

    .line 541
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 542
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    goto :goto_2

    :pswitch_9
    const-string v0, ""

    .line 537
    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 538
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    :goto_2
    if-eqz v0, :cond_1

    .line 581
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e289d18 -> :sswitch_9
        -0x7a2b4217 -> :sswitch_8
        -0x77723c79 -> :sswitch_7
        -0x610ffa77 -> :sswitch_6
        -0x2f81f008 -> :sswitch_5
        0x3a30817c -> :sswitch_4
        0x4739a0e8 -> :sswitch_3
        0x552f8495 -> :sswitch_2
        0x55f4e2d3 -> :sswitch_1
        0x7f21bf87 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private startCopyActivity()V
    .locals 3

    .line 472
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/MemcardWriteProgress;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "imageList"

    .line 473
    iget-object v2, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "target_folder_name"

    .line 475
    iget-object v2, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "memcard_storage_type"

    .line 477
    iget v2, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mMemcardStorageType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    .line 478
    invoke-virtual {p0, v0, v1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->startActivityForResult(Landroid/content/Intent;I)V

    .line 480
    iput-boolean v1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isKeepSimleApConnect:Z

    return-void
.end method

.method private updateFileInfo(Ljava/util/ArrayList;)V
    .locals 7
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 238
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 240
    :goto_0
    iget-object v1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mFileNumText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e03bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    .line 241
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v4, v0

    .line 240
    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mFileSizeText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e03d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-wide v3, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mTotalFileSize:J

    long-to-double v3, v3

    const-wide v5, 0x412e848000000000L    # 1000000.0

    div-double/2addr v3, v5

    .line 244
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v0

    .line 243
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public add_button_clicked(Landroid/view/View;)V
    .locals 0

    .line 334
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getStartArgumentFileList(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/epson/memcardacc/LocalImageSelectActivity;->startAddImageList(Landroid/app/Activity;Ljava/util/ArrayList;)V

    .line 335
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->finish()V

    return-void
.end method

.method cifsGetTargetFoldername()Ljava/lang/String;
    .locals 3

    .line 403
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getInstance()Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    const/4 v1, 0x1

    .line 407
    :try_start_0
    invoke-virtual {v0, p0, v1}, Lcom/epson/memcardacc/CifsAccess;->initDefault(Landroid/content/Context;I)I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 422
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v2

    .line 410
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->checkStorage()I

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_1

    .line 422
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v2

    .line 414
    :cond_1
    :try_start_2
    iput v1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mMemcardStorageType:I

    .line 415
    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/CifsAccess;->connectDefaultStorageWidthDefaultAthInfo(I)I

    move-result v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v1, :cond_2

    .line 422
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v2

    .line 419
    :cond_2
    :try_start_3
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getTargetFolderName(Lcom/epson/memcardacc/CifsAccess;)Ljava/lang/String;

    move-result-object v1

    .line 420
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 422
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    throw v1
.end method

.method public copy_button_clicked(Landroid/view/View;)V
    .locals 1

    .line 345
    new-instance p1, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;

    invoke-direct {p1, p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;-><init>(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V

    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCheckAndNextActivity:Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;

    .line 346
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCheckAndNextActivity:Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method isWirteDirNameDetermined()Z
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method localShowDialog(Ljava/lang/String;)V
    .locals 1

    .line 526
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {v0, p1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    .line 490
    iput-boolean p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isKeepSimleApConnect:Z

    const/4 p1, -0x2

    if-eq p2, p1, :cond_2

    if-eqz p2, :cond_1

    const-string p1, "dialog_copy_result_error"

    .line 507
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->localShowDialog(Ljava/lang/String;)V

    return-void

    :cond_1
    return-void

    .line 495
    :cond_2
    invoke-direct {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->loggerCountUp()V

    .line 496
    invoke-virtual {p0, p3}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->setResult(I)V

    .line 497
    invoke-direct {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->deleteTemporallyFile()V

    .line 498
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->finish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    .line 352
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->setResult(I)V

    .line 353
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->finish()V

    return-void
.end method

.method public onCancelDialog(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 124
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0023

    .line 125
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->setContentView(I)V

    .line 128
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgressViewModel;

    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 129
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/memcardacc/-$$Lambda$ConfirmWriteMemcard$2DhIiMizC7hn1AJJqGsAwJgjvkE;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/-$$Lambda$ConfirmWriteMemcard$2DhIiMizC7hn1AJJqGsAwJgjvkE;-><init>(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    const/4 p1, 0x1

    const v0, 0x7f0e03d2

    .line 151
    invoke-virtual {p0, v0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->setActionBar(IZ)V

    const v0, 0x7f08031f

    .line 153
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mFileNumText:Landroid/widget/TextView;

    const v0, 0x7f080320

    .line 154
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mFileSizeText:Landroid/widget/TextView;

    const v0, 0x7f08032f

    .line 155
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetPrinter:Landroid/widget/TextView;

    const v0, 0x7f08032e

    .line 156
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetPathText:Landroid/widget/TextView;

    .line 157
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetPathText:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 159
    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mImageList:Ljava/util/ArrayList;

    .line 160
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetPrinter:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e03cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, p1, [Ljava/lang/Object;

    .line 161
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v3

    invoke-virtual {v3}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 160
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iput-boolean p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->doDirectoryNameTask:Z

    .line 165
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->observeImageConvertFromIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onNegativeClick(Ljava/lang/String;)V
    .locals 4

    .line 607
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x7a2b4217    # -2.0006146E-35f

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_1

    const v1, 0x4739a0e8

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_copy_result_complete"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "dialog_copy_result_cancel"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, -0x1

    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_2

    .line 614
    :pswitch_0
    new-instance p1, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;

    invoke-direct {p1, p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;-><init>(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V

    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mDirectoryNameTask:Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;

    .line 615
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mDirectoryNameTask:Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;

    new-array v0, v2, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2

    .line 610
    :pswitch_1
    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->setResult(I)V

    .line 611
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->finish()V

    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onNeutralClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 277
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 278
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    invoke-direct {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->deleteTemporallyFile()V

    :cond_0
    return-void
.end method

.method public onPositiveClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onResume()V
    .locals 4

    const-string v0, "ConfirmWriteMemcard"

    const-string v1, "onResume()"

    .line 249
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "printer"

    .line 253
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 255
    iput-boolean v1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 256
    :cond_0
    iget-boolean v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isTryConnectSimpleAp:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 258
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isTryConnectSimpleAp:Z

    const-string v2, "printer"

    const/4 v3, -0x1

    .line 259
    invoke-static {p0, v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 260
    iput-boolean v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->doDirectoryNameTask:Z

    return-void

    .line 265
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isKeepSimleApConnect:Z

    .line 267
    iget-boolean v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->doDirectoryNameTask:Z

    if-eqz v0, :cond_2

    .line 269
    new-instance v0, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;-><init>(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V

    iput-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mDirectoryNameTask:Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;

    .line 270
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mDirectoryNameTask:Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;

    new-array v2, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/epson/memcardacc/ConfirmWriteMemcard$DirectoryNameTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 271
    iput-boolean v1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->doDirectoryNameTask:Z

    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "ConfirmWriteMemcard"

    const-string v1, "onStop()"

    .line 285
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    .line 290
    iget-boolean v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isKeepSimleApConnect:Z

    if-nez v0, :cond_0

    const-string v0, "printer"

    .line 291
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method setWriteDirName(Ljava/lang/String;)V
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    if-nez p1, :cond_0

    return-void

    .line 304
    :cond_0
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard;->mCopyTargetFolderName:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 305
    new-instance p1, Lcom/epson/memcardacc/ConfirmWriteMemcard$2;

    invoke-direct {p1, p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard$2;-><init>(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

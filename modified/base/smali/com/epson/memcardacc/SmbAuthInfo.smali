.class public Lcom/epson/memcardacc/SmbAuthInfo;
.super Ljava/lang/Object;
.source "SmbAuthInfo.java"


# instance fields
.field public mIsPasswordValid:Z

.field public mPassword:Ljava/lang/String;

.field public mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mIsPasswordValid:Z

    .line 10
    iput-object p1, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mUserName:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mPassword:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public clearData()V
    .locals 1

    const/4 v0, 0x0

    .line 23
    iput-boolean v0, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mIsPasswordValid:Z

    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mUserName:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mPassword:Ljava/lang/String;

    return-void
.end method

.method public setUserNameAndPassword(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 16
    iput-boolean v0, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mIsPasswordValid:Z

    .line 17
    iput-object p1, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mUserName:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mPassword:Ljava/lang/String;

    return-void
.end method

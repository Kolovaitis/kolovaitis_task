.class public Lcom/epson/memcardacc/CifsLib;
.super Ljava/lang/Object;
.source "CifsLib.java"


# instance fields
.field private mNativeInscance:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "cifs"

    .line 10
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native cancel()V
.end method

.method public native cancelByFileSizeZero()V
.end method

.method public synchronized native connectStrageNative([BLjava/lang/String;Ljava/lang/String;)I
.end method

.method public synchronized native createDirectory([B)I
.end method

.method public synchronized native deleteDirectory([B)I
.end method

.method public synchronized native deleteFile([B)I
.end method

.method public synchronized native disConnectStrage()I
.end method

.method public synchronized native free()V
.end method

.method public native getErrorCode()I
.end method

.method public synchronized native getFileList([B)[B
.end method

.method public synchronized native getFileSize([B)J
.end method

.method public synchronized native getFreeUnit()J
.end method

.method public synchronized native getStrageNames()[B
.end method

.method public synchronized native getUnitSize()I
.end method

.method public synchronized native init([BI)I
.end method

.method public synchronized native readFromPrinterMemcard([BLjava/lang/String;)I
.end method

.method public synchronized native writeToPrinterMemcard(Ljava/lang/String;[BI)I
.end method

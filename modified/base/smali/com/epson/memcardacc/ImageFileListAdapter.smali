.class public Lcom/epson/memcardacc/ImageFileListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ImageFileListAdapter.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;
    }
.end annotation


# static fields
.field private static final FILE_SELECT_NUMBER_LIMIT:I = 0x1e

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mBaseDir:Ljava/lang/String;

.field mFileInfoWithCheck:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;",
            ">;"
        }
    .end annotation
.end field

.field private transient mLayoutInflator:Landroid/view/LayoutInflater;

.field private mLayoutParams:Landroid/widget/AbsListView$LayoutParams;

.field mReaderSelectedImage:Lcom/epson/memcardacc/ReaderSelectedImage;

.field mReqHeight:I

.field mReqWidth:I

.field private mUpFolderString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    new-instance v0, Lcom/epson/memcardacc/ReaderSelectedImage;

    invoke-direct {v0}, Lcom/epson/memcardacc/ReaderSelectedImage;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mReaderSelectedImage:Lcom/epson/memcardacc/ReaderSelectedImage;

    .line 39
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    const-string v0, ""

    .line 45
    iput-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    const-string v0, ""

    .line 47
    iput-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mUpFolderString:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/epson/memcardacc/ImageFileListAdapter;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    return-object p0
.end method

.method private getSelectedFileList()Ljava/util/LinkedList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/CifsFileInfo;",
            ">;"
        }
    .end annotation

    .line 178
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 179
    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 180
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 181
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    .line 182
    iget v3, v2, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    if-eqz v3, :cond_0

    .line 183
    invoke-static {v2}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->access$000(Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;)Lcom/epson/memcardacc/CifsFileInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 212
    invoke-virtual {p0}, Lcom/epson/memcardacc/ImageFileListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getAllSelectedFile()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 239
    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 240
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 241
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    .line 242
    iget v3, v2, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    if-eqz v3, :cond_0

    .line 243
    invoke-virtual {v2}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 247
    :cond_1
    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mReaderSelectedImage:Lcom/epson/memcardacc/ReaderSelectedImage;

    invoke-virtual {v1}, Lcom/epson/memcardacc/ReaderSelectedImage;->getAllFileFullPath()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public getDirectoryName(I)Ljava/lang/String;
    .locals 0

    .line 226
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ImageFileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    .line 227
    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->getDirectoryName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getFileInfoIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;",
            ">;"
        }
    .end annotation

    .line 216
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedFileNum()I
    .locals 3

    .line 195
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    .line 196
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 197
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    .line 198
    iget v2, v2, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mReaderSelectedImage:Lcom/epson/memcardacc/ReaderSelectedImage;

    invoke-virtual {v0}, Lcom/epson/memcardacc/ReaderSelectedImage;->getFileSize()I

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method

.method public getTargetDirectoryName()Ljava/lang/String;
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 88
    iget-object p2, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mLayoutInflator:Landroid/view/LayoutInflater;

    const v1, 0x7f0a008c

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const p3, 0x7f080339

    .line 95
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 97
    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    .line 98
    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->getThumNail()Landroid/graphics/Bitmap;

    move-result-object v1

    const v2, 0x7f08006e

    .line 99
    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/4 v3, 0x4

    if-nez v1, :cond_4

    .line 101
    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 102
    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->isParentDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f070103

    .line 103
    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 104
    iget-object p3, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mUpFolderString:Ljava/lang/String;

    if-eqz p3, :cond_2

    .line 105
    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const v1, 0x7f0700a0

    .line 108
    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 109
    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->getFileName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    :cond_2
    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    const v1, 0x7f0700b5

    .line 114
    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 115
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 118
    :cond_4
    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 119
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    const p3, 0x7f0800bc

    .line 122
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 123
    iget p1, p1, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    if-eqz p1, :cond_5

    .line 124
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_5
    const/16 p1, 0x8

    .line 126
    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    return-object p2
.end method

.method public itemSelected(I)Z
    .locals 2

    .line 259
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ImageFileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    .line 261
    iget v0, p1, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    if-nez v0, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/epson/memcardacc/ImageFileListAdapter;->getSelectedFileNum()I

    move-result v0

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 267
    :cond_0
    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->toggleCheck()V

    .line 268
    invoke-virtual {p0}, Lcom/epson/memcardacc/ImageFileListAdapter;->notifyDataSetChanged()V

    const/4 p1, 0x1

    return p1
.end method

.method public setFileList(Ljava/lang/String;Ljava/util/LinkedList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/CifsFileInfo;",
            ">;)V"
        }
    .end annotation

    .line 149
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    const-string v0, "\\"

    goto :goto_0

    :cond_0
    move-object v0, p1

    .line 152
    :goto_0
    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 154
    invoke-direct {p0}, Lcom/epson/memcardacc/ImageFileListAdapter;->getSelectedFileList()Ljava/util/LinkedList;

    move-result-object v1

    .line 155
    iget-object v2, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mReaderSelectedImage:Lcom/epson/memcardacc/ReaderSelectedImage;

    iget-object v3, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/epson/memcardacc/ReaderSelectedImage;->setFileList(Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 156
    iput-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    .line 158
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    const-string v0, "\\"

    .line 159
    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getParentDirectory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 162
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    new-instance v1, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    invoke-direct {v1, p0, p1}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;-><init>(Lcom/epson/memcardacc/ImageFileListAdapter;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_1
    invoke-virtual {p2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/epson/memcardacc/CifsFileInfo;

    .line 165
    invoke-virtual {p2}, Lcom/epson/memcardacc/CifsFileInfo;->isReadable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    new-instance v0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;

    invoke-direct {v0, p0, p2}, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;-><init>(Lcom/epson/memcardacc/ImageFileListAdapter;Lcom/epson/memcardacc/CifsFileInfo;)V

    .line 167
    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mReaderSelectedImage:Lcom/epson/memcardacc/ReaderSelectedImage;

    iget-object v2, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    invoke-virtual {v1, v2, p2}, Lcom/epson/memcardacc/ReaderSelectedImage;->contains(Ljava/lang/String;Lcom/epson/memcardacc/CifsFileInfo;)Z

    move-result p2

    if-eqz p2, :cond_3

    const/4 p2, 0x1

    .line 168
    iput p2, v0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    .line 170
    :cond_3
    iget-object p2, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mFileInfoWithCheck:Ljava/util/LinkedList;

    invoke-virtual {p2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 173
    :cond_4
    iget-object p1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mReaderSelectedImage:Lcom/epson/memcardacc/ReaderSelectedImage;

    iget-object p2, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mBaseDir:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/epson/memcardacc/ReaderSelectedImage;->deleteBaseDir(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public setImageSize(II)V
    .locals 0

    .line 136
    iput p1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mReqWidth:I

    .line 137
    iput p2, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mReqHeight:I

    return-void
.end method

.method public setLayoutInflater(Landroid/view/LayoutInflater;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mLayoutInflator:Landroid/view/LayoutInflater;

    return-void
.end method

.method public setUpFolderString(Ljava/lang/String;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/epson/memcardacc/ImageFileListAdapter;->mUpFolderString:Ljava/lang/String;

    return-void
.end method

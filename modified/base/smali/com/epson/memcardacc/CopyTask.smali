.class Lcom/epson/memcardacc/CopyTask;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "MemcardWriteProgress.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "CopyTask"


# instance fields
.field mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

.field private mCanceling:Z

.field mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

.field private mCifsAccessStorageType:I

.field private mErrorCode:I

.field private mEscprCheck:Z

.field private mImageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLogData:Lcom/epson/iprint/prtlogger/CommonLog;

.field private mMemcardStorageType:I

.field private mTargetFolderName:Ljava/lang/String;

.field private mWriteFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/epson/memcardacc/MemcardWriteProgress;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Lcom/epson/iprint/prtlogger/CommonLog;)V
    .locals 1
    .param p1    # Lcom/epson/memcardacc/MemcardWriteProgress;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7    # Lcom/epson/iprint/prtlogger/CommonLog;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/memcardacc/MemcardWriteProgress;",
            "II",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/epson/iprint/prtlogger/CommonLog;",
            ")V"
        }
    .end annotation

    .line 458
    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    .line 427
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getInstance()Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    const/4 v0, 0x0

    .line 439
    iput v0, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    .line 445
    iput-boolean v0, p0, Lcom/epson/memcardacc/CopyTask;->mEscprCheck:Z

    .line 459
    iput-object p1, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    .line 460
    iput p2, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccessStorageType:I

    .line 461
    iput p3, p0, Lcom/epson/memcardacc/CopyTask;->mMemcardStorageType:I

    .line 462
    iput-object p4, p0, Lcom/epson/memcardacc/CopyTask;->mImageList:Ljava/util/ArrayList;

    .line 463
    iput-object p5, p0, Lcom/epson/memcardacc/CopyTask;->mWriteFileList:Ljava/util/ArrayList;

    .line 464
    iput-object p6, p0, Lcom/epson/memcardacc/CopyTask;->mTargetFolderName:Ljava/lang/String;

    .line 466
    iput-object p7, p0, Lcom/epson/memcardacc/CopyTask;->mLogData:Lcom/epson/iprint/prtlogger/CommonLog;

    return-void
.end method

.method private checkPrinterByEscprLib(Landroid/content/Context;)I
    .locals 3

    .line 519
    invoke-virtual {p0}, Lcom/epson/memcardacc/CopyTask;->getEscprLibPrinter()Lcom/epson/memcardacc/EscprLibPrinter;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x3c

    .line 520
    invoke-virtual {v0, p1, v2, v1}, Lcom/epson/memcardacc/EscprLibPrinter;->init(Landroid/content/Context;IZ)I

    move-result p1

    if-eqz p1, :cond_0

    return v1

    .line 526
    :cond_0
    invoke-virtual {v0}, Lcom/epson/memcardacc/EscprLibPrinter;->getStatus()I

    move-result p1

    .line 527
    invoke-virtual {v0}, Lcom/epson/memcardacc/EscprLibPrinter;->release()V

    if-eqz p1, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 p1, 0x2

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method protected checkFileSize(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .line 725
    invoke-static {p1}, Lcom/epson/memcardacc/MemcardUtil;->getFileLength(Ljava/lang/String;)J

    move-result-wide v0

    .line 726
    iget-object p1, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {p1, p2}, Lcom/epson/memcardacc/CifsAccess;->getFileSize(Ljava/lang/String;)J

    move-result-wide p1

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    cmp-long v5, p1, v3

    if-gtz v5, :cond_0

    return v2

    :cond_0
    cmp-long v3, v0, p1

    if-nez v3, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    return v2
.end method

.method protected copyFile()I
    .locals 13

    .line 623
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 625
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, -0x2

    if-eqz v3, :cond_9

    .line 626
    invoke-virtual {p0}, Lcom/epson/memcardacc/CopyTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    return v1

    .line 629
    :cond_0
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/CopyTask;->localPublishProgress(I)V

    .line 631
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 634
    iget-object v5, p0, Lcom/epson/memcardacc/CopyTask;->mWriteFileList:Ljava/util/ArrayList;

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz v5, :cond_1

    .line 636
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 637
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%s\\%s"

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/epson/memcardacc/CopyTask;->mTargetFolderName:Ljava/lang/String;

    aput-object v10, v6, v1

    aput-object v5, v6, v7

    invoke-static {v8, v9, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 641
    :cond_1
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s\\IMG_%04d.JPG"

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/epson/memcardacc/CopyTask;->mTargetFolderName:Ljava/lang/String;

    aput-object v9, v6, v1

    .line 642
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    .line 641
    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    add-int/lit8 v2, v2, 0x1

    const-string v6, "MemcardWriteProgress"

    .line 645
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "copy "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " => "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    invoke-static {v3}, Lcom/epson/memcardacc/MemcardUtil;->getFileLength(Ljava/lang/String;)J

    move-result-wide v8

    const-wide/32 v10, 0x7fffffff

    const/4 v6, 0x3

    cmp-long v12, v8, v10

    if-lez v12, :cond_2

    const/4 v0, 0x5

    .line 650
    iput v0, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    return v6

    :cond_2
    const/16 v10, -0x4b3

    .line 655
    :try_start_0
    iget-object v11, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    long-to-int v9, v8

    invoke-virtual {v11, v3, v5, v9}, Lcom/epson/memcardacc/CifsAccess;->writeToPrinterMemcard(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v8
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 657
    :try_start_1
    iget-object v11, p0, Lcom/epson/memcardacc/CopyTask;->mWriteFileList:Ljava/util/ArrayList;

    if-eqz v11, :cond_3

    if-eqz v8, :cond_3

    if-eq v8, v4, :cond_3

    iget-object v11, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    .line 659
    invoke-virtual {v11}, Lcom/epson/memcardacc/CifsAccess;->getErrorCode()I

    move-result v11

    if-ne v11, v10, :cond_3

    .line 662
    iget-object v11, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v11, v5}, Lcom/epson/memcardacc/CifsAccess;->deleteFile(Ljava/lang/String;)I

    .line 663
    iget-object v11, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v11, v3, v5, v9}, Lcom/epson/memcardacc/CifsAccess;->writeToPrinterMemcard(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v8
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v9

    goto :goto_2

    :catch_1
    move-exception v9

    const/4 v8, 0x0

    .line 668
    :goto_2
    invoke-virtual {v9}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :cond_3
    :goto_3
    if-eqz v8, :cond_8

    const-string v9, "MemcardWriteProgress"

    .line 671
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "writeFromPrinterMemcard() returns <"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, ">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    if-ne v8, v4, :cond_4

    .line 675
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0, v5}, Lcom/epson/memcardacc/CifsAccess;->deleteFile(Ljava/lang/String;)I

    return v1

    .line 678
    :cond_4
    iget-object v4, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v4}, Lcom/epson/memcardacc/CifsAccess;->getErrorCode()I

    move-result v4

    const/4 v8, 0x4

    if-ne v4, v10, :cond_6

    .line 681
    iget-object v4, p0, Lcom/epson/memcardacc/CopyTask;->mWriteFileList:Ljava/util/ArrayList;

    if-nez v4, :cond_5

    .line 682
    invoke-virtual {p0, v3, v5}, Lcom/epson/memcardacc/CopyTask;->checkFileSize(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    goto/16 :goto_0

    .line 685
    :cond_5
    iput v8, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    return v6

    .line 689
    :cond_6
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0, v5}, Lcom/epson/memcardacc/CifsAccess;->deleteFile(Ljava/lang/String;)I

    const/16 v0, -0x44c

    if-ne v4, v0, :cond_7

    .line 691
    iput v7, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    goto :goto_4

    .line 693
    :cond_7
    iput v8, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    :goto_4
    return v6

    .line 700
    :cond_8
    iget-object v3, p0, Lcom/epson/memcardacc/CopyTask;->mLogData:Lcom/epson/iprint/prtlogger/CommonLog;

    iput v2, v3, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    goto/16 :goto_0

    :cond_9
    return v4
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 7

    const/4 p1, 0x3

    .line 546
    :try_start_0
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardWriteProgress;->waitWifiConnect()Z

    move-result v0

    if-nez v0, :cond_0

    .line 547
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 550
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/CopyTask;->isCancelled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 551
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 553
    :cond_1
    :try_start_2
    iget-boolean v0, p0, Lcom/epson/memcardacc/CopyTask;->mEscprCheck:Z

    if-eqz v0, :cond_2

    .line 555
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/CopyTask;->checkPrinterByEscprLib(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    .line 556
    iget v0, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    if-eqz v0, :cond_2

    .line 557
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 561
    :cond_2
    :try_start_3
    invoke-virtual {p0}, Lcom/epson/memcardacc/CopyTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 562
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 564
    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    iget-object v1, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    iget v2, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccessStorageType:I

    invoke-virtual {v0, v1, v2}, Lcom/epson/memcardacc/CifsAccess;->initDefault(Landroid/content/Context;I)I

    move-result v0

    if-nez v0, :cond_4

    .line 565
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 567
    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    iget v1, p0, Lcom/epson/memcardacc/CopyTask;->mMemcardStorageType:I

    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/CifsAccess;->connectDefaultStorageWidthDefaultAthInfo(I)I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, -0x44c

    if-nez v0, :cond_6

    .line 568
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->getErrorCode()I

    move-result v0

    if-ne v0, v2, :cond_5

    .line 570
    iput v1, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    goto :goto_0

    .line 572
    :cond_5
    iput p1, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    .line 574
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 577
    :cond_6
    :try_start_6
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->getFreeUnit()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-gez v0, :cond_7

    .line 579
    iput p1, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    .line 580
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 583
    :cond_7
    :try_start_7
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mTargetFolderName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_a

    .line 585
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    iget-object v3, p0, Lcom/epson/memcardacc/CopyTask;->mTargetFolderName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/epson/memcardacc/CifsAccess;->createDirectory(Ljava/lang/String;)I

    move-result v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v0, :cond_9

    const/16 v3, -0x4b3

    if-eq v0, v3, :cond_9

    if-ne v0, v2, :cond_8

    .line 589
    :try_start_8
    iput v1, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    goto :goto_1

    :cond_8
    const/4 v1, 0x4

    .line 591
    iput v1, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    .line 593
    :goto_1
    iget-object v1, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v1}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 594
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    :catch_0
    move p1, v0

    goto :goto_2

    :cond_9
    move p1, v0

    .line 598
    :cond_a
    :try_start_9
    invoke-virtual {p0}, Lcom/epson/memcardacc/CopyTask;->copyFile()I

    move-result p1

    .line 599
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 600
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mLogData:Lcom/epson/iprint/prtlogger/CommonLog;

    iget-object v1, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    invoke-virtual {v1}, Lcom/epson/memcardacc/MemcardWriteProgress;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/iprint/prtlogger/CommonLog;->setConnectionType(Landroid/content/Context;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    .line 607
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    throw p1

    :catch_1
    :goto_2
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    .line 610
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 424
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/CopyTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected getEscprLibPrinter()Lcom/epson/memcardacc/EscprLibPrinter;
    .locals 1

    .line 509
    new-instance v0, Lcom/epson/memcardacc/EscprLibPrinter;

    invoke-direct {v0}, Lcom/epson/memcardacc/EscprLibPrinter;-><init>()V

    return-object v0
.end method

.method protected localPublishProgress(I)V
    .locals 2

    const/4 v0, 0x1

    .line 711
    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/CopyTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method protected onCancelled()V
    .locals 3

    const-string v0, "CopyTask"

    const-string v1, "canceled in AsyncTask"

    .line 483
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    iget-object v1, p0, Lcom/epson/memcardacc/CopyTask;->mLogData:Lcom/epson/iprint/prtlogger/CommonLog;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2, v1}, Lcom/epson/memcardacc/MemcardWriteProgress;->finishWithState(IILcom/epson/iprint/prtlogger/CommonLog;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3

    const-string v0, "CopyTask"

    const-string v1, "onPostExecute in AsyncTask"

    .line 490
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v1, p0, Lcom/epson/memcardacc/CopyTask;->mErrorCode:I

    iget-object v2, p0, Lcom/epson/memcardacc/CopyTask;->mLogData:Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-virtual {v0, p1, v1, v2}, Lcom/epson/memcardacc/MemcardWriteProgress;->finishWithState(IILcom/epson/iprint/prtlogger/CommonLog;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 424
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/CopyTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    const/4 v0, 0x0

    .line 497
    iput-boolean v0, p0, Lcom/epson/memcardacc/CopyTask;->mCanceling:Z

    .line 498
    iget-object v1, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    iget-object v2, p0, Lcom/epson/memcardacc/CopyTask;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/epson/memcardacc/MemcardWriteProgress;->setProgress(II)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2

    .line 504
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCallback:Lcom/epson/memcardacc/MemcardWriteProgress;

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v1, p0, Lcom/epson/memcardacc/CopyTask;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/epson/memcardacc/MemcardWriteProgress;->setProgress(II)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 424
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/CopyTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public setCheck(ZZ)V
    .locals 0

    .line 477
    iput-boolean p1, p0, Lcom/epson/memcardacc/CopyTask;->mEscprCheck:Z

    return-void
.end method

.method public taskCancel()V
    .locals 2

    .line 738
    iget-boolean v0, p0, Lcom/epson/memcardacc/CopyTask;->mCanceling:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 741
    iput-boolean v0, p0, Lcom/epson/memcardacc/CopyTask;->mCanceling:Z

    const-string v0, "CopyTask"

    const-string v1, "taskCancel()"

    .line 742
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->cancel()V

    const/4 v0, 0x0

    .line 744
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/CopyTask;->cancel(Z)Z

    return-void
.end method

.method public taskCancelByFileSizeZero()V
    .locals 2

    .line 753
    iget-boolean v0, p0, Lcom/epson/memcardacc/CopyTask;->mCanceling:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 756
    iput-boolean v0, p0, Lcom/epson/memcardacc/CopyTask;->mCanceling:Z

    const-string v0, "CopyTask"

    const-string v1, "taskCancel()"

    .line 757
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    iget-object v0, p0, Lcom/epson/memcardacc/CopyTask;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->cancelByFileSizeZero()V

    const/4 v0, 0x0

    .line 759
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/CopyTask;->cancel(Z)Z

    return-void
.end method

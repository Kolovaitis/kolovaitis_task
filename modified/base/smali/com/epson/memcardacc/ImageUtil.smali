.class public Lcom/epson/memcardacc/ImageUtil;
.super Ljava/lang/Object;
.source "ImageUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/ImageUtil$WidthHeight;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSampleSize(Lcom/epson/memcardacc/ImageUtil$WidthHeight;Lcom/epson/memcardacc/ImageUtil$WidthHeight;)I
    .locals 2

    .line 21
    iget v0, p0, Lcom/epson/memcardacc/ImageUtil$WidthHeight;->mWidth:I

    iget v1, p1, Lcom/epson/memcardacc/ImageUtil$WidthHeight;->mWidth:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/epson/memcardacc/ImageUtil$WidthHeight;->mHeight:I

    iget v1, p1, Lcom/epson/memcardacc/ImageUtil$WidthHeight;->mHeight:I

    if-ge v0, v1, :cond_0

    const/4 p0, 0x1

    return p0

    .line 26
    :cond_0
    iget v0, p0, Lcom/epson/memcardacc/ImageUtil$WidthHeight;->mWidth:I

    int-to-float v0, v0

    iget v1, p1, Lcom/epson/memcardacc/ImageUtil$WidthHeight;->mWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 27
    iget p0, p0, Lcom/epson/memcardacc/ImageUtil$WidthHeight;->mHeight:I

    int-to-float p0, p0

    iget p1, p1, Lcom/epson/memcardacc/ImageUtil$WidthHeight;->mHeight:I

    int-to-float p1, p1

    div-float/2addr p0, p1

    .line 28
    invoke-static {v0, p0}, Ljava/lang/Math;->min(FF)F

    move-result p0

    float-to-double p0, p0

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide p0

    double-to-int p0, p0

    return p0
.end method

.method public static getSize(Ljava/io/File;)Lcom/epson/memcardacc/ImageUtil$WidthHeight;
    .locals 2

    .line 10
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 12
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 13
    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 14
    new-instance p0, Lcom/epson/memcardacc/ImageUtil$WidthHeight;

    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {p0, v1, v0}, Lcom/epson/memcardacc/ImageUtil$WidthHeight;-><init>(II)V

    return-object p0
.end method

.method public static makeThumbNail(Ljava/io/File;Lcom/epson/memcardacc/ImageUtil$WidthHeight;)Landroid/graphics/Bitmap;
    .locals 2

    .line 40
    invoke-static {p0}, Lcom/epson/memcardacc/ImageUtil;->getSize(Ljava/io/File;)Lcom/epson/memcardacc/ImageUtil$WidthHeight;

    move-result-object v0

    .line 41
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 42
    invoke-static {v0, p1}, Lcom/epson/memcardacc/ImageUtil;->getSampleSize(Lcom/epson/memcardacc/ImageUtil$WidthHeight;Lcom/epson/memcardacc/ImageUtil$WidthHeight;)I

    move-result p1

    iput p1, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 44
    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

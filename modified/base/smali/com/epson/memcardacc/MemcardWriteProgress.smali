.class public Lcom/epson/memcardacc/MemcardWriteProgress;
.super Landroid/app/Activity;
.source "MemcardWriteProgress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;
    }
.end annotation


# static fields
.field public static final ERROR_CODE_COMMUNICATION_ERROR:I = 0x1

.field public static final ERROR_CODE_DISK_FULL:I = 0x5

.field public static final ERROR_CODE_INTERNAL_ERROR:I = 0x6

.field public static final ERROR_CODE_NO_ERROR:I = 0x0

.field public static final ERROR_CODE_PRINTER_BUSY:I = 0x2

.field public static final ERROR_CODE_STORAGE_CONNECCT_ERROR:I = 0x3

.field public static final ERROR_CODE_WRITE_ERROR:I = 0x4

.field public static final KEY_CHECK_WIFI_DIRECT:Ljava/lang/String; = "check_wifi_direct"

.field public static final KEY_CIFS_STORAGE_TYPE:Ljava/lang/String; = "cifs_storage_set_type"

.field public static final KEY_IMAGE_LIST:Ljava/lang/String; = "imageList"

.field public static final KEY_WRITE_FILE_LIST:Ljava/lang/String; = "write_file_list"

.field public static final KEY_WRITE_FOLDER_NAME:Ljava/lang/String; = "target_folder_name"

.field private static final LOG_TAG:Ljava/lang/String; = "MemcardWriteProgress"

.field public static final MEMCARD_STORAGE_TYPE:Ljava/lang/String; = "memcard_storage_type"

.field public static final RESULT_COMPLETE:I = -0x2

.field public static final RESULT_ERROR:I = 0x3

.field public static final RESULT_KEY_CIFS_ERROR_CODE:Ljava/lang/String; = "cifs_errorcode"


# instance fields
.field disconnectOnThreadFinish:Z

.field private mCancelButton:Landroid/widget/Button;

.field protected mCifsAccessStorageType:I

.field mCopyTask:Lcom/epson/memcardacc/CopyTask;

.field private mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

.field protected mImageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mMemcardStorageType:I

.field private mSheetPerTotalText:Landroid/widget/TextView;

.field protected mTargetFolderName:Ljava/lang/String;

.field private mTotalProgress:Landroid/widget/ProgressBar;

.field private mWifiDirectConnector:Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;

.field protected mWriteFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 90
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 145
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->disconnectOnThreadFinish:Z

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/MemcardWriteProgress;)V
    .locals 0

    .line 90
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->connectOk()V

    return-void
.end method

.method protected static checkWriteFile(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 244
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result p0

    const/4 v2, 0x0

    if-eq v1, p0, :cond_1

    return v2

    .line 247
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    const-string v1, ".*[\"*/:<>?\\\\|].*"

    .line 249
    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 251
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v1, 0x80

    if-lt p1, v1, :cond_2

    :cond_3
    return v2

    :cond_4
    return v0
.end method

.method private connectOk()V
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method private getLogData()Lcom/epson/iprint/prtlogger/CommonLog;
    .locals 2

    .line 224
    new-instance v0, Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/CommonLog;-><init>()V

    const/4 v1, 0x0

    .line 225
    iput v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    const/16 v1, 0x2001

    .line 226
    iput v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 227
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->getDefaultPrinterName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->printerName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected actionCancelTask()V
    .locals 2

    .line 293
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 294
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->taskCancel()V

    return-void
.end method

.method protected execCopyTask()V
    .locals 10

    const-string v0, "MemcardWriteProgress"

    const-string v1, "mCopyTask.execute()"

    .line 215
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    new-instance v0, Lcom/epson/memcardacc/CopyTask;

    iget v4, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCifsAccessStorageType:I

    iget v5, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mMemcardStorageType:I

    iget-object v6, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mImageList:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mWriteFileList:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mTargetFolderName:Ljava/lang/String;

    .line 219
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->getLogData()Lcom/epson/iprint/prtlogger/CommonLog;

    move-result-object v9

    move-object v2, v0

    move-object v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/epson/memcardacc/CopyTask;-><init>(Lcom/epson/memcardacc/MemcardWriteProgress;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Lcom/epson/iprint/prtlogger/CommonLog;)V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCopyTask:Lcom/epson/memcardacc/CopyTask;

    .line 220
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCopyTask:Lcom/epson/memcardacc/CopyTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/CopyTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public finishWithState(IILcom/epson/iprint/prtlogger/CommonLog;)V
    .locals 3
    .param p3    # Lcom/epson/iprint/prtlogger/CommonLog;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const-string v0, "MemcardWriteProgress"

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishWithState state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mWifiDirectConnector:Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->tryDisconnectWifiDirect()V

    if-eqz p3, :cond_0

    .line 328
    iget v0, p3, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    if-lez v0, :cond_0

    .line 329
    invoke-static {p0, p3}, Lcom/epson/iprint/prtlogger/Analytics;->sendCommonLog(Landroid/content/Context;Lcom/epson/iprint/prtlogger/CommonLog;)V

    :cond_0
    const/4 p3, 0x0

    if-eqz p2, :cond_1

    .line 334
    new-instance p3, Landroid/content/Intent;

    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    const-string v0, "cifs_errorcode"

    .line 335
    invoke-virtual {p3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 338
    :cond_1
    invoke-virtual {p0, p1, p3}, Lcom/epson/memcardacc/MemcardWriteProgress;->setResult(ILandroid/content/Intent;)V

    .line 339
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 168
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 169
    invoke-static {p0, p1}, Lepson/common/Utils;->setFInishOnTOuchOutside(Landroid/app/Activity;Z)V

    const v0, 0x7f0a0091

    .line 171
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardWriteProgress;->setContentView(I)V

    .line 173
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    const v0, 0x7f08025b

    .line 175
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardWriteProgress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mSheetPerTotalText:Landroid/widget/TextView;

    const v0, 0x7f08029d

    .line 176
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardWriteProgress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mTotalProgress:Landroid/widget/ProgressBar;

    .line 178
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "imageList"

    .line 179
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mImageList:Ljava/util/ArrayList;

    const-string v2, "target_folder_name"

    .line 180
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mTargetFolderName:Ljava/lang/String;

    const-string v2, "memcard_storage_type"

    .line 181
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mMemcardStorageType:I

    .line 182
    iget-object v2, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mTargetFolderName:Ljava/lang/String;

    const/4 v3, 0x3

    if-nez v2, :cond_0

    .line 183
    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/MemcardWriteProgress;->setResult(I)V

    .line 184
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->finish()V

    :cond_0
    const-string v2, "write_file_list"

    .line 186
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mWriteFileList:Ljava/util/ArrayList;

    .line 187
    iget-object v2, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mWriteFileList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 188
    iget-object v4, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mImageList:Ljava/util/ArrayList;

    invoke-static {v4, v2}, Lcom/epson/memcardacc/MemcardWriteProgress;->checkWriteFile(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 190
    invoke-virtual {p0, v3}, Lcom/epson/memcardacc/MemcardWriteProgress;->setResult(I)V

    .line 191
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->finish()V

    :cond_1
    const v2, 0x7f0800fa

    .line 194
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/MemcardWriteProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0e03ae

    .line 195
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    const-string v2, "cifs_storage_set_type"

    .line 197
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCifsAccessStorageType:I

    const-string v1, "check_wifi_direct"

    .line 199
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    .line 200
    new-instance v0, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;

    invoke-direct {v0, p0, p0, p1}, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;-><init>(Lcom/epson/memcardacc/MemcardWriteProgress;Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mWifiDirectConnector:Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;

    const p1, 0x7f0800b3

    .line 202
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardWriteProgress;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCancelButton:Landroid/widget/Button;

    .line 203
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCancelButton:Landroid/widget/Button;

    new-instance v0, Lcom/epson/memcardacc/MemcardWriteProgress$1;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/MemcardWriteProgress$1;-><init>(Lcom/epson/memcardacc/MemcardWriteProgress;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->execCopyTask()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 267
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 268
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mWifiDirectConnector:Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardWriteProgress$WifiDirectConnector;->tryConnectWifiDirect()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 261
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 262
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardUtil;->keepScreenOn(Landroid/view/Window;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "MemcardWriteProgress"

    const-string v1, "onStop()"

    .line 275
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardUtil;->clearKeepScreenOn(Landroid/view/Window;)V

    .line 280
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardWriteProgress;->actionCancelTask()V

    .line 285
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public setProgress(II)V
    .locals 1

    .line 311
    invoke-virtual {p0, p1, p2}, Lcom/epson/memcardacc/MemcardWriteProgress;->setProgressText(II)V

    .line 312
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mTotalProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 313
    iget-object p2, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mTotalProgress:Landroid/widget/ProgressBar;

    invoke-virtual {p2, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.method public setProgressText(II)V
    .locals 4

    .line 305
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mSheetPerTotalText:Landroid/widget/TextView;

    const-string v1, "%d/%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 306
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v2, p2

    .line 305
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected taskCancel()V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCopyTask:Lcom/epson/memcardacc/CopyTask;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CopyTask;->taskCancel()V

    return-void
.end method

.method protected waitWifiConnect()Z
    .locals 5

    const/4 v0, 0x0

    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardWriteProgress;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v1

    .line 355
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    return v0
.end method

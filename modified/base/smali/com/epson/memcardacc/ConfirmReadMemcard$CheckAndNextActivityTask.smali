.class Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;
.super Landroid/os/AsyncTask;
.source "ConfirmReadMemcard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/ConfirmReadMemcard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckAndNextActivityTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/ConfirmReadMemcard;)V
    .locals 0

    .line 538
    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1

    .line 546
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$400(Lcom/epson/memcardacc/ConfirmReadMemcard;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    const/16 p1, 0x64

    .line 549
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 551
    :cond_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->isCancelled()Z

    move-result p1

    const/4 v0, -0x1

    if-eqz p1, :cond_1

    .line 552
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 554
    :cond_1
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$500(Lcom/epson/memcardacc/ConfirmReadMemcard;)Z

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    .line 555
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 558
    :cond_2
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->isCancelled()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 559
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 561
    :cond_3
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    const/16 v0, 0x3c

    invoke-static {p1, v0}, Lcom/epson/memcardacc/EscprLibPrinter;->checkIdleOrError(Landroid/content/Context;I)I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0xa

    .line 571
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_1
    const/16 p1, 0xb

    .line 568
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 574
    :goto_0
    :pswitch_2
    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$400(Lcom/epson/memcardacc/ConfirmReadMemcard;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    const/4 p1, 0x0

    .line 576
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 538
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled()V
    .locals 2

    .line 582
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$300(Lcom/epson/memcardacc/ConfirmReadMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    .line 589
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$300(Lcom/epson/memcardacc/ConfirmReadMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    .line 590
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    return-void

    .line 601
    :sswitch_0
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    const-string v0, "dialog_communication_error"

    invoke-static {p1, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$200(Lcom/epson/memcardacc/ConfirmReadMemcard;Ljava/lang/String;)V

    return-void

    .line 598
    :sswitch_1
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    const-string v0, "dialog_printer_busy"

    invoke-static {p1, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$200(Lcom/epson/memcardacc/ConfirmReadMemcard;Ljava/lang/String;)V

    return-void

    .line 595
    :sswitch_2
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    const-string v0, "dialog_disk_space_shortage"

    invoke-static {p1, v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$200(Lcom/epson/memcardacc/ConfirmReadMemcard;Ljava/lang/String;)V

    return-void

    .line 592
    :sswitch_3
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {p1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$600(Lcom/epson/memcardacc/ConfirmReadMemcard;)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x1 -> :sswitch_2
        0xa -> :sswitch_1
        0xb -> :sswitch_0
    .end sparse-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 538
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 613
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$300(Lcom/epson/memcardacc/ConfirmReadMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    .line 614
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmReadMemcard;->access$400(Lcom/epson/memcardacc/ConfirmReadMemcard;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmReadMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmReadMemcard;

    const-string v1, "eproll"

    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/ConfirmReadMemcard;->setTargetDirectory(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

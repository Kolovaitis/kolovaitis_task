.class public Lcom/epson/memcardacc/DirectoryCache;
.super Ljava/lang/Object;
.source "DirectoryCache.java"


# instance fields
.field mCacheHashMap:Lcom/epson/memcardacc/LimitedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/epson/memcardacc/LimitedHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/CifsFileInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field mCifsAccess:Lcom/epson/memcardacc/CifsAccess;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Lcom/epson/memcardacc/LimitedHashMap;

    invoke-direct {v0}, Lcom/epson/memcardacc/LimitedHashMap;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/DirectoryCache;->mCacheHashMap:Lcom/epson/memcardacc/LimitedHashMap;

    return-void
.end method


# virtual methods
.method public getFileList(Ljava/lang/String;)Ljava/util/LinkedList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/CifsFileInfo;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/epson/memcardacc/DirectoryCache;->mCacheHashMap:Lcom/epson/memcardacc/LimitedHashMap;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/LimitedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    return-object v0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/DirectoryCache;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/CifsAccess;->getFileList(Ljava/lang/String;)Ljava/util/LinkedList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 24
    iget-object v1, p0, Lcom/epson/memcardacc/DirectoryCache;->mCacheHashMap:Lcom/epson/memcardacc/LimitedHashMap;

    invoke-virtual {v1, p1, v0}, Lcom/epson/memcardacc/LimitedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public setCifsAccess(Lcom/epson/memcardacc/CifsAccess;)V
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/epson/memcardacc/DirectoryCache;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    return-void
.end method

.class interface abstract Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;
.super Ljava/lang/Object;
.source "FileConvertTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/FileConvertTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "ConvertEndListener"
.end annotation


# virtual methods
.method public abstract onConvertEnd(Lcom/epson/memcardacc/FileConvertTask$ErrorType;Ljava/util/ArrayList;)V
    .param p1    # Lcom/epson/memcardacc/FileConvertTask$ErrorType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/memcardacc/FileConvertTask$ErrorType;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

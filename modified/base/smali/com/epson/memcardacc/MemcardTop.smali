.class public Lcom/epson/memcardacc/MemcardTop;
.super Lcom/epson/memcardacc/MemcardTopSuper;
.source "MemcardTop.java"


# static fields
.field private static final REQEST_RUNTIMEPERMMISSION:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardTopSuper;-><init>()V

    return-void
.end method


# virtual methods
.method public back_home_button_clicked(Landroid/view/View;)V
    .locals 0

    .line 129
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTop;->finish()V

    return-void
.end method

.method public copy_term_from_mem(Landroid/view/View;)V
    .locals 6

    .line 89
    iget p1, p0, Lcom/epson/memcardacc/MemcardTop;->mLaunchType:I

    if-eqz p1, :cond_0

    return-void

    .line 95
    :cond_0
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 96
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x2

    .line 97
    new-array v1, v0, [Ljava/lang/String;

    const v2, 0x7f0e0422

    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/MemcardTop;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v1, v4

    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/MemcardTop;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 98
    new-array v0, v0, [Ljava/lang/String;

    const v2, 0x7f0e0420

    .line 99
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/MemcardTop;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    .line 100
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/MemcardTop;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v5, 0x7f0e0424

    invoke-virtual {p0, v5}, Lcom/epson/memcardacc/MemcardTop;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v2, v5}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 106
    new-instance v2, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v4, p1, v4

    invoke-direct {v2, v4, v1, v0}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 108
    invoke-static {p0, p1}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 109
    invoke-static {p0, v2, v3}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 114
    :cond_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTop;->startReader()V

    return-void
.end method

.method public copy_term_to_mem(Landroid/view/View;)V
    .locals 0

    .line 75
    iget p1, p0, Lcom/epson/memcardacc/MemcardTop;->mLaunchType:I

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x1

    .line 79
    iput p1, p0, Lcom/epson/memcardacc/MemcardTop;->mLaunchType:I

    .line 81
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTop;->startMemcardStorageCheck()V

    return-void
.end method

.method protected getStorageSetType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public launchReaderActivity(I)V
    .locals 2

    .line 142
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "memcard_storage_type"

    .line 143
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 144
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardTop;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public launchWriterActivity()V
    .locals 2

    .line 135
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/LocalImageSelectActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 136
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardTop;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/epson/memcardacc/MemcardTopSuper;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_1

    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTop;->startReader()V

    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 27
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardBitmapCache;->getInstance(Landroid/content/Context;)Lcom/epson/memcardacc/MemcardBitmapCache;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardTop;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    .line 29
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTop;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTop;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardBitmapCache;->checkEnv()Z

    move-result v0

    if-nez v0, :cond_1

    .line 30
    :cond_0
    new-instance v0, Lcom/epson/memcardacc/MemcardTempAlertDialog;

    invoke-direct {v0}, Lcom/epson/memcardacc/MemcardTempAlertDialog;-><init>()V

    new-instance v1, Lcom/epson/memcardacc/MemcardTop$1;

    invoke-direct {v1, p0}, Lcom/epson/memcardacc/MemcardTop$1;-><init>(Lcom/epson/memcardacc/MemcardTop;)V

    invoke-virtual {v0, p0, v1}, Lcom/epson/memcardacc/MemcardTempAlertDialog;->showAlertDialog(Landroid/content/Context;Lcom/epson/memcardacc/MemcardTempAlertDialog$DialogCallback;)V

    :cond_1
    if-nez p1, :cond_2

    .line 41
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->clearSmbAuthInfo()V

    .line 44
    :cond_2
    invoke-super {p0, p1}, Lcom/epson/memcardacc/MemcardTopSuper;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0038

    .line 45
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardTop;->setContentView(I)V

    const p1, 0x7f0e022c

    const/4 v0, 0x1

    .line 48
    invoke-virtual {p0, p1, v0}, Lcom/epson/memcardacc/MemcardTop;->setActionBar(IZ)V

    const/4 p1, 0x0

    .line 50
    iput p1, p0, Lcom/epson/memcardacc/MemcardTop;->mLaunchType:I

    return-void
.end method

.method startReader()V
    .locals 1

    const/4 v0, 0x2

    .line 118
    iput v0, p0, Lcom/epson/memcardacc/MemcardTop;->mLaunchType:I

    .line 120
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTop;->startMemcardStorageCheck()V

    return-void
.end method

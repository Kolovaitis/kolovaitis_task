.class Lcom/epson/memcardacc/FileConvertTask;
.super Landroid/os/AsyncTask;
.source "FileConvertTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;,
        Lcom/epson/memcardacc/FileConvertTask$ErrorType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final FILE_SIZE_LIMIT:I = 0xa00000


# instance fields
.field private final mConvertEndListener:Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;

.field private mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

.field private final mOrgFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/ArrayList;Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;)V
    .locals 0
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;",
            ")V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertTask;->mOrgFileList:Ljava/util/ArrayList;

    .line 35
    iput-object p2, p0, Lcom/epson/memcardacc/FileConvertTask;->mConvertEndListener:Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;

    .line 36
    sget-object p1, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->NO_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertTask;->mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    return-void
.end method

.method static checkSize(Ljava/util/ArrayList;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    .line 43
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 46
    :try_start_0
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardUtil;->getFileLength(Ljava/lang/String;)J

    move-result-wide v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v1, 0x0

    :goto_0
    const-wide/32 v3, 0xa00000

    cmp-long p0, v1, v3

    if-lez p0, :cond_0

    return v0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method private convertImageFile(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 96
    invoke-static {}, Lcom/epson/memcardacc/FileConvertTask;->initTempDirectory()Ljava/io/File;

    move-result-object v1

    .line 98
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 99
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 102
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    .line 101
    invoke-static {v2, v3, v4}, Lepson/common/ExternalFileUtils;->getNotDuplicateFilename(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    if-nez v3, :cond_0

    .line 104
    sget-object p1, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->CONVERT_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertTask;->mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    return-object v4

    .line 107
    :cond_0
    invoke-static {v2, v3}, Lepson/common/ImageUtil;->convertHEIFtoJPEG(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 108
    sget-object p1, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->CONVERT_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertTask;->mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    return-object v4

    .line 111
    :cond_1
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method static initTempDirectory()Ljava/io/File;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 64
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v0

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    .line 65
    sget-object v1, Lepson/common/ExternalFileUtils$TempCacheDirectory;->MEMORY_CARD_ACCESS:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    invoke-virtual {v0, v1}, Lepson/common/ExternalFileUtils;->initTempCacheDirectory(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/FileConvertTask;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 80
    iget-object p1, p0, Lcom/epson/memcardacc/FileConvertTask;->mOrgFileList:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/epson/memcardacc/FileConvertTask;->convertImageFile(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 84
    :cond_0
    invoke-static {p1}, Lcom/epson/memcardacc/FileConvertTask;->checkSize(Ljava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 85
    sget-object p1, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->FILE_SIZE_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertTask;->mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    return-object v0

    :cond_1
    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/FileConvertTask;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/epson/memcardacc/FileConvertTask;->mConvertEndListener:Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;

    if-nez v0, :cond_0

    return-void

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/epson/memcardacc/FileConvertTask;->mErrorType:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    invoke-interface {v0, v1, p1}, Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;->onConvertEnd(Lcom/epson/memcardacc/FileConvertTask$ErrorType;Ljava/util/ArrayList;)V

    return-void
.end method

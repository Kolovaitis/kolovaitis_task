.class public Lcom/epson/memcardacc/AltViewImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "AltViewImageAdapter.java"

# interfaces
.implements Lepson/print/CommonDefine;


# static fields
.field private static final TAG:Ljava/lang/String; = "ViewImageAdapter"


# instance fields
.field itmH:I

.field itmW:I

.field private final mContext:Landroid/content/Context;

.field private mImageItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/print/ImageItem;",
            ">;"
        }
    .end annotation
.end field

.field mSelectPictureBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 36
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    const/4 v0, 0x0

    .line 34
    iput-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mSelectPictureBitmap:Landroid/graphics/Bitmap;

    .line 37
    iput-object p1, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mContext:Landroid/content/Context;

    .line 41
    new-instance p1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v0, 0x1

    .line 42
    iput-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    const/4 v0, 0x0

    .line 43
    iput-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mContext:Landroid/content/Context;

    .line 46
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070142

    .line 45
    invoke-static {v0, v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mSelectPictureBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "ViewImageAdapter"

    .line 49
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BitmapFactory.decodeResource Error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public add(Lepson/print/ImageItem;)V
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearImageItem()V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getImageItem(I)Lepson/print/ImageItem;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/ImageItem;

    return-object p1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    if-nez p2, :cond_0

    .line 67
    new-instance p2, Landroid/widget/RelativeLayout;

    iget-object p3, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 70
    new-instance p3, Landroid/widget/AbsListView$LayoutParams;

    iget v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->itmW:I

    iget v1, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->itmH:I

    invoke-direct {p3, v0, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 71
    invoke-virtual {p2, p3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 p3, 0x11

    .line 72
    invoke-virtual {p2, p3}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto :goto_0

    .line 74
    :cond_0
    check-cast p2, Landroid/widget/RelativeLayout;

    :goto_0
    const/4 p3, 0x0

    .line 80
    invoke-virtual {p2, p3}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, -0x1

    if-nez v0, :cond_1

    .line 82
    new-instance v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 83
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 89
    invoke-virtual {p2, v0, p3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 96
    :cond_1
    iget-object v2, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    if-ltz v2, :cond_2

    iget-object v2, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, p1, :cond_2

    .line 98
    :try_start_0
    iget-object v2, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/ImageItem;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string p3, "null pointer Exception"

    const-string v0, "line 46 <> View Image Adapter"

    .line 100
    invoke-static {p3, v0}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p1}, Ljava/lang/NullPointerException;->printStackTrace()V

    return-object p2

    :cond_2
    move-object p1, v3

    :goto_1
    if-nez p1, :cond_3

    return-object p2

    .line 111
    :cond_3
    :try_start_1
    invoke-virtual {p1}, Lepson/print/ImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 112
    invoke-virtual {p1}, Lepson/print/ImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 114
    :cond_4
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v2, -0x333334

    .line 115
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    const-string v0, "null"

    const-string v2, "bitmap is null"

    .line 116
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v2, "null pointer Exception"

    const-string v4, "line 61 <> View Image Adapter"

    .line 120
    invoke-static {v2, v4}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    :goto_2
    const/4 v0, 0x1

    .line 128
    :try_start_2
    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 129
    invoke-virtual {p1}, Lepson/print/ImageItem;->getSelected()I

    move-result p1

    if-eqz p1, :cond_6

    if-nez v2, :cond_5

    .line 131
    new-instance p1, Landroid/widget/ImageView;

    iget-object p3, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p1, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    .line 133
    :try_start_3
    new-instance p3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p3, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    sget-object p3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 137
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 138
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 139
    iget-object p3, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mSelectPictureBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    :catch_2
    move-exception p3

    :try_start_4
    const-string v1, "ViewImageAdapter"

    .line 141
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setImageBitmap Error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {v1, p3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-static {}, Ljava/lang/System;->runFinalization()V

    .line 143
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 145
    :goto_3
    invoke-virtual {p2, p1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    goto :goto_4

    .line 147
    :cond_5
    invoke-virtual {v2, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    :cond_6
    if-eqz v2, :cond_7

    const/4 p1, 0x4

    .line 151
    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_4

    :catch_3
    move-exception p1

    const-string p3, "null pointer Exception"

    const-string v0, "line 73 <> View Image Adapter"

    .line 154
    invoke-static {p3, v0}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p1}, Ljava/lang/NullPointerException;->printStackTrace()V

    :cond_7
    :goto_4
    return-object p2
.end method

.method public recycleBitmap()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/ImageItem;

    .line 195
    invoke-virtual {v1}, Lepson/print/ImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 198
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setImageSize(II)V
    .locals 0

    .line 54
    iput p1, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->itmW:I

    .line 55
    iput p2, p0, Lcom/epson/memcardacc/AltViewImageAdapter;->itmH:I

    return-void
.end method

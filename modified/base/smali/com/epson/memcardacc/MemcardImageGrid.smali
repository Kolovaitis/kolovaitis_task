.class public Lcom/epson/memcardacc/MemcardImageGrid;
.super Lepson/print/ActivityIACommon;
.source "MemcardImageGrid.java"

# interfaces
.implements Lepson/common/DialogProgress$DialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;,
        Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;
    }
.end annotation


# static fields
.field static final ACTIVITY_TYPE_CONFIRM_COPY:I = 0x1

.field private static final DIALOG_FILE_READ_ERROR:Ljava/lang/String; = "dialog_file_read_error"

.field private static final DIALOG_NO_IMAGE_SELECTED:Ljava/lang/String; = "dialog_no_image_selected"

.field private static final DIALOG_OVER_SELECTED_NUMBER:Ljava/lang/String; = "dialog_over_selected_number"

.field private static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field private static final LOG_TAG:Ljava/lang/String; = "MemcardImageGrid"

.field public static final MEMCARD_STORAGE_TYPE:Ljava/lang/String; = "memcard_storage_type"

.field static final MESSAGE_EXEC_NEXT_TASK_OR_IDLE:I = 0x2

.field static final MESSAGE_WAIT_TASK_FINISHED_AND_NEXT_TASK:I = 0x1

.field static final NEXT_TASK_WAITE:J = 0x12cL

.field public static final RESULT_BACK_TO_MEMCARD_TOP:I = 0x1

.field protected static final TASK_WAITE_PRIOD:J = 0x3e8L


# instance fields
.field discconectOnThreadFinish:Z

.field private isFinishRequested:Z

.field isKeepSimleApConnect:Z

.field isTryConnectSimpleAp:Z

.field private mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

.field private mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

.field private mCifsFileListLoader:Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

.field private mDirectoryCache:Lcom/epson/memcardacc/DirectoryCache;

.field private mGridView:Landroid/widget/GridView;

.field private mHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

.field private mMemcardStorageType:I

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSelectedFileNumText:Landroid/widget/TextView;

.field public mStartDirectory:Z

.field private mTaskQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I

.field private targetDir:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 45
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 88
    iput-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->targetDir:Ljava/lang/String;

    .line 95
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mTaskQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v0, 0x0

    .line 108
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isTryConnectSimpleAp:Z

    .line 109
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->discconectOnThreadFinish:Z

    .line 110
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isKeepSimleApConnect:Z

    .line 526
    new-instance v0, Lcom/epson/memcardacc/MemcardImageGrid$3;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/MemcardImageGrid$3;-><init>(Lcom/epson/memcardacc/MemcardImageGrid;)V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mCifsFileListLoader:Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    return-object p0
.end method

.method static synthetic access$100(Lcom/epson/memcardacc/MemcardImageGrid;)Landroid/os/Handler;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$200(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/CifsAccess;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    return-object p0
.end method

.method static synthetic access$300(Lcom/epson/memcardacc/MemcardImageGrid;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mMemcardStorageType:I

    return p0
.end method

.method static synthetic access$400(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/DirectoryCache;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mDirectoryCache:Lcom/epson/memcardacc/DirectoryCache;

    return-object p0
.end method

.method static synthetic access$500(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/ImageFileListAdapter;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    return-object p0
.end method

.method static synthetic access$600(Lcom/epson/memcardacc/MemcardImageGrid;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/epson/memcardacc/MemcardImageGrid;Ljava/lang/String;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->localShowDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/MemcardBitmapCache;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    return-object p0
.end method

.method private chengeTargetDirectory(Ljava/lang/String;)V
    .locals 2

    const-string v0, "MemcardImageGrid"

    const-string v1, "chengeTargetDirectory() start"

    .line 429
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->showProgressDialog()V

    .line 433
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mTaskQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 434
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->cancelFileListLoadTask()V

    .line 436
    :try_start_0
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mTaskQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 445
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :catch_0
    move-exception p1

    .line 438
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    const-string p1, "MemcardImageGrid"

    const-string v0, "queue.put() error in chengeTargetDirectory()"

    .line 439
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->dismissProgressDialog()V

    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 520
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 522
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private dismissProgressDialog()V
    .locals 2

    .line 484
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method private finishMemCardImageGridActivity()V
    .locals 2

    .line 413
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 414
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->cancelFileListLoadTask()V

    .line 415
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->finish()V

    .line 416
    iput-boolean v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isFinishRequested:Z

    return-void
.end method

.method private goNext()V
    .locals 3

    .line 386
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->showProgressDialog()V

    .line 387
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->getSelectedFileNum()I

    move-result v0

    if-gtz v0, :cond_0

    .line 389
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->dismissProgressDialog()V

    const-string v0, "dialog_no_image_selected"

    .line 390
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->localShowDialog(Ljava/lang/String;)V

    return-void

    .line 393
    :cond_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->cancelFileListLoadTask()V

    .line 394
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/ConfirmReadMemcard;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "imagelist"

    .line 395
    iget-object v2, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    .line 396
    invoke-virtual {v2}, Lcom/epson/memcardacc/ImageFileListAdapter;->getAllSelectedFile()Ljava/util/ArrayList;

    move-result-object v2

    .line 395
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "memcard_storage_type"

    .line 397
    iget v2, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mMemcardStorageType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    .line 398
    invoke-virtual {p0, v0, v1}, Lcom/epson/memcardacc/MemcardImageGrid;->startActivityForResult(Landroid/content/Intent;I)V

    .line 399
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->dismissProgressDialog()V

    .line 401
    iput-boolean v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isKeepSimleApConnect:Z

    return-void
.end method

.method public static synthetic lambda$onCreate$0(Lcom/epson/memcardacc/MemcardImageGrid;Ljava/util/Deque;)V
    .locals 2

    .line 201
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 203
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 204
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 207
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 211
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 212
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private localShowDialog(Ljava/lang/String;)V
    .locals 1

    .line 421
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {v0, p1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 10

    .line 493
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x35da03fa    # -2719489.5f

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    const v1, -0x323c0967

    if-eq v0, v1, :cond_2

    const v1, -0xba25c35

    if-eq v0, v1, :cond_1

    const v1, -0x14b98bc

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "dialog_file_read_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    const-string v0, "dialog_over_selected_number"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const-string v0, "dialog_no_image_selected"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v0, -0x1

    :goto_1
    const v1, 0x7f0e04f2

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_2

    :pswitch_0
    const/4 v4, 0x2

    const v0, 0x7f0e03b7

    .line 507
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const v0, 0x7f0e0476

    .line 508
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const v0, 0x7f0e04fb

    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v3, p1

    .line 507
    invoke-static/range {v3 .. v9}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    const/4 v2, 0x2

    const v0, 0x7f0e03c9

    .line 503
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0e03ca

    .line 504
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/MemcardImageGrid;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    .line 503
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    :pswitch_2
    const/4 v2, 0x2

    const v0, 0x7f0e03a7

    .line 499
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 500
    invoke-virtual {p0, v1}, Lcom/epson/memcardacc/MemcardImageGrid;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, p1

    .line 499
    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    goto :goto_2

    .line 495
    :pswitch_3
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e03b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 496
    invoke-virtual {v0, v2}, Lepson/common/DialogProgress;->setCancelable(Z)V

    :goto_2
    if-eqz v0, :cond_5

    .line 512
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showProgressDialog()V
    .locals 2

    .line 479
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method cancelFileListLoadTask()V
    .locals 1

    .line 472
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mCifsFileListLoader:Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    if-eqz v0, :cond_0

    .line 473
    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->cancelTask()V

    :cond_0
    return-void
.end method

.method dispTargetDirectory()V
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->getTargetDirectoryName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected execNextTaskOrIdle()V
    .locals 4

    .line 455
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mTaskQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "MemcardImageGrid"

    const-string v1, "checkQueue() queue not empty"

    .line 456
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    new-instance v0, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;-><init>(Lcom/epson/memcardacc/MemcardImageGrid;)V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mCifsFileListLoader:Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    const-string v0, ""

    .line 460
    :goto_0
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mTaskQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 461
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mTaskQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 463
    :cond_0
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mCifsFileListLoader:Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 466
    :cond_1
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->dismissProgressDialog()V

    :goto_1
    return-void
.end method

.method gridViewItemClicked(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 304
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {p1, p3}, Lcom/epson/memcardacc/ImageFileListAdapter;->getDirectoryName(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p2, "MemcardImageGrid"

    .line 306
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "item clicked : dir_name => "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->chengeTargetDirectory(Ljava/lang/String;)V

    goto :goto_0

    .line 311
    :cond_0
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {p1, p3}, Lcom/epson/memcardacc/ImageFileListAdapter;->itemSelected(I)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 312
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->updateSelectedNumber()V

    goto :goto_0

    :cond_1
    const-string p1, "dialog_over_selected_number"

    .line 315
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->localShowDialog(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method hasExtensionJpg(Ljava/lang/String;)Z
    .locals 1

    .line 589
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, ".jpg"

    .line 590
    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    .line 591
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method

.method public next_button_clicked(Landroid/view/View;)V
    .locals 0

    .line 382
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->goNext()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 284
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p1, 0x1

    if-eq p2, p1, :cond_0

    goto :goto_0

    :cond_0
    if-eq p2, p1, :cond_1

    :goto_0
    return-void

    .line 290
    :cond_1
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->setResult(I)V

    .line 291
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->finishMemCardImageGridActivity()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 408
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->finishMemCardImageGridActivity()V

    return-void
.end method

.method public onCancelDialog(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 119
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardBitmapCache;->getInstance(Landroid/content/Context;)Lcom/epson/memcardacc/MemcardBitmapCache;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    .line 121
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardBitmapCache;->checkEnv()Z

    move-result v0

    if-nez v0, :cond_1

    .line 123
    :cond_0
    new-instance v0, Lcom/epson/memcardacc/MemcardTempAlertDialog;

    invoke-direct {v0}, Lcom/epson/memcardacc/MemcardTempAlertDialog;-><init>()V

    new-instance v1, Lcom/epson/memcardacc/MemcardImageGrid$1;

    invoke-direct {v1, p0}, Lcom/epson/memcardacc/MemcardImageGrid$1;-><init>(Lcom/epson/memcardacc/MemcardImageGrid;)V

    invoke-virtual {v0, p0, v1}, Lcom/epson/memcardacc/MemcardTempAlertDialog;->showAlertDialog(Landroid/content/Context;Lcom/epson/memcardacc/MemcardTempAlertDialog$DialogCallback;)V

    .line 133
    :cond_1
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getInstance()Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    .line 134
    new-instance v0, Lcom/epson/memcardacc/DirectoryCache;

    invoke-direct {v0}, Lcom/epson/memcardacc/DirectoryCache;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mDirectoryCache:Lcom/epson/memcardacc/DirectoryCache;

    .line 135
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mDirectoryCache:Lcom/epson/memcardacc/DirectoryCache;

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/DirectoryCache;->setCifsAccess(Lcom/epson/memcardacc/CifsAccess;)V

    .line 137
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 142
    new-instance p1, Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-direct {p1}, Lcom/epson/memcardacc/ImageFileListAdapter;-><init>()V

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    .line 143
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->setLayoutInflater(Landroid/view/LayoutInflater;)V

    .line 144
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    .line 145
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e03c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->setUpFolderString(Ljava/lang/String;)V

    const p1, 0x7f0a0037

    .line 146
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->setContentView(I)V

    const-string p1, ""

    const/4 v0, 0x1

    .line 149
    invoke-virtual {p0, p1, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->setActionBar(Ljava/lang/String;Z)V

    const p1, 0x7f080167

    .line 151
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/GridView;

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mGridView:Landroid/widget/GridView;

    .line 152
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {p1, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    const p1, 0x7f080321

    .line 153
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mSelectedFileNumText:Landroid/widget/TextView;

    .line 154
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mGridView:Landroid/widget/GridView;

    new-instance v1, Lcom/epson/memcardacc/MemcardImageGrid$2;

    invoke-direct {v1, p0}, Lcom/epson/memcardacc/MemcardImageGrid$2;-><init>(Lcom/epson/memcardacc/MemcardImageGrid;)V

    invoke-virtual {p1, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 162
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f060064

    .line 163
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mThumbnailWidth:I

    .line 164
    iget p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mThumbnailWidth:I

    iput p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mThumbnailHeight:I

    .line 165
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    iget v2, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mThumbnailHeight:I

    invoke-virtual {v1, p1, v2}, Lcom/epson/memcardacc/MemcardBitmapCache;->setSize(II)V

    .line 166
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    iget v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mThumbnailWidth:I

    iget v2, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mThumbnailHeight:I

    invoke-virtual {p1, v1, v2}, Lcom/epson/memcardacc/ImageFileListAdapter;->setImageSize(II)V

    .line 169
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f060065

    .line 170
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 172
    new-instance v1, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;

    iget-object v2, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mGridView:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-direct {v1, v2, v3, p1}, Lcom/epson/memcardacc/MemcardImageGrid$MyGridLayoutListener;-><init>(Landroid/widget/GridView;Lcom/epson/memcardacc/ImageFileListAdapter;I)V

    .line 174
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p1}, Landroid/widget/GridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 177
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->updateSelectedNumber()V

    const p1, 0x7f080292

    .line 179
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mProgressBar:Landroid/widget/ProgressBar;

    .line 180
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 183
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "memcard_storage_type"

    .line 184
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mMemcardStorageType:I

    .line 185
    iget p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mMemcardStorageType:I

    if-gtz p1, :cond_2

    .line 187
    iput v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mMemcardStorageType:I

    .line 190
    :cond_2
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mStartDirectory:Z

    const-string p1, "\\DCIM"

    .line 192
    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->targetDir:Ljava/lang/String;

    .line 195
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgressViewModel;

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 196
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/memcardacc/-$$Lambda$MemcardImageGrid$nQEvWxmQCbj3qrczu4IA7fQ5x7k;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/-$$Lambda$MemcardImageGrid$nQEvWxmQCbj3qrczu4IA7fQ5x7k;-><init>(Lcom/epson/memcardacc/MemcardImageGrid;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .line 322
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->getSelectedFileNum()I

    move-result v0

    .line 324
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f080044

    .line 325
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lez v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 330
    :goto_0
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    if-gtz v0, :cond_1

    .line 333
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {p1}, Lcom/epson/memcardacc/ImageFileListAdapter;->getTargetDirectoryName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 336
    :cond_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v3, 0x7f0e042c

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-array v3, v2, [Ljava/lang/Object;

    .line 337
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    .line 336
    invoke-static {p1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->setTitle(Ljava/lang/CharSequence;)V

    :goto_1
    return v2
.end method

.method public onNegativeClick(Ljava/lang/String;)V
    .locals 2

    .line 611
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0xba25c35

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_file_read_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 p1, 0x1

    .line 614
    iput-boolean p1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mStartDirectory:Z

    const-string p1, "\\DCIM"

    .line 615
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/MemcardImageGrid;->chengeTargetDirectory(Ljava/lang/String;)V

    :goto_2
    return-void
.end method

.method public onNeutralClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 346
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080044

    if-ne v0, v1, :cond_0

    .line 348
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->goNext()V

    const/4 p1, 0x1

    return p1

    .line 352
    :cond_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPositiveClick(Ljava/lang/String;)V
    .locals 2

    .line 596
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0xba25c35

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_file_read_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    .line 599
    :cond_2
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->finishMemCardImageGridActivity()V

    :goto_2
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .line 249
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onRestart()V

    .line 251
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mImageFileListAdapter:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-virtual {v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->getTargetDirectoryName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->targetDir:Ljava/lang/String;

    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 256
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "printer"

    .line 259
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 261
    iput-boolean v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 262
    :cond_0
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isTryConnectSimpleAp:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 264
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isTryConnectSimpleAp:Z

    const-string v0, "printer"

    const/4 v2, -0x1

    .line 265
    invoke-static {p0, v0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 270
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isKeepSimleApConnect:Z

    .line 271
    iput-boolean v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->discconectOnThreadFinish:Z

    .line 274
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isFinishRequested:Z

    if-nez v0, :cond_2

    .line 277
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->targetDir:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->chengeTargetDirectory(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 3

    .line 229
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 231
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->isKeepSimleApConnect:Z

    if-nez v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->mCifsFileListLoader:Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v2, :cond_0

    .line 234
    iput-boolean v1, p0, Lcom/epson/memcardacc/MemcardImageGrid;->discconectOnThreadFinish:Z

    .line 236
    :cond_0
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardImageGrid;->discconectOnThreadFinish:Z

    if-nez v0, :cond_1

    const-string v0, "MemcardImageGrid"

    const-string v1, "discconect at onStop()"

    .line 237
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "printer"

    .line 238
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 242
    :cond_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->cancelFileListLoadTask()V

    .line 243
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    return-void
.end method

.method selectFileList(Ljava/util/LinkedList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/CifsFileInfo;",
            ">;)V"
        }
    .end annotation

    .line 570
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 571
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 572
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/memcardacc/CifsFileInfo;

    .line 576
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsFileInfo;->isReadable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 577
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsFileInfo;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardImageGrid;->hasExtensionJpg(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 578
    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public updateSelectedNumber()V
    .locals 0

    .line 371
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardImageGrid;->supportInvalidateOptionsMenu()V

    return-void
.end method

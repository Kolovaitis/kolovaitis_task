.class Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;
.super Landroid/os/AsyncTask;
.source "ConfirmWriteMemcard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/ConfirmWriteMemcard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckAndNextActivityTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V
    .locals 0

    .line 664
    iput-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1

    .line 672
    invoke-virtual {p0}, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->isCancelled()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    .line 673
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 675
    :cond_0
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    const/16 v0, 0x3c

    invoke-static {p1, v0}, Lcom/epson/memcardacc/EscprLibPrinter;->checkIdleOrError(Landroid/content/Context;I)I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0xa

    .line 685
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_1
    const/16 p1, 0xb

    .line 682
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 688
    :goto_0
    :pswitch_2
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-virtual {p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->isWirteDirNameDetermined()Z

    move-result p1

    if-nez p1, :cond_3

    .line 689
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-virtual {p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->cifsGetTargetFoldername()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 690
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    goto :goto_1

    .line 693
    :cond_1
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->setWriteDirName(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    :goto_1
    const/16 p1, 0xc

    .line 691
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 696
    :cond_3
    :goto_2
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-static {p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->access$400(Lcom/epson/memcardacc/ConfirmWriteMemcard;)I

    move-result p1

    if-eqz p1, :cond_4

    .line 698
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_4
    const/4 p1, 0x0

    .line 700
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 664
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled()V
    .locals 2

    .line 713
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->access$300(Lcom/epson/memcardacc/ConfirmWriteMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_check_and_next"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    .line 719
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->access$300(Lcom/epson/memcardacc/ConfirmWriteMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_check_and_next"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    .line 720
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    return-void

    .line 737
    :pswitch_0
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    const-string v0, "dialog_file_write_error"

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->localShowDialog(Ljava/lang/String;)V

    return-void

    .line 734
    :pswitch_1
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    const-string v0, "dialog_communication_error"

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->localShowDialog(Ljava/lang/String;)V

    return-void

    .line 731
    :pswitch_2
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    const-string v0, "dialog_printer_busy"

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->localShowDialog(Ljava/lang/String;)V

    return-void

    .line 728
    :pswitch_3
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    const-string v0, "dialog_storage_connect_error"

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->localShowDialog(Ljava/lang/String;)V

    return-void

    .line 725
    :pswitch_4
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    const-string v0, "dialog_disk_space_shortage"

    invoke-virtual {p1, v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->localShowDialog(Ljava/lang/String;)V

    return-void

    .line 722
    :pswitch_5
    iget-object p1, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-static {p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->access$500(Lcom/epson/memcardacc/ConfirmWriteMemcard;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 664
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 745
    iget-object v0, p0, Lcom/epson/memcardacc/ConfirmWriteMemcard$CheckAndNextActivityTask;->this$0:Lcom/epson/memcardacc/ConfirmWriteMemcard;

    invoke-static {v0}, Lcom/epson/memcardacc/ConfirmWriteMemcard;->access$300(Lcom/epson/memcardacc/ConfirmWriteMemcard;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_check_and_next"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.class public Lcom/epson/memcardacc/BitmapCache;
.super Ljava/lang/Object;
.source "BitmapCache.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "BitmapCache"

.field private static sBitmapCacheInstance:Lcom/epson/memcardacc/BitmapCache;


# instance fields
.field protected mCacheDir:Ljava/io/File;

.field mCacheHashMap:Lcom/epson/memcardacc/CacheHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/epson/memcardacc/CacheHashMap<",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

.field protected mTempolaryDownloadDir:Ljava/io/File;

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/epson/memcardacc/CacheHashMap;

    invoke-direct {v0}, Lcom/epson/memcardacc/CacheHashMap;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheHashMap:Lcom/epson/memcardacc/CacheHashMap;

    return-void
.end method

.method private checkDirectory(Ljava/io/File;)Z
    .locals 2

    .line 85
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 86
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    .line 90
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_0
    return v1
.end method

.method private deleteDir(Ljava/io/File;)V
    .locals 4

    .line 106
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 109
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 110
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 111
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    return-void
.end method

.method private getBitmapFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheHashMap:Lcom/epson/memcardacc/CacheHashMap;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/CacheHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 270
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    .line 273
    iget-object v1, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheHashMap:Lcom/epson/memcardacc/CacheHashMap;

    invoke-virtual {v1, p1}, Lcom/epson/memcardacc/CacheHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method private static getExifRotation(Ljava/io/File;)I
    .locals 3

    const/4 v0, 0x0

    .line 211
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string p0, "Orientation"

    const/4 v2, 0x1

    .line 212
    invoke-virtual {v1, p0, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_2

    const/4 v1, 0x6

    if-eq p0, v1, :cond_1

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    return v0

    :cond_0
    const/16 p0, 0x10e

    return p0

    :cond_1
    const/16 p0, 0x5a

    return p0

    :cond_2
    const/16 p0, 0xb4

    return p0

    :catch_0
    return v0
.end method

.method public static getInstance()Lcom/epson/memcardacc/BitmapCache;
    .locals 1

    .line 301
    sget-object v0, Lcom/epson/memcardacc/BitmapCache;->sBitmapCacheInstance:Lcom/epson/memcardacc/BitmapCache;

    if-nez v0, :cond_0

    .line 302
    new-instance v0, Lcom/epson/memcardacc/BitmapCache;

    invoke-direct {v0}, Lcom/epson/memcardacc/BitmapCache;-><init>()V

    sput-object v0, Lcom/epson/memcardacc/BitmapCache;->sBitmapCacheInstance:Lcom/epson/memcardacc/BitmapCache;

    .line 304
    :cond_0
    sget-object v0, Lcom/epson/memcardacc/BitmapCache;->sBitmapCacheInstance:Lcom/epson/memcardacc/BitmapCache;

    return-object v0
.end method

.method private initEnv()Z
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/BitmapCache;->checkDirectory(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/memcardacc/BitmapCache;->mTempolaryDownloadDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/BitmapCache;->checkDirectory(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 59
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheDir:Ljava/io/File;

    const-string v2, "mapfile.bin"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private static rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 189
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 190
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-lez v4, :cond_2

    if-gtz v5, :cond_1

    goto :goto_0

    .line 195
    :cond_1
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    int-to-float p1, p1

    .line 196
    invoke-virtual {v6, p1}, Landroid/graphics/Matrix;->setRotate(F)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    move-object v1, p0

    .line 198
    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0

    :cond_2
    :goto_0
    return-object v0
.end method

.method private saveCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6

    if-nez p2, :cond_0

    return-void

    .line 238
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%08x.bin"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 239
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 238
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 240
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheDir:Ljava/io/File;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 243
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 244
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 250
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catchall_0
    move-exception p1

    move-object v0, v2

    goto :goto_2

    :catch_0
    move-exception p2

    move-object v0, v2

    goto :goto_0

    :catchall_1
    move-exception p1

    goto :goto_2

    :catch_1
    move-exception p2

    .line 246
    :goto_0
    :try_start_3
    invoke-virtual {p2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v0, :cond_1

    .line 250
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception p2

    .line 253
    invoke-virtual {p2}, Ljava/io/IOException;->printStackTrace()V

    .line 257
    :cond_1
    :goto_1
    iget-object p2, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheHashMap:Lcom/epson/memcardacc/CacheHashMap;

    invoke-virtual {p2, p1, v1}, Lcom/epson/memcardacc/CacheHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :goto_2
    if-eqz v0, :cond_2

    .line 250
    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_3
    move-exception p2

    .line 253
    invoke-virtual {p2}, Ljava/io/IOException;->printStackTrace()V

    .line 254
    :cond_2
    :goto_3
    throw p1
.end method


# virtual methods
.method public cacheOrMakeThumbnailBitmap(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .line 287
    invoke-direct {p0, p2}, Lcom/epson/memcardacc/BitmapCache;->getBitmapFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 289
    invoke-virtual {p0, p1, p2}, Lcom/epson/memcardacc/BitmapCache;->makeThumbnailBitmap(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 290
    invoke-direct {p0, p2, v0}, Lcom/epson/memcardacc/BitmapCache;->saveCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_0
    return-object v0
.end method

.method public checkSettings()Z
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/BitmapCache;->checkDirectory(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/memcardacc/BitmapCache;->mTempolaryDownloadDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/BitmapCache;->checkDirectory(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public clearCache()V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/BitmapCache;->deleteDir(Ljava/io/File;)V

    .line 122
    iget-object v0, p0, Lcom/epson/memcardacc/BitmapCache;->mTempolaryDownloadDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/epson/memcardacc/BitmapCache;->deleteDir(Ljava/io/File;)V

    return-void
.end method

.method public makeThumbnailBitmap(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v0, 0x0

    .line 137
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/epson/memcardacc/BitmapCache;->mTempolaryDownloadDir:Ljava/io/File;

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 138
    :try_start_1
    iget-object p1, p0, Lcom/epson/memcardacc/BitmapCache;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, p2, v2}, Lcom/epson/memcardacc/CifsAccess;->readFromPrinterMemcard(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_1

    .line 170
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 171
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    return-object v0

    .line 145
    :cond_1
    :try_start_2
    invoke-static {v1}, Lcom/epson/memcardacc/BitmapCache;->getExifRotation(Ljava/io/File;)I

    move-result p1

    .line 147
    iget p2, p0, Lcom/epson/memcardacc/BitmapCache;->mThumbnailWidth:I

    .line 148
    iget v2, p0, Lcom/epson/memcardacc/BitmapCache;->mThumbnailHeight:I

    const/16 v3, 0x5a

    if-eq p1, v3, :cond_2

    const/16 v3, 0x10e

    if-ne p1, v3, :cond_3

    .line 150
    :cond_2
    iget p2, p0, Lcom/epson/memcardacc/BitmapCache;->mThumbnailHeight:I

    .line 151
    iget v2, p0, Lcom/epson/memcardacc/BitmapCache;->mThumbnailWidth:I

    .line 155
    :cond_3
    new-instance v3, Lcom/epson/memcardacc/ImageUtil$WidthHeight;

    invoke-direct {v3, p2, v2}, Lcom/epson/memcardacc/ImageUtil$WidthHeight;-><init>(II)V

    invoke-static {v1, v3}, Lcom/epson/memcardacc/ImageUtil;->makeThumbNail(Ljava/io/File;Lcom/epson/memcardacc/ImageUtil$WidthHeight;)Landroid/graphics/Bitmap;

    move-result-object p2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez p1, :cond_5

    .line 170
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 171
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_4
    return-object p2

    .line 162
    :cond_5
    :try_start_3
    invoke-static {p2, p1}, Lcom/epson/memcardacc/BitmapCache;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 163
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 170
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p2

    if-eqz p2, :cond_6

    .line 171
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_6
    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    nop

    goto :goto_1

    :catchall_1
    move-exception p1

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_7

    .line 170
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p2

    if-eqz p2, :cond_7

    .line 171
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_7
    throw p1

    :catch_1
    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_8

    .line 170
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 171
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_8
    return-object v0
.end method

.method public setCifsAccess(Lcom/epson/memcardacc/CifsAccess;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/epson/memcardacc/BitmapCache;->mCifsAccess:Lcom/epson/memcardacc/CifsAccess;

    return-void
.end method

.method public setDirectory(Ljava/io/File;Ljava/io/File;)Z
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/epson/memcardacc/BitmapCache;->mCacheDir:Ljava/io/File;

    .line 45
    iput-object p2, p0, Lcom/epson/memcardacc/BitmapCache;->mTempolaryDownloadDir:Ljava/io/File;

    .line 46
    invoke-direct {p0}, Lcom/epson/memcardacc/BitmapCache;->initEnv()Z

    move-result p1

    return p1
.end method

.method public setSize(II)V
    .locals 0

    .line 309
    iput p1, p0, Lcom/epson/memcardacc/BitmapCache;->mThumbnailWidth:I

    .line 310
    iput p2, p0, Lcom/epson/memcardacc/BitmapCache;->mThumbnailHeight:I

    return-void
.end method

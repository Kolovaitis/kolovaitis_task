.class public Lcom/epson/memcardacc/MemcardUtil;
.super Ljava/lang/Object;
.source "MemcardUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkFileName(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "."

    .line 101
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_5

    const-string v0, ".."

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const-string v0, "/"

    .line 104
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, "/.."

    .line 107
    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    :cond_2
    const-string v0, "/../"

    .line 110
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result p0

    if-ltz p0, :cond_3

    return v1

    :cond_3
    const/4 p0, 0x1

    return p0

    :cond_4
    :goto_0
    return v1

    :cond_5
    :goto_1
    return v1
.end method

.method public static clearKeepScreenOn(Landroid/view/Window;)V
    .locals 1

    const/16 v0, 0x80

    .line 132
    invoke-virtual {p0, v0}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method public static getFileLength(Ljava/lang/String;)J
    .locals 2

    .line 137
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    .line 85
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p0

    .line 86
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method

.method public static isJpegFile(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 25
    :cond_0
    invoke-static {p0}, Lepson/common/Utils;->getExtention(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_1

    return v0

    .line 30
    :cond_1
    invoke-static {p0}, Lepson/common/Utils;->getExtType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_2

    return v0

    .line 35
    :cond_2
    invoke-static {p0}, Lepson/common/MimeType;->toMimeType(Ljava/lang/String;)Lepson/common/MimeType;

    move-result-object p0

    sget-object v1, Lepson/common/MimeType;->IMAGE_TYPE:Lepson/common/MimeType;

    if-ne p0, v1, :cond_3

    const/4 p0, 0x1

    return p0

    :cond_3
    return v0
.end method

.method public static isJpegOrHeif(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 57
    :cond_0
    invoke-static {p0}, Lepson/common/Utils;->getExtention(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_1

    return v0

    .line 62
    :cond_1
    invoke-static {p0}, Lepson/common/Utils;->getExtType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_2

    return v0

    .line 70
    :cond_2
    invoke-static {p0}, Lepson/common/MimeType;->toMimeType(Ljava/lang/String;)Lepson/common/MimeType;

    move-result-object v1

    sget-object v2, Lepson/common/MimeType;->IMAGE_TYPE:Lepson/common/MimeType;

    if-eq v1, v2, :cond_4

    .line 71
    invoke-static {p0}, Lepson/common/MimeType;->toMimeType(Ljava/lang/String;)Lepson/common/MimeType;

    move-result-object p0

    sget-object v1, Lepson/common/MimeType;->IMAGE_TYPE_HEIF:Lepson/common/MimeType;

    if-ne p0, v1, :cond_3

    goto :goto_0

    :cond_3
    return v0

    :cond_4
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public static keepScreenOn(Landroid/view/Window;)V
    .locals 1

    const/16 v0, 0x80

    .line 123
    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.class Lcom/epson/memcardacc/PasswordDialogFragment$4;
.super Ljava/lang/Object;
.source "PasswordDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/memcardacc/PasswordDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/memcardacc/PasswordDialogFragment;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/PasswordDialogFragment;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/epson/memcardacc/PasswordDialogFragment$4;->this$0:Lcom/epson/memcardacc/PasswordDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 96
    iget-object p1, p0, Lcom/epson/memcardacc/PasswordDialogFragment$4;->this$0:Lcom/epson/memcardacc/PasswordDialogFragment;

    invoke-virtual {p1}, Lcom/epson/memcardacc/PasswordDialogFragment;->getUserName()Ljava/lang/String;

    move-result-object p1

    .line 97
    iget-object p2, p0, Lcom/epson/memcardacc/PasswordDialogFragment$4;->this$0:Lcom/epson/memcardacc/PasswordDialogFragment;

    invoke-virtual {p2}, Lcom/epson/memcardacc/PasswordDialogFragment;->getPassword()Ljava/lang/String;

    move-result-object p2

    .line 99
    new-instance v0, Lcom/epson/memcardacc/SmbAuthInfo;

    invoke-direct {v0, p1, p2}, Lcom/epson/memcardacc/SmbAuthInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/epson/memcardacc/CifsAccess;->setSmbAuthInfo(Lcom/epson/memcardacc/SmbAuthInfo;)V

    .line 101
    iget-object v0, p0, Lcom/epson/memcardacc/PasswordDialogFragment$4;->this$0:Lcom/epson/memcardacc/PasswordDialogFragment;

    invoke-virtual {v0}, Lcom/epson/memcardacc/PasswordDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/epson/memcardacc/PasswordDialogFragment$Callback;

    invoke-interface {v0, p1, p2}, Lcom/epson/memcardacc/PasswordDialogFragment$Callback;->onPositiveClicked(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.class public Lcom/epson/memcardacc/CifsFileInfo;
.super Ljava/lang/Object;
.source "CifsFileInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public mAttribute:I

.field public mFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    .line 16
    iput p2, p0, Lcom/epson/memcardacc/CifsFileInfo;->mAttribute:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 64
    check-cast p1, Lcom/epson/memcardacc/CifsFileInfo;

    .line 65
    iget-object v0, p0, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    iget-object p1, p1, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public isDirectory()Z
    .locals 1

    .line 20
    iget v0, p0, Lcom/epson/memcardacc/CifsFileInfo;->mAttribute:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isReadable()Z
    .locals 1

    .line 53
    iget v0, p0, Lcom/epson/memcardacc/CifsFileInfo;->mAttribute:I

    and-int/lit8 v0, v0, 0x6

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isReadableDir()Z
    .locals 2

    .line 32
    iget v0, p0, Lcom/epson/memcardacc/CifsFileInfo;->mAttribute:I

    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_0

    and-int/lit8 v0, v0, 0x6

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public readableNormalFile()Z
    .locals 2

    .line 46
    iget v0, p0, Lcom/epson/memcardacc/CifsFileInfo;->mAttribute:I

    and-int/lit8 v1, v0, 0x10

    if-nez v1, :cond_0

    and-int/lit8 v0, v0, 0x6

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

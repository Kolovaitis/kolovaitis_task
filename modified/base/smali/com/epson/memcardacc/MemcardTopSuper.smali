.class public abstract Lcom/epson/memcardacc/MemcardTopSuper;
.super Lepson/print/ActivityIACommon;
.source "MemcardTopSuper.java"

# interfaces
.implements Lcom/epson/memcardacc/PasswordDialogFragment$Callback;
.implements Lcom/epson/memcardacc/LocalAlertDialogFragment$DialogCallback;


# static fields
.field protected static final DIALOG_AUTHENTICATION:I = 0x32

.field protected static final DIALOG_COMMUNICATION_ERROR:I = 0x2

.field protected static final DIALOG_MEMCARD_NOT_FOUND:I = 0x1

.field protected static final DIALOG_NO_DIALOG:I = 0x0

.field protected static final DIALOG_PRINTER_BUSY:I = 0x3

.field protected static final DIALOG_PROGRESS:Ljava/lang/String; = "dialog_progress"

.field protected static final FRAGMENT_TAG_ERROR_DIALOG:Ljava/lang/String; = "error_dialog"

.field protected static final FRAGMENT_TAG_PASSWORD_DIALOG:Ljava/lang/String; = "password_dialog"

.field protected static final FRAGMENT_TAG_PROGRESS:Ljava/lang/String; = "progress_dialog"

.field protected static final LAUNCH_TYPE_NOT_SET:I = 0x0

.field protected static final LAUNCH_TYPE_READER:I = 0x2

.field protected static final LAUNCH_TYPE_WRITER:I = 0x1


# instance fields
.field isTryConnectSimpleAp:Z

.field protected mActivityIsFinishing:Z

.field protected mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

.field protected mIsActivityForeground:Z

.field protected mLaunchType:I

.field protected mMemcardCheckTask:Lcom/epson/memcardacc/MemcardCheckTask;

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field private mPasswordDialogFragment:Lcom/epson/memcardacc/PasswordDialogFragment;

.field protected mReservationDialog:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->isTryConnectSimpleAp:Z

    .line 57
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mActivityIsFinishing:Z

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/MemcardTopSuper;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->onPreMemcardChek()V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/memcardacc/MemcardTopSuper;Ljava/lang/Integer;I)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/epson/memcardacc/MemcardTopSuper;->onMemcardCheckEnd(Ljava/lang/Integer;I)V

    return-void
.end method

.method private disconnectWifiDirect()V
    .locals 2

    const-string v0, "printer"

    .line 162
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 161
    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 462
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 464
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method public static synthetic lambda$onCreate$0(Lcom/epson/memcardacc/MemcardTopSuper;Ljava/util/Deque;)V
    .locals 2

    .line 81
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 83
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 84
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 87
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/MemcardTopSuper;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 91
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 92
    invoke-direct {p0, v0}, Lcom/epson/memcardacc/MemcardTopSuper;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private onMemcardCheckEnd(Ljava/lang/Integer;I)V
    .locals 1

    const/4 v0, 0x0

    .line 373
    iput-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mMemcardCheckTask:Lcom/epson/memcardacc/MemcardCheckTask;

    .line 374
    iput-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mPasswordDialogFragment:Lcom/epson/memcardacc/PasswordDialogFragment;

    if-eqz p1, :cond_1

    .line 376
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 412
    :cond_0
    iget-object p2, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v0, "dialog_progress"

    invoke-virtual {p2, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    .line 413
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardTopSuper;->launchActivity(I)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "printer"

    .line 381
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 380
    invoke-static {p0, p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 p1, 0x0

    .line 383
    iput p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mLaunchType:I

    .line 386
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    const/4 p1, 0x1

    if-eq p2, p1, :cond_3

    const/4 p1, 0x3

    if-eq p2, p1, :cond_2

    packed-switch p2, :pswitch_data_0

    const/4 p1, 0x2

    .line 406
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardTopSuper;->localShowDialog(I)V

    return-void

    :pswitch_0
    return-void

    .line 394
    :cond_2
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardTopSuper;->localShowDialog(I)V

    return-void

    .line 391
    :cond_3
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardTopSuper;->localShowDialog(I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onPreMemcardChek()V
    .locals 2

    .line 255
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method private releaseResource()V
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardBitmapCache;->clearCache()V

    :cond_0
    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 8

    const v0, 0x7f0e03b8

    .line 452
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardTopSuper;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    const/4 v1, 0x0

    .line 454
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 455
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showDialogOnResume()Z
    .locals 3

    .line 123
    iget v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mReservationDialog:I

    const/4 v1, 0x0

    .line 124
    iput v1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mReservationDialog:I

    if-eqz v0, :cond_1

    const/16 v2, 0x32

    if-eq v0, v2, :cond_0

    .line 134
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardTopSuper;->localShowDialog(I)V

    return v1

    :cond_0
    const/4 v0, 0x1

    .line 131
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardTopSuper;->showPasswordDialog(Z)V

    return v0

    :cond_1
    return v1
.end method


# virtual methods
.method protected authInfoRequested(Lcom/epson/memcardacc/MemcardCheckTask;Z)V
    .locals 1

    .line 308
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mIsActivityForeground:Z

    if-nez v0, :cond_0

    const/16 p1, 0x32

    .line 309
    iput p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mReservationDialog:I

    return-void

    .line 313
    :cond_0
    new-instance v0, Lcom/epson/memcardacc/MemcardTopSuper$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/epson/memcardacc/MemcardTopSuper$2;-><init>(Lcom/epson/memcardacc/MemcardTopSuper;Lcom/epson/memcardacc/MemcardCheckTask;Z)V

    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardTopSuper;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected chechAndConnectWiFiDirect()Z
    .locals 4

    const-string v0, "printer"

    .line 201
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 203
    iput-boolean v1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 204
    :cond_0
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->isTryConnectSimpleAp:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 206
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->isTryConnectSimpleAp:Z

    const-string v2, "printer"

    const/4 v3, -0x1

    .line 207
    invoke-static {p0, v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    return v0

    :cond_1
    :goto_0
    return v1
.end method

.method protected execMemcardCheck()V
    .locals 3

    .line 217
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mMemcardCheckTask:Lcom/epson/memcardacc/MemcardCheckTask;

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardCheckTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    return-void

    .line 227
    :cond_0
    new-instance v0, Lcom/epson/memcardacc/MemcardCheckTask;

    new-instance v1, Lcom/epson/memcardacc/MemcardTopSuper$1;

    invoke-direct {v1, p0}, Lcom/epson/memcardacc/MemcardTopSuper$1;-><init>(Lcom/epson/memcardacc/MemcardTopSuper;)V

    .line 248
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->getStorageSetType()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/epson/memcardacc/MemcardCheckTask;-><init>(Landroid/content/Context;Lcom/epson/memcardacc/MemcardCheckTask$MemcardCheckCallback;I)V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mMemcardCheckTask:Lcom/epson/memcardacc/MemcardCheckTask;

    .line 249
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mMemcardCheckTask:Lcom/epson/memcardacc/MemcardCheckTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/MemcardCheckTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected abstract getStorageSetType()I
.end method

.method public launchActivity(I)V
    .locals 1

    .line 421
    iget v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mLaunchType:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 435
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardTopSuper;->launchReaderActivity(I)V

    goto :goto_0

    .line 424
    :pswitch_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->launchWriterActivity()V

    const-string p1, "printer"

    .line 429
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 427
    invoke-static {p0, p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :goto_0
    const/4 p1, 0x0

    .line 439
    iput p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mLaunchType:I

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public abstract launchReaderActivity(I)V
.end method

.method public abstract launchWriterActivity()V
.end method

.method localShowDialog(I)V
    .locals 2

    .line 332
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mActivityIsFinishing:Z

    if-eqz v0, :cond_0

    return-void

    .line 335
    :cond_0
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mIsActivityForeground:Z

    if-nez v0, :cond_1

    .line 336
    iput p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mReservationDialog:I

    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const p1, 0x7f0e0217

    const v0, 0x7f0e0218

    .line 355
    invoke-static {p1, v0, v1}, Lcom/epson/memcardacc/LocalAlertDialogFragment;->newInstance(III)Lcom/epson/memcardacc/LocalAlertDialogFragment;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const p1, 0x7f0e03b6

    const v0, 0x7f0e03b7

    .line 349
    invoke-static {p1, v0, v1}, Lcom/epson/memcardacc/LocalAlertDialogFragment;->newInstance(III)Lcom/epson/memcardacc/LocalAlertDialogFragment;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const p1, 0x7f0e03c6

    const v0, 0x7f0e03c7

    const/4 v1, 0x1

    .line 343
    invoke-static {p1, v0, v1}, Lcom/epson/memcardacc/LocalAlertDialogFragment;->newInstance(III)Lcom/epson/memcardacc/LocalAlertDialogFragment;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    .line 362
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    const-string v1, "error_dialog"

    invoke-virtual {v0, p1, v1}, Lcom/epson/memcardacc/LocalAlertDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 72
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgressViewModel;

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 76
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/memcardacc/-$$Lambda$MemcardTopSuper$mhdDVmc_9QCaE4FQ8AINdZxeopk;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/-$$Lambda$MemcardTopSuper$mhdDVmc_9QCaE4FQ8AINdZxeopk;-><init>(Lcom/epson/memcardacc/MemcardTopSuper;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    const/4 p1, 0x0

    .line 97
    iput p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mReservationDialog:I

    return-void
.end method

.method public onDialogCallback(I)V
    .locals 0

    return-void
.end method

.method public onNegativeClicked()V
    .locals 2

    const/4 v0, 0x0

    .line 277
    iput v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mLaunchType:I

    .line 280
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mMemcardCheckTask:Lcom/epson/memcardacc/MemcardCheckTask;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 281
    invoke-virtual {v0, v1}, Lcom/epson/memcardacc/MemcardCheckTask;->cancel(Z)Z

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x0

    .line 146
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mIsActivityForeground:Z

    .line 148
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 149
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mActivityIsFinishing:Z

    .line 152
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->clearSmbAuthInfo()V

    .line 154
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->releaseResource()V

    .line 157
    :cond_0
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    return-void
.end method

.method public onPositiveClicked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mMemcardCheckTask:Lcom/epson/memcardacc/MemcardCheckTask;

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {v0, p1, p2}, Lcom/epson/memcardacc/MemcardCheckTask;->setAuthData(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_0
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string p2, "dialog_progress"

    invoke-virtual {p1, p2}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 103
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x1

    .line 105
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mIsActivityForeground:Z

    .line 107
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->showDialogOnResume()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 113
    :cond_0
    iget v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mLaunchType:I

    if-eqz v0, :cond_1

    .line 114
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->execMemcardCheck()V

    const/4 v0, 0x0

    .line 117
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->isTryConnectSimpleAp:Z

    :cond_1
    return-void
.end method

.method protected onStart()V
    .locals 0

    .line 169
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    .line 175
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    return-void
.end method

.method showPasswordDialog(Z)V
    .locals 2

    .line 291
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mActivityIsFinishing:Z

    if-nez v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mPasswordDialogFragment:Lcom/epson/memcardacc/PasswordDialogFragment;

    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {v0}, Lcom/epson/memcardacc/PasswordDialogFragment;->dismissAllowingStateLoss()V

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    .line 298
    invoke-static {p1}, Lcom/epson/memcardacc/PasswordDialogFragment;->newInstance(Z)Lcom/epson/memcardacc/PasswordDialogFragment;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mPasswordDialogFragment:Lcom/epson/memcardacc/PasswordDialogFragment;

    .line 299
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardTopSuper;->mPasswordDialogFragment:Lcom/epson/memcardacc/PasswordDialogFragment;

    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "password_dialog"

    invoke-virtual {p1, v0, v1}, Lcom/epson/memcardacc/PasswordDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected startMemcardStorageCheck()V
    .locals 1

    .line 184
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->chechAndConnectWiFiDirect()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 189
    :cond_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardTopSuper;->execMemcardCheck()V

    return-void
.end method

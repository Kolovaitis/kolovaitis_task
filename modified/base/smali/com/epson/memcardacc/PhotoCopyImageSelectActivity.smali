.class public Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;
.super Lepson/print/imgsel/ImageSelectActivity;
.source "PhotoCopyImageSelectActivity.java"

# interfaces
.implements Lcom/epson/memcardacc/AltAlertDialogFragment$DialogCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;
    }
.end annotation


# static fields
.field public static final DIALOG_FRAGMENT_TAG_PROGRESS:Ljava/lang/String; = "progress"

.field private static final REQUEST_CODE_EXEC_MEMCARD_WRITE:I = 0x6a

.field private static final SAVED_INSTANCE_STATE_KEY_WAITING_FOR_TRANSFER_END:Ljava/lang/String; = "WaitingForTransferEnd"


# instance fields
.field private mDialogType:Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;

.field private final mFileConvertStateObserver:Landroid/arch/lifecycle/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/Observer<",
            "Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mFileConvertViewModel:Lcom/epson/memcardacc/FileConvertViewModel;

.field private mWaitingForTransferEnd:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;-><init>()V

    .line 46
    new-instance v0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$1;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$1;-><init>(Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;)V

    iput-object v0, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mFileConvertStateObserver:Landroid/arch/lifecycle/Observer;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->onStateChanged(Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;)V

    return-void
.end method

.method private getErrorDialogType(Lcom/epson/memcardacc/FileConvertTask$ErrorType;)I
    .locals 1

    .line 328
    sget-object v0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$2;->$SwitchMap$com$epson$memcardacc$FileConvertTask$ErrorType:[I

    invoke-virtual {p1}, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/16 p1, 0x70

    return p1

    :cond_0
    const/16 p1, 0x71

    return p1
.end method

.method private getProgressDialog()Lcom/epson/memcardacc/LocalProgressDialogFragment;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 283
    invoke-virtual {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "progress"

    .line 284
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/epson/memcardacc/LocalProgressDialogFragment;

    return-object v0
.end method

.method private hideProgress()V
    .locals 1

    .line 300
    invoke-direct {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->getProgressDialog()Lcom/epson/memcardacc/LocalProgressDialogFragment;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 305
    :cond_0
    invoke-virtual {v0}, Lcom/epson/memcardacc/LocalProgressDialogFragment;->dismiss()V

    return-void
.end method

.method private onStateChanged(Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;)V
    .locals 2
    .param p1    # Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->hideProgress()V

    goto :goto_0

    .line 58
    :cond_0
    sget-object v0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$2;->$SwitchMap$com$epson$memcardacc$FileConvertViewModel$ConvertStatus:[I

    invoke-virtual {p1}, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->getConvertStatus()Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 69
    invoke-direct {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->hideProgress()V

    goto :goto_0

    .line 64
    :pswitch_0
    invoke-direct {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->hideProgress()V

    .line 65
    invoke-virtual {p1}, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->getErrorType()Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;->getConvertFileList()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->onConvertEnd(Lcom/epson/memcardacc/FileConvertTask$ErrorType;Ljava/util/ArrayList;)V

    return-void

    .line 60
    :pswitch_1
    invoke-direct {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->showProgress()V

    return-void

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showProgress()V
    .locals 3

    .line 288
    invoke-direct {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->getProgressDialog()Lcom/epson/memcardacc/LocalProgressDialogFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 290
    invoke-virtual {v0}, Lcom/epson/memcardacc/LocalProgressDialogFragment;->dismiss()V

    :cond_0
    const v0, 0x7f0e030b

    .line 293
    invoke-static {v0}, Lcom/epson/memcardacc/LocalProgressDialogFragment;->newInstance(I)Lcom/epson/memcardacc/LocalProgressDialogFragment;

    move-result-object v0

    .line 294
    invoke-virtual {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress"

    invoke-virtual {v0, v1, v2}, Lcom/epson/memcardacc/LocalProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startWriteProgressActivity(Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 184
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/PhotoCopyWriteProgress;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "imageList"

    .line 185
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string p1, "target_folder_name"

    const-string v1, ""

    .line 187
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "cifs_storage_set_type"

    const/4 v1, 0x2

    .line 189
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "check_wifi_direct"

    const/4 v1, 0x1

    .line 191
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 193
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "image.jpg"

    .line 194
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "write_file_list"

    .line 195
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/16 p1, 0x6a

    .line 198
    invoke-virtual {p0, v0, p1}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected clearSelectFile()V
    .locals 1

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->refreshSelector(Ljava/util/ArrayList;)V

    return-void
.end method

.method public getImageFinder()Lepson/print/imgsel/ImageFinder;
    .locals 2

    .line 76
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    .line 77
    new-instance v0, Lepson/print/imgsel/JpegHeifImageFinder;

    invoke-direct {v0}, Lepson/print/imgsel/JpegHeifImageFinder;-><init>()V

    return-object v0

    .line 79
    :cond_0
    new-instance v0, Lepson/print/imgsel/JpegImageFinder;

    invoke-direct {v0}, Lepson/print/imgsel/JpegImageFinder;-><init>()V

    return-object v0
.end method

.method public getMessageType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected goNext()V
    .locals 2

    .line 171
    invoke-virtual {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->getImageSelector()Lepson/print/imgsel/ImageSelector;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    return-void

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mFileConvertViewModel:Lcom/epson/memcardacc/FileConvertViewModel;

    invoke-virtual {v1, v0}, Lcom/epson/memcardacc/FileConvertViewModel;->startConvert(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected localShowDialog(Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;)V
    .locals 2

    .line 149
    invoke-virtual {p1, p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;->getDialogFragment(Landroid/content/Context;)Lcom/epson/memcardacc/AltAlertDialogFragment;

    move-result-object p1

    .line 150
    invoke-virtual {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "dialog"

    invoke-virtual {p1, v0, v1}, Lcom/epson/memcardacc/AltAlertDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 105
    invoke-super {p0, p1, p2, p3}, Lepson/print/imgsel/ImageSelectActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x6a

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    .line 108
    iput-boolean p1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mWaitingForTransferEnd:Z

    .line 110
    invoke-static {}, Lcom/epson/memcardacc/FileConvertTask;->initTempDirectory()Ljava/io/File;

    const/4 v0, -0x2

    if-eq p2, v0, :cond_3

    if-eqz p2, :cond_2

    .line 123
    new-instance p2, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;

    const/16 v0, 0x70

    invoke-direct {p2, v0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;-><init>(I)V

    iput-object p2, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mDialogType:Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;

    if-eqz p3, :cond_1

    .line 125
    iget-object p2, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mDialogType:Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;

    const-string v0, "cifs_errorcode"

    invoke-virtual {p3, v0, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p2, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;->mErrorCode:I

    goto :goto_0

    .line 128
    :cond_1
    iget-object p2, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mDialogType:Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;

    iput p1, p2, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;->mErrorCode:I

    .line 130
    :goto_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->clearSelectFile()V

    return-void

    .line 119
    :cond_2
    invoke-virtual {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->clearSelectFile()V

    return-void

    .line 114
    :cond_3
    invoke-virtual {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->finish()V

    return-void
.end method

.method onConvertEnd(Lcom/epson/memcardacc/FileConvertTask$ErrorType;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Lcom/epson/memcardacc/FileConvertTask$ErrorType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/epson/memcardacc/FileConvertTask$ErrorType;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 309
    sget-object v0, Lcom/epson/memcardacc/FileConvertTask$ErrorType;->NO_ERROR:Lcom/epson/memcardacc/FileConvertTask$ErrorType;

    if-eq p1, v0, :cond_0

    .line 310
    new-instance p2, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;

    invoke-direct {p0, p1}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->getErrorDialogType(Lcom/epson/memcardacc/FileConvertTask$ErrorType;)I

    move-result p1

    invoke-direct {p2, p1}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;-><init>(I)V

    .line 311
    invoke-virtual {p0, p2}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->localShowDialog(Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;)V

    .line 312
    invoke-virtual {p0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->clearSelectFile()V

    return-void

    :cond_0
    if-nez p2, :cond_1

    return-void

    :cond_1
    const/4 p1, 0x1

    .line 323
    iput-boolean p1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mWaitingForTransferEnd:Z

    .line 324
    invoke-direct {p0, p2}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->startWriteProgressActivity(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 90
    invoke-super {p0, p1}, Lepson/print/imgsel/ImageSelectActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    .line 92
    iput-boolean v0, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mWaitingForTransferEnd:Z

    if-eqz p1, :cond_0

    const-string v0, "WaitingForTransferEnd"

    .line 95
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mWaitingForTransferEnd:Z

    .line 98
    :cond_0
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lcom/epson/memcardacc/FileConvertViewModel;

    .line 99
    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lcom/epson/memcardacc/FileConvertViewModel;

    iput-object p1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mFileConvertViewModel:Lcom/epson/memcardacc/FileConvertViewModel;

    .line 100
    iget-object p1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mFileConvertViewModel:Lcom/epson/memcardacc/FileConvertViewModel;

    invoke-virtual {p1}, Lcom/epson/memcardacc/FileConvertViewModel;->getData()Landroid/arch/lifecycle/LiveData;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mFileConvertStateObserver:Landroid/arch/lifecycle/Observer;

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    return-void
.end method

.method public onPositiveCallback()V
    .locals 0

    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .line 140
    invoke-super {p0}, Lepson/print/imgsel/ImageSelectActivity;->onPostResume()V

    .line 141
    iget-object v0, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mDialogType:Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->localShowDialog(Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;)V

    const/4 v0, 0x0

    .line 143
    iput-object v0, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mDialogType:Lcom/epson/memcardacc/PhotoCopyImageSelectActivity$DialogType;

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 155
    invoke-super {p0, p1}, Lepson/print/imgsel/ImageSelectActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "WaitingForTransferEnd"

    .line 156
    iget-boolean v1, p0, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;->mWaitingForTransferEnd:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public singleImageMode()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.class public Lcom/epson/memcardacc/CifsAccess;
.super Ljava/lang/Object;
.source "CifsAccess.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;
    }
.end annotation


# static fields
.field public static final CHECK_STORAGE_ERROR:I = 0x0

.field public static final CIFS_ERROR_LOGIN_FAILURE:I = -0x4b5

.field public static final CIFS_ERROR_PATH_EXISTED:I = -0x4b3

.field public static final KEY_SMB_PASSWORD:Ljava/lang/String; = "password"

.field public static final KEY_SMB_USER_NAME:Ljava/lang/String; = "smb_user_name"

.field public static final SMB_AUTHINFO_SHARED_PREFS_KEY:Ljava/lang/String; = "smb_authinfo"

.field public static final STORAGE_SET_TYPE_GENERIC_MEMCARD_COPY:I = 0x1

.field public static final STORAGE_SET_TYPE_PHOTO_COPY:I = 0x2

.field private static sCifsLib:Lcom/epson/memcardacc/CifsLib;

.field private static sCifsLibFactory:Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;

.field protected static final sCifsLibSemaphore:Ljava/util/concurrent/Semaphore;


# instance fields
.field private mGotCifsLibSemaphore:Z

.field private mStorageSet:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 47
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLibSemaphore:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .line 652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 653
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLibFactory:Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;

    if-nez v0, :cond_0

    .line 654
    new-instance v0, Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;

    invoke-direct {v0}, Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;-><init>()V

    sput-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLibFactory:Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;

    :cond_0
    return-void
.end method

.method public static clearSmbAuthInfo()V
    .locals 1

    .line 682
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getCifsAccessSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 683
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method protected static getCifsAccessSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 672
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v0

    const-string v1, "smb_authinfo"

    const/4 v2, 0x0

    .line 673
    invoke-virtual {v0, v1, v2}, Lepson/print/IprintApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getCifsLibSemaphore()Ljava/util/concurrent/Semaphore;
    .locals 1

    .line 719
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLibSemaphore:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method public static getFileInfoFromBin([B)Ljava/util/LinkedList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/CifsFileInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 608
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/lit8 v2, v1, 0x4

    add-int/lit8 v3, v2, 0x2

    .line 614
    array-length v4, p0

    if-gt v3, v4, :cond_1

    const/4 v4, 0x4

    .line 615
    invoke-static {p0, v1, v4}, Lcom/epson/memcardacc/CifsAccess;->readLeNByteInt([BII)I

    move-result v4

    const/4 v5, 0x2

    .line 616
    invoke-static {p0, v2, v5}, Lcom/epson/memcardacc/CifsAccess;->readLeNByteInt([BII)I

    move-result v2

    .line 617
    new-instance v5, Ljava/lang/String;

    const-string v6, "UTF-16LE"

    invoke-direct {v5, p0, v3, v2, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 618
    new-instance v3, Lcom/epson/memcardacc/CifsFileInfo;

    invoke-direct {v3, v5, v4}, Lcom/epson/memcardacc/CifsFileInfo;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x6

    add-int/2addr v1, v2

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static getInstance()Lcom/epson/memcardacc/CifsAccess;
    .locals 1

    .line 663
    new-instance v0, Lcom/epson/memcardacc/CifsAccess;

    invoke-direct {v0}, Lcom/epson/memcardacc/CifsAccess;-><init>()V

    return-object v0
.end method

.method public static getParentDirectory(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_1

    .line 632
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "\\\\[^\\\\]+\\\\?$"

    const-string v1, ""

    .line 635
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    const-string p0, ""

    return-object p0
.end method

.method public static getSmbAuthInfo()Lcom/epson/memcardacc/SmbAuthInfo;
    .locals 5

    .line 692
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getCifsAccessSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 693
    new-instance v1, Lcom/epson/memcardacc/SmbAuthInfo;

    const-string v2, "smb_user_name"

    const-string v3, ""

    .line 694
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "password"

    const-string v4, ""

    .line 695
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/epson/memcardacc/SmbAuthInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static getStringsFromUtf16LeByte([B)Ljava/util/LinkedList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 537
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 541
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    .line 542
    invoke-static {p0, v1}, Lcom/epson/memcardacc/CifsAccess;->getUtf16ZZTermDataByteLength([BI)I

    move-result v2

    .line 545
    :try_start_0
    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-16LE"

    invoke-direct {v3, p0, v1, v2, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    const-string v4, "??can_not_decode??"

    .line 548
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v3, v4

    .line 550
    :goto_1
    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static getUtf16ZZTermDataByteLength([BI)I
    .locals 3

    .line 563
    array-length v0, p0

    if-ge v0, p1, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    move v0, p1

    .line 567
    :goto_0
    array-length v1, p0

    add-int/lit8 v2, v0, 0x2

    if-lt v1, v2, :cond_2

    .line 568
    aget-byte v1, p0, v0

    if-nez v1, :cond_1

    add-int/lit8 v1, v0, 0x1

    aget-byte v1, p0, v1

    if-nez v1, :cond_1

    sub-int/2addr v0, p1

    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    sub-int/2addr v0, p1

    return v0
.end method

.method public static getZZTermUtf16Bin(Ljava/lang/String;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\u0000"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "UTF-16LE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method

.method public static join(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "\\"

    .line 643
    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 647
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\\"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static readLeNByteInt([BII)I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    add-int v2, p1, v0

    .line 595
    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    mul-int/lit8 v3, v0, 0x8

    shl-int/2addr v2, v3

    or-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method protected static setCifsFactory(Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;)V
    .locals 0

    return-void
.end method

.method public static setSmbAuthInfo(Lcom/epson/memcardacc/SmbAuthInfo;)V
    .locals 3

    .line 707
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getCifsAccessSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 708
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "smb_user_name"

    .line 709
    iget-object v2, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mUserName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "password"

    .line 710
    iget-object p0, p0, Lcom/epson/memcardacc/SmbAuthInfo;->mPassword:Ljava/lang/String;

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 711
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method protected declared-synchronized acquireCifsLibSemaphore()Z
    .locals 5

    monitor-enter p0

    .line 154
    :try_start_0
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getCifsLibSemaphore()Ljava/util/concurrent/Semaphore;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, 0x3c

    const/4 v3, 0x0

    .line 157
    :try_start_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v4}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CifsAccess"

    const-string v1, "cifsSemaphore.tryAcquire() can not get semaphore."

    .line 160
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    monitor-exit p0

    return v3

    :cond_0
    const/4 v0, 0x1

    .line 168
    :try_start_2
    iput-boolean v0, p0, Lcom/epson/memcardacc/CifsAccess;->mGotCifsLibSemaphore:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 170
    monitor-exit p0

    return v0

    :catch_0
    :try_start_3
    const-string v0, "CifsAccess"

    const-string v1, "cifsSemaphore.tryAcquire() interrupted."

    .line 165
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 166
    monitor-exit p0

    return v3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized cancel()V
    .locals 1

    monitor-enter p0

    .line 486
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->checkCifsLibStatus()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 487
    monitor-exit p0

    return-void

    .line 489
    :cond_0
    :try_start_1
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->cancel()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized cancelByFileSizeZero()V
    .locals 1

    monitor-enter p0

    .line 498
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->checkCifsLibStatus()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 499
    monitor-exit p0

    return-void

    .line 501
    :cond_0
    :try_start_1
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->cancelByFileSizeZero()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 502
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected checkCifsLibStatus()Z
    .locals 1

    .line 511
    iget-boolean v0, p0, Lcom/epson/memcardacc/CifsAccess;->mGotCifsLibSemaphore:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public checkStorage()I
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 223
    :goto_0
    iget-object v2, p0, Lcom/epson/memcardacc/CifsAccess;->mStorageSet:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_2

    .line 224
    aget-object v2, v2, v1

    .line 225
    invoke-virtual {p0, v2}, Lcom/epson/memcardacc/CifsAccess;->connectStorageWithApplicationUsernameAndPassword(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->getFreeUnit()J

    move-result-wide v2

    .line 227
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-ltz v6, :cond_0

    add-int/lit8 v1, v1, 0x1

    return v1

    .line 232
    :cond_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->getErrorCode()I

    move-result v2

    const/16 v3, -0x4b5

    if-ne v2, v3, :cond_1

    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method

.method public connectDefaultStorage(ILjava/lang/String;Ljava/lang/String;)I
    .locals 1

    if-gtz p1, :cond_0

    const/4 p1, -0x1

    return p1

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/CifsAccess;->mStorageSet:[Ljava/lang/String;

    add-int/lit8 p1, p1, -0x1

    aget-object p1, v0, p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/epson/memcardacc/CifsAccess;->connectStorage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public connectDefaultStorageWidthDefaultAthInfo(I)I
    .locals 2

    .line 308
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getSmbAuthInfo()Lcom/epson/memcardacc/SmbAuthInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    const-string v1, ""

    .line 310
    invoke-virtual {p0, p1, v0, v1}, Lcom/epson/memcardacc/CifsAccess;->connectDefaultStorage(ILjava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1

    .line 312
    :cond_0
    iget-object v1, v0, Lcom/epson/memcardacc/SmbAuthInfo;->mUserName:Ljava/lang/String;

    iget-object v0, v0, Lcom/epson/memcardacc/SmbAuthInfo;->mPassword:Ljava/lang/String;

    invoke-virtual {p0, p1, v1, v0}, Lcom/epson/memcardacc/CifsAccess;->connectDefaultStorage(ILjava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public connectStorage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .line 250
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->checkCifsLibStatus()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 255
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getZZTermUtf16Bin(Ljava/lang/String;)[B

    move-result-object p1

    .line 256
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0, p1, p2, p3}, Lcom/epson/memcardacc/CifsLib;->connectStrageNative([BLjava/lang/String;Ljava/lang/String;)I

    move-result p1

    const-string p2, "CifsAccess"

    .line 257
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "@@@return <"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ">"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 260
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    return v1
.end method

.method public connectStorageWithApplicationUsernameAndPassword(Ljava/lang/String;)I
    .locals 3

    const-string v0, ""

    const-string v1, ""

    .line 277
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getSmbAuthInfo()Lcom/epson/memcardacc/SmbAuthInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 279
    iget-object v0, v2, Lcom/epson/memcardacc/SmbAuthInfo;->mUserName:Ljava/lang/String;

    .line 280
    iget-object v1, v2, Lcom/epson/memcardacc/SmbAuthInfo;->mPassword:Ljava/lang/String;

    .line 283
    :cond_0
    invoke-virtual {p0, p1, v0, v1}, Lcom/epson/memcardacc/CifsAccess;->connectStorage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public createDirectory(Ljava/lang/String;)I
    .locals 1

    .line 430
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->checkCifsLibStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 436
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getZZTermUtf16Bin(Ljava/lang/String;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/CifsLib;->createDirectory([B)I

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 438
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const/16 p1, -0x64

    return p1
.end method

.method public deleteDirectory(Ljava/lang/String;)I
    .locals 1

    .line 448
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->checkCifsLibStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 454
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getZZTermUtf16Bin(Ljava/lang/String;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/CifsLib;->deleteDirectory([B)I

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 456
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const/16 p1, -0x64

    return p1
.end method

.method public deleteFile(Ljava/lang/String;)I
    .locals 1

    .line 466
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->checkCifsLibStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 472
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getZZTermUtf16Bin(Ljava/lang/String;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 477
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/CifsLib;->deleteFile([B)I

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 474
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const/16 p1, -0x64

    return p1
.end method

.method public disconnectStorage()V
    .locals 1

    .line 319
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    if-nez v0, :cond_0

    return-void

    .line 322
    :cond_0
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->cancel()V

    .line 323
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->disConnectStrage()I

    return-void
.end method

.method public declared-synchronized free()V
    .locals 1

    monitor-enter p0

    .line 142
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->freeCifsLib()V

    .line 143
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->releaseCifsLibSemaphore()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected freeCifsLib()V
    .locals 1

    .line 130
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->free()V

    const/4 v0, 0x0

    .line 132
    sput-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    :cond_0
    return-void
.end method

.method public getErrorCode()I
    .locals 1

    .line 521
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 524
    :cond_0
    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->getErrorCode()I

    move-result v0

    return v0
.end method

.method public getFileList(Ljava/lang/String;)Ljava/util/LinkedList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/LinkedList<",
            "Lcom/epson/memcardacc/CifsFileInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "\\"

    .line 333
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v0, 0x0

    .line 340
    :try_start_0
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getZZTermUtf16Bin(Ljava/lang/String;)[B

    move-result-object p1

    .line 341
    sget-object v1, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v1, p1}, Lcom/epson/memcardacc/CifsLib;->getFileList([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez p1, :cond_0

    return-object v0

    .line 352
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getFileInfoFromBin([B)Ljava/util/LinkedList;

    move-result-object v0
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 354
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :goto_0
    return-object v0

    :catch_1
    move-exception p1

    .line 343
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    return-object v0
.end method

.method public getFileSize(Ljava/lang/String;)J
    .locals 2

    .line 366
    :try_start_0
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getZZTermUtf16Bin(Ljava/lang/String;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/CifsLib;->getFileSize([B)J

    move-result-wide v0

    return-wide v0

    :catch_0
    move-exception p1

    .line 368
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const-wide/16 v0, -0x64

    return-wide v0
.end method

.method public getFreeSize()J
    .locals 4

    .line 386
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->getUnitSize()I

    move-result v0

    int-to-long v0, v0

    .line 387
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->getFreeUnit()J

    move-result-wide v2

    mul-long v0, v0, v2

    return-wide v0
.end method

.method public getFreeUnit()J
    .locals 2

    .line 376
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->getFreeUnit()J

    move-result-wide v0

    return-wide v0
.end method

.method public getStorageNames()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 207
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->getStrageNames()[B

    move-result-object v0

    .line 208
    invoke-static {v0}, Lcom/epson/memcardacc/CifsAccess;->getStringsFromUtf16LeByte([B)Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method public getUnitSize()I
    .locals 1

    .line 381
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsLib;->getUnitSize()I

    move-result v0

    return v0
.end method

.method protected declared-synchronized init(Ljava/lang/String;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    .line 95
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_0

    goto :goto_0

    .line 99
    :cond_0
    iget-boolean v1, p0, Lcom/epson/memcardacc/CifsAccess;->mGotCifsLibSemaphore:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 101
    monitor-exit p0

    return v0

    .line 104
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->acquireCifsLibSemaphore()Z

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    .line 105
    monitor-exit p0

    return v0

    .line 108
    :cond_2
    :try_start_2
    sget-object v1, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    if-eqz v1, :cond_3

    const-string v1, "CifsAccess"

    const-string v2, "in init() sCifsLib != null"

    .line 110
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->freeCifsLib()V

    .line 116
    :cond_3
    sget-object v1, Lcom/epson/memcardacc/CifsAccess;->sCifsLibFactory:Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;

    invoke-virtual {v1}, Lcom/epson/memcardacc/CifsAccess$CifsLibFactory;->getCifsLib()Lcom/epson/memcardacc/CifsLib;

    move-result-object v1

    sput-object v1, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    const-string v1, "US-ASCII"

    .line 117
    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    .line 118
    sget-object v1, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v1, p1, p2}, Lcom/epson/memcardacc/CifsLib;->init([BI)I

    move-result p1

    if-nez p1, :cond_4

    .line 121
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->free()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    monitor-exit p0

    return v0

    .line 125
    :cond_4
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    .line 96
    :cond_5
    :goto_0
    monitor-exit p0

    return v0
.end method

.method public initDefault(Landroid/content/Context;I)I
    .locals 1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    .line 70
    sget-object p2, Lcom/epson/memcardacc/MemcardConfig;->GENERIC_STORAGE_SET:[Ljava/lang/String;

    iput-object p2, p0, Lcom/epson/memcardacc/CifsAccess;->mStorageSet:[Ljava/lang/String;

    goto :goto_0

    .line 67
    :cond_0
    sget-object p2, Lcom/epson/memcardacc/MemcardConfig;->PHOTO_COPY_STORAGE_SET:[Ljava/lang/String;

    iput-object p2, p0, Lcom/epson/memcardacc/CifsAccess;->mStorageSet:[Ljava/lang/String;

    .line 73
    :goto_0
    invoke-static {p1}, Lcom/epson/memcardacc/MemcardUtil;->getPrinterIpAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 75
    :try_start_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object p2

    .line 76
    invoke-virtual {p2}, Ljava/util/TimeZone;->getRawOffset()I

    move-result p2

    const v0, 0x36ee80

    div-int/2addr p2, v0

    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/epson/memcardacc/CifsAccess;->init(Ljava/lang/String;I)I

    move-result p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 79
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public readFromPrinterMemcard(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 397
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->checkCifsLibStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 403
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/epson/memcardacc/CifsAccess;->getZZTermUtf16Bin(Ljava/lang/String;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0, p1, p2}, Lcom/epson/memcardacc/CifsLib;->readFromPrinterMemcard([BLjava/lang/String;)I

    move-result p1

    return p1

    :catch_0
    move-exception p1

    .line 405
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const/16 p1, -0x64

    return p1
.end method

.method protected declared-synchronized releaseCifsLibSemaphore()V
    .locals 1

    monitor-enter p0

    .line 180
    :try_start_0
    iget-boolean v0, p0, Lcom/epson/memcardacc/CifsAccess;->mGotCifsLibSemaphore:Z

    if-eqz v0, :cond_0

    .line 181
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getCifsLibSemaphore()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    const/4 v0, 0x0

    .line 189
    iput-boolean v0, p0, Lcom/epson/memcardacc/CifsAccess;->mGotCifsLibSemaphore:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public writeToPrinterMemcard(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 418
    invoke-virtual {p0}, Lcom/epson/memcardacc/CifsAccess;->checkCifsLibStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 422
    :cond_0
    invoke-static {p2}, Lcom/epson/memcardacc/CifsAccess;->getZZTermUtf16Bin(Ljava/lang/String;)[B

    move-result-object p2

    .line 423
    sget-object v0, Lcom/epson/memcardacc/CifsAccess;->sCifsLib:Lcom/epson/memcardacc/CifsLib;

    invoke-virtual {v0, p1, p2, p3}, Lcom/epson/memcardacc/CifsLib;->writeToPrinterMemcard(Ljava/lang/String;[BI)I

    move-result p1

    return p1
.end method

.class public Lcom/epson/memcardacc/FileConvertViewModel;
.super Landroid/arch/lifecycle/AndroidViewModel;
.source "FileConvertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;,
        Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;
    }
.end annotation


# instance fields
.field private final mConvertEndListener:Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;

.field private final mFileConvertStatusLiveData:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData<",
            "Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mFileConvertTask:Lcom/epson/memcardacc/FileConvertTask;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 72
    invoke-direct {p0, p1}, Landroid/arch/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 61
    new-instance p1, Lcom/epson/memcardacc/FileConvertViewModel$1;

    invoke-direct {p1, p0}, Lcom/epson/memcardacc/FileConvertViewModel$1;-><init>(Lcom/epson/memcardacc/FileConvertViewModel;)V

    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mConvertEndListener:Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;

    .line 73
    new-instance p1, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {p1}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object p1, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mFileConvertStatusLiveData:Landroid/arch/lifecycle/MutableLiveData;

    .line 74
    iget-object p1, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mFileConvertStatusLiveData:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v0, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;

    invoke-direct {v0}, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;-><init>()V

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/FileConvertViewModel;)Landroid/arch/lifecycle/MutableLiveData;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mFileConvertStatusLiveData:Landroid/arch/lifecycle/MutableLiveData;

    return-object p0
.end method


# virtual methods
.method public getData()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData<",
            "Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;",
            ">;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mFileConvertStatusLiveData:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method public startConvert(Ljava/util/ArrayList;)V
    .locals 3
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mFileConvertStatusLiveData:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v1, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;

    sget-object v2, Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;->TASK_RUNNING:Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;

    invoke-direct {v1, v2}, Lcom/epson/memcardacc/FileConvertViewModel$FileConvertStatus;-><init>(Lcom/epson/memcardacc/FileConvertViewModel$ConvertStatus;)V

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 83
    new-instance v0, Lcom/epson/memcardacc/FileConvertTask;

    iget-object v1, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mConvertEndListener:Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;

    invoke-direct {v0, p1, v1}, Lcom/epson/memcardacc/FileConvertTask;-><init>(Ljava/util/ArrayList;Lcom/epson/memcardacc/FileConvertTask$ConvertEndListener;)V

    iput-object v0, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mFileConvertTask:Lcom/epson/memcardacc/FileConvertTask;

    .line 84
    iget-object p1, p0, Lcom/epson/memcardacc/FileConvertViewModel;->mFileConvertTask:Lcom/epson/memcardacc/FileConvertTask;

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p1, v0, v1}, Lcom/epson/memcardacc/FileConvertTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

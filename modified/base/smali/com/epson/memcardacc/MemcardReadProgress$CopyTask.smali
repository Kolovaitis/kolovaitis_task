.class Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;
.super Landroid/os/AsyncTask;
.source "MemcardReadProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/MemcardReadProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CopyTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field cifsAccess:Lcom/epson/memcardacc/CifsAccess;

.field private mCanceling:Z

.field private mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

.field final synthetic this$0:Lcom/epson/memcardacc/MemcardReadProgress;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/MemcardReadProgress;)V
    .locals 2

    .line 168
    iput-object p1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 164
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->getInstance()Lcom/epson/memcardacc/CifsAccess;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    .line 169
    new-instance v0, Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/CommonLog;-><init>()V

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    .line 170
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    const/4 v1, 0x0

    iput v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    const/16 v1, 0x2002

    .line 171
    iput v1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 172
    invoke-virtual {p1}, Lcom/epson/memcardacc/MemcardReadProgress;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/iprint/prtlogger/Analytics;->getDefaultPrinterName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/epson/iprint/prtlogger/CommonLog;->printerName:Ljava/lang/String;

    return-void
.end method

.method private copyFile()I
    .locals 10

    .line 236
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardReadProgress;->access$000(Lcom/epson/memcardacc/MemcardReadProgress;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 238
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, -0x2

    if-eqz v3, :cond_5

    .line 239
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    return v1

    :cond_0
    const/4 v3, 0x1

    .line 242
    new-array v5, v3, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {p0, v5}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->publishProgress([Ljava/lang/Object;)V

    .line 244
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 245
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%s/IMG_%04d.JPG"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    .line 246
    invoke-static {v9}, Lcom/epson/memcardacc/MemcardReadProgress;->access$200(Lcom/epson/memcardacc/MemcardReadProgress;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    .line 245
    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    add-int/2addr v2, v3

    .line 248
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v3, "MemcardReadProgress"

    const-string v4, "local file exists. skip"

    .line 251
    invoke-static {v3, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v7, "MemcardReadProgress"

    .line 254
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "copy "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " => "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v7, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v7, v5, v6}, Lcom/epson/memcardacc/CifsAccess;->readFromPrinterMemcard(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_3

    const-string v0, "MemcardReadProgress"

    .line 257
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cifsAccess.writeFromPrinterMemcard() return <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    if-ne v5, v4, :cond_2

    return v1

    :cond_2
    const/4 v0, 0x3

    return v0

    .line 271
    :cond_3
    sget-object v4, Lcom/epson/memcardacc/MemcardConfig;->BASE_DIRECTORY:Ljava/lang/String;

    sget-object v5, Lepson/print/CommonDefine;->DEFAULT_DIR:Ljava/lang/String;

    invoke-virtual {v6, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 273
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "MemcardReadProgress"

    .line 274
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaScannerConnection.scanFile path = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v5, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    new-array v3, v3, [Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v4, 0x0

    invoke-static {v5, v3, v4, v4}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    goto :goto_1

    :cond_4
    const-string v3, "MemcardReadProgress"

    .line 278
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed MediaScannerConnection.scanFile path = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_1
    iget-object v3, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    iput v2, v3, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    goto/16 :goto_0

    :cond_5
    return v4
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2

    .line 210
    :try_start_0
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/epson/memcardacc/CifsAccess;->initDefault(Landroid/content/Context;I)I

    move-result p1

    const/4 v0, 0x3

    if-nez p1, :cond_0

    .line 212
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 214
    :cond_0
    :try_start_1
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-static {v1}, Lcom/epson/memcardacc/MemcardReadProgress;->access$100(Lcom/epson/memcardacc/MemcardReadProgress;)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/epson/memcardacc/CifsAccess;->connectDefaultStorageWidthDefaultAthInfo(I)I

    move-result p1

    if-nez p1, :cond_1

    .line 215
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    .line 218
    :cond_1
    :try_start_2
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->copyFile()I

    move-result p1

    .line 219
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->disconnectStorage()V

    .line 221
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-virtual {v1}, Lcom/epson/memcardacc/MemcardReadProgress;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/iprint/prtlogger/CommonLog;->setConnectionType(Landroid/content/Context;)V

    .line 222
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 224
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    return-object p1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->free()V

    throw p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 163
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled()V
    .locals 3

    .line 177
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardReadProgress;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardUtil;->clearKeepScreenOn(Landroid/view/Window;)V

    const-string v0, "MemcardReadProgress"

    const-string v1, "canceld in AsyncTask"

    .line 178
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/epson/memcardacc/MemcardReadProgress;->finishWithState(ILcom/epson/iprint/prtlogger/CommonLog;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardReadProgress;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardUtil;->clearKeepScreenOn(Landroid/view/Window;)V

    .line 186
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCommonLog:Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-virtual {v0, p1, v1}, Lcom/epson/memcardacc/MemcardReadProgress;->finishWithState(ILcom/epson/iprint/prtlogger/CommonLog;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 163
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    const/4 v0, 0x0

    .line 192
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCanceling:Z

    .line 193
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-virtual {v1}, Lcom/epson/memcardacc/MemcardReadProgress;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/memcardacc/MemcardUtil;->keepScreenOn(Landroid/view/Window;)V

    .line 194
    iget-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-static {v1}, Lcom/epson/memcardacc/MemcardReadProgress;->access$000(Lcom/epson/memcardacc/MemcardReadProgress;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/epson/memcardacc/MemcardReadProgress;->setProgress(II)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->this$0:Lcom/epson/memcardacc/MemcardReadProgress;

    invoke-static {v1}, Lcom/epson/memcardacc/MemcardReadProgress;->access$000(Lcom/epson/memcardacc/MemcardReadProgress;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/epson/memcardacc/MemcardReadProgress;->setProgress(II)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 163
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public taskCancel()V
    .locals 1

    .line 289
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCanceling:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 292
    iput-boolean v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->mCanceling:Z

    .line 293
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cifsAccess:Lcom/epson/memcardacc/CifsAccess;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsAccess;->cancel()V

    const/4 v0, 0x0

    .line 294
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardReadProgress$CopyTask;->cancel(Z)Z

    return-void
.end method

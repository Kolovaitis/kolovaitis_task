.class public Lcom/epson/memcardacc/MemcardConfig;
.super Ljava/lang/Object;
.source "MemcardConfig.java"


# static fields
.field public static final BASE_DIRECTORY:Ljava/lang/String;

.field public static final CIFS_WRITE_FOLDER:Ljava/lang/String; = "\\EPIMG"

.field public static final DEFAULT_READER_LOCAL_DIRECTORY:Ljava/lang/String; = "eproll"

.field public static final DOWNLOAD_DIRECTORY:Ljava/lang/String; = "bindata02"

.field public static final GENERIC_STORAGE_SET:[Ljava/lang/String;

.field public static final INIT_CIFS_DIRECTORY:Ljava/lang/String; = "\\DCIM"

.field public static final MAP_FILE_NAME:Ljava/lang/String; = "mapfile.bin"

.field public static final MEMCARD_STORAGE_NAME:Ljava/lang/String; = "MEMORYCARD"

.field public static final PHOTO_COPY_STORAGE_SET:[Ljava/lang/String;

.field public static final PHOTO_COPY_WRITE_FILE_NAME:Ljava/lang/String; = "image.jpg"

.field public static final PHOTO_COPY_WRITE_FOLDER_NAME:Ljava/lang/String; = ""

.field public static final THUMBNAIL_CACHE_DIRECTORY:Ljava/lang/String; = "bindata01"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 7
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/memcardacc/MemcardConfig;->BASE_DIRECTORY:Ljava/lang/String;

    const-string v0, "MEMORYCARD"

    const-string v1, "USBSTORAGE"

    .line 43
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/memcardacc/MemcardConfig;->GENERIC_STORAGE_SET:[Ljava/lang/String;

    const-string v0, "FILESTORE"

    .line 49
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/memcardacc/MemcardConfig;->PHOTO_COPY_STORAGE_SET:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/epson/memcardacc/MemCardWriteConvertViewModel;
.super Landroid/arch/lifecycle/AndroidViewModel;
.source "MemCardWriteConvertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;,
        Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertResult;
    }
.end annotation


# instance fields
.field private final mApplication:Landroid/app/Application;

.field private final mConvertLiveData:Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0
    .param p1    # Landroid/app/Application;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 37
    invoke-direct {p0, p1}, Landroid/arch/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 38
    iput-object p1, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->mApplication:Landroid/app/Application;

    .line 39
    new-instance p1, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;

    invoke-direct {p1, p0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;-><init>(Lcom/epson/memcardacc/MemCardWriteConvertViewModel;)V

    iput-object p1, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->mConvertLiveData:Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;

    return-void
.end method

.method static synthetic access$200(Lcom/epson/memcardacc/MemCardWriteConvertViewModel;)Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->mConvertLiveData:Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;

    return-object p0
.end method

.method static synthetic access$400(Lcom/epson/memcardacc/MemCardWriteConvertViewModel;)Landroid/app/Application;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->mApplication:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method public deleteTempFile()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->mConvertLiveData:Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;

    invoke-static {v0}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->access$000(Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;)V

    return-void
.end method

.method public getData()Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->mConvertLiveData:Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;

    return-object v0
.end method

.method public setOrigFileList(Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/epson/memcardacc/MemCardWriteConvertViewModel;->mConvertLiveData:Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;

    invoke-virtual {v0, p1}, Lcom/epson/memcardacc/MemCardWriteConvertViewModel$ConvertLiveData;->setOrigFileList(Ljava/util/ArrayList;)V

    return-void
.end method

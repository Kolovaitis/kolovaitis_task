.class public Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;
.super Ljava/lang/Object;
.source "ImageFileListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/ImageFileListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FileInfoWithCheck"
.end annotation


# instance fields
.field public mChecked:I

.field private mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

.field mParentDirectory:Ljava/lang/String;

.field public mThumbNail:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/epson/memcardacc/ImageFileListAdapter;


# direct methods
.method public constructor <init>(Lcom/epson/memcardacc/ImageFileListAdapter;Lcom/epson/memcardacc/CifsFileInfo;)V
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->this$0:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    iput-object p2, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    return-void
.end method

.method public constructor <init>(Lcom/epson/memcardacc/ImageFileListAdapter;Ljava/lang/String;)V
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->this$0:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355
    iput-object p2, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mParentDirectory:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;)Lcom/epson/memcardacc/CifsFileInfo;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    return-object p0
.end method


# virtual methods
.method public getDirectoryName()Ljava/lang/String;
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mParentDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsFileInfo;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->this$0:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-static {v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->access$100(Lcom/epson/memcardacc/ImageFileListAdapter;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->this$0:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-static {v1}, Lcom/epson/memcardacc/ImageFileListAdapter;->access$100(Lcom/epson/memcardacc/ImageFileListAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    iget-object v1, v1, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 371
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->this$0:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-static {v1}, Lcom/epson/memcardacc/ImageFileListAdapter;->access$100(Lcom/epson/memcardacc/ImageFileListAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    iget-object v1, v1, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 2

    .line 314
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mParentDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, v0, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    return-object v0

    .line 318
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "item don\'t have CifsFileInfo"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFullPath()Ljava/lang/String;
    .locals 2

    .line 324
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mParentDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->this$0:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-static {v0}, Lcom/epson/memcardacc/ImageFileListAdapter;->access$100(Lcom/epson/memcardacc/ImageFileListAdapter;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->this$0:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-static {v1}, Lcom/epson/memcardacc/ImageFileListAdapter;->access$100(Lcom/epson/memcardacc/ImageFileListAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    iget-object v1, v1, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 330
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->this$0:Lcom/epson/memcardacc/ImageFileListAdapter;

    invoke-static {v1}, Lcom/epson/memcardacc/ImageFileListAdapter;->access$100(Lcom/epson/memcardacc/ImageFileListAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    iget-object v1, v1, Lcom/epson/memcardacc/CifsFileInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getThumNail()Landroid/graphics/Bitmap;
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mThumbNail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public isDirectory()Z
    .locals 1

    .line 345
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mParentDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mCifsFileInfo:Lcom/epson/memcardacc/CifsFileInfo;

    invoke-virtual {v0}, Lcom/epson/memcardacc/CifsFileInfo;->isDirectory()Z

    move-result v0

    return v0
.end method

.method public isParentDirectory()Z
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mParentDirectory:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public needThumbNail()Z
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mThumbNail:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setThumbNail(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mThumbNail:Landroid/graphics/Bitmap;

    return-void
.end method

.method public toggleCheck()V
    .locals 1

    .line 383
    iget v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 384
    iput v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 386
    iput v0, p0, Lcom/epson/memcardacc/ImageFileListAdapter$FileInfoWithCheck;->mChecked:I

    :goto_0
    return-void
.end method

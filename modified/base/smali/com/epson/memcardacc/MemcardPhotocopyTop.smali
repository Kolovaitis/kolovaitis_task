.class public Lcom/epson/memcardacc/MemcardPhotocopyTop;
.super Lcom/epson/memcardacc/MemcardTopSuper;
.source "MemcardPhotocopyTop.java"


# static fields
.field protected static final DIALOG_TYPE_NOT_READY:I = 0x65


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardTopSuper;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/memcardacc/MemcardPhotocopyTop;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/epson/memcardacc/MemcardPhotocopyTop;->launchWriter()V

    return-void
.end method

.method private launchWriter()V
    .locals 1

    .line 55
    iget v0, p0, Lcom/epson/memcardacc/MemcardPhotocopyTop;->mLaunchType:I

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 59
    iput v0, p0, Lcom/epson/memcardacc/MemcardPhotocopyTop;->mLaunchType:I

    .line 61
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardPhotocopyTop;->startMemcardStorageCheck()V

    return-void
.end method


# virtual methods
.method public back_home_button_clicked(Landroid/view/View;)V
    .locals 0

    .line 70
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardPhotocopyTop;->finish()V

    return-void
.end method

.method protected getStorageSetType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public launchReaderActivity(I)V
    .locals 0

    return-void
.end method

.method public launchWriterActivity()V
    .locals 2

    .line 100
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/memcardacc/PhotoCopyImageSelectActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    invoke-virtual {p0, v0}, Lcom/epson/memcardacc/MemcardPhotocopyTop;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method localShowDialog(I)V
    .locals 3

    .line 75
    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardPhotocopyTop;->mActivityIsFinishing:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/epson/memcardacc/MemcardPhotocopyTop;->mIsActivityForeground:Z

    if-nez v0, :cond_0

    goto :goto_2

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const v0, 0x7f0e03ab

    const v1, 0x7f0e03ac

    const/16 v2, 0x65

    .line 83
    invoke-static {v0, v1, v2}, Lcom/epson/memcardacc/LocalAlertDialogFragment;->newInstance(III)Lcom/epson/memcardacc/LocalAlertDialogFragment;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    .line 91
    invoke-virtual {p0}, Lcom/epson/memcardacc/MemcardPhotocopyTop;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    const-string v1, "error_dialog"

    invoke-virtual {v0, p1, v1}, Lcom/epson/memcardacc/LocalAlertDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 94
    :cond_2
    invoke-super {p0, p1}, Lcom/epson/memcardacc/MemcardTopSuper;->localShowDialog(I)V

    :goto_1
    return-void

    :cond_3
    :goto_2
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 19
    invoke-static {p0}, Lcom/epson/memcardacc/MemcardBitmapCache;->getInstance(Landroid/content/Context;)Lcom/epson/memcardacc/MemcardBitmapCache;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/memcardacc/MemcardPhotocopyTop;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    .line 21
    iget-object v0, p0, Lcom/epson/memcardacc/MemcardPhotocopyTop;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/epson/memcardacc/MemcardPhotocopyTop;->mBitmapCache:Lcom/epson/memcardacc/MemcardBitmapCache;

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardBitmapCache;->checkEnv()Z

    move-result v0

    if-nez v0, :cond_1

    .line 22
    :cond_0
    new-instance v0, Lcom/epson/memcardacc/MemcardTempAlertDialog;

    invoke-direct {v0}, Lcom/epson/memcardacc/MemcardTempAlertDialog;-><init>()V

    new-instance v1, Lcom/epson/memcardacc/MemcardPhotocopyTop$1;

    invoke-direct {v1, p0}, Lcom/epson/memcardacc/MemcardPhotocopyTop$1;-><init>(Lcom/epson/memcardacc/MemcardPhotocopyTop;)V

    invoke-virtual {v0, p0, v1}, Lcom/epson/memcardacc/MemcardTempAlertDialog;->showAlertDialog(Landroid/content/Context;Lcom/epson/memcardacc/MemcardTempAlertDialog$DialogCallback;)V

    :cond_1
    if-nez p1, :cond_2

    .line 33
    invoke-static {}, Lcom/epson/memcardacc/CifsAccess;->clearSmbAuthInfo()V

    .line 36
    :cond_2
    invoke-super {p0, p1}, Lcom/epson/memcardacc/MemcardTopSuper;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0036

    .line 37
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardPhotocopyTop;->setContentView(I)V

    const p1, 0x7f0e0230

    const/4 v0, 0x1

    .line 40
    invoke-virtual {p0, p1, v0}, Lcom/epson/memcardacc/MemcardPhotocopyTop;->setActionBar(IZ)V

    const/4 p1, 0x0

    .line 42
    iput p1, p0, Lcom/epson/memcardacc/MemcardPhotocopyTop;->mLaunchType:I

    const p1, 0x7f08025d

    .line 44
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/MemcardPhotocopyTop;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/epson/memcardacc/MemcardPhotocopyTop$2;

    invoke-direct {v0, p0}, Lcom/epson/memcardacc/MemcardPhotocopyTop$2;-><init>(Lcom/epson/memcardacc/MemcardPhotocopyTop;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

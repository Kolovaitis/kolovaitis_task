.class public Lcom/epson/memcardacc/EscprLibPrinter;
.super Ljava/lang/Object;
.source "EscprLibPrinter.java"


# static fields
.field public static final ERROR_PRINTER_CONNECTION:I = 0x2

.field public static final ERROR_PRINTER_LOCATION:I = 0x1

.field public static final NO_ERROR:I = 0x0

.field public static final STATUS_BUSY:I = 0x2

.field public static final STATUS_CANCELLING:I = 0x3

.field public static final STATUS_ERROR:I = 0x4

.field public static final STATUS_IDEL:I = 0x0

.field public static final STATUS_PRINTING:I = 0x1


# instance fields
.field mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkIdleOrError(Landroid/content/Context;I)I
    .locals 2

    .line 143
    new-instance v0, Lcom/epson/memcardacc/EscprLibPrinter;

    invoke-direct {v0}, Lcom/epson/memcardacc/EscprLibPrinter;-><init>()V

    const/4 v1, 0x0

    .line 144
    invoke-virtual {v0, p0, p1, v1}, Lcom/epson/memcardacc/EscprLibPrinter;->init(Landroid/content/Context;IZ)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    .line 147
    :cond_0
    invoke-virtual {v0}, Lcom/epson/memcardacc/EscprLibPrinter;->getStatus()I

    move-result p0

    .line 148
    invoke-virtual {v0}, Lcom/epson/memcardacc/EscprLibPrinter;->release()V

    if-eqz p0, :cond_1

    const/4 p1, 0x4

    if-eq p0, p1, :cond_1

    const/4 p0, 0x2

    return p0

    :cond_1
    return v1
.end method


# virtual methods
.method getCurrentPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;
    .locals 0

    .line 163
    invoke-static {p1}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p1

    return-object p1
.end method

.method getMaintainPrinter()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 1

    .line 171
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    return-object v0
.end method

.method public getStatus()I
    .locals 3

    .line 106
    iget-object v0, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetStatus()I

    move-result v0

    const/4 v1, 0x4

    if-eqz v0, :cond_0

    return v1

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object v0

    const/4 v2, 0x0

    .line 112
    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    return v1

    :pswitch_0
    const/4 v0, 0x3

    return v0

    :pswitch_1
    const/4 v0, 0x2

    return v0

    :pswitch_2
    const/4 v0, 0x1

    return v0

    :pswitch_3
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public init(Landroid/content/Context;IZ)I
    .locals 8

    .line 47
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 48
    invoke-virtual {p0, p1}, Lcom/epson/memcardacc/EscprLibPrinter;->getCurrentPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v3

    const/4 v4, 0x3

    const/4 v5, 0x1

    if-eq v3, v5, :cond_0

    if-eq v3, v4, :cond_0

    return v5

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/epson/memcardacc/EscprLibPrinter;->getMaintainPrinter()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v6

    iput-object v6, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 62
    iget-object v6, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/4 v7, 0x2

    invoke-virtual {v6, p1, v7}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    .line 63
    iget-object v6, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v6, p2, v1, v2, v3}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p2

    const/4 v6, 0x0

    if-eqz p2, :cond_1

    .line 66
    iput-object v6, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return v7

    .line 69
    :cond_1
    iget-object p2, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result p2

    if-eqz p2, :cond_2

    .line 71
    iput-object v6, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return v7

    :cond_2
    if-eq v3, v5, :cond_4

    if-eq v3, v4, :cond_3

    goto :goto_0

    .line 85
    :cond_3
    iget-object p1, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetId()Ljava/lang/String;

    move-result-object p1

    .line 86
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 89
    iput-object v6, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return v7

    .line 76
    :cond_4
    iget-object p2, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetIp()Ljava/lang/String;

    move-result-object p2

    .line 77
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    if-eqz p3, :cond_5

    .line 80
    invoke-virtual {v0, p2}, Lepson/print/MyPrinter;->setIp(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v0, p1}, Lepson/print/MyPrinter;->setCurPrinter(Landroid/content/Context;)V

    :cond_5
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public release()V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 133
    iput-object v0, p0, Lcom/epson/memcardacc/EscprLibPrinter;->mMaintainPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-void
.end method

.class Lcom/epson/memcardacc/MemcardImageGrid$3;
.super Landroid/os/Handler;
.source "MemcardImageGrid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/memcardacc/MemcardImageGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/memcardacc/MemcardImageGrid;


# direct methods
.method constructor <init>(Lcom/epson/memcardacc/MemcardImageGrid;)V
    .locals 0

    .line 527
    iput-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$3;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .line 530
    iget p1, p1, Landroid/os/Message;->what:I

    packed-switch p1, :pswitch_data_0

    return-void

    .line 558
    :pswitch_0
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$3;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-virtual {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->execNextTaskOrIdle()V

    return-void

    .line 534
    :pswitch_1
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$3;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$000(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p1, "MemcardImageGrid"

    .line 535
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "status :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/memcardacc/MemcardImageGrid$3;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$000(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    sget-object p1, Lcom/epson/memcardacc/MemcardImageGrid$4;->$SwitchMap$android$os$AsyncTask$Status:[I

    iget-object v0, p0, Lcom/epson/memcardacc/MemcardImageGrid$3;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {v0}, Lcom/epson/memcardacc/MemcardImageGrid;->access$000(Lcom/epson/memcardacc/MemcardImageGrid;)Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/memcardacc/MemcardImageGrid$CifsFileListLoader;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/AsyncTask$Status;->ordinal()I

    move-result v0

    aget p1, p1, v0

    packed-switch p1, :pswitch_data_1

    const-string p1, "MemcardImageGrid"

    const-string v0, "unkown AsyncTask.getStatus() value"

    .line 548
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 540
    :pswitch_2
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$3;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$100(Lcom/epson/memcardacc/MemcardImageGrid;)Landroid/os/Handler;

    move-result-object p1

    const/4 v0, 0x1

    const-wide/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :cond_0
    const-string p1, "MemcardImageGrid"

    const-string v0, "mCifsFileListLoader == null"

    .line 552
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :pswitch_3
    iget-object p1, p0, Lcom/epson/memcardacc/MemcardImageGrid$3;->this$0:Lcom/epson/memcardacc/MemcardImageGrid;

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardImageGrid;->access$100(Lcom/epson/memcardacc/MemcardImageGrid;)Landroid/os/Handler;

    move-result-object p1

    const/4 v0, 0x2

    const-wide/16 v1, 0x12c

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.class public Lcom/epson/lib/escani2/ScannerI2Info;
.super Ljava/lang/Object;
.source "ScannerI2Info.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final ALIGNMENT_UNDEFINED:I = 0xff


# instance fields
.field public adfAlignment:I

.field public adfDuplexType:I

.field public adfInstalled:Z

.field public adfSensorHeight:I

.field public adfSensorWidth:I

.field public adfType:I

.field public flatbedAlignment:I

.field public flatbedInstalled:Z

.field public flatbedSensorHeight:I

.field public flatbedSensorWidth:I

.field public imageSensorHeight:I

.field public imageSensorWidth:I

.field public productName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAlignment(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)I
    .locals 1

    .line 57
    sget-object v0, Lcom/epson/lib/escani2/ScannerI2Info$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

    invoke-virtual {p1}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/16 v0, 0xff

    packed-switch p1, :pswitch_data_0

    return v0

    .line 64
    :pswitch_0
    iget-boolean p1, p0, Lcom/epson/lib/escani2/ScannerI2Info;->flatbedInstalled:Z

    if-nez p1, :cond_0

    return v0

    .line 67
    :cond_0
    iget p1, p0, Lcom/epson/lib/escani2/ScannerI2Info;->flatbedAlignment:I

    return p1

    .line 59
    :pswitch_1
    iget-boolean p1, p0, Lcom/epson/lib/escani2/ScannerI2Info;->adfInstalled:Z

    if-nez p1, :cond_1

    return v0

    .line 62
    :cond_1
    iget p1, p0, Lcom/epson/lib/escani2/ScannerI2Info;->adfAlignment:I

    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSensorPixelSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I
    .locals 6

    .line 80
    invoke-virtual {p0, p1}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I

    move-result-object p1

    .line 82
    invoke-virtual {p0}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorSizeBaseResolution()I

    move-result v0

    if-eq p2, v0, :cond_0

    const/4 v1, 0x0

    .line 86
    aget v2, p1, v1

    int-to-double v2, v2

    int-to-double v4, p2

    mul-double v2, v2, v4

    int-to-double v4, v0

    div-double/2addr v2, v4

    double-to-int p2, v2

    aput p2, p1, v1

    :cond_0
    if-eq p3, v0, :cond_1

    const/4 p2, 0x1

    .line 91
    aget v1, p1, p2

    int-to-double v1, v1

    int-to-double v3, p3

    mul-double v1, v1, v3

    int-to-double v3, v0

    div-double/2addr v1, v3

    double-to-int p3, v1

    aput p3, p1, p2

    :cond_1
    return-object p1
.end method

.method public getSensorSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I
    .locals 4

    .line 105
    sget-object v0, Lcom/epson/lib/escani2/ScannerI2Info$1;->$SwitchMap$com$epson$lib$escani2$EscanI2Lib$InputUnit:[I

    invoke-virtual {p1}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq p1, v3, :cond_1

    .line 113
    iget-boolean p1, p0, Lcom/epson/lib/escani2/ScannerI2Info;->flatbedInstalled:Z

    if-nez p1, :cond_0

    return-object v0

    .line 116
    :cond_0
    new-array p1, v2, [I

    iget v0, p0, Lcom/epson/lib/escani2/ScannerI2Info;->flatbedSensorWidth:I

    aput v0, p1, v1

    iget v0, p0, Lcom/epson/lib/escani2/ScannerI2Info;->flatbedSensorHeight:I

    aput v0, p1, v3

    return-object p1

    .line 107
    :cond_1
    iget-boolean p1, p0, Lcom/epson/lib/escani2/ScannerI2Info;->adfInstalled:Z

    if-nez p1, :cond_2

    return-object v0

    .line 110
    :cond_2
    new-array p1, v2, [I

    iget v0, p0, Lcom/epson/lib/escani2/ScannerI2Info;->adfSensorWidth:I

    aput v0, p1, v1

    iget v0, p0, Lcom/epson/lib/escani2/ScannerI2Info;->adfSensorHeight:I

    aput v0, p1, v3

    return-object p1
.end method

.method public getSensorSizeBaseResolution()I
    .locals 1

    const/16 v0, 0x64

    return v0
.end method

.method public isAdfAlignmentInvalid()Z
    .locals 2

    .line 134
    iget v0, p0, Lcom/epson/lib/escani2/ScannerI2Info;->adfAlignment:I

    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.class public interface abstract Lcom/epson/lib/escani2/EscanI2NativeInterface;
.super Ljava/lang/Object;
.source "EscanI2NativeInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;
    }
.end annotation


# virtual methods
.method public abstract calibrateAdf()I
.end method

.method public abstract cancelScanJni()V
.end method

.method public abstract cancelWaitingJni()V
.end method

.method public abstract cleanAdf()I
.end method

.method public abstract freeBlockedCalibrateAdf()I
.end method

.method public abstract freeBlockedCleanAdf()I
.end method

.method public abstract getCapability(Lcom/epson/lib/escani2/ScannerI2Capability;)I
.end method

.method public abstract getEscanI2LibError()I
.end method

.method public abstract getMaintAppliedParameters()[I
.end method

.method public abstract getMaintCapability()[I
.end method

.method public abstract getScanPages(I)I
.end method

.method public abstract getScannerInfo(Lcom/epson/lib/escani2/ScannerI2Info;)I
.end method

.method public abstract initializeLibraryJni()I
.end method

.method public abstract releaseLibraryJni()I
.end method

.method public abstract releaseScannerJni()I
.end method

.method public abstract scan(ILjava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract setPageWriteCallback(Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;)V
.end method

.method public abstract setPowerOffTime(I)I
.end method

.method public abstract setPowerSaveTime(I)I
.end method

.method public abstract setScannerJni(Ljava/lang/String;)I
.end method

.method public abstract startScan(IIIIIIIIIIIIIIIIIIIII)I
.end method

.method public abstract waitStartRequest()I
.end method

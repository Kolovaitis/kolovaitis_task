.class public final enum Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;
.super Ljava/lang/Enum;
.source "EscanI2Lib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/lib/escani2/EscanI2Lib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PagerGuidPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

.field public static final enum CENTER:Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

.field public static final enum UPPER_LEFT:Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 141
    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    const-string v1, "CENTER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;->CENTER:Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    const-string v1, "UPPER_LEFT"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;->UPPER_LEFT:Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    const/4 v0, 0x2

    .line 140
    new-array v0, v0, [Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;->CENTER:Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;->UPPER_LEFT:Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    aput-object v1, v0, v3

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;
    .locals 1

    .line 140
    const-class v0, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    return-object p0
.end method

.method public static values()[Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;
    .locals 1

    .line 140
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    invoke-virtual {v0}, [Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;

    return-object v0
.end method

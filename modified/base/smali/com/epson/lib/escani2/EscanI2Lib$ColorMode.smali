.class public final enum Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;
.super Ljava/lang/Enum;
.source "EscanI2Lib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/lib/escani2/EscanI2Lib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ColorMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

.field public static final enum COLOR_24BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

.field public static final enum GRAYSCALE_8BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

.field public static final enum MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 83
    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    const-string v1, "MONO_1BIT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    const-string v1, "GRAYSCALE_8BIT"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->GRAYSCALE_8BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    const-string v1, "COLOR_24BIT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->COLOR_24BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    const/4 v0, 0x3

    .line 80
    new-array v0, v0, [Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->MONO_1BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->GRAYSCALE_8BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->COLOR_24BIT:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;
    .locals 1

    .line 80
    const-class v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    return-object p0
.end method

.method public static values()[Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;
    .locals 1

    .line 80
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    invoke-virtual {v0}, [Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    return-object v0
.end method

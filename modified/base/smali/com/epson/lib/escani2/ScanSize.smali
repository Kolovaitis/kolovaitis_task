.class public Lcom/epson/lib/escani2/ScanSize;
.super Ljava/lang/Object;
.source "ScanSize.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/lib/escani2/ScanSize$PaperSize;
    }
.end annotation


# instance fields
.field private mOffsetX:I

.field private mOffsetY:I

.field private mPixelHeight:I

.field private mPixelWidth:I

.field private mPortrait:Z

.field private mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;


# direct methods
.method public constructor <init>(IIII)V
    .locals 1

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    sget-object v0, Lcom/epson/lib/escani2/ScanSize$PaperSize;->PIXEL:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    iput-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    .line 89
    iput p1, p0, Lcom/epson/lib/escani2/ScanSize;->mPixelWidth:I

    .line 90
    iput p2, p0, Lcom/epson/lib/escani2/ScanSize;->mPixelHeight:I

    .line 91
    iput p3, p0, Lcom/epson/lib/escani2/ScanSize;->mOffsetX:I

    .line 92
    iput p4, p0, Lcom/epson/lib/escani2/ScanSize;->mOffsetY:I

    const/4 p1, 0x1

    .line 93
    iput-boolean p1, p0, Lcom/epson/lib/escani2/ScanSize;->mPortrait:Z

    return-void
.end method

.method public constructor <init>(Lcom/epson/lib/escani2/ScanSize$PaperSize;Z)V
    .locals 0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    .line 84
    iput-boolean p2, p0, Lcom/epson/lib/escani2/ScanSize;->mPortrait:Z

    return-void
.end method

.method static get300DpiPixelFromPaperSize(Lcom/epson/lib/escani2/ScanSize$PaperSize;)[I
    .locals 1

    .line 51
    sget-object v0, Lcom/epson/lib/escani2/ScanSize$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    invoke-virtual {p0}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x2

    packed-switch p0, :pswitch_data_0

    .line 78
    new-array p0, v0, [I

    fill-array-data p0, :array_0

    return-object p0

    .line 75
    :pswitch_0
    new-array p0, v0, [I

    fill-array-data p0, :array_1

    return-object p0

    .line 73
    :pswitch_1
    new-array p0, v0, [I

    fill-array-data p0, :array_2

    return-object p0

    .line 71
    :pswitch_2
    new-array p0, v0, [I

    fill-array-data p0, :array_3

    return-object p0

    .line 69
    :pswitch_3
    new-array p0, v0, [I

    fill-array-data p0, :array_4

    return-object p0

    .line 67
    :pswitch_4
    new-array p0, v0, [I

    fill-array-data p0, :array_5

    return-object p0

    .line 65
    :pswitch_5
    new-array p0, v0, [I

    fill-array-data p0, :array_6

    return-object p0

    .line 63
    :pswitch_6
    new-array p0, v0, [I

    fill-array-data p0, :array_7

    return-object p0

    .line 61
    :pswitch_7
    new-array p0, v0, [I

    fill-array-data p0, :array_8

    return-object p0

    .line 59
    :pswitch_8
    new-array p0, v0, [I

    fill-array-data p0, :array_9

    return-object p0

    .line 57
    :pswitch_9
    new-array p0, v0, [I

    fill-array-data p0, :array_a

    return-object p0

    .line 55
    :pswitch_a
    new-array p0, v0, [I

    fill-array-data p0, :array_b

    return-object p0

    .line 53
    :pswitch_b
    new-array p0, v0, [I

    fill-array-data p0, :array_c

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data

    :array_1
    .array-data 4
        0x9f6
        0x1068
    .end array-data

    :array_2
    .array-data 4
        0x3f3
        0x27e
    .end array-data

    :array_3
    .array-data 4
        0x41b
        0x28a
    .end array-data

    :array_4
    .array-data 4
        0x267
        0x36a
    .end array-data

    :array_5
    .array-data 4
        0x4d8
        0x6d4
    .end array-data

    :array_6
    .array-data 4
        0x6d4
        0x9b0
    .end array-data

    :array_7
    .array-data 4
        0x5e8
        0x866
    .end array-data

    :array_8
    .array-data 4
        0x865
        0xbdb
    .end array-data

    :array_9
    .array-data 4
        0x9f6
        0x1068
    .end array-data

    :array_a
    .array-data 4
        0x9f6
        0xce4
    .end array-data

    :array_b
    .array-data 4
        0x9b0
        0xdb3
    .end array-data

    :array_c
    .array-data 4
        0xdb3
        0x135f
    .end array-data
.end method

.method private getCenterAlignPixelSizeValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I
    .locals 5

    .line 258
    invoke-virtual {p0, p1, p2}, Lcom/epson/lib/escani2/ScanSize;->getSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 260
    aput v2, v0, v1

    const/4 v3, 0x3

    .line 261
    aput v2, v0, v3

    .line 263
    iget-object v3, p2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    iget v4, p2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    iget p2, p2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    invoke-virtual {p1, v3, v4, p2}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorPixelSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object p1

    .line 266
    aget p2, v0, v2

    aget v3, p1, v2

    if-le p2, v3, :cond_0

    .line 267
    aget p2, p1, v2

    aput p2, v0, v2

    goto :goto_0

    .line 269
    :cond_0
    aget p2, p1, v2

    aget v2, v0, v2

    sub-int/2addr p2, v2

    .line 270
    div-int/2addr p2, v1

    aput p2, v0, v1

    :goto_0
    const/4 p2, 0x1

    .line 274
    aget v1, v0, p2

    aget v2, p1, p2

    if-le v1, v2, :cond_1

    .line 275
    aget p1, p1, p2

    aput p1, v0, p2

    :cond_1
    return-object v0
.end method

.method private static getI2AlignmentValue(I)I
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x2

    return p0

    :cond_0
    return v0
.end method

.method private getLeftAlignPixelSizeValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I
    .locals 6

    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/epson/lib/escani2/ScanSize;->getSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object v0

    .line 288
    iget-object v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    iget v2, p2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    iget p2, p2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    invoke-virtual {p1, v1, v2, p2}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorPixelSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object p1

    const/4 p2, 0x0

    .line 291
    aget v1, v0, p2

    const/4 v2, 0x2

    aget v3, v0, v2

    add-int/2addr v1, v3

    if-lez v1, :cond_5

    const/4 v1, 0x1

    aget v3, v0, v1

    const/4 v4, 0x3

    aget v5, v0, v4

    add-int/2addr v3, v5

    if-lez v3, :cond_5

    aget v3, v0, v2

    aget v5, p1, p2

    if-ge v3, v5, :cond_5

    aget v3, v0, v4

    aget v5, p1, v1

    if-lt v3, v5, :cond_0

    goto :goto_2

    .line 299
    :cond_0
    aget v3, v0, v2

    if-gez v3, :cond_1

    .line 301
    aget v3, v0, v2

    aget v5, v0, p2

    add-int/2addr v3, v5

    aput v3, v0, p2

    .line 302
    aput p2, v0, v2

    .line 305
    :cond_1
    aget v3, v0, p2

    aget v5, v0, v2

    add-int/2addr v3, v5

    aget v5, p1, p2

    if-lt v3, v5, :cond_2

    aget v3, p1, p2

    aget v2, v0, v2

    sub-int/2addr v3, v2

    goto :goto_0

    :cond_2
    aget v3, v0, p2

    :goto_0
    aput v3, v0, p2

    .line 308
    aget v2, v0, v4

    if-gez v2, :cond_3

    .line 310
    aget v2, v0, v4

    aget v3, v0, v1

    add-int/2addr v2, v3

    aput v2, v0, v1

    .line 311
    aput p2, v0, v4

    .line 314
    :cond_3
    aget p2, v0, v1

    aget v2, v0, v4

    add-int/2addr p2, v2

    aget v2, p1, v1

    if-lt p2, v2, :cond_4

    aget p1, p1, v1

    aget p2, v0, v4

    sub-int/2addr p1, p2

    goto :goto_1

    :cond_4
    aget p1, v0, v1

    :goto_1
    aput p1, v0, v1

    return-object v0

    :cond_5
    :goto_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public static getLocaleDefaultSize()Lcom/epson/lib/escani2/ScanSize;
    .locals 1

    .line 448
    invoke-static {}, Lepson/scan/lib/ScanSizeHelper;->getDefaultScanSize()I

    move-result v0

    invoke-static {v0}, Lepson/scan/i2lib/I2ScanParamArbiter;->getI2ScanSize(I)Lcom/epson/lib/escani2/ScanSize;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method adjustSizeFromScannerInfo([ILcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I
    .locals 3
    .param p1    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/lib/escani2/ScannerI2Info;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/epson/lib/escani2/ScanI2Params;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 182
    iget-object v0, p3, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    iget v1, p3, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    iget p3, p3, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    invoke-virtual {p2, v0, v1, p3}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorPixelSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object p2

    const/4 p3, 0x0

    .line 186
    aget v0, p1, p3

    aget v1, p2, p3

    if-le v0, v1, :cond_0

    .line 187
    aget v0, p2, p3

    aput v0, p1, p3

    .line 190
    :cond_0
    aget v0, p1, p3

    const/4 v1, 0x2

    aget v2, p1, v1

    add-int/2addr v0, v2

    aget p3, p2, p3

    sub-int/2addr v0, p3

    if-lez v0, :cond_1

    .line 193
    aget p3, p1, v1

    sub-int/2addr p3, v0

    aput p3, p1, v1

    :cond_1
    const/4 p3, 0x1

    .line 197
    aget v0, p1, p3

    aget v1, p2, p3

    if-le v0, v1, :cond_2

    .line 198
    aget v0, p2, p3

    aput v0, p1, p3

    .line 201
    :cond_2
    aget v0, p1, p3

    const/4 v1, 0x3

    aget v2, p1, v1

    add-int/2addr v0, v2

    aget p2, p2, p3

    sub-int/2addr v0, p2

    if-lez v0, :cond_3

    .line 204
    aget p2, p1, v1

    sub-int/2addr p2, v0

    aput p2, p1, v1

    :cond_3
    return-object p1
.end method

.method calcOffset(III)I
    .locals 0

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    :pswitch_0
    sub-int/2addr p3, p2

    return p3

    :pswitch_1
    sub-int/2addr p3, p2

    .line 438
    div-int/lit8 p3, p3, 0x2

    return p3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 104
    :cond_0
    instance-of v1, p1, Lcom/epson/lib/escani2/ScanSize;

    if-nez v1, :cond_1

    return v0

    .line 108
    :cond_1
    check-cast p1, Lcom/epson/lib/escani2/ScanSize;

    .line 109
    iget-object v1, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    iget-object v2, p1, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v1, v2}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v0

    .line 113
    :cond_2
    sget-object v1, Lcom/epson/lib/escani2/ScanSize$1;->$SwitchMap$com$epson$lib$escani2$ScanSize$PaperSize:[I

    iget-object v2, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    packed-switch v1, :pswitch_data_0

    .line 126
    iget-boolean p1, p0, Lcom/epson/lib/escani2/ScanSize;->mPortrait:Z

    return p1

    :pswitch_0
    return v2

    .line 116
    :pswitch_1
    iget v1, p0, Lcom/epson/lib/escani2/ScanSize;->mPixelWidth:I

    iget v3, p1, Lcom/epson/lib/escani2/ScanSize;->mPixelWidth:I

    if-ne v1, v3, :cond_3

    iget v1, p0, Lcom/epson/lib/escani2/ScanSize;->mPixelHeight:I

    iget v3, p1, Lcom/epson/lib/escani2/ScanSize;->mPixelHeight:I

    if-ne v1, v3, :cond_3

    iget v1, p0, Lcom/epson/lib/escani2/ScanSize;->mOffsetX:I

    iget v3, p1, Lcom/epson/lib/escani2/ScanSize;->mOffsetX:I

    if-ne v1, v3, :cond_3

    iget v1, p0, Lcom/epson/lib/escani2/ScanSize;->mOffsetY:I

    iget p1, p1, Lcom/epson/lib/escani2/ScanSize;->mOffsetY:I

    if-ne v1, p1, :cond_3

    return v2

    :cond_3
    return v0

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method getOffset(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I
    .locals 6

    .line 402
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    sget-object v1, Lcom/epson/lib/escani2/ScanSize$PaperSize;->PIXEL:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    .line 403
    new-array p1, v2, [I

    iget p2, p0, Lcom/epson/lib/escani2/ScanSize;->mOffsetX:I

    aput p2, p1, v3

    const/4 p2, 0x1

    iget p3, p0, Lcom/epson/lib/escani2/ScanSize;->mOffsetY:I

    aput p3, p1, p2

    return-object p1

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    sget-object v1, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    if-ne v0, v1, :cond_1

    .line 405
    new-array p1, v2, [I

    fill-array-data p1, :array_0

    return-object p1

    .line 408
    :cond_1
    invoke-virtual {p1, p2}, Lcom/epson/lib/escani2/ScannerI2Info;->getAlignment(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)I

    move-result v0

    .line 409
    invoke-virtual {p1, p2}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I

    move-result-object v1

    .line 418
    new-array v2, v2, [I

    fill-array-data v2, :array_1

    .line 421
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/epson/lib/escani2/ScanSize;->getPixel(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object p2

    aget p2, p2, v3

    aget p4, v1, v3

    int-to-double v4, p4

    int-to-double p3, p3

    mul-double v4, v4, p3

    .line 423
    invoke-virtual {p1}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorSizeBaseResolution()I

    move-result p1

    int-to-double p3, p1

    div-double/2addr v4, p3

    .line 422
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide p3

    long-to-int p1, p3

    .line 420
    invoke-virtual {p0, v0, p2, p1}, Lcom/epson/lib/escani2/ScanSize;->calcOffset(III)I

    move-result p1

    aput p1, v2, v3

    return-object v2

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method getPixel(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I
    .locals 6

    .line 362
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    sget-object v1, Lcom/epson/lib/escani2/ScanSize$PaperSize;->MAX:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    if-ne v0, v1, :cond_0

    .line 363
    invoke-virtual {p1, p2, p3, p4}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorPixelSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object p1

    return-object p1

    .line 366
    :cond_0
    iget-object p1, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    sget-object p2, Lcom/epson/lib/escani2/ScanSize$PaperSize;->PIXEL:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p2, :cond_1

    const/4 p1, 0x2

    .line 367
    new-array p1, p1, [I

    iget p2, p0, Lcom/epson/lib/escani2/ScanSize;->mPixelWidth:I

    aput p2, p1, v1

    iget p2, p0, Lcom/epson/lib/escani2/ScanSize;->mPixelHeight:I

    aput p2, p1, v0

    return-object p1

    .line 370
    :cond_1
    iget-object p1, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-static {p1}, Lcom/epson/lib/escani2/ScanSize;->get300DpiPixelFromPaperSize(Lcom/epson/lib/escani2/ScanSize$PaperSize;)[I

    move-result-object p1

    .line 371
    iget-boolean p2, p0, Lcom/epson/lib/escani2/ScanSize;->mPortrait:Z

    if-nez p2, :cond_2

    .line 372
    aget p2, p1, v1

    .line 373
    aget v2, p1, v0

    aput v2, p1, v1

    .line 374
    aput p2, p1, v0

    :cond_2
    const/16 p2, 0x12c

    if-eq p3, p2, :cond_3

    .line 380
    aget v2, p1, v1

    int-to-double v2, v2

    int-to-double v4, p3

    mul-double v2, v2, v4

    int-to-double v4, p2

    div-double/2addr v2, v4

    double-to-int p3, v2

    aput p3, p1, v1

    :cond_3
    if-eq p4, p2, :cond_4

    .line 385
    aget p3, p1, v0

    int-to-double v1, p3

    int-to-double p3, p4

    mul-double v1, v1, p3

    int-to-double p2, p2

    div-double/2addr v1, p2

    double-to-int p2, v1

    aput p2, p1, v0

    :cond_4
    return-object p1
.end method

.method public getSize()Lcom/epson/lib/escani2/ScanSize$PaperSize;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    return-object v0
.end method

.method getSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I
    .locals 4

    const/4 v0, 0x4

    .line 340
    new-array v0, v0, [I

    .line 341
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/epson/lib/escani2/ScanSize;->getPixel(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object v1

    const/4 v2, 0x0

    .line 342
    aget v3, v1, v2

    aput v3, v0, v2

    const/4 v3, 0x1

    .line 343
    aget v1, v1, v3

    aput v1, v0, v3

    .line 345
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/epson/lib/escani2/ScanSize;->getOffset(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object p1

    .line 346
    aget p2, p1, v2

    const/4 p3, 0x2

    aput p2, v0, p3

    .line 347
    aget p1, p1, v3

    const/4 p2, 0x3

    aput p1, v0, p2

    return-object v0
.end method

.method public getSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I
    .locals 2

    .line 328
    iget-object v0, p2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    iget v1, p2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    iget p2, p2, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/epson/lib/escani2/ScanSize;->getSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;II)[I

    move-result-object p1

    return-object p1
.end method

.method public getValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I
    .locals 1

    .line 165
    invoke-virtual {p0, p1, p2}, Lcom/epson/lib/escani2/ScanSize;->getValidScanSizeInternal(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 170
    :cond_0
    invoke-virtual {p0, v0, p1, p2}, Lcom/epson/lib/escani2/ScanSize;->adjustSizeFromScannerInfo([ILcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object p1

    return-object p1
.end method

.method getValidScanSizeInternal(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 212
    invoke-virtual {p1}, Lcom/epson/lib/escani2/ScannerI2Info;->isAdfAlignmentInvalid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget v0, p2, Lcom/epson/lib/escani2/ScanI2Params;->manualAdfAlignment:I

    invoke-static {v0}, Lcom/epson/lib/escani2/ScanSize;->getI2AlignmentValue(I)I

    move-result v0

    .line 216
    iput v0, p1, Lcom/epson/lib/escani2/ScannerI2Info;->adfAlignment:I

    .line 219
    :cond_0
    invoke-virtual {p0}, Lcom/epson/lib/escani2/ScanSize;->isPixelSize()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220
    iget-object v0, p2, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    if-ne v0, v1, :cond_2

    .line 221
    iget v0, p1, Lcom/epson/lib/escani2/ScannerI2Info;->adfAlignment:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 223
    invoke-direct {p0, p1, p2}, Lcom/epson/lib/escani2/ScanSize;->getCenterAlignPixelSizeValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object p1

    return-object p1

    .line 226
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/epson/lib/escani2/ScanSize;->getLeftAlignPixelSizeValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object p1

    return-object p1

    .line 230
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/epson/lib/escani2/ScanSize;->getLeftAlignPixelSizeValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object p1

    return-object p1

    .line 233
    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/epson/lib/escani2/ScanSize;->getSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    sget-object v1, Lcom/epson/lib/escani2/ScanSize$PaperSize;->PIXEL:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    if-ne v0, v1, :cond_0

    .line 133
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 134
    iget v1, p0, Lcom/epson/lib/escani2/ScanSize;->mPixelWidth:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 135
    iget v1, p0, Lcom/epson/lib/escani2/ScanSize;->mPixelHeight:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 136
    iget v1, p0, Lcom/epson/lib/escani2/ScanSize;->mOffsetX:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 137
    iget v1, p0, Lcom/epson/lib/escani2/ScanSize;->mOffsetY:I

    add-int/2addr v0, v1

    return v0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/ScanSize$PaperSize;->hashCode()I

    move-result v0

    return v0
.end method

.method public isPixelSize()Z
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanSize;->mSizeType:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    sget-object v1, Lcom/epson/lib/escani2/ScanSize$PaperSize;->PIXEL:Lcom/epson/lib/escani2/ScanSize$PaperSize;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

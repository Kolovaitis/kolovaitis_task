.class Lcom/epson/lib/escani2/EscanI2NativeIf;
.super Ljava/lang/Object;
.source "EscanI2NativeIf.java"

# interfaces
.implements Lcom/epson/lib/escani2/EscanI2NativeInterface;


# instance fields
.field private mNativeInscance:J

.field mPageWriteCallback:Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "escani2"

    .line 23
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private declared-synchronized pageWriteEndCallback(II)I
    .locals 1

    monitor-enter p0

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/epson/lib/escani2/EscanI2NativeIf;->mPageWriteCallback:Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/epson/lib/escani2/EscanI2NativeIf;->mPageWriteCallback:Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;

    invoke-interface {v0, p1, p2}, Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;->pageWriteDone(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 p1, 0x1

    .line 136
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public native calibrateAdf()I
.end method

.method public native cancelScanJni()V
.end method

.method public native cancelWaitingJni()V
.end method

.method public native cleanAdf()I
.end method

.method public native freeBlockedCalibrateAdf()I
.end method

.method public native freeBlockedCleanAdf()I
.end method

.method public native getCapability(Lcom/epson/lib/escani2/ScannerI2Capability;)I
.end method

.method public getEscanI2LibError()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public native getMaintAppliedParameters()[I
.end method

.method public native getMaintCapability()[I
.end method

.method public native getScanPages(I)I
.end method

.method public native getScannerInfo(Lcom/epson/lib/escani2/ScannerI2Info;)I
.end method

.method public native initializeLibraryJni()I
.end method

.method public native releaseLibraryJni()I
.end method

.method public native releaseScannerJni()I
.end method

.method public native scan(ILjava/lang/String;Ljava/lang/String;)I
.end method

.method public declared-synchronized setPageWriteCallback(Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;)V
    .locals 0

    monitor-enter p0

    .line 147
    :try_start_0
    iput-object p1, p0, Lcom/epson/lib/escani2/EscanI2NativeIf;->mPageWriteCallback:Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public native setPowerOffTime(I)I
.end method

.method public native setPowerSaveTime(I)I
.end method

.method public native setScannerJni(Ljava/lang/String;)I
.end method

.method public native startScan(IIIIIIIIIIIIIIIIIIIII)I
.end method

.method public native waitStartRequest()I
.end method

.class public final enum Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;
.super Ljava/lang/Enum;
.source "EscanI2Lib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/lib/escani2/EscanI2Lib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DoubleFeedLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

.field public static final enum LEVEL_1:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

.field public static final enum LEVEL_2:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

.field public static final enum LEVEL_NONE:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

.field public static final enum LEVEL_UNDEFINED:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 123
    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    const-string v1, "LEVEL_NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_NONE:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    const-string v1, "LEVEL_1"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_1:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    const-string v1, "LEVEL_2"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_2:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    const-string v1, "LEVEL_UNDEFINED"

    const/4 v5, 0x3

    const/16 v6, 0xff

    invoke-direct {v0, v1, v5, v6}, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_UNDEFINED:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    const/4 v0, 0x4

    .line 121
    new-array v0, v0, [Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_NONE:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_1:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_2:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->LEVEL_UNDEFINED:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    aput-object v1, v0, v5

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 127
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 128
    iput p3, p0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->mValue:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;
    .locals 1

    .line 121
    const-class v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    return-object p0
.end method

.method public static values()[Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;
    .locals 1

    .line 121
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    invoke-virtual {v0}, [Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 132
    iget v0, p0, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->mValue:I

    return v0
.end method

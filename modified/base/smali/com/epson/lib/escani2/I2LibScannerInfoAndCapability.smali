.class public Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;
.super Ljava/lang/Object;
.source "I2LibScannerInfoAndCapability.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public scannerI2Capability:Lcom/epson/lib/escani2/ScannerI2Capability;

.field public scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Lcom/epson/lib/escani2/ScannerI2Capability;

    invoke-direct {v0}, Lcom/epson/lib/escani2/ScannerI2Capability;-><init>()V

    iput-object v0, p0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Capability:Lcom/epson/lib/escani2/ScannerI2Capability;

    .line 14
    new-instance v0, Lcom/epson/lib/escani2/ScannerI2Info;

    invoke-direct {v0}, Lcom/epson/lib/escani2/ScannerI2Info;-><init>()V

    iput-object v0, p0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    return-void
.end method


# virtual methods
.method public canDoubleSideScan()Z
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->isAdfAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Capability:Lcom/epson/lib/escani2/ScannerI2Capability;

    iget-object v0, v0, Lcom/epson/lib/escani2/ScannerI2Capability;->adfCapability:Lcom/epson/lib/escani2/AdfCapability;

    iget-boolean v0, v0, Lcom/epson/lib/escani2/AdfCapability;->duplex:Z

    return v0
.end method

.method public getAlignment(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)I
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    invoke-virtual {v0, p1}, Lcom/epson/lib/escani2/ScannerI2Info;->getAlignment(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)I

    move-result p1

    return p1
.end method

.method public getSensorSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    invoke-virtual {v0, p1}, Lcom/epson/lib/escani2/ScannerI2Info;->getSensorSize(Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;)[I

    move-result-object p1

    return-object p1
.end method

.method public isAdfAvailable()Z
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    iget-boolean v0, v0, Lcom/epson/lib/escani2/ScannerI2Info;->adfInstalled:Z

    return v0
.end method

.method public isFlatbedAvailable()Z
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    iget-boolean v0, v0, Lcom/epson/lib/escani2/ScannerI2Info;->flatbedInstalled:Z

    return v0
.end method

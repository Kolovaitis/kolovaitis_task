.class public Lcom/epson/lib/escani2/ScanI2PropertyTaker;
.super Ljava/lang/Object;
.source "ScanI2PropertyTaker.java"


# instance fields
.field private mEscanI2LibError:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/epson/lib/escani2/ScanI2PropertyTaker;->mEscanI2LibError:I

    return-void
.end method


# virtual methods
.method public getEscanI2LibError()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/epson/lib/escani2/ScanI2PropertyTaker;->mEscanI2LibError:I

    return v0
.end method

.method public getScannerInfoAndCapability(Ljava/lang/String;)Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;
    .locals 4

    if-eqz p1, :cond_4

    .line 14
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 18
    new-instance v0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;

    invoke-direct {v0}, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;-><init>()V

    .line 20
    invoke-static {}, Lcom/epson/lib/escani2/EscanI2Lib;->getInstance()Lcom/epson/lib/escani2/EscanI2Lib;

    move-result-object v1

    .line 22
    invoke-virtual {v1}, Lcom/epson/lib/escani2/EscanI2Lib;->initializeLibrary()I

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 25
    iput v2, p0, Lcom/epson/lib/escani2/ScanI2PropertyTaker;->mEscanI2LibError:I

    return-object v3

    .line 31
    :cond_0
    :try_start_0
    invoke-virtual {v1, p1}, Lcom/epson/lib/escani2/EscanI2Lib;->setScanner(Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_1

    .line 34
    iput p1, p0, Lcom/epson/lib/escani2/ScanI2PropertyTaker;->mEscanI2LibError:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    invoke-virtual {v1}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    return-object v3

    .line 38
    :cond_1
    :try_start_1
    iget-object p1, v0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Info:Lcom/epson/lib/escani2/ScannerI2Info;

    invoke-virtual {v1, p1}, Lcom/epson/lib/escani2/EscanI2Lib;->getScannerInfo(Lcom/epson/lib/escani2/ScannerI2Info;)I

    move-result p1

    if-eqz p1, :cond_2

    .line 42
    iput p1, p0, Lcom/epson/lib/escani2/ScanI2PropertyTaker;->mEscanI2LibError:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56
    invoke-virtual {v1}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    return-object v3

    .line 46
    :cond_2
    :try_start_2
    iget-object p1, v0, Lcom/epson/lib/escani2/I2LibScannerInfoAndCapability;->scannerI2Capability:Lcom/epson/lib/escani2/ScannerI2Capability;

    invoke-virtual {v1, p1}, Lcom/epson/lib/escani2/EscanI2Lib;->getCapability(Lcom/epson/lib/escani2/ScannerI2Capability;)I

    move-result p1

    if-eqz p1, :cond_3

    .line 50
    iput p1, p0, Lcom/epson/lib/escani2/ScanI2PropertyTaker;->mEscanI2LibError:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 56
    invoke-virtual {v1}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    return-object v3

    :cond_3
    invoke-virtual {v1}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    return-object v0

    :catchall_0
    move-exception p1

    invoke-virtual {v1}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    throw p1

    .line 15
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "ipAddress error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

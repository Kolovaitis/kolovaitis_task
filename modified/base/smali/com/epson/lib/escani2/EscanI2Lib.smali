.class public Lcom/epson/lib/escani2/EscanI2Lib;
.super Ljava/lang/Object;
.source "EscanI2Lib.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/lib/escani2/EscanI2Lib$Observer;,
        Lcom/epson/lib/escani2/EscanI2Lib$PagerGuidPosition;,
        Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;,
        Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;,
        Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;,
        Lcom/epson/lib/escani2/EscanI2Lib$Gamma;,
        Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;,
        Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;,
        Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field public static final ERROR_CODE_COMM_ERROR:I = -0xd6

.field public static final ERROR_CODE_COVER_OPEN:I = -0xd7

.field public static final ERROR_CODE_DOUBLE_FEED:I = -0xd3

.field public static final ERROR_CODE_ESCANI2_ERROR:I = -0x3e8

.field public static final ERROR_CODE_FILE_WRITE_ERROR:I = -0xca

.field public static final ERROR_CODE_GENERAL_ERROR:I = -0xc8

.field public static final ERROR_CODE_INTERNAL_ERROR:I = -0x64

.field public static final ERROR_CODE_JOB_CANCELED:I = 0x1

.field public static final ERROR_CODE_MEMORY_ALLOCATE_ERROR:I = -0xc9

.field public static final ERROR_CODE_NO_ERROR:I = 0x0

.field public static final ERROR_CODE_PAPER_EMPTY:I = -0xd2

.field public static final ERROR_CODE_PAPER_JAM:I = -0xd4

.field public static final ERROR_CODE_SCANNER_OCCUPIED:I = -0xd5

.field private static final LOG_TAG:Ljava/lang/String; = "EscanI2Lib"

.field static sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

.field static sInstance:Lcom/epson/lib/escani2/EscanI2Lib;


# instance fields
.field mDuplex:I

.field private mLibraryInitialized:Z

.field mMaxScanSheets:I

.field mObserver:Lcom/epson/lib/escani2/EscanI2Lib$Observer;

.field final mObserverLock:Ljava/lang/Object;

.field private mScannerLocked:Z

.field private mSetPaperSize:Z

.field private mSetScannerDone:Z

.field volatile mWaitStartRequest:Z

.field marg_jpeg_quality:I

.field marg_over_scan:I

.field marg_threshold:I

.field mautoCtop:I

.field mautoSkew:I

.field mbufferSize:I

.field mcolorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

.field mdoubleFeedLevel:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

.field mheight:I

.field mimageFormat:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

.field minputSource:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

.field mlookupTableNo:I

.field moffsetX:I

.field moffsetY:I

.field mresolutionMain:I

.field mresolutionSub:I

.field muserGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

.field mwidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mObserverLock:Ljava/lang/Object;

    return-void
.end method

.method private checkResolution(II)Z
    .locals 3

    const/4 v0, 0x1

    const/16 v1, 0x12c

    const/4 v2, 0x0

    if-eq p1, v1, :cond_2

    const/16 v1, 0x258

    if-eq p1, v1, :cond_0

    return v2

    :cond_0
    if-eq p2, v1, :cond_1

    return v2

    :cond_1
    return v0

    :cond_2
    if-eq p2, v1, :cond_3

    return v2

    :cond_3
    return v0
.end method

.method public static declared-synchronized getInstance()Lcom/epson/lib/escani2/EscanI2Lib;
    .locals 2

    const-class v0, Lcom/epson/lib/escani2/EscanI2Lib;

    monitor-enter v0

    .line 154
    :try_start_0
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    if-nez v1, :cond_0

    .line 155
    new-instance v1, Lcom/epson/lib/escani2/EscanI2NativeIf;

    invoke-direct {v1}, Lcom/epson/lib/escani2/EscanI2NativeIf;-><init>()V

    sput-object v1, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    .line 158
    :cond_0
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib;->sInstance:Lcom/epson/lib/escani2/EscanI2Lib;

    if-nez v1, :cond_1

    .line 159
    new-instance v1, Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-direct {v1}, Lcom/epson/lib/escani2/EscanI2Lib;-><init>()V

    sput-object v1, Lcom/epson/lib/escani2/EscanI2Lib;->sInstance:Lcom/epson/lib/escani2/EscanI2Lib;

    .line 161
    :cond_1
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib;->sInstance:Lcom/epson/lib/escani2/EscanI2Lib;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized getInstance(Lcom/epson/lib/escani2/EscanI2NativeInterface;)Lcom/epson/lib/escani2/EscanI2Lib;
    .locals 1

    const-class v0, Lcom/epson/lib/escani2/EscanI2Lib;

    monitor-enter v0

    .line 168
    :try_start_0
    sput-object p0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    .line 170
    sget-object p0, Lcom/epson/lib/escani2/EscanI2Lib;->sInstance:Lcom/epson/lib/escani2/EscanI2Lib;

    if-nez p0, :cond_0

    .line 171
    new-instance p0, Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-direct {p0}, Lcom/epson/lib/escani2/EscanI2Lib;-><init>()V

    sput-object p0, Lcom/epson/lib/escani2/EscanI2Lib;->sInstance:Lcom/epson/lib/escani2/EscanI2Lib;

    .line 173
    :cond_0
    sget-object p0, Lcom/epson/lib/escani2/EscanI2Lib;->sInstance:Lcom/epson/lib/escani2/EscanI2Lib;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private isDeviceChige()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public NOT_setScanner(Ljava/lang/String;)I
    .locals 0

    .line 257
    invoke-virtual {p0, p1}, Lcom/epson/lib/escani2/EscanI2Lib;->setScanner(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public calibrateAdf()I
    .locals 2

    .line 649
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->calibrateAdf()I

    move-result v0

    const/4 v1, 0x1

    .line 650
    invoke-virtual {p0, v0, v1}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result v0

    return v0
.end method

.method public cancelScan()V
    .locals 1

    .line 543
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->cancelScanJni()V

    return-void
.end method

.method public cancelWaiting()I
    .locals 1

    .line 316
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->cancelWaitingJni()V

    const/4 v0, 0x0

    return v0
.end method

.method public cleanAdf()I
    .locals 2

    .line 635
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->cleanAdf()I

    move-result v0

    const/4 v1, 0x1

    .line 636
    invoke-virtual {p0, v0, v1}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result v0

    return v0
.end method

.method public freeBlockedCalibrateAdf()I
    .locals 2

    .line 656
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->freeBlockedCalibrateAdf()I

    move-result v0

    const/4 v1, 0x0

    .line 657
    invoke-virtual {p0, v0, v1}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result v0

    return v0
.end method

.method public freeBlockedCleanAdf()I
    .locals 2

    .line 642
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->freeBlockedCleanAdf()I

    move-result v0

    const/4 v1, 0x0

    .line 643
    invoke-virtual {p0, v0, v1}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result v0

    return v0
.end method

.method public getCapability(Lcom/epson/lib/escani2/ScannerI2Capability;)I
    .locals 1

    .line 747
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0, p1}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->getCapability(Lcom/epson/lib/escani2/ScannerI2Capability;)I

    move-result p1

    return p1
.end method

.method public getMaintAppliedParameters([I)I
    .locals 3

    .line 695
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->getMaintAppliedParameters()[I

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/16 v1, -0x64

    goto :goto_0

    .line 700
    :cond_0
    aget v2, v0, v1

    aput v2, p1, v1

    const/4 v2, 0x1

    .line 701
    aget v0, v0, v2

    aput v0, p1, v2

    :goto_0
    return v1
.end method

.method public getMaintCapability(Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .line 716
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->getMaintCapability()[I

    move-result-object v0

    .line 718
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    .line 721
    aget v5, v0, v3

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    const/4 v4, 0x1

    goto :goto_1

    :cond_0
    if-nez v4, :cond_1

    .line 726
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 728
    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    const/16 v2, -0x64

    :cond_3
    return v2
.end method

.method public getScanPages(I)I
    .locals 1

    .line 627
    iget-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mLibraryInitialized:Z

    if-eqz v0, :cond_0

    .line 630
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0, p1}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->getScanPages(I)I

    move-result p1

    return p1

    .line 628
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "initializeLibrary() has not been called."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getScannerInfo(Lcom/epson/lib/escani2/ScannerI2Info;)I
    .locals 1

    .line 742
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0, p1}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->getScannerInfo(Lcom/epson/lib/escani2/ScannerI2Info;)I

    move-result p1

    return p1
.end method

.method public declared-synchronized initializeLibrary()I
    .locals 3

    monitor-enter p0

    .line 185
    :try_start_0
    iget-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mLibraryInitialized:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 190
    iput-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mLibraryInitialized:Z

    .line 191
    iput-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mSetScannerDone:Z

    .line 192
    iput-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mScannerLocked:Z

    .line 193
    iput-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mSetPaperSize:Z

    .line 195
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->initializeLibraryJni()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    .line 197
    iput-boolean v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mLibraryInitialized:Z

    .line 201
    :cond_0
    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    new-instance v2, Lcom/epson/lib/escani2/EscanI2Lib$1;

    invoke-direct {v2, p0}, Lcom/epson/lib/escani2/EscanI2Lib$1;-><init>(Lcom/epson/lib/escani2/EscanI2Lib;)V

    invoke-interface {v1, v2}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->setPageWriteCallback(Lcom/epson/lib/escani2/EscanI2NativeInterface$PageWriteCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    monitor-exit p0

    return v0

    .line 188
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "initializeLibrary() has already called."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized releaseLibrary()I
    .locals 4

    monitor-enter p0

    .line 221
    :try_start_0
    iget-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mLibraryInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 223
    monitor-exit p0

    return v1

    .line 227
    :catch_0
    :cond_0
    :goto_0
    :try_start_1
    iget-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mWaitStartRequest:Z

    if-eqz v0, :cond_1

    .line 228
    invoke-virtual {p0}, Lcom/epson/lib/escani2/EscanI2Lib;->cancelWaiting()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v2, 0x64

    .line 230
    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 237
    :cond_1
    :try_start_3
    invoke-virtual {p0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseScanner()I

    .line 239
    iput-boolean v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mLibraryInitialized:Z

    .line 240
    iput-boolean v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mSetScannerDone:Z

    .line 241
    iput-boolean v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mScannerLocked:Z

    .line 242
    iput-boolean v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mSetPaperSize:Z

    const/4 v0, 0x0

    .line 243
    iput-object v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mObserver:Lcom/epson/lib/escani2/EscanI2Lib$Observer;

    .line 245
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->releaseLibraryJni()I

    move-result v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized releaseScanner()I
    .locals 3

    monitor-enter p0

    .line 515
    :try_start_0
    iget-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mScannerLocked:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 517
    monitor-exit p0

    return v1

    .line 519
    :cond_0
    :try_start_1
    iput-boolean v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mScannerLocked:Z

    .line 520
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->releaseScannerJni()I

    move-result v0

    .line 523
    invoke-virtual {p0, v0, v1}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    .line 531
    :cond_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V
    .locals 1

    .line 611
    iget-object v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mObserverLock:Ljava/lang/Object;

    monitor-enter v0

    .line 612
    :try_start_0
    iput-object p1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mObserver:Lcom/epson/lib/escani2/EscanI2Lib$Observer;

    .line 613
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public declared-synchronized setPaperSize(Lcom/epson/lib/escani2/ScanSize$PaperSize;IIZ)I
    .locals 5

    monitor-enter p0

    .line 351
    :try_start_0
    iget-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mSetScannerDone:Z

    if-eqz v0, :cond_4

    .line 354
    invoke-direct {p0, p2, p3}, Lcom/epson/lib/escani2/EscanI2Lib;->checkResolution(II)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v0, :cond_3

    .line 364
    invoke-static {p1}, Lcom/epson/lib/escani2/ScanSize;->get300DpiPixelFromPaperSize(Lcom/epson/lib/escani2/ScanSize$PaperSize;)[I

    move-result-object p1

    const/16 v0, 0x9f6

    const/16 v4, 0x258

    if-ne p2, v4, :cond_0

    .line 368
    aget v0, p1, v3

    mul-int/lit8 v0, v0, 0x2

    aput v0, p1, v3

    .line 369
    aget v0, p1, v1

    mul-int/lit8 v0, v0, 0x2

    aput v0, p1, v1

    const/16 v0, 0x13ec

    :cond_0
    if-nez p4, :cond_2

    .line 374
    invoke-direct {p0}, Lcom/epson/lib/escani2/EscanI2Lib;->isDeviceChige()Z

    move-result p4

    if-eqz p4, :cond_1

    goto :goto_0

    .line 377
    :cond_1
    aget p4, p1, v3

    sub-int/2addr v0, p4

    div-int/2addr v0, v2

    iput v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->moffsetX:I

    goto :goto_1

    .line 375
    :cond_2
    :goto_0
    iput v3, p0, Lcom/epson/lib/escani2/EscanI2Lib;->moffsetX:I

    .line 379
    :goto_1
    iput v3, p0, Lcom/epson/lib/escani2/EscanI2Lib;->moffsetY:I

    .line 380
    aget p4, p1, v3

    iput p4, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mwidth:I

    .line 381
    aget p1, p1, v1

    iput p1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mheight:I

    .line 383
    iput p2, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mresolutionMain:I

    .line 384
    iput p3, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mresolutionSub:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    monitor-exit p0

    return v3

    .line 355
    :cond_3
    :try_start_1
    new-instance p1, Ljava/lang/RuntimeException;

    sget-object p4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v0, "%dx%d dpi is not supported."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v1

    invoke-static {p4, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 352
    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "setScanner() has not been called."

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setPowerOffTime(I)I
    .locals 1

    .line 680
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0, p1}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->setPowerOffTime(I)I

    move-result p1

    const/4 v0, 0x0

    .line 681
    invoke-virtual {p0, p1, v0}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result p1

    return p1
.end method

.method public setPowerSaveTime(I)I
    .locals 1

    .line 668
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0, p1}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->setPowerSaveTime(I)I

    move-result p1

    const/4 v0, 0x0

    .line 669
    invoke-virtual {p0, p1, v0}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result p1

    return p1
.end method

.method public declared-synchronized setScanner(Ljava/lang/String;)I
    .locals 1

    monitor-enter p0

    .line 277
    :try_start_0
    iget-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mLibraryInitialized:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 280
    iput-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mSetScannerDone:Z

    .line 281
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0, p1}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->setScannerJni(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x0

    .line 282
    invoke-virtual {p0, p1, v0}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    .line 278
    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "initializeLibrary() has not been called."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized startScan(Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;Lcom/epson/lib/escani2/EscanI2Lib$Gamma;ILcom/epson/lib/escani2/EscanI2Lib$InputUnit;IIZZLcom/epson/lib/escani2/EscanI2Lib$ImageFormat;IILcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;)I
    .locals 25

    move-object/from16 v1, p0

    monitor-enter p0

    .line 439
    :try_start_0
    iget-boolean v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mLibraryInitialized:Z

    if-eqz v0, :cond_3

    .line 444
    invoke-virtual/range {p1 .. p1}, Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;->ordinal()I

    move-result v0

    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mautoCtop:I

    .line 445
    invoke-virtual/range {p2 .. p2}, Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;->ordinal()I

    move-result v0

    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mautoSkew:I

    move-object/from16 v0, p3

    .line 446
    iput-object v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mcolorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    move-object/from16 v0, p4

    .line 447
    iput-object v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->muserGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    move/from16 v0, p5

    .line 448
    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mlookupTableNo:I

    move-object/from16 v0, p6

    .line 449
    iput-object v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->minputSource:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    move/from16 v0, p7

    .line 450
    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mMaxScanSheets:I

    const/4 v0, 0x0

    .line 451
    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mDuplex:I

    const/4 v2, 0x1

    if-eqz p9, :cond_0

    .line 453
    iput v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mDuplex:I

    .line 455
    :cond_0
    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->marg_over_scan:I

    if-eqz p10, :cond_1

    .line 456
    invoke-direct/range {p0 .. p0}, Lcom/epson/lib/escani2/EscanI2Lib;->isDeviceChige()Z

    move-result v0

    if-nez v0, :cond_1

    .line 457
    iput v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->marg_over_scan:I

    move-object/from16 v0, p11

    goto :goto_0

    :cond_1
    move-object/from16 v0, p11

    .line 459
    :goto_0
    iput-object v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mimageFormat:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    move/from16 v0, p12

    .line 460
    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->marg_jpeg_quality:I

    move/from16 v0, p13

    .line 461
    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->marg_threshold:I

    move-object/from16 v0, p14

    .line 462
    iput-object v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mdoubleFeedLevel:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    const/high16 v0, 0x10000

    .line 464
    iput v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mbufferSize:I

    .line 467
    iput-boolean v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mScannerLocked:Z

    .line 470
    iget v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mMaxScanSheets:I

    .line 471
    iget v3, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mDuplex:I

    if-lez v3, :cond_2

    .line 472
    iget v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mMaxScanSheets:I

    mul-int/lit8 v0, v0, 0x2

    move/from16 v16, v0

    goto :goto_1

    :cond_2
    move/from16 v16, v0

    .line 475
    :goto_1
    sget-object v3, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    iget v4, v1, Lcom/epson/lib/escani2/EscanI2Lib;->moffsetX:I

    iget v5, v1, Lcom/epson/lib/escani2/EscanI2Lib;->moffsetY:I

    iget v6, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mwidth:I

    iget v7, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mheight:I

    iget v8, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mautoCtop:I

    iget v9, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mautoSkew:I

    iget v10, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mresolutionMain:I

    iget v11, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mresolutionSub:I

    iget-object v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mcolorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    .line 477
    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->ordinal()I

    move-result v12

    iget-object v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->muserGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->ordinal()I

    move-result v13

    iget v14, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mlookupTableNo:I

    iget-object v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->minputSource:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result v15

    iget v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mDuplex:I

    iget v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->marg_over_scan:I

    move/from16 v19, v2

    iget-object v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mimageFormat:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    .line 479
    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->ordinal()I

    move-result v20

    iget v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->marg_jpeg_quality:I

    move/from16 v21, v2

    iget v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->marg_threshold:I

    move/from16 v22, v2

    iget-object v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mdoubleFeedLevel:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    .line 480
    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->getValue()I

    move-result v23

    iget v2, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mbufferSize:I

    move/from16 v17, p8

    move/from16 v18, v0

    move/from16 v24, v2

    .line 475
    invoke-interface/range {v3 .. v24}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->startScan(IIIIIIIIIIIIIIIIIIIII)I

    move-result v0

    const/4 v2, 0x1

    .line 482
    invoke-virtual {v1, v0, v2}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 440
    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "initializeLibrary() has not been called."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startScan(Lcom/epson/lib/escani2/ScanI2Params;IIIILcom/epson/lib/escani2/EscanI2Lib$ImageFormat;III)I
    .locals 24

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    monitor-enter p0

    .line 491
    :try_start_0
    sget-object v2, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    iget-object v3, v0, Lcom/epson/lib/escani2/ScanI2Params;->autoCrop:Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;

    .line 492
    invoke-virtual {v3}, Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;->ordinal()I

    move-result v7

    iget-object v3, v0, Lcom/epson/lib/escani2/ScanI2Params;->autoSkew:Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;

    invoke-virtual {v3}, Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;->ordinal()I

    move-result v8

    iget v9, v0, Lcom/epson/lib/escani2/ScanI2Params;->resolutionMain:I

    iget v10, v0, Lcom/epson/lib/escani2/ScanI2Params;->resolutionSub:I

    iget-object v3, v0, Lcom/epson/lib/escani2/ScanI2Params;->colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

    .line 494
    invoke-virtual {v3}, Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;->ordinal()I

    move-result v11

    iget-object v3, v0, Lcom/epson/lib/escani2/ScanI2Params;->userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;

    invoke-virtual {v3}, Lcom/epson/lib/escani2/EscanI2Lib$Gamma;->ordinal()I

    move-result v12

    iget v13, v0, Lcom/epson/lib/escani2/ScanI2Params;->lookupTableNo:I

    iget-object v3, v0, Lcom/epson/lib/escani2/ScanI2Params;->inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    .line 495
    invoke-virtual {v3}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ordinal()I

    move-result v14

    iget v15, v0, Lcom/epson/lib/escani2/ScanI2Params;->maxScanSheets:I

    iget v6, v0, Lcom/epson/lib/escani2/ScanI2Params;->maxWriteSheets:I

    iget-boolean v3, v0, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_0

    const/16 v17, 0x1

    goto :goto_0

    :cond_0
    const/16 v17, 0x0

    :goto_0
    iget-boolean v3, v0, Lcom/epson/lib/escani2/ScanI2Params;->overScan:Z

    if-eqz v3, :cond_1

    const/16 v18, 0x1

    goto :goto_1

    :cond_1
    const/16 v18, 0x0

    .line 497
    :goto_1
    invoke-virtual/range {p6 .. p6}, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->ordinal()I

    move-result v19

    iget-object v0, v0, Lcom/epson/lib/escani2/ScanI2Params;->doubleFeedLevel:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

    .line 498
    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;->getValue()I

    move-result v22

    move/from16 v3, p4

    move/from16 v4, p5

    const/4 v0, 0x1

    move/from16 v5, p2

    move/from16 v16, v6

    move/from16 v6, p3

    move/from16 v20, p7

    move/from16 v21, p8

    move/from16 v23, p9

    .line 491
    invoke-interface/range {v2 .. v23}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->startScan(IIIIIIIIIIIIIIIIIIIII)I

    move-result v2

    .line 502
    iput-boolean v0, v1, Lcom/epson/lib/escani2/EscanI2Lib;->mScannerLocked:Z

    .line 504
    invoke-virtual {v1, v2, v0}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method trunsErrorCode(IZ)I
    .locals 2

    const/16 v0, -0x138f

    if-eq p1, v0, :cond_4

    const/16 v0, -0x5dd

    if-eq p1, v0, :cond_3

    const/16 v0, -0x45a

    const/16 v1, -0xd6

    if-eq p1, v0, :cond_2

    const/16 v0, -0x44c

    if-eq p1, v0, :cond_1

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    return p1

    :pswitch_0
    const/16 p1, -0xc9

    return p1

    :pswitch_1
    const/16 p1, -0xca

    return p1

    :pswitch_2
    const/16 p1, -0xd7

    return p1

    :pswitch_3
    const/16 p1, -0xd4

    return p1

    :pswitch_4
    if-eqz p2, :cond_0

    const/16 p1, -0xd2

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    return v1

    :cond_2
    return v1

    :cond_3
    const/16 p1, -0xd5

    return p1

    :cond_4
    const/16 p1, -0xd3

    return p1

    :pswitch_data_0
    .packed-switch -0x138b
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x66
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method updatePages(II)V
    .locals 2

    .line 850
    iget-object v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mObserverLock:Ljava/lang/Object;

    monitor-enter v0

    .line 851
    :try_start_0
    iget-object v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mObserver:Lcom/epson/lib/escani2/EscanI2Lib$Observer;

    if-eqz v1, :cond_0

    .line 852
    iget-object v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mObserver:Lcom/epson/lib/escani2/EscanI2Lib$Observer;

    invoke-interface {v1, p1, p2}, Lcom/epson/lib/escani2/EscanI2Lib$Observer;->update(II)V

    .line 854
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public waitStartRequest()I
    .locals 2

    const/4 v0, 0x1

    .line 297
    iput-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mWaitStartRequest:Z

    .line 298
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->waitStartRequest()I

    move-result v0

    const/4 v1, 0x0

    .line 299
    iput-boolean v1, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mWaitStartRequest:Z

    .line 300
    invoke-virtual {p0, v0, v1}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result v0

    return v0
.end method

.method public writeScanImage(ILjava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 566
    iget-boolean v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mScannerLocked:Z

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    .line 573
    iget v0, p0, Lcom/epson/lib/escani2/EscanI2Lib;->mDuplex:I

    if-lez v0, :cond_1

    if-eqz p3, :cond_0

    goto :goto_0

    .line 575
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "outfile_fmt_b must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 579
    :cond_1
    :goto_0
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib;->sEscanI2NativeInterface:Lcom/epson/lib/escani2/EscanI2NativeInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/epson/lib/escani2/EscanI2NativeInterface;->scan(ILjava/lang/String;Ljava/lang/String;)I

    move-result p1

    const/4 p2, 0x0

    .line 580
    invoke-virtual {p0, p1, p2}, Lcom/epson/lib/escani2/EscanI2Lib;->trunsErrorCode(IZ)I

    move-result p1

    return p1

    .line 570
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "outfile_fmt_a must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 567
    :cond_3
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "startScan() has not been called"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

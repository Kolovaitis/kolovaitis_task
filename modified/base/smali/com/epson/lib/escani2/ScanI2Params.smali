.class public Lcom/epson/lib/escani2/ScanI2Params;
.super Ljava/lang/Object;
.source "ScanI2Params.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# instance fields
.field public autoCrop:Lcom/epson/lib/escani2/EscanI2Lib$AutoCrop;

.field public autoSkew:Lcom/epson/lib/escani2/EscanI2Lib$AutoSkew;

.field public colorMode:Lcom/epson/lib/escani2/EscanI2Lib$ColorMode;

.field public density:I

.field public densityChangeable:Z

.field public doubleFeedLevel:Lcom/epson/lib/escani2/EscanI2Lib$DoubleFeedLevel;

.field public duplex:Z

.field public duplexTurnDirection:I

.field public inputUnit:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

.field public lookupTableNo:I

.field public manualAdfAlignment:I

.field public maxScanSheets:I

.field public maxWriteSheets:I

.field public overScan:Z

.field public qualityHW:I

.field public qualitySW:I

.field public resolutionMain:I

.field public resolutionSub:I

.field public scanSize:Lcom/epson/lib/escani2/ScanSize;

.field public userGamma:Lcom/epson/lib/escani2/EscanI2Lib$Gamma;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clone()Lcom/epson/lib/escani2/ScanI2Params;
    .locals 2

    .line 66
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/lib/escani2/ScanI2Params;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 9
    invoke-virtual {p0}, Lcom/epson/lib/escani2/ScanI2Params;->clone()Lcom/epson/lib/escani2/ScanI2Params;

    move-result-object v0

    return-object v0
.end method

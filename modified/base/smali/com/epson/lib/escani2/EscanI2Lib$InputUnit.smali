.class public final enum Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;
.super Ljava/lang/Enum;
.source "EscanI2Lib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/lib/escani2/EscanI2Lib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InputUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

.field public static final enum ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

.field public static final enum FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

.field public static final enum TRANSPARENT_UNIT:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 104
    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    const-string v1, "FLATBED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    const-string v1, "ADF"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    const-string v1, "TRANSPARENT_UNIT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->TRANSPARENT_UNIT:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    const/4 v0, 0x3

    .line 100
    new-array v0, v0, [Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->FLATBED:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->ADF:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->TRANSPARENT_UNIT:Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    aput-object v1, v0, v4

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;
    .locals 1

    .line 100
    const-class v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    return-object p0
.end method

.method public static values()[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;
    .locals 1

    .line 100
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    invoke-virtual {v0}, [Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/EscanI2Lib$InputUnit;

    return-object v0
.end method

.class public Lcom/epson/lib/escani2/ScanI2Runner;
.super Ljava/lang/Object;
.source "ScanI2Runner.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ScanI2Runner"


# instance fields
.field private volatile mCancelScan:Z

.field private mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

.field private mScanPages:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 13
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/lib/escani2/ScanI2Runner;->mScanPages:[I

    .line 45
    invoke-static {}, Lcom/epson/lib/escani2/EscanI2Lib;->getInstance()Lcom/epson/lib/escani2/EscanI2Lib;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    const/4 v0, 0x0

    .line 46
    iput-boolean v0, p0, Lcom/epson/lib/escani2/ScanI2Runner;->mCancelScan:Z

    return-void
.end method

.method public constructor <init>(Lcom/epson/lib/escani2/EscanI2Lib;)V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x2

    .line 13
    new-array p1, p1, [I

    iput-object p1, p0, Lcom/epson/lib/escani2/ScanI2Runner;->mScanPages:[I

    .line 54
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "ScanI2Runner(EscanI2Lib) is test only."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private setCancelScan(Z)V
    .locals 0

    .line 38
    iput-boolean p1, p0, Lcom/epson/lib/escani2/ScanI2Runner;->mCancelScan:Z

    return-void
.end method


# virtual methods
.method public cancelScan()V
    .locals 1

    const/4 v0, 0x1

    .line 68
    invoke-direct {p0, v0}, Lcom/epson/lib/escani2/ScanI2Runner;->setCancelScan(Z)V

    .line 70
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->cancelScan()V

    :cond_0
    return-void
.end method

.method public doScan(Ljava/lang/String;Lcom/epson/lib/escani2/ScanI2Params;[Ljava/lang/String;Lcom/epson/lib/escani2/EscanI2Lib$Observer;)I
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v12, p2

    if-eqz v0, :cond_d

    .line 96
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto/16 :goto_4

    :cond_0
    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 103
    :try_start_0
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mScanPages:[I

    aput v15, v2, v15

    .line 104
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mScanPages:[I

    aput v15, v2, v13

    .line 105
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->initializeLibrary()I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_1

    .line 212
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v2

    .line 113
    :cond_1
    :try_start_1
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2, v0}, Lcom/epson/lib/escani2/EscanI2Lib;->setScanner(Ljava/lang/String;)I

    move-result v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_2

    .line 212
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v0

    .line 118
    :cond_2
    :try_start_2
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    move-object/from16 v2, p4

    invoke-virtual {v0, v2}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    .line 121
    sget-object v8, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->JPG:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    .line 123
    iget v0, v12, Lcom/epson/lib/escani2/ScanI2Params;->qualityHW:I

    if-nez v0, :cond_3

    const/16 v0, 0x5a

    const/16 v9, 0x5a

    goto :goto_0

    :cond_3
    iget v0, v12, Lcom/epson/lib/escani2/ScanI2Params;->qualityHW:I

    move v9, v0

    :goto_0
    const/16 v10, 0x80

    const/high16 v11, 0x10000

    .line 130
    invoke-virtual/range {p0 .. p0}, Lcom/epson/lib/escani2/ScanI2Runner;->isCancelScan()Z

    move-result v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v0, :cond_4

    .line 212
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v15

    .line 134
    :cond_4
    :try_start_3
    new-instance v0, Lcom/epson/lib/escani2/ScannerI2Info;

    invoke-direct {v0}, Lcom/epson/lib/escani2/ScannerI2Info;-><init>()V

    .line 135
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2, v0}, Lcom/epson/lib/escani2/EscanI2Lib;->getScannerInfo(Lcom/epson/lib/escani2/ScannerI2Info;)I

    move-result v2
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_5

    .line 212
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v2

    .line 141
    :cond_5
    :try_start_4
    iget-object v2, v12, Lcom/epson/lib/escani2/ScanI2Params;->scanSize:Lcom/epson/lib/escani2/ScanSize;

    .line 142
    invoke-virtual {v2, v0, v12}, Lcom/epson/lib/escani2/ScanSize;->getValidScanSize(Lcom/epson/lib/escani2/ScannerI2Info;Lcom/epson/lib/escani2/ScanI2Params;)[I

    move-result-object v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-nez v0, :cond_6

    .line 212
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v15

    .line 148
    :cond_6
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/epson/lib/escani2/ScanI2Runner;->isCancelScan()Z

    move-result v2
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v2, :cond_7

    .line 212
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v15

    .line 157
    :cond_7
    :try_start_6
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    aget v4, v0, v15

    aget v5, v0, v13

    const/4 v3, 0x2

    aget v6, v0, v3

    const/4 v3, 0x3

    aget v7, v0, v3

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v11}, Lcom/epson/lib/escani2/EscanI2Lib;->startScan(Lcom/epson/lib/escani2/ScanI2Params;IIIILcom/epson/lib/escani2/EscanI2Lib$ImageFormat;III)I

    move-result v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_8

    .line 206
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseScanner()I

    .line 212
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v0

    .line 166
    :cond_8
    :try_start_7
    aget-object v0, p3, v15

    .line 169
    iget-boolean v2, v12, Lcom/epson/lib/escani2/ScanI2Params;->duplex:Z

    if-eqz v2, :cond_9

    .line 170
    aget-object v2, p3, v13

    goto :goto_1

    :cond_9
    move-object v2, v14

    .line 181
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/epson/lib/escani2/ScanI2Runner;->isCancelScan()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 182
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseScanner()I
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 206
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseScanner()I

    .line 212
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v0, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v0, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v15

    .line 188
    :cond_a
    :try_start_8
    iget-object v3, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v3, v15, v0, v2}, Lcom/epson/lib/escani2/EscanI2Lib;->writeScanImage(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 192
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mScanPages:[I

    iget-object v3, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v3, v15}, Lcom/epson/lib/escani2/EscanI2Lib;->getScanPages(I)I

    move-result v3

    aput v3, v2, v15

    .line 193
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mScanPages:[I

    iget-object v3, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v3, v13}, Lcom/epson/lib/escani2/EscanI2Lib;->getScanPages(I)I

    move-result v3

    aput v3, v2, v13
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 206
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseScanner()I

    .line 212
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v0

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    nop

    goto :goto_3

    :catchall_1
    move-exception v0

    const/4 v13, 0x0

    :goto_2
    if-eqz v13, :cond_b

    .line 206
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseScanner()I

    .line 212
    :cond_b
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    throw v0

    :catch_1
    const/4 v13, 0x0

    :goto_3
    const/16 v0, -0xc8

    if-eqz v13, :cond_c

    .line 206
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseScanner()I

    .line 212
    :cond_c
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2}, Lcom/epson/lib/escani2/EscanI2Lib;->releaseLibrary()I

    .line 213
    iget-object v2, v1, Lcom/epson/lib/escani2/ScanI2Runner;->mEscanI2Lib:Lcom/epson/lib/escani2/EscanI2Lib;

    invoke-virtual {v2, v14}, Lcom/epson/lib/escani2/EscanI2Lib;->setObserver(Lcom/epson/lib/escani2/EscanI2Lib$Observer;)V

    return v0

    :cond_d
    :goto_4
    const/16 v0, -0xd6

    return v0
.end method

.method public getScanPages()[I
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/epson/lib/escani2/ScanI2Runner;->mScanPages:[I

    return-object v0
.end method

.method public isCancelScan()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/epson/lib/escani2/ScanI2Runner;->mCancelScan:Z

    return v0
.end method

.class public final enum Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;
.super Ljava/lang/Enum;
.source "EscanI2Lib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/lib/escani2/EscanI2Lib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ImageFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

.field public static final enum JPG:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

.field public static final enum RAW:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 114
    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    const-string v1, "JPG"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->JPG:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    new-instance v0, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    const-string v1, "RAW"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->RAW:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    const/4 v0, 0x2

    .line 111
    new-array v0, v0, [Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->JPG:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->RAW:Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    aput-object v1, v0, v3

    sput-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;
    .locals 1

    .line 111
    const-class v0, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    return-object p0
.end method

.method public static values()[Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;
    .locals 1

    .line 111
    sget-object v0, Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->$VALUES:[Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    invoke-virtual {v0}, [Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/lib/escani2/EscanI2Lib$ImageFormat;

    return-object v0
.end method

.class public Lcom/epson/sd/common/apf/apflib;
.super Ljava/lang/Object;
.source "apflib.java"


# static fields
.field public static final EPS_APF_INCREMENT_OF_CORRECTION_VALUE:I = 0xa

.field public static final EPS_APF_SHRPNESS_OPTIMUN_VALUE:I = 0x1e

.field private static final EPS_CM_COLOR:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "apf"

    .line 18
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native autoCorrectFile(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZ)I
.end method

.method private native autoCorrectFileWithValue(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZIII)I
.end method

.method private static isEmpty(Ljava/lang/String;)Z
    .locals 0

    if-eqz p0, :cond_1

    .line 227
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method private native setInterruptionFlg()V
.end method


# virtual methods
.method public apfFile(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z
    .locals 10

    .line 57
    invoke-static {p3}, Lcom/epson/sd/common/apf/apflib;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    invoke-static {p4}, Lcom/epson/sd/common/apf/apflib;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x8

    if-le p2, v0, :cond_0

    goto :goto_1

    :cond_0
    if-gez p2, :cond_1

    .line 64
    invoke-virtual {p0, p1}, Lcom/epson/sd/common/apf/apflib;->exifOrientation(Ljava/lang/String;)I

    move-result p2

    move v4, p2

    goto :goto_0

    :cond_1
    move v4, p2

    :goto_0
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v5, p3

    move-object v6, p4

    .line 67
    invoke-direct/range {v2 .. v9}, Lcom/epson/sd/common/apf/apflib;->autoCorrectFile(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZ)I

    move-result p1

    if-nez p1, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1

    :cond_3
    :goto_1
    return v1
.end method

.method public apfFile(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZIII)Z
    .locals 11

    move v0, p2

    move/from16 v7, p7

    .line 139
    invoke-static {p3}, Lcom/epson/sd/common/apf/apflib;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v10, 0x0

    if-nez v1, :cond_4

    invoke-static {p4}, Lcom/epson/sd/common/apf/apflib;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const/16 v1, 0x8

    if-gt v0, v1, :cond_4

    if-nez p5, :cond_0

    if-eqz p6, :cond_4

    :cond_0
    if-eqz p6, :cond_1

    const/16 v1, 0x7f

    if-gt v7, v1, :cond_4

    const/16 v1, -0x80

    if-ge v7, v1, :cond_1

    goto :goto_1

    :cond_1
    if-gez v0, :cond_2

    .line 147
    invoke-virtual {p0, p1}, Lcom/epson/sd/common/apf/apflib;->exifOrientation(Ljava/lang/String;)I

    move-result v0

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    .line 150
    invoke-direct/range {v0 .. v9}, Lcom/epson/sd/common/apf/apflib;->autoCorrectFileWithValue(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZIII)I

    move-result v0

    if-nez v0, :cond_3

    const/4 v10, 0x1

    :cond_3
    return v10

    :cond_4
    :goto_1
    return v10
.end method

.method public apfFile(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZ)Z
    .locals 11

    move v0, p2

    .line 96
    invoke-static {p3}, Lcom/epson/sd/common/apf/apflib;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_3

    invoke-static {p4}, Lcom/epson/sd/common/apf/apflib;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v1, 0x8

    if-gt v0, v1, :cond_3

    if-nez p5, :cond_0

    if-nez p6, :cond_0

    goto :goto_1

    :cond_0
    if-gez v0, :cond_1

    .line 103
    invoke-virtual {p0, p1}, Lcom/epson/sd/common/apf/apflib;->exifOrientation(Ljava/lang/String;)I

    move-result v0

    move v5, v0

    goto :goto_0

    :cond_1
    move v5, v0

    :goto_0
    move-object v3, p0

    move-object v4, p1

    move-object v6, p3

    move-object v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    .line 106
    invoke-direct/range {v3 .. v10}, Lcom/epson/sd/common/apf/apflib;->autoCorrectFile(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZ)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v2, 0x1

    :cond_2
    return v2

    :cond_3
    :goto_1
    return v2
.end method

.method public exifOrientation(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    .line 167
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string p1, "Orientation"

    .line 168
    invoke-virtual {v1, p1, v0}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return v0
.end method

.method public interrupt()V
    .locals 0

    .line 182
    invoke-direct {p0}, Lcom/epson/sd/common/apf/apflib;->setInterruptionFlg()V

    return-void
.end method

.method public isDspEpsonColorLogo(Ljava/lang/String;IIZ)Z
    .locals 1

    const/4 v0, 0x0

    if-nez p4, :cond_0

    return v0

    :cond_0
    if-eqz p3, :cond_1

    return v0

    .line 207
    :cond_1
    invoke-static {}, Lcom/epson/sd/common/apf/EpsonColorLogoList;->getMediatypeidlist()[Ljava/lang/Integer;

    move-result-object p3

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    return v0

    .line 212
    :cond_2
    invoke-static {}, Lcom/epson/sd/common/apf/EpsonColorLogoList;->getDeviceIDList()[Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    return v0

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

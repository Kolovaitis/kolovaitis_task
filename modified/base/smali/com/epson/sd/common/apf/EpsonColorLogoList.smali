.class public Lcom/epson/sd/common/apf/EpsonColorLogoList;
.super Ljava/lang/Object;
.source "EpsonColorLogoList.java"


# static fields
.field private static final DeviceIDList:[Ljava/lang/String;

.field private static final MediaTypeIDList:[Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 100

    const-string v0, "PM-A950"

    const-string v1, "PM-A890"

    const-string v2, "PM-D800"

    const-string v3, "PM-G730"

    const-string v4, "E-150"

    const-string v5, "PX-G930"

    const-string v6, "PM-T990"

    const-string v7, "PM-A970"

    const-string v8, "PM-A920"

    const-string v9, "PM-A820"

    const-string v10, "PM-D870"

    const-string v11, "E-700"

    const-string v12, "E-500"

    const-string v13, "E-300"

    const-string v14, "PM-G850"

    const-string v15, "PM-G4500"

    const-string v16, "PX-G5100"

    const-string v17, "PM-T960"

    const-string v18, "PM-A940"

    const-string v19, "PM-A840"

    const-string v20, "PM-G860"

    const-string v21, "E-720"

    const-string v22, "E-520"

    const-string v23, "PX-G5300"

    const-string v24, "EP-901F"

    const-string v25, "EP-901A"

    const-string v26, "EP-801A"

    const-string v27, "PM-A840S"

    const-string v28, "EP-301"

    const-string v29, "E-530"

    const-string v30, "E-330"

    const-string v31, "EP-902A"

    const-string v32, "EP-802A"

    const-string v33, "EP-702A"

    const-string v34, "EP-302"

    const-string v35, "E-800"

    const-string v36, "E-600"

    const-string v37, "E-330S"

    const-string v38, "EP-903F"

    const-string v39, "EP-903A"

    const-string v40, "EP-803A"

    const-string v41, "EP-803AW"

    const-string v42, "EP-703A"

    const-string v43, "E-810"

    const-string v44, "E-340"

    const-string v45, "EP-904F"

    const-string v46, "EP-904A"

    const-string v47, "EP-804A"

    const-string v48, "EP-774A"

    const-string v49, "EP-704A"

    const-string v50, "E-820"

    const-string v51, "E-350"

    const-string v52, "PX-7V"

    const-string v53, "EP-4004"

    const-string v54, "EP-905F Series"

    const-string v55, "EP-905A Series"

    const-string v56, "EP-805A Series"

    const-string v57, "EP-775A Series"

    const-string v58, "E-830 Series"

    const-string v59, "E-360 Series"

    const-string v60, "EP-976A3 Series"

    const-string v61, "EP-906F Series"

    const-string v62, "EP-806A Series"

    const-string v63, "EP-776A Series"

    const-string v64, "EP-706A Series"

    const-string v65, "EP-306 Series"

    const-string v66, "E-840 Series"

    const-string v67, "E-370 Series"

    const-string v68, "EP-977A3 Series"

    const-string v69, "EP-907F Series"

    const-string v70, "EP-807A Series"

    const-string v71, "EP-777A Series"

    const-string v72, "EP-707A Series"

    const-string v73, "E-850 Series"

    const-string v74, "EP-978A3 Series"

    const-string v75, "EP-808A Series"

    const-string v76, "EP-708A Series"

    const-string v77, "EP-10VA Series"

    const-string v78, "PF-71 Series"

    const-string v79, "PF-81 Series"

    const-string v80, "SC-PX7V2"

    const-string v81, "EP-979A3 Series"

    const-string v82, "EP-879A Series"

    const-string v83, "EP-709A Series"

    const-string v84, "EP-30VA Series"

    const-string v85, "EW-M770T Series"

    const-string v86, "EW-M970A3T Series"

    const-string v87, "EP-880A Series"

    const-string v88, "EP-50V Series"

    const-string v89, "EP-710A Series"

    const-string v90, "EP-810A Series"

    const-string v91, "EP-881A Series"

    const-string v92, "EP-711A Series"

    const-string v93, "EP-811A Series"

    const-string v94, "EP-882A Series"

    const-string v95, "EP-712A Series"

    const-string v96, "EP-812A Series"

    const-string v97, "EP-982A3 Series"

    const-string v98, "EW-M752T Series"

    const-string v99, "EP-M552T Series"

    .line 12
    filled-new-array/range {v0 .. v99}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/sd/common/apf/EpsonColorLogoList;->DeviceIDList:[Ljava/lang/String;

    const/16 v0, 0x11

    .line 116
    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x5

    .line 117
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    const/4 v2, 0x6

    .line 118
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v0, v4

    const/16 v3, 0x8

    .line 119
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x2

    aput-object v4, v0, v5

    const/16 v4, 0xb

    .line 120
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x3

    aput-object v5, v0, v6

    const/16 v5, 0xc

    .line 121
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x4

    aput-object v6, v0, v7

    const/16 v6, 0xd

    .line 122
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v0, v1

    const/16 v1, 0xf

    .line 123
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v0, v2

    const/16 v2, 0x10

    .line 124
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x7

    aput-object v7, v0, v8

    const/16 v7, 0x18

    .line 125
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v0, v3

    const/16 v3, 0x19

    .line 126
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v7, 0x9

    aput-object v3, v0, v7

    const/16 v3, 0x26

    .line 127
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v7, 0xa

    aput-object v3, v0, v7

    const/16 v3, 0x27

    .line 128
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v4

    const/16 v3, 0x29

    .line 129
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v5

    const/16 v3, 0x2b

    .line 130
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v6

    const/16 v3, 0x2c

    .line 131
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0xe

    aput-object v3, v0, v4

    const/16 v3, 0x34

    .line 132
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x3c

    .line 133
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/epson/sd/common/apf/EpsonColorLogoList;->MediaTypeIDList:[Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDeviceIDList()[Ljava/lang/String;
    .locals 1

    .line 141
    sget-object v0, Lcom/epson/sd/common/apf/EpsonColorLogoList;->DeviceIDList:[Ljava/lang/String;

    return-object v0
.end method

.method public static getMediatypeidlist()[Ljava/lang/Integer;
    .locals 1

    .line 137
    sget-object v0, Lcom/epson/sd/common/apf/EpsonColorLogoList;->MediaTypeIDList:[Ljava/lang/Integer;

    return-object v0
.end method

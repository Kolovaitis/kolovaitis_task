.class public Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;
.super Ljava/lang/Object;
.source "NfcTagParser.java"


# static fields
.field public static final FLAG_DISABLE_WIFI:I = 0x1000000

.field public static final FLAG_INFRASTRUCTURE:I = 0x2

.field public static final FLAG_LOCKED_BY_ADMIN:I = 0x10000

.field public static final FLAG_MAX_CONNECTED:I = 0x100

.field public static final FLAG_POWER_STATUS:I = 0x1

.field public static final FLAG_SIMPLEAP_STATUS:I = 0x4

.field public static final FLAG_SIMPLEAP_STATUS_5G:I = 0x10

.field private static final INVALID_IPADDRESSV4:Ljava/lang/String; = "0.0.0.0"

.field private static final LENGTH_IPADDRESSV4:I = 0x4

.field private static final LENGTH_MACADDRESS:I = 0x6

.field private static final LENGTH_TAGDATA:I = 0x122

.field private static final OFFSET_DISABLE_WIFI:I = 0x85

.field private static final OFFSET_INFRASTRUCTURE:I = 0x82

.field private static final OFFSET_IPADDRESSV4:I = 0x92

.field private static final OFFSET_IPADDRESSV4_SIMPLEAP:I = 0x102

.field private static final OFFSET_LOCKED_BY_ADMIN:I = 0x84

.field private static final OFFSET_MACADDRESS:I = 0x73

.field private static final OFFSET_MAX_CONNECTED:I = 0x44

.field private static final OFFSET_MAX_CONNECT_SSID_LENGTH:I = 0x42

.field private static final OFFSET_PASSWORD:I = 0x2

.field private static final OFFSET_PASSWORD_LENGTH:I = 0x0

.field private static final OFFSET_POWER_STATUS:I = 0x72

.field private static final OFFSET_SIMPLEAP_STATUS:I = 0x83

.field private static final OFFSET_SSID:I = 0x45


# instance fields
.field protected tagData:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 48
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    return-void
.end method


# virtual methods
.method formatIPAddressV4([B)Ljava/lang/String;
    .locals 3

    .line 145
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    .line 148
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 149
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    goto :goto_1

    :cond_0
    const-string v2, "."

    .line 153
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    const-string p1, "0.0.0.0"

    .line 157
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 161
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getIPAddressV4()Ljava/lang/String;
    .locals 3

    .line 123
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v1, 0x92

    const/16 v2, 0x96

    invoke-static {v0, v1, v2}, Lorg/apache/commons/lang/ArrayUtils;->subarray([BII)[B

    move-result-object v0

    .line 124
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->formatIPAddressV4([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIPAddressV4SimpleAP()Ljava/lang/String;
    .locals 3

    .line 133
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v1, 0x102

    const/16 v2, 0x106

    invoke-static {v0, v1, v2}, Lorg/apache/commons/lang/ArrayUtils;->subarray([BII)[B

    move-result-object v0

    .line 134
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->formatIPAddressV4([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v1, 0x73

    const/16 v2, 0x79

    invoke-static {v0, v1, v2}, Lorg/apache/commons/lang/ArrayUtils;->subarray([BII)[B

    move-result-object v0

    .line 76
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPassWord()Ljava/lang/String;
    .locals 4

    .line 103
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    const/4 v3, 0x2

    add-int/2addr v2, v3

    .line 105
    invoke-static {v0, v3, v2}, Lorg/apache/commons/lang/ArrayUtils;->subarray([BII)[B

    move-result-object v0

    .line 108
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 109
    aget-byte v2, v0, v1

    not-int v2, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 112
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1
.end method

.method getPrinterStatus()I
    .locals 4

    .line 173
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v1, 0x72

    aget-byte v0, v0, v1

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 178
    :goto_0
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v3, 0x82

    aget-byte v2, v2, v3

    if-eqz v2, :cond_1

    or-int/lit8 v0, v0, 0x2

    .line 183
    :cond_1
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v3, 0x83

    aget-byte v2, v2, v3

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x4

    .line 187
    :cond_2
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    aget-byte v1, v1, v3

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    or-int/lit8 v0, v0, 0x10

    .line 192
    :cond_3
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v2, 0x44

    aget-byte v1, v1, v2

    if-eqz v1, :cond_4

    or-int/lit16 v0, v0, 0x100

    .line 197
    :cond_4
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v2, 0x84

    aget-byte v1, v1, v2

    if-eqz v1, :cond_5

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    .line 202
    :cond_5
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v2, 0x85

    aget-byte v1, v1, v2

    if-eqz v1, :cond_6

    const/high16 v1, 0x1000000

    or-int/2addr v0, v1

    :cond_6
    return v0
.end method

.method public getSSID()Ljava/lang/String;
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/16 v1, 0x42

    aget-byte v1, v0, v1

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x45

    add-int/2addr v1, v2

    .line 89
    invoke-static {v0, v2, v1}, Lorg/apache/commons/lang/ArrayUtils;->subarray([BII)[B

    move-result-object v0

    .line 91
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1
.end method

.method public parseTag([B)Z
    .locals 2

    .line 59
    array-length v0, p1

    const/16 v1, 0x122

    if-eq v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 63
    :cond_0
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->tagData:[B

    const/4 p1, 0x1

    return p1
.end method

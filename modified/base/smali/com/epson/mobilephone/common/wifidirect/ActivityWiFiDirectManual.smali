.class public Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;
.super Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;
.source "ActivityWiFiDirectManual.java"


# static fields
.field private static final IDD_WIFI_WAITING:Ljava/lang/String; = "idd_wifi_waiting"

.field private static final ID_FOUND:I = 0x1

.field private static final ID_NOT_FOUND:I = 0x2

.field private static final ID_REQUEST_PERMISSION:I = 0x1

.field private static final ID_WIFI_SETTINGS:I = 0x0

.field private static final PARAM_NO_LCD:Ljava/lang/String; = "NO_LCD"

.field private static final TIMEOUT_CONNECTING:I = 0x1e


# instance fields
.field mHandler:Landroid/os/Handler;

.field private mModelDialog:Lepson/common/DialogProgressViewModel;

.field mNoLcdGuidanceWebView:Landroid/webkit/WebView;

.field mWifiManager:Landroid/net/wifi/WifiManager;

.field mWithLcdGuidanceView:Landroid/view/ViewGroup;

.field taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

.field viewWiFiSettings:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;-><init>()V

    const/4 v0, 0x0

    .line 56
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 61
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    .line 209
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;)Lepson/common/DialogProgressViewModel;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mModelDialog:Lepson/common/DialogProgressViewModel;

    return-object p0
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 295
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 297
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private static getNoLcdValueFromIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    const-string v1, "NO_LCD"

    .line 269
    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static getStartIntent(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 273
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "NO_LCD"

    .line 274
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method public static synthetic lambda$onCreate$0(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;Ljava/util/Deque;)V
    .locals 2

    .line 86
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 88
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 89
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 92
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->showDialog(Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 96
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 97
    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->dismissDialog(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 2

    .line 286
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0480

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lepson/common/DialogProgress;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lepson/common/DialogProgress;

    move-result-object v0

    .line 287
    invoke-virtual {v0, v1}, Lepson/common/DialogProgress;->setCancelable(Z)V

    .line 288
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lepson/common/DialogProgress;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private updateGuidance(Z)V
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x8

    if-eqz p1, :cond_1

    .line 147
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mWithLcdGuidanceView:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 148
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mNoLcdGuidanceWebView:Landroid/webkit/WebView;

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    const p1, 0x7f0e041c

    .line 149
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getString(I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->finish()V

    return-void

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mNoLcdGuidanceWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mNoLcdGuidanceWebView:Landroid/webkit/WebView;

    invoke-virtual {p1, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 159
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mWithLcdGuidanceView:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method interruptFindingPrinter()V
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->cancel()V

    const/4 v0, 0x0

    .line 254
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 173
    invoke-super {p0, p1, p2, p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_0

    .line 202
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->onBackPressed()V

    goto :goto_0

    .line 179
    :pswitch_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 183
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->setDefaultNetworkSimpleAp()V

    .line 186
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mModelDialog:Lepson/common/DialogProgressViewModel;

    const-string p2, "idd_wifi_waiting"

    invoke-virtual {p1, p2}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    .line 188
    new-instance p1, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mHandler:Landroid/os/Handler;

    const/16 p3, 0x1e

    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-direct {p1, p2, p3, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;-><init>(Landroid/os/Handler;III)V

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    .line 189
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 77
    invoke-super {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lepson/common/DialogProgressViewModel;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mModelDialog:Lepson/common/DialogProgressViewModel;

    .line 81
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mModelDialog:Lepson/common/DialogProgressViewModel;

    invoke-virtual {p1}, Lepson/common/DialogProgressViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/-$$Lambda$ActivityWiFiDirectManual$MuVLGlWIESpX522WWRFPgBTmA_A;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/-$$Lambda$ActivityWiFiDirectManual$MuVLGlWIESpX522WWRFPgBTmA_A;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 103
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 106
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a00ca

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 107
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->setContentView(Landroid/view/View;)V

    const/4 v0, 0x1

    .line 108
    invoke-virtual {p0, v0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->setupCommonHeaderControl(ZZ)V

    const v1, 0x7f0802b4

    .line 111
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->viewWiFiSettings:Landroid/view/View;

    .line 112
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->viewWiFiSettings:Landroid/view/View;

    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08016c

    .line 126
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/webkit/WebView;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mNoLcdGuidanceWebView:Landroid/webkit/WebView;

    const p1, 0x7f08016d

    .line 127
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->mWithLcdGuidanceView:Landroid/view/ViewGroup;

    .line 129
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getNoLcdValueFromIntent(Landroid/content/Intent;)Z

    move-result p1

    .line 130
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->updateGuidance(Z)V

    .line 133
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->canAccessWiFiInfo(Landroid/content/Context;I)Z

    move-result p1

    if-nez p1, :cond_0

    .line 136
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestLocationPermissionForce(Landroid/app/Activity;I)V

    return-void

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .line 165
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onPause()V

    .line 167
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->interruptFindingPrinter()V

    return-void
.end method

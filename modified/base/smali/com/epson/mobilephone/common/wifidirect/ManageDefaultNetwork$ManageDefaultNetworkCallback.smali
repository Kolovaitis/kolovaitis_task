.class Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "ManageDefaultNetwork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ManageDefaultNetworkCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)V
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 2

    const-string v0, "ManageDefaultNetwork"

    const-string v1, "Call onAvailable"

    .line 298
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onAvailable(Landroid/net/Network;)V

    .line 303
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 304
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->access$100(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBoundNetworkForProcess()Landroid/net/Network;

    move-result-object v0

    if-nez v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->access$100(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->bindProcessToNetwork(Landroid/net/Network;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "ManageDefaultNetwork"

    const-string v0, "bindProcessToNetwork"

    .line 306
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->access$100(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)Landroid/net/ConnectivityManager;

    invoke-static {}, Landroid/net/ConnectivityManager;->getProcessDefaultNetwork()Landroid/net/Network;

    move-result-object v0

    if-nez v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->access$100(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)Landroid/net/ConnectivityManager;

    invoke-static {p1}, Landroid/net/ConnectivityManager;->setProcessDefaultNetwork(Landroid/net/Network;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "ManageDefaultNetwork"

    const-string v0, "setProcessDefaultNetwork"

    .line 312
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 2

    const-string v0, "ManageDefaultNetwork"

    const-string v1, "Call onLost"

    .line 321
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onLost(Landroid/net/Network;)V

    .line 325
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->resetDefaultNetwork()V

    return-void
.end method

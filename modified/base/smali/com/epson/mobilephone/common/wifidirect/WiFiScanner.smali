.class public Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;
.super Ljava/lang/Object;
.source "WiFiScanner.java"


# static fields
.field private static final INTERVAL_SCAN:I = 0x1e

.field private static wifiScannerHandler:Landroid/os/Handler;

.field private static wifiScannerToken:Ljava/lang/Object;


# instance fields
.field private TAG:Ljava/lang/String;

.field private refWiFiManager:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/net/wifi/WifiManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "WiFiScanner"

    .line 46
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    .line 53
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->refWiFiManager:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;)Ljava/lang/ref/WeakReference;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->refWiFiManager:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;)Ljava/lang/String;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->TAG:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method startScan(Landroid/net/wifi/WifiManager;)Z
    .locals 5

    .line 56
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/16 v2, 0x1c

    if-lt v0, v2, :cond_1

    .line 60
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->wifiScannerHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->wifiScannerHandler:Landroid/os/Handler;

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->wifiScannerToken:Ljava/lang/Object;

    .line 66
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->wifiScannerHandler:Landroid/os/Handler;

    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->wifiScannerToken:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 69
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->refWiFiManager:Ljava/lang/ref/WeakReference;

    .line 70
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->wifiScannerHandler:Landroid/os/Handler;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;)V

    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->wifiScannerToken:Ljava/lang/Object;

    const-wide/16 v3, 0x7530

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    return v1

    .line 85
    :cond_1
    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->startScan()Z

    move-result p1

    if-nez p1, :cond_2

    .line 86
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->TAG:Ljava/lang/String;

    const-string v0, "Failed startScan()"

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1

    :cond_2
    return v1
.end method

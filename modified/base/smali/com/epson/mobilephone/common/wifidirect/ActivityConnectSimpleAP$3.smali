.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;
.super Landroid/os/Handler;
.source "ActivityConnectSimpleAP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)V
    .locals 0

    .line 431
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .line 435
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 437
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$4;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityConnectBase$Status:[I

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 506
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 537
    :pswitch_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onError()V

    goto/16 :goto_1

    .line 509
    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_a

    const-string v0, "name"

    .line 511
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ip"

    .line 512
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    .line 513
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_a

    if-eqz v1, :cond_a

    if-eqz v2, :cond_a

    const-string v1, "ActivityConnectSimpleAP"

    const-string v3, "epsWrapperFindPrinter Success"

    .line 516
    invoke-static {v1, v3}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 520
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->expectedPrtMacAddr:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string p1, "ActivityConnectSimpleAP"

    .line 521
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This Printer is not expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->interruptFindingPrinter()V

    .line 528
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 529
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 531
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onConnectedPrinter(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 457
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_1

    goto :goto_0

    .line 459
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 460
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "ssid"

    .line 462
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 465
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 468
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz v2, :cond_2

    .line 469
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    .line 470
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 474
    :cond_2
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    const-string v3, "id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->access$302(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;I)I

    .line 477
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    .line 478
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)I

    move-result v2

    .line 477
    invoke-virtual {v0, v2, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->enableSimpleAP(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 480
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onError()V

    goto :goto_0

    .line 483
    :cond_3
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v1, v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 484
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setupObserver()V

    goto :goto_0

    :cond_4
    const-string v0, "ActivityConnectSimpleAP"

    const-string v1, "Not Found Network"

    .line 488
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v1, v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 490
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onError()V

    .line 497
    :cond_5
    :goto_0
    :pswitch_4
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0xa

    if-eq p1, v0, :cond_6

    goto :goto_1

    .line 499
    :cond_6
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 500
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onConnectedWiFi()V

    goto :goto_1

    .line 439
    :pswitch_5
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0xb

    if-eq p1, v0, :cond_7

    goto :goto_1

    .line 442
    :cond_7
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x19

    if-le p1, v0, :cond_8

    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-boolean p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->createSimpleAP:Z

    if-nez p1, :cond_8

    .line 443
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    move-result-object p1

    const-string v0, "idd_wifi_simpleap"

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->doShow(Ljava/lang/String;)V

    goto :goto_1

    .line 444
    :cond_8
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result p1

    if-nez p1, :cond_9

    .line 446
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 447
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 450
    :cond_9
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onEnabledWifi()V

    :cond_a
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

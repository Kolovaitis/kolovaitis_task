.class public Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;
.super Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;
.source "NfcMacAddrUtils.java"


# static fields
.field private static final LENGTH_PASSWORD:I = 0x8

.field private static final PREFIX_SSID:Ljava/lang/String; = "DIRECT-"

.field private static final SUFIX_SSID_INDEX:I = 0x4

.field static final acCharacterTblPassphrase:[C

.field static final acCharacterTblPassphraseCL1:[C

.field private static final password_seed_salt:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x23

    .line 27
    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->acCharacterTblPassphrase:[C

    const/16 v0, 0xa

    .line 35
    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->acCharacterTblPassphraseCL1:[C

    const/16 v0, 0x14

    .line 41
    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->password_seed_salt:[B

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
        0x67s
        0x68s
        0x69s
        0x6as
        0x6bs
        0x6ds
        0x6es
        0x6fs
        0x70s
        0x71s
        0x72s
        0x73s
        0x74s
        0x75s
        0x76s
        0x77s
        0x78s
        0x79s
        0x7as
    .end array-data

    nop

    :array_1
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
    .end array-data

    :array_2
    .array-data 1
        0x0t
        0x9t
        0x8t
        0x7t
        0x6t
        0x5t
        0x4t
        0x3t
        0x2t
        0x1t
        0x0t
        0x9t
        0x8t
        0x7t
        0x6t
        0x5t
        0x4t
        0x3t
        0x2t
        0x1t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;-><init>()V

    return-void
.end method

.method static getMacAddressP2P(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 105
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAdressBytes(Ljava/lang/String;)[B

    move-result-object p0

    const/4 v0, 0x0

    .line 111
    aget-byte v1, p0, v0

    or-int/lit8 v1, v1, 0x2

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    const/4 v0, 0x4

    .line 112
    aget-byte v1, p0, v0

    xor-int/lit8 v1, v1, -0x80

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 114
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static getPassFormMacAddress(Ljava/lang/String;[C)Ljava/lang/String;
    .locals 6

    .line 65
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 66
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAdressBytes(Ljava/lang/String;)[B

    move-result-object p0

    .line 67
    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->password_seed_salt:[B

    array-length v1, v1

    const/4 v2, 0x6

    add-int/2addr v1, v2

    new-array v1, v1, [B

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    rsub-int/lit8 v5, v4, 0x6

    add-int/lit8 v5, v5, -0x1

    .line 72
    aget-byte v5, p0, v5

    aput-byte v5, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 76
    :cond_0
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->password_seed_salt:[B

    array-length v4, p0

    invoke-static {p0, v3, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :try_start_0
    const-string p0, "MD5"

    .line 80
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object p0

    .line 81
    invoke-virtual {p0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 82
    invoke-virtual {p0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object p0

    :goto_1
    const/16 v1, 0x8

    if-ge v3, v1, :cond_1

    .line 86
    aget-byte v1, p0, v3

    and-int/lit16 v1, v1, 0xff

    array-length v2, p1

    rem-int/2addr v1, v2

    aget-char v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception p0

    .line 91
    invoke-virtual {p0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 94
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static getSSIDFromMacAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DIRECT-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.class Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;
.super Landroid/os/AsyncTask;
.source "ActivityiPrintConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->startConnectRealAp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;)V
    .locals 0

    .line 423
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 10

    .line 442
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->pingWiFiDirectPrinter()V

    .line 445
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    const/16 v0, 0xc0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperInitDriver(I)I

    const/4 p1, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_2

    .line 450
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 455
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    invoke-static {}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->access$100()[B

    move-result-object v3

    iget-object v4, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    invoke-virtual {v2, v3, v4}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperGetConnectStrings([BLcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;)I

    move-result v2

    .line 457
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v2, :cond_2

    add-int/lit8 v1, v1, 0x1

    const-string v3, "ActivityiPrintConnect"

    .line 469
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "epsWrapperGetConnectStrings ret = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " retry = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v3, 0x7d0

    .line 470
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 458
    :cond_0
    new-instance v1, Ljava/lang/InterruptedException;

    invoke-direct {v1}, Ljava/lang/InterruptedException;-><init>()V

    throw v1

    .line 451
    :cond_1
    new-instance v1, Ljava/lang/InterruptedException;

    invoke-direct {v1}, Ljava/lang/InterruptedException;-><init>()V

    throw v1

    :catch_0
    move-exception v1

    goto/16 :goto_2

    :cond_2
    if-nez v2, :cond_4

    const-string v1, "ActivityiPrintConnect"

    .line 475
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "epsWrapperGetConnectStrings ssid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " printerId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->printerId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    iget-object v3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v3, v3, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v3, v3, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->printerIp:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperStartWifiDirect([C)I

    .line 482
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->waitRealAp:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    :try_start_1
    iget-object v3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->registBroadcastReciever()V

    .line 485
    iget-object v3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v3, v3, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 487
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 488
    iget-object v5, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v5, v5, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->waitRealAp:Ljava/lang/Object;

    const-wide/32 v6, 0x1d4c0

    invoke-virtual {v5, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 489
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v3

    cmp-long v3, v8, v6

    if-ltz v3, :cond_3

    const-string v2, "ActivityiPrintConnect"

    const-string v3, "Find RealAp Timepout"

    .line 490
    invoke-static {v2, v3}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, -0x1

    .line 494
    :cond_3
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2

    :cond_4
    const-string v1, "ActivityiPrintConnect"

    .line 498
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "epsWrapperGetConnectStrings ret = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "epsWrapperGetConnectStrings ret = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->lastErrorStrings:Ljava/lang/String;

    .line 503
    :goto_1
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    iget-object v3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v3, v3, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    iget-object v4, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeSimpleAP(I)Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    move v0, v2

    goto :goto_3

    .line 506
    :goto_2
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 511
    :goto_3
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->unRegistBroadcastReciever()V

    .line 514
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperReleaseDriver()I

    if-nez v0, :cond_5

    const/4 p1, 0x1

    .line 516
    :cond_5
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 423
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled()V
    .locals 3

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "taskGetConnectStrings Canceled"

    .line 548
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->access$002(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;)Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    .line 554
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 557
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeSimpleAP(I)Z

    .line 559
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3

    .line 522
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 524
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 526
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTING_REALAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->access$002(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;)Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    .line 529
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.epson.iprint.wifidirect.apname"

    .line 530
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.appass"

    .line 531
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->password:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.create"

    const/4 v1, 0x1

    .line 532
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.showerror"

    const/4 v2, 0x0

    .line 533
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.showconnecttip"

    .line 534
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.timeout"

    const/16 v2, 0x3c

    .line 535
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.needinfo"

    .line 536
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 538
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 541
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->access$002(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;)Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    .line 542
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->onConnectError()V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 423
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 428
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 430
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->GETTING_GONNECTSTRINGS:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->access$002(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;)Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    return-void
.end method

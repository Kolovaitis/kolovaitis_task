.class public Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;
.super Landroid/app/Activity;
.source "ActivityRequestLocationPermission.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler;
    }
.end annotation


# static fields
.field public static final ACCESS_BLE:I = 0x4

.field public static final ACCESS_SIMPLEPAP:I = 0x1

.field public static final ACCESS_WIFI:I = 0x1

.field public static final ACCESS_WIFIP2P:I = 0x10

.field private static final DONOTASKAGAIN:Ljava/lang/String; = "DONOTASKAGAIN"

.field private static final ID_CHECK_LOCATION_SETTINGS:I = 0x1

.field private static final ID_ENABLED_LOCATION_SETTINGS:I = 0x1

.field private static final ID_ENABLED_PERMISSION:I = 0x1

.field private static final ID_RECHECK_LOCATION_SETTINGS:I = 0x2

.field private static final ID_RESULT_APPSETTINGS:I = 0x3

.field private static final INTENT_FORCE_REQUEST:Ljava/lang/String; = "INTENT_FORCE_REQUEST"

.field private static final REJECTED_REQUEST:Ljava/lang/String; = "REJECTED_REQUEST"

.field private static final TAG:Ljava/lang/String; = "ActivityRequestLocationPermission"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private requestPermission:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 287
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->setRejectRequestPermission()V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)Ljava/lang/String;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestPermission:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Landroid/content/Context;)Z
    .locals 0

    .line 47
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->isNeedChangeLocationSettings(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->checkLocationPreference()V

    return-void
.end method

.method static synthetic access$400(Landroid/content/Context;I)Landroid/widget/LinearLayout;
    .locals 0

    .line 47
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getTitleLayout(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object p0

    return-object p0
.end method

.method public static canAccessWiFiInfo(Landroid/content/Context;I)Z
    .locals 3

    .line 660
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0x1c

    if-le v0, v2, :cond_1

    and-int/lit8 p1, p1, 0x15

    if-eqz p1, :cond_3

    .line 665
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->isNeedLocationPermission(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 666
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->hasLocationPermission(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    return v1

    .line 673
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->isNeedChangeLocationSettings(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_3

    return v1

    :cond_1
    and-int/lit8 p1, p1, 0x5

    if-eqz p1, :cond_3

    .line 684
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->isNeedLocationPermission(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 685
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->hasLocationPermission(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    return v1

    .line 691
    :cond_2
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->isNeedChangeLocationSettings(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_3

    return v1

    :cond_3
    const/4 p0, 0x1

    return p0
.end method

.method private checkLocationPreference()V
    .locals 4

    .line 308
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 310
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/LocationServices;->API:Lcom/google/android/gms/common/api/Api;

    .line 311
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 312
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 313
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    .line 317
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 320
    new-instance v1, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v1}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    const/16 v2, 0x66

    .line 321
    invoke-virtual {v1, v2}, Lcom/google/android/gms/location/LocationRequest;->setPriority(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    .line 324
    new-instance v2, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;-><init>()V

    const/4 v3, 0x1

    .line 325
    invoke-virtual {v2, v3}, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;->setAlwaysShow(Z)Lcom/google/android/gms/location/LocationSettingsRequest$Builder;

    move-result-object v2

    .line 326
    invoke-virtual {v2, v1}, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;->addLocationRequest(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/LocationSettingsRequest$Builder;

    move-result-object v1

    .line 328
    sget-object v2, Lcom/google/android/gms/location/LocationServices;->SettingsApi:Lcom/google/android/gms/location/SettingsApi;

    .line 329
    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;->build()Lcom/google/android/gms/location/LocationSettingsRequest;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/location/SettingsApi;->checkLocationSettings(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/location/LocationSettingsRequest;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 330
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 359
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 360
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    .line 364
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    .line 365
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x2

    .line 367
    :try_start_1
    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    .line 369
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 370
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    :goto_0
    return-void
.end method

.method private static getNeededPermissionString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 600
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1c

    if-le p0, v0, :cond_0

    const-string p0, "android.permission.ACCESS_FINE_LOCATION"

    return-object p0

    :cond_0
    const-string p0, "android.permission.ACCESS_COARSE_LOCATION"

    return-object p0
.end method

.method private static getTitleLayout(Landroid/content/Context;I)Landroid/widget/LinearLayout;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 154
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 156
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0x19

    const/16 v3, 0x28

    .line 157
    invoke-virtual {v1, v3, v2, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 158
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 159
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 p0, 0x41880000    # 17.0f

    .line 160
    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setTextSize(F)V

    const/4 p0, 0x5

    .line 161
    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 162
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private static hasLocationPermission(Landroid/content/Context;)Z
    .locals 3

    .line 564
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/16 v2, 0x17

    if-lt v0, v2, :cond_1

    .line 565
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getNeededPermissionString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    return v1
.end method

.method private static isNeedChangeLocationSettings(Landroid/content/Context;)Z
    .locals 3

    .line 616
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0x17

    if-ge v0, v2, :cond_0

    return v1

    .line 620
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_2

    .line 623
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "location_mode"

    invoke-static {p0, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result p0

    const/4 v0, 0x1

    if-eqz p0, :cond_1

    if-ne p0, v0, :cond_2

    :cond_1
    const-string p0, "ActivityRequestLocationPermission"

    const-string v2, "isNeedChangeLocationSettings() return true;"

    .line 626
    invoke-static {p0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p0

    .line 630
    invoke-virtual {p0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    :cond_2
    return v1
.end method

.method private static isNeedLocationPermission(Landroid/content/Context;)Z
    .locals 3

    .line 540
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    return v2

    .line 544
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 546
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 548
    iget p0, p0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0

    :catch_0
    const/4 p0, 0x0

    :goto_0
    if-lt p0, v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    return v2
.end method

.method private isRejectedRequestPermission()Z
    .locals 5

    .line 173
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 176
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "REJECTED_REQUEST_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v1
.end method

.method public static requestLocationPermission(Landroid/app/Activity;I)V
    .locals 2

    .line 494
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 497
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static requestLocationPermission(Landroid/support/v4/app/Fragment;I)V
    .locals 3

    .line 525
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 528
    invoke-virtual {p0, v0, p1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static requestLocationPermissionForce(Landroid/app/Activity;I)V
    .locals 3

    .line 509
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "INTENT_FORCE_REQUEST"

    const/4 v2, 0x1

    .line 510
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 513
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private setRejectRequestPermission()V
    .locals 4

    .line 190
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 192
    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 193
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 194
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REJECTED_REQUEST_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 195
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 379
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 383
    :pswitch_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->hasLocationPermission(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 385
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 388
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    :pswitch_1
    if-eq p2, v0, :cond_2

    .line 417
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->isNeedChangeLocationSettings(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 419
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onSuccess()V

    goto :goto_0

    .line 421
    :cond_1
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    .line 412
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_2
    if-eq p2, v0, :cond_3

    .line 402
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    .line 397
    :cond_3
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onSuccess()V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "ActivityRequestLocationPermission"

    .line 443
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConnectionFailed() connectionResult="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    const-string v0, "ActivityRequestLocationPermission"

    const-string v2, "connectionResult has resolution. Try resolution."

    .line 446
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :try_start_0
    invoke-virtual {p1, p0, v1}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 450
    invoke-virtual {p1}, Landroid/content/IntentSender$SendIntentException;->printStackTrace()V

    .line 451
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    :cond_0
    const-string p1, "ActivityRequestLocationPermission"

    const-string v0, "connectionResult do\'nt has resolution"

    .line 454
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 459
    invoke-virtual {p1, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    const-string p1, "ActivityRequestLocationPermission"

    const-string v0, "resultCode is SUCCESS. No resolution"

    .line 462
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    .line 464
    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isUserResolvableError(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 466
    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$4;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$4;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V

    invoke-virtual {p1, p0, v0, v1, v2}, Lcom/google/android/gms/common/GoogleApiAvailability;->showErrorDialogFragment(Landroid/app/Activity;IILandroid/content/DialogInterface$OnCancelListener;)Z

    goto :goto_0

    :cond_2
    const-string p1, "ActivityRequestLocationPermission"

    const-string v0, "No userResolvableError."

    .line 475
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    :cond_3
    const-string p1, "ActivityRequestLocationPermission"

    const-string v0, "GoogleApiAvailability.getInstance() failed."

    .line 479
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    :goto_0
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "INTENT_FORCE_REQUEST"

    .line 80
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 86
    :goto_0
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->isRejectedRequestPermission()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 87
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    return-void

    :cond_1
    const/4 p1, 0x1

    .line 92
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestWindowFeature(I)Z

    .line 94
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->isNeedLocationPermission(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 95
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->hasLocationPermission(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 97
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getNeededPermissionString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestPermission:Ljava/lang/String;

    .line 100
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v1, "DONOTASKAGAIN"

    .line 101
    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    .line 104
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_need_location_setting_title:I

    .line 105
    invoke-static {p0, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getTitleLayout(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_need_location_permission:I

    .line 106
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_next:I

    new-instance v3, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;

    invoke-direct {v3, p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;Z)V

    .line 107
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_cancel:I

    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$1;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V

    .line 123
    invoke-virtual {p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 131
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 132
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 137
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-le v0, v1, :cond_4

    .line 139
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 141
    :cond_4
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onSuccess()V

    :goto_1
    return-void
.end method

.method onError()V
    .locals 1

    const/4 v0, 0x0

    .line 297
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->setResult(I)V

    .line 298
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->finish()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 4
    .param p2    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 203
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x0

    .line 208
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_2

    .line 209
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestPermission:Ljava/lang/String;

    aget-object v3, p2, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 210
    aget v2, p3, v1

    if-nez v2, :cond_1

    const-string p1, "ActivityRequestLocationPermission"

    .line 211
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "PERMISSION_GRANTED "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestPermission:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 223
    :cond_2
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestPermission:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_3

    .line 225
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 226
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string p2, "DONOTASKAGAIN"

    .line 227
    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 228
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 231
    :cond_3
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    :goto_1
    return-void
.end method

.method onSuccess()V
    .locals 1

    const/4 v0, -0x1

    .line 291
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->setResult(I)V

    .line 292
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->finish()V

    return-void
.end method

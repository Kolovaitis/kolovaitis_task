.class public Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;
.super Ljava/lang/Object;
.source "WiFiUtils.java"


# static fields
.field static final INVALID_NETWORKID:I = -0x1

.field static final PREFIX_SIMPLEAP_DIRECT:Ljava/lang/String; = "DIRECT-"

.field static final PREFIX_SIMPLEAP_DISALBED:Ljava/lang/String; = "DISABLED-"

.field static final PREFIX_SIMPLEAP_UNUSED:Ljava/lang/String; = "UNUSED-"

.field static final SUFIX_SIMPLEAP_DISALBED:Ljava/lang/String; = "-DISABLED"

.field private static final TAG:Ljava/lang/String; = "WiFiUtils"

.field static final WifiSsid_NONE:Ljava/lang/String; = "<unknown ssid>"

.field private static htforToast:Landroid/os/HandlerThread;

.field private static instance:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;


# instance fields
.field private cm:Landroid/net/ConnectivityManager;

.field private wm:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 797
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Thread for Toast"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->htforToast:Landroid/os/HandlerThread;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    .line 64
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->cm:Landroid/net/ConnectivityManager;

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->cm:Landroid/net/ConnectivityManager;

    return-void
.end method

.method static displayToastThroughHandlerThread(Landroid/content/Context;I)V
    .locals 2

    .line 809
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->htforToast:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_0

    .line 810
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->htforToast:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 813
    :cond_0
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->htforToast:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 814
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils$1;

    invoke-direct {v1, p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils$1;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;
    .locals 1

    .line 83
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    if-eqz v0, :cond_0

    return-object v0

    .line 86
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    .line 87
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    return-object p0
.end method

.method private static putAddress(Ljava/lang/StringBuffer;I)V
    .locals 2

    and-int/lit16 v0, p1, 0xff

    .line 977
    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    ushr-int/lit8 p1, p1, 0x8

    and-int/lit16 v1, p1, 0xff

    .line 978
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    ushr-int/lit8 p1, p1, 0x8

    and-int/lit16 v1, p1, 0xff

    .line 979
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    ushr-int/lit8 p1, p1, 0x8

    and-int/lit16 p1, p1, 0xff

    .line 980
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    return-void
.end method

.method public static removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "\""

    .line 118
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\""

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static replaceUnusedSSID(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "DIRECT-"

    .line 96
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UNUSED-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static setAutoGoTimeout(Ljava/lang/String;I)I
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, -0x1

    return p0

    .line 898
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/escprLib;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib;-><init>()V

    const/16 v1, 0xc0

    .line 900
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperInitDriver(I)I

    move-result v1

    const/16 v2, -0x41a

    if-eq v1, v2, :cond_1

    if-eqz v1, :cond_1

    return v1

    .line 910
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object p0

    invoke-virtual {v0, p0, p1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperSetAutoGoTimeOut([CI)I

    move-result p0

    const-string p1, "WiFiUtils"

    .line 911
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "epsWrapperSetAutoGoTimeOut ret = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperReleaseDriver()I

    return p0
.end method

.method public static showOsWifiSettings(Landroid/app/Activity;I)V
    .locals 2

    .line 928
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 929
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private waitDisconnect()V
    .locals 5

    const/16 v0, 0x1388

    const/16 v1, 0xc8

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 752
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->isConnectedWifi()Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    const-string v3, "WiFiUtils"

    const-string v4, "Waiting wifi disconnected"

    .line 756
    invoke-static {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    int-to-long v3, v1

    .line 757
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    add-int/lit16 v2, v2, 0xc8

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_2

    :cond_1
    :goto_1
    if-lt v2, v0, :cond_2

    const-string v0, "WiFiUtils"

    const-string v1, "TIMEOUT wifi disconnected"

    .line 760
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 764
    :goto_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_2
    :goto_3
    return-void
.end method

.method private wifiManager_connect(I)Z
    .locals 2

    .line 456
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string p1, "WiFiUtils"

    const-string v1, "Failed  enableNetwork"

    .line 457
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    .line 481
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->saveWifiConfiguration(Landroid/net/wifi/WifiManager;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "WiFiUtils"

    const-string v1, "Failed  saveConfiguration"

    .line 482
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_1
    return v1
.end method


# virtual methods
.method public createSimpleAP(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5

    .line 318
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 319
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v1

    .line 321
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v3, -0x1

    const/16 v4, 0x19

    if-le v2, v4, :cond_1

    if-eq v1, v3, :cond_1

    .line 327
    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeNetwork(I)Z

    move-result v1

    if-nez v1, :cond_0

    return v3

    :cond_0
    const/4 v1, -0x1

    .line 335
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 336
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\""

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    const/4 p2, 0x2

    .line 337
    iput p2, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    .line 338
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v2, p2}, Ljava/util/BitSet;->set(I)V

    .line 339
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 340
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 341
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 342
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v2, p2}, Ljava/util/BitSet;->set(I)V

    .line 343
    iget-object p2, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    invoke-virtual {p2, v4}, Ljava/util/BitSet;->set(I)V

    .line 344
    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    if-ne v1, v3, :cond_2

    const-string p2, "WiFiUtils"

    const-string v1, "call addNetwork!!"

    .line 348
    invoke-static {p2, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p2, v0}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result p2

    if-ne p2, v3, :cond_3

    const-string p2, "WiFiUtils"

    const-string v1, "Failed addNetwork. Try ramdam ssid!"

    .line 358
    invoke-static {p2, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "-"

    const-string v4, ""

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 361
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p2, v0}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result p2

    if-eq p2, v3, :cond_3

    .line 364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\""

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 365
    iput p2, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 366
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1, v0}, Landroid/net/wifi/WifiManager;->updateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result p2

    goto :goto_0

    :cond_2
    const-string p1, "WiFiUtils"

    const-string p2, "call updateNetwork!!"

    .line 371
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1, v0}, Landroid/net/wifi/WifiManager;->updateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result p2

    :cond_3
    :goto_0
    if-ne p2, v3, :cond_4

    const-string p1, "WiFiUtils"

    const-string p2, "Failed addNetwork or updateNetwork"

    .line 382
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    .line 387
    :cond_4
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurNetworkId()I

    move-result p1

    if-eq p1, v3, :cond_5

    .line 389
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->disableNetwork(I)Z

    .line 393
    :cond_5
    invoke-direct {p0, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wifiManager_connect(I)Z

    move-result p1

    if-nez p1, :cond_6

    const-string p1, "WiFiUtils"

    const-string p2, "Failed  wifiManager_connect"

    .line 394
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :cond_6
    return p2
.end method

.method public disableSimpleAP(Landroid/content/Context;I)Z
    .locals 3

    const-string v0, "WiFiUtils"

    const-string v1, "Enter disableSimpleAP()"

    .line 565
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/16 v2, 0x19

    if-gt v0, v2, :cond_1

    .line 569
    invoke-virtual {p0, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->invalidateSimpleAP(Landroid/content/Context;I)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    return v1

    .line 574
    :cond_1
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->resetDefaultNetwork()V

    return v1
.end method

.method public enableSimpleAP(ILjava/lang/String;)Z
    .locals 5

    .line 411
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getWifiConfig(I)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string v2, "WiFiUtils"

    .line 416
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Enable Wifi Profile: SSID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    const/4 v4, -0x1

    if-ge v2, v3, :cond_1

    .line 421
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\""

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 422
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p2, v0}, Landroid/net/wifi/WifiManager;->updateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result p2

    if-ne p2, v4, :cond_1

    const-string p1, "WiFiUtils"

    const-string p2, "Failed  updateNetwork"

    .line 423
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 429
    :cond_1
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurNetworkId()I

    move-result p2

    if-eq p2, v4, :cond_2

    .line 431
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p2}, Landroid/net/wifi/WifiManager;->disableNetwork(I)Z

    .line 435
    :cond_2
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wifiManager_connect(I)Z

    move-result p1

    if-nez p1, :cond_3

    const-string p1, "WiFiUtils"

    const-string p2, "Failed  wifiManager_connect"

    .line 436
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method public getCurNetworkId()I
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v2

    if-eq v2, v1, :cond_0

    sget-object v2, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    .line 164
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSupplicantState()Landroid/net/wifi/SupplicantState;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/wifi/SupplicantState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public getCurP2PMacAdder()Ljava/lang/String;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurSSID()Ljava/lang/String;
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 183
    sget-object v1, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSupplicantState()Landroid/net/wifi/SupplicantState;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/wifi/SupplicantState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 187
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1c

    if-gt v1, v2, :cond_0

    .line 188
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 189
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getSSID(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 190
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    return-object v1

    .line 199
    :cond_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 200
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    const-string v1, "<unknown ssid>"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    return-object v0

    :cond_1
    const-string v0, "WiFiUtils"

    const-string v1, "Error getCurSSID() : Do you have location permission?"

    .line 204
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExistSimpleApDisabled(Ljava/lang/String;)I
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 134
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->replaceUnusedSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-DISABLED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v0

    :cond_0
    if-ne v0, v1, :cond_1

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DISABLED-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public getNetworkId(Ljava/lang/String;)I
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, -0x1

    if-nez p1, :cond_0

    return v0

    .line 243
    :cond_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 246
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    .line 248
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 249
    iget p1, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    return p1

    :cond_2
    return v0
.end method

.method public getSSID(I)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    return-object v0

    .line 299
    :cond_0
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getWifiConfig(I)Landroid/net/wifi/WifiConfiguration;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 302
    iget-object p1, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    return-object v0
.end method

.method public getWifiConfig(I)Landroid/net/wifi/WifiConfiguration;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 271
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 274
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    .line 276
    iget v2, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    if-ne v2, p1, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public invalidateSimpleAP(Landroid/content/Context;I)Z
    .locals 9

    const-string v0, "WiFiUtils"

    const-string v1, "Enter invalidateSimpleAP()"

    .line 587
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    const/4 v1, 0x0

    if-ne p2, v0, :cond_0

    return v1

    .line 596
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurNetworkId()I

    move-result v2

    const/4 v3, 0x1

    if-ne p2, v2, :cond_3

    .line 599
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2, p2}, Landroid/net/wifi/WifiManager;->disableNetwork(I)Z

    move-result v2

    if-nez v2, :cond_1

    const-string p1, "WiFiUtils"

    const-string p2, "Failed  disableNetwork"

    .line 600
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 604
    :cond_1
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->disconnect()Z

    move-result v2

    if-nez v2, :cond_2

    const-string p1, "WiFiUtils"

    const-string p2, "Failed  disconnect"

    .line 605
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 611
    :cond_2
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->waitDisconnect()V

    .line 617
    sget v2, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_notice_wifi_disconnected:I

    invoke-static {p1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->displayToastThroughHandlerThread(Landroid/content/Context;I)V

    const/4 p1, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    .line 621
    :goto_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-ge v2, v4, :cond_a

    .line 623
    invoke-virtual {p0, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getWifiConfig(I)Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    if-nez v2, :cond_4

    return v1

    :cond_4
    const-string v4, "WiFiUtils"

    .line 628
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Disable Wifi Profile : SSID = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " networkid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " status = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, v2, Landroid/net/wifi/WifiConfiguration;->status:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "^DIRECT-..-.*"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 632
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->replaceUnusedSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    goto :goto_1

    .line 634
    :cond_5
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_6

    .line 636
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\"DISABLED-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    goto :goto_1

    .line 638
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "-DISABLED"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 643
    :goto_1
    iget-object v4, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v4

    .line 644
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/WifiConfiguration;

    .line 645
    iget v6, v5, Landroid/net/wifi/WifiConfiguration;->networkId:I

    if-eq v6, p2, :cond_7

    .line 646
    iget-object v6, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget-object v7, v5, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "WiFiUtils"

    .line 647
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Delete network for Backup : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v5, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    iget-object v6, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    iget v5, v5, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v6, v5}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    goto :goto_2

    .line 654
    :cond_8
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p2, v2}, Landroid/net/wifi/WifiManager;->updateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result p2

    if-ne p2, v0, :cond_9

    const-string p1, "WiFiUtils"

    const-string p2, "Failed  updateNetwork"

    .line 655
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 660
    :cond_9
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p0, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->saveWifiConfiguration(Landroid/net/wifi/WifiManager;)Z

    move-result p2

    if-nez p2, :cond_a

    const-string p1, "WiFiUtils"

    const-string p2, "Failed  saveConfiguration"

    .line 661
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_a
    if-eqz p1, :cond_b

    .line 668
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->reConnectNetwork()V

    :cond_b
    return v3
.end method

.method public isConnectedWifi()Z
    .locals 8

    .line 829
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/16 v4, 0x15

    if-lt v0, v4, :cond_2

    .line 832
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->cm:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworks()[Landroid/net/Network;

    move-result-object v0

    const/4 v4, 0x0

    .line 835
    :goto_0
    array-length v5, v0

    if-ge v4, v5, :cond_5

    .line 837
    aget-object v5, v0, v4

    .line 838
    iget-object v6, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->cm:Landroid/net/ConnectivityManager;

    invoke-virtual {v6, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 842
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    if-eq v6, v3, :cond_0

    if-eq v6, v1, :cond_0

    goto :goto_1

    .line 846
    :cond_0
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v6

    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v6, v7}, Landroid/net/NetworkInfo$State;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v0, "WiFiUtils"

    const-string v1, "isConnectedWifi = true : TypeName = %s"

    .line 847
    new-array v4, v3, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 859
    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->cm:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v0

    .line 861
    array-length v4, v0

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v4, :cond_5

    aget-object v6, v0, v5

    .line 862
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getType()I

    move-result v7

    if-eq v7, v3, :cond_3

    if-eq v7, v1, :cond_3

    goto :goto_3

    .line 869
    :cond_3
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v0, "WiFiUtils"

    const-string v1, "isConnectedWifi = true : TypeName = %s"

    .line 870
    new-array v4, v3, [Ljava/lang/Object;

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :cond_4
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    return v2
.end method

.method public pingWiFiDirectPrinter()V
    .locals 4

    .line 941
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getDhcpInfo()Landroid/net/DhcpInfo;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 947
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 948
    iget v0, v0, Landroid/net/DhcpInfo;->serverAddress:I

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->putAddress(Ljava/lang/StringBuffer;I)V

    .line 952
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v2, 0x3e8

    .line 954
    invoke-virtual {v0, v2}, Ljava/net/InetAddress;->isReachable(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "WiFiUtils"

    .line 955
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Success ping "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "WiFiUtils"

    .line 957
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed ping "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 965
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 962
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    :cond_2
    :goto_0
    return-void
.end method

.method public reConnectNetwork()V
    .locals 4

    const-string v0, "WiFiUtils"

    const-string v1, "called reConnectNetwork"

    .line 679
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    .line 689
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    .line 693
    iget-object v2, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 694
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 695
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    iget v1, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v2, v1}, Landroid/net/wifi/WifiManager;->disableNetwork(I)Z

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_0

    .line 700
    iget v2, v1, Landroid/net/wifi/WifiConfiguration;->status:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 701
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    iget v1, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    goto :goto_0

    .line 706
    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->saveWifiConfiguration(Landroid/net/wifi/WifiManager;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "WiFiUtils"

    const-string v1, "Failed  saveConfiguration"

    .line 707
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :cond_3
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->reassociate()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "WiFiUtils"

    const-string v1, "Failed  reassociate"

    .line 712
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method public removeNetwork(I)Z
    .locals 5

    .line 538
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getWifiConfig(I)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string v2, "WiFiUtils"

    .line 543
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remove Wifi Profile : SSID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " networkid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " status = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "WiFiUtils"

    const-string v0, "Failed  removeNetwork"

    .line 546
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 551
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->saveWifiConfiguration(Landroid/net/wifi/WifiManager;)Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "WiFiUtils"

    const-string v0, "Failed  saveConfiguration"

    .line 552
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public removeSimpleAP(I)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    return v0

    .line 505
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurNetworkId()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 506
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->wm:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->disconnect()Z

    move-result v1

    if-nez v1, :cond_1

    const-string p1, "WiFiUtils"

    const-string v1, "Failed  disconnect"

    .line 507
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    .line 519
    :cond_1
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeNetwork(I)Z

    move-result p1

    if-nez p1, :cond_2

    return v0

    .line 525
    :cond_2
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->reConnectNetwork()V

    const/4 p1, 0x1

    return p1
.end method

.method saveWifiConfiguration(Landroid/net/wifi/WifiManager;)Z
    .locals 2

    .line 725
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    const-string p1, "WiFiUtils"

    const-string v0, "Needless call saveConfiguration"

    .line 731
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 733
    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "WiFiUtils"

    const-string v0, "Failed  saveConfiguration"

    .line 734
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public waitConnected()V
    .locals 5

    const/16 v0, 0x2710

    const/16 v1, 0x1f4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 778
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->isConnectedWifi()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    goto :goto_1

    :cond_0
    const-string v3, "WiFiUtils"

    const-string v4, "Waiting wifi connected"

    .line 782
    invoke-static {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    int-to-long v3, v1

    .line 783
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    add-int/lit16 v2, v2, 0x1f4

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_2

    :cond_1
    :goto_1
    if-lt v2, v0, :cond_2

    const-string v0, "WiFiUtils"

    const-string v1, "TIMEOUT wifi connected"

    .line 786
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 790
    :goto_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_2
    :goto_3
    return-void
.end method

.class public final Lcom/epson/mobilephone/common/wifidirect/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0e027a

.field public static final abc_action_bar_up_description:I = 0x7f0e027b

.field public static final abc_action_menu_overflow_description:I = 0x7f0e027c

.field public static final abc_action_mode_done:I = 0x7f0e027d

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0e027e

.field public static final abc_activitychooserview_choose_application:I = 0x7f0e027f

.field public static final abc_capital_off:I = 0x7f0e0280

.field public static final abc_capital_on:I = 0x7f0e0281

.field public static final abc_font_family_body_1_material:I = 0x7f0e0282

.field public static final abc_font_family_body_2_material:I = 0x7f0e0283

.field public static final abc_font_family_button_material:I = 0x7f0e0284

.field public static final abc_font_family_caption_material:I = 0x7f0e0285

.field public static final abc_font_family_display_1_material:I = 0x7f0e0286

.field public static final abc_font_family_display_2_material:I = 0x7f0e0287

.field public static final abc_font_family_display_3_material:I = 0x7f0e0288

.field public static final abc_font_family_display_4_material:I = 0x7f0e0289

.field public static final abc_font_family_headline_material:I = 0x7f0e028a

.field public static final abc_font_family_menu_material:I = 0x7f0e028b

.field public static final abc_font_family_subhead_material:I = 0x7f0e028c

.field public static final abc_font_family_title_material:I = 0x7f0e028d

.field public static final abc_menu_alt_shortcut_label:I = 0x7f0e028e

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f0e028f

.field public static final abc_menu_delete_shortcut_label:I = 0x7f0e0290

.field public static final abc_menu_enter_shortcut_label:I = 0x7f0e0291

.field public static final abc_menu_function_shortcut_label:I = 0x7f0e0292

.field public static final abc_menu_meta_shortcut_label:I = 0x7f0e0293

.field public static final abc_menu_shift_shortcut_label:I = 0x7f0e0294

.field public static final abc_menu_space_shortcut_label:I = 0x7f0e0295

.field public static final abc_menu_sym_shortcut_label:I = 0x7f0e0296

.field public static final abc_prepend_shortcut_label:I = 0x7f0e0297

.field public static final abc_search_hint:I = 0x7f0e0298

.field public static final abc_searchview_description_clear:I = 0x7f0e0299

.field public static final abc_searchview_description_query:I = 0x7f0e029a

.field public static final abc_searchview_description_search:I = 0x7f0e029b

.field public static final abc_searchview_description_submit:I = 0x7f0e029c

.field public static final abc_searchview_description_voice:I = 0x7f0e029d

.field public static final abc_shareactionprovider_share_with:I = 0x7f0e029e

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0e029f

.field public static final abc_toolbar_collapse_description:I = 0x7f0e02a0

.field public static final common_google_play_services_enable_button:I = 0x7f0e02f7

.field public static final common_google_play_services_enable_text:I = 0x7f0e02f8

.field public static final common_google_play_services_enable_title:I = 0x7f0e02f9

.field public static final common_google_play_services_install_button:I = 0x7f0e02fa

.field public static final common_google_play_services_install_text:I = 0x7f0e02fb

.field public static final common_google_play_services_install_title:I = 0x7f0e02fc

.field public static final common_google_play_services_notification_ticker:I = 0x7f0e02fe

.field public static final common_google_play_services_unknown_issue:I = 0x7f0e02ff

.field public static final common_google_play_services_unsupported_text:I = 0x7f0e0300

.field public static final common_google_play_services_update_button:I = 0x7f0e0301

.field public static final common_google_play_services_update_text:I = 0x7f0e0302

.field public static final common_google_play_services_update_title:I = 0x7f0e0303

.field public static final common_google_play_services_updating_text:I = 0x7f0e0304

.field public static final common_google_play_services_wear_update_text:I = 0x7f0e0305

.field public static final common_open_on_phone:I = 0x7f0e0306

.field public static final common_signin_button_text:I = 0x7f0e0307

.field public static final common_signin_button_text_long:I = 0x7f0e0308

.field public static final search_menu_title:I = 0x7f0e045e

.field public static final status_bar_notification_info_overflow:I = 0x7f0e046c

.field public static final str_cancel:I = 0x7f0e0476

.field public static final str_connecting_printer:I = 0x7f0e0480

.field public static final str_connecting_printer_p2p:I = 0x7f0e0481

.field public static final str_error_connecting_printer:I = 0x7f0e04b5

.field public static final str_error_connecting_printer_p2p:I = 0x7f0e04b6

.field public static final str_error_connecting_printer_short:I = 0x7f0e04b7

.field public static final str_error_wifi_connecting_failed:I = 0x7f0e04bc

.field public static final str_error_wifi_connecting_simpleAP:I = 0x7f0e04bd

.field public static final str_goto_wifi_setting:I = 0x7f0e04c2

.field public static final str_need_location_permission:I = 0x7f0e04e1

.field public static final str_need_location_permission_title:I = 0x7f0e04e2

.field public static final str_need_location_setting:I = 0x7f0e04e3

.field public static final str_need_location_setting_title:I = 0x7f0e04e4

.field public static final str_next:I = 0x7f0e04e5

.field public static final str_no_wifi:I = 0x7f0e04eb

.field public static final str_notice_connecting_simpleAP:I = 0x7f0e04ec

.field public static final str_notice_delete_simpleAP:I = 0x7f0e04ed

.field public static final str_notice_duplicated_simpleAP:I = 0x7f0e04ee

.field public static final str_notice_wifi_connected:I = 0x7f0e04ef

.field public static final str_notice_wifi_disconnected:I = 0x7f0e04f0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

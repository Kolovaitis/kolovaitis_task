.class public Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;
.super Ljava/lang/Object;
.source "WifiP2pUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;,
        Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;,
        Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WifiP2pUtils"

.field private static final TIMEOUT_CONNECTIONINFO:I = 0x1388

.field private static instance:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;


# instance fields
.field private volatile groupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

.field private handlerThread:Landroid/os/HandlerThread;

.field private volatile lock:Ljava/lang/Object;

.field private mAppContext:Landroid/content/Context;

.field private mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private volatile status:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 46
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mAppContext:Landroid/content/Context;

    .line 48
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 49
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 54
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->handlerThread:Landroid/os/HandlerThread;

    .line 59
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->lock:Ljava/lang/Object;

    .line 60
    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->IDLE:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->status:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    .line 65
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->groupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 83
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_0

    return-void

    .line 87
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mAppContext:Landroid/content/Context;

    .line 90
    new-instance p1, Landroid/os/HandlerThread;

    const-string v1, "Thread for WiFip2p"

    invoke-direct {p1, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->handlerThread:Landroid/os/HandlerThread;

    .line 91
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    .line 94
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mAppContext:Landroid/content/Context;

    const-string v1, "wifip2p"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 95
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz p1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {p1, v1, v2, v0}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 97
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez p1, :cond_1

    .line 98
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    return-void

    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->groupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object p0
.end method

.method static synthetic access$002(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->groupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object p1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;
    .locals 1

    .line 74
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->instance:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    if-eqz v0, :cond_0

    return-object v0

    .line 78
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->instance:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    .line 79
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->instance:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    return-object p0
.end method

.method private getP2POwnerInfo()Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 4

    .line 188
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 192
    :cond_0
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->groupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 194
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 196
    :try_start_0
    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->REQUEST_GROUPINFO:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->status:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    .line 199
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$1;

    invoke-direct {v3, p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;)V

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->requestGroupInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    :try_start_1
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->lock:Ljava/lang/Object;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 216
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 220
    :goto_0
    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->IDLE:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->status:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    .line 221
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 223
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->groupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object v0

    :catchall_0
    move-exception v1

    .line 221
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method


# virtual methods
.method public disconnect()V
    .locals 3

    .line 303
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    return-void

    .line 307
    :cond_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$2;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    return-void
.end method

.method public getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;
    .locals 3

    .line 118
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getP2POwnerInfo()Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 123
    :cond_0
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getP2PPrinterInfo(Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v2

    if-nez v2, :cond_1

    return-object v1

    .line 127
    :cond_1
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;-><init>(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;)V

    .line 128
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v0

    iput-boolean v0, v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->isGroupOwnerThisDevice:Z

    .line 129
    iget-object v0, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iput-object v0, v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    .line 130
    iget-object v0, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iput-object v0, v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    return-object v1
.end method

.method public getP2PInterfaceInfo()Ljava/net/NetworkInterface;
    .locals 2

    .line 167
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getP2POwnerInfo()Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 173
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v0
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 175
    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    return-object v1
.end method

.method public getP2PPrinterInfo(Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 147
    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->isPrinter(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object p1

    return-object p1

    .line 152
    :cond_1
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 153
    iget-object v2, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->isPrinter(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    return-object v1

    :cond_3
    return-object v0
.end method

.method public isConnectedWiFiP2P()Z
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method resumeThread()V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 232
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->lock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    .line 235
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/IllegalMonitorStateException;->printStackTrace()V

    .line 237
    :goto_0
    monitor-exit v0

    return-void

    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

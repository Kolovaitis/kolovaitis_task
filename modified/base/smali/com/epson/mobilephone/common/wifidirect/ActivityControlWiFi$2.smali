.class Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$2;
.super Ljava/lang/Object;
.source "ActivityControlWiFi.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;)V
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 193
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->registerReciever()V

    .line 196
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result p1

    const-string p2, "ActivityControlWiFi"

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setWifiEnabled return "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    goto :goto_0

    .line 202
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->showOsWifiSettings(Landroid/app/Activity;I)V

    :goto_0
    return-void
.end method

.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;
.super Ljava/lang/Object;
.source "ActivityConnectP2P.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;)V
    .locals 0

    .line 499
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4

    const/4 v0, 0x2

    if-eqz p1, :cond_1

    if-eq p1, v0, :cond_0

    const-string v0, "ActivityConnectP2P"

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "P2PConnect_Operation Fail "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onError()V

    goto :goto_0

    :cond_0
    const-string p1, "ActivityConnectP2P"

    const-string v0, "P2PConnect_Operation BUSY"

    .line 509
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xc

    const-wide/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_1
    const-string v1, "ActivityConnectP2P"

    .line 513
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "P2PConnect_Operation Error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz p1, :cond_3

    .line 519
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$400(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)I

    move-result p1

    const/4 v1, 0x5

    if-ge p1, v1, :cond_2

    .line 521
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    .line 522
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->interruptConnecting()V

    .line 523
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onActivityResult(IILandroid/content/Intent;)V

    .line 524
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$408(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)I

    goto :goto_0

    .line 526
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onError()V

    goto :goto_0

    .line 529
    :cond_3
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onError()V

    :goto_0
    return-void
.end method

.method public onSuccess()V
    .locals 2

    const-string v0, "ActivityConnectP2P"

    const-string v1, "P2PConnect_Operation Success"

    .line 502
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

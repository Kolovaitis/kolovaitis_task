.class final enum Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;
.super Ljava/lang/Enum;
.source "ActivityiPrintConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

.field public static final enum CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

.field public static final enum CONNECTED_OTHERAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

.field public static final enum CONNECTING_REALAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

.field public static final enum CONNECTING_TEMPAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

.field public static final enum CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

.field public static final enum GETTING_GONNECTSTRINGS:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

.field public static final enum IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 61
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const-string v1, "IDLE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const-string v1, "CONNECTING_TEMPAP"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTING_TEMPAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const-string v1, "GETTING_GONNECTSTRINGS"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->GETTING_GONNECTSTRINGS:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const-string v1, "CONNECTING_REALAP"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTING_REALAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const-string v1, "CONNECTED"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const-string v1, "CONNECTED_OTHERAP"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED_OTHERAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const-string v1, "CONNECT_ERROR"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTING_TEMPAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->GETTING_GONNECTSTRINGS:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTING_REALAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    aput-object v1, v0, v6

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED_OTHERAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    aput-object v1, v0, v7

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    aput-object v1, v0, v8

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;
    .locals 1

    .line 61
    const-class v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;
    .locals 1

    .line 61
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    return-object v0
.end method

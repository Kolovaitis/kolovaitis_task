.class public Lcom/epson/mobilephone/common/wifidirect/EventWrapperWiFiDirect;
.super Ljava/lang/Object;
.source "EventWrapperWiFiDirect.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private handled:Z

.field private mContent:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/EventWrapperWiFiDirect;->handled:Z

    if-eqz p1, :cond_0

    .line 18
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/EventWrapperWiFiDirect;->mContent:Ljava/lang/Object;

    return-void

    .line 16
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "null values in Event are not allowed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public getEventContent()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 25
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/EventWrapperWiFiDirect;->handled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/EventWrapperWiFiDirect;->handled:Z

    .line 29
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/EventWrapperWiFiDirect;->mContent:Ljava/lang/Object;

    return-object v0
.end method

.method public hasBeenHandled()Z
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/EventWrapperWiFiDirect;->handled:Z

    return v0
.end method

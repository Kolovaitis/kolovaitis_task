.class public Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SimpleAPInfoDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;
    }
.end annotation


# static fields
.field public static final COLUMN_DEVICEID:Ljava/lang/String; = "deviceid"

.field public static final COLUMN_SSID:Ljava/lang/String; = "ssid"

.field private static final CREATEDB:Ljava/lang/String; = "create table simleapinfo ( ssid text primary key not null, deviceid text not null  );"

.field private static final DB_NAME:Ljava/lang/String;

.field private static final DB_VERSION:I = 0x1

.field public static final TABLE_SIMPLEAPINFO:Ljava/lang/String; = "simleapinfo"

.field private static final TAG:Ljava/lang/String; = "SimpeAPInfoDB"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    invoke-static {}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;->getClassName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;->DB_NAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 106
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;->DB_NAME:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method private static getClassName()Ljava/lang/String;
    .locals 2

    .line 127
    :try_start_0
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 129
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 131
    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "com.epson.iprint.wifidirect.SimpeAPInfoDB"

    return-object v0
.end method

.method public static getSimpleAPInfoDB(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;
    .locals 9

    .line 51
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p0

    const-string v0, "ssid"

    const-string v1, "deviceid"

    .line 55
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    .line 56
    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v5, v0

    const-string v2, "simleapinfo"

    const-string v4, "ssid=?"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    .line 58
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 59
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "SimpeAPInfoDB"

    const-string v1, "getSimpleAPInfoDB found info."

    .line 60
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ssid"

    .line 62
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const-string v1, "deviceid"

    .line 63
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 65
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 68
    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;

    invoke-direct {v2}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;-><init>()V

    .line 69
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->ssid:Ljava/lang/String;

    .line 70
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->printerName:Ljava/lang/String;

    .line 72
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 75
    :goto_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-object v2
.end method

.method public static updateSimpleAPInfoDB(Landroid/content/Context;Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;)Z
    .locals 3

    const-string v0, "SimpeAPInfoDB"

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSimpleAPInfoDB ssid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->ssid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;-><init>(Landroid/content/Context;)V

    .line 92
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p0

    .line 94
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "ssid"

    .line 95
    iget-object v2, p1, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->ssid:Ljava/lang/String;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "deviceid"

    .line 96
    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->printerName:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "simleapinfo"

    const/4 v1, 0x0

    .line 98
    invoke-virtual {p0, p1, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 100
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    const/4 p0, 0x1

    return p0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "create table simleapinfo ( ssid text primary key not null, deviceid text not null  );"

    .line 114
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    return-void
.end method

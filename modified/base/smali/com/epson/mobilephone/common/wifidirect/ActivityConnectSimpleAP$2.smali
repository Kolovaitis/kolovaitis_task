.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;
.super Landroid/os/AsyncTask;
.source "ActivityConnectSimpleAP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onConnectedWiFi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)V
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 316
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    .line 320
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->waitConnected()V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 316
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6

    .line 328
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->setDefaultNetworkSimpleAp()V

    .line 331
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-boolean p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->showConnectedTip:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 332
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    sget v1, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_notice_wifi_connected:I

    invoke-virtual {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 335
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-boolean p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->needInfo:Z

    if-nez p1, :cond_1

    .line 340
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->closeWaitingDialog()V

    .line 342
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 343
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setResult(I)V

    .line 344
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->setLastDetailResult(I)V

    .line 345
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->finish()V

    return-void

    .line 350
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    if-eqz p1, :cond_2

    const-string p1, "ActivityConnectSimpleAP"

    const-string v1, "Already called onConnect()"

    .line 351
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurP2PMacAdder()Ljava/lang/String;

    move-result-object p1

    .line 356
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddrP2P(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->expectedPrtMacAddr:Ljava/lang/String;

    .line 358
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->PRINTER_FINDING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 359
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    iget-object v2, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget v3, v3, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->timeout:I

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;-><init>(Landroid/os/Handler;III)V

    iput-object v1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    .line 360
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v1, v0}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

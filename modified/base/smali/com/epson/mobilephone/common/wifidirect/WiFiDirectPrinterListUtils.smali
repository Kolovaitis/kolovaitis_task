.class public Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;
.super Ljava/lang/Object;
.source "WiFiDirectPrinterListUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;,
        Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;,
        Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WiFiDirectPrinterListUtils"


# instance fields
.field private curConnectInfo:Ljava/lang/String;

.field private innerPrinterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

.field private mAppContext:Landroid/content/Context;

.field private p2pPrinterMacAddr:Ljava/lang/String;

.field private uiPrinterList:Ljava/util/AbstractList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/AbstractList<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private wiFiControl:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/AbstractList;Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/AbstractList<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 20
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->mAppContext:Landroid/content/Context;

    .line 22
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

    .line 24
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->wiFiControl:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    .line 27
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->innerPrinterList:Ljava/util/ArrayList;

    .line 30
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->uiPrinterList:Ljava/util/AbstractList;

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->mAppContext:Landroid/content/Context;

    .line 77
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->mAppContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->wiFiControl:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    .line 78
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->innerPrinterList:Ljava/util/ArrayList;

    .line 79
    iput-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->uiPrinterList:Ljava/util/AbstractList;

    .line 80
    iput-object p3, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

    .line 81
    iput-object p4, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->curConnectInfo:Ljava/lang/String;

    .line 82
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->clearPrinterInfoList()V

    return-void
.end method

.method public static getCurPrinterString(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p2, :cond_0

    return-object p1

    .line 129
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    .line 132
    invoke-virtual {p0, v0}, Landroid/app/Activity;->checkSelfPermission(Ljava/lang/String;)I

    move-result p0

    if-eqz p0, :cond_1

    return-object p1

    :cond_1
    return-object p2
.end method

.method private getType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;
    .locals 2

    .line 323
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->wiFiControl:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "USB"

    .line 325
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 326
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->USB:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    return-object p1

    .line 329
    :cond_0
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->LOCAL:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    return-object p1

    .line 331
    :cond_1
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->WIFIDIRECT:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    return-object p1
.end method


# virtual methods
.method public addPrinter(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 152
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiDirectPrinterListUtils$PRINTER_TYPE:[I

    invoke-direct {p0, p3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->getType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    packed-switch v0, :pswitch_data_0

    const-string p1, "WiFiDirectPrinterListUtils"

    const-string p2, "Un known Type"

    .line 317
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 313
    :pswitch_0
    iget-object p3, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

    invoke-interface {p3, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;->addPrinter(Ljava/lang/Object;I)V

    goto/16 :goto_6

    .line 209
    :pswitch_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->innerPrinterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;

    .line 210
    iget-object v5, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->macAddr:Ljava/lang/String;

    if-eqz v5, :cond_a

    .line 211
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    goto/16 :goto_1

    .line 216
    :cond_1
    sget-object v6, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->WIFIDIRECT:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    iget-object v7, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->type:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    invoke-virtual {v6, v7}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eqz v5, :cond_0

    .line 218
    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 220
    sget-object v5, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    invoke-static {p3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    const-string v4, "WiFiDirectPrinterListUtils"

    const-string v5, "Invalid Type"

    .line 252
    invoke-static {v4, v5}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 243
    :pswitch_2
    iget-object v5, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->curConnectInfo:Ljava/lang/String;

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v1, "WiFiDirectPrinterListUtils"

    .line 244
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found Duplicate Printer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->ssid:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0

    .line 224
    :pswitch_3
    iget-object v5, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->ssid:Ljava/lang/String;

    iget-object v6, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->curConnectInfo:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "WiFiDirectPrinterListUtils"

    .line 225
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Replace Duplicate Printer:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->ssid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "->"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v5, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->uiPrinterList:Ljava/util/AbstractList;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->printer:Ljava/lang/Object;

    invoke-virtual {v5, v4}, Ljava/util/AbstractList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    if-eq v2, v4, :cond_2

    .line 229
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

    invoke-interface {v1, v4, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;->replacePrinter(ILjava/lang/Object;I)V

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_2
    const-string v4, "WiFiDirectPrinterListUtils"

    .line 232
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Illegal curPrinterList2 (Maybe replaced):"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 259
    :cond_3
    invoke-static {v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pPtrAddr2PtrAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v5, :cond_4

    .line 261
    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    :cond_4
    if-eqz v6, :cond_0

    .line 262
    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 264
    :cond_5
    iget-object v6, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->wiFiControl:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    invoke-virtual {v6, v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getCurConnectInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 266
    sget-object v6, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    iget-object v7, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->wiFiControl:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    invoke-static {p3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    const/4 v7, 0x2

    if-eq v6, v7, :cond_6

    move v3, v1

    goto/16 :goto_2

    .line 268
    :cond_6
    iget-object v6, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->curConnectInfo:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v5, "WiFiDirectPrinterListUtils"

    .line 270
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Replace Duplicate Printer:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->ip:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "->"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget-object v5, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->uiPrinterList:Ljava/util/AbstractList;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->printer:Ljava/lang/Object;

    invoke-virtual {v5, v4}, Ljava/util/AbstractList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    if-eq v2, v4, :cond_7

    .line 274
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

    invoke-interface {v0, v4, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;->replacePrinter(ILjava/lang/Object;I)V

    goto :goto_2

    :cond_7
    const-string v4, "WiFiDirectPrinterListUtils"

    .line 277
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Illegal curPrinterList3 (Maybe replaced):"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "WiFiDirectPrinterListUtils"

    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Found Duplicate Printer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->ip:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    const-string v0, "WiFiDirectPrinterListUtils"

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Found Duplicate Printer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->ip:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_a
    :goto_1
    const-string v4, "WiFiDirectPrinterListUtils"

    const-string v5, "Invalid MacAddress"

    .line 212
    invoke-static {v4, v5}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move v3, v1

    :goto_2
    if-eqz v3, :cond_c

    .line 302
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

    invoke-interface {v0, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;->addPrinter(Ljava/lang/Object;I)V

    .line 306
    :cond_c
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->innerPrinterList:Ljava/util/ArrayList;

    new-instance v7, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;

    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->WIFIDIRECT:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    const/4 v4, 0x0

    move-object v0, v7

    move-object v1, p0

    move-object v3, p4

    move-object v5, p3

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;-><init>(Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_4
    if-eqz p4, :cond_14

    .line 158
    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    goto/16 :goto_5

    .line 164
    :cond_d
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->p2pPrinterMacAddr:Ljava/lang/String;

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string p1, "WiFiDirectPrinterListUtils"

    .line 165
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Ignore Printer. This printer communicates via P2P:"

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 169
    :cond_e
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->innerPrinterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_f
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;

    .line 171
    sget-object v5, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->LOCAL:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    iget-object v6, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->type:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    invoke-virtual {v5, v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    goto :goto_3

    .line 175
    :cond_10
    iget-object v5, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->macAddr:Ljava/lang/String;

    if-eqz p4, :cond_f

    .line 176
    invoke-virtual {p4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    const-string v5, "WiFiDirectPrinterListUtils"

    .line 177
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Replace Duplicate Printer:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->ssid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "->"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v5, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->uiPrinterList:Ljava/util/AbstractList;

    iget-object v4, v4, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;->printer:Ljava/lang/Object;

    invoke-virtual {v5, v4}, Ljava/util/AbstractList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    if-eq v2, v4, :cond_11

    .line 181
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

    invoke-interface {v0, v4, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;->replacePrinter(ILjava/lang/Object;I)V

    const/4 v1, 0x0

    goto :goto_4

    :cond_11
    const-string v4, "WiFiDirectPrinterListUtils"

    .line 184
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Illegal curPrinterList1 (Maybe replaced):"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_12
    :goto_4
    if-eqz v1, :cond_13

    .line 194
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->listController:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;

    invoke-interface {v0, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;->addPrinter(Ljava/lang/Object;I)V

    .line 198
    :cond_13
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->innerPrinterList:Ljava/util/ArrayList;

    new-instance v7, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;

    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->LOCAL:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    const/4 v5, 0x0

    move-object v0, v7

    move-object v1, p0

    move-object v3, p4

    move-object v4, p3

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PrinterInfo;-><init>(Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_14
    :goto_5
    const-string p1, "WiFiDirectPrinterListUtils"

    const-string p2, "Invalid MacAddress"

    .line 159
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_6
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public clearPrinterInfoList()V
    .locals 2

    const-string v0, "WiFiDirectPrinterListUtils"

    const-string v1, "clearPrinterInfoList()"

    .line 86
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->innerPrinterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 89
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->updateP2pPrinterMacAddr()V

    return-void
.end method

.method public updateP2pPrinterMacAddr()V
    .locals 4

    const-string v0, "WiFiDirectPrinterListUtils"

    const-string v1, "updateP2pPrinterMacAddr()"

    .line 93
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 96
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->p2pPrinterMacAddr:Ljava/lang/String;

    goto :goto_1

    .line 98
    :cond_0
    iget-boolean v2, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->isGroupOwnerThisDevice:Z

    if-eqz v2, :cond_1

    .line 100
    iget-object v2, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object v3, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->p2pPrinterMacAddr:Ljava/lang/String;

    goto :goto_0

    .line 103
    :cond_1
    iget-object v2, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object v3, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddrP2P(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->p2pPrinterMacAddr:Ljava/lang/String;

    .line 107
    :goto_0
    iget-object v2, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->mAppContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->isExcludedMacAddress(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->p2pPrinterMacAddr:Ljava/lang/String;

    :cond_2
    :goto_1
    return-void
.end method

.class public Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;
.super Ljava/lang/Object;
.source "WiFiDirectManager.java"


# static fields
.field public static final DEVICE_TYPE_OTHER:Ljava/lang/String; = "other"

.field public static final DEVICE_TYPE_PRINTER:Ljava/lang/String; = "printer"

.field public static final DEVICE_TYPE_SCANNER:Ljava/lang/String; = "scanner"

.field private static final TAG:Ljava/lang/String; = "WiFiDirectManager"

.field public static final TIMEOUT_CONNECTING:I = 0x5a

.field public static final TIMEOUT_WIFIDIRECTMODE:I = 0x78

.field static bCheckDebuggable:Z = false

.field static bSimpleAPCreated:Z = false


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static checkDebuggable(Landroid/content/Context;)V
    .locals 1

    .line 58
    sget-boolean v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->bCheckDebuggable:Z

    if-nez v0, :cond_0

    .line 59
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable(Landroid/content/Context;)Z

    const/4 p0, 0x1

    .line 60
    sput-boolean p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->bCheckDebuggable:Z

    :cond_0
    return-void
.end method

.method public static connect(Landroid/app/Activity;Ljava/lang/String;I)Z
    .locals 3

    .line 223
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->checkDebuggable(Landroid/content/Context;)V

    const-string v0, "WiFiDirectManager"

    const-string v1, "connect()"

    .line 224
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    .line 237
    new-instance p1, Landroid/content/Intent;

    const-class v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {p1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 233
    :cond_0
    new-instance p1, Landroid/content/Intent;

    const-class v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-direct {p1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_0
    const-string v2, "com.epson.iprint.wifidirect.apname"

    .line 242
    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.create"

    const/4 v2, 0x0

    .line 243
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.showerror"

    .line 244
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.timeout"

    const/16 v2, 0x5a

    .line 245
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.needinfo"

    .line 246
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 248
    invoke-virtual {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return v1
.end method

.method public static connectSimpleAP(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 3

    .line 499
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->checkDebuggable(Landroid/content/Context;)V

    const-string v0, "WiFiDirectManager"

    const-string v1, "connectSimpleAP()"

    .line 500
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 506
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.epson.iprint.wifidirect.apname"

    .line 507
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "com.epson.iprint.wifidirect.appass"

    .line 508
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "com.epson.iprint.wifidirect.create"

    const/4 p2, 0x1

    .line 509
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "com.epson.iprint.wifidirect.showerror"

    const/4 v1, 0x0

    .line 510
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "com.epson.iprint.wifidirect.timeout"

    const/16 v2, 0x5a

    .line 511
    invoke-virtual {v0, p1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "com.epson.iprint.wifidirect.needinfo"

    .line 512
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 514
    invoke-virtual {p0, v0, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return p2
.end method

.method public static disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .line 306
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->checkDebuggable(Landroid/content/Context;)V

    .line 309
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 314
    :cond_0
    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_6

    .line 319
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 324
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    return v1

    .line 329
    :cond_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v2

    .line 332
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    return v1

    .line 336
    :cond_2
    invoke-virtual {v2, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-ne v0, v4, :cond_3

    return v1

    :cond_3
    const-string v4, "WiFiDirectManager"

    .line 341
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "disconnect() deviceType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " printerIp = "

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0x78

    .line 345
    invoke-static {p2, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->setAutoGoTimeout(Ljava/lang/String;I)I

    .line 348
    sget-boolean p1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->bSimpleAPCreated:Z

    if-eqz p1, :cond_4

    .line 350
    sput-boolean v1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->bSimpleAPCreated:Z

    .line 351
    invoke-virtual {v2, p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->invalidateSimpleAP(Landroid/content/Context;I)Z

    move-result p0

    if-nez p0, :cond_5

    return v1

    .line 356
    :cond_4
    invoke-virtual {v2, p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->disableSimpleAP(Landroid/content/Context;I)Z

    move-result p0

    if-nez p0, :cond_5

    return v1

    :cond_5
    return v3

    :cond_6
    return v3
.end method

.method public static disconnectSimpleAP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .line 527
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->checkDebuggable(Landroid/content/Context;)V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 534
    :cond_0
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 537
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    .line 542
    :cond_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v1

    .line 545
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v0

    .line 549
    :cond_2
    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    return v0

    :cond_3
    const-string v3, "WiFiDirectManager"

    .line 554
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "disconnectSimpleAP() ssid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " printerIp = "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0x78

    .line 558
    invoke-static {p2, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->setAutoGoTimeout(Ljava/lang/String;I)I

    .line 561
    invoke-virtual {v1, p0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->disableSimpleAP(Landroid/content/Context;I)Z

    move-result p0

    if-nez p0, :cond_4

    return v0

    :cond_4
    const/4 p0, 0x1

    return p0
.end method

.method public static enableWiFi(Landroid/app/Activity;I)V
    .locals 3

    .line 150
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.epson.iprint.wifidirect.progress"

    const/4 v2, 0x1

    .line 153
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.epson.iprint.wifidirect.wifisettings"

    const/4 v2, 0x0

    .line 154
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "PREFS_WIFI"

    const/4 v1, 0x0

    .line 395
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 397
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 402
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AUTOCONNECT_SSID."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v2, "printer"

    .line 406
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string p1, "AUTOCONNECT_SSID.other"

    .line 409
    invoke-interface {p0, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v2, "other"

    .line 410
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "AUTOCONNECT_SSID.printer"

    .line 413
    invoke-interface {p0, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    .line 420
    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->rebuildSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_3
    return-object v1

    :cond_4
    :goto_1
    const-string p0, "WiFiDirectManager"

    const-string p1, "getConnectInfo Ilegal param"

    .line 398
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getConnectString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 212
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    move-result-object p0

    invoke-virtual {p0, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getEstimateSSID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getConnectType(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;
    .locals 0

    .line 198
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object p0

    return-object p0
.end method

.method public static getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 205
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getCurConnectInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getCurSSID(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    .line 137
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getLastDetailError()I
    .locals 1

    .line 580
    invoke-static {}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getLastDetailResult()I

    move-result v0

    return v0
.end method

.method public static isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 0

    .line 179
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->checkDebuggable(Landroid/content/Context;)V

    .line 181
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p0, 0x0

    return p0

    .line 187
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isNeedConnect(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static isSimpleAP(Landroid/content/Context;)Z
    .locals 0

    .line 88
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->checkDebuggable(Landroid/content/Context;)V

    .line 91
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p0

    .line 93
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object p0

    .line 95
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static isWifiDirectFY13(Landroid/content/Context;)Z
    .locals 0

    .line 106
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->checkDebuggable(Landroid/content/Context;)V

    .line 109
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p0

    .line 111
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object p0

    .line 113
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAPFY13WiFiDirect(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static isWifiDirectP2P(Landroid/content/Context;)Z
    .locals 0

    .line 125
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object p0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->isConnectedWiFiP2P()Z

    move-result p0

    return p0
.end method

.method public static isWifiEnabled(Landroid/content/Context;)Z
    .locals 1

    const-string v0, "wifi"

    .line 73
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/wifi/WifiManager;

    .line 75
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result p0

    return p0
.end method

.method public static reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z
    .locals 4

    .line 263
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->checkDebuggable(Landroid/content/Context;)V

    const-string v0, "WiFiDirectManager"

    const-string v1, "reconnect()"

    .line 264
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string p0, "WiFiDirectManager"

    const-string p1, "getConnectInfo == null"

    .line 272
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    .line 276
    :cond_0
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 278
    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result p1

    aget p1, v2, p1

    const/4 v2, 0x1

    if-eq p1, v2, :cond_1

    .line 284
    new-instance p1, Landroid/content/Intent;

    const-class v3, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {p1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 280
    :cond_1
    new-instance p1, Landroid/content/Intent;

    const-class v3, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-direct {p1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_0
    const-string v3, "com.epson.iprint.wifidirect.apname"

    .line 289
    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.epson.iprint.wifidirect.create"

    .line 290
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.showerror"

    .line 291
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.epson.iprint.wifidirect.timeout"

    const/16 v1, 0x5a

    .line 292
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 294
    invoke-virtual {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return v2
.end method

.method public static resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    const-string v0, "WiFiDirectManager"

    .line 472
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resetConnectInfo :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 475
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "PREFS_WIFI"

    const/4 v1, 0x0

    .line 481
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    .line 482
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    .line 483
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AUTOCONNECT_SSID."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 484
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_1
    :goto_0
    const-string p0, "WiFiDirectManager"

    const-string p1, "resetConnectInfo Ilegal param"

    .line 476
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static searchWiFiDirectPrinter(Landroid/content/Context;Landroid/os/Handler;II)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
    .locals 1

    .line 375
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    move-result-object p0

    const/4 v0, 0x3

    .line 377
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->start(ILandroid/os/Handler;II)Z

    move-result p1

    if-eqz p1, :cond_0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static setConnectInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    const-string p0, "WiFiDirectManager"

    const-string p1, "SSID Ilegal param"

    .line 437
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 441
    :cond_0
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->rebuildSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "WiFiDirectManager"

    .line 443
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setConnectInfo :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    .line 445
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, "PREFS_WIFI"

    const/4 v1, 0x0

    .line 451
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 452
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 453
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AUTOCONNECT_SSID."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 454
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 457
    new-instance p2, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;-><init>()V

    .line 458
    iput-object p1, p2, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->ssid:Ljava/lang/String;

    .line 459
    iput-object p3, p2, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->printerName:Ljava/lang/String;

    .line 460
    invoke-static {p0, p2}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;->updateSimpleAPInfoDB(Landroid/content/Context;Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;)Z

    return-void

    :cond_2
    :goto_0
    const-string p0, "WiFiDirectManager"

    const-string p1, "setConnectInfo Ilegal param"

    .line 446
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static setFlagSimpleAPCreated()V
    .locals 1

    const/4 v0, 0x1

    .line 573
    sput-boolean v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->bSimpleAPCreated:Z

    return-void
.end method

.method public static setPriorityToSimpleAP(Landroid/content/Context;Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 168
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object p0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->setDefaultNetworkSimpleAp()V

    goto :goto_0

    .line 170
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object p0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->resetDefaultNetwork()V

    :goto_0
    return-void
.end method

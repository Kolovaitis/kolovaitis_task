.class final enum Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;
.super Ljava/lang/Enum;
.source "ActivityConnectBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

.field public static final enum IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

.field public static final enum PRINTER_FINDING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

.field public static final enum WIFI_CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

.field public static final enum WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

.field public static final enum WIFI_SCANNING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 65
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    const-string v1, "IDLE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    const-string v1, "WIFI_SCANNING"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_SCANNING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    const-string v1, "WIFI_CONNECTING"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    const-string v1, "WIFI_CONNECTED"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    const-string v1, "PRINTER_FINDING"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->PRINTER_FINDING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_SCANNING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->PRINTER_FINDING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    aput-object v1, v0, v6

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;
    .locals 1

    .line 65
    const-class v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;
    .locals 1

    .line 65
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    return-object v0
.end method

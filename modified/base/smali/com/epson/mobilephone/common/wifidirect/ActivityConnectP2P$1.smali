.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$1;
.super Ljava/lang/Object;
.source "ActivityConnectP2P.java"

# interfaces
.implements Landroid/arch/lifecycle/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/arch/lifecycle/Observer<",
        "Ljava/util/Deque<",
        "[",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onChanged(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 106
    check-cast p1, Ljava/util/Deque;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$1;->onChanged(Ljava/util/Deque;)V

    return-void
.end method

.method public onChanged(Ljava/util/Deque;)V
    .locals 2
    .param p1    # Ljava/util/Deque;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Deque<",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 113
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 115
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 116
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 119
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$100(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 123
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 124
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$200(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

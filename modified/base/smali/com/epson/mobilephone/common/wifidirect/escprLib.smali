.class public Lcom/epson/mobilephone/common/wifidirect/escprLib;
.super Ljava/lang/Object;
.source "escprLib.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;,
        Lcom/epson/mobilephone/common/wifidirect/escprLib$AutoGoTimeoutInfo;
    }
.end annotation


# static fields
.field public static final EPS_ERR_COMM_ERROR:I = -0x44c

.field public static final EPS_ERR_LIB_INTIALIZED:I = -0x41a

.field public static final EPS_ERR_NONE:I = 0x0

.field public static final EPS_ERR_PRINTER_NOT_FOUND:I = -0x514

.field public static final EPS_FIND_CANCELED:I = 0x2a

.field public static final EPS_PROTOCOL_LPR:I = 0x40

.field public static final EPS_PROTOCOL_NET:I = 0xc0

.field public static final EPS_PROTOCOL_RAW:I = 0x80

.field public static final PRINTER_FRIENDLYNAME:Ljava/lang/String; = "friendlyname"

.field public static final PRINTER_ID:Ljava/lang/String; = "id"

.field public static final PRINTER_IP:Ljava/lang/String; = "ip"

.field public static final PRINTER_MACADDRESS:Ljava/lang/String; = "mac"

.field public static final PRINTER_MANIFACTURER:Ljava/lang/String; = "manufacturer"

.field public static final PRINTER_NAME:Ljava/lang/String; = "name"

.field public static final PRINTER_PROTCOL:Ljava/lang/String; = "protcol"

.field public static final PRINTER_SERIAL_NO:Ljava/lang/String; = "serial_no"

.field public static final PRINTER_SUPPORTFUNC:Ljava/lang/String; = "supportfunc"


# instance fields
.field private debugString:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field mIdFound:I

.field private mIsSearch:Z

.field private printerList:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 13
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "begin load escpr-wifidirect lib"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v0, "escpr-wifidirect"

    .line 14
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 15
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "load escpr-wifidirect lib finish"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private findPrinterCB()V
    .locals 6

    .line 138
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->printerList:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EXTRACT Printer information: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->printerList:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->printerList:Ljava/lang/String;

    const-string v1, "\\|\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 142
    iget-boolean v1, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->mIsSearch:Z

    if-eqz v1, :cond_1

    .line 143
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 144
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 145
    iget v2, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->mIdFound:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 146
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, ""

    .line 149
    array-length v4, v0

    const/16 v5, 0xa

    if-ne v4, v5, :cond_0

    const/16 v4, 0x9

    .line 150
    aget-object v5, v0, v4

    if-eqz v5, :cond_0

    .line 151
    aget-object v3, v0, v4

    :cond_0
    const-string v4, "supportfunc"

    const/4 v5, 0x1

    .line 154
    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "protcol"

    const/4 v5, 0x2

    .line 155
    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "manufacturer"

    const/4 v5, 0x3

    .line 156
    aget-object v5, v0, v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "name"

    const/4 v5, 0x4

    .line 157
    aget-object v5, v0, v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ip"

    const/4 v5, 0x5

    .line 158
    aget-object v5, v0, v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "friendlyname"

    const/4 v5, 0x6

    .line 159
    aget-object v5, v0, v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "mac"

    const/4 v5, 0x7

    .line 160
    aget-object v5, v0, v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "id"

    const/16 v5, 0x8

    .line 161
    aget-object v0, v0, v5

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "serial_no"

    .line 162
    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 164
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    const-string v0, ""

    .line 168
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->printerList:Ljava/lang/String;

    return-void
.end method

.method public static getMd5([B)[B
    .locals 2

    const/4 v0, 0x1

    .line 196
    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v1, v0, v1

    :try_start_0
    const-string v1, "MD5"

    .line 201
    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 202
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 203
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 205
    invoke-virtual {p0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private javaDebugCB()V
    .locals 2

    .line 174
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->debugString:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    .line 175
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->debugString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public native epsWrapperCancelFindPrinter()I
.end method

.method public native epsWrapperFindPrinter(II)I
.end method

.method public native epsWrapperGetAutoGoTimeOutValues([CLcom/epson/mobilephone/common/wifidirect/escprLib$AutoGoTimeoutInfo;)I
.end method

.method public native epsWrapperGetConnectStrings([BLcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;)I
.end method

.method public native epsWrapperGetSetupConnectStrings(Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;)I
.end method

.method public native epsWrapperInitDriver(I)I
.end method

.method public native epsWrapperReleaseDriver()I
.end method

.method public native epsWrapperSetAutoGoTimeOut([CI)I
.end method

.method public native epsWrapperStartWifiDirect([C)I
.end method

.method public setHanlder(Landroid/os/Handler;I)V
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->mHandler:Landroid/os/Handler;

    .line 180
    iput p2, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->mIdFound:I

    return-void
.end method

.method public setSearchStt(Z)V
    .locals 0

    .line 184
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/wifidirect/escprLib;->mIsSearch:Z

    return-void
.end method

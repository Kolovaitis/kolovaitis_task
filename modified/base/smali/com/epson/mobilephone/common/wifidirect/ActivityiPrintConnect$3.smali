.class synthetic Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$3;
.super Ljava/lang/Object;
.source "ActivityiPrintConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityiPrintConnect$Status:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 203
    invoke-static {}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->values()[Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$3;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityiPrintConnect$Status:[I

    :try_start_0
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$3;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityiPrintConnect$Status:[I

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$3;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityiPrintConnect$Status:[I

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$3;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityiPrintConnect$Status:[I

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED_OTHERAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$3;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityiPrintConnect$Status:[I

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method

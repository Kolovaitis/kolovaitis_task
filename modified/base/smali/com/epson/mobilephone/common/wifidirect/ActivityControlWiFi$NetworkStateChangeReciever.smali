.class Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;
.super Landroid/content/BroadcastReceiver;
.source "ActivityControlWiFi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NetworkStateChangeReciever"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;)V
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .line 278
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "android.net.wifi.WIFI_STATE_CHANGED"

    .line 280
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const-string p1, "wifi_state"

    const/4 v0, 0x4

    .line 284
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    const-string p2, "ActivityControlWiFi"

    const-string v0, "WiFi State Change : wifiState = %d"

    .line 285
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_0
    const-string v0, "android.net.wifi.STATE_CHANGE"

    .line 287
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "networkInfo"

    .line 291
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/net/NetworkInfo;

    const-string p2, "ActivityControlWiFi"

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Network State Changed:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget-object p2, p2, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 295
    sget-object p2, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$3;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 303
    :pswitch_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;->interrunpt()V

    goto/16 :goto_0

    .line 299
    :pswitch_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;->interrunpt()V

    .line 300
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->onOK()V

    goto :goto_0

    :cond_1
    const-string v0, "android.net.wifi.supplicant.STATE_CHANGE"

    .line 307
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "newState"

    .line 311
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/SupplicantState;

    const-string p2, "ActivityControlWiFi"

    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Supplicant State Changed: State = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/net/wifi/SupplicantState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    sget-object p2, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$3;->$SwitchMap$android$net$wifi$SupplicantState:[I

    invoke-virtual {p1}, Landroid/net/wifi/SupplicantState;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 333
    :pswitch_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;->start()V

    goto :goto_0

    .line 329
    :pswitch_3
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->onOK()V

    goto :goto_0

    .line 325
    :pswitch_4
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;->interrunpt()V

    goto :goto_0

    .line 317
    :pswitch_5
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget p2, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scancount:I

    add-int/2addr p2, v1

    iput p2, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scancount:I

    .line 318
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scancount:I

    const/4 p2, 0x5

    if-le p1, p2, :cond_2

    .line 319
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->onOK()V

    goto :goto_0

    .line 321
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;->start()V

    :cond_3
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.class Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;
.super Ljava/lang/Object;
.source "MacAddrFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MacAddressEntry"
.end annotation


# instance fields
.field end:Ljava/lang/String;

.field start:Ljava/lang/String;

.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;


# direct methods
.method public constructor <init>(Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->this$0:Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->start:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->end:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 41
    instance-of v0, p1, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 42
    check-cast p1, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;

    .line 43
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->start:Ljava/lang/String;

    iget-object v2, p1, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->start:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->end:Ljava/lang/String;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->end:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    return v1
.end method

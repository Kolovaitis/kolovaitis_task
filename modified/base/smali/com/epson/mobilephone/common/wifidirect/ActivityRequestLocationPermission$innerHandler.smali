.class Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler;
.super Landroid/os/Handler;
.source "ActivityRequestLocationPermission.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "innerHandler"
.end annotation


# instance fields
.field private final mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V
    .locals 1

    .line 240
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 241
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler;->mActivity:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .line 246
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 248
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 250
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    if-eqz p1, :cond_2

    .line 252
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->access$200(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_need_location_setting_title:I

    .line 257
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->access$400(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_need_location_setting:I

    .line 258
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_next:I

    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler$2;

    invoke-direct {v2, p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler;Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V

    .line 259
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_cancel:I

    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler$1;

    invoke-direct {v2, p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$innerHandler;Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V

    .line 266
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 274
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 275
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 276
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 279
    :cond_1
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onSuccess()V

    :cond_2
    :goto_0
    return-void
.end method

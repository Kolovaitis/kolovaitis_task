.class public abstract Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;
.super Landroid/support/v7/app/AppCompatActivity;
.source "ActivityConnectBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;,
        Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;
    }
.end annotation


# static fields
.field public static final APNAME:Ljava/lang/String; = "com.epson.iprint.wifidirect.apname"

.field public static final APPASS:Ljava/lang/String; = "com.epson.iprint.wifidirect.appass"

.field public static final CONNECTING_TIMEOUT:I = 0x1e

.field public static final CREATE:Ljava/lang/String; = "com.epson.iprint.wifidirect.create"

.field protected static final ID_ENABLED_LOCATIONSETTINGS:I = 0x2

.field protected static final ID_ENABLED_WIFI:I = 0x1

.field public static final ID_SHOWWIFISETTINGS:I = 0x4

.field public static final ID_SHOWWIFISETTINGS_PROFILEFAILED:I = 0x5

.field public static final NEEDINFO:Ljava/lang/String; = "com.epson.iprint.wifidirect.needinfo"

.field public static final RESULT_ERROR:I = 0x2

.field public static final RESULT_USERCANCEL:I = 0x1

.field public static final SHOWERROR:Ljava/lang/String; = "com.epson.iprint.wifidirect.showerror"

.field public static final SHOWTIP:Ljava/lang/String; = "com.epson.iprint.wifidirect.showconnecttip"

.field private static final TAG:Ljava/lang/String; = "ActivityConnectBase"

.field public static final TIMEOUT:Ljava/lang/String; = "com.epson.iprint.wifidirect.timeout"

.field private static sLastDetailResult:I = -0x1


# instance fields
.field connectingObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;

.field expectedPrtMacAddr:Ljava/lang/String;

.field mWifiManager:Landroid/net/wifi/WifiManager;

.field needInfo:Z

.field searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

.field showConnectedTip:Z

.field showErrorDlg:Z

.field protected status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

.field targetSsid:Ljava/lang/String;

.field taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

.field timeout:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 17
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x0

    .line 70
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 75
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;)V

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->connectingObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;

    .line 80
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 85
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    const-string v0, ""

    .line 90
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->targetSsid:Ljava/lang/String;

    const/4 v0, 0x0

    .line 92
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->showErrorDlg:Z

    .line 94
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->needInfo:Z

    const/4 v0, 0x1

    .line 95
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->showConnectedTip:Z

    const/16 v0, 0x1e

    .line 96
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->timeout:I

    const-string v0, ""

    .line 97
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->expectedPrtMacAddr:Ljava/lang/String;

    .line 103
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    return-void
.end method

.method public static getLastDetailResult()I
    .locals 1

    .line 109
    sget v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->sLastDetailResult:I

    return v0
.end method

.method public static setLastDetailResult(I)V
    .locals 0

    .line 113
    sput p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->sLastDetailResult:I

    return-void
.end method


# virtual methods
.method abstract closeWaitingDialog()V
.end method

.method interrupt()V
    .locals 1

    .line 212
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 215
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    const/4 v0, 0x0

    .line 217
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 220
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->interruptConnecting()V

    .line 222
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->interruptFindingPrinter()V

    return-void
.end method

.method protected interruptConnecting()V
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->connectingObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;->interrupt()V

    return-void
.end method

.method interruptFindingPrinter()V
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->cancel()V

    const/4 v0, 0x0

    .line 231
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 134
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_1

    .line 145
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->closeWaitingDialog()V

    const/4 p1, 0x0

    .line 146
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->setResult(I)V

    .line 147
    invoke-static {p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->setLastDetailResult(I)V

    .line 148
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->finish()V

    goto :goto_0

    .line 141
    :cond_1
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->onEnabledWifi()V

    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    const-string v0, "ActivityConnectBase"

    const-string v1, "onBackPressed()"

    .line 201
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onBackPressed()V

    .line 204
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->interrupt()V

    return-void
.end method

.method onConnectedPrinter(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "ActivityConnectBase"

    const-string v1, "finish():RESULT_OK"

    .line 247
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->closeWaitingDialog()V

    const/4 v0, -0x1

    .line 253
    invoke-virtual {p0, v0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->setResult(ILandroid/content/Intent;)V

    .line 254
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->setLastDetailResult(I)V

    .line 255
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 118
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 121
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 p1, 0x1

    .line 125
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->requestWindowFeature(I)Z

    .line 128
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "ActivityConnectBase"

    const-string v1, "onDestroy()"

    .line 192
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 195
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->interrupt()V

    return-void
.end method

.method protected abstract onEnabledWifi()V
.end method

.method abstract onError()V
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "ActivityConnectBase"

    const-string v1, "onPause()"

    .line 174
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 178
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcDispatchUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    const-string v0, "ActivityConnectBase"

    const-string v1, "clearFlags : FLAG_KEEP_SCREEN_ON "

    .line 180
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "ActivityConnectBase"

    const-string v1, "onResume()"

    .line 162
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    const/4 v0, 0x0

    .line 166
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcDispatchUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    const-string v0, "ActivityConnectBase"

    const-string v1, "addFlags : FLAG_KEEP_SCREEN_ON "

    .line 168
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "ActivityConnectBase"

    const-string v1, "onStop()"

    .line 186
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStop()V

    return-void
.end method

.method setupObserver()V
    .locals 2

    .line 273
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->connectingObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;

    iget v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->timeout:I

    mul-int/lit16 v1, v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;->start(I)V

    return-void
.end method

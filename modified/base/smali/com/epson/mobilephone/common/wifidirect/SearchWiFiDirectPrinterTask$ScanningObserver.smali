.class Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;
.super Ljava/lang/Object;
.source "SearchWiFiDirectPrinterTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScanningObserver"
.end annotation


# instance fields
.field observerTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)V
    .locals 0

    .line 622
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 624
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->observerTask:Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method interrunpt()V
    .locals 2

    .line 674
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->observerTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 675
    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->observerTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SearchWiFiDirectPrinterTask"

    const-string v1, "Stop ScanningObserver"

    .line 677
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->observerTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method start(I)V
    .locals 2

    .line 635
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->interrunpt()V

    const-string v0, "SearchWiFiDirectPrinterTask"

    const-string v1, "Start ScanningObserver"

    .line 637
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver$1;

    invoke-direct {v0, p0, p1}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;I)V

    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 667
    invoke-virtual {v0, p1, v1}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->observerTask:Landroid/os/AsyncTask;

    return-void
.end method

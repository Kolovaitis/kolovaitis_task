.class final enum Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;
.super Ljava/lang/Enum;
.source "WifiP2pUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "STATUS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

.field public static final enum IDLE:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

.field public static final enum REQUEST_GROUPINFO:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 30
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    const-string v1, "IDLE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->IDLE:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    .line 31
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    const-string v1, "REQUEST_GROUPINFO"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->REQUEST_GROUPINFO:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    const/4 v0, 0x2

    .line 29
    new-array v0, v0, [Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->IDLE:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->REQUEST_GROUPINFO:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    aput-object v1, v0, v3

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;
    .locals 1

    .line 29
    const-class v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;
    .locals 1

    .line 29
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$STATUS;

    return-object v0
.end method

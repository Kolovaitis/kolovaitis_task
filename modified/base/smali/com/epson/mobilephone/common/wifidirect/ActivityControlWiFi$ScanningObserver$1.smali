.class Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver$1;
.super Landroid/os/AsyncTask;
.source "ActivityControlWiFi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;)V
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 364
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    const/4 p1, 0x0

    :cond_0
    const/16 v0, 0x2710

    if-ge p1, v0, :cond_1

    const-wide/16 v0, 0x64

    .line 373
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    add-int/lit8 p1, p1, 0x64

    .line 375
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver$1;->isCancelled()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 378
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 364
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    const-string p1, "ActivityControlWiFi"

    const-string v0, "Timeout ScanningObserver"

    .line 388
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->onOK()V

    return-void
.end method

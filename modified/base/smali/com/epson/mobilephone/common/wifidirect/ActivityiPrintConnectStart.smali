.class public Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;
.super Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;
.source "ActivityiPrintConnectStart.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;
    }
.end annotation


# static fields
.field private static final ID_ENABLED_LOCATION_SETTINGS:I = 0x1

.field public static final TAG:Ljava/lang/String; = "ActivityiPrintConnectStart"


# instance fields
.field bFirstScanResultAvailable:Z

.field btnNext:Landroid/widget/Button;

.field mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

.field mWifiManager:Landroid/net/wifi/WifiManager;

.field networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;

.field settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;-><init>()V

    .line 44
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/escprLib;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    .line 49
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    const/4 v0, 0x0

    .line 54
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;

    const/4 v0, 0x1

    .line 60
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->bFirstScanResultAvailable:Z

    return-void
.end method


# virtual methods
.method StartScan()V
    .locals 2

    .line 111
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 113
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectError;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xa

    .line 114
    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->startActivityForResult(Landroid/content/Intent;I)V

    .line 115
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->finish()V

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 120
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->bFirstScanResultAvailable:Z

    .line 123
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 157
    invoke-super {p0, p1, p2, p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_1

    .line 166
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->onBackPressed()V

    goto :goto_0

    .line 163
    :cond_1
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->StartScan()V

    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 65
    invoke-super {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 71
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a0082

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 72
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->setContentView(Landroid/view/View;)V

    const/4 v0, 0x1

    .line 73
    invoke-virtual {p0, v0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->setupCommonHeaderControl(ZZ)V

    const v1, 0x7f080099

    .line 76
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->btnNext:Landroid/widget/Button;

    .line 77
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->btnNext:Landroid/widget/Button;

    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;)V

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    invoke-virtual {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperGetSetupConnectStrings(Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;)I

    .line 90
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->canAccessWiFiInfo(Landroid/content/Context;I)Z

    move-result p1

    if-nez p1, :cond_0

    .line 93
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestLocationPermissionForce(Landroid/app/Activity;I)V

    return-void

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->StartScan()V

    return-void
.end method

.method protected onPause()V
    .locals 2

    .line 141
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onPause()V

    .line 144
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    .line 146
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 129
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onResume()V

    .line 132
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;

    .line 133
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart$NetworkStateChangeReciever;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.method onScanResultAvailable()V
    .locals 3

    .line 179
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->bFirstScanResultAvailable:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityiPrintConnectStart"

    const-string v1, "First onScanResultAvailable, ignore this"

    .line 180
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 181
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->bFirstScanResultAvailable:Z

    goto :goto_1

    .line 185
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 187
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 190
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 194
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    .line 197
    iget-object v1, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 198
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "ActivityiPrintConnectStart"

    const-string v1, "Found TempAP"

    .line 200
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->btnNext:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    return-void

    .line 209
    :cond_2
    :goto_1
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;-><init>()V

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->startScan(Landroid/net/wifi/WifiManager;)Z

    return-void
.end method

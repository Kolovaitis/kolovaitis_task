.class Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$1;
.super Ljava/lang/Object;
.source "WifiP2pUtils.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getP2POwnerInfo()Landroid/net/wifi/p2p/WifiP2pGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;)V
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->access$002(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 203
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->access$000(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p1, "WifiP2pUtils"

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received EXTRA_WIFI_P2P_GROUP = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->access$000(Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string p1, "WifiP2pUtils"

    const-string v0, "Received EXTRA_WIFI_P2P_GROUP = null"

    .line 206
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :goto_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->resumeThread()V

    return-void
.end method

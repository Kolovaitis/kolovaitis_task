.class Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$1;
.super Ljava/lang/Object;
.source "ManageDefaultNetwork.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->checkCurSSIDisOnline(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v0, 0x0

    .line 115
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v2, "http://epson.com"

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    check-cast v1, Ljava/net/HttpURLConnection;

    const-string v2, "HEAD"

    .line 116
    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    const-string v2, "Connection"

    const-string v3, "close"

    .line 118
    invoke-virtual {v1, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x1388

    .line 119
    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 121
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 122
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const-string v3, "ManageDefaultNetwork"

    .line 123
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getResponseCode() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    if-lez v2, :cond_0

    .line 127
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->canConnected:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v1, "ManageDefaultNetwork"

    const-string v2, "HttpURLConnection failed"

    .line 131
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    iput-boolean v0, v1, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->canConnected:Z

    .line 136
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->access$000(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 138
    :try_start_1
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->access$000(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    .line 141
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IllegalMonitorStateException;->printStackTrace()V

    .line 143
    :goto_1
    monitor-exit v0

    return-void

    :goto_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

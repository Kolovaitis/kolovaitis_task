.class Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SearchWiFiDirectPrinterTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WiFiDirectBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)V
    .locals 0

    .line 545
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .line 550
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    .line 552
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "wifiP2pDeviceList"

    .line 555
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p2

    check-cast p2, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    if-eqz p2, :cond_6

    .line 558
    invoke-virtual {p2}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object p2

    .line 559
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "SearchWiFiDirectPrinterTask"

    const-string p2, "No devices found"

    .line 560
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 563
    :cond_0
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 565
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 571
    :cond_1
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->isPrinter(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "SearchWiFiDirectPrinterTask"

    .line 572
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Category Not Printer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 578
    :cond_2
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    move-result-object v1

    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v3, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->isExcludedMacAddress(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "SearchWiFiDirectPrinterTask"

    .line 579
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Excluded Printer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 584
    :cond_3
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    sget-object v3, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->WiFiP2P:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->onFindPrinterResult(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :cond_4
    :goto_1
    const-string v1, "SearchWiFiDirectPrinterTask"

    .line 566
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deviceName is empty"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string p1, "SearchWiFiDirectPrinterTask"

    const-string p2, "Continue discover Peers()"

    .line 594
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    return-void
.end method

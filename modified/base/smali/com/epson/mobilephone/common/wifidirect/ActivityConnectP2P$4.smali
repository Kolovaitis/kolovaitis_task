.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;
.super Landroid/os/Handler;
.source "ActivityConnectP2P.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)V
    .locals 0

    .line 440
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .line 444
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 446
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$5;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityConnectBase$Status:[I

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/16 v1, 0xc

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 463
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 465
    :cond_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 466
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "ssid"

    .line 468
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 471
    iget-object v3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object v3, v3, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->targetSsid:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 474
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    new-instance v3, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v3}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    invoke-static {v2, v3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$302(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;Landroid/net/wifi/p2p/WifiP2pConfig;)Landroid/net/wifi/p2p/WifiP2pConfig;

    .line 475
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v2

    const-string v3, "addr_p2p"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 476
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v0

    const/4 v2, -0x1

    iput v2, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    .line 477
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v0

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    const/4 v2, 0x0

    iput v2, v0, Landroid/net/wifi/WpsInfo;->setup:I

    .line 479
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v2, v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 480
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setupObserver()V

    .line 481
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_1
    const-string v0, "ActivityConnectP2P"

    const-string v2, "Not Found Network"

    .line 485
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v2, v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 487
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onError()V

    goto/16 :goto_0

    .line 448
    :pswitch_1
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0xb

    if-eq p1, v0, :cond_2

    goto/16 :goto_1

    .line 450
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result p1

    if-nez p1, :cond_3

    .line 452
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 453
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 456
    :cond_3
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onEnabledWifi()V

    goto/16 :goto_1

    .line 550
    :pswitch_2
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 581
    :pswitch_3
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onError()V

    goto/16 :goto_1

    .line 553
    :pswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_8

    const-string v0, "name"

    .line 555
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ip"

    .line 556
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    .line 557
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    if-eqz v2, :cond_8

    const-string v1, "ActivityConnectP2P"

    .line 560
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "epsWrapperFindPrinter Success"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 564
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->expectedPrtMacAddr:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string p1, "ActivityConnectP2P"

    .line 565
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This Printer is not expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 570
    :cond_4
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->interruptFindingPrinter()V

    .line 572
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 573
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 575
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onConnectedPrinter(Landroid/content/Intent;)V

    goto :goto_1

    .line 494
    :cond_5
    :goto_0
    :pswitch_5
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0xa

    if-eq p1, v0, :cond_7

    if-eq p1, v1, :cond_6

    goto :goto_1

    :cond_6
    const-string p1, "ActivityConnectP2P"

    .line 498
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connect() "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->targetSsid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v1

    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_1

    .line 543
    :cond_7
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 544
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onConnectedWiFi()V

    :cond_8
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

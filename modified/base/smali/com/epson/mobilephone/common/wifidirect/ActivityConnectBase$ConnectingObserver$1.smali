.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver$1;
.super Landroid/os/AsyncTask;
.source "ActivityConnectBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;->start(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;

.field final synthetic val$timeout:I


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;I)V
    .locals 0

    .line 296
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;

    iput p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver$1;->val$timeout:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 296
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    const/4 p1, 0x0

    .line 304
    :cond_0
    :try_start_0
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver$1;->val$timeout:I

    if-ge p1, v0, :cond_1

    const-wide/16 v0, 0x64

    .line 305
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    add-int/lit8 p1, p1, 0x64

    .line 307
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver$1;->isCancelled()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 310
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 296
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    const-string p1, "ActivityConnectBase"

    const-string v0, "Timeout ConnectingObserver"

    .line 320
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$ConnectingObserver;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->onError()V

    return-void
.end method

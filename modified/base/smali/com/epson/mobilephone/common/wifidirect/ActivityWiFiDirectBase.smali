.class public abstract Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;
.super Lepson/print/ActivityIACommon;
.source "ActivityWiFiDirectBase.java"


# static fields
.field public static final ID_NEXT:I = 0xa


# instance fields
.field public final RESULT_BACK:I

.field private bDisplayHomeAsUpEnabled:Z

.field private bSupportCancel:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 22
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->bDisplayHomeAsUpEnabled:Z

    .line 23
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->bSupportCancel:Z

    const/4 v0, 0x1

    .line 27
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->RESULT_BACK:I

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 53
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 67
    :pswitch_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCancelPressed()V

    goto :goto_0

    .line 60
    :pswitch_1
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->getLocalClassName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "finish with RESULT_OK"

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, -0x1

    .line 61
    invoke-virtual {p0, p1, p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->setResult(ILandroid/content/Intent;)V

    .line 62
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->finish()V

    :goto_0
    :pswitch_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .line 92
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->bDisplayHomeAsUpEnabled:Z

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "finish with RESULT_BACK"

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 95
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->setResult(I)V

    .line 96
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->finish()V

    goto :goto_0

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCancelPressed()V

    :goto_0
    return-void
.end method

.method public onCancelPressed()V
    .locals 2

    .line 108
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "finish with RESULT_CANCELED"

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 109
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->setResult(I)V

    .line 110
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 34
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 116
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->bSupportCancel:Z

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0002

    .line 118
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 121
    :cond_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 127
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080211

    if-eq v0, v1, :cond_0

    .line 133
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCancelPressed()V

    const/4 p1, 0x1

    return p1
.end method

.method protected setDisplayMenu(Z)V
    .locals 0

    .line 137
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->bSupportCancel:Z

    .line 138
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->invalidateOptionsMenu()V

    return-void
.end method

.method setupCommonHeaderControl(ZZ)V
    .locals 1

    const v0, 0x7f0e0524

    .line 43
    invoke-virtual {p0, v0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->setActionBar(IZ)V

    .line 44
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->bDisplayHomeAsUpEnabled:Z

    .line 45
    iput-boolean p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->bSupportCancel:Z

    return-void
.end method

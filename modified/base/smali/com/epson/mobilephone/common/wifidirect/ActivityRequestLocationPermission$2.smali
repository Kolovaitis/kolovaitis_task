.class Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;
.super Ljava/lang/Object;
.source "ActivityRequestLocationPermission.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

.field final synthetic val$bDonotAskAgain:Z


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;Z)V
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    iput-boolean p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;->val$bDonotAskAgain:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .line 111
    iget-boolean p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;->val$bDonotAskAgain:Z

    if-nez p2, :cond_0

    .line 113
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p2}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->access$100(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_0

    .line 116
    :cond_0
    new-instance p2, Landroid/content/Intent;

    const-string v0, "android.settings.APPLICATION_DETAILS_SETTINGS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    .line 117
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p2, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 118
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    const/4 v1, 0x3

    invoke-virtual {v0, p2, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->startActivityForResult(Landroid/content/Intent;I)V

    .line 120
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

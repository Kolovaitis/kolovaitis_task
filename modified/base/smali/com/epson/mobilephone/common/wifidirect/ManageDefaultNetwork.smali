.class public Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;
.super Ljava/lang/Object;
.source "ManageDefaultNetwork.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;
    }
.end annotation


# static fields
.field private static final CHECKURL:Ljava/lang/String; = "http://epson.com"

.field private static final TAG:Ljava/lang/String; = "ManageDefaultNetwork"

.field private static final TIMEOUT_CHECKURL:I = 0x1388

.field private static manageDefaultNetwork:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;


# instance fields
.field volatile canConnected:Z

.field private checkedSSID:Ljava/lang/String;

.field private connectivityManager:Landroid/net/ConnectivityManager;

.field private isOnline:Z

.field private volatile lock:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private manageDefaultNetworkCallback:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;

.field private wifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 37
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetworkCallback:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;

    .line 39
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->mContext:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 43
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->wifiManager:Landroid/net/wifi/WifiManager;

    .line 46
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->checkedSSID:Ljava/lang/String;

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->isOnline:Z

    .line 51
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->lock:Ljava/lang/Object;

    .line 85
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->canConnected:Z

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->mContext:Landroid/content/Context;

    .line 60
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->mContext:Landroid/content/Context;

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 61
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->mContext:Landroid/content/Context;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->wifiManager:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)Ljava/lang/Object;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->lock:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)Landroid/net/ConnectivityManager;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    return-object p0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;
    .locals 1

    .line 69
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetwork:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    if-eqz v0, :cond_0

    return-object v0

    .line 74
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetwork:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    .line 76
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetwork:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    return-object p0
.end method


# virtual methods
.method declared-synchronized checkCurSSIDisOnline(Ljava/lang/String;)Z
    .locals 4

    monitor-enter p0

    const/4 v0, 0x0

    .line 88
    :try_start_0
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->canConnected:Z

    .line 91
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_0

    .line 92
    monitor-exit p0

    return v0

    .line 96
    :cond_0
    :try_start_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_1

    .line 97
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetwork()Landroid/net/Network;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 99
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 100
    invoke-virtual {v1, v0}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "ManageDefaultNetwork"

    .line 101
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WiFi connected, But default network is cellular. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "is offline."

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 102
    monitor-exit p0

    return v0

    .line 107
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->lock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    const-string v1, "ManageDefaultNetwork"

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Checking whether isOnline : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    new-instance p1, Ljava/lang/Thread;

    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)V

    invoke-direct {p1, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 146
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 150
    :try_start_4
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->lock:Ljava/lang/Object;

    const-wide/16 v1, 0x2710

    invoke-virtual {p1, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 152
    :try_start_5
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 155
    :goto_0
    iget-boolean p1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->canConnected:Z

    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    .line 156
    :try_start_6
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resetDefaultNetwork()V
    .locals 3

    monitor-enter p0

    .line 260
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    const/4 v2, 0x0

    if-lt v0, v1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBoundNetworkForProcess()Landroid/net/Network;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "ManageDefaultNetwork"

    const-string v1, "resetProcessDefaultNetwork"

    .line 262
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->bindProcessToNetwork(Landroid/net/Network;)Z

    goto :goto_0

    .line 266
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 267
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-static {}, Landroid/net/ConnectivityManager;->getProcessDefaultNetwork()Landroid/net/Network;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "ManageDefaultNetwork"

    const-string v1, "resetProcessDefaultNetwork"

    .line 268
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-static {v2}, Landroid/net/ConnectivityManager;->setProcessDefaultNetwork(Landroid/net/Network;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :cond_1
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetworkCallback:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;

    if-eqz v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetworkCallback:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 280
    iput-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetworkCallback:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 286
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    .line 284
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 289
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 273
    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setDefaultNetworkSimpleAp()V
    .locals 4

    monitor-enter p0

    .line 165
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 167
    monitor-exit p0

    return-void

    .line 171
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "ManageDefaultNetwork"

    const-string v1, "Not connected"

    .line 173
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 174
    monitor-exit p0

    return-void

    .line 177
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->checkedSSID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 179
    iget-boolean v2, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->isOnline:Z

    if-eqz v2, :cond_3

    const-string v1, "ManageDefaultNetwork"

    .line 180
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "is online."

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    monitor-exit p0

    return-void

    .line 187
    :cond_2
    :try_start_3
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->checkedSSID:Ljava/lang/String;

    .line 188
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->checkCurSSIDisOnline(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->isOnline:Z

    .line 189
    iget-boolean v2, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->isOnline:Z

    if-eqz v2, :cond_3

    const-string v1, "ManageDefaultNetwork"

    .line 190
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "is online."

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 191
    monitor-exit p0

    return-void

    .line 198
    :cond_3
    :try_start_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v0, v2, :cond_4

    .line 199
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBoundNetworkForProcess()Landroid/net/Network;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "ManageDefaultNetwork"

    const-string v1, "Already bindProcessToNetwork called"

    .line 200
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 201
    monitor-exit p0

    return-void

    .line 204
    :cond_4
    :try_start_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_8

    .line 205
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-static {}, Landroid/net/ConnectivityManager;->getProcessDefaultNetwork()Landroid/net/Network;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "ManageDefaultNetwork"

    const-string v1, "Already setProcessDefaultNetwork called"

    .line 206
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 207
    monitor-exit p0

    return-void

    .line 217
    :cond_5
    :try_start_6
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    const/16 v1, 0xc

    .line 218
    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    const/4 v1, 0x1

    .line 219
    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 222
    :try_start_7
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetworkCallback:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;

    if-nez v1, :cond_6

    .line 223
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;-><init>(Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;)V

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetworkCallback:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;

    .line 225
    :cond_6
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iget-object v3, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->manageDefaultNetworkCallback:Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork$ManageDefaultNetworkCallback;

    invoke-virtual {v1, v0, v3}, Landroid/net/ConnectivityManager;->requestNetwork(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
    :try_end_7
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 247
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 227
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 232
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-lt v0, v2, :cond_7

    .line 234
    :try_start_9
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 235
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.action.MANAGE_WRITE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    .line 236
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 237
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 238
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 239
    monitor-exit p0

    return-void

    :catch_2
    move-exception v0

    .line 243
    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 251
    :cond_7
    :goto_0
    monitor-exit p0

    return-void

    .line 211
    :cond_8
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.class public Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectError;
.super Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;
.source "ActivityWiFiDirectError.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ActivityWiFiDirectError"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 24
    invoke-super {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCreate(Landroid/os/Bundle;)V

    const-string p1, "ActivityWiFiDirectError"

    const-string v0, "onCreate"

    .line 25
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectError;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a00c9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 29
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectError;->setContentView(Landroid/view/View;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    invoke-virtual {p0, v1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectError;->setupCommonHeaderControl(ZZ)V

    .line 33
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v2

    const v3, 0x7f080326

    .line 36
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 37
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectError;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v0, v0, [Ljava/lang/Object;

    .line 38
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurNetworkId()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getSSID(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const v1, 0x7f0e0484

    .line 37
    invoke-virtual {v3, v1, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

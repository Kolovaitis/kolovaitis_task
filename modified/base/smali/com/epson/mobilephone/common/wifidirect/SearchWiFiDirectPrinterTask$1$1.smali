.class Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;
.super Ljava/lang/Object;
.source "SearchWiFiDirectPrinterTask.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;)V
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 3

    packed-switch p1, :pswitch_data_0

    const-string p1, "SearchWiFiDirectPrinterTask"

    const-string v0, "P2P_Operation Fail"

    .line 271
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    const-string p1, "SearchWiFiDirectPrinterTask"

    const-string v0, "P2P_Operation BUSY"

    .line 260
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-boolean p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->bDisConnectP2P:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->isConnectedWiFiP2P()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "SearchWiFiDirectPrinterTask"

    const-string v0, "Disconnect P2P and And Retry"

    .line 263
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->disconnect()V

    .line 265
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->bDisConnectP2P:Z

    .line 266
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->searchHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    const-wide/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :pswitch_1
    const-string p1, "SearchWiFiDirectPrinterTask"

    const-string v0, "P2P_Operation P2P_UNSUPPORTED"

    .line 254
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string p1, "SearchWiFiDirectPrinterTask"

    const-string v0, "P2P_Operation ERROR"

    .line 257
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    if-nez p1, :cond_1

    .line 277
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget v0, v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->idResult:I

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onSuccess()V
    .locals 2

    const-string v0, "SearchWiFiDirectPrinterTask"

    const-string v1, "discoverPeers Start"

    .line 246
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;->this$1:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget v1, v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    return-void
.end method

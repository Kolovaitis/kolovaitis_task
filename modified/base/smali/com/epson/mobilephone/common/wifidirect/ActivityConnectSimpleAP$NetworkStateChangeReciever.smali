.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;
.super Landroid/content/BroadcastReceiver;
.source "ActivityConnectSimpleAP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NetworkStateChangeReciever"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)V
    .locals 0

    .line 619
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .line 630
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "android.net.wifi.WIFI_STATE_CHANGED"

    .line 632
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string p1, "wifi_state"

    const/4 v0, 0x4

    .line 636
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    const-string p2, "ActivityConnectSimpleAP"

    const-string v0, "WiFi State Change : wifiState = %d"

    const/4 v2, 0x1

    .line 637
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_0
    const-string v0, "android.net.wifi.STATE_CHANGE"

    .line 639
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "networkInfo"

    .line 643
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/net/NetworkInfo;

    const-string p2, "ActivityConnectSimpleAP"

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Network State Changed:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    .line 647
    invoke-static {p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p2

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "ActivityConnectSimpleAP"

    const-string p2, "Wi-Fi connected."

    .line 649
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mHandler:Landroid/os/Handler;

    const/16 p2, 0xa

    const-wide/16 v0, 0x3e8

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_1
    const-string v0, "android.net.wifi.supplicant.STATE_CHANGE"

    .line 655
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "newState"

    .line 659
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/SupplicantState;

    const-string v0, "ActivityConnectSimpleAP"

    .line 660
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Supplicant State Changed: State = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/net/wifi/SupplicantState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "supplicantError"

    .line 662
    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "supplicantError"

    .line 663
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string v0, "ActivityConnectSimpleAP"

    .line 664
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Supplicant State Changed: ErrorCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object p2, p2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    if-ne p2, v0, :cond_2

    const-string p1, "ActivityConnectSimpleAP"

    const-string p2, "EXTRA_SUPPLICANT_ERROR"

    .line 668
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onError()V

    return-void

    .line 675
    :cond_2
    sget-object p2, Landroid/net/wifi/SupplicantState;->ASSOCIATING:Landroid/net/wifi/SupplicantState;

    if-ne p1, p2, :cond_3

    .line 676
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    sget-object p2, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    if-ne p1, p2, :cond_3

    .line 677
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->access$408(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)I

    .line 678
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->access$400(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)I

    move-result p1

    const/4 p2, 0x5

    if-le p1, p2, :cond_3

    const-string p1, "ActivityConnectSimpleAP"

    const-string p2, "Connecting Retry timeout"

    .line 679
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onError()V

    return-void

    :cond_3
    :goto_0
    return-void
.end method

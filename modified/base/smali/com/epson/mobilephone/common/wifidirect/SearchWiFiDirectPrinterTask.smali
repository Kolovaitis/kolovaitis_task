.class public Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
.super Ljava/lang/Object;
.source "SearchWiFiDirectPrinterTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;,
        Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;,
        Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;
    }
.end annotation


# static fields
.field private static final DELEY_SEARCH:I = 0x3e8

.field public static final MACADDR_INFRA:Ljava/lang/String; = "addr_infra"

.field public static final MACADDR_P2P:Ljava/lang/String; = "addr_p2p"

.field private static final MAX_RETRY_SCANNING:I = 0x5

.field private static final MAX_RETRY_SCANNING_CHECKINGTIME:I = 0x3

.field private static final MAX_RETRY_SCANNING_P2P:I = 0x1e

.field private static final MAX_SCANRESULT_TIME:I = 0x7530

.field public static final NETWORK_ID:Ljava/lang/String; = "id"

.field public static final PRINTER_NAME:Ljava/lang/String; = "name"

.field public static final PRINTER_P2P:I = 0x2

.field public static final PRINTER_SIMPLEAP:I = 0x1

.field public static final PRINTER_SSID:Ljava/lang/String; = "ssid"

.field public static final SCANNING_TIMEOUT:I = 0xf

.field public static final TAG:Ljava/lang/String; = "SearchWiFiDirectPrinterTask"

.field protected static lastScan:J

.field private static searchWiFiDirectPrinterTask:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;


# instance fields
.field private final DISCOVER_PEERS:I

.field bDisConnectP2P:Z

.field context:Landroid/content/Context;

.field idResult:I

.field listFoundSSID:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mHandler:Landroid/os/Handler;

.field mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field mWifiManager:Landroid/net/wifi/WifiManager;

.field networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;

.field p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;

.field retryScaning:I

.field retryScaningP2P:I

.field scannigObserver:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;

.field searchHandler:Landroid/os/Handler;

.field volatile serarchingStatus:I

.field timeout:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 67
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->DISCOVER_PEERS:I

    const/4 v1, 0x0

    .line 77
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    .line 82
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 87
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 88
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 93
    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;-><init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)V

    iput-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;

    .line 98
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;

    .line 104
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;

    .line 109
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mHandler:Landroid/os/Handler;

    .line 110
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->idResult:I

    const/16 v2, 0xf

    .line 111
    iput v2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->timeout:I

    .line 116
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaning:I

    .line 117
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaningP2P:I

    .line 119
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    .line 121
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->listFoundSSID:Ljava/util/ArrayList;

    .line 123
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->bDisConnectP2P:Z

    .line 223
    new-instance v0, Landroid/os/Handler;

    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)V

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->searchHandler:Landroid/os/Handler;

    .line 148
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    const-string v0, "wifi"

    .line 151
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    const-string v0, "wifip2p"

    .line 153
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 154
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v0, p1, v2, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->onScanResultAvailable()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
    .locals 1

    .line 136
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->searchWiFiDirectPrinterTask:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz v0, :cond_0

    return-object v0

    .line 139
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->searchWiFiDirectPrinterTask:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 140
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->searchWiFiDirectPrinterTask:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    return-object p0
.end method

.method private onScanResultAvailable()V
    .locals 13

    .line 351
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 356
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const-wide/16 v3, 0x7530

    const/16 v5, 0x11

    const/4 v6, 0x1

    if-lt v2, v5, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 359
    :cond_0
    sget-wide v7, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->lastScan:J

    const-wide/16 v9, 0x0

    cmp-long v2, v7, v9

    if-eqz v2, :cond_1

    sub-long v7, v0, v7

    cmp-long v2, v7, v3

    if-gez v2, :cond_1

    const-string v2, "SearchWiFiDirectPrinterTask"

    const-string v7, "onScanResultAvailable, use this Result"

    .line 362
    invoke-static {v2, v7}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    goto :goto_0

    .line 363
    :cond_1
    iget v2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaning:I

    if-lez v2, :cond_2

    const-string v2, "SearchWiFiDirectPrinterTask"

    const-string v7, "onScanResultAvailable, use this Retry Result"

    .line 366
    invoke-static {v2, v7}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "SearchWiFiDirectPrinterTask"

    const-string v7, "onScanResultAvailable, ignore this Result"

    .line 369
    invoke-static {v2, v7}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_6

    .line 375
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 377
    :try_start_0
    iget-object v7, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v7

    .line 380
    invoke-virtual {v7}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 384
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-eqz v7, :cond_6

    .line 386
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/ScanResult;

    .line 389
    iget v8, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaning:I

    const/4 v9, 0x3

    if-ge v8, v9, :cond_4

    .line 390
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v8, v5, :cond_4

    .line 391
    iget-wide v8, v7, Landroid/net/wifi/ScanResult;->timestamp:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    sub-long v8, v0, v8

    cmp-long v10, v8, v3

    if-ltz v10, :cond_4

    const-string v10, "SearchWiFiDirectPrinterTask"

    .line 395
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onScanResultAvailable, Ignore for timeout : SSID ="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v7, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " past "

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, " msec"

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v10, v7}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 401
    :cond_4
    iget-object v8, v7, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-static {v8}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 402
    invoke-static {v8}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 407
    iget-object v9, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-static {v9}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_5

    .line 413
    sget-object v10, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {v8, v10}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;

    move-result-object v8

    iget-object v7, v7, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {p0, v8, v9, v7}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->onFindPrinterResult(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_2

    .line 419
    :cond_5
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x17

    if-ge v9, v11, :cond_3

    .line 421
    iget-object v9, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-static {v9}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getExistSimpleApDisabled(Ljava/lang/String;)I

    move-result v9

    if-eq v9, v10, :cond_3

    .line 427
    sget-object v10, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {v8, v10}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;

    move-result-object v8

    iget-object v7, v7, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {p0, v8, v9, v7}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->onFindPrinterResult(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_2

    .line 437
    :cond_6
    sput-wide v0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->lastScan:J

    .line 439
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaning:I

    add-int/2addr v0, v6

    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaning:I

    .line 440
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaning:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_7

    const-string v0, "SearchWiFiDirectPrinterTask"

    const-string v1, "Retry startScan()"

    .line 441
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    goto :goto_3

    :cond_7
    const-string v0, "SearchWiFiDirectPrinterTask"

    const-string v1, "MAX_RETRY_SCANING"

    .line 445
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->unRegisterReceiverSimpleAp()V

    .line 448
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    .line 449
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    if-nez v0, :cond_8

    .line 450
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->idResult:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 451
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    :cond_8
    :goto_3
    return-void
.end method

.method private unRegisterReceiverP2P()V
    .locals 2

    .line 334
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;

    if-eqz v0, :cond_0

    .line 336
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 338
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_0
    const/4 v0, 0x0

    .line 340
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;

    :cond_0
    return-void
.end method

.method private unRegisterReceiverSimpleAp()V
    .locals 2

    .line 323
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;

    if-eqz v0, :cond_0

    .line 325
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 327
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_0
    const/4 v0, 0x0

    .line 329
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;

    :cond_0
    return-void
.end method


# virtual methods
.method public interrupt()V
    .locals 4

    const-string v0, "SearchWiFiDirectPrinterTask"

    const-string v1, "interrupt()"

    .line 292
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->searchHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->interrunpt()V

    .line 304
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_1

    const-string v0, "SearchWiFiDirectPrinterTask"

    const-string v2, "stopPeerDiscovery"

    .line 305
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 312
    :cond_1
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->unRegisterReceiverSimpleAp()V

    .line 313
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->unRegisterReceiverP2P()V

    .line 316
    iput v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    return-void
.end method

.method onFindPrinterResult(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4

    .line 464
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->listFoundSSID:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->listFoundSSID:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB;->getSimpleAPInfoDB(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;

    move-result-object v0

    if-nez v0, :cond_1

    .line 475
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;-><init>()V

    .line 476
    iput-object p1, v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->ssid:Ljava/lang/String;

    .line 477
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getPrinterNetworkName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->printerName:Ljava/lang/String;

    .line 480
    :cond_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p1

    .line 481
    iget v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->idResult:I

    iput v1, p1, Landroid/os/Message;->what:I

    .line 482
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ssid"

    .line 483
    iget-object v3, v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->ssid:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "name"

    .line 484
    iget-object v3, v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->printerName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "id"

    .line 485
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "addr_p2p"

    .line 486
    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, -0x1

    if-eq v2, p2, :cond_2

    const-string p2, "addr_infra"

    const/4 v0, 0x0

    .line 489
    invoke-static {p3, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string p2, "addr_infra"

    .line 492
    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/SimpleAPInfoDB$SimpleAPInfo;->ssid:Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string p2, "SearchWiFiDirectPrinterTask"

    .line 496
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onFindPrinterResult() data = "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "ssid"

    .line 497
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "name"

    .line 498
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "addr_infra"

    .line 499
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "addr_p2p"

    .line 500
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 496
    invoke-static {p2, p3}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    invoke-virtual {p1, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 504
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {p2, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method start(ILandroid/os/Handler;II)Z
    .locals 3

    .line 171
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const-string p1, "SearchWiFiDirectPrinterTask"

    const-string p2, "Already started"

    .line 172
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    const-string v0, "SearchWiFiDirectPrinterTask"

    const-string v2, "start()"

    .line 176
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iput-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mHandler:Landroid/os/Handler;

    .line 179
    iput p3, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->idResult:I

    .line 180
    iput p4, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->timeout:I

    .line 182
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result p2

    const/4 p3, 0x0

    if-nez p2, :cond_1

    return p3

    .line 187
    :cond_1
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->listFoundSSID:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 188
    iput p3, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaning:I

    .line 189
    iput p3, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->retryScaningP2P:I

    .line 192
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;

    mul-int/lit16 p4, p4, 0x3e8

    invoke-virtual {p2, p4}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$ScanningObserver;->start(I)V

    and-int/lit8 p2, p1, 0x1

    if-eqz p2, :cond_2

    .line 197
    new-instance p2, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;

    invoke-direct {p2, p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;-><init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)V

    iput-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;

    .line 198
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    iget-object p4, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$NetworkStateChangeReciever;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p4, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 200
    iget p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    or-int/2addr p2, v1

    iput p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->serarchingStatus:I

    .line 201
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->onScanResultAvailable()V

    :cond_2
    and-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_3

    .line 208
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz p1, :cond_3

    .line 210
    new-instance p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;

    invoke-direct {p1, p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;-><init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)V

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;

    .line 211
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$WiFiDirectBroadcastReceiver;

    new-instance p4, Landroid/content/IntentFilter;

    const-string v0, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-direct {p4, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2, p4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 213
    iput-boolean p3, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->bDisConnectP2P:Z

    .line 214
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->searchHandler:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    return v1
.end method

.class public Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;
.super Landroid/app/Activity;
.source "ActivityControlWiFi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;,
        Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;
    }
.end annotation


# static fields
.field public static final SCANNING_TIMEOUT:I = 0x2710

.field public static final SHOWPROGRESS:Ljava/lang/String; = "com.epson.iprint.wifidirect.progress"

.field public static final SHOWWIFISETTINGS:Ljava/lang/String; = "com.epson.iprint.wifidirect.wifisettings"

.field public static final TAG:Ljava/lang/String; = "ActivityControlWiFi"


# instance fields
.field final IDD_NO_WIFI:I

.field final ID_SHOWWIFISETTINGS:I

.field final MAX_RETRY_SCANNING:I

.field mWifiManager:Landroid/net/wifi/WifiManager;

.field networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

.field scancount:I

.field scannigObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

.field showProgress:Z

.field showWifiSettings:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->IDD_NO_WIFI:I

    const/4 v1, 0x5

    .line 37
    iput v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->MAX_RETRY_SCANNING:I

    .line 42
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->ID_SHOWWIFISETTINGS:I

    const/4 v1, 0x0

    .line 54
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 59
    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;)V

    iput-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    .line 64
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    .line 69
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scancount:I

    .line 74
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->showWifiSettings:Z

    .line 75
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->showProgress:Z

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const-string v0, "ActivityControlWiFi"

    const-string v1, "onActivityResult()"

    .line 136
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    if-eqz p1, :cond_0

    goto :goto_0

    .line 143
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    .line 153
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->onCancel()V

    goto :goto_0

    .line 147
    :pswitch_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->registerReciever()V

    .line 148
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->startScan()Z

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method onCancel()V
    .locals 2

    const-string v0, "ActivityControlWiFi"

    const-string v1, "finish():RESULT_CANCELED"

    .line 242
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 243
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->setResult(I)V

    .line 244
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "ActivityControlWiFi"

    const-string v1, "onCreate()"

    .line 79
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 p1, 0x1

    .line 86
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->requestWindowFeature(I)Z

    .line 89
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "com.epson.iprint.wifidirect.progress"

    .line 92
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->showProgress:Z

    const-string v1, "com.epson.iprint.wifidirect.wifisettings"

    .line 93
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->showWifiSettings:Z

    .line 97
    :cond_0
    iget-boolean p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->showProgress:Z

    if-eqz p1, :cond_1

    .line 98
    sget p1, Lcom/epson/mobilephone/common/wifidirect/R$layout;->progress:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->setContentView(I)V

    .line 101
    :cond_1
    iget-boolean p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->showWifiSettings:Z

    if-eqz p1, :cond_2

    .line 103
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->showOsWifiSettings(Landroid/app/Activity;I)V

    goto :goto_0

    .line 107
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result p1

    if-nez p1, :cond_3

    .line 108
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->showDialog(I)V

    goto :goto_0

    :cond_3
    const-string p1, "ActivityControlWiFi"

    const-string v0, "Already WiFi Enabled"

    .line 111
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->onOK()V

    :goto_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    .line 176
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    if-eqz p1, :cond_0

    goto :goto_0

    .line 186
    :cond_0
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x104000a

    .line 189
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$2;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const/high16 v0, 0x1040000

    .line 209
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_no_wifi:I

    .line 218
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 219
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 221
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "ActivityControlWiFi"

    const-string v1, "onDestroy()"

    .line 161
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 165
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    if-eqz v0, :cond_0

    const-string v0, "ActivityControlWiFi"

    const-string v1, "unregisterReceiver()"

    .line 166
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    .line 168
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->scannigObserver:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$ScanningObserver;->interrunpt()V

    return-void
.end method

.method onOK()V
    .locals 2

    const-string v0, "ActivityControlWiFi"

    const-string v1, "finish():RESULT_OK"

    .line 233
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    .line 234
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->setResult(I)V

    .line 235
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->finish()V

    return-void
.end method

.method protected onPause()V
    .locals 0

    .line 128
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 131
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcDispatchUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "ActivityControlWiFi"

    const-string v1, "onResume()"

    .line 119
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    .line 123
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcDispatchUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    return-void
.end method

.method registerReciever()V
    .locals 3

    .line 251
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    if-nez v0, :cond_0

    const-string v0, "ActivityControlWiFi"

    const-string v1, "registerReceiver()"

    .line 252
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    .line 254
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 255
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 256
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi$NetworkStateChangeReciever;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityControlWiFi;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

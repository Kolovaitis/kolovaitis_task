.class Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;
.super Landroid/content/BroadcastReceiver;
.source "ActivityiPrintConnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NetworkStateChangeReciever"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;)V
    .locals 0

    .line 658
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 669
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string p2, "android.net.wifi.SCAN_RESULTS"

    .line 671
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "ActivityiPrintConnect"

    const-string p2, "Scan Results Available"

    .line 675
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->onScanResultAvailable()V

    :cond_0
    return-void
.end method

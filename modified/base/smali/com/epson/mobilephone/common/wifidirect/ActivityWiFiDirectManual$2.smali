.class Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;
.super Landroid/os/Handler;
.source "ActivityWiFiDirectManual.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;)V
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .line 213
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 215
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 240
    :pswitch_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;)Lepson/common/DialogProgressViewModel;

    move-result-object p1

    const-string v0, "idd_wifi_waiting"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    .line 241
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;

    const v0, 0x7f0e04b7

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 218
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "name"

    .line 220
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ip"

    .line 221
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    .line 222
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 226
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->interruptFindingPrinter()V

    .line 229
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 230
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 231
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;

    const/4 v1, -0x1

    invoke-virtual {p1, v1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->setResult(ILandroid/content/Intent;)V

    .line 232
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual$2;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->finish()V

    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

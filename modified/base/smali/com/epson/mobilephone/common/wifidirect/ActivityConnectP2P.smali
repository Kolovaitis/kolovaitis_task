.class public Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;
.super Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;
.source "ActivityConnectP2P.java"

# interfaces
.implements Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect$DialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final DELAY_P2P_CONNECT:I = 0x3e8

.field private static final IDD_WIFI_ERROR:Ljava/lang/String; = "idd_wifi_error"

.field private static final IDD_WIFI_WAITING:Ljava/lang/String; = "idd_wifi_waiting"

.field private static final ID_CHECK_WIIFISTATE:I = 0xb

.field private static final ID_CONNECTED_WIFI:I = 0xa

.field private static final ID_CONNECT_P2P:I = 0xc

.field private static final ID_FOUND:I = 0x1

.field private static final ID_NOT_FOUND:I = 0x2

.field private static final ID_RESULT:I = 0x0

.field private static final MAX_P2P_CONNECT:I = 0x5

.field private static final TAG:Ljava/lang/String; = "ActivityConnectP2P"


# instance fields
.field mHandler:Landroid/os/Handler;

.field private mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

.field mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private p2pConnectConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

.field private p2pRetryCount:I

.field p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 34
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;-><init>()V

    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 70
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 75
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;

    .line 93
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pConnectConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    const/4 v0, 0x0

    .line 95
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pRetryCount:I

    .line 440
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$4;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    return-object p0
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;Ljava/lang/String;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->showDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;Ljava/lang/String;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->dismissDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pConnectConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    return-object p0
.end method

.method static synthetic access$302(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;Landroid/net/wifi/p2p/WifiP2pConfig;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pConnectConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    return-object p1
.end method

.method static synthetic access$400(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pRetryCount:I

    return p0
.end method

.method static synthetic access$408(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)I
    .locals 2

    .line 34
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pRetryCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pRetryCount:I

    return v0
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 292
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 294
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 10

    .line 269
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0xa654b07

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    const v1, 0x2da5abb4

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "idd_wifi_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "idd_wifi_waiting"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_2

    :pswitch_0
    const/4 v4, 0x2

    .line 278
    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_error_connecting_printer_p2p:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const v0, 0x104000a

    .line 279
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p1

    .line 278
    invoke-static/range {v3 .. v9}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    const/4 v4, 0x0

    .line 274
    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_connecting_printer_p2p:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_cancel:I

    .line 275
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p1

    .line 274
    invoke-static/range {v3 .. v9}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_3

    .line 283
    invoke-virtual {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->setCancelable(Z)V

    .line 284
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method closeWaitingDialog()V
    .locals 2

    const-string v0, "ActivityConnectP2P"

    const-string v1, "Called closeWaitingDialog()"

    .line 430
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    const-string v1, "idd_wifi_waiting"

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->doDismiss(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "ActivityConnectP2P"

    const-string v1, "IDD_WIFI_WAITING already closed"

    .line 436
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected interruptConnecting()V
    .locals 1

    .line 303
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->interruptConnecting()V

    .line 306
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;

    if-eqz v0, :cond_0

    .line 308
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 310
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_0
    const/4 v0, 0x0

    .line 312
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 166
    invoke-super {p0, p1, p2, p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x2

    if-eq p1, p3, :cond_1

    const/4 p2, 0x5

    if-eq p1, p2, :cond_0

    goto :goto_0

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onEnabledWifi()V

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    if-eq p2, p1, :cond_2

    .line 178
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->closeWaitingDialog()V

    const/4 p1, 0x0

    .line 179
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setResult(I)V

    const/4 p1, 0x1

    .line 180
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setLastDetailResult(I)V

    .line 181
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->finish()V

    goto :goto_0

    .line 173
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    const/16 p2, 0xb

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void
.end method

.method public onCancelDialog(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method onConnectedWiFi()V
    .locals 6

    const-string v0, "ActivityConnectP2P"

    const-string v1, "onConnect()"

    .line 322
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->interruptConnecting()V

    .line 327
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->resetDefaultNetwork()V

    .line 330
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->showConnectedTip:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 331
    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_notice_wifi_connected:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 334
    :cond_0
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->needInfo:Z

    if-nez v0, :cond_1

    .line 339
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->closeWaitingDialog()V

    .line 341
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    const/4 v0, -0x1

    .line 342
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setResult(I)V

    .line 343
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setLastDetailResult(I)V

    .line 344
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->finish()V

    return-void

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    if-eqz v0, :cond_2

    const-string v0, "ActivityConnectP2P"

    const-string v2, "Already called onConnect()"

    .line 350
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_2
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    move-result-object v0

    .line 355
    iget-boolean v2, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->isGroupOwnerThisDevice:Z

    if-eqz v2, :cond_3

    .line 357
    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->targetSsid:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->expectedPrtMacAddr:Ljava/lang/String;

    goto :goto_0

    .line 360
    :cond_3
    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->targetSsid:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddrP2P(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->expectedPrtMacAddr:Ljava/lang/String;

    .line 363
    :goto_0
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->PRINTER_FINDING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 364
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    iget v3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->timeout:I

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;-><init>(Landroid/os/Handler;III)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    .line 365
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v2, v1}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 102
    invoke-super {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    .line 106
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 131
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    const-string v0, "idd_wifi_waiting"

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->doShow(Ljava/lang/String;)V

    const-string p1, "wifip2p"

    .line 134
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 135
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz p1, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 138
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez p1, :cond_1

    goto :goto_1

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 145
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_2

    const-string v0, "com.epson.iprint.wifidirect.apname"

    .line 147
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->targetSsid:Ljava/lang/String;

    const-string v0, "com.epson.iprint.wifidirect.showerror"

    const/4 v1, 0x0

    .line 148
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->showErrorDlg:Z

    const-string v0, "com.epson.iprint.wifidirect.showconnecttip"

    const/4 v2, 0x1

    .line 149
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->showConnectedTip:Z

    const-string v0, "com.epson.iprint.wifidirect.needinfo"

    .line 150
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->needInfo:Z

    const-string v0, "com.epson.iprint.wifidirect.timeout"

    const/16 v1, 0x1e

    .line 151
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->timeout:I

    :cond_2
    const-string p1, "ActivityConnectP2P"

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Starting connect :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->targetSsid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " timeout = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->timeout:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1d

    if-lt p1, v0, :cond_3

    const/4 p1, 0x2

    .line 158
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestLocationPermissionForce(Landroid/app/Activity;I)V

    goto :goto_0

    .line 160
    :cond_3
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    .line 139
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onError()V

    return-void
.end method

.method protected onEnabledWifi()V
    .locals 5

    .line 199
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v1

    .line 203
    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 204
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurNetworkId()I

    move-result v2

    invoke-virtual {v0, p0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->invalidateSimpleAP(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_error_wifi_connecting_failed:I

    .line 209
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_goto_wifi_setting:I

    .line 211
    invoke-virtual {p0, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$3;

    invoke-direct {v4, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$3;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)V

    invoke-virtual {v0, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_cancel:I

    .line 221
    invoke-virtual {p0, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$2;

    invoke-direct {v4, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)V

    invoke-virtual {v0, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_notice_delete_simpleAP:I

    .line 232
    invoke-virtual {p0, v4}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n\nSSID: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 233
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 235
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 240
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 242
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->targetSsid:Ljava/lang/String;

    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ActivityConnectP2P"

    const-string v1, "Already Connected!!"

    .line 244
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 246
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 250
    :cond_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->disconnect()V

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    iget v4, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->timeout:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->start(ILandroid/os/Handler;II)Z

    move-result v0

    if-nez v0, :cond_3

    .line 257
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onError()V

    goto :goto_0

    .line 259
    :cond_3
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_SCANNING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    :goto_0
    return-void
.end method

.method onError()V
    .locals 4

    .line 375
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->closeWaitingDialog()V

    .line 378
    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_error_connecting_printer_short:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 381
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    .line 383
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 386
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$5;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityConnectBase$Status:[I

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 402
    :pswitch_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    if-eqz v0, :cond_1

    .line 403
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->cancel()V

    .line 404
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->taskFindPrinter:Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;

    goto :goto_0

    .line 388
    :pswitch_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pConnectConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    if-eqz v0, :cond_1

    .line 390
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->interruptConnecting()V

    .line 393
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_1

    const-string v0, "ActivityConnectP2P"

    .line 394
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancelConnect() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->targetSsid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v2, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 396
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pConnectConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    .line 410
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->showErrorDlg:Z

    if-eqz v0, :cond_2

    .line 414
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    const-string v1, "idd_wifi_error"

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->doShow(Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v0, "ActivityConnectP2P"

    const-string v1, "finish():RESULT_CANCELED"

    .line 418
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 419
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setResult(I)V

    const/4 v0, 0x2

    .line 420
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setLastDetailResult(I)V

    .line 421
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->finish()V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onNegativeClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onNeutralClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onPositiveClick(Ljava/lang/String;)V
    .locals 3

    .line 603
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0xa654b07

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    const v1, 0x2da5abb4

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "idd_wifi_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "idd_wifi_waiting"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, -0x1

    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const-string p1, "ActivityConnectP2P"

    const-string v0, "finish():RESULT_CANCELED"

    .line 609
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    invoke-virtual {p0, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setResult(I)V

    const/4 p1, 0x2

    .line 611
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->setLastDetailResult(I)V

    .line 612
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->finish()V

    goto :goto_2

    .line 605
    :pswitch_1
    iput-boolean v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->showErrorDlg:Z

    .line 606
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->onError()V

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method setupObserver()V
    .locals 3

    .line 594
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->setupObserver()V

    .line 597
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;

    .line 598
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->p2pStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

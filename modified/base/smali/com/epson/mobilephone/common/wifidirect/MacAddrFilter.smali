.class public Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;
.super Ljava/lang/Object;
.source "MacAddrFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;
    }
.end annotation


# static fields
.field private static COMMENT_PREFIX:Ljava/lang/String; = "#"

.field private static EXCLUDE_MACADDRESS_FILE:Ljava/lang/String; = "exclude_macaddress.csv"

.field private static final TAG:Ljava/lang/String; = "MacAddrFilter"

.field private static instance:Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;


# instance fields
.field private excludeMacAddressList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->mAppContext:Landroid/content/Context;

    .line 25
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->excludeMacAddressList:Ljava/util/ArrayList;

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->mAppContext:Landroid/content/Context;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;
    .locals 1

    .line 57
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->instance:Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    if-eqz v0, :cond_0

    return-object v0

    .line 60
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->instance:Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    .line 61
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->instance:Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    return-object p0
.end method

.method private readMacAddressTable(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, "MacAddrFilter"

    .line 110
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Enter readMacAddressTable: path = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 120
    :try_start_0
    iget-object v7, v1, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->mAppContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz v7, :cond_a

    .line 122
    :try_start_1
    new-instance v8, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v8, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v5, 0x0

    .line 128
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 130
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    sget-object v9, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->COMMENT_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    goto :goto_0

    :cond_1
    add-int/lit8 v5, v5, 0x1

    const-string v9, "[,\\u002C\t\\u0009]+"

    .line 138
    invoke-virtual {v0, v9, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v9

    .line 139
    array-length v10, v9

    const/4 v11, 0x2

    if-ge v10, v11, :cond_2

    const-string v9, "MacAddrFilter"

    .line 141
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Illegal Line = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v10, 0x1

    const/4 v12, 0x1

    .line 146
    :goto_1
    array-length v13, v9

    if-ge v12, v13, :cond_5

    .line 147
    aget-object v13, v9, v12

    const-string v14, "\""

    .line 148
    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 149
    invoke-virtual {v13, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    :cond_3
    const-string v14, "\""

    .line 151
    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 152
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v14

    sub-int/2addr v14, v10

    invoke-virtual {v13, v6, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 154
    :cond_4
    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_5
    const/4 v12, 0x1

    .line 158
    :goto_2
    array-length v13, v9

    if-ge v12, v13, :cond_0

    .line 159
    aget-object v13, v9, v12

    const-string v14, "[-\u2013]"

    const-string v15, " "

    .line 162
    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "[\\s]+"

    .line 163
    invoke-virtual {v13, v14, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v14

    .line 165
    array-length v15, v14

    if-eq v15, v11, :cond_6

    const-string v14, "MacAddrFilter"

    .line 167
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Illegal item = "

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v14, v11}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 171
    :cond_6
    aget-object v11, v14, v6

    invoke-static {v11}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->p2pAddr2MacAddrStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 172
    aget-object v13, v14, v10

    invoke-static {v13}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->p2pAddr2MacAddrStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v11, :cond_7

    .line 174
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v14

    const/16 v15, 0xc

    if-ne v14, v15, :cond_7

    if-eqz v13, :cond_7

    .line 175
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v14

    if-ne v14, v15, :cond_7

    .line 177
    new-instance v14, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;

    invoke-direct {v14, v1, v11, v13}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;-><init>(Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_8

    .line 179
    new-instance v14, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;

    invoke-direct {v14, v1, v11, v13}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;-><init>(Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    const-string v11, "MacAddrFilter"

    .line 183
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Illegal Line = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_8
    :goto_3
    add-int/lit8 v12, v12, 0x1

    const/4 v11, 0x2

    goto :goto_2

    :cond_9
    move v6, v5

    move-object v5, v8

    goto :goto_6

    :catchall_0
    move-exception v0

    move-object v2, v0

    goto/16 :goto_d

    :catch_0
    move-exception v0

    move v6, v5

    goto :goto_4

    :catch_1
    move-exception v0

    move v6, v5

    goto :goto_5

    :catchall_1
    move-exception v0

    move-object v2, v0

    move-object v8, v5

    goto/16 :goto_d

    :catch_2
    move-exception v0

    move-object v8, v5

    :goto_4
    move-object v5, v7

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v8, v5

    :goto_5
    move-object v5, v7

    goto :goto_a

    :cond_a
    :goto_6
    if-eqz v7, :cond_b

    .line 195
    :try_start_3
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_7

    :catch_4
    move-exception v0

    move-object v7, v0

    .line 197
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    :cond_b
    :goto_7
    if-eqz v5, :cond_e

    .line 200
    :try_start_4
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_c

    :catch_5
    move-exception v0

    move-object v5, v0

    .line 202
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    :catchall_2
    move-exception v0

    move-object v2, v0

    move-object v7, v5

    move-object v8, v7

    goto :goto_d

    :catch_6
    move-exception v0

    move-object v8, v5

    .line 192
    :goto_8
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    if-eqz v5, :cond_c

    .line 195
    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    goto :goto_9

    :catch_7
    move-exception v0

    move-object v5, v0

    .line 197
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    :cond_c
    :goto_9
    if-eqz v8, :cond_e

    .line 200
    :try_start_7
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_c

    :catch_8
    move-exception v0

    move-object v8, v5

    .line 190
    :goto_a
    :try_start_8
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    if-eqz v5, :cond_d

    .line 195
    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9

    goto :goto_b

    :catch_9
    move-exception v0

    move-object v5, v0

    .line 197
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    :cond_d
    :goto_b
    if-eqz v8, :cond_e

    .line 200
    :try_start_a
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    :cond_e
    :goto_c
    const-string v0, "MacAddrFilter"

    .line 206
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Leave readMacAddressTable: readlinecount = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " entry = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " time = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " msec"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 206
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    :catchall_3
    move-exception v0

    move-object v2, v0

    move-object v7, v5

    :goto_d
    if-eqz v7, :cond_f

    .line 195
    :try_start_b
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    goto :goto_e

    :catch_a
    move-exception v0

    move-object v3, v0

    .line 197
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    :cond_f
    :goto_e
    if-eqz v8, :cond_10

    .line 200
    :try_start_c
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_b

    goto :goto_f

    :catch_b
    move-exception v0

    move-object v3, v0

    .line 202
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 203
    :cond_10
    :goto_f
    throw v2
.end method


# virtual methods
.method public isExcludedMacAddress(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    .line 80
    :try_start_0
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->p2pAddr2MacAddrStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 82
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->excludeMacAddressList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 84
    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->EXCLUDE_MACADDRESS_FILE:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->readMacAddressTable(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->excludeMacAddressList:Ljava/util/ArrayList;

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->excludeMacAddressList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;

    .line 89
    iget-object v3, v2, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->start:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-gtz v3, :cond_1

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter$MacAddressEntry;->end:Ljava/lang/String;

    .line 90
    invoke-virtual {v2, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ltz v2, :cond_1

    const/4 p1, 0x1

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 97
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    :goto_0
    return v0
.end method

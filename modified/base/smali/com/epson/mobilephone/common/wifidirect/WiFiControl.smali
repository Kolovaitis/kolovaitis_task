.class public Lcom/epson/mobilephone/common/wifidirect/WiFiControl;
.super Ljava/lang/Object;
.source "WiFiControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAC_ADDRESS:Ljava/lang/String; = "02:00:00:00:00:00"

.field private static final MACADDR2SSID_OFFSET:I = 0x4

.field public static final P2P_FY14_17_DEFAULT:Ljava/lang/String; = "^EPSON...... "

.field private static final PREFIX_SIMPLEAP:Ljava/lang/String; = "#S "

.field private static final PREFIX_WIFIP2P:Ljava/lang/String; = "#P "

.field public static final SSID_P2P_FY18:Ljava/lang/String; = "^DIRECT-..-"

.field public static final SSID_P2P_FY18_DEFAULT:Ljava/lang/String; = "^DIRECT-..-EPSON-"

.field public static final SSID_P2P_FY18_LCD:Ljava/lang/String; = "^DIRECT-..-EPSON-[0-9A-F]{6}$"

.field public static final SSID_SIMPLEAP_PREFIX:Ljava/lang/String; = "DIRECT-"

.field public static final SSID_WIFIDIRECTMODE_FY13:Ljava/lang/String; = "^DIRECT-..EPSON..$"

.field private static final TAG:Ljava/lang/String; = "WiFiControl"

.field public static instance:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;


# instance fields
.field private mAppContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    return-void
.end method

.method public static addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;
    .locals 1

    .line 303
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->hasSSIDPrefix(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    .line 307
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const-string p1, "WiFiControl"

    const-string v0, "Unknown Type"

    .line 315
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 312
    :pswitch_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "#P "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 309
    :pswitch_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "#S "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl;
    .locals 1

    .line 45
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    if-eqz v0, :cond_0

    return-object v0

    .line 49
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    .line 50
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiControl;

    return-object p0
.end method

.method public static getPrinterNetworkName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 161
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 167
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAPFY13WiFiDirect(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const-string v0, "^EPSON...... .*"

    .line 171
    invoke-static {v0, p0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "^EPSON...... "

    const-string v1, ""

    .line 173
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const-string v0, "^DIRECT-..-EPSON-.*"

    .line 174
    invoke-static {v0, p0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "^DIRECT-..-EPSON-[0-9A-F]{6}$"

    .line 175
    invoke-static {v0, p0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const-string v0, "^DIRECT-..-EPSON-"

    const-string v1, ""

    .line 180
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    :cond_3
    :goto_0
    return-object p0
.end method

.method public static getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;
    .locals 1

    if-eqz p0, :cond_1

    .line 268
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "#S "

    .line 269
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    return-object p0

    :cond_0
    const-string v0, "#P "

    .line 271
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 272
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->WiFiP2P:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    return-object p0

    .line 276
    :cond_1
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    return-object p0
.end method

.method public static hasSSIDPrefix(Ljava/lang/String;)Z
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 353
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method public static isSimpleAP(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 229
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "DIRECT-"

    .line 230
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static isSimpleAPFY13WiFiDirect(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 249
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "^DIRECT-..EPSON..$"

    .line 250
    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 406
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressBytes(Ljava/lang/String;)[B

    move-result-object p0

    const/4 v0, 0x0

    .line 409
    aget-byte v1, p0, v0

    aget-byte v2, p0, v0

    and-int/lit8 v2, v2, -0x3

    int-to-byte v2, v2

    and-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    if-eqz p1, :cond_1

    .line 412
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->hasSSIDPrefix(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    const-string v0, "^DIRECT-..-.*"

    .line 416
    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 417
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p1, 0x4

    .line 422
    aget-byte v0, p0, p1

    xor-int/lit8 v0, v0, -0x80

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 424
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static p2pAddr2PtrAddrP2P(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 370
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressBytes(Ljava/lang/String;)[B

    move-result-object p0

    if-eqz p1, :cond_2

    .line 373
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->hasSSIDPrefix(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    const-string v0, "^DIRECT-..-.*"

    .line 377
    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 378
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p1, 0x4

    .line 386
    aget-byte v0, p0, p1

    xor-int/lit8 v0, v0, -0x80

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 388
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 382
    :cond_2
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static p2pPtrAddr2PtrAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 437
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressBytes(Ljava/lang/String;)[B

    move-result-object p0

    const/4 v0, 0x0

    .line 439
    aget-byte v1, p0, v0

    and-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    .line 441
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 445
    :cond_0
    aget-byte v1, p0, v0

    aget-byte v2, p0, v0

    and-int/lit8 v2, v2, -0x3

    int-to-byte v2, v2

    and-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    const/4 v0, 0x4

    .line 447
    aget-byte v1, p0, v0

    xor-int/lit8 v1, v1, -0x80

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 449
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAdressStr([B)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static rebuildSSID(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 286
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->hasSSIDPrefix(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    .line 290
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "#S "

    .line 333
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "#S "

    const-string v1, ""

    .line 334
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const-string v0, "#P "

    .line 335
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "#P "

    const-string v1, ""

    .line 336
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    :cond_2
    :goto_0
    return-object p0
.end method


# virtual methods
.method public getCurConnectInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 65
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 67
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto/16 :goto_0

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 75
    iget-object v3, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object v4, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 77
    iget-object v4, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->isExcludedMacAddress(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 78
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v1

    .line 83
    :cond_1
    iget-boolean v3, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->isGroupOwnerThisDevice:Z

    if-eqz v3, :cond_2

    .line 85
    iget-object v3, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object v4, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 86
    iget-object p1, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->WiFiP2P:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 90
    :cond_2
    iget-object v3, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object v4, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddrP2P(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 91
    iget-object p1, v2, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->WiFiP2P:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 96
    :cond_3
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurP2PMacAdder()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v3, "02:00:00:00:00:00"

    .line 99
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 101
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->p2pAddr2MacAddrStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 102
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {v2, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 106
    :cond_4
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->isSimpleAP(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 107
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {v2, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->addSSIDPrefix(Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_5
    return-object v1

    :cond_6
    :goto_0
    return-object v1
.end method

.method public getEstimateSSID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 199
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 204
    :pswitch_0
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "^EPSON...... .*"

    .line 205
    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "DIRECT-"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 201
    :pswitch_1
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isNeedConnect(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 120
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 124
    :cond_0
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->removeSSIDPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 126
    sget-object v2, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$1;->$SwitchMap$com$epson$mobilephone$common$wifidirect$WiFiControl$ConnectType:[I

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->getSSIDType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->ordinal()I

    move-result p1

    aget p1, v2, p1

    packed-switch p1, :pswitch_data_0

    const-string p1, "WiFiControl"

    const-string v0, "Unknown Type"

    .line 147
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :pswitch_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 138
    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "WiFiControl"

    const-string v1, "Already connected : P2P"

    .line 141
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->resetDefaultNetwork()V

    return v0

    .line 128
    :pswitch_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "WiFiControl"

    const-string v1, "Already connected : SimpleAP"

    .line 131
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->mAppContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ManageDefaultNetwork;->setDefaultNetworkSimpleAp()V

    return v0

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_1
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

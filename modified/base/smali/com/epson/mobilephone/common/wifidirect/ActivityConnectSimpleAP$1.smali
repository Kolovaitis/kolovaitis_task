.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$1;
.super Ljava/lang/Object;
.source "ActivityConnectSimpleAP.java"

# interfaces
.implements Landroid/arch/lifecycle/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/arch/lifecycle/Observer<",
        "Ljava/util/Deque<",
        "[",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onChanged(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 99
    check-cast p1, Ljava/util/Deque;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$1;->onChanged(Ljava/util/Deque;)V

    return-void
.end method

.method public onChanged(Ljava/util/Deque;)V
    .locals 2
    .param p1    # Ljava/util/Deque;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Deque<",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 106
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->checkQueue()[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 108
    aget-object v0, p1, v0

    const/4 v1, 0x1

    .line 109
    aget-object p1, p1, v1

    const-string v1, "do_show"

    .line 112
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->access$100(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;Ljava/lang/String;)V

    :cond_0
    const-string v1, "do_dismiss"

    .line 116
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 117
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->access$200(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

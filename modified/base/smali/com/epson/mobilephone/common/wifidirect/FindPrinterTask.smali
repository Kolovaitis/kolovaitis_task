.class Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;
.super Landroid/os/AsyncTask;
.source "FindPrinterTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FindPrinterTask"


# instance fields
.field canceled:Z

.field handler:Landroid/os/Handler;

.field idFound:I

.field idNotFound:I

.field mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

.field timeout:I


# direct methods
.method constructor <init>(Landroid/os/Handler;III)V
    .locals 1

    .line 35
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 19
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/escprLib;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->handler:Landroid/os/Handler;

    const/4 v0, 0x0

    .line 28
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->canceled:Z

    .line 36
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->handler:Landroid/os/Handler;

    .line 37
    iput p2, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->timeout:I

    .line 38
    iput p3, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->idFound:I

    .line 39
    iput p4, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->idNotFound:I

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->setSearchStt(Z)V

    .line 93
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperCancelFindPrinter()I

    const/4 v0, 0x1

    .line 94
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->canceled:Z

    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .line 54
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    const/16 v0, 0xc0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperInitDriver(I)I

    move-result p1

    const/16 v1, -0x41a

    const/4 v2, 0x1

    if-eq p1, v1, :cond_0

    if-eqz p1, :cond_0

    .line 62
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, "FindPrinterTask"

    const-string v1, "epsWrapperFindPrinter start"

    .line 65
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    iget v1, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->timeout:I

    invoke-virtual {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperFindPrinter(II)I

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x2a

    if-eq p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 79
    :goto_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperReleaseDriver()I

    .line 81
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    .line 85
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    iget-boolean p1, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->canceled:Z

    if-nez p1, :cond_1

    :cond_0
    const-string p1, "FindPrinterTask"

    const-string v0, "epsWrapperFindPrinter = EPS_ERR_PRINTER_NOT_FOUND"

    .line 86
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->handler:Landroid/os/Handler;

    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->idNotFound:I

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .line 44
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 46
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->handler:Landroid/os/Handler;

    iget v2, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->idFound:I

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->setHanlder(Landroid/os/Handler;I)V

    .line 47
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/FindPrinterTask;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->setSearchStt(Z)V

    return-void
.end method

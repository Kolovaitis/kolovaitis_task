.class public Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;
.super Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;
.source "ActivityConnectSimpleAP.java"

# interfaces
.implements Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect$DialogButtonClick;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;
    }
.end annotation


# static fields
.field private static final DELAY_CONNECTED_WIFI:I = 0x3e8

.field private static final IDD_WIFI_ERROR:Ljava/lang/String; = "idd_wifi_error"

.field private static final IDD_WIFI_PROFILE_FAILED:Ljava/lang/String; = "idd_wifi_profile_failed"

.field private static final IDD_WIFI_SIMPLEAP:Ljava/lang/String; = "idd_wifi_simpleap"

.field private static final IDD_WIFI_WAITING:Ljava/lang/String; = "idd_wifi_waiting"

.field private static final ID_CHECK_WIIFISTATE:I = 0xb

.field private static final ID_CONNECTED_WIFI:I = 0xa

.field private static final ID_FOUND:I = 0x1

.field private static final ID_NOT_FOUND:I = 0x2

.field private static final ID_RESULT:I = 0x0

.field private static final MAX_RETRY_CONNECTING:I = 0x5

.field private static final TAG:Ljava/lang/String; = "ActivityConnectSimpleAP"


# instance fields
.field private connectingNetworkId:I

.field createSimpleAP:Z

.field mHandler:Landroid/os/Handler;

.field private mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

.field networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

.field private retryConnecting:I

.field targetPass:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;-><init>()V

    const/4 v0, 0x0

    .line 68
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

    const-string v0, ""

    .line 74
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetPass:Ljava/lang/String;

    const/4 v0, 0x0

    .line 76
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->createSimpleAP:Z

    .line 86
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->retryConnecting:I

    const/4 v0, -0x1

    .line 88
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->connectingNetworkId:I

    .line 431
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$3;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    return-object p0
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;Ljava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->showDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;Ljava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->dismissDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->connectingNetworkId:I

    return p0
.end method

.method static synthetic access$302(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;I)I
    .locals 0

    .line 30
    iput p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->connectingNetworkId:I

    return p1
.end method

.method static synthetic access$400(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->retryConnecting:I

    return p0
.end method

.method static synthetic access$408(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)I
    .locals 2

    .line 30
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->retryConnecting:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->retryConnecting:I

    return v0
.end method

.method private dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 280
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 282
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 10

    .line 235
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x33faafcb    # -3.4947284E7f

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    const v1, -0x29423099

    if-eq v0, v1, :cond_2

    const v1, -0xa654b07

    if-eq v0, v1, :cond_1

    const v1, 0x2da5abb4

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "idd_wifi_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "idd_wifi_waiting"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v0, "idd_wifi_profile_failed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_3
    const-string v0, "idd_wifi_simpleap"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto/16 :goto_2

    :pswitch_0
    const/4 v4, 0x2

    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v1, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_notice_duplicated_simpleAP:I

    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\nSSID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_error_wifi_connecting_failed:I

    .line 267
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_goto_wifi_setting:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_cancel:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v3, p1

    .line 266
    invoke-static/range {v3 .. v9}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    const/4 v4, 0x2

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v1, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_notice_connecting_simpleAP:I

    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\nSSID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_error_wifi_connecting_simpleAP:I

    .line 258
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_goto_wifi_setting:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_cancel:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v3, p1

    .line 257
    invoke-static/range {v3 .. v9}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;

    move-result-object v0

    goto :goto_2

    :pswitch_2
    const/4 v4, 0x2

    .line 248
    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_error_connecting_printer:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const v0, 0x104000a

    .line 249
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p1

    .line 248
    invoke-static/range {v3 .. v9}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->newInstance(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;

    move-result-object v0

    goto :goto_2

    .line 240
    :pswitch_3
    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_connecting_printer:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->newInstance(Ljava/lang/String;ILjava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_5

    .line 271
    invoke-virtual {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->setCancelable(Z)V

    .line 272
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirect;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method closeWaitingDialog()V
    .locals 2

    const-string v0, "ActivityConnectSimpleAP"

    const-string v1, "Called closeWaitingDialog()"

    .line 421
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    const-string v1, "idd_wifi_waiting"

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->doDismiss(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "ActivityConnectSimpleAP"

    const-string v1, "IDD_WIFI_WATING already closed"

    .line 427
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected interruptConnecting()V
    .locals 1

    .line 291
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->interruptConnecting()V

    .line 294
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

    if-eqz v0, :cond_0

    .line 296
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 298
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_0
    const/4 v0, 0x0

    .line 300
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 146
    invoke-super {p0, p1, p2, p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x2

    const/4 v0, 0x0

    if-eq p1, p3, :cond_1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 187
    :pswitch_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onEnabledWifi()V

    goto :goto_0

    .line 174
    :pswitch_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p2

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onConnectedWiFi()V

    goto :goto_0

    .line 179
    :cond_0
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setResult(I)V

    .line 180
    invoke-static {p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setLastDetailResult(I)V

    .line 181
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->finish()V

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    const/4 p3, 0x1

    if-eq p2, p1, :cond_2

    .line 164
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->closeWaitingDialog()V

    .line 165
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setResult(I)V

    .line 166
    invoke-static {p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setLastDetailResult(I)V

    .line 167
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->finish()V

    goto :goto_0

    .line 154
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->timeout:I

    invoke-virtual {p1, p3, p2, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->start(ILandroid/os/Handler;II)Z

    move-result p1

    if-nez p1, :cond_3

    .line 156
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onError()V

    goto :goto_0

    .line 158
    :cond_3
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_SCANNING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCancelDialog(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method onConnectedWiFi()V
    .locals 3

    const-string v0, "ActivityConnectSimpleAP"

    const-string v1, "onConnect()"

    .line 312
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->interruptConnecting()V

    .line 316
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    .line 364
    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 95
    invoke-super {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->onCreate(Landroid/os/Bundle;)V

    .line 98
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object p1

    const-class v0, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    .line 99
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->getDialogJob()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object p1

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)V

    invoke-virtual {p1, p0, v0}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 124
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    const-string v0, "idd_wifi_waiting"

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->doShow(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 128
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "com.epson.iprint.wifidirect.apname"

    .line 130
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    const-string v0, "com.epson.iprint.wifidirect.appass"

    .line 131
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetPass:Ljava/lang/String;

    const-string v0, "com.epson.iprint.wifidirect.showerror"

    const/4 v1, 0x0

    .line 132
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->showErrorDlg:Z

    const-string v0, "com.epson.iprint.wifidirect.showconnecttip"

    const/4 v2, 0x1

    .line 133
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->showConnectedTip:Z

    const-string v0, "com.epson.iprint.wifidirect.create"

    .line 134
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->createSimpleAP:Z

    const-string v0, "com.epson.iprint.wifidirect.needinfo"

    .line 135
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->needInfo:Z

    const-string v0, "com.epson.iprint.wifidirect.timeout"

    const/16 v1, 0x1e

    .line 136
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->timeout:I

    :cond_0
    const-string p1, "ActivityConnectSimpleAP"

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Starting connect :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " timeout = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->timeout:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected onEnabledWifi()V
    .locals 3

    .line 198
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ActivityConnectSimpleAP"

    const-string v1, "Already Connected!!"

    .line 200
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 202
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 207
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->isConnectedWiFiP2P()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->disconnect()V

    .line 211
    :cond_1
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->createSimpleAP:Z

    if-eqz v0, :cond_3

    .line 213
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetSsid:Ljava/lang/String;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->targetPass:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->createSimpleAP(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->connectingNetworkId:I

    .line 214
    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->connectingNetworkId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 215
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    const-string v1, "idd_wifi_profile_failed"

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->doShow(Ljava/lang/String;)V

    return-void

    .line 219
    :cond_2
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    .line 220
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setupObserver()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    .line 224
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestLocationPermissionForce(Landroid/app/Activity;I)V

    :goto_0
    return-void
.end method

.method onError()V
    .locals 3

    .line 375
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->closeWaitingDialog()V

    .line 378
    sget v0, Lcom/epson/mobilephone/common/wifidirect/R$string;->str_error_connecting_printer_short:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 381
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    const/4 v0, 0x0

    .line 383
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;->WIFI_CONNECTING:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase$Status;

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->connectingNetworkId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 390
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->interruptConnecting()V

    .line 393
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    iget v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->connectingNetworkId:I

    invoke-virtual {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeNetwork(I)Z

    .line 394
    iput v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->connectingNetworkId:I

    .line 397
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->reConnectNetwork()V

    .line 400
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->showErrorDlg:Z

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->mModelDialog:Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;

    const-string v1, "idd_wifi_error"

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/DialogProgressWiFiDirectViewModel;->doShow(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, "ActivityConnectSimpleAP"

    const-string v1, "finish():RESULT_CANCELED"

    .line 409
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 410
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setResult(I)V

    const/4 v0, 0x2

    .line 411
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setLastDetailResult(I)V

    .line 412
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->finish()V

    return-void
.end method

.method public onNegativeClick(Ljava/lang/String;)V
    .locals 4

    .line 592
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x33faafcb    # -3.4947284E7f

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_1

    const v1, -0x29423099

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "idd_wifi_profile_failed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "idd_wifi_simpleap"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, -0x1

    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const-string p1, "ActivityConnectSimpleAP"

    const-string v0, "finish():RESULT_CANCELED"

    .line 600
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->onError()V

    .line 602
    invoke-static {v3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setLastDetailResult(I)V

    goto :goto_2

    :pswitch_1
    const-string p1, "ActivityConnectSimpleAP"

    const-string v0, "finish():RESULT_CANCELED"

    .line 594
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    invoke-virtual {p0, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setResult(I)V

    .line 596
    invoke-static {v3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setLastDetailResult(I)V

    .line 597
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->finish()V

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onNeutralClick(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onPositiveClick(Ljava/lang/String;)V
    .locals 4

    .line 561
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x33faafcb    # -3.4947284E7f

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eq v0, v1, :cond_2

    const v1, -0x29423099

    if-eq v0, v1, :cond_1

    const v1, 0x2da5abb4

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "idd_wifi_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "idd_wifi_profile_failed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x2

    goto :goto_1

    :cond_2
    const-string v0, "idd_wifi_simpleap"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p1, -0x1

    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const-string p1, "ActivityConnectSimpleAP"

    const-string v0, "finish():GOTO_WIFI_SETTINGS"

    .line 579
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x5

    .line 580
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->showOsWifiSettings(Landroid/app/Activity;I)V

    goto :goto_2

    :pswitch_1
    const-string p1, "ActivityConnectSimpleAP"

    const-string v0, "finish():GOTO_WIFI_SETTINGS"

    .line 574
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x4

    .line 575
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->showOsWifiSettings(Landroid/app/Activity;I)V

    goto :goto_2

    :pswitch_2
    const-string p1, "ActivityConnectSimpleAP"

    const-string v0, "finish():RESULT_CANCELED"

    .line 564
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    invoke-virtual {p0, v3}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setResult(I)V

    .line 566
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->setLastDetailResult(I)V

    .line 567
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->finish()V

    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method setupObserver()V
    .locals 3

    .line 550
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectBase;->setupObserver()V

    .line 553
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

    .line 554
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 555
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 556
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP$NetworkStateChangeReciever;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.class final enum Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;
.super Ljava/lang/Enum;
.source "WiFiDirectPrinterListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PRINTER_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

.field public static final enum LOCAL:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

.field public static final enum USB:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

.field public static final enum WIFIDIRECT:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 39
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    const-string v1, "USB"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->USB:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    .line 40
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    const-string v1, "LOCAL"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->LOCAL:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    .line 41
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    const-string v1, "WIFIDIRECT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->WIFIDIRECT:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    const/4 v0, 0x3

    .line 38
    new-array v0, v0, [Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->USB:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->LOCAL:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->WIFIDIRECT:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    aput-object v1, v0, v4

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;
    .locals 1

    .line 38
    const-class v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;
    .locals 1

    .line 38
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$PRINTER_TYPE;

    return-object v0
.end method

.class public Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;
.super Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;
.source "ActivityWiFiDirectStart.java"


# static fields
.field private static final ID_ENABLEWIFI:I = 0x1


# instance fields
.field viewiPrintConnect:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;Z)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->startWifiManualSetup(Z)V

    return-void
.end method

.method private startWifiManualSetup(Z)V
    .locals 1

    .line 73
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectManual;->getStartIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object p1

    const/16 v0, 0xa

    .line 74
    invoke-virtual {p0, p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 103
    invoke-super {p0, p1, p2, p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_1

    .line 109
    new-instance p1, Landroid/content/Intent;

    const-class p2, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnectStart;

    invoke-direct {p1, p0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 p2, 0xa

    .line 110
    invoke-virtual {p0, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 29
    invoke-super {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00cb

    .line 31
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->setContentView(I)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    .line 32
    invoke-virtual {p0, p1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->setupCommonHeaderControl(ZZ)V

    const p1, 0x7f080396

    .line 35
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    const v0, 0x7f0801ac

    .line 38
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->viewiPrintConnect:Landroid/view/View;

    .line 39
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->viewiPrintConnect:Landroid/view/View;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080393

    .line 55
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 56
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080227

    .line 63
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 64
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart$3;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart$3;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onResume()V
    .locals 0

    .line 80
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onResume()V

    return-void
.end method

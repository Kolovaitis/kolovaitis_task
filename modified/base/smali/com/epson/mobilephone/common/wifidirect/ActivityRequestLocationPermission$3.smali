.class Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;
.super Ljava/lang/Object;
.source "ActivityRequestLocationPermission.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->checkLocationPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback<",
        "Lcom/google/android/gms/location/LocationSettingsResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;)V
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/api/Result;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 330
    check-cast p1, Lcom/google/android/gms/location/LocationSettingsResult;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;->onResult(Lcom/google/android/gms/location/LocationSettingsResult;)V

    return-void
.end method

.method public onResult(Lcom/google/android/gms/location/LocationSettingsResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/location/LocationSettingsResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 334
    invoke-virtual {p1}, Lcom/google/android/gms/location/LocationSettingsResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object p1

    .line 335
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/16 p1, 0x2136

    if-eq v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "ActivityRequestLocationPermission"

    const-string v0, "Receive LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE;"

    .line 352
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    .line 343
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/common/api/Status;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 346
    invoke-virtual {p1}, Landroid/content/IntentSender$SendIntentException;->printStackTrace()V

    .line 347
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onError()V

    goto :goto_0

    .line 338
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission$3;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->onSuccess()V

    :goto_0
    return-void
.end method

.class public Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;
.super Ljava/lang/Object;
.source "NfcTagUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;
    }
.end annotation


# static fields
.field private static final FIXEDTAG_EPS_V1:Ljava/lang/String; = "EPS_V1"

.field private static final FIXEDTAG_EPS_V2:Ljava/lang/String; = "EPS_V2"

.field private static final ID_CL:Ljava/lang/String; = "CL"

.field private static final ID_EPSON:Ljava/lang/String; = "EPSON"

.field private static final ID_MACADDRESS:Ljava/lang/String; = "MAC_ADDRESS"

.field private static final MIME_TEXTPLAIN:Ljava/lang/String; = "text/plain"

.field private static final PREFIX_FIXEDTAG:Ljava/lang/String; = "EPS_V"

.field private static final PREFIX_TAGWITHNTERFACE:Ljava/lang/String; = "EPS_I"

.field private static final TAG:Ljava/lang/String; = "NfcTagUtils"

.field private static final TYPE_ANDROID_COM_PKG:Ljava/lang/String; = "android.com:pkg"

.field public static final VIBE_TIME:I = 0x64


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static IsCraigDempsey(Ljava/lang/String;)Z
    .locals 9

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    .line 450
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xc

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 455
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAdressBytes(Ljava/lang/String;)[B

    move-result-object v1

    const-string v2, "9CAED3"

    .line 458
    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    const/4 v2, 0x1

    if-ne p0, v2, :cond_2

    const/4 p0, 0x5

    .line 460
    aget-byte p0, v1, p0

    int-to-long v3, p0

    const-wide/16 v5, 0xff

    and-long/2addr v3, v5

    const/4 p0, 0x4

    aget-byte p0, v1, p0

    int-to-long v7, p0

    and-long/2addr v7, v5

    const/16 p0, 0x8

    shl-long/2addr v7, p0

    add-long/2addr v3, v7

    const/4 p0, 0x3

    aget-byte p0, v1, p0

    int-to-long v7, p0

    and-long/2addr v5, v7

    const/16 p0, 0x10

    shl-long/2addr v5, p0

    add-long/2addr v3, v5

    const-wide/32 v5, 0xb68000

    cmp-long p0, v5, v3

    if-gtz p0, :cond_1

    const-wide/32 v5, 0xd31fff

    cmp-long p0, v5, v3

    if-ltz p0, :cond_1

    return v2

    :cond_1
    return v0

    :cond_2
    return v0

    :cond_3
    :goto_0
    return v0
.end method

.method public static disableForegroundDispatch(Landroid/app/Activity;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 220
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 221
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {v0, p0}, Landroid/nfc/NfcAdapter;->disableForegroundDispatch(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public static enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 203
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 204
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 206
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    .line 207
    invoke-static {p0, v2, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 208
    invoke-virtual {v0, p0, v1, p1, p2}, Landroid/nfc/NfcAdapter;->enableForegroundDispatch(Landroid/app/Activity;Landroid/app/PendingIntent;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static findNdefMessage([BLandroid/nfc/NdefMessage;)Landroid/nfc/NdefRecord;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 408
    invoke-virtual {p1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    .line 409
    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->getId()[B

    move-result-object v3

    .line 410
    invoke-static {p0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getAARPackageName(Landroid/nfc/NdefMessage;)Ljava/lang/String;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 427
    invoke-virtual {p0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object p0

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p0, v1

    const/4 v3, 0x4

    .line 428
    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v4

    if-ne v3, v4, :cond_0

    const-string v3, "android.com:pkg"

    .line 429
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 430
    new-instance p0, Ljava/lang/String;

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>([B)V

    return-object p0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static isExistSimpleApDisabled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    const-string v0, "NfcTagUtils"

    const-string v1, "isExistSimpleApDisabled()"

    .line 479
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getExistSimpleApDisabled(Ljava/lang/String;)I

    move-result p0

    const/4 p1, -0x1

    if-eq p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isExistSimpleApNormal(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    const-string v0, "NfcTagUtils"

    const-string v1, "isExistSimpleApNormal()"

    .line 491
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result p0

    const/4 p1, -0x1

    if-eq p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isNeedChangePrinter(Landroid/content/Context;Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;)Z
    .locals 3

    .line 146
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 152
    :cond_0
    iget-object v1, p2, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->macAdress:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 157
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p0

    const/4 p1, 0x3

    .line 158
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v2

    if-ne p1, v2, :cond_1

    .line 159
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p0

    iget-object p1, p2, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ipAddressV4:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    return v0

    :cond_1
    return v1

    :cond_2
    const-string p1, "printer"

    .line 172
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 173
    iget-object v2, p2, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ssid:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 175
    iget-object p1, p2, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ssid:Ljava/lang/String;

    .line 178
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->isExistSimpleApDisabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_3

    return v1

    .line 184
    :cond_3
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->isExistSimpleApNormal(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_4

    return v1

    :cond_4
    return v0
.end method

.method public static parseNECTag(Landroid/content/Context;Landroid/content/Intent;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 237
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0xe

    if-lt v0, v2, :cond_6

    const-string v0, "android.nfc.extra.NDEF_MESSAGES"

    .line 240
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object p1

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    .line 246
    aget-object p1, p1, v0

    check-cast p1, Landroid/nfc/NdefMessage;

    const-string v0, "EPSON"

    .line 249
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->findNdefMessage([BLandroid/nfc/NdefMessage;)Landroid/nfc/NdefRecord;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    .line 257
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    const-string v0, "EPS_V"

    .line 260
    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    invoke-static {v2, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTagFixed(Ljava/lang/String;Landroid/nfc/NdefMessage;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, "EPS_I"

    .line 262
    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 263
    invoke-static {v2, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTagWithInterface(Ljava/lang/String;Landroid/nfc/NdefMessage;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_3

    return-object v1

    .line 273
    :cond_3
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->getAARPackageName(Landroid/nfc/NdefMessage;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->aarPackageName:Ljava/lang/String;

    const-string p1, "vibrator"

    .line 276
    invoke-virtual {p0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/Vibrator;

    .line 277
    invoke-virtual {p0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result p1

    if-eqz p1, :cond_4

    const-wide/16 v1, 0x64

    .line 278
    invoke-virtual {p0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    :cond_4
    return-object v0

    :cond_5
    return-object v1

    :cond_6
    return-object v1
.end method

.method public static parseNECTagFixed(Ljava/lang/String;Landroid/nfc/NdefMessage;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const-string v0, "MAC_ADDRESS"

    .line 298
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->findNdefMessage([BLandroid/nfc/NdefMessage;)Landroid/nfc/NdefRecord;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 304
    :cond_0
    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v2

    const/4 v3, 0x2

    if-ne v3, v2, :cond_6

    const-string v2, "text/plain"

    .line 305
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v4

    invoke-static {v2, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_3

    .line 310
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    const-string v0, "CL"

    .line 313
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->findNdefMessage([BLandroid/nfc/NdefMessage;)Landroid/nfc/NdefRecord;

    move-result-object p1

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    .line 316
    invoke-virtual {p1}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v4

    if-ne v3, v4, :cond_2

    const-string v3, "text/plain"

    .line 317
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p1}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-ne v3, v0, :cond_2

    .line 319
    :try_start_0
    new-instance v3, Ljava/lang/String;

    invoke-virtual {p1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object p1

    invoke-direct {v3, p1}, Ljava/lang/String;-><init>([B)V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_2
    const/4 p1, 0x0

    .line 327
    :goto_0
    new-instance v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;-><init>()V

    .line 328
    iput-object p0, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->tagVersion:Ljava/lang/String;

    .line 329
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->macAdress:Ljava/lang/String;

    .line 330
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAddressP2P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->macAdressP2P:Ljava/lang/String;

    .line 331
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getSSIDFromMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ssid:Ljava/lang/String;

    if-lt p1, v0, :cond_3

    .line 334
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->acCharacterTblPassphraseCL1:[C

    invoke-static {v2, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getPassFormMacAddress(Ljava/lang/String;[C)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->password:Ljava/lang/String;

    goto :goto_1

    .line 336
    :cond_3
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->acCharacterTblPassphrase:[C

    invoke-static {v2, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getPassFormMacAddress(Ljava/lang/String;[C)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->password:Ljava/lang/String;

    .line 342
    :goto_1
    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->IsCraigDempsey(Ljava/lang/String;)Z

    move-result p1

    if-ne p1, v0, :cond_4

    const-string p0, "000048D400E6"

    .line 343
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->acCharacterTblPassphraseCL1:[C

    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getPassFormMacAddress(Ljava/lang/String;[C)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->initialpassword:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const-string p1, "EPS_V1"

    .line 344
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-ne p0, v0, :cond_5

    const-string p0, "000048D400E6"

    .line 345
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->acCharacterTblPassphrase:[C

    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getPassFormMacAddress(Ljava/lang/String;[C)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->initialpassword:Ljava/lang/String;

    goto :goto_2

    .line 347
    :cond_5
    iput-object v1, v3, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->initialpassword:Ljava/lang/String;

    :goto_2
    return-object v3

    :cond_6
    :goto_3
    return-object v1
.end method

.method public static parseNECTagWithInterface(Ljava/lang/String;Landroid/nfc/NdefMessage;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 358
    invoke-virtual {p1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object p1

    .line 359
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    .line 360
    aget-object p1, p1, v2

    invoke-virtual {p1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object p1

    .line 366
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;-><init>()V

    .line 367
    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->parseTag([B)Z

    move-result p1

    if-nez p1, :cond_0

    return-object v1

    .line 371
    :cond_0
    new-instance p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    invoke-direct {p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;-><init>()V

    .line 373
    iput-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->tagVersion:Ljava/lang/String;

    .line 376
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->getMacAddress()Ljava/lang/String;

    move-result-object p0

    iput-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->macAdress:Ljava/lang/String;

    .line 377
    iget-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->macAdress:Ljava/lang/String;

    if-eqz p0, :cond_1

    .line 378
    iget-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->macAdress:Ljava/lang/String;

    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAddressP2P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->macAdressP2P:Ljava/lang/String;

    .line 382
    :cond_1
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->getSSID()Ljava/lang/String;

    move-result-object p0

    iput-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ssid:Ljava/lang/String;

    .line 385
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->getPassWord()Ljava/lang/String;

    move-result-object p0

    iput-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->password:Ljava/lang/String;

    .line 388
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->getIPAddressV4()Ljava/lang/String;

    move-result-object p0

    iput-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ipAddressV4:Ljava/lang/String;

    .line 391
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->getIPAddressV4SimpleAP()Ljava/lang/String;

    move-result-object p0

    iput-object p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ipAddressV4SimpleAP:Ljava/lang/String;

    .line 394
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagParser;->getPrinterStatus()I

    move-result p0

    iput p0, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->printerStatus:I

    return-object p1

    :cond_2
    return-object v1
.end method

.class public Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;
.super Ljava/lang/Object;
.source "WifiP2pUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PrimaryDeviceType"
.end annotation


# static fields
.field private static final DEMIRITA_WIFI_OUI:Ljava/lang/String; = "0050F204"

.field public static final P2P_CATEGORY_PRINTER:I = 0x3

.field public static final P2P_SUBCATEGORY_MFP:I = 0x5

.field public static final P2P_SUBCATEGORY_PRINTER:I = 0x1


# instance fields
.field private categoryId:I

.field private subCategoryId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 356
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->categoryId:I

    .line 357
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->subCategoryId:I

    return-void
.end method

.method public static isPrinter(Ljava/lang/String;)Z
    .locals 3

    .line 366
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->parsePrimaryDeviceType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;

    move-result-object p0

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x3

    .line 371
    iget v2, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->categoryId:I

    if-eq v1, v2, :cond_1

    return v0

    .line 376
    :cond_1
    iget p0, p0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->subCategoryId:I

    const/4 v1, 0x1

    if-eq v1, p0, :cond_2

    const/4 v2, 0x5

    if-eq v2, p0, :cond_2

    return v0

    :cond_2
    return v1
.end method

.method private static parsePrimaryDeviceType(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;
    .locals 4

    .line 392
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;-><init>()V

    const/4 v1, 0x0

    .line 393
    iput v1, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->categoryId:I

    .line 394
    iput v1, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->subCategoryId:I

    if-eqz p0, :cond_2

    .line 396
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 411
    :cond_0
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    const-string v2, "[^(0-9)^(A-Z)]"

    const-string v3, ""

    .line 414
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v2, "0050F204"

    .line 417
    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    return-object v0

    .line 423
    :cond_1
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v2, 0x8

    .line 424
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    const/16 v2, 0x10

    .line 426
    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->categoryId:I

    .line 427
    invoke-static {p0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result p0

    iput p0, v0, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$PrimaryDeviceType;->subCategoryId:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 429
    invoke-virtual {p0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    :goto_0
    return-object v0

    :cond_2
    :goto_1
    return-object v0
.end method

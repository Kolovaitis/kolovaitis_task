.class Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;
.super Ljava/lang/Object;
.source "SearchWiFiDirectPrinterTask.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3

    .line 226
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 231
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getConnectionInfo()Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 234
    iget-object v1, p1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->p2PMacAdder:Ljava/lang/String;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils$ConnectionInfo;->printerName:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl;->p2pAddr2PtrAddr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 237
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrFilter;->isExcludedMacAddress(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 238
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iput-boolean v0, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->bDisConnectP2P:Z

    .line 243
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->mWiFiP2PManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;->this$0:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->p2pChannnel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask$1;)V

    invoke-virtual {p1, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    :goto_0
    return v0
.end method

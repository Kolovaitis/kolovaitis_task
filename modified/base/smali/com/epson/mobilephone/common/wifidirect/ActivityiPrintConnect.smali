.class public Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;
.super Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;
.source "ActivityiPrintConnect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;,
        Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;
    }
.end annotation


# static fields
.field public static final FINISH_EAYSETUP:Ljava/lang/String; = "easysetup"

.field private static final MACADRESS_SIZE:I = 0x6

.field private static final MAX_RETRY:I = 0x5

.field private static final RUNDOM_SIZE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "ActivityiPrintConnect"

.field private static final TIMEOUT_CONNECTING_REALAP:I = 0x3c

.field private static final TIMEOUT_CONNECTING_TESTAP:I = 0x3c

.field private static final WAIT_CONNECTREALAP:I = 0x78

.field private static final WAIT_RETRY:I = 0x2

.field private static engineId:[B


# instance fields
.field private final CONNECT_REALAP:I

.field private final CONNECT_TESTAP:I

.field btnFinish:Landroid/widget/Button;

.field intentResult:Landroid/content/Intent;

.field isKeepSimpleAPConnection:Z

.field lastErrorStrings:Ljava/lang/String;

.field mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

.field mWifiManager:Landroid/net/wifi/WifiManager;

.field networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;

.field progress:Landroid/widget/ProgressBar;

.field settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

.field settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

.field private status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

.field taskGetConnectStrings:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field textView1:Landroid/widget/TextView;

.field textView2:Landroid/widget/TextView;

.field textView3:Landroid/widget/TextView;

.field toolbar:Landroid/view/ViewGroup;

.field waitRealAp:Ljava/lang/Object;

.field wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 33
    invoke-direct {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;-><init>()V

    const/4 v0, 0x1

    .line 50
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->CONNECT_TESTAP:I

    const/4 v0, 0x2

    .line 51
    iput v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->CONNECT_REALAP:I

    .line 66
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->IDLE:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    const/4 v0, 0x0

    .line 71
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 72
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    .line 77
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/escprLib;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    .line 82
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    .line 83
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    .line 88
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->intentResult:Landroid/content/Intent;

    const/4 v1, 0x0

    .line 89
    iput-boolean v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->isKeepSimpleAPConnection:Z

    const-string v1, ""

    .line 99
    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->lastErrorStrings:Ljava/lang/String;

    .line 104
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->taskGetConnectStrings:Landroid/os/AsyncTask;

    .line 109
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->waitRealAp:Ljava/lang/Object;

    .line 119
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;

    return-void
.end method

.method static synthetic access$002(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;)Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    return-object p1
.end method

.method static synthetic access$100()[B
    .locals 1

    .line 33
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->engineId:[B

    return-object v0
.end method


# virtual methods
.method disconnectRealAp()V
    .locals 2

    .line 192
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    .line 194
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeSimpleAP(I)Z

    return-void
.end method

.method getEngineId()[B
    .locals 7

    .line 576
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 579
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x4

    if-ge v3, v4, :cond_0

    .line 581
    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 589
    :cond_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x6

    if-eqz v1, :cond_1

    const-string v4, "ActivityiPrintConnect"

    .line 591
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getEngineId macAdress = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, ":"

    .line 592
    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 594
    array-length v4, v1

    if-ne v4, v3, :cond_2

    :goto_1
    if-ge v2, v3, :cond_2

    .line 596
    aget-object v4, v1, v2

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 600
    :cond_1
    new-array v1, v3, [B

    fill-array-data v1, :array_0

    .line 601
    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 611
    :cond_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 278
    invoke-super {p0, p1, p2, p3}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onActivityResult(IILandroid/content/Intent;)V

    .line 281
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurNetworkId()I

    move-result v0

    .line 282
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    invoke-virtual {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getSSID(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    if-ne p2, v1, :cond_2

    .line 309
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 311
    iput-object p3, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->intentResult:Landroid/content/Intent;

    .line 314
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->intentResult:Landroid/content/Intent;

    const-string p2, "easysetup"

    const/4 p3, 0x1

    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 317
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    .line 321
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED_OTHERAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    goto :goto_0

    .line 325
    :cond_1
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    goto :goto_0

    .line 329
    :cond_2
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    goto :goto_0

    :pswitch_1
    if-ne p2, v1, :cond_5

    .line 287
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 291
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->startConnectRealAp()V

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    .line 295
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTED_OTHERAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    goto :goto_0

    .line 299
    :cond_4
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    goto :goto_0

    .line 303
    :cond_5
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCancelPressed()V
    .locals 0

    .line 181
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCancelPressed()V

    .line 184
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->disconnectRealAp()V

    return-void
.end method

.method onConnectError()V
    .locals 3

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "onConnectError()"

    .line 381
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->toolbar:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 385
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/4 v0, 0x1

    .line 388
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->setDisplayMenu(Z)V

    .line 390
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0477

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0478

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView3:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0479

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method onConnectOtherAP()V
    .locals 3

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "onConnectOtherAP()"

    .line 362
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->toolbar:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 366
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/4 v0, 0x1

    .line 369
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->setDisplayMenu(Z)V

    .line 371
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e047a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e047b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView3:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e047c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method onConnectSimpleAP()V
    .locals 7

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "onConnectSimpleAP()"

    .line 341
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->toolbar:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->progress:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/4 v0, 0x1

    .line 348
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->setDisplayMenu(Z)V

    .line 350
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e047d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->intentResult:Landroid/content/Intent;

    const-string v6, "name"

    .line 352
    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    .line 353
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurNetworkId()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getSSID(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const v0, 0x7f0e047e

    .line 351
    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView3:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e047f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 124
    invoke-super {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onCreate(Landroid/os/Bundle;)V

    .line 127
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 128
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    .line 131
    sget-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->engineId:[B

    if-nez p1, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getEngineId()[B

    move-result-object p1

    sput-object p1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->engineId:[B

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a0081

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 137
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->setContentView(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 138
    invoke-virtual {p0, v0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->setupCommonHeaderControl(ZZ)V

    const v0, 0x7f08034d

    .line 141
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->toolbar:Landroid/view/ViewGroup;

    const v0, 0x7f080096

    .line 142
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->btnFinish:Landroid/widget/Button;

    const v0, 0x7f080326

    .line 143
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView1:Landroid/widget/TextView;

    const v0, 0x7f080327

    .line 144
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView2:Landroid/widget/TextView;

    const v0, 0x7f080329

    .line 145
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->textView3:Landroid/widget/TextView;

    const v0, 0x7f080296

    .line 146
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->progress:Landroid/widget/ProgressBar;

    .line 149
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->btnFinish:Landroid/widget/Button;

    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$1;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mEscprLib:Lcom/epson/mobilephone/common/wifidirect/escprLib;

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/escprLib;->epsWrapperGetSetupConnectStrings(Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;)I

    return-void
.end method

.method public onFinishPressed()V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->intentResult:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v1, -0x1

    .line 167
    invoke-virtual {p0, v1, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->setResult(ILandroid/content/Intent;)V

    const/4 v0, 0x1

    .line 168
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->isKeepSimpleAPConnection:Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 170
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->setResult(I)V

    .line 172
    :goto_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->finish()V

    return-void
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "onPause()"

    .line 233
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onPause()V

    .line 237
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->GETTING_GONNECTSTRINGS:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    if-ne v0, v1, :cond_0

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "interruputGetConnectSttings()"

    .line 238
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECT_ERROR:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    .line 244
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->taskGetConnectStrings:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 245
    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    const/4 v0, 0x0

    .line 246
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->taskGetConnectStrings:Landroid/os/AsyncTask;

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->wiFiUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v1, v1, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getNetworkId(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeSimpleAP(I)Z

    .line 254
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->isKeepSimpleAPConnection:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v0, v0, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 255
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->disconnectRealAp()V

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 199
    invoke-super {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectBase;->onResume()V

    const/4 v0, 0x0

    .line 201
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->isKeepSimpleAPConnection:Z

    .line 203
    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$3;->$SwitchMap$com$epson$mobilephone$common$wifidirect$ActivityiPrintConnect$Status:[I

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 224
    :pswitch_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->onConnectError()V

    goto :goto_0

    .line 220
    :pswitch_1
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->onConnectOtherAP()V

    goto :goto_0

    .line 216
    :pswitch_2
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->onConnectSimpleAP()V

    goto :goto_0

    .line 206
    :pswitch_3
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->setDisplayMenu(Z)V

    .line 207
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->toolbar:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 210
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;->CONNECTING_TEMPAP:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->status:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$Status;

    .line 211
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->startConnectTempAp()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method onScanResultAvailable()V
    .locals 3

    .line 620
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 622
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 625
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 629
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 631
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    .line 632
    iget-object v1, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-static {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->removeQuotationsInSSID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 633
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsRealAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "Found AP"

    .line 635
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->waitRealAp:Ljava/lang/Object;

    monitor-enter v1

    .line 639
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->waitRealAp:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 640
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 648
    :cond_1
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;-><init>()V

    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiScanner;->startScan(Landroid/net/wifi/WifiManager;)Z

    return-void
.end method

.method registBroadcastReciever()V
    .locals 3

    .line 262
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;

    if-nez v0, :cond_0

    .line 263
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;

    .line 264
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method startConnectRealAp()V
    .locals 2

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "startConnectRealAp()"

    .line 421
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;-><init>(Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 562
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->taskGetConnectStrings:Landroid/os/AsyncTask;

    return-void
.end method

.method startConnectTempAp()V
    .locals 5

    const-string v0, "ActivityiPrintConnect"

    const-string v1, "startConnectTempAp()"

    .line 401
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectSimpleAP;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.epson.iprint.wifidirect.apname"

    .line 405
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->ssid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.epson.iprint.wifidirect.appass"

    .line 406
    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->settingsTempAp:Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;

    iget-object v2, v2, Lcom/epson/mobilephone/common/wifidirect/escprLib$ConnectStrings;->password:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.epson.iprint.wifidirect.create"

    const/4 v2, 0x1

    .line 407
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.epson.iprint.wifidirect.showerror"

    const/4 v3, 0x0

    .line 408
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.epson.iprint.wifidirect.showconnecttip"

    .line 409
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.epson.iprint.wifidirect.timeout"

    const/16 v4, 0x3c

    .line 410
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.epson.iprint.wifidirect.needinfo"

    .line 411
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 413
    invoke-virtual {p0, v0, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method unRegistBroadcastReciever()V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    .line 272
    iput-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect;->networkStateChangeReciever:Lcom/epson/mobilephone/common/wifidirect/ActivityiPrintConnect$NetworkStateChangeReciever;

    :cond_0
    return-void
.end method

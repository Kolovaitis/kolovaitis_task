.class public final enum Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;
.super Ljava/lang/Enum;
.source "WiFiControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/WiFiControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

.field public static final enum NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

.field public static final enum SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

.field public static final enum WiFiP2P:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 34
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    const-string v1, "SimpleAP"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    .line 35
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    const-string v1, "WiFiP2P"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->WiFiP2P:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    .line 36
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    const-string v1, "NONE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    const/4 v0, 0x3

    .line 33
    new-array v0, v0, [Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->SimpleAP:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->WiFiP2P:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->NONE:Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;
    .locals 1

    .line 33
    const-class v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;
    .locals 1

    .line 33
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->$VALUES:[Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/wifidirect/WiFiControl$ConnectType;

    return-object v0
.end method

.class public Lcom/epson/mobilephone/common/wifidirect/EPLog;
.super Ljava/lang/Object;
.source "EPLog.java"


# static fields
.field private static isDebuggable:Z = false


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 33
    sget-boolean v0, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 34
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static final e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 40
    sget-boolean v0, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 41
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static final i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 47
    sget-boolean v0, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 48
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static isDebuggable(Landroid/content/Context;)Z
    .locals 2

    .line 16
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    .line 19
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 21
    :catch_0
    sput-boolean v1, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable:Z

    const/4 p0, 0x0

    .line 23
    :goto_0
    iget p0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    const/4 v0, 0x2

    and-int/2addr p0, v0

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    .line 24
    sput-boolean p0, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable:Z

    .line 28
    :cond_0
    sget-boolean p0, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable:Z

    return p0
.end method

.method public static final v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 54
    sget-boolean v0, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 55
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static final w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 61
    sget-boolean v0, Lcom/epson/mobilephone/common/wifidirect/EPLog;->isDebuggable:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 62
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

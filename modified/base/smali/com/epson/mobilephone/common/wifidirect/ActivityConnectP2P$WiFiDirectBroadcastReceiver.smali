.class Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ActivityConnectP2P.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WiFiDirectBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)V
    .locals 0

    .line 639
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .line 644
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ActivityConnectP2P"

    .line 645
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WiFiDirectBroadcastReceiver:onReceive() action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    .line 647
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "p2pGroupInfo"

    .line 648
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p2

    check-cast p2, Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 649
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WifiP2pUtils;->getP2PPrinterInfo(Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object p1

    const-string v0, "ActivityConnectP2P"

    .line 650
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EXTRA_WIFI_P2P_GROUP = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 653
    iget-object p2, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    invoke-static {p2}, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->access$300(Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object p2

    iget-object p2, p2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    iget-object v0, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p2, "ActivityConnectP2P"

    .line 654
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "P2P Established "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz p1, :cond_0

    .line 657
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    .line 658
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    const/4 p2, 0x0

    iput-object p2, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 660
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P$WiFiDirectBroadcastReceiver;->this$0:Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/ActivityConnectP2P;->mHandler:Landroid/os/Handler;

    const/16 p2, 0xa

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method

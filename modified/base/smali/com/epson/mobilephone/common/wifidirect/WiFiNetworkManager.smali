.class public Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;
.super Ljava/lang/Object;
.source "WiFiNetworkManager.java"


# annotations
.annotation build Landroid/support/annotation/RequiresApi;
    api = 0x15
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WiFiNetworkManager"

.field private static instance:Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;


# instance fields
.field connectivityManager:Landroid/net/ConnectivityManager;

.field context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->context:Landroid/content/Context;

    const-string v0, "connectivity"

    .line 22
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    iput-object p1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->connectivityManager:Landroid/net/ConnectivityManager;

    return-void
.end method

.method public static getInsetance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 26
    sget-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;

    .line 29
    :cond_0
    sget-object p0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->instance:Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;

    return-object p0
.end method


# virtual methods
.method protected getWifiNetwork()Landroid/net/Network;
    .locals 6

    .line 39
    iget-object v0, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworks()[Landroid/net/Network;

    move-result-object v0

    .line 40
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 41
    iget-object v4, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v4, v3}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v4

    const/4 v5, 0x1

    .line 42
    invoke-virtual {v4, v5}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public isWiFiValidated()Z
    .locals 4
    .annotation build Landroid/support/annotation/RequiresApi;
        api = 0x17
    .end annotation

    .line 57
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->getWifiNetwork()Landroid/net/Network;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v1, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v0}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v0

    const/16 v1, 0x10

    .line 61
    invoke-virtual {v0, v1}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "WiFiNetworkManager"

    .line 62
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "capabilities :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/net/NetworkCapabilities;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "WiFiNetworkManager"

    .line 63
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isWiFiValidated = OK : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string v0, "WiFiNetworkManager"

    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isWiFiValidated = NG :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiUtils;->getCurSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/wifidirect/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

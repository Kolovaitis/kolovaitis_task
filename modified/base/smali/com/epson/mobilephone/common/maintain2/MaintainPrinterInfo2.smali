.class public Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;
.super Ljava/lang/Object;
.source "MaintainPrinterInfo2.java"


# static fields
.field private static final EPS_INK_NUM:I = 0x14

.field private static final EPS_STATUS_NUM:I = 0x3

.field public static final ESCPR_INK_STATUS_END:I = 0x2

.field public static final ESCPR_INK_STATUS_FAIL:I = -0x2

.field public static final ESCPR_INK_STATUS_LOW:I = 0x1

.field public static final ESCPR_INK_STATUS_NOREAD:I = -0x4

.field public static final ESCPR_INK_STATUS_NORMAL:I = 0x0

.field public static final ESCPR_INK_STATUS_NOTAVAIL:I = -0x3

.field public static final ESCPR_INK_STATUS_NOTPRESENT:I = -0x1


# instance fields
.field private mInkColorCode:[I

.field private mInkColorName:[I

.field private mInkNum:I

.field private mInkRemeiningAmount:[I

.field private mInkStatus:[I

.field private mStatus:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 50
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mStatus:[I

    const/16 v0, 0x14

    .line 54
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkColorCode:[I

    .line 55
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkColorName:[I

    .line 56
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkRemeiningAmount:[I

    .line 57
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkStatus:[I

    return-void
.end method


# virtual methods
.method public getInkCode(I)I
    .locals 1

    if-ltz p1, :cond_0

    .line 77
    iget v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkNum:I

    if-gt p1, v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkColorCode:[I

    aget p1, v0, p1

    return p1

    .line 78
    :cond_0
    new-instance p1, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method public getInkName(I)I
    .locals 1

    if-ltz p1, :cond_0

    .line 84
    iget v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkNum:I

    if-gt p1, v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkColorName:[I

    aget p1, v0, p1

    return p1

    .line 85
    :cond_0
    new-instance p1, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method public getInkNum()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkNum:I

    return v0
.end method

.method public getInkRemainingAmount(I)I
    .locals 1

    if-ltz p1, :cond_1

    .line 91
    iget v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkNum:I

    if-gt p1, v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkRemeiningAmount:[I

    aget p1, v0, p1

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    return p1

    .line 92
    :cond_1
    new-instance p1, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method public getInkStatus(I)I
    .locals 2

    if-ltz p1, :cond_1

    .line 104
    iget v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkNum:I

    if-gt p1, v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mInkStatus:[I

    array-length v1, v0

    if-le p1, v1, :cond_0

    const/4 p1, -0x2

    return p1

    .line 110
    :cond_0
    aget p1, v0, p1

    return p1

    .line 105
    :cond_1
    new-instance p1, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method public getMStatus()[I
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mStatus:[I

    return-object v0
.end method

.method public setMStatus([I)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->mStatus:[I

    return-void
.end method

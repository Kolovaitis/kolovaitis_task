.class public Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;
.super Ljava/lang/Object;
.source "BatteryInfoEx.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;",
            ">;"
        }
    .end annotation
.end field

.field public static final EPS_BATTERY_FIXED:I = 0x1

.field public static final EPS_BATTERY_REMOVABLE_EXTERNAL:I = 0x3

.field public static final EPS_BATTERY_REMOVABLE_INTEARNAL:I = 0x2

.field public static final EPS_BATTERY_TYPE_UNKNOWN:I = 0x0

.field public static final EPS_CHARGE_STATE_CHARGING:I = 0x1

.field public static final EPS_CHARGE_STATE_NONE:I = 0x0

.field public static final EPS_CHARGE_STATE_NOT_INSERTED:I = 0x2

.field public static final EPS_POWER_SOUECE_AC:I = 0x1

.field public static final EPS_POWER_SOUECE_BATTERY:I = 0x2

.field public static final EPS_POWER_SOUECE_BATTERY2:I = 0x3

.field public static final EPS_POWER_SOUECE_NOT_SUPPORTED:I = -0x1

.field public static final EPS_POWER_SOUECE_UNKNOWN:I


# instance fields
.field public batteryRemain:[I

.field public batteryState:[I

.field public batteryType:[I

.field public number:I

.field public powerSourceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    new-instance v0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx$1;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx$1;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->powerSourceType:I

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->number:I

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryType:[I

    .line 69
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryType:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryState:[I

    .line 72
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryState:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryRemain:[I

    .line 75
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryRemain:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx$1;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 86
    iget p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->powerSourceType:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->number:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    iget-object p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryType:[I

    array-length p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget-object p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryType:[I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 92
    iget-object p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryState:[I

    array-length p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget-object p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryState:[I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 95
    iget-object p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryRemain:[I

    array-length p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget-object p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryRemain:[I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    return-void
.end method

.class public Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
.super Ljava/lang/Object;
.source "MaintainPrinter2.java"


# static fields
.field public static final STATUS_BUSY:I = 0x2

.field public static final STATUS_CANCELLING:I = 0x3

.field public static final STATUS_ERROR:I = 0x4

.field public static final STATUS_IDEL:I = 0x0

.field public static final STATUS_PRINTING:I = 0x1

.field private static lockObj:Ljava/lang/Object;

.field private static final maintainPrinter_INSTANCE:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;


# instance fields
.field protected mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

.field protected mPrinterInfor:Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

.field protected mSearchPos:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->maintainPrinter_INSTANCE:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getInstance()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    .line 33
    new-instance v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mSearchPos:I

    return-void
.end method

.method public static getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 1

    .line 42
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->maintainPrinter_INSTANCE:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-object v0
.end method


# virtual methods
.method public doCancelFindPrinter()I
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setSearchStt(Z)V

    .line 100
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperCancelFindPrinter()I

    move-result v0

    return v0
.end method

.method public doDoMainteCmd(I)I
    .locals 3

    .line 113
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    const-string v1, "MaintainPrinter2"

    const-string v2, "**********doDoMainteCmd************"

    .line 114
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperDoMainteCmd(I)I

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 116
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public doFindPrinter(II)I
    .locals 4

    .line 74
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    const-string v1, "MaintainPrinter2"

    const-string v2, "**********Call do find printer************"

    .line 75
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setSearchStt(Z)V

    .line 77
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->resetIPAdressCheck()V

    const-string v1, "MaintainPrinter2"

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "findType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->doFindPrinter(II)I

    move-result p1

    const-string p2, "MaintainPrinter2"

    const-string v1, "**********Finish do find printer************"

    .line 81
    invoke-static {p2, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 83
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public doGetId()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public doGetInkInfo()I
    .locals 3

    .line 125
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v2, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetInkInfo2(Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;)I

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 127
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public doGetIp()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetIp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public doGetLang()I
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_lang()I

    move-result v0

    return v0
.end method

.method public doGetStatus()I
    .locals 3

    .line 108
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v2, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetStatus([I)I

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 110
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public doInitDriver(Landroid/content/Context;I)I
    .locals 2

    const-string v0, "MaintainPrinter2"

    const-string v1, " @@@@@@@    doInitDriver                 @"

    .line 65
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->is64bit()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Native is operating at \u2606 64 bit !!"

    goto :goto_0

    :cond_0
    const-string v0, "Native is operating at 32 bit"

    :goto_0
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperInitDriver(Landroid/content/Context;I)I

    move-result p1

    return p1
.end method

.method public doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I
    .locals 3

    .line 131
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    const-string v1, "MaintainPrinter2"

    const-string v2, "**********Call doProbePrinter************"

    .line 132
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->resetIPAdressCheck()V

    .line 134
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperProbePrinter2(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-gez p1, :cond_0

    const-string p2, "MaintainPrinter2"

    .line 136
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "ret="

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string p2, "MaintainPrinter2"

    const-string p3, "**********Finish doProbePrinter************"

    .line 138
    invoke-static {p2, p3}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 140
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public doReleaseDriver()I
    .locals 2

    const-string v0, "MaintainPrinter2"

    const-string v1, " xxxxxxx    doReleaseDriver                x"

    .line 70
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->release_driver()I

    move-result v0

    return v0
.end method

.method public doSetPrinter()I
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mSearchPos:I

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->set_printer(I)I

    move-result v0

    return v0
.end method

.method public getBatteryInfo(Lcom/epson/mobilephone/common/maintain2/BatteryInfo;)I
    .locals 2

    .line 180
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getBatteryInfo(Lcom/epson/mobilephone/common/maintain2/BatteryInfo;)I

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 182
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getBatteryInfo(Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)I
    .locals 2

    .line 186
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 187
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getBatteryInfoEx(Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)I

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 188
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getEmaStatus([I)I
    .locals 2

    .line 202
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 203
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetEmaStatus([I)I

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 204
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getFirmwareInfo(Ljava/util/EnumMap;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap<",
            "Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .line 174
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 175
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getFirmwareInfo(Ljava/util/EnumMap;)I

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 176
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    return-object v0
.end method

.method public getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    return-object v0
.end method

.method public getMSearchPos()I
    .locals 1

    .line 59
    iget v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mSearchPos:I

    return v0
.end method

.method public getMaintenanceBoxInformation()[I
    .locals 2

    .line 196
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 197
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetMaintenanceBoxInformation()[I

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 198
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getMediaInfo(II)[I
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetMediaInfo(II)[I

    move-result-object p1

    return-object p1
.end method

.method public getPaperInfo()[I
    .locals 2

    .line 168
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 169
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetPaperInfo()[I

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 170
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method getStatus()[I
    .locals 1

    .line 154
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object v0

    return-object v0
.end method

.method public setEmaStatus(I)I
    .locals 2

    .line 208
    sget-object v0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 209
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mEscpLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperSetEmaStatus(I)I

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 210
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setMSearchPos(I)V
    .locals 0

    .line 46
    iput p1, p0, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->mSearchPos:I

    return-void
.end method

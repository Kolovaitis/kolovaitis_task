.class public Lcom/epson/mobilephone/common/maintain2/BatteryInfo;
.super Ljava/lang/Object;
.source "BatteryInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/epson/mobilephone/common/maintain2/BatteryInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final EPS_CHARGE_STATE_CHARGING:I = 0x1

.field public static final EPS_CHARGE_STATE_NONE:I = 0x0

.field public static final EPS_POWER_SOUECE_AC:I = 0x1

.field public static final EPS_POWER_SOUECE_BATTERY:I = 0x2

.field public static final EPS_POWER_SOUECE_NOT_SUPPORTED:I = -0x1

.field public static final EPS_POWER_SOUECE_UNKNOWN:I


# instance fields
.field public batteryChargeState:I

.field public batteryRemain:I

.field public powerSourceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 44
    new-instance v0, Lcom/epson/mobilephone/common/maintain2/BatteryInfo$1;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain2/BatteryInfo$1;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->powerSourceType:I

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->batteryChargeState:I

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->batteryRemain:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/epson/mobilephone/common/maintain2/BatteryInfo$1;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 71
    iget p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->powerSourceType:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    iget p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->batteryChargeState:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 73
    iget p2, p0, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->batteryRemain:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

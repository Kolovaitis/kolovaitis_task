.class public final enum Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;
.super Ljava/lang/Enum;
.source "EscprLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/EscprLib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PrinterFirmInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

.field public static final enum FY:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

.field public static final enum MainVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

.field public static final enum MarketID:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

.field public static final enum NetVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

.field public static final enum NicFlg:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

.field public static final enum UrlVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 549
    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    const-string v1, "NicFlg"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->NicFlg:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    const-string v1, "NetVer"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->NetVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    const-string v1, "MainVer"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MainVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    const-string v1, "FY"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->FY:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    const-string v1, "MarketID"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MarketID:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    const-string v1, "UrlVer"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->UrlVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    const/4 v0, 0x6

    .line 548
    new-array v0, v0, [Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->NicFlg:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->NetVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MainVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->FY:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    aput-object v1, v0, v5

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MarketID:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    aput-object v1, v0, v6

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->UrlVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    aput-object v1, v0, v7

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->$VALUES:[Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 548
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;
    .locals 1

    .line 548
    const-class v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;
    .locals 1

    .line 548
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->$VALUES:[Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    return-object v0
.end method

.class public Lcom/epson/mobilephone/common/escpr/MIBCommand;
.super Ljava/lang/Object;
.source "MIBCommand.java"


# static fields
.field public static final SNMP_MAX_BUF:I = 0x400

.field public static final SNMP_MAX_OID:I = 0x80


# instance fields
.field public commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

.field public mibIdentifier:Lcom/epson/mobilephone/common/escpr/MIBIdentifier;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MIBIdentifier;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MIBIdentifier;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/MIBCommand;->mibIdentifier:Lcom/epson/mobilephone/common/escpr/MIBIdentifier;

    .line 14
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    return-void
.end method

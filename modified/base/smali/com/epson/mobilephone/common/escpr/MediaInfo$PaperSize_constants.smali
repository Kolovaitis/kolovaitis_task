.class public Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;
.super Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;
.source "MediaInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/MediaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PaperSize_constants"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 19

    .line 447
    invoke-direct/range {p0 .. p0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;-><init>()V

    .line 448
    new-instance v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;-><init>()V

    .line 449
    invoke-static {}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getInstance()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_printable_area_info_all(Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;)I

    move-result v1

    if-ltz v1, :cond_4

    const/4 v2, 0x0

    .line 451
    :goto_0
    iget v3, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->numSizes:I

    if-ge v2, v3, :cond_3

    .line 452
    iget-object v3, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v3, v3, v2

    iget v5, v3, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->paperWidth:I

    .line 453
    iget-object v3, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v3, v3, v2

    iget v6, v3, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->paperHeight:I

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 467
    :goto_1
    iget-object v4, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v4, v4, v2

    iget v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->numLayouts:I

    if-ge v3, v4, :cond_2

    .line 468
    iget-object v4, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v4, v4, v2

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v4, v4, v3

    iget v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->layout:I

    const/4 v1, 0x2

    if-ne v4, v1, :cond_0

    .line 469
    iget-object v1, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v1, v1, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->left:I

    sub-int v1, v5, v1

    iget-object v4, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v4, v4, v2

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v4, v4, v3

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->right:I

    sub-int/2addr v1, v4

    .line 471
    iget-object v4, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v4, v4, v2

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v4, v4, v3

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->top:I

    sub-int v4, v6, v4

    iget-object v7, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v7, v7, v2

    iget-object v7, v7, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v7, v7, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->bottom:I

    sub-int/2addr v4, v7

    .line 474
    iget-object v7, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v7, v7, v2

    iget-object v7, v7, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v7, v7, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->left:I

    .line 475
    iget-object v8, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v8, v8, v2

    iget-object v8, v8, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v8, v8, v3

    iget-object v8, v8, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v8, v8, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->top:I

    .line 476
    iget-object v13, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v13, v13, v2

    iget-object v13, v13, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v13, v13, v3

    iget-object v13, v13, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v13, v13, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->right:I

    .line 477
    iget-object v14, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v14, v14, v2

    iget-object v14, v14, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v14, v14, v3

    iget-object v14, v14, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v14, v14, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->bottom:I

    move v15, v13

    move/from16 v16, v14

    move v13, v7

    move v14, v8

    move v7, v1

    move v8, v4

    goto/16 :goto_2

    .line 478
    :cond_0
    iget-object v1, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v1, v1, v3

    iget v1, v1, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->layout:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 479
    iget-object v1, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v1, v1, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->left:I

    sub-int v1, v5, v1

    iget-object v4, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v4, v4, v2

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v4, v4, v3

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->right:I

    sub-int/2addr v1, v4

    .line 481
    iget-object v4, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v4, v4, v2

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v4, v4, v3

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v4, v4, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->top:I

    sub-int v4, v6, v4

    iget-object v9, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v9, v9, v2

    iget-object v9, v9, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v9, v9, v3

    iget-object v9, v9, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v9, v9, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->bottom:I

    sub-int/2addr v4, v9

    .line 484
    iget-object v9, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v9, v9, v2

    iget-object v9, v9, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v9, v9, v3

    iget-object v9, v9, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v9, v9, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->left:I

    .line 485
    iget-object v10, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v10, v10, v2

    iget-object v10, v10, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v10, v10, v3

    iget-object v10, v10, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v10, v10, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->top:I

    .line 486
    iget-object v11, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v11, v11, v2

    iget-object v11, v11, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v11, v11, v3

    iget-object v11, v11, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v11, v11, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->right:I

    .line 487
    iget-object v12, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v12, v12, v2

    iget-object v12, v12, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    aget-object v12, v12, v3

    iget-object v12, v12, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;->margin:Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;

    iget v12, v12, Lcom/epson/mobilephone/common/escpr/EPS_MARGIN;->bottom:I

    move/from16 v17, v11

    move/from16 v18, v12

    move v11, v1

    move v12, v4

    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 491
    :cond_2
    new-instance v1, Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-object v4, v1

    invoke-direct/range {v4 .. v18}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIIIIIIIII)V

    move-object/from16 v3, p0

    .line 497
    iget-object v4, v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    iget-object v5, v0, Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;->sizeList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;

    aget-object v5, v5, v2

    iget v5, v5, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->mediaSizeID:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_3
    move-object/from16 v3, p0

    goto :goto_3

    :cond_4
    move-object/from16 v3, p0

    .line 500
    invoke-direct/range {p0 .. p0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->presetPaperSize()V

    :goto_3
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .line 503
    invoke-direct {p0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;-><init>()V

    .line 504
    invoke-direct {p0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->presetPaperSize()V

    return-void
.end method

.method private presetPaperSize()V
    .locals 12

    .line 510
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xba0

    const/16 v4, 0x1071

    const/16 v5, 0xb4c

    const/16 v6, 0x101d

    const/16 v7, -0x24

    const/16 v8, -0x2a

    const/16 v9, 0xbe8

    const/16 v10, 0x10e1

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xbf4

    const/16 v4, 0xf78

    const/16 v5, 0xba0

    const/16 v6, 0xf24

    const/16 v9, 0xc3c

    const/16 v10, 0xfe8

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v4, 0x13b0

    const/16 v6, 0x135c

    const/16 v10, 0x1420

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x832

    const/16 v4, 0xba0

    const/16 v5, 0x7de

    const/16 v6, 0xb4c

    const/16 v9, 0x87a

    const/16 v10, 0xc10

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x5d0

    const/16 v4, 0x832

    const/16 v5, 0x57c

    const/16 v6, 0x7de

    const/16 v9, 0x618

    const/16 v10, 0x8a2

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xa14

    const/16 v4, 0xe3b

    const/16 v5, 0x9c0

    const/16 v6, 0xde7

    const/16 v9, 0xa5c

    const/16 v10, 0xeab

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xa33

    const/16 v4, 0xec4

    const/16 v5, 0x9df

    const/16 v6, 0xe70

    const/16 v9, 0xa7b

    const/16 v10, 0xf34

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x7bc

    const/16 v4, 0xbf4

    const/16 v5, 0x768

    const/16 v6, 0xba0

    const/16 v9, 0x804

    const/16 v10, 0xc64

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xba0

    const/16 v4, 0x20e3

    const/16 v5, 0xb4c

    const/16 v6, 0x208f

    const/16 v9, 0xbe8

    const/16 v10, 0x2153

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x64a

    const/16 v4, 0x91a

    const/16 v5, 0x5f6

    const/16 v6, 0x8c6

    const/16 v9, 0x692

    const/16 v10, 0x98a

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x5a0

    const/16 v4, 0x870

    const/16 v5, 0x54c

    const/16 v6, 0x81c

    const/16 v9, 0x5e8

    const/16 v10, 0x8e0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x708

    const/16 v4, 0xb40

    const/16 v5, 0x6b4

    const/16 v6, 0xaec

    const/16 v9, 0x750

    const/16 v10, 0xbb0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xb40

    const/16 v4, 0xe10

    const/16 v5, 0xaec

    const/16 v6, 0xdbc

    const/16 v9, 0xb88

    const/16 v10, 0xe80

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x589

    const/16 v4, 0x84d

    const/16 v5, 0x535

    const/16 v6, 0x7f9

    const/16 v9, 0x5d1

    const/16 v10, 0x8bd

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 524
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xbf5

    const/16 v4, 0x12b6

    const/16 v5, 0xba1

    const/16 v6, 0x1262

    const/16 v9, 0xc3d

    const/16 v10, 0x1326

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x4ed

    const/16 v4, 0x708

    const/16 v5, 0x499

    const/16 v6, 0x6b4

    const/16 v9, 0x535

    const/16 v10, 0x778

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x589

    const/16 v4, 0x832

    const/16 v5, 0x535

    const/16 v6, 0x7de

    const/16 v9, 0x5d1

    const/16 v10, 0x8a2

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xb13

    const/16 v5, 0xabf

    const/16 v9, 0xb5b

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x12

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xd5c

    const/16 v4, 0x5cd

    const/16 v5, 0xd08

    const/16 v6, 0x579

    const/16 v9, 0xda4

    const/16 v10, 0x63d

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x8f8

    const/16 v4, 0x650

    const/16 v5, 0x8a4

    const/16 v6, 0x5fc

    const/16 v9, 0x940

    const/16 v10, 0x6c0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xc2e

    const/16 v4, 0x617

    const/16 v5, 0xbda

    const/16 v6, 0x5c3

    const/16 v9, 0xc76

    const/16 v10, 0x687

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v4, 0x74f

    const/16 v6, 0x6fb

    const/16 v10, 0x7bf

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x6a5

    const/16 v4, 0xd03

    const/16 v5, 0x619

    const/16 v6, 0xbbe

    const/16 v9, 0x6ed

    const/16 v10, 0xd73

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x4fc

    const/16 v4, 0xb5a

    const/16 v5, 0x470

    const/16 v6, 0xa15

    const/16 v9, 0x544

    const/16 v10, 0xbca

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x18

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x6a5

    const/16 v4, 0x9be

    const/16 v5, 0x619

    const/16 v6, 0x879

    const/16 v9, 0x6ed

    const/16 v10, 0xa2e

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x650

    const/16 v4, 0x8f8

    const/16 v5, 0x5c4

    const/16 v6, 0x7b3

    const/16 v9, 0x698

    const/16 v10, 0x968

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x1a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x56d

    const/16 v4, 0x832

    const/16 v5, 0x4e1

    const/16 v6, 0x6ed

    const/16 v9, 0x5b5

    const/16 v10, 0x8a2

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x1b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x5d0

    const/16 v4, 0xd03

    const/16 v5, 0x544

    const/16 v6, 0xbbe

    const/16 v9, 0x618

    const/16 v10, 0xd73

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x1c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x708

    const/16 v4, 0x9da

    const/16 v5, 0x6b4

    const/16 v6, 0x986

    const/16 v9, 0x750

    const/16 v10, 0xa4a

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x5cd

    const/16 v4, 0xd5c

    const/16 v5, 0x541

    const/16 v6, 0xc17

    const/16 v9, 0x615

    const/16 v10, 0xdcc

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x650

    const/16 v4, 0x8f8

    const/16 v5, 0x5c4

    const/16 v6, 0x7b3

    const/16 v9, 0x698

    const/16 v10, 0x968

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x617

    const/16 v4, 0xc2e

    const/16 v5, 0x58b

    const/16 v6, 0xae9

    const/16 v9, 0x65f

    const/16 v10, 0xc9e

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x74f

    const/16 v5, 0x6c3

    const/16 v9, 0x797

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x4ed

    const/16 v4, 0x30b

    const/16 v5, 0x499

    const/16 v6, 0x2b7

    const/16 v9, 0x535

    const/16 v10, 0x37b

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v4, 0x2c5

    const/16 v6, 0x271

    const/16 v10, 0x335

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x2fd

    const/16 v4, 0x4c3

    const/16 v5, 0x2a9

    const/16 v6, 0x46f

    const/16 v9, 0x345

    const/16 v10, 0x533

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x24

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x30c

    const/16 v4, 0x50a

    const/16 v5, 0x2b8

    const/16 v6, 0x4b6

    const/16 v9, 0x354

    const/16 v10, 0x57a

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x25

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x708

    const/16 v4, 0xa2f

    const/16 v5, 0x6b4

    const/16 v6, 0x9db

    const/16 v9, 0x750

    const/16 v10, 0xa9f

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x26

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xba0

    const/16 v4, 0x10c6

    const/16 v5, 0xb4c

    const/16 v6, 0x1072

    const/16 v9, 0xbe8

    const/16 v10, 0x1136

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x27

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x708

    const/16 v4, 0x4ec

    const/16 v5, 0x6b4

    const/16 v6, 0x498

    const/16 v9, 0x750

    const/16 v10, 0x55c

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v4, 0x9d9

    const/16 v6, 0x985

    const/16 v10, 0xa49

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x29

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xba0

    const/16 v4, 0x835

    const/16 v5, 0xb4c

    const/16 v6, 0x7e1

    const/16 v9, 0xbe8

    const/16 v10, 0x8a5

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x2a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v4, 0x106b

    const/16 v6, 0x1017

    const/16 v10, 0x10db

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x2b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x5a0

    const/16 v4, 0xa00

    const/16 v5, 0x54c

    const/16 v6, 0x9ac

    const/16 v9, 0x5e8

    const/16 v10, 0xa70

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x2c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xd49

    const/16 v4, 0x1261

    const/16 v5, 0xba1

    const/16 v6, 0x111c

    const/16 v9, 0xd91

    const/16 v10, 0x12d1

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x2d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xcad

    const/16 v4, 0x11f0

    const/16 v6, 0x10ab

    const/16 v9, 0xcf5

    const/16 v10, 0x1260

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x716

    const/16 v4, 0xa14

    const/16 v5, 0x6c2

    const/16 v6, 0x9c0

    const/16 v7, -0x30

    const/16 v9, 0x776

    const/16 v10, 0xa84

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x2f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xcad

    const/16 v4, 0x11f0

    const/16 v5, 0xba1

    const/16 v6, 0x10ab

    const/16 v7, -0x24

    const/16 v9, 0xcf5

    const/16 v10, 0x1260

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x30

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x832

    const/16 v4, 0xba0

    const/16 v5, 0x718

    const/16 v6, 0xa86

    const/16 v9, 0x87a

    const/16 v10, 0xc10

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x34

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x4fc

    const/16 v4, 0xc75

    const/16 v5, 0x470

    const/16 v6, 0xab0

    const/16 v9, 0x544

    const/16 v10, 0xce5

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x3d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x1237

    const/16 v4, 0x1abe

    const/16 v5, 0x11e3

    const/16 v6, 0x1a6a

    const/16 v7, -0x30

    const/16 v9, 0x1297

    const/16 v10, 0x1b2e

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x3e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x1071

    const/16 v4, 0x1741

    const/16 v5, 0x101d

    const/16 v6, 0x16ed

    const/16 v9, 0x10d1

    const/16 v10, 0x17b1

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x3f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xe3b

    const/16 v4, 0x1427

    const/16 v5, 0xde7

    const/16 v6, 0x13d3

    const/16 v9, 0xe9b

    const/16 v10, 0x1497

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xf78

    const/16 v4, 0x17e8

    const/16 v5, 0xf24

    const/16 v6, 0x1794

    const/16 v9, 0xfd8

    const/16 v10, 0x1858

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x41

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v4, 0x13b0

    const/16 v6, 0x135c

    const/16 v10, 0x1420

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x42

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x1427

    const/16 v4, 0x1c83

    const/16 v5, 0x13d3

    const/16 v6, 0x1c2f

    const/16 v9, 0x1487

    const/16 v10, 0x1cf3

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x43

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x1741

    const/16 v4, 0x20e3

    const/16 v5, 0x16ed

    const/16 v6, 0x208f

    const/16 v9, 0x17a1

    const/16 v10, 0x2153

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x44

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x17e8

    const/16 v4, 0x1ef0

    const/16 v5, 0x1794

    const/16 v6, 0x1e9c

    const/16 v9, 0x1848

    const/16 v10, 0x1f60

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x45

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xe10

    const/16 v4, 0x10e0

    const/16 v5, 0xdbc

    const/16 v6, 0x108c

    const/16 v9, 0xe70

    const/16 v10, 0x1150

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x46

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x10e0

    const/16 v5, 0x108c

    const/16 v9, 0x1140

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 570
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x47

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xba0

    const/16 v4, 0xef3

    const/16 v5, 0xb4c

    const/16 v6, 0xe9f

    const/16 v7, -0x24

    const/16 v9, 0xbe8

    const/16 v10, 0xf63

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x48

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v4, 0x840

    const/16 v6, 0x7ec

    const/16 v10, 0x8b0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x49

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x589

    const/16 v4, 0x969

    const/16 v5, 0x535

    const/16 v6, 0x915

    const/16 v9, 0x5d1

    const/16 v10, 0x9d9

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x4a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x733

    const/16 v4, 0xa14

    const/16 v5, 0x6df

    const/16 v6, 0x9c0

    const/16 v9, 0x77b

    const/16 v10, 0xa84

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x4b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xaa1

    const/16 v4, 0x74f

    const/16 v5, 0xa4d

    const/16 v6, 0x6fb

    const/16 v9, 0xae9

    const/16 v10, 0x7bf

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 575
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x4c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xacc

    const/16 v4, 0xef3

    const/16 v5, 0xa78

    const/16 v6, 0xe9f

    const/16 v9, 0xb14

    const/16 v10, 0xf63

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x4d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xef3

    const/16 v4, 0x1598

    const/16 v5, 0xe9f

    const/16 v6, 0x1544

    const/16 v9, 0xf3b

    const/16 v10, 0x1608

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x57

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xba0

    const/16 v4, 0xba0

    const/16 v5, 0xb4c

    const/16 v6, 0xb4c

    const/16 v9, 0xbe8

    const/16 v10, 0xc10

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x58

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x708

    const/16 v4, 0x708

    const/16 v5, 0x6b4

    const/16 v6, 0x6b4

    const/16 v9, 0x750

    const/16 v10, 0x778

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x63

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 580
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x68

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xb40

    const/16 v4, 0xec4

    const/16 v5, 0xaec

    const/16 v6, 0xe70

    const/16 v9, 0xb88

    const/16 v10, 0xf34

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x6a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0xba0

    const/16 v4, 0x1246

    const/16 v5, 0xb4c

    const/16 v6, 0x11f2

    const/16 v9, 0xbe8

    const/16 v10, 0x12b6

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x6f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x9be

    const/16 v4, 0xdd7

    const/16 v5, 0x932

    const/16 v6, 0xc92

    const/16 v9, 0xa06

    const/16 v10, 0xe47

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x13b0

    const/16 v4, 0x17e8

    const/16 v5, 0x135c

    const/16 v6, 0x1794

    const/16 v9, 0x13f8

    const/16 v10, 0x1858

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0x81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x1680

    const/16 v4, 0x1c20

    const/16 v5, 0x162c

    const/16 v6, 0x1bcc

    const/16 v9, 0x16c8

    const/16 v10, 0x1c90

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;->sCodeTable:Ljava/util/Hashtable;

    const/16 v1, 0xff

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v11, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIII)V

    invoke-virtual {v0, v1, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

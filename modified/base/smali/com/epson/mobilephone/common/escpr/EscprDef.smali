.class public Lcom/epson/mobilephone/common/escpr/EscprDef;
.super Ljava/lang/Object;
.source "EscprDef.java"


# static fields
.field public static final EPS_BATTERY_NUM:I = 0x5

.field static final EPS_BK1PRINT_ACCEPTED:I = 0x2

.field static final EPS_BK1PRINT_NONE:I = 0xff

.field static final EPS_BK1PRINT_NOTACCEPTED:I = 0x1

.field static final EPS_BK1PRINT_UNCONFIRMED:I = 0x0

.field public static final EPS_CM_COLOR:I = 0x0

.field public static final EPS_CM_MONOCHROME:I = 0x1

.field public static final EPS_CM_SEPIA:I = 0x2

.field public static final EPS_DUPLEX_LONG:I = 0x1

.field public static final EPS_DUPLEX_NONE:I = 0x0

.field public static final EPS_DUPLEX_SHORT:I = 0x2

.field public static final EPS_FEEDDIR_LANDSCAPE:I = 0x1

.field public static final EPS_FEEDDIR_PORTRAIT:I = 0x0

.field public static final EPS_LANG_ESCPAGE:I = 0x2

.field public static final EPS_LANG_ESCPAGE_COLOR:I = 0x3

.field public static final EPS_LANG_ESCPAGE_S:I = 0x4

.field public static final EPS_LANG_ESCPR:I = 0x1

.field public static final EPS_LANG_PCL:I = 0x6

.field public static final EPS_LANG_PCL_COLOR:I = 0x5

.field public static final EPS_LANG_UNKNOWN:I = 0x0

.field public static final EPS_MLID_BORDERLESS:I = 0x1

.field public static final EPS_MLID_BORDERS:I = 0x2

.field public static final EPS_MLID_CDLABEL:I = 0x4

.field public static final EPS_MLID_CUSTOM:I = 0x0

.field public static final EPS_MLID_DIVIDE16:I = 0x8

.field public static final EPS_MPID_ALL_ESCPAGE:I = 0x8066

.field public static final EPS_MPID_ALL_ESCPR:I = 0x3ff

.field public static final EPS_MPID_AUTO:I = 0x80

.field public static final EPS_MPID_CDTRAY:I = 0x8

.field public static final EPS_MPID_FRONT1:I = 0x2

.field public static final EPS_MPID_FRONT2:I = 0x4

.field public static final EPS_MPID_FRONT3:I = 0x20

.field public static final EPS_MPID_FRONT4:I = 0x40

.field public static final EPS_MPID_MANUAL:I = 0x10

.field public static final EPS_MPID_MANUAL2:I = 0x200

.field public static final EPS_MPID_MPTRAY:I = 0x8000

.field public static final EPS_MPID_NOT_SPEC:I = 0x0

.field public static final EPS_MPID_REAR:I = 0x1

.field public static final EPS_MPID_REARMANUAL:I = 0x10

.field public static final EPS_MPID_ROLL:I = 0x100

.field public static final EPS_MQID_ALL:I = 0x87

.field public static final EPS_MQID_BEST:I = 0x10

.field public static final EPS_MQID_BEST_PLAIN:I = 0x80

.field public static final EPS_MQID_DRAFT:I = 0x1

.field public static final EPS_MQID_HIGH:I = 0x4

.field public static final EPS_MQID_NORMAL:I = 0x2

.field public static final EPS_MQID_STANDARD_VIVID:I = 0x40

.field public static final EPS_MQID_SUPER_HIGH:I = 0x8

.field public static final EPS_MQID_UNKNOWN:I = 0x0

.field public static final EPS_MTID_HAGAKIINKJET:I = 0x1c

.field public static final EPS_PF_IPRINT_ANDROID:B = 0x8t

.field public static final EPS_PF_OTHER_ANDROID:B = 0xat


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

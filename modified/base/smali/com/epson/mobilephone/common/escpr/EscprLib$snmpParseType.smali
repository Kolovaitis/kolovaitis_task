.class public final enum Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;
.super Ljava/lang/Enum;
.source "EscprLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/EscprLib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "snmpParseType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

.field public static final enum INTEGER:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

.field public static final enum MAC_ADDRESS:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

.field public static final enum SECURITY:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

.field public static final enum STRING:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 68
    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    const-string v1, "STRING"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->STRING:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    .line 69
    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    const-string v1, "MAC_ADDRESS"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->MAC_ADDRESS:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    .line 70
    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    const-string v1, "INTEGER"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->INTEGER:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    .line 71
    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    const-string v1, "SECURITY"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->SECURITY:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    const/4 v0, 0x4

    .line 67
    new-array v0, v0, [Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->STRING:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->MAC_ADDRESS:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->INTEGER:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->SECURITY:Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->$VALUES:[Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;
    .locals 1

    .line 67
    const-class v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;
    .locals 1

    .line 67
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->$VALUES:[Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;

    return-object v0
.end method

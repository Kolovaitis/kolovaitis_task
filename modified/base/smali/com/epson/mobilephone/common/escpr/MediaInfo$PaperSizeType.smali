.class public Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;
.super Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;
.source "MediaInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/MediaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PaperSizeType"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 772
    invoke-direct {p0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;-><init>()V

    .line 773
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 774
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 775
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 776
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 777
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 778
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 779
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_EXECUTIVE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HALFLETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 781
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PANORAMIC:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 782
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_TRIM_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 783
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 784
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_5X8:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8X10:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_10X15:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 787
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_200X300:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 788
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 790
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_POSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 791
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_DBLPOSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 792
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_10_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C6_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 794
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_DL_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_NEWEVN_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CHOKEI_3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 797
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CHOKEI_4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 798
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 799
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 800
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 802
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_2L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 803
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_10_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C6_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 805
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_DL_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 807
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_NEWENV_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_MEISHI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_BUZCARD_89X50:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 810
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CARD_54X86:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 811
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_BUZCARD_55X91:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ALBUM_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ALBUM_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 814
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_L_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_2L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 816
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_A5_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 818
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HIVISION:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 819
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_KAKU_2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C4_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_KAKU_20:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3NOBI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 825
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 828
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USB:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_11X14:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 831
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 832
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USC:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 833
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_10X12:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 834
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_12X12:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 836
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 838
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HALFCUT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 839
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->sCodeTable:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_16X20:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

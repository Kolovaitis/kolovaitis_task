.class public Lcom/epson/mobilephone/common/escpr/EscprError;
.super Ljava/lang/Object;
.source "EscprError.java"


# static fields
.field public static final EPS_ERR_2ND_OPEN_IO:I = -0x44f

.field public static final EPS_ERR_CAN_NOT_RESET:I = -0x672

.field public static final EPS_ERR_COMM_ERROR:I = -0x44c

.field public static final EPS_ERR_FIND_NOT_STARTED:I = -0x519

.field public static final EPS_ERR_INVALID_CALL:I = -0x3f4

.field public static final EPS_ERR_INVALID_DATATYPE:I = -0x3f6

.field public static final EPS_ERR_INVALID_VERSION:I = -0x3f5

.field public static final EPS_ERR_INV_APF_ACT:I = -0x597

.field public static final EPS_ERR_INV_APF_RDE:I = -0x599

.field public static final EPS_ERR_INV_APF_SHP:I = -0x598

.field public static final EPS_ERR_INV_ARG_ADDDATA:I = -0x7d1

.field public static final EPS_ERR_INV_ARG_COMMMODE:I = -0x4b0

.field public static final EPS_ERR_INV_ARG_DATA:I = -0x640

.field public static final EPS_ERR_INV_ARG_DATASIZE:I = -0x64a

.field public static final EPS_ERR_INV_ARG_HEIGHT_PIXELS:I = -0x643

.field public static final EPS_ERR_INV_ARG_INKINFO:I = -0x6ae

.field public static final EPS_ERR_INV_ARG_JOB_ATTRIB:I = -0x5aa

.field public static final EPS_ERR_INV_ARG_PAGE_ATTRIB:I = -0x5ab

.field public static final EPS_ERR_INV_ARG_PRINTABLE_HEIGHT:I = -0x709

.field public static final EPS_ERR_INV_ARG_PRINTABLE_WIDTH:I = -0x708

.field public static final EPS_ERR_INV_ARG_PRINTER:I = -0x546

.field public static final EPS_ERR_INV_ARG_PRINTER_ADDR:I = -0x518

.field public static final EPS_ERR_INV_ARG_PRINTER_ID:I = -0x517

.field public static final EPS_ERR_INV_ARG_PROBEINFO:I = -0x515

.field public static final EPS_ERR_INV_ARG_QRSOURCE:I = -0x7d2

.field public static final EPS_ERR_INV_ARG_QRXPOS:I = -0x7d3

.field public static final EPS_ERR_INV_ARG_QRYPOS:I = -0x7d4

.field public static final EPS_ERR_INV_ARG_STATUS:I = -0x6a4

.field public static final EPS_ERR_INV_ARG_SUPPORTED_MEDIA:I = -0x6d6

.field public static final EPS_ERR_INV_ARG_UNK_METHOD:I = -0x516

.field public static final EPS_ERR_INV_ARG_WIDTH_PIXELS:I = -0x641

.field public static final EPS_ERR_INV_BORDER_MODE:I = -0x57a

.field public static final EPS_ERR_INV_BOTTOM_MARGIN:I = -0x58e

.field public static final EPS_ERR_INV_BRIGHTNESS:I = -0x582

.field public static final EPS_ERR_INV_CD_INDIM:I = -0x59a

.field public static final EPS_ERR_INV_CD_OUTDIM:I = -0x59b

.field public static final EPS_ERR_INV_CMDTYPE:I = -0x59c

.field public static final EPS_ERR_INV_COLOR_MODE:I = -0x57c

.field public static final EPS_ERR_INV_COLOR_PLANE:I = -0x57f

.field public static final EPS_ERR_INV_CONTRAST:I = -0x583

.field public static final EPS_ERR_INV_DUPLEX:I = -0x593

.field public static final EPS_ERR_INV_FEED_DIRECTION:I = -0x594

.field public static final EPS_ERR_INV_FNCP_CLOSEPORTAL:I = -0x4bc

.field public static final EPS_ERR_INV_FNCP_FINDCALLBACK:I = -0x4b2

.field public static final EPS_ERR_INV_FNCP_FINDCLOSE:I = -0x4c1

.field public static final EPS_ERR_INV_FNCP_FINDFIRST:I = -0x4bf

.field public static final EPS_ERR_INV_FNCP_FINDNEXT:I = -0x4c0

.field public static final EPS_ERR_INV_FNCP_GETTIME:I = -0x4b6

.field public static final EPS_ERR_INV_FNCP_MEMALLOC:I = -0x4b3

.field public static final EPS_ERR_INV_FNCP_MEMFREE:I = -0x4b4

.field public static final EPS_ERR_INV_FNCP_NETACCEPT:I = -0x4cb

.field public static final EPS_ERR_INV_FNCP_NETBIND:I = -0x4c9

.field public static final EPS_ERR_INV_FNCP_NETCLOSE:I = -0x4c6

.field public static final EPS_ERR_INV_FNCP_NETCONNECT:I = -0x4c7

.field public static final EPS_ERR_INV_FNCP_NETGETSOCKNAME:I = -0x4d0

.field public static final EPS_ERR_INV_FNCP_NETLISTEN:I = -0x4ca

.field public static final EPS_ERR_INV_FNCP_NETRECEIVE:I = -0x4ce

.field public static final EPS_ERR_INV_FNCP_NETRECEIVEFROM:I = -0x4cf

.field public static final EPS_ERR_INV_FNCP_NETSEND:I = -0x4cc

.field public static final EPS_ERR_INV_FNCP_NETSENDTO:I = -0x4cd

.field public static final EPS_ERR_INV_FNCP_NETSETBROADCAST:I = -0x4d2

.field public static final EPS_ERR_INV_FNCP_NETSETMULTITTL:I = -0x4d1

.field public static final EPS_ERR_INV_FNCP_NETSHUTDOWN:I = -0x4c8

.field public static final EPS_ERR_INV_FNCP_NETSOCKET:I = -0x4c5

.field public static final EPS_ERR_INV_FNCP_NULL:I = -0x4b1

.field public static final EPS_ERR_INV_FNCP_OPENPORTAL:I = -0x4bb

.field public static final EPS_ERR_INV_FNCP_READPORTAL:I = -0x4bd

.field public static final EPS_ERR_INV_FNCP_SLEEP:I = -0x4b5

.field public static final EPS_ERR_INV_FNCP_WRITEPORTAL:I = -0x4be

.field public static final EPS_ERR_INV_INPUT_RESOLUTION:I = -0x57d

.field public static final EPS_ERR_INV_LEFT_MARGIN:I = -0x58d

.field public static final EPS_ERR_INV_MEDIA_SIZE:I = -0x578

.field public static final EPS_ERR_INV_MEDIA_TYPE:I = -0x579

.field public static final EPS_ERR_INV_PALETTE_DATA:I = -0x581

.field public static final EPS_ERR_INV_PALETTE_SIZE:I = -0x580

.field public static final EPS_ERR_INV_PAPER_SOURCE:I = -0x592

.field public static final EPS_ERR_INV_PRINT_DIRECTION:I = -0x57e

.field public static final EPS_ERR_INV_PRINT_LANGUAGE:I = -0x548

.field public static final EPS_ERR_INV_PRINT_QUALITY:I = -0x57b

.field public static final EPS_ERR_INV_RIGHT_MARGIN:I = -0x58f

.field public static final EPS_ERR_INV_SATURATION:I = -0x584

.field public static final EPS_ERR_INV_TOP_MARGIN:I = -0x58c

.field public static final EPS_ERR_JOB_NOT_CLOSED:I = -0x41d

.field public static final EPS_ERR_JOB_NOT_INITIALIZED:I = -0x41c

.field public static final EPS_ERR_LANGUAGE_NOT_SUPPORTED:I = -0x3f7

.field public static final EPS_ERR_LIB_INTIALIZED:I = -0x41a

.field public static final EPS_ERR_LIB_NOT_INITIALIZED:I = -0x41b

.field public static final EPS_ERR_MARGIN_OVER_PRINTABLE_HEIGHT:I = -0x591

.field public static final EPS_ERR_MARGIN_OVER_PRINTABLE_WIDTH:I = -0x590

.field public static final EPS_ERR_MEMORY_ALLOCATION:I = -0x3e9

.field public static final EPS_ERR_NEED_BIDIRECT:I = -0x3f3

.field public static final EPS_ERR_NONE:I = 0x0

.field public static final EPS_ERR_NOT_CLOSE_IO:I = -0x44e

.field public static final EPS_ERR_NOT_OPEN_IO:I = -0x44d

.field public static final EPS_ERR_OPR_FAIL:I = -0x3e8

.field public static final EPS_ERR_PAGE_NOT_CLOSED:I = -0x41f

.field public static final EPS_ERR_PAGE_NOT_INITIALIZED:I = -0x41e

.field public static final EPS_ERR_PRINTER_ERR_OCCUR:I = -0x3eb

.field public static final EPS_ERR_PRINTER_NOT_FOUND:I = -0x514

.field public static final EPS_ERR_PRINTER_NOT_FOUND_2:I = -0x32c9

.field public static final EPS_ERR_PRINTER_NOT_SET:I = -0x547

.field public static final EPS_ERR_PRINTER_NOT_USEFUL:I = -0x51a

.field public static final EPS_ERR_PROTOCOL_NOT_SUPPORTED:I = -0x3f2

.field public static final EPS_ERR_QRSOURCE_TOO_LAGE:I = -0x7d5

.field public static final EPS_ERR_SEARCH_PRINTER_NOT_FOUND:I = -0x2afd

.field public static final EPS_FIND_CANCELED:I = 0x2a

.field public static final EPS_JOB_CANCELED:I = 0x28

.field public static final EPS_OUT_OF_BOUNDS:I = 0x29

.field public static final EPS_PRNERR_3DMEDIA_DIRECTION:I = 0x1b

.field public static final EPS_PRNERR_3DMEDIA_FACE:I = 0x1a

.field public static final EPS_PRNERR_ANY:I = 0xc8

.field public static final EPS_PRNERR_BATTERYEMPTY:I = 0x11

.field public static final EPS_PRNERR_BATTERYTEMPERATURE:I = 0x10

.field public static final EPS_PRNERR_BATTERYVOLTAGE:I = 0xf

.field public static final EPS_PRNERR_BATTERY_CHARGING:I = 0x28

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_HIGH:I = 0x29

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_LOW:I = 0x2a

.field public static final EPS_PRNERR_BK1MODE_NEED_ACCEPT:I = 0x6d

.field public static final EPS_PRNERR_BK1MODE_WAITING_ACCEPT:I = 0x38

.field public static final EPS_PRNERR_BORDERLESS_WIP_END:I = 0x36

.field public static final EPS_PRNERR_BORDERLESS_WIP_NEAR_END:I = 0x35

.field public static final EPS_PRNERR_BUSY:I = 0x64

.field public static final EPS_PRNERR_CARDLOADING:I = 0xd

.field public static final EPS_PRNERR_CARTRIDGEOVERFLOW:I = 0xe

.field public static final EPS_PRNERR_CASSETTECOVER_CLOSED:I = 0x3a

.field public static final EPS_PRNERR_CASSETTECOVER_OPENED:I = 0x39

.field public static final EPS_PRNERR_CDDVDCONFIG:I = 0x17

.field public static final EPS_PRNERR_CDDVDCONFIG_FEEDBUTTON:I = 0x23

.field public static final EPS_PRNERR_CDDVDCONFIG_STARTBUTTON:I = 0x22

.field public static final EPS_PRNERR_CDGUIDECLOSE:I = 0x6a

.field public static final EPS_PRNERR_CDREXIST_MAINTE:I = 0x18

.field public static final EPS_PRNERR_CDRGUIDEOPEN:I = 0x16

.field public static final EPS_PRNERR_CEMPTY:I = 0x67

.field public static final EPS_PRNERR_CFAIL:I = 0x68

.field public static final EPS_PRNERR_COMM:I = 0x66

.field public static final EPS_PRNERR_COVEROPEN:I = 0x4

.field public static final EPS_PRNERR_DISABEL_CLEANING:I = 0x6c

.field public static final EPS_PRNERR_DISABLE_DUPLEX:I = 0x33

.field public static final EPS_PRNERR_DOUBLEFEED:I = 0xa

.field public static final EPS_PRNERR_FACTORY:I = 0x65

.field public static final EPS_PRNERR_FATAL:I = 0x2

.field public static final EPS_PRNERR_FEEDERCLOSE:I = 0x19

.field public static final EPS_PRNERR_GENERAL:I = 0x1

.field public static final EPS_PRNERR_INKCOVEROPEN:I = 0xb

.field public static final EPS_PRNERR_INKOUT:I = 0x6

.field public static final EPS_PRNERR_INKOUT_BK1MODE:I = 0x37

.field public static final EPS_PRNERR_INTERFACE:I = 0x3

.field public static final EPS_PRNERR_INTERRUPT_BY_INKEND:I = 0x24

.field public static final EPS_PRNERR_JPG_LIMIT:I = 0x6b

.field public static final EPS_PRNERR_LOW_BATTERY_FNC:I = 0x27

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE:I = 0x20

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_NOLCD:I = 0x21

.field public static final EPS_PRNERR_MANUALFEED_FAILED:I = 0x1e

.field public static final EPS_PRNERR_MANUALFEED_FAILED_NOLCD:I = 0x1f

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER:I = 0x1c

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_NOLCD:I = 0x1d

.field public static final EPS_PRNERR_NOERROR:I = 0x0

.field public static final EPS_PRNERR_NOTRAY:I = 0xc

.field public static final EPS_PRNERR_NOT_INITIALFILL:I = 0x13

.field public static final EPS_PRNERR_NO_BATTERY:I = 0x26

.field public static final EPS_PRNERR_NO_MAINTENANCE_BOX:I = 0x30

.field public static final EPS_PRNERR_NO_STAPLE:I = 0x32

.field public static final EPS_PRNERR_PAPERJAM:I = 0x5

.field public static final EPS_PRNERR_PAPEROUT:I = 0x7

.field public static final EPS_PRNERR_PC_DIRECTION1:I = 0x2b

.field public static final EPS_PRNERR_PC_DIRECTION2:I = 0x2e

.field public static final EPS_PRNERR_PC_FACE1:I = 0x2c

.field public static final EPS_PRNERR_PC_FACE2:I = 0x2d

.field public static final EPS_PRNERR_PRINTPACKEND:I = 0x14

.field public static final EPS_PRNERR_READYPRINT_SERVICE:I = 0x3b

.field public static final EPS_PRNERR_REPLACE_MAINTENANCE_BOX:I = 0x2f

.field public static final EPS_PRNERR_ROLLPAPER_TOOSHORT:I = 0x25

.field public static final EPS_PRNERR_SCANNEROPEN:I = 0x15

.field public static final EPS_PRNERR_SERVICEREQ:I = 0x9

.field public static final EPS_PRNERR_SHUTOFF:I = 0x12

.field public static final EPS_PRNERR_SIZE_TYPE_PATH:I = 0x8

.field public static final EPS_PRNERR_STACKER_FULL:I = 0x31

.field public static final EPS_PRNERR_TRAYCLOSE:I = 0x69

.field public static final EPS_PRNERR_WIP_NEAR_END:I = 0x34

.field public static final EPS_PRNST_BUSY:I = 0x2

.field public static final EPS_PRNST_CANCELLING:I = 0x3

.field public static final EPS_PRNST_ERROR:I = 0x4

.field public static final EPS_PRNST_IDLE:I = 0x0

.field public static final EPS_PRNST_PRINTING:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;
.super Ljava/lang/Object;
.source "EPS_LAYOUTSIZE_INFO.java"


# instance fields
.field layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

.field mediaSizeID:I

.field numLayouts:I

.field paperHeight:I

.field paperWidth:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 11
    iput v0, p0, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->mediaSizeID:I

    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->paperWidth:I

    .line 13
    iput v0, p0, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->paperHeight:I

    .line 14
    iput v0, p0, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->numLayouts:I

    const/4 v0, 0x0

    .line 15
    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/EPS_LAYOUTSIZE_INFO;->layoutList:[Lcom/epson/mobilephone/common/escpr/EPS_LAYOUT_INFO;

    return-void
.end method

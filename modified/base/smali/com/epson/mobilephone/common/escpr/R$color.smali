.class public final Lcom/epson/mobilephone/common/escpr/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final epson_lib_all_black:I = 0x7f05004b

.field public static final epson_lib_all_white:I = 0x7f05004c

.field public static final epson_lib_black:I = 0x7f05004d

.field public static final epson_lib_black1:I = 0x7f05004e

.field public static final epson_lib_black2:I = 0x7f05004f

.field public static final epson_lib_blue:I = 0x7f050050

.field public static final epson_lib_clean:I = 0x7f050051

.field public static final epson_lib_clear:I = 0x7f050052

.field public static final epson_lib_composite:I = 0x7f050053

.field public static final epson_lib_cyan:I = 0x7f050054

.field public static final epson_lib_darkyellow:I = 0x7f050055

.field public static final epson_lib_deep_blue:I = 0x7f050056

.field public static final epson_lib_gray:I = 0x7f050057

.field public static final epson_lib_green:I = 0x7f050058

.field public static final epson_lib_lightblack:I = 0x7f050059

.field public static final epson_lib_lightcyan:I = 0x7f05005a

.field public static final epson_lib_lightgray:I = 0x7f05005b

.field public static final epson_lib_lightlightblack:I = 0x7f05005c

.field public static final epson_lib_lightmagenta:I = 0x7f05005d

.field public static final epson_lib_lightyellow:I = 0x7f05005e

.field public static final epson_lib_magenta:I = 0x7f05005f

.field public static final epson_lib_maintainance_box:I = 0x7f050060

.field public static final epson_lib_matteblack:I = 0x7f050061

.field public static final epson_lib_orange:I = 0x7f050062

.field public static final epson_lib_photoblack:I = 0x7f050063

.field public static final epson_lib_red:I = 0x7f050064

.field public static final epson_lib_violet:I = 0x7f050065

.field public static final epson_lib_vivid_lightmagenta:I = 0x7f050066

.field public static final epson_lib_vivid_magenta:I = 0x7f050067

.field public static final epson_lib_white:I = 0x7f050068

.field public static final epson_lib_yellow:I = 0x7f050069


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public final Lcom/epson/mobilephone/common/escpr/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final APP_MLID_DIV2:I = 0x7f0e0000

.field public static final APP_MLID_DIV4N:I = 0x7f0e0001

.field public static final APP_MLID_DIV4Z:I = 0x7f0e0002

.field public static final Advanced_Settings:I = 0x7f0e0003

.field public static final Auto_Correct:I = 0x7f0e000d

.field public static final Brightness:I = 0x7f0e0027

.field public static final Buy_Ink:I = 0x7f0e0028

.field public static final Buy_Ink_Toner:I = 0x7f0e0029

.field public static final Cancel:I = 0x7f0e002a

.field public static final Cannot_retrieve_any_information_from_the_printer_:I = 0x7f0e002d

.field public static final Check_that_the_device_or_printer_is_connected_to_the_network_:I = 0x7f0e002e

.field public static final Close:I = 0x7f0e002f

.field public static final Continue:I = 0x7f0e0030

.field public static final Contrast:I = 0x7f0e0031

.field public static final Copies:I = 0x7f0e0032

.field public static final Done:I = 0x7f0e0046

.field public static final ECC_EXERR_SRATUS_FAILE_MSG:I = 0x7f0e005d

.field public static final ECC_EXERR_SRATUS_FAILE_TITLE:I = 0x7f0e005e

.field public static final EPS_APF_OFF:I = 0x7f0e0077

.field public static final EPS_APF_ON:I = 0x7f0e0078

.field public static final EPS_CM_COLOR:I = 0x7f0e007b

.field public static final EPS_CM_MONOCHROME:I = 0x7f0e007c

.field public static final EPS_COLOR_BLACK:I = 0x7f0e007d

.field public static final EPS_COLOR_BLACK1:I = 0x7f0e007e

.field public static final EPS_COLOR_BLACK1_CON:I = 0x7f0e007f

.field public static final EPS_COLOR_BLACK2:I = 0x7f0e0080

.field public static final EPS_COLOR_BLACK2_CON:I = 0x7f0e0081

.field public static final EPS_COLOR_BLACK_CON:I = 0x7f0e0082

.field public static final EPS_COLOR_BLUE:I = 0x7f0e0083

.field public static final EPS_COLOR_BLUE_CON:I = 0x7f0e0084

.field public static final EPS_COLOR_CLEAN:I = 0x7f0e0085

.field public static final EPS_COLOR_CLEAN_CON:I = 0x7f0e0086

.field public static final EPS_COLOR_CLEAR:I = 0x7f0e0087

.field public static final EPS_COLOR_CLEAR_CON:I = 0x7f0e0088

.field public static final EPS_COLOR_COMPOSITE:I = 0x7f0e0089

.field public static final EPS_COLOR_COMPOSITE_CON:I = 0x7f0e008a

.field public static final EPS_COLOR_CYAN:I = 0x7f0e008b

.field public static final EPS_COLOR_CYAN_CON:I = 0x7f0e008c

.field public static final EPS_COLOR_DARKYELLOW:I = 0x7f0e008d

.field public static final EPS_COLOR_DARKYELLOW_CON:I = 0x7f0e008e

.field public static final EPS_COLOR_DEEP_BLUE:I = 0x7f0e008f

.field public static final EPS_COLOR_DEEP_BLUE_CON:I = 0x7f0e0090

.field public static final EPS_COLOR_GRAY:I = 0x7f0e0091

.field public static final EPS_COLOR_GRAY_CON:I = 0x7f0e0092

.field public static final EPS_COLOR_GREEN:I = 0x7f0e0093

.field public static final EPS_COLOR_GREEN_CON:I = 0x7f0e0094

.field public static final EPS_COLOR_LIGHTBLACK:I = 0x7f0e0095

.field public static final EPS_COLOR_LIGHTBLACK_CON:I = 0x7f0e0096

.field public static final EPS_COLOR_LIGHTCYAN:I = 0x7f0e0097

.field public static final EPS_COLOR_LIGHTCYAN_CON:I = 0x7f0e0098

.field public static final EPS_COLOR_LIGHTGRAY:I = 0x7f0e0099

.field public static final EPS_COLOR_LIGHTGRAY_CON:I = 0x7f0e009a

.field public static final EPS_COLOR_LIGHTLIGHTBLACK:I = 0x7f0e009b

.field public static final EPS_COLOR_LIGHTLIGHTBLACK__CON:I = 0x7f0e009c

.field public static final EPS_COLOR_LIGHTMAGENTA:I = 0x7f0e009d

.field public static final EPS_COLOR_LIGHTMAGENTA_CON:I = 0x7f0e009e

.field public static final EPS_COLOR_LIGHTYELLOW:I = 0x7f0e009f

.field public static final EPS_COLOR_LIGHTYELLOW_CON:I = 0x7f0e00a0

.field public static final EPS_COLOR_MAGENTA:I = 0x7f0e00a1

.field public static final EPS_COLOR_MAGENTA_CON:I = 0x7f0e00a2

.field public static final EPS_COLOR_MATTEBLACK:I = 0x7f0e00a3

.field public static final EPS_COLOR_MATTEBLACK_CON:I = 0x7f0e00a4

.field public static final EPS_COLOR_MATTEBLACK_CON_MK:I = 0x7f0e00a5

.field public static final EPS_COLOR_ORANGE:I = 0x7f0e00a6

.field public static final EPS_COLOR_ORANGE_CON:I = 0x7f0e00a7

.field public static final EPS_COLOR_PHOTOBLACK:I = 0x7f0e00a8

.field public static final EPS_COLOR_PHOTOBLACK_CON:I = 0x7f0e00a9

.field public static final EPS_COLOR_PHOTOBLACK_CON_BK:I = 0x7f0e00aa

.field public static final EPS_COLOR_PHOTOBLACK_CON_PK:I = 0x7f0e00ab

.field public static final EPS_COLOR_RED:I = 0x7f0e00ac

.field public static final EPS_COLOR_RED_CON:I = 0x7f0e00ad

.field public static final EPS_COLOR_VIOLET:I = 0x7f0e00ae

.field public static final EPS_COLOR_VIOLET_CON:I = 0x7f0e00af

.field public static final EPS_COLOR_VIVID_LIGHTMAGENTA:I = 0x7f0e00b0

.field public static final EPS_COLOR_VIVID_LIGHTMAGENTA_CON:I = 0x7f0e00b1

.field public static final EPS_COLOR_VIVID_MAGENTA:I = 0x7f0e00b2

.field public static final EPS_COLOR_VIVID_MAGENTA_CON:I = 0x7f0e00b3

.field public static final EPS_COLOR_WHITE:I = 0x7f0e00b4

.field public static final EPS_COLOR_WHITE_CON:I = 0x7f0e00b5

.field public static final EPS_COLOR_YELLOW:I = 0x7f0e00b6

.field public static final EPS_COLOR_YELLOW_CON:I = 0x7f0e00b7

.field public static final EPS_DUPLEX_LONG:I = 0x7f0e00ba

.field public static final EPS_DUPLEX_NONE:I = 0x7f0e00bb

.field public static final EPS_DUPLEX_SHORT:I = 0x7f0e00bc

.field public static final EPS_FEEDDIR_LANDSCAPE:I = 0x7f0e00c0

.field public static final EPS_FEEDDIR_PORTRAIT:I = 0x7f0e00c1

.field public static final EPS_MLID_BORDERLESS:I = 0x7f0e00c2

.field public static final EPS_MLID_BORDERS:I = 0x7f0e00c3

.field public static final EPS_MLID_CDLABEL:I = 0x7f0e00c7

.field public static final EPS_MLID_CUSTOM:I = 0x7f0e00c8

.field public static final EPS_MLID_DIVIDE16:I = 0x7f0e00c9

.field public static final EPS_MPID_AUTO:I = 0x7f0e00ca

.field public static final EPS_MPID_CDTRAY:I = 0x7f0e00cb

.field public static final EPS_MPID_FRONT1:I = 0x7f0e00cc

.field public static final EPS_MPID_FRONT2:I = 0x7f0e00cd

.field public static final EPS_MPID_FRONT3:I = 0x7f0e00ce

.field public static final EPS_MPID_FRONT4:I = 0x7f0e00cf

.field public static final EPS_MPID_HIGHCAP:I = 0x7f0e00d0

.field public static final EPS_MPID_MANUAL:I = 0x7f0e00d1

.field public static final EPS_MPID_MANUAL2:I = 0x7f0e00d2

.field public static final EPS_MPID_MPTRAY:I = 0x7f0e00d3

.field public static final EPS_MPID_MPTRAY_IJ:I = 0x7f0e00d4

.field public static final EPS_MPID_NOT_SPEC:I = 0x7f0e00d5

.field public static final EPS_MPID_REAR:I = 0x7f0e00d6

.field public static final EPS_MPID_REARMANUAL:I = 0x7f0e00d7

.field public static final EPS_MPID_ROLL:I = 0x7f0e00d8

.field public static final EPS_MQID_BEST_PLAIN:I = 0x7f0e00d9

.field public static final EPS_MQID_DRAFT:I = 0x7f0e00da

.field public static final EPS_MQID_HIGH:I = 0x7f0e00db

.field public static final EPS_MQID_NORMAL:I = 0x7f0e00dc

.field public static final EPS_MSID_10X12:I = 0x7f0e00de

.field public static final EPS_MSID_10X15:I = 0x7f0e00df

.field public static final EPS_MSID_11X14:I = 0x7f0e00e0

.field public static final EPS_MSID_12X12:I = 0x7f0e00e1

.field public static final EPS_MSID_12X18:I = 0x7f0e00e2

.field public static final EPS_MSID_16K:I = 0x7f0e00e3

.field public static final EPS_MSID_16X20:I = 0x7f0e00e4

.field public static final EPS_MSID_200X300:I = 0x7f0e00e5

.field public static final EPS_MSID_2L:I = 0x7f0e00e6

.field public static final EPS_MSID_4X6:I = 0x7f0e00e7

.field public static final EPS_MSID_5X8:I = 0x7f0e00e8

.field public static final EPS_MSID_8K:I = 0x7f0e00e9

.field public static final EPS_MSID_8X10:I = 0x7f0e00ea

.field public static final EPS_MSID_8X10_5:I = 0x7f0e00eb

.field public static final EPS_MSID_8_27X13:I = 0x7f0e00ec

.field public static final EPS_MSID_8_5X13:I = 0x7f0e00ed

.field public static final EPS_MSID_A2:I = 0x7f0e00ee

.field public static final EPS_MSID_A3:I = 0x7f0e00ef

.field public static final EPS_MSID_A3NOBI:I = 0x7f0e00f0

.field public static final EPS_MSID_A4:I = 0x7f0e00f1

.field public static final EPS_MSID_A5:I = 0x7f0e00f2

.field public static final EPS_MSID_A5_24HOLE:I = 0x7f0e00f3

.field public static final EPS_MSID_A6:I = 0x7f0e00f4

.field public static final EPS_MSID_ALBUM_A5:I = 0x7f0e00f5

.field public static final EPS_MSID_ALBUM_L:I = 0x7f0e00f6

.field public static final EPS_MSID_B3:I = 0x7f0e00f7

.field public static final EPS_MSID_B4:I = 0x7f0e00f8

.field public static final EPS_MSID_B5:I = 0x7f0e00f9

.field public static final EPS_MSID_B6:I = 0x7f0e00fa

.field public static final EPS_MSID_BUZCARD_55X91:I = 0x7f0e00fb

.field public static final EPS_MSID_BUZCARD_89X50:I = 0x7f0e00fc

.field public static final EPS_MSID_CARD_54X86:I = 0x7f0e00fd

.field public static final EPS_MSID_CHOKEI_3:I = 0x7f0e00fe

.field public static final EPS_MSID_CHOKEI_4:I = 0x7f0e00ff

.field public static final EPS_MSID_CHOKEI_40:I = 0x7f0e0100

.field public static final EPS_MSID_DBLPOSTCARD:I = 0x7f0e0101

.field public static final EPS_MSID_ENV_10_L:I = 0x7f0e0102

.field public static final EPS_MSID_ENV_10_P:I = 0x7f0e0103

.field public static final EPS_MSID_ENV_B5_P:I = 0x7f0e0104

.field public static final EPS_MSID_ENV_C4_P:I = 0x7f0e0105

.field public static final EPS_MSID_ENV_C5_P:I = 0x7f0e0106

.field public static final EPS_MSID_ENV_C6_L:I = 0x7f0e0107

.field public static final EPS_MSID_ENV_C6_P:I = 0x7f0e0108

.field public static final EPS_MSID_ENV_DL_L:I = 0x7f0e0109

.field public static final EPS_MSID_ENV_DL_P:I = 0x7f0e010a

.field public static final EPS_MSID_EXECUTIVE:I = 0x7f0e010b

.field public static final EPS_MSID_HALFCUT:I = 0x7f0e010c

.field public static final EPS_MSID_HALFLETTER:I = 0x7f0e010d

.field public static final EPS_MSID_HIVISION:I = 0x7f0e010e

.field public static final EPS_MSID_INDIAN_LEGAL:I = 0x7f0e010f

.field public static final EPS_MSID_KAKU_2:I = 0x7f0e0110

.field public static final EPS_MSID_KAKU_20:I = 0x7f0e0111

.field public static final EPS_MSID_L:I = 0x7f0e0112

.field public static final EPS_MSID_LEGAL:I = 0x7f0e0113

.field public static final EPS_MSID_LETTER:I = 0x7f0e0114

.field public static final EPS_MSID_MEISHI:I = 0x7f0e0115

.field public static final EPS_MSID_MEXICO_OFICIO:I = 0x7f0e0116

.field public static final EPS_MSID_NEWENV_P:I = 0x7f0e0117

.field public static final EPS_MSID_NEWEVN_L:I = 0x7f0e0118

.field public static final EPS_MSID_OFICIO9:I = 0x7f0e0119

.field public static final EPS_MSID_PALBUM_2L:I = 0x7f0e011a

.field public static final EPS_MSID_PALBUM_A4:I = 0x7f0e011b

.field public static final EPS_MSID_PALBUM_A5_L:I = 0x7f0e011c

.field public static final EPS_MSID_PALBUM_L_L:I = 0x7f0e011d

.field public static final EPS_MSID_PANORAMIC:I = 0x7f0e011e

.field public static final EPS_MSID_POSTCARD:I = 0x7f0e011f

.field public static final EPS_MSID_QUADRAPLEPOSTCARD:I = 0x7f0e0120

.field public static final EPS_MSID_SP1:I = 0x7f0e0121

.field public static final EPS_MSID_SP2:I = 0x7f0e0122

.field public static final EPS_MSID_SP3:I = 0x7f0e0123

.field public static final EPS_MSID_SP4:I = 0x7f0e0124

.field public static final EPS_MSID_SP5:I = 0x7f0e0125

.field public static final EPS_MSID_SQUARE_5:I = 0x7f0e0126

.field public static final EPS_MSID_SQUARE_8_27:I = 0x7f0e0127

.field public static final EPS_MSID_SRA3:I = 0x7f0e0128

.field public static final EPS_MSID_TRIM_4X6:I = 0x7f0e0129

.field public static final EPS_MSID_USB:I = 0x7f0e012a

.field public static final EPS_MSID_USC:I = 0x7f0e012b

.field public static final EPS_MSID_USER:I = 0x7f0e012c

.field public static final EPS_MSID_YOKEI_0:I = 0x7f0e012d

.field public static final EPS_MSID_YOKEI_1:I = 0x7f0e012e

.field public static final EPS_MSID_YOKEI_2:I = 0x7f0e012f

.field public static final EPS_MSID_YOKEI_3:I = 0x7f0e0130

.field public static final EPS_MSID_YOKEI_4:I = 0x7f0e0131

.field public static final EPS_MSID_YOKEI_6:I = 0x7f0e0132

.field public static final EPS_MTID_3D:I = 0x7f0e0133

.field public static final EPS_MTID_ARCHMATTE:I = 0x7f0e0134

.field public static final EPS_MTID_AUTO_PLAIN:I = 0x7f0e0135

.field public static final EPS_MTID_BARYTA:I = 0x7f0e0136

.field public static final EPS_MTID_BSMATTE_DS:I = 0x7f0e0137

.field public static final EPS_MTID_BS_HALFGLOSSY_DS:I = 0x7f0e0138

.field public static final EPS_MTID_BUSINESS_PLAIN:I = 0x7f0e0139

.field public static final EPS_MTID_CDDVD:I = 0x7f0e013a

.field public static final EPS_MTID_CDDVDHIGH:I = 0x7f0e013b

.field public static final EPS_MTID_COATED:I = 0x7f0e013c

.field public static final EPS_MTID_COLOR:I = 0x7f0e013d

.field public static final EPS_MTID_ECOPHOTO:I = 0x7f0e013e

.field public static final EPS_MTID_ENVELOPE:I = 0x7f0e013f

.field public static final EPS_MTID_GLOSSYCAST:I = 0x7f0e0140

.field public static final EPS_MTID_GLOSSYHAGAKI:I = 0x7f0e0141

.field public static final EPS_MTID_GLOSSYPHOTO:I = 0x7f0e0142

.field public static final EPS_MTID_GROSSY_ROLL_STICKER:I = 0x7f0e0143

.field public static final EPS_MTID_HAGAKIATENA:I = 0x7f0e0144

.field public static final EPS_MTID_HAGAKIINKJET:I = 0x7f0e0145

.field public static final EPS_MTID_HAGAKIRECL:I = 0x7f0e0146

.field public static final EPS_MTID_HIGH_QUALITY_PLAIN:I = 0x7f0e0147

.field public static final EPS_MTID_IRON:I = 0x7f0e0148

.field public static final EPS_MTID_LABEL:I = 0x7f0e0149

.field public static final EPS_MTID_LCPP:I = 0x7f0e014a

.field public static final EPS_MTID_LETTERHEAD:I = 0x7f0e014b

.field public static final EPS_MTID_MATTE:I = 0x7f0e014c

.field public static final EPS_MTID_MATTEMEISHI:I = 0x7f0e014d

.field public static final EPS_MTID_MINIPHOTO:I = 0x7f0e014e

.field public static final EPS_MTID_PGPHOTO:I = 0x7f0e014f

.field public static final EPS_MTID_PHOTO:I = 0x7f0e0150

.field public static final EPS_MTID_PHOTOINKJET:I = 0x7f0e0151

.field public static final EPS_MTID_PHOTOINKJET2:I = 0x7f0e0152

.field public static final EPS_MTID_PHOTOSTD:I = 0x7f0e0153

.field public static final EPS_MTID_PLAIN:I = 0x7f0e0154

.field public static final EPS_MTID_PLAIN1:I = 0x7f0e0155

.field public static final EPS_MTID_PLAIN2:I = 0x7f0e0156

.field public static final EPS_MTID_PLAIN_ROLL_STICKER:I = 0x7f0e0157

.field public static final EPS_MTID_PLATINA:I = 0x7f0e0158

.field public static final EPS_MTID_PLOOFING_WHITE_MAT:I = 0x7f0e0159

.field public static final EPS_MTID_PLPHOTO:I = 0x7f0e015a

.field public static final EPS_MTID_PREPRINTED:I = 0x7f0e015b

.field public static final EPS_MTID_PSPHOTO:I = 0x7f0e015c

.field public static final EPS_MTID_RECYCLED:I = 0x7f0e015d

.field public static final EPS_MTID_SEMI_THICK:I = 0x7f0e015e

.field public static final EPS_MTID_SFHAGAKI:I = 0x7f0e015f

.field public static final EPS_MTID_SPECIAL:I = 0x7f0e0160

.field public static final EPS_MTID_THICKPAPER:I = 0x7f0e0161

.field public static final EPS_MTID_THICKPAPER1:I = 0x7f0e0162

.field public static final EPS_MTID_THICKPAPER2:I = 0x7f0e0163

.field public static final EPS_MTID_THICKPAPER3:I = 0x7f0e0164

.field public static final EPS_MTID_THICKPAPER4:I = 0x7f0e0165

.field public static final EPS_MTID_THICKPAPER5:I = 0x7f0e0166

.field public static final EPS_MTID_THINPAPER1:I = 0x7f0e0167

.field public static final EPS_MTID_TRANSPARENCY:I = 0x7f0e0168

.field public static final EPS_MTID_ULTRASMOOTH:I = 0x7f0e0169

.field public static final EPS_MTID_UNSPECIFIED:I = 0x7f0e016a

.field public static final EPS_MTID_VELVETFINEART:I = 0x7f0e016b

.field public static final EPS_MTID_WATERCOLOR:I = 0x7f0e016c

.field public static final EPS_PD_DATETYPE1:I = 0x7f0e016d

.field public static final EPS_PD_DATETYPE2:I = 0x7f0e016e

.field public static final EPS_PD_DATETYPE3:I = 0x7f0e016f

.field public static final EPS_PRNERR_3DMEDIA_DIRECTION_MSG:I = 0x7f0e0171

.field public static final EPS_PRNERR_3DMEDIA_DIRECTION_TITLE:I = 0x7f0e0172

.field public static final EPS_PRNERR_3DMEDIA_FACE_MSG:I = 0x7f0e0173

.field public static final EPS_PRNERR_3DMEDIA_FACE_TITLE:I = 0x7f0e0174

.field public static final EPS_PRNERR_ANY_MSG:I = 0x7f0e0175

.field public static final EPS_PRNERR_ANY_TITLE:I = 0x7f0e0176

.field public static final EPS_PRNERR_BATTERYEMPTY_MSG:I = 0x7f0e0177

.field public static final EPS_PRNERR_BATTERYEMPTY_TITLE:I = 0x7f0e0178

.field public static final EPS_PRNERR_BATTERYTEMPERATURE_MSG:I = 0x7f0e0179

.field public static final EPS_PRNERR_BATTERYTEMPERATURE_TITLE:I = 0x7f0e017a

.field public static final EPS_PRNERR_BATTERYVOLTAGE_MSG:I = 0x7f0e017b

.field public static final EPS_PRNERR_BATTERYVOLTAGE_TITLE:I = 0x7f0e017c

.field public static final EPS_PRNERR_BATTERY_CHARGING_MSG:I = 0x7f0e017d

.field public static final EPS_PRNERR_BATTERY_CHARGING_TITLE:I = 0x7f0e017e

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_HIGH_MSG:I = 0x7f0e017f

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_HIGH_TITLE:I = 0x7f0e0180

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_LOW_MSG:I = 0x7f0e0181

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_LOW_TITLE:I = 0x7f0e0182

.field public static final EPS_PRNERR_BK1MODE_NEED_ACCEPT_MSG:I = 0x7f0e0183

.field public static final EPS_PRNERR_BK1MODE_NEED_ACCEPT_TITLE:I = 0x7f0e0184

.field public static final EPS_PRNERR_BK1MODE_WAITING_ACCEPT_MSG:I = 0x7f0e0185

.field public static final EPS_PRNERR_BK1MODE_WAITING_ACCEPT_TITLE:I = 0x7f0e0186

.field public static final EPS_PRNERR_BORDERLESS_WIP_END_MSG:I = 0x7f0e0187

.field public static final EPS_PRNERR_BORDERLESS_WIP_END_TITLE:I = 0x7f0e0188

.field public static final EPS_PRNERR_BORDERLESS_WIP_NEAR_END_MSG:I = 0x7f0e0189

.field public static final EPS_PRNERR_BORDERLESS_WIP_NEAR_END_TITLE:I = 0x7f0e018a

.field public static final EPS_PRNERR_BUSY_MSG:I = 0x7f0e018b

.field public static final EPS_PRNERR_BUSY_TITLE:I = 0x7f0e018c

.field public static final EPS_PRNERR_CARDLOADING_MSG:I = 0x7f0e018d

.field public static final EPS_PRNERR_CARDLOADING_TITLE:I = 0x7f0e018e

.field public static final EPS_PRNERR_CARTRIDGEOVERFLOW_MSG:I = 0x7f0e018f

.field public static final EPS_PRNERR_CARTRIDGEOVERFLOW_TITLE:I = 0x7f0e0190

.field public static final EPS_PRNERR_CASSETTECOVER_CLOSED_MSG:I = 0x7f0e0191

.field public static final EPS_PRNERR_CASSETTECOVER_CLOSED_TITLE:I = 0x7f0e0192

.field public static final EPS_PRNERR_CASSETTECOVER_OPENED_MSG:I = 0x7f0e0193

.field public static final EPS_PRNERR_CASSETTECOVER_OPENED_TITLE:I = 0x7f0e0194

.field public static final EPS_PRNERR_CDDVDCONFIG_FEEDBUTTON_MSG:I = 0x7f0e0195

.field public static final EPS_PRNERR_CDDVDCONFIG_FEEDBUTTON_TITLE:I = 0x7f0e0196

.field public static final EPS_PRNERR_CDDVDCONFIG_MSG:I = 0x7f0e0197

.field public static final EPS_PRNERR_CDDVDCONFIG_STARTBUTTON_MSG:I = 0x7f0e0198

.field public static final EPS_PRNERR_CDDVDCONFIG_STARTBUTTON_TITLE:I = 0x7f0e0199

.field public static final EPS_PRNERR_CDDVDCONFIG_TITLE:I = 0x7f0e019a

.field public static final EPS_PRNERR_CDGUIDECLOSE_MSG:I = 0x7f0e019b

.field public static final EPS_PRNERR_CDGUIDECLOSE_TITLE:I = 0x7f0e019c

.field public static final EPS_PRNERR_CDREXIST_MAINTE_MSG:I = 0x7f0e019d

.field public static final EPS_PRNERR_CDREXIST_MAINTE_TITLE:I = 0x7f0e019e

.field public static final EPS_PRNERR_CDRGUIDEOPEN_MSG:I = 0x7f0e019f

.field public static final EPS_PRNERR_CDRGUIDEOPEN_TITLE:I = 0x7f0e01a0

.field public static final EPS_PRNERR_CEMPTY_MSG:I = 0x7f0e01a1

.field public static final EPS_PRNERR_CEMPTY_TITLE:I = 0x7f0e01a2

.field public static final EPS_PRNERR_CFAIL_MSG:I = 0x7f0e01a3

.field public static final EPS_PRNERR_CFAIL_TITLE:I = 0x7f0e01a4

.field public static final EPS_PRNERR_COLOR_INKOUT_MSG:I = 0x7f0e01a5

.field public static final EPS_PRNERR_COLOR_INKOUT_TITLE:I = 0x7f0e01a6

.field public static final EPS_PRNERR_COMM1:I = 0x7f0e01a8

.field public static final EPS_PRNERR_COMM2:I = 0x7f0e01aa

.field public static final EPS_PRNERR_COMM2_MSG:I = 0x7f0e01ab

.field public static final EPS_PRNERR_COMM2_TITLE:I = 0x7f0e01ac

.field public static final EPS_PRNERR_COMM3:I = 0x7f0e01ad

.field public static final EPS_PRNERR_COMM4:I = 0x7f0e01ae

.field public static final EPS_PRNERR_COMM5:I = 0x7f0e01af

.field public static final EPS_PRNERR_COMM_MSG:I = 0x7f0e01b0

.field public static final EPS_PRNERR_COMM_TITLE:I = 0x7f0e01b1

.field public static final EPS_PRNERR_COMM_TITLE3:I = 0x7f0e01b4

.field public static final EPS_PRNERR_COVEROPEN_MSG:I = 0x7f0e01b6

.field public static final EPS_PRNERR_COVEROPEN_TITLE:I = 0x7f0e01b7

.field public static final EPS_PRNERR_DISABEL_CLEANING_MSG:I = 0x7f0e01b8

.field public static final EPS_PRNERR_DISABEL_CLEANING_TITLE:I = 0x7f0e01b9

.field public static final EPS_PRNERR_DISABLE_DUPLEX_MSG:I = 0x7f0e01ba

.field public static final EPS_PRNERR_DISABLE_DUPLEX_TITLE:I = 0x7f0e01bb

.field public static final EPS_PRNERR_DOUBLEFEED_MSG:I = 0x7f0e01bc

.field public static final EPS_PRNERR_DOUBLEFEED_TITLE:I = 0x7f0e01bd

.field public static final EPS_PRNERR_FACTORY_MSG:I = 0x7f0e01be

.field public static final EPS_PRNERR_FACTORY_TITLE:I = 0x7f0e01bf

.field public static final EPS_PRNERR_FATAL_MSG:I = 0x7f0e01c0

.field public static final EPS_PRNERR_FATAL_TITLE:I = 0x7f0e01c1

.field public static final EPS_PRNERR_FEEDERCLOSE_MSG:I = 0x7f0e01c2

.field public static final EPS_PRNERR_FEEDERCLOSE_TITLE:I = 0x7f0e01c3

.field public static final EPS_PRNERR_GENERAL_MSG:I = 0x7f0e01c5

.field public static final EPS_PRNERR_GENERAL_TITLE:I = 0x7f0e01c6

.field public static final EPS_PRNERR_INKCOVEROPEN_MSG:I = 0x7f0e01c7

.field public static final EPS_PRNERR_INKCOVEROPEN_TITLE:I = 0x7f0e01c8

.field public static final EPS_PRNERR_INKOUT_BK1MODE_MSG:I = 0x7f0e01c9

.field public static final EPS_PRNERR_INKOUT_BK1MODE_TITLE:I = 0x7f0e01ca

.field public static final EPS_PRNERR_INKOUT_MSG:I = 0x7f0e01cb

.field public static final EPS_PRNERR_INKOUT_TITLE:I = 0x7f0e01cc

.field public static final EPS_PRNERR_INK_TONER_OUT_MSG:I = 0x7f0e01cd

.field public static final EPS_PRNERR_INK_TONER_OUT_TITLE:I = 0x7f0e01ce

.field public static final EPS_PRNERR_INTERFACE_MSG:I = 0x7f0e01cf

.field public static final EPS_PRNERR_INTERFACE_TITLE:I = 0x7f0e01d0

.field public static final EPS_PRNERR_INTERRUPT_BY_INKEND_MSG:I = 0x7f0e01d1

.field public static final EPS_PRNERR_INTERRUPT_BY_INKEND_TITLE:I = 0x7f0e01d2

.field public static final EPS_PRNERR_LOW_BATTERY_FNC_MSG:I = 0x7f0e01d3

.field public static final EPS_PRNERR_LOW_BATTERY_FNC_TITLE:I = 0x7f0e01d4

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_MSG:I = 0x7f0e01d5

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_NOLCD_MSG:I = 0x7f0e01d6

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_NOLCD_TITLE:I = 0x7f0e01d7

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_TITLE:I = 0x7f0e01d8

.field public static final EPS_PRNERR_MANUALFEED_FAILED_MSG:I = 0x7f0e01d9

.field public static final EPS_PRNERR_MANUALFEED_FAILED_NOLCD_MSG:I = 0x7f0e01da

.field public static final EPS_PRNERR_MANUALFEED_FAILED_NOLCD_TITLE:I = 0x7f0e01db

.field public static final EPS_PRNERR_MANUALFEED_FAILED_TITLE:I = 0x7f0e01dc

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_MSG:I = 0x7f0e01dd

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_NOLCD_MSG:I = 0x7f0e01de

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_NOLCD_TITLE:I = 0x7f0e01df

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_TITLE:I = 0x7f0e01e0

.field public static final EPS_PRNERR_NOTRAY_MSG:I = 0x7f0e01e1

.field public static final EPS_PRNERR_NOTRAY_TITLE:I = 0x7f0e01e2

.field public static final EPS_PRNERR_NOT_INITIALFILL_MSG:I = 0x7f0e01e3

.field public static final EPS_PRNERR_NOT_INITIALFILL_TITLE:I = 0x7f0e01e4

.field public static final EPS_PRNERR_NO_BATTERY_MSG:I = 0x7f0e01e7

.field public static final EPS_PRNERR_NO_BATTERY_TITLE:I = 0x7f0e01e8

.field public static final EPS_PRNERR_NO_MAINTENANCE_BOX_MSG:I = 0x7f0e01e9

.field public static final EPS_PRNERR_NO_MAINTENANCE_BOX_TITLE:I = 0x7f0e01ea

.field public static final EPS_PRNERR_NO_STAPLE_MSG:I = 0x7f0e01eb

.field public static final EPS_PRNERR_NO_STAPLE_TITLE:I = 0x7f0e01ec

.field public static final EPS_PRNERR_PAPERJAM_MRP_MSG:I = 0x7f0e01ed

.field public static final EPS_PRNERR_PAPERJAM_MRP_TITLE:I = 0x7f0e01ee

.field public static final EPS_PRNERR_PAPERJAM_MSG:I = 0x7f0e01ef

.field public static final EPS_PRNERR_PAPERJAM_TITLE:I = 0x7f0e01f0

.field public static final EPS_PRNERR_PAPEROUT_MRP_MSG:I = 0x7f0e01f1

.field public static final EPS_PRNERR_PAPEROUT_MRP_TITLE:I = 0x7f0e01f2

.field public static final EPS_PRNERR_PAPEROUT_MSG:I = 0x7f0e01f3

.field public static final EPS_PRNERR_PAPEROUT_TITLE:I = 0x7f0e01f4

.field public static final EPS_PRNERR_PC_DIRECTION1_MSG:I = 0x7f0e01f5

.field public static final EPS_PRNERR_PC_DIRECTION1_TITLE:I = 0x7f0e01f6

.field public static final EPS_PRNERR_PC_DIRECTION2_MSG:I = 0x7f0e01f7

.field public static final EPS_PRNERR_PC_DIRECTION2_TITLE:I = 0x7f0e01f8

.field public static final EPS_PRNERR_PC_FACE1_MSG:I = 0x7f0e01f9

.field public static final EPS_PRNERR_PC_FACE1_TITLE:I = 0x7f0e01fa

.field public static final EPS_PRNERR_PC_FACE2_MSG:I = 0x7f0e01fb

.field public static final EPS_PRNERR_PC_FACE2_TITLE:I = 0x7f0e01fc

.field public static final EPS_PRNERR_PRINTPACKEND_MSG:I = 0x7f0e01fd

.field public static final EPS_PRNERR_PRINTPACKEND_TITLE:I = 0x7f0e01fe

.field public static final EPS_PRNERR_READYPRINT_SERVICE_MSG:I = 0x7f0e01ff

.field public static final EPS_PRNERR_READYPRINT_SERVICE_TITLE:I = 0x7f0e0200

.field public static final EPS_PRNERR_REPLACE_MAINTENANCE_BOX_MSG:I = 0x7f0e0201

.field public static final EPS_PRNERR_REPLACE_MAINTENANCE_BOX_TITLE:I = 0x7f0e0202

.field public static final EPS_PRNERR_ROLLPAPER_TOOSHORT_MSG:I = 0x7f0e0203

.field public static final EPS_PRNERR_ROLLPAPER_TOOSHORT_TITLE:I = 0x7f0e0204

.field public static final EPS_PRNERR_SCANNEROPEN_MSG:I = 0x7f0e0205

.field public static final EPS_PRNERR_SCANNEROPEN_TITLE:I = 0x7f0e0206

.field public static final EPS_PRNERR_SERVICEREQ_MSG:I = 0x7f0e0207

.field public static final EPS_PRNERR_SERVICEREQ_TITLE:I = 0x7f0e0208

.field public static final EPS_PRNERR_SHUTOFF_MSG:I = 0x7f0e0209

.field public static final EPS_PRNERR_SHUTOFF_TITLE:I = 0x7f0e020a

.field public static final EPS_PRNERR_SIZE_TYPE_PATH_MRP_MSG:I = 0x7f0e020b

.field public static final EPS_PRNERR_SIZE_TYPE_PATH_MRP_TITLE:I = 0x7f0e020c

.field public static final EPS_PRNERR_SIZE_TYPE_PATH_MSG:I = 0x7f0e020d

.field public static final EPS_PRNERR_SIZE_TYPE_PATH_TITLE:I = 0x7f0e020e

.field public static final EPS_PRNERR_STACKER_FULL_MSG:I = 0x7f0e020f

.field public static final EPS_PRNERR_STACKER_FULL_TITLE:I = 0x7f0e0210

.field public static final EPS_PRNERR_TRAYCLOSE_MSG:I = 0x7f0e0211

.field public static final EPS_PRNERR_TRAYCLOSE_TITLE:I = 0x7f0e0212

.field public static final EPS_PRNERR_WEB_REMOTE_MAINTENANCE_BOX_MSG:I = 0x7f0e0213

.field public static final EPS_PRNERR_WEB_REMOTE_MAINTENANCE_BOX_TITLE:I = 0x7f0e0214

.field public static final EPS_PRNERR_WIP_NEAR_END_MSG:I = 0x7f0e0215

.field public static final EPS_PRNERR_WIP_NEAR_END_TITLE:I = 0x7f0e0216

.field public static final EPS_PRNST_BUSY_MSG:I = 0x7f0e0217

.field public static final EPS_PRNST_BUSY_TITLE:I = 0x7f0e0218

.field public static final EPS_PRNST_CANCELLING_MSG:I = 0x7f0e0219

.field public static final EPS_PRNST_CANCELLING_TITLE:I = 0x7f0e021a

.field public static final EPS_PRNST_IDLE_MSG:I = 0x7f0e021b

.field public static final EPS_PRNST_IDLE_TITLE:I = 0x7f0e021c

.field public static final EPS_PRNST_PRINTING_MSG:I = 0x7f0e021d

.field public static final EPS_PRNST_PRINTING_TITLE:I = 0x7f0e021e

.field public static final EPS_SHARPNESS_OFF:I = 0x7f0e021f

.field public static final EPS_SHARPNESS_ON:I = 0x7f0e0220

.field public static final End_page:I = 0x7f0e0222

.field public static final Executing___:I = 0x7f0e0223

.field public static final Head_Cleaning:I = 0x7f0e0235

.field public static final Layout:I = 0x7f0e0236

.field public static final Maintenance:I = 0x7f0e0237

.field public static final Maintenance_Box_Service_Life:I = 0x7f0e0238

.field public static final Media_Type:I = 0x7f0e0239

.field public static final No:I = 0x7f0e023c

.field public static final Nozzle_Check:I = 0x7f0e023d

.field public static final OK:I = 0x7f0e023e

.field public static final Off:I = 0x7f0e0240

.field public static final On:I = 0x7f0e0241

.field public static final Orientation:I = 0x7f0e0242

.field public static final Paper_Size:I = 0x7f0e0243

.field public static final Paper_Source:I = 0x7f0e0244

.field public static final Print_All:I = 0x7f0e0245

.field public static final Print_Date:I = 0x7f0e0246

.field public static final Print_Quality:I = 0x7f0e0247

.field public static final Print_Settings:I = 0x7f0e0248

.field public static final Printer:I = 0x7f0e0249

.field public static final Printer_Settings:I = 0x7f0e024a

.field public static final Printer_Status:I = 0x7f0e024b

.field public static final Printing_Range:I = 0x7f0e024c

.field public static final Remaining_Ink:I = 0x7f0e024e

.field public static final Remaining_Ink_Toner:I = 0x7f0e024f

.field public static final Saturation:I = 0x7f0e0250

.field public static final Select_a_printer_:I = 0x7f0e0254

.field public static final Serial_Number:I = 0x7f0e0255

.field public static final Sharpness:I = 0x7f0e0256

.field public static final Start_page:I = 0x7f0e0257

.field public static final Tap_Printer_to_select_a_printer_:I = 0x7f0e0258

.field public static final Yes:I = 0x7f0e0279

.field public static final n2_Sided_Printing:I = 0x7f0e03d7

.field public static final str_err_msg_out_of_memory_title:I = 0x7f0e04a0

.field public static final str_error_get_info_from_printer_title:I = 0x7f0e04b8

.field public static final title_Color:I = 0x7f0e0535


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

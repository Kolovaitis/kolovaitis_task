.class public Lcom/epson/mobilephone/common/escpr/Info_paper;
.super Ljava/lang/Object;
.source "Info_paper.java"


# static fields
.field public static boder_pixel:I = 0x2a


# instance fields
.field private border_bottom_margin:I

.field private border_left_margin:I

.field private border_right_margin:I

.field private border_top_margin:I

.field private borderless_bottom_margin:I

.field private borderless_left_margin:I

.field private borderless_right_margin:I

.field private borderless_top_margin:I

.field private paper_height:I

.field private paper_height_boder:I

.field private paper_height_boderless:I

.field private paper_width:I

.field private paper_width_boder:I

.field private paper_width_boderless:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(IIIIIIII)V
    .locals 0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput p2, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height:I

    .line 103
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width:I

    .line 104
    iput p4, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boder:I

    .line 105
    iput p8, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boderless:I

    .line 106
    iput p3, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boder:I

    .line 107
    iput p7, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boderless:I

    .line 109
    iget p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width:I

    iget p2, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boder:I

    sub-int p3, p1, p2

    div-int/lit8 p3, p3, 0x2

    iput p3, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_left_margin:I

    .line 110
    iget p3, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height:I

    iget p4, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boder:I

    sub-int p7, p3, p4

    div-int/lit8 p7, p7, 0x2

    iput p7, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_top_margin:I

    sub-int p2, p1, p2

    .line 111
    div-int/lit8 p2, p2, 0x2

    iput p2, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_right_margin:I

    sub-int p2, p3, p4

    .line 112
    div-int/lit8 p2, p2, 0x2

    iput p2, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_bottom_margin:I

    .line 113
    iput p5, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_left_margin:I

    .line 114
    iput p6, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_top_margin:I

    .line 115
    iget p2, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boderless:I

    sub-int/2addr p2, p1

    sub-int/2addr p2, p5

    iput p2, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_right_margin:I

    .line 116
    iget p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boderless:I

    sub-int/2addr p1, p3

    sub-int/2addr p1, p6

    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_bottom_margin:I

    return-void
.end method

.method public constructor <init>(IIIIIIIIIIIIII)V
    .locals 0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput p2, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height:I

    .line 125
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width:I

    .line 126
    iput p4, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boder:I

    .line 127
    iput p8, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boderless:I

    .line 128
    iput p3, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boder:I

    .line 129
    iput p7, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boderless:I

    .line 131
    iput p9, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_left_margin:I

    .line 132
    iput p10, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_top_margin:I

    .line 133
    iput p11, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_right_margin:I

    .line 134
    iput p12, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_bottom_margin:I

    .line 135
    iput p5, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_left_margin:I

    .line 136
    iput p6, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_top_margin:I

    .line 137
    iput p13, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_right_margin:I

    .line 138
    iput p14, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_bottom_margin:I

    return-void
.end method


# virtual methods
.method public getBorderBottomMargin()I
    .locals 1

    .line 76
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_bottom_margin:I

    return v0
.end method

.method public getBorderLeftMargin()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_left_margin:I

    return v0
.end method

.method public getBorderRightMargin()I
    .locals 1

    .line 73
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_right_margin:I

    return v0
.end method

.method public getBorderTopMargin()I
    .locals 1

    .line 70
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->border_top_margin:I

    return v0
.end method

.method public getBottomMargin()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_bottom_margin:I

    return v0
.end method

.method public getLeftMargin()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_left_margin:I

    return v0
.end method

.method public getPaper_height()I
    .locals 1

    .line 10
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height:I

    return v0
.end method

.method public getPaper_height_boder()I
    .locals 1

    .line 22
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boder:I

    return v0
.end method

.method public getPaper_height_boderless()I
    .locals 1

    .line 34
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boderless:I

    return v0
.end method

.method public getPaper_width()I
    .locals 1

    .line 80
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width:I

    return v0
.end method

.method public getPaper_width_boder()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boder:I

    return v0
.end method

.method public getPaper_width_boderless()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boderless:I

    return v0
.end method

.method public getRightMargin()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_right_margin:I

    return v0
.end method

.method public getTopMargin()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_top_margin:I

    return v0
.end method

.method public setLeftMargin(I)V
    .locals 0

    .line 48
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_left_margin:I

    return-void
.end method

.method public setPaper_height(I)V
    .locals 0

    .line 13
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height:I

    return-void
.end method

.method public setPaper_height_boder(I)V
    .locals 0

    .line 25
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boder:I

    return-void
.end method

.method public setPaper_height_boderless(I)V
    .locals 0

    .line 37
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_height_boderless:I

    return-void
.end method

.method public setPaper_width(I)V
    .locals 0

    .line 83
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width:I

    return-void
.end method

.method public setPaper_width_boder(I)V
    .locals 0

    .line 19
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boder:I

    return-void
.end method

.method public setPaper_width_boderless(I)V
    .locals 0

    .line 31
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->paper_width_boderless:I

    return-void
.end method

.method public setTopMargin(I)V
    .locals 0

    .line 54
    iput p1, p0, Lcom/epson/mobilephone/common/escpr/Info_paper;->borderless_top_margin:I

    return-void
.end method

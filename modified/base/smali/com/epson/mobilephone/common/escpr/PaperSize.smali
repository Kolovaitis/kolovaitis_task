.class public Lcom/epson/mobilephone/common/escpr/PaperSize;
.super Ljava/lang/Object;
.source "PaperSize.java"


# instance fields
.field public mHeight:I

.field mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

.field public mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 11
    iput v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mWidth:I

    .line 12
    iput v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mHeight:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 16
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 17
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_width()I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mWidth:I

    .line 18
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_height()I

    move-result p1

    iput p1, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mHeight:I

    .line 19
    iget-object p1, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    return-void
.end method


# virtual methods
.method public getHeightPaper()I
    .locals 1

    .line 86
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mHeight:I

    return v0
.end method

.method public getHeight_boder(I)I
    .locals 1

    .line 47
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 48
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_height_boder()I

    move-result p1

    .line 50
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    return p1
.end method

.method public getHeight_boderLess(I)I
    .locals 1

    .line 63
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 64
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_height_boderless()I

    move-result p1

    .line 66
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    return p1
.end method

.method public getLeftMargin(I)I
    .locals 1

    .line 79
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 80
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getLeftMargin()I

    move-result p1

    .line 82
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    .line 83
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    return p1
.end method

.method public getPaperSizeBoder(I)[I
    .locals 3

    const/4 v0, 0x2

    .line 29
    new-array v0, v0, [I

    .line 30
    new-instance v1, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 31
    iget-object v1, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v1, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_width_boder()I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 33
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_height_boder()I

    move-result p1

    const/4 v1, 0x1

    aput p1, v0, v1

    .line 34
    iget-object p1, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    return-object v0
.end method

.method public getTopMargin(I)I
    .locals 1

    .line 71
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 72
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getTopMargin()I

    move-result p1

    .line 74
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    .line 75
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    return p1
.end method

.method public getWidthPaper()I
    .locals 1

    .line 89
    iget v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mWidth:I

    return v0
.end method

.method public getWidth_boder(I)I
    .locals 1

    .line 39
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 40
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_width_boder()I

    move-result p1

    .line 42
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    return p1
.end method

.method public getWidth_boderLess(I)I
    .locals 1

    .line 55
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 56
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_width_boderless()I

    move-result p1

    .line 58
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    return p1
.end method

.method public setPaperSize(I)V
    .locals 1

    .line 22
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    .line 23
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_width()I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mWidth:I

    .line 25
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/Info_paper;->getPaper_height()I

    move-result p1

    iput p1, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mHeight:I

    .line 26
    iget-object p1, p0, Lcom/epson/mobilephone/common/escpr/PaperSize;->mInfo:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->destructor()V

    return-void
.end method

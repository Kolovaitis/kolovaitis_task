.class public Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;
.super Ljava/lang/Object;
.source "MediaInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/MediaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ColorTable"
.end annotation


# static fields
.field protected static mHashMap:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 173
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    .line 175
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_all_black:I

    .line 176
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_BLACK:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v6, 0x1

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_BLACK_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v7, 0x2

    aput-object v4, v3, v7

    .line 175
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CYAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_cyan:I

    .line 178
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_CYAN:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_CYAN_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 177
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_MAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_magenta:I

    .line 180
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_MAGENTA:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_MAGENTA_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 179
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_YELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_yellow:I

    .line 182
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_YELLOW:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_YELLOW_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 181
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTCYAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_lightcyan:I

    .line 184
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTCYAN:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTCYAN_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 183
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTMAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_lightmagenta:I

    .line 186
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTMAGENTA:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTMAGENTA_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 185
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTYELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_lightyellow:I

    .line 188
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTYELLOW:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTYELLOW_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 187
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_DARKYELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_darkyellow:I

    .line 190
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_DARKYELLOW:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_DARKYELLOW_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 189
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_lightblack:I

    .line 192
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTBLACK:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTBLACK_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 191
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_RED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_red:I

    .line 194
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_RED:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_RED_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 193
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIOLET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_violet:I

    .line 196
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_VIOLET:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_VIOLET_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 195
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_MATTEBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_matteblack:I

    .line 198
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_MATTEBLACK:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_MATTEBLACK_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 197
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTLIGHTBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_lightlightblack:I

    .line 200
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTLIGHTBLACK:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTLIGHTBLACK__CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 199
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_PHOTOBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_photoblack:I

    .line 202
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_PHOTOBLACK:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_PHOTOBLACK_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 201
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CLEAR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_clear:I

    .line 204
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_CLEAR:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_CLEAR_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 203
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_GRAY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_gray:I

    .line 206
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_GRAY:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_GRAY_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 205
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_black2:I

    .line 210
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_BLACK2:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_BLACK2_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 209
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_ORANGE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_orange:I

    .line 212
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_ORANGE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_ORANGE_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 211
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_GREEN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_green:I

    .line 214
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_GREEN:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_GREEN_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 213
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_WHITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_all_white:I

    .line 216
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_WHITE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_WHITE_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 215
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CLEAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_clean:I

    .line 218
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_CLEAN:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_CLEAN_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 217
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_COMPOSITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_composite:I

    .line 222
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_COMPOSITE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_COMPOSITE_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 221
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_all_black:I

    .line 226
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_BLACK1:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_BLACK1_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 225
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLUE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_blue:I

    .line 230
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_BLUE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_BLUE_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 229
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_DEEP_BLUE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_deep_blue:I

    .line 232
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_DEEP_BLUE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_DEEP_BLUE_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 231
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIVID_MAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_vivid_magenta:I

    .line 234
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_VIVID_MAGENTA:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_VIVID_MAGENTA_CON:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 233
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIVID_LIGHTMAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Integer;

    sget v4, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_vivid_lightmagenta:I

    .line 236
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_VIVID_LIGHTMAGENTA:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_VIVID_LIGHTMAGENTA_CON:I

    .line 237
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 235
    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTGRAY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Integer;

    sget v3, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_lightgray:I

    .line 239
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    sget v3, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTGRAY:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    sget v3, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_LIGHTGRAY_CON:I

    .line 240
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    .line 238
    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getColorName(I)[Ljava/lang/Integer;
    .locals 2

    .line 245
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/Integer;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getColorNameByModel(ILjava/lang/String;)[Ljava/lang/Integer;
    .locals 6

    .line 253
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 254
    sget-object v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;->mHashMap:Ljava/util/Hashtable;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    const-string v1, "SC-PX1V"

    .line 255
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x3

    if-nez v1, :cond_3

    const-string v1, "SC-PX1VL"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "SC-P700 Series"

    .line 259
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "SC-P900 Series"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 260
    :cond_1
    sget-object p1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_PHOTOBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result p1

    if-ne p0, p1, :cond_2

    .line 261
    new-array v0, v5, [Ljava/lang/Integer;

    sget p0, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_all_black:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v4

    sget p0, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_PHOTOBLACK:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v3

    sget p0, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_PHOTOBLACK_CON_PK:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v2

    goto :goto_1

    .line 262
    :cond_2
    sget-object p1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_MATTEBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result p1

    if-ne p0, p1, :cond_4

    .line 263
    new-array v0, v5, [Ljava/lang/Integer;

    sget p0, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_all_black:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v4

    sget p0, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_MATTEBLACK:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v3

    sget p0, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_MATTEBLACK_CON_MK:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v2

    goto :goto_1

    .line 256
    :cond_3
    :goto_0
    sget-object p1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_PHOTOBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->ordinal()I

    move-result p1

    if-ne p0, p1, :cond_4

    .line 257
    new-array v0, v5, [Ljava/lang/Integer;

    sget p0, Lcom/epson/mobilephone/common/escpr/R$color;->epson_lib_all_black:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v4

    sget p0, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_PHOTOBLACK:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v3

    sget p0, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_COLOR_PHOTOBLACK_CON_BK:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v2

    :cond_4
    :goto_1
    return-object v0

    :cond_5
    const/4 p0, 0x0

    return-object p0
.end method

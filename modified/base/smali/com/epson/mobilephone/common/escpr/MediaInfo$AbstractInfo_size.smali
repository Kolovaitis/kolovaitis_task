.class public abstract Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;
.super Ljava/lang/Object;
.source "MediaInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/MediaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AbstractInfo_size"
.end annotation


# instance fields
.field protected final sCodeTable:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable<",
            "Ljava/lang/Integer;",
            "Lcom/epson/mobilephone/common/escpr/Info_paper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->sCodeTable:Ljava/util/Hashtable;

    return-void
.end method


# virtual methods
.method public destructor()V
    .locals 1

    .line 442
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->sCodeTable:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    return-void
.end method

.method public getStringId(I)Lcom/epson/mobilephone/common/escpr/Info_paper;
    .locals 2

    .line 418
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->sCodeTable:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 419
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->sCodeTable:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/escpr/Info_paper;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getStringIdForFormPrint(I)Lcom/epson/mobilephone/common/escpr/Info_paper;
    .locals 17

    move-object/from16 v0, p0

    const/4 v1, 0x3

    move/from16 v2, p1

    if-ne v2, v1, :cond_0

    .line 427
    new-instance v1, Lcom/epson/mobilephone/common/escpr/Info_paper;

    const/16 v3, 0x832

    const/16 v4, 0xba0

    const/16 v5, 0x718

    const/16 v6, 0xa86

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x832

    const/16 v10, 0xba0

    const/16 v11, 0x8d

    const/16 v12, 0x8d

    const/16 v13, 0x8d

    const/16 v14, 0x8d

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v16}, Lcom/epson/mobilephone/common/escpr/Info_paper;-><init>(IIIIIIIIIIIIII)V

    return-object v1

    .line 434
    :cond_0
    iget-object v1, v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->sCodeTable:Ljava/util/Hashtable;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 435
    iget-object v1, v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;->sCodeTable:Ljava/util/Hashtable;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/mobilephone/common/escpr/Info_paper;

    return-object v1

    :cond_1
    const/4 v1, 0x0

    return-object v1
.end method

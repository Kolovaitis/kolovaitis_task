.class public Lcom/epson/mobilephone/common/escpr/EscprLib;
.super Ljava/lang/Object;
.source "EscprLib.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;,
        Lcom/epson/mobilephone/common/escpr/EscprLib$snmpParseType;
    }
.end annotation


# static fields
.field public static final EPS_ERR_NONE:I = 0x0

.field public static final EPS_ERR_PRINTER_NOT_FOUND:I = -0x514

.field private static final MACADRESS_SIZE:I = 0x6

.field public static final MSG_FIND_PRINTER_CALLBACK:I = 0x0

.field public static final MSG_NOTIFY_CONTINUEABLE:I = 0x1

.field public static final MSG_NOTIFY_ERROR:I = 0x2

.field public static final MSG_NOTIFY_PAGE:I = 0x1e

.field private static final RUNDOM_SIZE:I = 0x4

.field public static final SECURITY_TYPE_AUTO:I = 0xff

.field public static final SECURITY_TYPE_NONE:I

.field private static final instance:Lcom/epson/mobilephone/common/escpr/EscprLib;


# instance fields
.field final EPS_CANCEL:I

.field final EPS_CONTINUE:I

.field volatile bInvalidId:Z

.field private cardprintPackageName:Ljava/lang/String;

.field private debugString:Ljava/lang/String;

.field private iPrintPackageName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mIsSearch:Z

.field volatile printerId:Ljava/lang/String;

.field volatile printerIp:Ljava/lang/String;

.field private printerList:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "escprlib"

    .line 30
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 35
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 39
    :goto_0
    new-instance v0, Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib;->instance:Lcom/epson/mobilephone/common/escpr/EscprLib;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "epson.print"

    .line 47
    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->iPrintPackageName:Ljava/lang/String;

    const-string v0, "com.epson.cardprint"

    .line 48
    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->cardprintPackageName:Ljava/lang/String;

    const/4 v0, 0x0

    .line 53
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->bInvalidId:Z

    const/4 v0, 0x4

    .line 57
    iput v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->EPS_CONTINUE:I

    const/4 v0, 0x1

    .line 58
    iput v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->EPS_CANCEL:I

    return-void
.end method

.method private cStrToString([B)Ljava/lang/String;
    .locals 2

    .line 598
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    const/4 p1, 0x0

    .line 599
    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private native epsWrapperFindPrinter(II)I
.end method

.method private native epsWrapperGetFirmwareInfo([B[B[B[B[B[B)I
.end method

.method private native epsWrapperGetReadyPrintStatus([I)I
.end method

.method private native epsWrapperProbePrinter2(II[C)I
.end method

.method private findPrinterCB()V
    .locals 8

    const-string v0, "JAVA findPrinterCB"

    .line 466
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 468
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerList:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 469
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EXTRACT Printer information: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerList:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerList:Ljava/lang/String;

    const-string v1, "\\|\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 472
    iget-object v1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerIp:Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 474
    iget-object v1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerIp:Ljava/lang/String;

    aget-object v5, v0, v3

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerId:Ljava/lang/String;

    aget-object v5, v0, v2

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 475
    iput-boolean v4, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->bInvalidId:Z

    .line 476
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid IP Printer: Expected = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 481
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "JAVA mIsSearch == "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v5, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mIsSearch:Z

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 483
    iget-boolean v1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mIsSearch:Z

    if-eqz v1, :cond_4

    .line 493
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "JAVA mHandler == "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 494
    iget-object v1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_4

    .line 495
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const/4 v5, 0x0

    .line 496
    iput v5, v1, Landroid/os/Message;->what:I

    .line 497
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 499
    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sub-int/2addr v5, v4

    const-string v7, "index"

    .line 500
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "name"

    .line 501
    aget-object v4, v0, v4

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ip"

    .line 502
    aget-object v3, v0, v3

    invoke-virtual {v6, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "id"

    .line 503
    aget-object v2, v0, v2

    invoke-virtual {v6, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    array-length v2, v0

    const/4 v3, 0x6

    if-lt v2, v3, :cond_1

    const-string v2, "serial_no"

    const/4 v4, 0x5

    .line 505
    aget-object v4, v0, v4

    invoke-virtual {v6, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    :cond_1
    array-length v2, v0

    const/4 v4, 0x7

    if-lt v2, v4, :cond_2

    const-string v2, "common_devicename"

    .line 508
    aget-object v3, v0, v3

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    :cond_2
    array-length v2, v0

    const/16 v3, 0x8

    if-lt v2, v3, :cond_3

    const-string v2, "language"

    .line 511
    aget-object v0, v0, v4

    invoke-virtual {v6, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :cond_3
    invoke-virtual {v1, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    const-string v0, "JAVA send message"

    .line 514
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_4
    :goto_0
    const-string v0, ""

    .line 521
    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerList:Ljava/lang/String;

    return-void
.end method

.method public static getInstance()Lcom/epson/mobilephone/common/escpr/EscprLib;
    .locals 1

    .line 44
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib;->instance:Lcom/epson/mobilephone/common/escpr/EscprLib;

    return-object v0
.end method

.method public static getMd5([B)[B
    .locals 2

    const/4 v0, 0x1

    .line 611
    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v1, v0, v1

    :try_start_0
    const-string v1, "MD5"

    .line 616
    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 617
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 618
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 620
    invoke-virtual {p0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private native start_job(IIIIIIIIIIIIIIIIIII)I
.end method

.method private native start_job2(IIIIIIIIIIII)I
.end method


# virtual methods
.method public native cancel_print()I
.end method

.method public native cancel_search_printer()I
.end method

.method public native confirm_cancel(Z)I
.end method

.method public native confirm_continue(Z)I
.end method

.method public doFindPrinter(II)I
    .locals 2

    const-string v0, "escprLib2"

    const-string v1, "**********Call do find printer************"

    .line 379
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-direct {p0, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperFindPrinter(II)I

    move-result p1

    return p1
.end method

.method public native end_job()I
.end method

.method public native end_job(I)I
.end method

.method public native end_page(Z)I
.end method

.method public epsGetReadyPrintStatus(Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;)I
    .locals 3

    const/4 v0, 0x5

    .line 357
    new-array v0, v0, [I

    const/4 v1, 0x0

    .line 358
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 360
    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetReadyPrintStatus([I)I

    move-result v2

    .line 362
    aget v1, v0, v1

    iput v1, p1, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;->emaStatus:I

    const/4 v1, 0x1

    .line 363
    aget v1, v0, v1

    iput v1, p1, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;->subscriptionStatus:I

    const/4 v1, 0x2

    .line 364
    aget v1, v0, v1

    iput v1, p1, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;->subscriptionErrorCode:I

    const/4 v1, 0x3

    .line 365
    aget v1, v0, v1

    iput v1, p1, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;->subscriptionMode:I

    const/4 v1, 0x4

    .line 366
    aget v0, v0, v1

    iput v0, p1, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;->subscriptionError:I

    return v2
.end method

.method public epsNotifyContinueable(I)V
    .locals 2

    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "called epsNotifyContinueable :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 316
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    .line 317
    iput v1, v0, Landroid/os/Message;->what:I

    .line 318
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 320
    iget-object p1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public epsNotifyDataChange(Ljava/lang/String;)V
    .locals 2

    const-string v0, "  @@@@@@@@@@   called epsNotifyDataChange : %s"

    .line 302
    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 304
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    .line 305
    iput v1, v0, Landroid/os/Message;->what:I

    .line 306
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 308
    iget-object p1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public epsNotifyError(IIZ)V
    .locals 2

    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "called epsNotifyError "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 327
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x2

    .line 328
    iput v1, v0, Landroid/os/Message;->what:I

    .line 329
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 330
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 331
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 333
    iget-object p1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public epsNotifyPage(I)V
    .locals 2

    .line 338
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "called epsNotifyPage :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 339
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 340
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x1e

    .line 341
    iput v1, v0, Landroid/os/Message;->what:I

    .line 342
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 344
    iget-object p1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public native epsWrapperCancelFindPrinter()I
.end method

.method public native epsWrapperDoMainteCmd(I)I
.end method

.method public native epsWrapperGetAutoPowerOnEnable(Ljava/lang/String;[I)I
.end method

.method public native epsWrapperGetBatteryInfo()[I
.end method

.method public native epsWrapperGetEmaStatus([I)I
.end method

.method public native epsWrapperGetId()Ljava/lang/String;
.end method

.method public native epsWrapperGetInkInfo([I)I
.end method

.method public native epsWrapperGetInkInfo2(Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;)I
.end method

.method public native epsWrapperGetIp()Ljava/lang/String;
.end method

.method public native epsWrapperGetMaintenanceBoxInformation()[I
.end method

.method public native epsWrapperGetMediaInfo(II)[I
.end method

.method public native epsWrapperGetPaperInfo()[I
.end method

.method public native epsWrapperGetStatus([I)I
.end method

.method public native epsWrapperGetSupplyInfo(Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;)I
.end method

.method public native epsWrapperGetSupportedMedia(Ljava/lang/String;)I
.end method

.method public epsWrapperInitDriver(Landroid/content/Context;I)I
    .locals 2

    .line 386
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->iPrintPackageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :cond_0
    const/16 v0, 0xa

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperInitDriver(Landroid/content/Context;IB)I

    move-result p1

    return p1
.end method

.method public native epsWrapperInitDriver(Landroid/content/Context;IB)I
.end method

.method public epsWrapperPrintErrorNotifyCB([I)I
    .locals 5

    .line 525
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "epsWrapperPrintErrorNotifyCB() call"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 528
    aget v0, p1, v0

    const/4 v1, 0x1

    .line 529
    aget p1, p1, v1

    .line 530
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "printer error"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ";;;choice type: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return v1
.end method

.method public epsWrapperProbePrinter2(ILjava/lang/String;Ljava/lang/String;I)I
    .locals 1

    .line 422
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    const/4 v0, 0x1

    if-eq p4, v0, :cond_1

    const/4 v0, 0x3

    if-eq p4, v0, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_2

    .line 433
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p4

    if-lez p4, :cond_2

    .line 435
    iput-object p3, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerIp:Ljava/lang/String;

    .line 436
    iput-object p2, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerId:Ljava/lang/String;

    const/4 p2, 0x2

    .line 438
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperProbePrinter2(II[C)I

    move-result p1

    if-nez p1, :cond_3

    .line 441
    iget-boolean p2, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->bInvalidId:Z

    if-eqz p2, :cond_3

    const p1, -0x7a121

    goto :goto_1

    :cond_1
    if-eqz p2, :cond_2

    .line 427
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p3

    if-lez p3, :cond_2

    .line 428
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    invoke-direct {p0, p1, v0, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperProbePrinter2(II[C)I

    move-result p1

    goto :goto_1

    :cond_2
    :goto_0
    const/16 p1, -0x514

    :cond_3
    :goto_1
    return p1
.end method

.method public native epsWrapperSetAutoPowerOnEnable(Ljava/lang/String;Z)I
.end method

.method public native epsWrapperSetEmaStatus(I)I
.end method

.method public findPrinterCallback(Ljava/lang/String;)V
    .locals 2

    const-string v0, "called findPrinterCallback : %s"

    .line 291
    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 293
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    .line 294
    iput v1, v0, Landroid/os/Message;->what:I

    .line 295
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 297
    iget-object p1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public getBatteryInfo(Lcom/epson/mobilephone/common/maintain2/BatteryInfo;)I
    .locals 3

    .line 572
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetBatteryInfo()[I

    move-result-object v0

    const/4 v1, 0x0

    .line 574
    aget v2, v0, v1

    if-nez v2, :cond_0

    const/4 v2, 0x1

    .line 575
    aget v2, v0, v2

    iput v2, p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->powerSourceType:I

    const/4 v2, 0x2

    .line 576
    aget v2, v0, v2

    iput v2, p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->batteryChargeState:I

    const/4 v2, 0x3

    .line 577
    aget v2, v0, v2

    iput v2, p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfo;->batteryRemain:I

    .line 580
    :cond_0
    aget p1, v0, v1

    return p1
.end method

.method public getBatteryInfoEx(Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;)I
    .locals 3

    .line 584
    new-instance v0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;-><init>()V

    .line 586
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetSupplyInfo(Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;)I

    move-result v1

    .line 588
    iget-object v2, v0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->powerSourceEx:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;

    iget v2, v2, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;->powerSource:I

    iput v2, p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->powerSourceType:I

    .line 589
    iget-object v2, v0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->powerSourceEx:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;

    iget v2, v2, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;->number:I

    iput v2, p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->number:I

    .line 590
    iget-object v2, v0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->powerSourceEx:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;

    iget-object v2, v2, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;->batteryType:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iput-object v2, p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryType:[I

    .line 591
    iget-object v2, v0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->powerSourceEx:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;

    iget-object v2, v2, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;->batteryState:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iput-object v2, p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryState:[I

    .line 592
    iget-object v0, v0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->powerSourceEx:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;

    iget-object v0, v0, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;->remaining:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p1, Lcom/epson/mobilephone/common/maintain2/BatteryInfoEx;->batteryRemain:[I

    return v1
.end method

.method public getEngineId(Landroid/content/Context;)[B
    .locals 5

    .line 636
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 639
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x4

    if-ge v3, v4, :cond_0

    .line 641
    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 649
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    .line 651
    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x6

    if-eqz p1, :cond_1

    .line 653
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getEngineId macAdress = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const-string v3, ":"

    .line 654
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object p1

    .line 656
    array-length v3, p1

    if-ne v3, v1, :cond_2

    :goto_1
    if-ge v2, v1, :cond_2

    .line 658
    aget-object v3, p1, v2

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 662
    :cond_1
    new-array p1, v1, [B

    fill-array-data p1, :array_0

    .line 663
    invoke-virtual {v0, p1, v2, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 666
    :cond_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public getFirmwareInfo(Ljava/util/EnumMap;)I
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap<",
            "Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x1

    .line 553
    new-array v0, v0, [B

    const/4 v1, 0x6

    new-array v8, v1, [B

    const/4 v1, 0x7

    new-array v9, v1, [B

    const/4 v1, 0x5

    new-array v10, v1, [B

    const/4 v1, 0x3

    new-array v11, v1, [B

    new-array v12, v1, [B

    move-object v1, p0

    move-object v2, v0

    move-object v3, v8

    move-object v4, v9

    move-object v5, v10

    move-object v6, v11

    move-object v7, v12

    .line 554
    invoke-direct/range {v1 .. v7}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetFirmwareInfo([B[B[B[B[B[B)I

    move-result v1

    .line 555
    invoke-virtual {p1}, Ljava/util/EnumMap;->clear()V

    if-nez v1, :cond_0

    .line 558
    :try_start_0
    sget-object v2, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->NicFlg:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-byte v0, v0, v4

    add-int/lit8 v0, v0, 0x30

    int-to-char v0, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->NetVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-direct {p0, v8}, Lcom/epson/mobilephone/common/escpr/EscprLib;->cStrToString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MainVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-direct {p0, v9}, Lcom/epson/mobilephone/common/escpr/EscprLib;->cStrToString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->FY:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-direct {p0, v10}, Lcom/epson/mobilephone/common/escpr/EscprLib;->cStrToString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->MarketID:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-direct {p0, v11}, Lcom/epson/mobilephone/common/escpr/EscprLib;->cStrToString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    sget-object v0, Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;->UrlVer:Lcom/epson/mobilephone/common/escpr/EscprLib$PrinterFirmInfo;

    invoke-direct {p0, v12}, Lcom/epson/mobilephone/common/escpr/EscprLib;->cStrToString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 566
    :catch_0
    invoke-virtual {p1}, Ljava/util/EnumMap;->clear()V

    :cond_0
    :goto_0
    return v1
.end method

.method public native get_color(II)[I
.end method

.method public native get_duplex(II)I
.end method

.method public native get_lang()I
.end method

.method public native get_layout(II)[I
.end method

.method public native get_paper_size()[I
.end method

.method public native get_paper_source(II)[I
.end method

.method public native get_paper_type(I)[I
.end method

.method public native get_printable_area()[I
.end method

.method public native get_printable_area_info_all(Lcom/epson/mobilephone/common/escpr/EPS_PRINT_AREA_INFO;)I
.end method

.method public native get_printer_status()[I
.end method

.method public native get_quality(II)[I
.end method

.method public native get_start_job_resolution2()I
.end method

.method public native get_supported_media(Ljava/lang/String;)I
.end method

.method public native get_supported_media2()I
.end method

.method public init_driver(Landroid/content/Context;Ljava/lang/String;)I
    .locals 2

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init_driver :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->is64bit()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Native is operating at 64 bit !!"

    goto :goto_0

    :cond_0
    const-string v0, "Native is operating at 32 bit"

    :goto_0
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->iPrintPackageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    goto :goto_1

    :cond_1
    const/16 v0, 0xa

    :goto_1
    invoke-virtual {p0, p1, p2, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->init_driver(Landroid/content/Context;Ljava/lang/String;B)I

    move-result p1

    return p1
.end method

.method public native init_driver(Landroid/content/Context;Ljava/lang/String;B)I
.end method

.method public native init_image(Ljava/lang/String;)I
.end method

.method public native is64bit()Z
.end method

.method public native is_printer_set()Z
.end method

.method public native mibCheckMaxofSimpleAP(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibEnableSimpleAP(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibEnableWiFi(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibGetESSIDList(Lcom/epson/mobilephone/common/escpr/MIBCommand;B)I
.end method

.method public native mibGetMacadress(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibGetSerial(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibGetSimpleAPSSID(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibGetStatusInfra(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibGetStatusSimpleAP(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibIsAdminLocked(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibParseResponseAsInt(Lcom/epson/mobilephone/common/escpr/MIBCommand;[I)I
.end method

.method public native mibParseResponseAsMacaddress(Lcom/epson/mobilephone/common/escpr/MIBCommand;)Ljava/lang/String;
.end method

.method public native mibParseResponseAsStr(Lcom/epson/mobilephone/common/escpr/MIBCommand;)Ljava/lang/String;
.end method

.method public native mibParseSecResponse(Lcom/epson/mobilephone/common/escpr/MIBCommand;[B)I
.end method

.method public native mibRebootNWwithBLE(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibRebootNWwithSNMP(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I
.end method

.method public native mibSecLogin(Lcom/epson/mobilephone/common/escpr/MIBCommand;[B)I
.end method

.method public native mibSecLogout(Lcom/epson/mobilephone/common/escpr/MIBCommand;[B)I
.end method

.method public native mibSecSetPassword(Lcom/epson/mobilephone/common/escpr/MIBCommand;[BLjava/lang/String;)I
.end method

.method public native mibSendRecieveCommand(Lcom/epson/mobilephone/common/escpr/MIBCommand;Lcom/epson/mobilephone/common/escpr/MIBCommand;[B)I
.end method

.method public native mibSetSSID(Lcom/epson/mobilephone/common/escpr/MIBCommand;Ljava/lang/String;)I
.end method

.method public native mibSetSecurityType(Lcom/epson/mobilephone/common/escpr/MIBCommand;I)I
.end method

.method public native pageS_needRotate2(I)Z
.end method

.method public native print_jpeg(Ljava/lang/String;)I
.end method

.method public native print_page()I
.end method

.method public native release_driver()I
.end method

.method public native release_image()I
.end method

.method public resetIPAdressCheck()V
    .locals 1

    const/4 v0, 0x0

    .line 454
    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerId:Ljava/lang/String;

    .line 455
    iput-object v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->printerIp:Ljava/lang/String;

    const/4 v0, 0x0

    .line 456
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->bInvalidId:Z

    return-void
.end method

.method public native search_device(I)I
.end method

.method public native search_printer(Ljava/lang/String;)I
.end method

.method public native search_printer2(Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public setHanlder(Landroid/os/Handler;)V
    .locals 0

    .line 287
    iput-object p1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public setSearchStt(Z)V
    .locals 0

    .line 462
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/escpr/EscprLib;->mIsSearch:Z

    return-void
.end method

.method public native set_printer(I)I
.end method

.method public start_job(IIIIIIIIIIIIIIIIIIZ)I
    .locals 21

    if-eqz p19, :cond_0

    const/4 v0, 0x2

    const/16 v20, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/16 v20, 0x0

    :goto_0
    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    move/from16 v15, p14

    move/from16 v16, p15

    move/from16 v17, p16

    move/from16 v18, p17

    move/from16 v19, p18

    .line 146
    invoke-direct/range {v1 .. v20}, Lcom/epson/mobilephone/common/escpr/EscprLib;->start_job(IIIIIIIIIIIIIIIIIII)I

    move-result v0

    return v0
.end method

.method public start_job2(IIIIIIIIIIIZ)I
    .locals 14

    if-eqz p12, :cond_0

    const/4 v0, 0x2

    const/4 v13, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v13, 0x0

    :goto_0
    move-object v1, p0

    move v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    .line 177
    invoke-direct/range {v1 .. v13}, Lcom/epson/mobilephone/common/escpr/EscprLib;->start_job2(IIIIIIIIIIII)I

    move-result v0

    return v0
.end method

.method public native start_job_roll(IIIIIIIIIIIIIIII)I
.end method

.method public native start_page()I
.end method

.class public Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;
.super Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;
.source "MediaInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/escpr/MediaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PrintDate"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 726
    invoke-direct {p0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;-><init>()V

    .line 727
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/epson/mobilephone/common/escpr/R$string;->Off:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 728
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_PD_DATETYPE1:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 729
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_PD_DATETYPE2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 730
    iget-object v0, p0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;->sCodeTable:Ljava/util/Hashtable;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/epson/mobilephone/common/escpr/R$string;->EPS_PD_DATETYPE3:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

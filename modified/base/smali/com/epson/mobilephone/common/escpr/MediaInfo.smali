.class public interface abstract Lcom/epson/mobilephone/common/escpr/MediaInfo;
.super Ljava/lang/Object;
.source "MediaInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/escpr/MediaInfo$SharpnessState;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$ApfState;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$PhotoeEhance;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$Layout;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize_constants;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo_size;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$ColorTable;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$StatusTable;,
        Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;
    }
.end annotation


# static fields
.field public static final EPS_PRNST_BUSY:I = 0x2

.field public static final EPS_PRNST_CANCELLING:I = 0x3

.field public static final EPS_PRNST_ERROR:I = 0x4

.field public static final EPS_PRNST_IDLE:I = 0x0

.field public static final EPS_PRNST_PRINTING:I = 0x1

.field public static final ERROR_GET_LAYOUT:I = -0x1

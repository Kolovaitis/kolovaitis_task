.class public Lcom/epson/mobilephone/common/ReviewInvitationViewModel;
.super Landroid/arch/lifecycle/AndroidViewModel;
.source "ReviewInvitationViewModel.java"


# instance fields
.field private final mShowStoreInvitation:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mStoreReview:Lcom/epson/mobilephone/common/StoreReview;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 16
    invoke-direct {p0, p1}, Landroid/arch/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 12
    new-instance p1, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {p1}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object p1, p0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->mShowStoreInvitation:Landroid/arch/lifecycle/MutableLiveData;

    .line 13
    new-instance p1, Lcom/epson/mobilephone/common/StoreReview;

    invoke-direct {p1}, Lcom/epson/mobilephone/common/StoreReview;-><init>()V

    iput-object p1, p0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->mStoreReview:Lcom/epson/mobilephone/common/StoreReview;

    .line 17
    iget-object p1, p0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->mShowStoreInvitation:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method checkLanguage()Z
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 52
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->getLanguageString()Ljava/lang/String;

    move-result-object v0

    .line 55
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    .line 56
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method getLanguageString()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 61
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowInvitationLiveData()Landroid/arch/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->mShowStoreInvitation:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method public onPrintEnd(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->checkLanguage()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 39
    iget-object p1, p0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->mStoreReview:Lcom/epson/mobilephone/common/StoreReview;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/StoreReview;->checkPrintSuccessCount(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 40
    iget-object p1, p0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->mShowStoreInvitation:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setStartStoreEnd()V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->mShowStoreInvitation:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method setStoreReviewObject(Lcom/epson/mobilephone/common/StoreReview;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 26
    iput-object p1, p0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->mStoreReview:Lcom/epson/mobilephone/common/StoreReview;

    return-void
.end method

.class public Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "NoticeGuideActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyFragmentStatePagerAdapter"
.end annotation


# instance fields
.field private HTML_PAGE:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private PAGE_LENGTH:I


# direct methods
.method private constructor <init>(Landroid/support/v4/app/FragmentManager;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentManager;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 221
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 217
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;->HTML_PAGE:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 218
    iput p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;->PAGE_LENGTH:I

    .line 222
    iput-object p2, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;->HTML_PAGE:Ljava/util/ArrayList;

    .line 223
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p1

    iput p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;->PAGE_LENGTH:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/app/FragmentManager;Ljava/util/ArrayList;Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$1;)V
    .locals 0

    .line 215
    invoke-direct {p0, p1, p2}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 233
    iget v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;->PAGE_LENGTH:I

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .line 228
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;->HTML_PAGE:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    add-int/2addr p1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v3, v2

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->newInstance(Ljava/lang/String;)Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;

    move-result-object p1

    return-object p1
.end method

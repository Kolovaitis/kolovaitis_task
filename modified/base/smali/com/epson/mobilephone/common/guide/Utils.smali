.class public Lcom/epson/mobilephone/common/guide/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static LAYOUT_MEARGIN:I = 0x20

.field public static WEBVIEW_300DP_SIZE:I = 0x12c

.field public static WEBVIEW_500DP_SIZE:I = 0x1f4


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dptopx(ILandroid/content/Context;)F
    .locals 1

    int-to-float p0, p0

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0, p0, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p0

    return p0
.end method

.method public static getDisplaySize(Landroid/app/Activity;)Landroid/graphics/Point;
    .locals 1

    .line 40
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p0

    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p0

    .line 41
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 42
    invoke-virtual {p0, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    return-object v0
.end method

.method public static getWebViewDP(Landroid/app/Activity;Landroid/content/Context;)I
    .locals 3

    .line 20
    invoke-static {p0}, Lcom/epson/mobilephone/common/guide/Utils;->getDisplaySize(Landroid/app/Activity;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/guide/Utils;->pxtodp(ILandroid/content/Context;)F

    move-result v0

    .line 21
    invoke-static {p0}, Lcom/epson/mobilephone/common/guide/Utils;->getDisplaySize(Landroid/app/Activity;)Landroid/graphics/Point;

    move-result-object p0

    iget p0, p0, Landroid/graphics/Point;->y:I

    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/guide/Utils;->pxtodp(ILandroid/content/Context;)F

    move-result p0

    const/4 p1, 0x0

    cmpl-float v1, p0, p1

    if-eqz v1, :cond_4

    cmpl-float p1, v0, p1

    if-nez p1, :cond_0

    goto :goto_2

    :cond_0
    const-wide v1, 0x3fe6666666666666L    # 0.7

    cmpg-float p1, p0, v0

    if-gez p1, :cond_2

    float-to-double p0, p0

    mul-double p0, p0, v1

    double-to-int p0, p0

    .line 28
    sget p1, Lcom/epson/mobilephone/common/guide/Utils;->WEBVIEW_500DP_SIZE:I

    sget v0, Lcom/epson/mobilephone/common/guide/Utils;->LAYOUT_MEARGIN:I

    add-int/2addr v0, p1

    if-ge p0, v0, :cond_1

    goto :goto_0

    :cond_1
    move p0, p1

    :goto_0
    return p0

    :cond_2
    float-to-double p0, v0

    mul-double p0, p0, v1

    double-to-int p0, p0

    .line 31
    sget p1, Lcom/epson/mobilephone/common/guide/Utils;->WEBVIEW_500DP_SIZE:I

    sget v0, Lcom/epson/mobilephone/common/guide/Utils;->LAYOUT_MEARGIN:I

    add-int/2addr v0, p1

    if-ge p0, v0, :cond_3

    goto :goto_1

    :cond_3
    move p0, p1

    :goto_1
    return p0

    .line 24
    :cond_4
    :goto_2
    sget p0, Lcom/epson/mobilephone/common/guide/Utils;->WEBVIEW_300DP_SIZE:I

    return p0
.end method

.method public static pxtodp(ILandroid/content/Context;)F
    .locals 0

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    int-to-float p0, p0

    .line 51
    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr p0, p1

    return p0
.end method

.class public Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "NoticeGuideActivity.java"

# interfaces
.implements Lcom/epson/mobilephone/common/guide/GuideWebviewFragment$MyClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;,
        Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;
    }
.end annotation


# static fields
.field public static DIALOG_MODE_OFF:I = 0x4e21

.field public static DIALOG_MODE_ON:I = 0x4e20

.field public static DIALOG_MODE_ONLY_BUTTON:I = 0x4e22

.field public static KEY_BOOT_GROUP:Ljava/lang/String; = "BOOT_GROUP"

.field public static KEY_BOOT_MODE:Ljava/lang/String; = "BOOT_MODE"

.field public static KEY_DIALOG_BUTTON_NO:Ljava/lang/String; = "DIALOG_NO"

.field public static KEY_DIALOG_BUTTON_YES:Ljava/lang/String; = "DIALOG_YES"

.field public static KEY_DIALOG_MODE:Ljava/lang/String; = "DIALOG_MODE"

.field public static KEY_DIALOG_TITLE:Ljava/lang/String; = "DIALOG_TITLE"

.field public static KEY_GUIDE_VER:Ljava/lang/String; = "GUIDE_VER"

.field public static KEY_HTML_PATH:Ljava/lang/String; = "HTML_PATH"

.field public static MODE_AUTO:I = 0x2711

.field public static MODE_MANUAL:I = 0x2710


# instance fields
.field public SP_KEY_GUIDE_VER:Ljava/lang/String;

.field private boot_group:Ljava/lang/String;

.field private boot_mode:I

.field private dialog_mode:I

.field private htmlpath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAdapter:Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;

.field private mCancel:Landroid/widget/Button;

.field private mDialogButtonNo:Ljava/lang/String;

.field private mDialogButtonYes:Ljava/lang/String;

.field private mDialogText:Landroid/widget/TextView;

.field private mDialogTextMessage:Ljava/lang/String;

.field private mGrayLine_Hor:Landroid/view/View;

.field private mGuideDialogView:Landroid/widget/LinearLayout;

.field private mOK:Landroid/widget/Button;

.field private mPageDisplayView:Landroid/widget/LinearLayout;

.field private mViewPager:Lcom/epson/mobilephone/common/guide/MyViewPager;

.field private version:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 29
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->htmlpath:Ljava/util/ArrayList;

    const-string v0, ""

    .line 41
    iput-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogTextMessage:Ljava/lang/String;

    const-string v0, ""

    .line 42
    iput-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogButtonYes:Ljava/lang/String;

    const-string v0, ""

    .line 43
    iput-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogButtonNo:Ljava/lang/String;

    const-string v0, "GuideVersion"

    .line 61
    iput-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->SP_KEY_GUIDE_VER:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .line 179
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->onClickClose()V

    return-void
.end method

.method public onClickClose()V
    .locals 3

    .line 186
    iget v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->boot_mode:I

    sget v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->MODE_AUTO:I

    if-ne v0, v1, :cond_0

    .line 187
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 188
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->SP_KEY_GUIDE_VER:Ljava/lang/String;

    iget v2, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->version:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .line 195
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 202
    :pswitch_0
    iget v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->dialog_mode:I

    sget v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->DIALOG_MODE_ON:I

    if-ne v0, v1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mGuideDialogView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 197
    :pswitch_1
    iget v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->dialog_mode:I

    sget v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->DIALOG_MODE_ON:I

    if-ne v0, v1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mGuideDialogView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 209
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 65
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    sget p1, Lcom/epson/guidelib/R$layout;->activity_notice_guide:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->setContentView(I)V

    .line 68
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 70
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_HTML_PATH:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->htmlpath:Ljava/util/ArrayList;

    .line 71
    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->htmlpath:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->finish()V

    .line 74
    :cond_0
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_BOOT_MODE:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->boot_mode:I

    .line 75
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_BOOT_GROUP:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->boot_group:Ljava/lang/String;

    .line 76
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_DIALOG_MODE:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->dialog_mode:I

    .line 77
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_GUIDE_VER:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->version:I

    .line 78
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_DIALOG_TITLE:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogTextMessage:Ljava/lang/String;

    .line 79
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_DIALOG_BUTTON_YES:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogButtonYes:Ljava/lang/String;

    .line 80
    sget-object v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_DIALOG_BUTTON_NO:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogButtonNo:Ljava/lang/String;

    .line 82
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->boot_group:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 83
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->SP_KEY_GUIDE_VER:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->boot_group:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->SP_KEY_GUIDE_VER:Ljava/lang/String;

    .line 88
    :cond_1
    iget p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->boot_mode:I

    sget v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->MODE_AUTO:I

    if-ne p1, v1, :cond_2

    .line 89
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 90
    iget v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->version:I

    iget-object v2, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->SP_KEY_GUIDE_VER:Ljava/lang/String;

    invoke-interface {p1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    if-gt v1, p1, :cond_2

    .line 91
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->finish()V

    .line 95
    :cond_2
    new-instance p1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;

    const/4 v1, 0x0

    invoke-direct {p1, p0, v1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;-><init>(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$1;)V

    .line 96
    new-instance v2, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    iget-object v4, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->htmlpath:Ljava/util/ArrayList;

    invoke-direct {v2, v3, v4, v1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Ljava/util/ArrayList;Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$1;)V

    iput-object v2, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mAdapter:Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;

    .line 99
    sget v1, Lcom/epson/guidelib/R$id;->guideViewPager:I

    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/epson/mobilephone/common/guide/MyViewPager;

    iput-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mViewPager:Lcom/epson/mobilephone/common/guide/MyViewPager;

    .line 100
    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mViewPager:Lcom/epson/mobilephone/common/guide/MyViewPager;

    iget-object v2, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mAdapter:Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/guide/MyViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 101
    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mViewPager:Lcom/epson/mobilephone/common/guide/MyViewPager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/guide/MyViewPager;->setOffscreenPageLimit(I)V

    .line 102
    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mViewPager:Lcom/epson/mobilephone/common/guide/MyViewPager;

    new-instance v2, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$1;

    invoke-direct {v2, p0, p1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$1;-><init>(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;)V

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/guide/MyViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 114
    sget v1, Lcom/epson/guidelib/R$id;->pageDisplayViewGroup:I

    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mPageDisplayView:Landroid/widget/LinearLayout;

    .line 115
    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mPageDisplayView:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mAdapter:Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyFragmentStatePagerAdapter;->getCount()I

    move-result v2

    invoke-static {p1, v1, v2}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;->access$200(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;Landroid/view/ViewGroup;I)V

    .line 116
    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mViewPager:Lcom/epson/mobilephone/common/guide/MyViewPager;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/guide/MyViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;->changePage(I)V

    .line 119
    sget p1, Lcom/epson/guidelib/R$id;->guide_dialog_layout:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mGuideDialogView:Landroid/widget/LinearLayout;

    .line 120
    sget p1, Lcom/epson/guidelib/R$id;->guide_dialog_horizon_gray_line:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mGrayLine_Hor:Landroid/view/View;

    .line 121
    sget p1, Lcom/epson/guidelib/R$id;->guide_dialog_message:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogText:Landroid/widget/TextView;

    .line 123
    iget p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->dialog_mode:I

    sget v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->DIALOG_MODE_ON:I

    if-eq p1, v1, :cond_3

    sget v1, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->DIALOG_MODE_ONLY_BUTTON:I

    if-ne p1, v1, :cond_7

    .line 126
    :cond_3
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mGuideDialogView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 129
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mGuideDialogView:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 130
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/guide/Utils;->getWebViewDP(Landroid/app/Activity;Landroid/content/Context;)I

    move-result v0

    .line 131
    sget v1, Lcom/epson/mobilephone/common/guide/Utils;->LAYOUT_MEARGIN:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/guide/Utils;->dptopx(ILandroid/content/Context;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 132
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mGuideDialogView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 135
    sget p1, Lcom/epson/guidelib/R$id;->ok_button:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mOK:Landroid/widget/Button;

    .line 136
    sget p1, Lcom/epson/guidelib/R$id;->cancel_btn:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mCancel:Landroid/widget/Button;

    .line 138
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogButtonYes:Ljava/lang/String;

    if-eqz p1, :cond_4

    .line 139
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mOK:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 141
    :cond_4
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogButtonNo:Ljava/lang/String;

    if-eqz p1, :cond_5

    .line 142
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mCancel:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 145
    :cond_5
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mOK:Landroid/widget/Button;

    new-instance v0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$2;-><init>(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mCancel:Landroid/widget/Button;

    new-instance v0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$3;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$3;-><init>(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->dialog_mode:I

    sget v0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->DIALOG_MODE_ON:I

    if-ne p1, v0, :cond_6

    .line 161
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogTextMessage:Ljava/lang/String;

    if-eqz p1, :cond_6

    .line 162
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    :cond_6
    iget p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->dialog_mode:I

    sget v0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->DIALOG_MODE_ONLY_BUTTON:I

    if-ne p1, v0, :cond_7

    .line 168
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mGrayLine_Hor:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mDialogText:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mOK:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/epson/guidelib/R$drawable;->rectangle_left_button:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 171
    iget-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->mCancel:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/epson/guidelib/R$drawable;->rectangle_right_button:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_7
    return-void
.end method

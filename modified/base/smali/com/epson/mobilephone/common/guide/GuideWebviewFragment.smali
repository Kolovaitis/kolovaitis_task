.class public Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;
.super Landroid/support/v4/app/Fragment;
.source "GuideWebviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/guide/GuideWebviewFragment$MyClickListener;
    }
.end annotation


# static fields
.field private static final ARG_PARAM1:Ljava/lang/String; = "html_file_path"


# instance fields
.field mClickListerner:Lcom/epson/mobilephone/common/guide/GuideWebviewFragment$MyClickListener;

.field private mHtmlPath:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;

.field private mWebViewFrame:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;
    .locals 3

    .line 30
    new-instance v0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;-><init>()V

    .line 31
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "html_file_path"

    .line 32
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .line 110
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 112
    :try_start_0
    check-cast p1, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment$MyClickListener;

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mClickListerner:Lcom/epson/mobilephone/common/guide/GuideWebviewFragment$MyClickListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 114
    :catch_0
    new-instance p1, Ljava/lang/ClassCastException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "must implement OnArticleSelectedListener."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 39
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "html_file_path"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mHtmlPath:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .line 47
    sget p3, Lcom/epson/guidelib/R$layout;->fragment_guide_webview:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 48
    sget p2, Lcom/epson/guidelib/R$id;->guideWebview:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/webkit/WebView;

    iput-object p2, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    .line 49
    sget p2, Lcom/epson/guidelib/R$id;->guideWebViewFrame:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/FrameLayout;

    iput-object p2, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebViewFrame:Landroid/widget/FrameLayout;

    .line 50
    iget-object p2, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p2}, Landroid/webkit/WebView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 51
    iget-object p3, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebViewFrame:Landroid/widget/FrameLayout;

    invoke-virtual {p3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p3

    check-cast p3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 53
    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 54
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 55
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 56
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 57
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mHtmlPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/guide/Utils;->getWebViewDP(Landroid/app/Activity;Landroid/content/Context;)I

    move-result v0

    .line 62
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/guide/Utils;->dptopx(ILandroid/content/Context;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 63
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/guide/Utils;->dptopx(ILandroid/content/Context;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p2, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 64
    sget v1, Lcom/epson/mobilephone/common/guide/Utils;->LAYOUT_MEARGIN:I

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/guide/Utils;->dptopx(ILandroid/content/Context;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p3, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 65
    sget v1, Lcom/epson/mobilephone/common/guide/Utils;->LAYOUT_MEARGIN:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/guide/Utils;->dptopx(ILandroid/content/Context;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p3, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 67
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    iget-object p2, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebViewFrame:Landroid/widget/FrameLayout;

    invoke-virtual {p2, p3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    sget p2, Lcom/epson/guidelib/R$id;->closeImage:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    new-instance p3, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment$1;

    invoke-direct {p3, p0}, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment$1;-><init>(Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    :cond_0
    const/4 v0, 0x0

    .line 104
    iput-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    .line 105
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDetach()V
    .locals 1

    .line 120
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    const/4 v0, 0x0

    .line 121
    iput-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mClickListerner:Lcom/epson/mobilephone/common/guide/GuideWebviewFragment$MyClickListener;

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 89
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 90
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/epson/mobilephone/common/guide/GuideWebviewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    .line 96
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    return-void
.end method

.class Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;
.super Ljava/lang/Object;
.source "NoticeGuideActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyPageAdapter"
.end annotation


# instance fields
.field private mPagePosition:[Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;


# direct methods
.method private constructor <init>(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;)V
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;->this$0:Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$1;)V
    .locals 0

    .line 237
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;-><init>(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;)V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;Landroid/view/ViewGroup;I)V
    .locals 0

    .line 237
    invoke-direct {p0, p1, p2}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;->setPageSize(Landroid/view/ViewGroup;I)V

    return-void
.end method

.method private setPageSize(Landroid/view/ViewGroup;I)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 245
    new-array v0, p2, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;->mPagePosition:[Landroid/widget/ImageView;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 247
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;->this$0:Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 248
    sget v2, Lcom/epson/guidelib/R$drawable;->cycle02:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    add-int/lit8 v2, v0, 0x1

    .line 249
    invoke-virtual {p1, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 250
    iget-object v3, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;->mPagePosition:[Landroid/widget/ImageView;

    aput-object v1, v3, v0

    move v0, v2

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method changePage(I)V
    .locals 3

    const/4 v0, 0x0

    .line 256
    :goto_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity$MyPageAdapter;->mPagePosition:[Landroid/widget/ImageView;

    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 257
    aget-object v2, v1, v0

    if-eqz v2, :cond_1

    if-ne p1, v0, :cond_0

    .line 259
    aget-object v1, v1, v0

    sget v2, Lcom/epson/guidelib/R$drawable;->cycle03:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 261
    :cond_0
    aget-object v1, v1, v0

    sget v2, Lcom/epson/guidelib/R$drawable;->cycle02:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.class public Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;
.super Ljava/lang/Object;
.source "EPS_SUPPLY_INFO.java"


# instance fields
.field public ink:Lcom/epson/mobilephone/common/maintain/EPS_INK_INFO;

.field public paperSource:Lcom/epson/mobilephone/common/maintain/EPS_PAPERSOURCE_INFO;

.field public powerSource:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO;

.field public powerSourceEx:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Lcom/epson/mobilephone/common/maintain/EPS_INK_INFO;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain/EPS_INK_INFO;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->ink:Lcom/epson/mobilephone/common/maintain/EPS_INK_INFO;

    .line 11
    new-instance v0, Lcom/epson/mobilephone/common/maintain/EPS_PAPERSOURCE_INFO;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain/EPS_PAPERSOURCE_INFO;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->paperSource:Lcom/epson/mobilephone/common/maintain/EPS_PAPERSOURCE_INFO;

    .line 12
    new-instance v0, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->powerSource:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO;

    .line 13
    new-instance v0, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->powerSourceEx:Lcom/epson/mobilephone/common/maintain/EPS_POWERSOURCE_INFO_EX;

    return-void
.end method

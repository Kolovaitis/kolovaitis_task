.class public Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;
.super Ljava/lang/Object;
.source "MaintainPrinterInfo.java"


# static fields
.field private static final EPS_STATUS_NUM:I = 0x3


# instance fields
.field private mId:Ljava/lang/String;

.field private mLocation:Ljava/lang/String;

.field private mPrinterName:Ljava/lang/String;

.field private mStatus:[I

.field private mSupplyInfo:Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 14
    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mPrinterName:Ljava/lang/String;

    .line 15
    new-instance v0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mSupplyInfo:Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;

    const/4 v0, 0x3

    .line 17
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mStatus:[I

    return-void
.end method


# virtual methods
.method public getMId()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getMLocation()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getMPrinterName()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mPrinterName:Ljava/lang/String;

    return-object v0
.end method

.method public getMStatus()[I
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mStatus:[I

    return-object v0
.end method

.method public getMSupplyInfo()Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mSupplyInfo:Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;

    return-object v0
.end method

.method public setMId(Ljava/lang/String;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mId:Ljava/lang/String;

    return-void
.end method

.method public setMInkInfor([I)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mSupplyInfo:Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;

    iget-object v0, v0, Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;->ink:Lcom/epson/mobilephone/common/maintain/EPS_INK_INFO;

    iput-object p1, v0, Lcom/epson/mobilephone/common/maintain/EPS_INK_INFO;->colors:[I

    return-void
.end method

.method public setMLocation(Ljava/lang/String;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mLocation:Ljava/lang/String;

    return-void
.end method

.method public setMPrinterName(Ljava/lang/String;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mPrinterName:Ljava/lang/String;

    return-void
.end method

.method public setMStatus([I)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->mStatus:[I

    return-void
.end method

.class public Lcom/epson/mobilephone/common/maintain/MaintainPrinter;
.super Ljava/lang/Object;
.source "MaintainPrinter.java"


# instance fields
.field protected mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

.field protected mPrinterInfor:Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;

.field protected mSearchPos:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getInstance()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    .line 19
    new-instance v0, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;

    const/4 v0, 0x0

    .line 20
    iput v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mSearchPos:I

    return-void
.end method


# virtual methods
.method public doCancelFindPrinter()I
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->cancel_search_printer()I

    move-result v0

    return v0
.end method

.method public doDoMainteCmd(I)I
    .locals 2

    const-string v0, "MaintainPrinter"

    const-string v1, "**********doDoMainteCmd************"

    .line 76
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperDoMainteCmd(I)I

    move-result p1

    return p1
.end method

.method public doFindPrinter(II)I
    .locals 0

    const-string p1, "MaintainPrinter"

    const-string p2, "**********Call do find printer************"

    .line 51
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object p1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->search_printer(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public doGetIp()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetIp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public doGetLang()I
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->get_lang()I

    move-result v0

    return v0
.end method

.method public doGetStatus()I
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->getMStatus()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetStatus([I)I

    move-result v0

    return v0
.end method

.method public doGetSupplyInfo()I
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->getMSupplyInfo()Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetSupplyInfo(Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;)I

    move-result v0

    return v0
.end method

.method public doGetSupplyInfoEx()I
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;->getMSupplyInfo()Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetSupplyInfo(Lcom/epson/mobilephone/common/maintain/EPS_SUPPLY_INFO;)I

    move-result v0

    return v0
.end method

.method public doInitDriver(Landroid/content/Context;I)I
    .locals 3

    const/4 p2, 0x0

    .line 41
    invoke-virtual {p1, p2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p2

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    const-string v0, "MaintainPrinter"

    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**********doInitDriver************:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0, p1, p2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->init_driver(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public doProbePrinter(IILjava/lang/String;)I
    .locals 0

    const-string p1, "MaintainPrinter"

    const-string p2, "doProbePrinter"

    .line 90
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object p1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {p1, p3}, Lcom/epson/mobilephone/common/escpr/EscprLib;->search_printer(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public doReleaseDriver()I
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->release_driver()I

    move-result v0

    return v0
.end method

.method public doSetPrinter()I
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget v1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mSearchPos:I

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->set_printer(I)I

    move-result v0

    return v0
.end method

.method public getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mEscprLib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    return-object v0
.end method

.method public getMPrinterInfor()Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mPrinterInfor:Lcom/epson/mobilephone/common/maintain/MaintainPrinterInfo;

    return-object v0
.end method

.method public getMSearchPos()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mSearchPos:I

    return v0
.end method

.method public setMSearchPos(I)V
    .locals 0

    .line 24
    iput p1, p0, Lcom/epson/mobilephone/common/maintain/MaintainPrinter;->mSearchPos:I

    return-void
.end method

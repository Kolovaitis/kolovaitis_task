.class public final enum Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/PrintingLib/define/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ColorName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_BLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_BLACK1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_BLACK2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_BLUE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_CLEAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_CLEAR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_COMPOSITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_CYAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_DARKYELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_DEEP_BLUE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_GRAY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_GREEN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTCYAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTGRAY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTLIGHTBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTMAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_LIGHTYELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_MAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_MATTEBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_ORANGE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_PHOTOBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_RED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_VIOLET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_VIVID_LIGHTMAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_VIVID_MAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_WHITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

.field public static final enum EPS_COLOR_YELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 74
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_BLACK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 75
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_CYAN"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CYAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 76
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_MAGENTA"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_MAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 77
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_YELLOW"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_YELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 78
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTCYAN"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTCYAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 79
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTMAGENTA"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTMAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 80
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTYELLOW"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTYELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 81
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_DARKYELLOW"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_DARKYELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 82
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTBLACK"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 83
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_RED"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_RED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 84
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_VIOLET"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIOLET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 85
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_MATTEBLACK"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_MATTEBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 86
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTLIGHTBLACK"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTLIGHTBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 87
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_PHOTOBLACK"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_PHOTOBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 88
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_CLEAR"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CLEAR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 89
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_GRAY"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_GRAY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 90
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_UNKNOWN"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 93
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_BLACK2"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 94
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_ORANGE"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_ORANGE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 95
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_GREEN"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_GREEN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 96
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_WHITE"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_WHITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 97
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_CLEAN"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CLEAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 100
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_COMPOSITE"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_COMPOSITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 103
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_BLACK1"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 106
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_BLUE"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLUE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 107
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_DEEP_BLUE"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_DEEP_BLUE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 108
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_VIVID_MAGENTA"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIVID_MAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 109
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_VIVID_LIGHTMAGENTA"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIVID_LIGHTMAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    .line 110
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const-string v1, "EPS_COLOR_LIGHTGRAY"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTGRAY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v0, 0x1d

    .line 73
    new-array v0, v0, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CYAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_MAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_YELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTCYAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTMAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTYELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_DARKYELLOW:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_RED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIOLET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_MATTEBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v13

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTLIGHTBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    aput-object v1, v0, v14

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_PHOTOBLACK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CLEAR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_GRAY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_ORANGE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_GREEN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_WHITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_CLEAN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_COMPOSITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLACK1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_BLUE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_DEEP_BLUE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIVID_MAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_VIVID_LIGHTMAGENTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->EPS_COLOR_LIGHTGRAY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->$VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;
    .locals 1

    .line 73
    const-class v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;
    .locals 1

    .line 73
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->$VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;

    return-object v0
.end method

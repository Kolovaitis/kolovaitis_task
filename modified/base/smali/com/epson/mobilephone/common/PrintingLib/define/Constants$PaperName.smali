.class public final enum Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/PrintingLib/define/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PaperName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_10X12:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_10X15:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_11X14:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_12X12:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_12X18:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_16K:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_16X20:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_200X300:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_2L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_5X8:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_8K:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_8X10:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_8X10_5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_8_27X13:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_8_5X13:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_A2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_A3NOBI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_A5_24HOLE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_A6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ALBUM_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ALBUM_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_B3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_B4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_B6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_BUZCARD_55X91:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_BUZCARD_89X50:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_CARD_54X86:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_CHOKEI_3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_CHOKEI_4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_CHOKEI_40:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_DBLPOSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_10_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_10_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_B5_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_C4_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_C5_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_C6_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_C6_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_DL_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_ENV_DL_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_EXECUTIVE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_HALFCUT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_HALFLETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_HIVISION:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_INDIAN_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_KAKU_2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_KAKU_20:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_MEISHI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_MEXICO_OFICIO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_NEWENV_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_NEWEVN_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_OFICIO9:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_PALBUM_2L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_PALBUM_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_PALBUM_A5_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_PALBUM_L_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_PANORAMIC:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_POSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_QUADRAPLEPOSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_SP1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_SP2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_SP3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_SP4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_SP5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_SQUARE_5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_SQUARE_8_27:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_SRA3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_TRIM_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_USB:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_USC:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_USER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_YOKEI_0:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_YOKEI_1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_YOKEI_2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_YOKEI_3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_YOKEI_4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

.field public static final enum EPS_MSID_YOKEI_6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;


# instance fields
.field code:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 115
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_A4"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 116
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_LETTER"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 117
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_LEGAL"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 118
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_A5"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5, v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 119
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_A6"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6, v6}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 120
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_B5"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7, v7}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 121
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_EXECUTIVE"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8, v8}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_EXECUTIVE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 122
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_HALFLETTER"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9, v9}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HALFLETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 123
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_PANORAMIC"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10, v10}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PANORAMIC:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 124
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_TRIM_4X6"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11, v11}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_TRIM_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 125
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_4X6"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12, v12}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 126
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_5X8"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13, v13}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_5X8:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 127
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_8X10"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14, v14}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8X10:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 128
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_10X15"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_10X15:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 129
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_200X300"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_200X300:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 130
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_L"

    const/16 v15, 0xf

    const/16 v14, 0xf

    invoke-direct {v0, v1, v15, v14}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 131
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_POSTCARD"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_POSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 132
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_DBLPOSTCARD"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_DBLPOSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 133
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_10_L"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_10_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 134
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_C6_L"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C6_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 135
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_DL_L"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_DL_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 136
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_NEWEVN_L"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_NEWEVN_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 137
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_CHOKEI_3"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CHOKEI_3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 138
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_CHOKEI_4"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CHOKEI_4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 139
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_YOKEI_1"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 140
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_YOKEI_2"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 141
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_YOKEI_3"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 142
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_YOKEI_4"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 143
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_2L"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_2L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 144
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_10_P"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_10_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 145
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_C6_P"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C6_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 146
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_DL_P"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_DL_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 147
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_NEWENV_P"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_NEWENV_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 148
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_MEISHI"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_MEISHI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 149
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_BUZCARD_89X50"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_BUZCARD_89X50:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 150
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_CARD_54X86"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CARD_54X86:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 151
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_BUZCARD_55X91"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_BUZCARD_55X91:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 152
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ALBUM_L"

    const/16 v14, 0x25

    const/16 v15, 0x25

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ALBUM_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 153
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ALBUM_A5"

    const/16 v14, 0x26

    const/16 v15, 0x26

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ALBUM_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 154
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_PALBUM_L_L"

    const/16 v14, 0x27

    const/16 v15, 0x27

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_L_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 155
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_PALBUM_2L"

    const/16 v14, 0x28

    const/16 v15, 0x28

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_2L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 156
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_PALBUM_A5_L"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_A5_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 157
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_PALBUM_A4"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 158
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_HIVISION"

    const/16 v14, 0x2b

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HIVISION:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 159
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_KAKU_2"

    const/16 v14, 0x2c

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_KAKU_2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 160
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_C4_P"

    const/16 v14, 0x2d

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C4_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 161
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_B6"

    const/16 v14, 0x2e

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 162
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_KAKU_20"

    const/16 v14, 0x2f

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_KAKU_20:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 163
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_A5_24HOLE"

    const/16 v14, 0x30

    const/16 v15, 0x30

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A5_24HOLE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 164
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_CHOKEI_40"

    const/16 v14, 0x31

    const/16 v15, 0x34

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CHOKEI_40:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 165
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_QUADRAPLEPOSTCARD"

    const/16 v14, 0x32

    const/16 v15, 0x35

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_QUADRAPLEPOSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 166
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_YOKEI_0"

    const/16 v14, 0x33

    const/16 v15, 0x36

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_0:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 167
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_C5_P"

    const/16 v14, 0x34

    const/16 v15, 0x38

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C5_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 168
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_YOKEI_6"

    const/16 v14, 0x35

    const/16 v15, 0x39

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 169
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_MEXICO_OFICIO"

    const/16 v14, 0x36

    const/16 v15, 0x3a

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_MEXICO_OFICIO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 170
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_OFICIO9"

    const/16 v14, 0x37

    const/16 v15, 0x3b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_OFICIO9:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 171
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_INDIAN_LEGAL"

    const/16 v14, 0x38

    const/16 v15, 0x3c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_INDIAN_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 172
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_A3NOBI"

    const/16 v14, 0x39

    const/16 v15, 0x3d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3NOBI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 173
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_A3"

    const/16 v14, 0x3a

    const/16 v15, 0x3e

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 174
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_B4"

    const/16 v14, 0x3b

    const/16 v15, 0x3f

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 175
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_USB"

    const/16 v14, 0x3c

    const/16 v15, 0x40

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USB:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 176
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_11X14"

    const/16 v14, 0x3d

    const/16 v15, 0x41

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_11X14:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 177
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_B3"

    const/16 v14, 0x3e

    const/16 v15, 0x42

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 178
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_A2"

    const/16 v14, 0x3f

    const/16 v15, 0x43

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 179
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_USC"

    const/16 v14, 0x40

    const/16 v15, 0x44

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USC:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 180
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_10X12"

    const/16 v14, 0x41

    const/16 v15, 0x45

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_10X12:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 181
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_12X12"

    const/16 v14, 0x42

    const/16 v15, 0x46

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_12X12:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 182
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_SP1"

    const/16 v14, 0x43

    const/16 v15, 0x47

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 183
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_SP2"

    const/16 v14, 0x44

    const/16 v15, 0x48

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 184
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_SP3"

    const/16 v14, 0x45

    const/16 v15, 0x49

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 185
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_SP4"

    const/16 v14, 0x46

    const/16 v15, 0x4a

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 186
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_SP5"

    const/16 v14, 0x47

    const/16 v15, 0x4b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 187
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_16K"

    const/16 v14, 0x48

    const/16 v15, 0x4c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_16K:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 188
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_8K"

    const/16 v14, 0x49

    const/16 v15, 0x4d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8K:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 189
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_SRA3"

    const/16 v14, 0x4a

    const/16 v15, 0x54

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SRA3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 190
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_12X18"

    const/16 v14, 0x4b

    const/16 v15, 0x55

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_12X18:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 191
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_8_5X13"

    const/16 v14, 0x4c

    const/16 v15, 0x56

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8_5X13:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 192
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_SQUARE_8_27"

    const/16 v14, 0x4d

    const/16 v15, 0x57

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SQUARE_8_27:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 193
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_SQUARE_5"

    const/16 v14, 0x4e

    const/16 v15, 0x58

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SQUARE_5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 194
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_USER"

    const/16 v14, 0x4f

    const/16 v15, 0x63

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 195
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_8X10_5"

    const/16 v14, 0x50

    const/16 v15, 0x68

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8X10_5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 196
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_8_27X13"

    const/16 v14, 0x51

    const/16 v15, 0x6a

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8_27X13:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 197
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_ENV_B5_P"

    const/16 v14, 0x52

    const/16 v15, 0x6f

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_B5_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 198
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_HALFCUT"

    const/16 v14, 0x53

    const/16 v15, 0x80

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HALFCUT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 199
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_16X20"

    const/16 v14, 0x54

    const/16 v15, 0x81

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_16X20:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 200
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const-string v1, "EPS_MSID_UNKNOWN"

    const/16 v14, 0x55

    const/16 v15, 0xff

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v0, 0x56

    .line 114
    new-array v0, v0, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_EXECUTIVE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HALFLETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PANORAMIC:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_TRIM_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_5X8:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    aput-object v1, v0, v13

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8X10:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_10X15:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_200X300:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_POSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_DBLPOSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_10_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C6_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_DL_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_NEWEVN_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CHOKEI_3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CHOKEI_4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_2L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_10_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C6_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_DL_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_NEWENV_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_MEISHI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_BUZCARD_89X50:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CARD_54X86:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_BUZCARD_55X91:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ALBUM_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ALBUM_A5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_L_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_2L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_A5_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_PALBUM_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HIVISION:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_KAKU_2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C4_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_KAKU_20:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A5_24HOLE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_CHOKEI_40:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_QUADRAPLEPOSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_0:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_C5_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_YOKEI_6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_MEXICO_OFICIO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_OFICIO9:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_INDIAN_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3NOBI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USB:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_11X14:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USC:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_10X12:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_12X12:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SP5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_16K:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8K:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SRA3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_12X18:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8_5X13:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SQUARE_8_27:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_SQUARE_5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_USER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8X10_5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_8_27X13:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_ENV_B5_P:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_HALFCUT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_16X20:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_UNKNOWN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->$VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 205
    iput p3, p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;
    .locals 1

    .line 114
    const-class v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;
    .locals 1

    .line 114
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->$VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .line 209
    iget v0, p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->code:I

    return v0
.end method

.class public final enum Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/PrintingLib/define/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_360INKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_3D:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_ARCHMATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_AUTO_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_BACKLIGHT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_BARYTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_BROCHURE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_BSMATTE_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_BS_HALFGLOSSY_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_BUSINESSCOAT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_BUSINESS_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_CDDVD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_CDDVDGLOSSY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_CDDVDHIGH:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_CLEANING:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_CLPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_COATED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_COLOR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_DSMATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_DURABRITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_ECOPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_ENVELOPE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_GLOSSYCAST:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_GLOSSYHAGAKI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_GLOSSYPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_GROSSY_ROLL_STICKER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_HAGAKIATENA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_HAGAKIINKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_HAGAKIRECL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_HIGH_QUALITY_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_IRON:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_LABEL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_LCPP:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_LETTERHEAD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_MATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_MATTEBOARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_MATTEMEISHI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_MATTE_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_MCGLOSSY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_MEDICINEBAG:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_MINIPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_OHP:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PGPHOTOEG:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTOADSHEET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTOALBUM:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTOFILM:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTOGLOSS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTOINKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTOINKJET2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTOSTAND:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PHOTOSTD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PLAIN1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PLAIN2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PLAIN_ROLL_STICKER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PLATINA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PLOOFING_WHITE_MAT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PLPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PREPRINTED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PROGLOSS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PROOFSEMI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_PSPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_RCB:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_RECYCLED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_SEMIPROOF:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_SEMI_THICK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_SFHAGAKI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_SPECIAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_SUPERFINE2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_THICKPAPER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_THICKPAPER1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_THICKPAPER2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_THICKPAPER3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_THICKPAPER4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_THICKPAPER5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_THINPAPER1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_TRANSPARENCY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_ULTRASMOOTH:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_UNSPECIFIED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_VELVETFINEART:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

.field public static final enum EPS_MTID_WATERCOLOR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;


# instance fields
.field code:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 218
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PLAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 219
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_360INKJET"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_360INKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 220
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_IRON"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_IRON:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 221
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTOINKJET"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5, v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOINKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 222
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTOADSHEET"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6, v6}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOADSHEET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 223
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_MATTE"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7, v7}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 224
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTO"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8, v8}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 225
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTOFILM"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9, v9}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOFILM:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 226
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_MINIPHOTO"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10, v10}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MINIPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 227
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_OHP"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11, v11}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_OHP:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 228
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_BACKLIGHT"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12, v12}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BACKLIGHT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 229
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PGPHOTO"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13, v13}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 230
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PSPHOTO"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14, v14}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PSPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 231
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PLPHOTO"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 232
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_MCGLOSSY"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MCGLOSSY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 233
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_ARCHMATTE"

    const/16 v15, 0xf

    const/16 v14, 0xf

    invoke-direct {v0, v1, v15, v14}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_ARCHMATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 234
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_WATERCOLOR"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_WATERCOLOR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 235
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PROGLOSS"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PROGLOSS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 236
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_MATTEBOARD"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTEBOARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 237
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTOGLOSS"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOGLOSS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 238
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_SEMIPROOF"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SEMIPROOF:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 239
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_SUPERFINE2"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SUPERFINE2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 240
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_DSMATTE"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_DSMATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 241
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_CLPHOTO"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CLPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 242
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_ECOPHOTO"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_ECOPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 243
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_VELVETFINEART"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_VELVETFINEART:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 244
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PROOFSEMI"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PROOFSEMI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 245
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_HAGAKIRECL"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HAGAKIRECL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 246
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_HAGAKIINKJET"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HAGAKIINKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 247
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTOINKJET2"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOINKJET2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 248
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_DURABRITE"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_DURABRITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 249
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_MATTEMEISHI"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTEMEISHI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 250
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_HAGAKIATENA"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HAGAKIATENA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 251
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTOALBUM"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOALBUM:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 252
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTOSTAND"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOSTAND:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 253
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_RCB"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_RCB:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 254
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PGPHOTOEG"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTOEG:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 255
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_ENVELOPE"

    const/16 v14, 0x25

    const/16 v15, 0x25

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_ENVELOPE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 256
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PLATINA"

    const/16 v14, 0x26

    const/16 v15, 0x26

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLATINA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 257
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_ULTRASMOOTH"

    const/16 v14, 0x27

    const/16 v15, 0x27

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_ULTRASMOOTH:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 258
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_SFHAGAKI"

    const/16 v14, 0x28

    const/16 v15, 0x28

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SFHAGAKI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 259
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PHOTOSTD"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOSTD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 260
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_GLOSSYHAGAKI"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_GLOSSYHAGAKI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 261
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_GLOSSYPHOTO"

    const/16 v14, 0x2b

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_GLOSSYPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 262
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_GLOSSYCAST"

    const/16 v14, 0x2c

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_GLOSSYCAST:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 263
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_BUSINESSCOAT"

    const/16 v14, 0x2d

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BUSINESSCOAT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 264
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_MEDICINEBAG"

    const/16 v14, 0x2e

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MEDICINEBAG:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 265
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_THICKPAPER"

    const/16 v14, 0x2f

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 266
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_BROCHURE"

    const/16 v14, 0x30

    const/16 v15, 0x30

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BROCHURE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 267
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_MATTE_DS"

    const/16 v14, 0x31

    const/16 v15, 0x31

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTE_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 268
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_BSMATTE_DS"

    const/16 v14, 0x32

    const/16 v15, 0x32

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BSMATTE_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 269
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_3D"

    const/16 v14, 0x33

    const/16 v15, 0x33

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_3D:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 270
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_LCPP"

    const/16 v14, 0x34

    const/16 v15, 0x34

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_LCPP:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 271
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PREPRINTED"

    const/16 v14, 0x35

    const/16 v15, 0x35

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PREPRINTED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 272
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_LETTERHEAD"

    const/16 v14, 0x36

    const/16 v15, 0x36

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_LETTERHEAD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 273
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_RECYCLED"

    const/16 v14, 0x37

    const/16 v15, 0x37

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_RECYCLED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 274
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_COLOR"

    const/16 v14, 0x38

    const/16 v15, 0x38

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_COLOR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 275
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_BUSINESS_PLAIN"

    const/16 v14, 0x39

    const/16 v15, 0x39

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BUSINESS_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 276
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PLAIN_ROLL_STICKER"

    const/16 v14, 0x3a

    const/16 v15, 0x3b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN_ROLL_STICKER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 277
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_GROSSY_ROLL_STICKER"

    const/16 v14, 0x3b

    const/16 v15, 0x3c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_GROSSY_ROLL_STICKER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 278
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PLAIN1"

    const/16 v14, 0x3c

    const/16 v15, 0x3d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 279
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PLAIN2"

    const/16 v14, 0x3d

    const/16 v15, 0x3e

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 280
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_THICKPAPER1"

    const/16 v14, 0x3e

    const/16 v15, 0x41

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 281
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_THICKPAPER2"

    const/16 v14, 0x3f

    const/16 v15, 0x42

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 282
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_THICKPAPER3"

    const/16 v14, 0x40

    const/16 v15, 0x43

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 283
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_THICKPAPER4"

    const/16 v14, 0x41

    const/16 v15, 0x44

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 284
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_THINPAPER1"

    const/16 v14, 0x42

    const/16 v15, 0x45

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THINPAPER1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 285
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_HIGH_QUALITY_PLAIN"

    const/16 v14, 0x43

    const/16 v15, 0x46

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HIGH_QUALITY_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 286
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_BS_HALFGLOSSY_DS"

    const/16 v14, 0x44

    const/16 v15, 0x47

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BS_HALFGLOSSY_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 287
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_THICKPAPER5"

    const/16 v14, 0x45

    const/16 v15, 0x48

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 288
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_TRANSPARENCY"

    const/16 v14, 0x46

    const/16 v15, 0x4a

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_TRANSPARENCY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 289
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_SPECIAL"

    const/16 v14, 0x47

    const/16 v15, 0x4b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SPECIAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 290
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_UNSPECIFIED"

    const/16 v14, 0x48

    const/16 v15, 0x4c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_UNSPECIFIED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 291
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_SEMI_THICK"

    const/16 v14, 0x49

    const/16 v15, 0x4d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SEMI_THICK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 292
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_CDDVD"

    const/16 v14, 0x4a

    const/16 v15, 0x5b

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CDDVD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 293
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_CDDVDHIGH"

    const/16 v14, 0x4b

    const/16 v15, 0x5c

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CDDVDHIGH:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 294
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_CDDVDGLOSSY"

    const/16 v14, 0x4c

    const/16 v15, 0x5d

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CDDVDGLOSSY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 295
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_CLEANING"

    const/16 v14, 0x4d

    const/16 v15, 0x63

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CLEANING:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 296
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_PLOOFING_WHITE_MAT"

    const/16 v14, 0x4e

    const/16 v15, 0x8e

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLOOFING_WHITE_MAT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 297
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_BARYTA"

    const/16 v14, 0x4f

    const/16 v15, 0xae

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BARYTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 298
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_COATED"

    const/16 v14, 0x50

    const/16 v15, 0xb0

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_COATED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 299
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_LABEL"

    const/16 v14, 0x51

    const/16 v15, 0xb7

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_LABEL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 300
    new-instance v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const-string v1, "EPS_MTID_AUTO_PLAIN"

    const/16 v14, 0x52

    const/16 v15, 0xfd

    invoke-direct {v0, v1, v14, v15}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_AUTO_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v0, 0x53

    .line 216
    new-array v0, v0, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_360INKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_IRON:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOINKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOADSHEET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOFILM:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MINIPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_OHP:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BACKLIGHT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    aput-object v1, v0, v13

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PSPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MCGLOSSY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_ARCHMATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_WATERCOLOR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PROGLOSS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTEBOARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOGLOSS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SEMIPROOF:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SUPERFINE2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_DSMATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CLPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_ECOPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_VELVETFINEART:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PROOFSEMI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HAGAKIRECL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HAGAKIINKJET:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOINKJET2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_DURABRITE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTEMEISHI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HAGAKIATENA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOALBUM:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOSTAND:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_RCB:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTOEG:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_ENVELOPE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLATINA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_ULTRASMOOTH:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SFHAGAKI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PHOTOSTD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_GLOSSYHAGAKI:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_GLOSSYPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_GLOSSYCAST:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BUSINESSCOAT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MEDICINEBAG:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BROCHURE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTE_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BSMATTE_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_3D:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_LCPP:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PREPRINTED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_LETTERHEAD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_RECYCLED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_COLOR:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BUSINESS_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN_ROLL_STICKER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_GROSSY_ROLL_STICKER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER2:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THINPAPER1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_HIGH_QUALITY_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BS_HALFGLOSSY_DS:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_THICKPAPER5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_TRANSPARENCY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SPECIAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_UNSPECIFIED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_SEMI_THICK:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CDDVD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CDDVDHIGH:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CDDVDGLOSSY:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_CLEANING:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLOOFING_WHITE_MAT:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_BARYTA:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_COATED:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_LABEL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_AUTO_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sput-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->$VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 304
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 305
    iput p3, p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;
    .locals 1

    .line 216
    const-class v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;
    .locals 1

    .line 216
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->$VALUES:[Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .line 309
    iget v0, p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->code:I

    return v0
.end method

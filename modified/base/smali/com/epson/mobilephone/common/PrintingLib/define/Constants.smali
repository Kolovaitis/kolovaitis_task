.class public Lcom/epson/mobilephone/common/PrintingLib/define/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;,
        Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;,
        Lcom/epson/mobilephone/common/PrintingLib/define/Constants$ColorName;
    }
.end annotation


# static fields
.field public static EPSON_VENDERID:I = 0x4b8

.field public static final EPS_ERR_COMM_ERROR:I = -0x44c

.field public static final EPS_ERR_IPPRINTER_CHANGED:I = -0x7a121

.field public static final EPS_ERR_LIB_INTIALIZED:I = -0x41a

.field public static final EPS_ERR_PRINTER_NOT_FOUND:I = -0x514

.field public static final EPS_ERR_PRINTER_NOT_SUPPORTED:I = -0x3f8

.field public static final EPS_MLID_BORDERLESS:I = 0x1

.field public static final EPS_MLID_BORDERS:I = 0x2

.field public static final EPS_MLID_BORDERS_1:I = 0x0

.field public static final EPS_MLID_BORDERS_2in1:I = 0x10000

.field public static final EPS_MLID_BORDERS_4in1_N:I = 0x40000

.field public static final EPS_MLID_BORDERS_4in1_Z:I = 0x20000

.field public static final EPS_MLID_CDLABEL:I = 0x4

.field public static final EPS_MLID_CUSTOM:I = 0x0

.field public static final EPS_MLID_DIVIDE16:I = 0x8

.field public static final EPS_MPID_AUTO:I = 0x80

.field public static final EPS_MPID_CDTRAY:I = 0x8

.field public static final EPS_MPID_FRONT1:I = 0x2

.field public static final EPS_MPID_FRONT2:I = 0x4

.field public static final EPS_MPID_FRONT3:I = 0x20

.field public static final EPS_MPID_FRONT4:I = 0x40

.field public static final EPS_MPID_HIGHCAP:I = 0x800

.field public static final EPS_MPID_MANUAL2:I = 0x200

.field public static final EPS_MPID_MPTRAY:I = 0x8000

.field public static final EPS_MPID_MPTRAY_IJ:I = 0x400

.field public static final EPS_MPID_NOT_SPEC:I = 0x0

.field public static final EPS_MPID_REAR:I = 0x1

.field public static final EPS_MPID_REARMANUAL:I = 0x10

.field public static final EPS_MPID_ROLL:I = 0x100

.field public static final EPS_PRB_BYADDR:I = 0x2

.field public static final EPS_PRB_BYID:I = 0x1

.field public static final EPS_PROTOCOL_LPR:I = 0x40

.field public static final EPS_PROTOCOL_NET:I = 0xc0

.field public static final EPS_PROTOCOL_RAW:I = 0x80

.field public static final PRINTER_IP:I = 0x3

.field public static final PRINTER_LOCAL:I = 0x1

.field public static final PRINTER_REMOTE:I = 0x2

.field public static final SCAN_REFS_SCANNER_SIMPLEAP:Ljava/lang/String; = "SCAN_REFS_SCANNER_SIMPLEAP"

.field public static final STRING_EMPTY:Ljava/lang/String; = ""


# instance fields
.field public final EPS_OK:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 64
    iput v0, p0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants;->EPS_OK:I

    return-void
.end method

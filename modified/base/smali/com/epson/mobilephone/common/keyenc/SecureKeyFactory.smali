.class public Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;
.super Ljava/lang/Object;
.source "SecureKeyFactory.java"


# static fields
.field static instance:Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;


# instance fields
.field private final ENCRYPT_ALGORITHM:Ljava/lang/String;

.field private final ENCRYPT_KEY_BYTES:I

.field private final ENCRYPT_METHOD:Ljava/lang/String;

.field private final ENCRYPT_PROVIDER:Ljava/lang/String;

.field private final charset:Ljava/nio/charset/Charset;

.field private key:[B

.field protected mAppContext:Landroid/content/Context;

.field mSecurityProvider:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    .line 57
    iput v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->ENCRYPT_KEY_BYTES:I

    const-string v0, "AES"

    .line 58
    iput-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->ENCRYPT_ALGORITHM:Ljava/lang/String;

    const-string v0, "AES/ECB/PKCS5Padding"

    .line 59
    iput-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->ENCRYPT_METHOD:Ljava/lang/String;

    const-string v0, "AndroidOpenSSL"

    .line 60
    iput-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->ENCRYPT_PROVIDER:Ljava/lang/String;

    const-string v0, "UTF8"

    .line 62
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->charset:Ljava/nio/charset/Charset;

    const/4 v0, 0x0

    .line 65
    iput-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->mAppContext:Landroid/content/Context;

    const-string v1, "AndroidOpenSSL"

    .line 68
    iput-object v1, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->mSecurityProvider:Ljava/lang/String;

    .line 71
    iput-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->key:[B

    :try_start_0
    const-string v0, "keyenclib"

    .line 78
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 80
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 89
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->mAppContext:Landroid/content/Context;

    .line 90
    iget-object p1, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->mAppContext:Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->navite_getKey(Landroid/content/Context;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->key:[B

    return-void
.end method

.method private getCipher()Ljavax/crypto/Cipher;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/NoSuchPaddingException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    :try_start_0
    const-string v0, "AES/ECB/PKCS5Padding"

    .line 139
    iget-object v1, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->mSecurityProvider:Ljava/lang/String;

    invoke-static {v0, v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    .line 141
    :goto_0
    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->printStackTrace()V

    const-string v0, "AES/ECB/PKCS5Padding"

    .line 143
    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;
    .locals 1

    const/4 v0, 0x0

    .line 100
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;
    .locals 1

    .line 111
    sget-object v0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->instance:Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->instance:Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    :cond_0
    if-nez p1, :cond_1

    .line 119
    sget-object p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->instance:Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string p1, "AndroidOpenSSL"

    iput-object p1, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->mSecurityProvider:Ljava/lang/String;

    goto :goto_0

    .line 120
    :cond_1
    sget-object p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->instance:Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    iget-object p0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->mSecurityProvider:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 121
    sget-object p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->instance:Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    iput-object p1, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->mSecurityProvider:Ljava/lang/String;

    .line 126
    :cond_2
    :goto_0
    sget-object p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->instance:Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;

    return-object p0
.end method

.method public static getMd5([B)[B
    .locals 2

    const/4 v0, 0x1

    .line 282
    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v1, v0, v1

    :try_start_0
    const-string v1, "MD5"

    .line 287
    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 288
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 289
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 291
    invoke-virtual {p0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public Decode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->key:[B

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    const/16 v0, 0xa

    .line 182
    invoke-static {p1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, ""

    return-object p1

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->key:[B

    invoke-virtual {p0, p1, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->decodeAes([B[B)[B

    move-result-object p1

    if-nez p1, :cond_2

    const-string p1, ""

    return-object p1

    .line 194
    :cond_2
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->charset:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public Encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->key:[B

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->charset:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    iget-object v0, p0, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->key:[B

    invoke-virtual {p0, p1, v0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->encodeAes([B[B)[B

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, ""

    return-object p1

    :cond_1
    const/16 v0, 0xa

    .line 168
    invoke-static {p1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    const-string p1, ""

    return-object p1

    :cond_2
    return-object p1
.end method

.method decodeAes([B[B)[B
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x10

    .line 251
    :try_start_0
    invoke-virtual {p0, v1, p2}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->makeKeyBytes(I[B)[B

    move-result-object p2

    .line 252
    invoke-direct {p0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v1

    .line 253
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "AES"

    invoke-direct {v2, p2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const/4 p2, 0x2

    .line 254
    invoke-virtual {v1, p2, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 255
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 257
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-object v0

    :cond_1
    :goto_0
    return-object v0
.end method

.method encodeAes([B[B)[B
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x10

    .line 209
    :try_start_0
    invoke-virtual {p0, v1, p2}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->makeKeyBytes(I[B)[B

    move-result-object p2

    .line 210
    invoke-direct {p0}, Lcom/epson/mobilephone/common/keyenc/SecureKeyFactory;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v1

    .line 211
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "AES"

    invoke-direct {v2, p2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const/4 p2, 0x1

    .line 212
    invoke-virtual {v1, p2, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 213
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 215
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-object v0

    :cond_1
    :goto_0
    return-object v0
.end method

.method makeKeyBytes(I[B)[B
    .locals 3

    .line 228
    new-array p1, p1, [B

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 229
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 230
    array-length v2, p2

    if-ge v1, v2, :cond_0

    .line 231
    aget-byte v2, p2, v1

    aput-byte v2, p1, v1

    goto :goto_1

    .line 233
    :cond_0
    aput-byte v0, p1, v1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object p1
.end method

.method native navite_getAppKey(Landroid/content/Context;)[B
.end method

.method native navite_getKey(Landroid/content/Context;)[B
.end method

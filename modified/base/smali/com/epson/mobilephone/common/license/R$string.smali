.class public final Lcom/epson/mobilephone/common/license/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/license/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Agree_License:I = 0x7f0e0004

.field public static final Cancel:I = 0x7f0e002a

.field public static final Disagree_License:I = 0x7f0e0044

.field public static final Disagree_License_Button_Message:I = 0x7f0e0045

.field public static final EULA_Title:I = 0x7f0e0221

.field public static final GALicense:I = 0x7f0e0233

.field public static final GALicense_Label:I = 0x7f0e0234

.field public static final OK:I = 0x7f0e023e

.field public static final OSS_License_Title:I = 0x7f0e023f

.field public static final Privacy_Statement_Title:I = 0x7f0e024d

.field public static final Update_Message:I = 0x7f0e0259

.field public static final Use_Information_Message:I = 0x7f0e025a

.field public static final Use_Information_Title:I = 0x7f0e025b

.field public static final abc_action_bar_home_description:I = 0x7f0e027a

.field public static final abc_action_bar_up_description:I = 0x7f0e027b

.field public static final abc_action_menu_overflow_description:I = 0x7f0e027c

.field public static final abc_action_mode_done:I = 0x7f0e027d

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0e027e

.field public static final abc_activitychooserview_choose_application:I = 0x7f0e027f

.field public static final abc_capital_off:I = 0x7f0e0280

.field public static final abc_capital_on:I = 0x7f0e0281

.field public static final abc_font_family_body_1_material:I = 0x7f0e0282

.field public static final abc_font_family_body_2_material:I = 0x7f0e0283

.field public static final abc_font_family_button_material:I = 0x7f0e0284

.field public static final abc_font_family_caption_material:I = 0x7f0e0285

.field public static final abc_font_family_display_1_material:I = 0x7f0e0286

.field public static final abc_font_family_display_2_material:I = 0x7f0e0287

.field public static final abc_font_family_display_3_material:I = 0x7f0e0288

.field public static final abc_font_family_display_4_material:I = 0x7f0e0289

.field public static final abc_font_family_headline_material:I = 0x7f0e028a

.field public static final abc_font_family_menu_material:I = 0x7f0e028b

.field public static final abc_font_family_subhead_material:I = 0x7f0e028c

.field public static final abc_font_family_title_material:I = 0x7f0e028d

.field public static final abc_search_hint:I = 0x7f0e0298

.field public static final abc_searchview_description_clear:I = 0x7f0e0299

.field public static final abc_searchview_description_query:I = 0x7f0e029a

.field public static final abc_searchview_description_search:I = 0x7f0e029b

.field public static final abc_searchview_description_submit:I = 0x7f0e029c

.field public static final abc_searchview_description_voice:I = 0x7f0e029d

.field public static final abc_shareactionprovider_share_with:I = 0x7f0e029e

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0e029f

.field public static final abc_toolbar_collapse_description:I = 0x7f0e02a0

.field public static final license_display_separation_string:I = 0x7f0e03a1

.field public static final search_menu_title:I = 0x7f0e045e

.field public static final status_bar_notification_info_overflow:I = 0x7f0e046c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

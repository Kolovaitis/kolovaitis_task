.class public Lcom/epson/mobilephone/common/license/InfoDisplayActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "InfoDisplayActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;,
        Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;
    }
.end annotation


# static fields
.field private static final KEY_SCROLL_Y:Ljava/lang/String; = "scroll_y"

.field private static final PARAM_KEY_DOCUMENT_TYPE:Ljava/lang/String; = "document_type"

.field private static final PARAM_KEY_LICENSE_INFO:Ljava/lang/String; = "license_info"

.field private static final PARAM_KEY_TITLE_AND_DOCUMENT:Ljava/lang/String; = "class_title_and_document"


# instance fields
.field private mScrollView:Lcom/epson/mobilephone/common/license/RestoreScrollView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(I)I
    .locals 0

    .line 19
    invoke-static {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getTitleStringId(I)I

    move-result p0

    return p0
.end method

.method private static getExternalTitleAndDocumentFromIntent(Landroid/content/Intent;)Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    const-string v0, "class_title_and_document"

    .line 103
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;

    return-object p0
.end method

.method private static getInternalTitleAndDocumentFromIntent(Landroid/content/Intent;)Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;
    .locals 3
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    const-string v0, "license_info"

    .line 93
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/epson/mobilephone/common/license/LicenseInfo;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v1, "document_type"

    const/4 v2, 0x1

    .line 97
    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    .line 98
    new-instance v1, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;

    invoke-direct {v1, p0, v0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;-><init>(ILcom/epson/mobilephone/common/license/LicenseInfo;)V

    return-object v1
.end method

.method public static getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 163
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "class_title_and_document"

    .line 164
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method public static getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;I)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 148
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "license_info"

    .line 149
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p0, "document_type"

    .line 150
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method static getTitleAndDocumentFromIntent(Landroid/content/Intent;)Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 84
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getInternalTitleAndDocumentFromIntent(Landroid/content/Intent;)Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 88
    :cond_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getExternalTitleAndDocumentFromIntent(Landroid/content/Intent;)Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;

    move-result-object p0

    return-object p0
.end method

.method private static getTitleStringId(I)I
    .locals 0

    packed-switch p0, :pswitch_data_0

    .line 117
    sget p0, Lcom/epson/mobilephone/common/license/R$string;->EULA_Title:I

    return p0

    .line 113
    :pswitch_0
    sget p0, Lcom/epson/mobilephone/common/license/R$string;->OSS_License_Title:I

    return p0

    .line 110
    :pswitch_1
    sget p0, Lcom/epson/mobilephone/common/license/R$string;->Privacy_Statement_Title:I

    return p0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 43
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const/16 v0, 0x8

    .line 44
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->supportRequestWindowFeature(I)Z

    .line 45
    sget v0, Lcom/epson/mobilephone/common/license/R$layout;->activity_info_display:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->setContentView(I)V

    .line 47
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->toolbar:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 48
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 50
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getTitleAndDocumentFromIntent(Landroid/content/Intent;)Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;

    move-result-object v0

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->finish()V

    return-void

    .line 59
    :cond_0
    sget v1, Lcom/epson/mobilephone/common/license/R$id;->contentText:I

    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 60
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;->getDocumentString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 61
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    sget v1, Lcom/epson/mobilephone/common/license/R$id;->mainScrollView:I

    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/epson/mobilephone/common/license/RestoreScrollView;

    iput-object v1, p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->mScrollView:Lcom/epson/mobilephone/common/license/RestoreScrollView;

    .line 65
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 67
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 69
    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_1
    if-eqz p1, :cond_2

    .line 73
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->mScrollView:Lcom/epson/mobilephone/common/license/RestoreScrollView;

    const-string v1, "scroll_y"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/license/RestoreScrollView;->setScrollY(I)V

    :cond_2
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 123
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-eq v0, v1, :cond_0

    .line 129
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->finish()V

    const/4 p1, 0x1

    return p1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 134
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "scroll_y"

    .line 135
    iget-object v1, p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->mScrollView:Lcom/epson/mobilephone/common/license/RestoreScrollView;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/license/RestoreScrollView;->getLastY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

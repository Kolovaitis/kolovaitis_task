.class public Lcom/epson/mobilephone/common/license/LicenseTopActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "LicenseTopActivity.java"

# interfaces
.implements Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;


# static fields
.field private static final DIALOG_ID:Ljava/lang/String; = "dialog"

.field private static final DIALOG_ID_DISAGREE:I = 0x2

.field private static final DIALOG_ID_NEW_LICENSE:I = 0x1

.field private static final KEY_LICENSE_INFO:Ljava/lang/String; = "license_info"

.field private static final KEY_USER_SURVEY_INFO:Ljava/lang/String; = "user_survey_info"

.field private static final REQUEST_CODE_USER_SURVEY_INVITATION:I = 0x2


# instance fields
.field private mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

.field private mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/license/LicenseTopActivity;I)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->startDocumentDisplayActivity(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/license/LicenseTopActivity;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->onAgree()V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/mobilephone/common/license/LicenseTopActivity;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->onDisagree()V

    return-void
.end method

.method public static getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;Lcom/epson/mobilephone/common/license/UserSurveyInfo;)Landroid/content/Intent;
    .locals 2
    .param p1    # Lcom/epson/mobilephone/common/license/LicenseInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/mobilephone/common/license/UserSurveyInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 241
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/license/LicenseTopActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "license_info"

    .line 242
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p0, "user_survey_info"

    .line 243
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method private initField(Landroid/content/Intent;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string v1, "license_info"

    .line 99
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/epson/mobilephone/common/license/LicenseInfo;

    iput-object v1, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    .line 100
    iget-object v1, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    if-nez v1, :cond_1

    return v0

    :cond_1
    const-string v0, "user_survey_info"

    .line 104
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    iput-object p1, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    const/4 p1, 0x1

    return p1
.end method

.method private onAgree()V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    if-nez v0, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->finish()V

    return-void

    .line 159
    :cond_0
    invoke-interface {v0, p0}, Lcom/epson/mobilephone/common/license/LicenseInfo;->setLicenceAgreement(Landroid/content/Context;)V

    const/4 v0, -0x1

    .line 160
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->setResult(I)V

    .line 162
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->startUserSurveyInvitationActivity()V

    return-void
.end method

.method private onDisagree()V
    .locals 3

    .line 187
    sget v0, Lcom/epson/mobilephone/common/license/R$string;->Disagree_License_Button_Message:I

    .line 188
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    .line 187
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->newInstance(Ljava/lang/String;I)Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;

    move-result-object v0

    .line 190
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showLicenseChangeDialogIfNeeded(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;Lcom/epson/mobilephone/common/license/UserSurveyInfo;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/mobilephone/common/license/LicenseInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 113
    invoke-interface {p2, p1}, Lcom/epson/mobilephone/common/license/LicenseInfo;->getLicenseAgreement(Landroid/content/Context;)I

    move-result p2

    packed-switch p2, :pswitch_data_0

    return-void

    :pswitch_0
    const/4 p2, -0x1

    .line 118
    invoke-virtual {p0, p2}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->setResult(I)V

    if-eqz p3, :cond_1

    .line 119
    invoke-interface {p3, p1}, Lcom/epson/mobilephone/common/license/UserSurveyInfo;->getResponseStatus(Landroid/content/Context;)I

    move-result p1

    const/4 p2, 0x2

    if-lt p1, p2, :cond_0

    goto :goto_0

    .line 123
    :cond_0
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->startUserSurveyInvitationActivity()V

    return-void

    .line 120
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->finish()V

    return-void

    .line 137
    :pswitch_1
    sget p1, Lcom/epson/mobilephone/common/license/R$string;->Privacy_Statement_Title:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 133
    :pswitch_2
    sget p1, Lcom/epson/mobilephone/common/license/R$string;->EULA_Title:I

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 128
    :pswitch_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget p2, Lcom/epson/mobilephone/common/license/R$string;->EULA_Title:I

    invoke-virtual {p0, p2}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget p2, Lcom/epson/mobilephone/common/license/R$string;->Privacy_Statement_Title:I

    .line 129
    invoke-virtual {p0, p2}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 145
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    sget p3, Lcom/epson/mobilephone/common/license/R$string;->Update_Message:I

    .line 146
    invoke-virtual {p0, p3}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "\n"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    .line 145
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->newInstance(Ljava/lang/String;I)Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;

    move-result-object p1

    .line 148
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p2

    const-string p3, "dialog"

    invoke-virtual {p1, p2, p3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private startDocumentDisplayActivity(I)V
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    if-nez v0, :cond_0

    return-void

    .line 198
    :cond_0
    invoke-static {p0, v0, p1}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;I)Landroid/content/Intent;

    move-result-object p1

    .line 199
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startUserSurveyInvitationActivity()V
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    if-nez v0, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->finish()V

    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    if-nez v0, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->finish()V

    return-void

    .line 177
    :cond_1
    invoke-interface {v0, p0}, Lcom/epson/mobilephone/common/license/UserSurveyInfo;->getResponseStatus(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_2

    .line 179
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->finish()V

    return-void

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    iget-object v2, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    invoke-static {p0, v0, v2}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getStartWithDialogIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/UserSurveyInfo;Lcom/epson/mobilephone/common/license/LicenseInfo;)Landroid/content/Intent;

    move-result-object v0

    .line 183
    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    const/4 p2, 0x2

    if-eq p1, p2, :cond_0

    return-void

    :cond_0
    const/4 p1, -0x1

    .line 227
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->setResult(I)V

    .line 228
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->finish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method public onButtonClicked(I)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    return-void

    :pswitch_0
    const/4 p1, 0x0

    .line 206
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->setResult(I)V

    .line 207
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->finish()V

    return-void

    :pswitch_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 39
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    sget v0, Lcom/epson/mobilephone/common/license/R$layout;->activity_license_top:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->setContentView(I)V

    .line 42
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->licenseViewGroup:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 43
    new-instance v1, Lcom/epson/mobilephone/common/license/LicenseTopActivity$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity$1;-><init>(Lcom/epson/mobilephone/common/license/LicenseTopActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->privacyStatementViewGroup:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 50
    new-instance v1, Lcom/epson/mobilephone/common/license/LicenseTopActivity$2;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity$2;-><init>(Lcom/epson/mobilephone/common/license/LicenseTopActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->agreeButton:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 59
    new-instance v1, Lcom/epson/mobilephone/common/license/LicenseTopActivity$3;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity$3;-><init>(Lcom/epson/mobilephone/common/license/LicenseTopActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->disagreeButton:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 67
    new-instance v1, Lcom/epson/mobilephone/common/license/LicenseTopActivity$4;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity$4;-><init>(Lcom/epson/mobilephone/common/license/LicenseTopActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->initField(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 76
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->setResult(I)V

    .line 77
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->finish()V

    return-void

    .line 81
    :cond_0
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->appNameText:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    iget-object v1, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    invoke-interface {v1, p0}, Lcom/epson/mobilephone/common/license/LicenseInfo;->getApplicationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_1

    .line 85
    iget-object p1, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    iget-object v0, p0, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    invoke-direct {p0, p0, p1, v0}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->showLicenseChangeDialogIfNeeded(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;Lcom/epson/mobilephone/common/license/UserSurveyInfo;)V

    :cond_1
    return-void
.end method

.class public Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SimpleMessageDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;
    }
.end annotation


# static fields
.field private static final PARAM_DIALOG_ID:Ljava/lang/String; = "dialog_id"

.field private static final PARAM_MESSAGE_STRING:Ljava/lang/String; = "message_string"


# instance fields
.field private mDialogCallback:Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;

.field private mDialogId:I

.field private mMessageString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->onOkClicked()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;I)Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 35
    new-instance v0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;-><init>()V

    .line 37
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "dialog_id"

    .line 38
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p1, "message_string"

    .line 39
    invoke-virtual {v1, p1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private onOkClicked()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->mDialogCallback:Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;

    iget v1, p0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->mDialogId:I

    invoke-interface {v0, v1}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;->onButtonClicked(I)V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .line 82
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 84
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;

    iput-object v0, p0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->mDialogCallback:Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 86
    :catch_0
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement DialogCallback"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 47
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "dialog_id"

    .line 50
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->mDialogId:I

    const-string v0, "message_string"

    .line 51
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->mMessageString:Ljava/lang/String;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 59
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->mMessageString:Ljava/lang/String;

    .line 60
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/epson/mobilephone/common/license/R$string;->OK:I

    new-instance v1, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$1;-><init>(Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 71
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->setCancelable(Z)V

    return-object p1
.end method

.method public onDetach()V
    .locals 1

    .line 92
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDetach()V

    const/4 v0, 0x0

    .line 93
    iput-object v0, p0, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->mDialogCallback:Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;

    return-void
.end method

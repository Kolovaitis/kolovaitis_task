.class public abstract Lcom/epson/mobilephone/common/license/DefaultLicenseInfo;
.super Ljava/lang/Object;
.source "DefaultLicenseInfo.java"

# interfaces
.implements Lcom/epson/mobilephone/common/license/LicenseInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getDefaultPrivacyStatement(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 16
    sget v0, Lcom/epson/mobilephone/common/license/R$raw;->privacy:I

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/license/Util;->getStringFromRawResource(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getDefaultSoftwareLicense(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 12
    sget v0, Lcom/epson/mobilephone/common/license/R$raw;->eula:I

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/license/Util;->getStringFromRawResource(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getDocumentString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    packed-switch p2, :pswitch_data_0

    .line 31
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/license/DefaultLicenseInfo;->getDefaultSoftwareLicense(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_0
    const-string p1, ""

    return-object p1

    .line 23
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/license/DefaultLicenseInfo;->getDefaultPrivacyStatement(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

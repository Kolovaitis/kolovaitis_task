.class public Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "UserSurveyInvitationActivity.java"

# interfaces
.implements Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment$DialogCallback;


# static fields
.field private static final DIALOG_ID_CHANGE_DIALOG:I = 0x1

.field private static final PARAM_DISPLAY_CHANGE_DIALOG:Ljava/lang/String; = "display_change_dialog"

.field private static final PARAM_LICENSE_INFO:Ljava/lang/String; = "license_info"

.field private static final PARAM_USER_SURVEY_INFO:Ljava/lang/String; = "user_survey_info"


# instance fields
.field private mDisplayChangeDialog:Z

.field private mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

.field private mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;Z)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->finishWithStatus(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->startPrivacyStatementDisplayActivity()V

    return-void
.end method

.method private changeButtonText(Landroid/widget/Button;ILcom/epson/mobilephone/common/license/UserSurveyInfo;Landroid/content/Context;)V
    .locals 0
    .param p4    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    if-nez p3, :cond_0

    return-void

    .line 143
    :cond_0
    invoke-interface {p3, p4, p2}, Lcom/epson/mobilephone/common/license/UserSurveyInfo;->getButtonString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 145
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private finishWithStatus(Z)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    if-eqz v0, :cond_0

    .line 151
    invoke-interface {v0, p0, p1}, Lcom/epson/mobilephone/common/license/UserSurveyInfo;->setUserSurveyAgreement(Landroid/content/Context;Z)V

    :cond_0
    if-eqz p1, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 154
    :goto_0
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->setResult(I)V

    .line 155
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->finish()V

    return-void
.end method

.method public static getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/UserSurveyInfo;Lcom/epson/mobilephone/common/license/LicenseInfo;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/epson/mobilephone/common/license/UserSurveyInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    .line 210
    invoke-static {p0, p1, p2, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/UserSurveyInfo;Lcom/epson/mobilephone/common/license/LicenseInfo;Z)Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method private static getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/UserSurveyInfo;Lcom/epson/mobilephone/common/license/LicenseInfo;Z)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 184
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "user_survey_info"

    .line 185
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p0, "display_change_dialog"

    .line 186
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "license_info"

    .line 187
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method static getStartWithDialogIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/UserSurveyInfo;Lcom/epson/mobilephone/common/license/LicenseInfo;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, 0x1

    .line 197
    invoke-static {p0, p1, p2, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/UserSurveyInfo;Lcom/epson/mobilephone/common/license/LicenseInfo;Z)Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method private showUpdateDialog()V
    .locals 3

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v1, Lcom/epson/mobilephone/common/license/R$string;->Update_Message:I

    .line 132
    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/epson/mobilephone/common/license/R$string;->GALicense:I

    .line 133
    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    .line 131
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;->newInstance(Ljava/lang/String;I)Lcom/epson/mobilephone/common/license/SimpleMessageDialogFragment;

    move-result-object v0

    .line 135
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startPrivacyStatementDisplayActivity()V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x2

    .line 114
    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;I)Landroid/content/Intent;

    move-result-object v0

    .line 116
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private updateAppNameText(Lcom/epson/mobilephone/common/license/LicenseInfo;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 123
    :cond_0
    invoke-interface {p1, p0}, Lcom/epson/mobilephone/common/license/LicenseInfo;->getApplicationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 125
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->appNameText:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 126
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .line 171
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mDisplayChangeDialog:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 177
    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->finishWithStatus(Z)V

    return-void
.end method

.method public onButtonClicked(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 45
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    sget v0, Lcom/epson/mobilephone/common/license/R$layout;->activity_user_survey_invitation:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->setContentView(I)V

    .line 48
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->toolbar:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 49
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 50
    sget v0, Lcom/epson/mobilephone/common/license/R$string;->GALicense:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->setTitle(I)V

    .line 52
    sget v0, Lcom/epson/mobilephone/common/license/R$id;->disagreeButton:I

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 53
    new-instance v1, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity$1;-><init>(Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    sget v1, Lcom/epson/mobilephone/common/license/R$id;->agreeButton:I

    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 61
    new-instance v2, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity$2;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity$2;-><init>(Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    sget v2, Lcom/epson/mobilephone/common/license/R$id;->privacyStatementViewGroup:I

    invoke-virtual {p0, v2}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 69
    new-instance v3, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity$3;

    invoke-direct {v3, p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity$3;-><init>(Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    sget v2, Lcom/epson/mobilephone/common/license/R$id;->analytics_content:I

    invoke-virtual {p0, v2}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/4 v3, 0x0

    .line 78
    iput-boolean v3, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mDisplayChangeDialog:Z

    .line 80
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v5, "user_survey_info"

    .line 82
    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    iput-object v5, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    const-string v5, "license_info"

    .line 83
    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/epson/mobilephone/common/license/LicenseInfo;

    iput-object v5, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    const/4 v5, 0x0

    .line 86
    iget-object v6, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    if-eqz v6, :cond_0

    .line 87
    invoke-interface {v6, p0}, Lcom/epson/mobilephone/common/license/UserSurveyInfo;->getUserSurveyInvitationText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    :cond_0
    if-nez v5, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/epson/mobilephone/common/license/R$raw;->user_survey:I

    invoke-static {v5, v6}, Lcom/epson/mobilephone/common/license/Util;->getStringFromRawResource(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 92
    :cond_1
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v2, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    const/4 v5, 0x1

    invoke-direct {p0, v1, v5, v2, p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->changeButtonText(Landroid/widget/Button;ILcom/epson/mobilephone/common/license/UserSurveyInfo;Landroid/content/Context;)V

    .line 95
    iget-object v1, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    invoke-direct {p0, v0, v3, v1, p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->changeButtonText(Landroid/widget/Button;ILcom/epson/mobilephone/common/license/UserSurveyInfo;Landroid/content/Context;)V

    .line 97
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->updateAppNameText(Lcom/epson/mobilephone/common/license/LicenseInfo;)V

    const-string v0, "display_change_dialog"

    .line 99
    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mDisplayChangeDialog:Z

    .line 100
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mDisplayChangeDialog:Z

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 101
    iget-object p1, p0, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->mUserSurveyInfo:Lcom/epson/mobilephone/common/license/UserSurveyInfo;

    if-eqz p1, :cond_2

    .line 102
    invoke-interface {p1, p0}, Lcom/epson/mobilephone/common/license/UserSurveyInfo;->getResponseStatus(Landroid/content/Context;)I

    move-result p1

    if-ne p1, v5, :cond_2

    .line 103
    invoke-direct {p0}, Lcom/epson/mobilephone/common/license/UserSurveyInvitationActivity;->showUpdateDialog()V

    :cond_2
    return-void
.end method

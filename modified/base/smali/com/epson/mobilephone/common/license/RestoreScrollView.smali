.class public Lcom/epson/mobilephone/common/license/RestoreScrollView;
.super Landroid/widget/ScrollView;
.source "RestoreScrollView.java"


# instance fields
.field private mLastY:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 17
    iput p1, p0, Lcom/epson/mobilephone/common/license/RestoreScrollView;->mLastY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 22
    iput p1, p0, Lcom/epson/mobilephone/common/license/RestoreScrollView;->mLastY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public getLastY()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/epson/mobilephone/common/license/RestoreScrollView;->mLastY:I

    return v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    .line 31
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 32
    iput p2, p0, Lcom/epson/mobilephone/common/license/RestoreScrollView;->mLastY:I

    return-void
.end method

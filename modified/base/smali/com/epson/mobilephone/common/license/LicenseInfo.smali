.class public interface abstract Lcom/epson/mobilephone/common/license/LicenseInfo;
.super Ljava/lang/Object;
.source "LicenseInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/license/LicenseInfo$DocumentType;,
        Lcom/epson/mobilephone/common/license/LicenseInfo$LicenseMode;
    }
.end annotation


# static fields
.field public static final DOCUMENT_TYPE_LICENSE:I = 0x1

.field public static final DOCUMENT_TYPE_OSS_LICENSE:I = 0x3

.field public static final DOCUMENT_TYPE_PRIVACY_STATEMENT:I = 0x2

.field public static final LICENSE_MODE_AGREED_CURRENT_VERSION:I = 0x3

.field public static final LICENSE_MODE_EULA_AND_PRIVACY_CHANGED:I = 0x0

.field public static final LICENSE_MODE_EULA_CHANGED:I = 0x1

.field public static final LICENSE_MODE_NOT_AGREED:I = -0x1

.field public static final LICENSE_MODE_PRIVACY_STATEMENT_CHANGED:I = 0x2


# virtual methods
.method public abstract getApplicationName(Landroid/content/Context;)Ljava/lang/String;
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getDocumentString(Landroid/content/Context;I)Ljava/lang/String;
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getLicenseAgreement(Landroid/content/Context;)I
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract setLicenceAgreement(Landroid/content/Context;)V
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

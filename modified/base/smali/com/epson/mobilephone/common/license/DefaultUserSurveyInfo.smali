.class public abstract Lcom/epson/mobilephone/common/license/DefaultUserSurveyInfo;
.super Ljava/lang/Object;
.source "DefaultUserSurveyInfo.java"

# interfaces
.implements Lcom/epson/mobilephone/common/license/UserSurveyInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getButtonString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    .line 24
    sget p2, Lcom/epson/mobilephone/common/license/R$string;->Cancel:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 21
    :cond_0
    sget p2, Lcom/epson/mobilephone/common/license/R$string;->OK:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUserSurveyInvitationText(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 13
    sget v0, Lcom/epson/mobilephone/common/license/R$raw;->user_survey:I

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/license/Util;->getStringFromRawResource(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

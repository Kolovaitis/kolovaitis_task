.class Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;
.super Ljava/lang/Object;
.source "InfoDisplayActivity.java"

# interfaces
.implements Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndDocument;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/license/InfoDisplayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TitleAndTextInternal"
.end annotation


# instance fields
.field private final mDocumentType:I

.field private final mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;


# direct methods
.method constructor <init>(ILcom/epson/mobilephone/common/license/LicenseInfo;)V
    .locals 0
    .param p2    # Lcom/epson/mobilephone/common/license/LicenseInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iput p1, p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;->mDocumentType:I

    .line 180
    iput-object p2, p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    return-void
.end method


# virtual methods
.method public getDocumentString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 192
    iget-object v0, p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;->mLicenseInfo:Lcom/epson/mobilephone/common/license/LicenseInfo;

    iget v1, p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;->mDocumentType:I

    invoke-interface {v0, p1, v1}, Lcom/epson/mobilephone/common/license/LicenseInfo;->getDocumentString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 186
    iget v0, p0, Lcom/epson/mobilephone/common/license/InfoDisplayActivity$TitleAndTextInternal;->mDocumentType:I

    invoke-static {v0}, Lcom/epson/mobilephone/common/license/InfoDisplayActivity;->access$000(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

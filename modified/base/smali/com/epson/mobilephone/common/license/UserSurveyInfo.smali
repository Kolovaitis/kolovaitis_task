.class public interface abstract Lcom/epson/mobilephone/common/license/UserSurveyInfo;
.super Ljava/lang/Object;
.source "UserSurveyInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/license/UserSurveyInfo$ButtonType;,
        Lcom/epson/mobilephone/common/license/UserSurveyInfo$UserSurveyMode;
    }
.end annotation


# static fields
.field public static final BUTTON_CANCEL:I = 0x0

.field public static final BUTTON_OK:I = 0x1

.field public static final NOT_RESPONDED:I = 0x0

.field public static final RESPONDED_CURRENT_VERSION:I = 0x2

.field public static final RESPONDED_OLD_VERSION:I = 0x1


# virtual methods
.method public abstract getButtonString(Landroid/content/Context;I)Ljava/lang/String;
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResponseStatus(Landroid/content/Context;)I
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getUserSurveyInvitationText(Landroid/content/Context;)Ljava/lang/String;
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract setUserSurveyAgreement(Landroid/content/Context;Z)V
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

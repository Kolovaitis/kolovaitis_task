.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$PasswordFilter;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PasswordFilter"
.end annotation


# instance fields
.field filterType:I

.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;I)V
    .locals 0

    .line 1105
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$PasswordFilter;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106
    iput p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$PasswordFilter;->filterType:I

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 1

    .line 1123
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, " "

    .line 1124
    invoke-virtual {p2, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 1125
    invoke-static {p2}, Lorg/apache/commons/lang/StringUtils;->deleteWhitespace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 1129
    :cond_0
    :try_start_0
    new-instance p3, Ljava/lang/String;

    const/4 p4, 0x2

    new-array p4, p4, [B

    const/16 p5, -0x3e

    const/4 p6, 0x0

    aput-byte p5, p4, p6

    const/16 p5, -0x5b

    const/4 v0, 0x1

    aput-byte p5, p4, v0

    const-string p5, "UTF-8"

    invoke-direct {p3, p4, p5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    new-instance p4, Ljava/lang/String;

    new-array p5, v0, [B

    const/16 v0, 0x5c

    aput-byte v0, p5, p6

    const-string p6, "UTF-8"

    invoke-direct {p4, p5, p6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p2, p3, p4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 1131
    invoke-virtual {p2}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1135
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "^[a-zA-Z0-9!-/:-@\\[-\\`\\{-\\~]+$"

    invoke-virtual {p2, p3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    return-object p1

    :cond_1
    const-string p1, "in NG"

    .line 1138
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const-string p1, ""

    return-object p1
.end method

.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->waitingCommunicationDaialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 492
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 495
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-boolean v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isStopSetUpProgress:Z

    if-eqz v0, :cond_0

    const-string v0, "\u2606\u2606runnableSetUpProgress retuen "

    .line 496
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    return-void

    .line 501
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSequence "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v1

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 502
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v1, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    .line 503
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    const/16 v1, 0xd2

    if-le v0, v1, :cond_2

    .line 504
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 505
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 506
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iput-object v1, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    :cond_1
    const-string v0, "TIME OUT"

    .line 508
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 509
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;->call(Ljava/lang/Object;)V

    .line 510
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$602(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    return-void

    .line 515
    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    const/16 v2, 0x5a

    if-gt v0, v2, :cond_3

    .line 517
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    goto :goto_1

    .line 520
    :cond_3
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    if-le v0, v1, :cond_4

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v1, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    :goto_0
    sub-int/2addr v1, v2

    int-to-double v0, v1

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    div-double/2addr v0, v2

    const-wide v2, 0x4056800000000000L    # 90.0

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 524
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v2, v2, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "::"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 525
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v1, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$1200(Lcom/epson/mobilephone/common/ble/BleWorkActivity;I)V

    .line 527
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-boolean v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isStopSetUpProgress:Z

    if-nez v0, :cond_5

    .line 530
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$1300(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_5
    return-void
.end method

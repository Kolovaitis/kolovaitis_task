.class public Lcom/epson/mobilephone/common/ble/BleAsyncTask;
.super Landroid/os/AsyncTask;
.source "BleAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static dialog:Landroid/app/AlertDialog; = null

.field private static handlerThread:Landroid/os/HandlerThread; = null

.field private static mStop:Z = false

.field private static mTask:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

.field private static mTimeOutTask:Ljava/lang/Runnable;


# instance fields
.field private mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

.field private mCallBack:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

.field private mDevicelist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mFind:Ljava/lang/Boolean;

.field private mTimeOut:J

.field private mTimeOutScan:J

.field private m_Activity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 4

    .line 66
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const-wide/16 v0, -0x1

    .line 30
    iput-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOut:J

    .line 31
    iput-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutScan:J

    const/4 v2, 0x0

    .line 33
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mFind:Ljava/lang/Boolean;

    const/4 v2, 0x0

    .line 34
    iput-object v2, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

    .line 37
    iput-object v2, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mDevicelist:Ljava/util/List;

    .line 41
    iput-object v2, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mCallBack:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 92
    new-instance v2, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;-><init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V

    sput-object v2, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutTask:Ljava/lang/Runnable;

    .line 69
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->m_Activity:Landroid/app/Activity;

    .line 71
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleWork;->getScanAdvertisingPacketTimeout()I

    move-result p1

    add-int/lit16 p1, p1, 0x7d0

    int-to-long v2, p1

    iput-wide v2, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOut:J

    .line 72
    iput-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutScan:J

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;I)V
    .locals 2

    .line 76
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const-wide/16 v0, -0x1

    .line 30
    iput-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOut:J

    .line 31
    iput-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutScan:J

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mFind:Ljava/lang/Boolean;

    const/4 v0, 0x0

    .line 34
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

    .line 37
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mDevicelist:Ljava/util/List;

    .line 41
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mCallBack:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 92
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;-><init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutTask:Ljava/lang/Runnable;

    .line 78
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->m_Activity:Landroid/app/Activity;

    add-int/lit16 p1, p3, 0x7d0

    int-to-long v0, p1

    .line 80
    iput-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOut:J

    int-to-long v0, p3

    .line 81
    iput-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutScan:J

    .line 82
    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mCallBack:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)J
    .locals 2

    .line 25
    iget-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOut:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Lcom/epson/mobilephone/common/ble/BleScanWork;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

    return-object p0
.end method

.method static synthetic access$200()Lcom/epson/mobilephone/common/ble/BleAsyncTask;
    .locals 1

    .line 25
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTask:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    return-object v0
.end method

.method static synthetic access$300(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Ljava/util/List;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mDevicelist:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$302(Lcom/epson/mobilephone/common/ble/BleAsyncTask;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mDevicelist:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Ljava/lang/Boolean;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mFind:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$402(Lcom/epson/mobilephone/common/ble/BleAsyncTask;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mFind:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$500(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->showAdvanceToSettingDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Landroid/app/Activity;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->m_Activity:Landroid/app/Activity;

    return-object p0
.end method

.method static synthetic access$700(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mCallBack:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-object p0
.end method

.method static synthetic access$800()Landroid/os/HandlerThread;
    .locals 1

    .line 25
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method public static blePrinterCheck(Landroid/content/Context;)V
    .locals 4

    const/4 v0, 0x0

    .line 288
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->setStop(Z)V

    .line 289
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 292
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 296
    :cond_0
    check-cast p0, Landroid/app/Activity;

    const-string v0, "bleCheck"

    .line 297
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->isThreadAlive(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, "isAlive bleCheck"

    .line 298
    invoke-static {p0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    return-void

    .line 303
    :cond_1
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    const-string p0, "handlerThread != null   "

    .line 304
    invoke-static {p0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v0, "\u2605\u3000\u3000\u3000\u3000\u3000handlerThread\u3000start   "

    .line 308
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 310
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "bleCheck"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    .line 311
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 313
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/epson/mobilephone/common/ble/BleAsyncTask$6;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$6;-><init>(Landroid/app/Activity;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static blePrinterCheck(Landroid/content/Context;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V
    .locals 2

    const/4 v0, 0x0

    .line 327
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->setStop(Z)V

    .line 329
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 330
    check-cast p0, Landroid/app/Activity;

    const-string v0, "bleCheck"

    .line 332
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->isThreadAlive(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "\u3000\u3000\u3000\u203c\u3000isAlive bleCheck"

    .line 333
    invoke-static {p0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "handlerThread"

    .line 338
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 340
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "bleCheck"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    .line 341
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 343
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/epson/mobilephone/common/ble/BleAsyncTask$7;

    invoke-direct {v1, p0, p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$7;-><init>(Landroid/app/Activity;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V

    const-wide/16 p0, 0x3e8

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static blePrinterCheckStop()V
    .locals 3

    .line 356
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 357
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 358
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->setStop(Z)V

    .line 359
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    const/4 v0, 0x0

    .line 360
    sput-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->handlerThread:Landroid/os/HandlerThread;

    .line 366
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    const-string v0, "bleCheck"

    .line 370
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->getThread(Ljava/lang/String;)Ljava/lang/Thread;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 372
    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "interrupt"

    .line 373
    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 374
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :try_start_0
    const-string v1, "join"

    .line 376
    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const-wide/16 v1, 0x32

    .line 377
    invoke-virtual {v0, v1, v2}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 379
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    return-void

    :cond_2
    return-void

    :cond_3
    const-string v0, "handlerThread NULL return !!"

    .line 362
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    return-void
.end method

.method public static setStop(Z)V
    .locals 2

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mStop   "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 45
    sput-boolean p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mStop:Z

    if-eqz p0, :cond_0

    const-string p0, "\u2605 stopScan"

    .line 47
    invoke-static {p0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 48
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleScanWork;->getInstace()Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object p0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleScanWork;->stopScan()V

    .line 49
    sget-object p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutTask:Ljava/lang/Runnable;

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    .line 50
    sput-object p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutTask:Ljava/lang/Runnable;

    .line 51
    sget-object p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTask:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    .line 52
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method private showAdvanceToSettingDialog()V
    .locals 7

    .line 210
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mCallBack:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleScanWork;->getScannedDeviceList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mDevicelist:Ljava/util/List;

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 215
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mDevicelist:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    .line 216
    new-instance v3, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getRssi()I

    move-result v5

    .line 217
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getHardwareType()[B

    move-result-object v2

    invoke-direct {v3, v4, v5, v6, v2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;-><init>(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)V

    .line 216
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 220
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " \u2605\u3000mDevicelist "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 221
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mCallBack:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    invoke-interface {v1, v0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    goto :goto_1

    .line 223
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->m_Activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 224
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->m_Activity:Landroid/app/Activity;

    const v2, 0x7f0e0015

    .line 225
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->m_Activity:Landroid/app/Activity;

    const v2, 0x7f0e0014

    .line 226
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;-><init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->m_Activity:Landroid/app/Activity;

    const v2, 0x7f0e0013

    .line 249
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/mobilephone/common/ble/BleAsyncTask$4;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$4;-><init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->dialog:Landroid/app/AlertDialog;

    .line 261
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :goto_1
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .line 123
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mStop  "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mStop:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 125
    sget-boolean p1, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mStop:Z

    if-eqz p1, :cond_0

    .line 126
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mFind:Ljava/lang/Boolean;

    return-object p1

    .line 128
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    const-string v0, "BleAsyncTask"

    invoke-virtual {p1, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 129
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->d()V

    .line 130
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 132
    sput-object p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTask:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    .line 134
    iget-wide v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOut:J

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_1

    .line 135
    new-instance p1, Ljava/lang/Thread;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOutTask:Ljava/lang/Runnable;

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    .line 138
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->m_Activity:Landroid/app/Activity;

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/ble/BleScanWork;->init_scan(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "BLE is not available !!"

    .line 139
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;)V

    .line 140
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mFind:Ljava/lang/Boolean;

    return-object p1

    :cond_2
    const-string p1, "BLE is available!!"

    .line 142
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 145
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

    new-instance v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;-><init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V

    new-instance v1, Lcom/epson/mobilephone/common/ble/BleAsyncTask$3;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$3;-><init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V

    invoke-virtual {p1, v0, v1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->search(Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V

    .line 177
    :cond_3
    sget-boolean p1, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mStop:Z

    if-nez p1, :cond_5

    .line 178
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u751f\u304d\u3066\u308b\u3093\u3067\u3059. mStop = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mStop:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const-wide/16 v0, 0x7d0

    .line 180
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, "InterruptedException"

    .line 182
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 185
    :goto_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->isCancelled()Z

    move-result p1

    if-nez p1, :cond_4

    sget-boolean p1, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mStop:Z

    if-eqz p1, :cond_3

    :cond_4
    const-string p1, "\u6b7b\u306b\u307e\u3059"

    .line 186
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const-string p1, "\u2605 stopScan"

    .line 187
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 188
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->stopScan()V

    .line 194
    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u898b\u3064\u304b\u3063\u305f\uff1f "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mFind:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 196
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mFind:Ljava/lang/Boolean;

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled()V
    .locals 0

    .line 274
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Boolean;)V
    .locals 2

    .line 267
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u6b7b\u306b\u307e\u3057\u305f\u3002  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->onCancelled(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    .line 201
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 117
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 118
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleScanWork;->getInstace()Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mBleWork:Lcom/epson/mobilephone/common/ble/BleScanWork;

    return-void
.end method

.method public setTimeOut(J)Lcom/epson/mobilephone/common/ble/BleAsyncTask;
    .locals 0

    .line 283
    iput-wide p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->mTimeOut:J

    return-object p0
.end method

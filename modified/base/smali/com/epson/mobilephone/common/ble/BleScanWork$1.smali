.class Lcom/epson/mobilephone/common/ble/BleScanWork$1;
.super Landroid/bluetooth/le/ScanCallback;
.source "BleScanWork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleScanWork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleScanWork;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-direct {p0}, Landroid/bluetooth/le/ScanCallback;-><init>()V

    return-void
.end method

.method private getDeviceName(Landroid/bluetooth/le/ScanResult;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/RequiresApi;
        api = 0x15
    .end annotation

    .line 141
    invoke-virtual {p1}, Landroid/bluetooth/le/ScanResult;->getScanRecord()Landroid/bluetooth/le/ScanRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p1}, Landroid/bluetooth/le/ScanResult;->getScanRecord()Landroid/bluetooth/le/ScanRecord;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/le/ScanRecord;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 144
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 145
    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/le/ScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public onScanFailed(I)V
    .locals 2

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onScanFailed "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    return-void
.end method

.method public onScanResult(ILandroid/bluetooth/le/ScanResult;)V
    .locals 5
    .param p2    # Landroid/bluetooth/le/ScanResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/RequiresApi;
        api = 0x15
    .end annotation

    .line 85
    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getScanRecord()Landroid/bluetooth/le/ScanRecord;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "scanRecord is NULL !!"

    .line 87
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    return-void

    .line 91
    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/le/ScanRecord;->getManufacturerSpecificData()Landroid/util/SparseArray;

    move-result-object p1

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 96
    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 97
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    const/16 v4, 0x40

    if-ne v3, v4, :cond_2

    .line 98
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    .line 99
    array-length v2, p1

    :goto_1
    if-ge v1, v2, :cond_1

    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    .line 100
    invoke-static {v3}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->IntToHex2(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 106
    :cond_1
    invoke-direct {p0, p2}, Lcom/epson/mobilephone/common/ble/BleScanWork$1;->getDeviceName(Landroid/bluetooth/le/ScanResult;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 109
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 110
    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getRssi()I

    move-result v1

    const/16 v2, -0x4b

    if-lt v1, v2, :cond_3

    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getRssi()I

    move-result v1

    const/16 v2, 0x7f

    if-ge v1, v2, :cond_3

    .line 112
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getRssi()I

    move-result p2

    invoke-virtual {v1, v2, p2, v0, p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->update(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 113
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$200(Lcom/epson/mobilephone/common/ble/BleScanWork;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_3

    .line 115
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$300(Lcom/epson/mobilephone/common/ble/BleScanWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 116
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "mScannedDeviceList --- "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$200(Lcom/epson/mobilephone/common/ble/BleScanWork;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 117
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$300(Lcom/epson/mobilephone/common/ble/BleScanWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$200(Lcom/epson/mobilephone/common/ble/BleScanWork;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_3
    :goto_2
    return-void
.end method

.class Lcom/epson/mobilephone/common/ble/BleWork$4;
.super Ljava/lang/Object;
.source "BleWork.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWork;->gattWriteCharacter(Landroid/bluetooth/BluetoothGattCharacteristic;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWork;

.field final synthetic val$characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field final synthetic val$count:[I


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWork;Landroid/bluetooth/BluetoothGattCharacteristic;[I)V
    .locals 0

    .line 1178
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->val$characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    iput-object p3, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->val$count:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1181
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "mConnGatt  NULL !"

    .line 1182
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1185
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->val$characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-nez v0, :cond_1

    const-string v0, "characteristic  NULL !"

    .line 1186
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1189
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGatt;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->val$characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1190
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->val$count:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    const/4 v0, 0x3

    if-gt v2, v0, :cond_2

    .line 1191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "writeCharacteristic error ! retry "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->val$count:[I

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1192
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerWrite:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1193
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerWrite:Landroid/os/Handler;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->val$count:[I

    aget v1, v2, v1

    mul-int/lit16 v1, v1, 0x1f4

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    const-string v0, "writeCharacteristic error !! retry failed !!"

    .line 1195
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1196
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$4;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "mConnGatt NULL !"

    .line 1200
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void
.end method

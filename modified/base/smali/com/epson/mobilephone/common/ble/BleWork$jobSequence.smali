.class public final enum Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;
.super Ljava/lang/Enum;
.source "BleWork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleWork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "jobSequence"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum AFTER_SETTING_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum BEFORE_CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum COMPLETION:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum DISCONNECTED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum FAILED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum GET_VERSION:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum INITIALIZE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_GET_MACADDRESS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_GET_SSID_LIST:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_SEC_LOGIN:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_SEC_LOGOUT:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_SEC_SET_PASS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_SEC_SKIP:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_SET_SECURITY_TYPE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum MIB_SET_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum SET_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field public static final enum UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 139
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "UNINITIALIZED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 141
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "DISCONNECTED"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->DISCONNECTED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 143
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "FAILED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->FAILED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 144
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "INITIALIZE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->INITIALIZE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 145
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "GET_VERSION"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->GET_VERSION:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 146
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "CHECK_SSID"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 147
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "SET_PASSWORD"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->SET_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 148
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_GET_MACADDRESS"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_MACADDRESS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 149
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_GET_SSID_LIST"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_SSID_LIST:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 150
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_SET_SSID"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 151
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_SET_SECURITY_TYPE"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SECURITY_TYPE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 152
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_SEC_LOGIN"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_LOGIN:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 153
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_SEC_SET_PASS"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_SET_PASS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 154
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_SEC_LOGOUT"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_LOGOUT:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 155
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_REBOOT_NW"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 156
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "COMPLETION"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->COMPLETION:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 157
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "MIB_SEC_SKIP"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_SKIP:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 158
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "AFTER_SETTING_PASSWORD"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->AFTER_SETTING_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 159
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const-string v1, "BEFORE_CHECK_SSID"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->BEFORE_CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const/16 v0, 0x13

    .line 136
    new-array v0, v0, [Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->DISCONNECTED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v3

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->FAILED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v4

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->INITIALIZE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v5

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->GET_VERSION:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v6

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v7

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->SET_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v8

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_MACADDRESS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v9

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_SSID_LIST:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v10

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v11

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SECURITY_TYPE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v12

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_LOGIN:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v13

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_SET_PASS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    aput-object v1, v0, v14

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_LOGOUT:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->COMPLETION:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_SKIP:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->AFTER_SETTING_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->BEFORE_CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->$VALUES:[Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;
    .locals 1

    .line 136
    const-class v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    return-object p0
.end method

.method public static values()[Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;
    .locals 1

    .line 136
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->$VALUES:[Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0}, [Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    return-object v0
.end method

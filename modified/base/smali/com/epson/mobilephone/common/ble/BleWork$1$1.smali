.class Lcom/epson/mobilephone/common/ble/BleWork$1$1;
.super Ljava/lang/Object;
.source "BleWork.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWork$1;->onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/ble/BleWork$1;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWork$1;)V
    .locals 0

    .line 254
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWork$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    const-string v0, "discoverServices"

    .line 257
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 259
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWork$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "discoverServices error"

    .line 260
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "mConnGatt NULL !"

    .line 263
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void
.end method

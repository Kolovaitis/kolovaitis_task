.class Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread$1;
.super Ljava/lang/Object;
.source "BleWork.java"

# interfaces
.implements Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->onPreExecute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;)V
    .locals 0

    .line 882
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, " -- NotifySetThread call"

    .line 885
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 886
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0007-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 887
    sget-object v1, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v2, "802a0004-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 888
    check-cast p1, Landroid/bluetooth/BluetoothGattDescriptor;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattDescriptor;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 889
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2600(Lcom/epson/mobilephone/common/ble/BleWork;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    goto :goto_0

    .line 890
    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattDescriptor;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p1

    const/4 v0, 0x1

    if-ne v1, p1, :cond_1

    .line 891
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;

    iput-boolean v0, p1, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->stop:Z

    .line 892
    iput-boolean v0, p1, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->success:Z

    goto :goto_0

    .line 895
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;

    iput-boolean v0, p1, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->stop:Z

    :goto_0
    return-void
.end method

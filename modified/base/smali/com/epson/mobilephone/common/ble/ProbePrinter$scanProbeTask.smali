.class Lcom/epson/mobilephone/common/ble/ProbePrinter$scanProbeTask;
.super Landroid/os/AsyncTask;
.source "ProbePrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/ProbePrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "scanProbeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Landroid/content/Context;",
        "Ljava/lang/Void;",
        "Landroid/content/Context;",
        ">;"
    }
.end annotation


# instance fields
.field private mPrinter:Lepson/print/MyPrinter;

.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;Lepson/print/MyPrinter;)V
    .locals 0

    .line 422
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$scanProbeTask;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    .line 423
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 424
    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$scanProbeTask;->mPrinter:Lepson/print/MyPrinter;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/content/Context;)Landroid/content/Context;
    .locals 5

    const/4 v0, 0x0

    .line 436
    aget-object v1, p1, v0

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "\u30b9\u30ad\u30e3\u30ca\u691c\u7d22\u51e6\u7406"

    .line 437
    invoke-static {v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 439
    invoke-static {}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$700()Lepson/scan/lib/escanLib;

    move-result-object v2

    invoke-virtual {v2, v1}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    move-result v2

    const/16 v3, -0x41a

    if-eq v2, v3, :cond_1

    if-eqz v2, :cond_0

    const-string v1, "Scan \u6a5f\u80fd\u4e0d\u660e"

    .line 449
    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 450
    aget-object p1, p1, v0

    return-object p1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    .line 454
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$700()Lepson/scan/lib/escanLib;

    move-result-object v3

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$scanProbeTask;->mPrinter:Lepson/print/MyPrinter;

    invoke-static {v3, v1, v4}, Lepson/scan/lib/ScanSettingHelper;->recodeScannerInfo(Lepson/scan/lib/escanLib;Landroid/content/Context;Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;

    const-string v1, "Scan \u6a5f\u80fd\u3042\u308a"

    .line 455
    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Lepson/scan/lib/EscanLibException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    .line 462
    invoke-static {}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$700()Lepson/scan/lib/escanLib;

    move-result-object v1

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :cond_2
    const-string v1, "YYY"

    .line 466
    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 467
    aget-object p1, p1, v0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    :try_start_1
    const-string v1, "Scan \u6a5f\u80fd\u306a\u3057"

    .line 457
    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 458
    aget-object p1, p1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_3

    .line 462
    invoke-static {}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$700()Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :cond_3
    return-object p1

    :goto_1
    if-nez v2, :cond_4

    invoke-static {}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$700()Lepson/scan/lib/escanLib;

    move-result-object v0

    invoke-virtual {v0}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :cond_4
    throw p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 415
    check-cast p1, [Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter$scanProbeTask;->doInBackground([Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/content/Context;)V
    .locals 1

    .line 472
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    const-string p1, "\u30b9\u30ad\u30e3\u30ca\u691c\u7d22onPostExecute"

    .line 473
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 475
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$scanProbeTask;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->callbackCompletion(Z)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 415
    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter$scanProbeTask;->onPostExecute(Landroid/content/Context;)V

    return-void
.end method

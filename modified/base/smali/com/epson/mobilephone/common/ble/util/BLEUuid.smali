.class public Lcom/epson/mobilephone/common/ble/util/BLEUuid;
.super Ljava/lang/Object;
.source "BLEUuid.java"


# static fields
.field public static final AD_UUID:Ljava/lang/String; = "802A0000-4EF4-4E59-B573-2B2A00004EF4"

.field public static final CHAR_ALERT_LEVEL:Ljava/lang/String; = "00002a06-0000-1000-8000-00805f9b34fb"

.field public static final CHAR_DEVICE_NAME_STRING:Ljava/lang/String; = "00002a00-0000-1000-8000-00805f9b34fb"

.field public static final CHAR_MANUFACTURER_NAME_STRING:Ljava/lang/String; = "00002a29-0000-1000-8000-00805f9b34fb"

.field public static final CHAR_MODEL_NUMBER_STRING:Ljava/lang/String; = "00002a24-0000-1000-8000-00805f9b34fb"

.field public static final CHAR_PASSWORD_STRING:Ljava/lang/String; = "802a0001-4ef4-4e59-b573-2bed4a4ac158"

.field public static final CHAR_SECURITY_TYPE_STRING:Ljava/lang/String; = "802a0007-4ef4-4e59-b573-2bed4a4ac158"

.field public static final CHAR_SERIAL_NUMBEAR_STRING:Ljava/lang/String; = "00002a25-0000-1000-8000-00805f9b34fb"

.field public static final CHAR_SNMP_REQUEST_STRING:Ljava/lang/String; = "802a0003-4ef4-4e59-b573-2bed4a4ac158"

.field public static final CHAR_SNMP_RESPONSE_STRING:Ljava/lang/String; = "802a0005-4ef4-4e59-b573-2bed4a4ac158"

.field public static final CHAR_SNMP_STATUS_STRING:Ljava/lang/String; = "802a0004-4ef4-4e59-b573-2bed4a4ac158"

.field public static final CHAR_SSID_STRING:Ljava/lang/String; = "802a0006-4ef4-4e59-b573-2bed4a4ac158"

.field public static final CHAR_VERSION_STRING:Ljava/lang/String; = "802a0002-4ef4-4e59-b573-2bed4a4ac158"

.field public static final CLIENT_CHARACTERISTIC_CONFIG:Ljava/lang/String; = "00002902-0000-1000-8000-00805f9b34fb"

.field public static GattCharsMap:Ljava/util/Map; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;"
        }
    .end annotation
.end field

.field public static final SERVICE_DEVICE_INFORMATION:Ljava/lang/String; = "0000180a-0000-1000-8000-00805f9b34fb"

.field public static final SERVICE_IMMEDIATE_ALERT:Ljava/lang/String; = "00001802-0000-1000-8000-00805f9b34fb"

.field public static final SERVICE_SNMP:Ljava/lang/String; = "802a0000-4ef4-4e59-b573-2bed4a4ac158"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    .line 43
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0001-4ef4-4e59-b573-2bed4a4ac158"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0002-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0003-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0004-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0005-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0006-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0007-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

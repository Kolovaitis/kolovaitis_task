.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setConnectTimeOut()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 626
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " timeout ------------"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v1

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 630
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->AFTER_SETTING_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    .line 631
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_MACADDRESS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    .line 632
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->BEFORE_CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    .line 633
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne v0, v1, :cond_1

    .line 635
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    .line 637
    :cond_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->setRetryFinish()V

    return-void
.end method

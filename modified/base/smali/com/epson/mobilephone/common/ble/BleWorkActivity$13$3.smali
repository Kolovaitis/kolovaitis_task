.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

.field final synthetic val$editView:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;Landroid/widget/EditText;)V
    .locals 0

    .line 1034
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->val$editView:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1036
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 1038
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const-string p2, "input_method"

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    .line 1039
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->val$editView:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object p2

    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1042
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->getSsidLis()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 1043
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_SSID_LIST:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object p2, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 1044
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWork;->setSecurityType(S)V

    .line 1045
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$900(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    goto :goto_0

    .line 1047
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->disconnect()V

    :goto_0
    return-void
.end method

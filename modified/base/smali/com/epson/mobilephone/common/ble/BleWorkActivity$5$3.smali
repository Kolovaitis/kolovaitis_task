.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;)V
    .locals 0

    .line 416
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 420
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lepson/maintain/activity/PrinterNotFoundDialogCreator;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->startActivity(Landroid/content/Intent;)V

    .line 421
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->Button:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->Home:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    .line 423
    :goto_0
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p2

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->getDeviceName()Ljava/lang/String;

    move-result-object p2

    sget-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Fail:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    invoke-static {p2, p1, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendSetup(Ljava/lang/String;Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;)V

    .line 424
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->finish()V

    return-void
.end method

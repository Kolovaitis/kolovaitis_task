.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->showSSIDList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 735
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 737
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v1, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 739
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/ble/BleWork;->disconnect(Z)V

    .line 740
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sequence = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v2

    iget-object v2, v2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 742
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSsidLis()Ljava/util/List;

    move-result-object v0

    .line 745
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 747
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$1700(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Ljava/util/List;)V

    .line 750
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 752
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 754
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 757
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0x19

    const/16 v5, 0x28

    .line 758
    invoke-virtual {v3, v5, v4, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 760
    new-instance v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 761
    iget-object v5, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v5}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e001d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v5, 0x41a00000    # 20.0f

    .line 762
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextSize(F)V

    .line 763
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 765
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 767
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 768
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 770
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 772
    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;

    invoke-direct {v2, p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;[Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 801
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e0021

    invoke-virtual {v0, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$2;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$2;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 808
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

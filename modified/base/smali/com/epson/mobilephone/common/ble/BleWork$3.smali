.class Lcom/epson/mobilephone/common/ble/BleWork$3;
.super Ljava/lang/Object;
.source "BleWork.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWork;->characteristicRead()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWork;

.field final synthetic val$retry:[I


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWork;[I)V
    .locals 0

    .line 965
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->val$retry:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 969
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 979
    :pswitch_0
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0002-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 980
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothGatt;->readCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "FALSE !! "

    .line 981
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 972
    :pswitch_1
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0001-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 973
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothGatt;->readCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "FALSE !! "

    .line 974
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 994
    :pswitch_2
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0005-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 996
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothGatt;->readCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 997
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->val$retry:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    const/4 v0, 0x3

    if-ge v2, v0, :cond_0

    .line 998
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FALSE !! retry "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->val$retry:[I

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 999
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerRead:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1000
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerRead:Landroid/os/Handler;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->val$retry:[I

    aget v1, v2, v1

    mul-int/lit16 v1, v1, 0x1f4

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    const-string v0, "FALSE !!!"

    .line 1002
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1003
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$200(Lcom/epson/mobilephone/common/ble/BleWork;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "mConnGatt NULL !"

    .line 1007
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.class Lcom/epson/mobilephone/common/ble/ProbePrinter$3;
.super Ljava/lang/Object;
.source "ProbePrinter.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/ProbePrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;)V
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$3;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    .line 262
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    const/16 v1, 0x11

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    .line 290
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "not handled ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 274
    :pswitch_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$3;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->interruptSearch()V

    goto :goto_0

    .line 268
    :pswitch_1
    :try_start_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$3;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->search()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 270
    invoke-virtual {p1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$3;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->bindEpsonService()V

    goto :goto_0

    .line 278
    :cond_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    .line 281
    new-instance v0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$3;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$500(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v1, v3, p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;-><init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;Landroid/content/Context;Lepson/print/MyPrinter;)V

    new-array p1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

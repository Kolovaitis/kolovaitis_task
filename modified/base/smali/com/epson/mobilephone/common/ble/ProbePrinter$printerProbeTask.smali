.class Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;
.super Landroid/os/AsyncTask;
.source "ProbePrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/ProbePrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "printerProbeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMyPrinter:Lepson/print/MyPrinter;

.field private mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;


# direct methods
.method public constructor <init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;Landroid/content/Context;Lepson/print/MyPrinter;)V
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    .line 322
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 316
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 323
    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mContext:Landroid/content/Context;

    .line 324
    iput-object p3, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mMyPrinter:Lepson/print/MyPrinter;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4

    .line 337
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mMyPrinter:Lepson/print/MyPrinter;

    .line 338
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mMyPrinter:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3c

    const/4 v3, 0x3

    .line 337
    invoke-virtual {p1, v2, v0, v1, v3}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-eqz p1, :cond_0

    .line 341
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 345
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setMSearchPos(I)V

    .line 348
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result p1

    if-eqz p1, :cond_1

    .line 350
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 354
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mMyPrinter:Lepson/print/MyPrinter;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetLang()I

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/MyPrinter;->setLang(I)V

    .line 357
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    .line 360
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 361
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mContext:Landroid/content/Context;

    .line 362
    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    .line 361
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetSupportedMedia(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 364
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 368
    :cond_2
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v1

    .line 369
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v2

    .line 371
    :try_start_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    const-string v1, "Success epsWrapperGetSupportedMedia"

    .line 372
    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 374
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 379
    :goto_0
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 380
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSavedAreaInfo()Ljava/io/File;

    move-result-object p1

    .line 382
    :try_start_1
    invoke-static {v1, p1}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    const-string p1, "Success copy AreaInfo"

    .line 383
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 385
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 389
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 315
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1

    .line 395
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 397
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_0

    .line 399
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->mMyPrinter:Lepson/print/MyPrinter;

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$600(Lcom/epson/mobilephone/common/ble/ProbePrinter;Lepson/print/MyPrinter;)V

    .line 401
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->callbackCompletion(Z)V

    goto :goto_0

    .line 404
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->callbackCompletion(Z)V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 315
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .line 329
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    return-void
.end method

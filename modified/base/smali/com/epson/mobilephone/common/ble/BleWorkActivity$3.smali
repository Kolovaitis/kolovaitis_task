.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->callBackFuncs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 2

    .line 308
    check-cast p1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSequenceCallbackSuccess  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 312
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->BEFORE_CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_MACADDRESS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->AFTER_SETTING_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, v0, :cond_1

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$700(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    .line 323
    :cond_1
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p1, v0, :cond_3

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->BEFORE_CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    .line 325
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSSIDPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 328
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSecurityType()S

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    .line 329
    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSecurityType()S

    move-result v0

    const/16 v1, 0xff

    if-eq v0, v1, :cond_3

    .line 330
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 331
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mAlertDialog        dismiss "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 336
    :cond_3
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, v0, :cond_4

    .line 337
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$800(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    goto :goto_0

    .line 338
    :cond_4
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_SSID_LIST:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, v0, :cond_5

    .line 339
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$900(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    :cond_5
    :goto_0
    return-void
.end method

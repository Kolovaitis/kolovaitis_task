.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->settingResultDailog(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Z)V
    .locals 0

    .line 383
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iput-boolean p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->val$success:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 386
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$1100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 388
    iget-boolean v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->val$success:Z

    if-eqz v1, :cond_0

    .line 389
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e0025

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 390
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e0024

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 391
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e03ff

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$1;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 412
    :cond_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e0012

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 413
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e0010

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 415
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e0011

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$3;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v3, 0x7f0e0473

    .line 426
    invoke-virtual {v2, v3}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$2;

    invoke-direct {v3, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5$2;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 438
    :goto_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 439
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$1;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

.field final synthetic val$security:I


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;I)V
    .locals 0

    .line 991
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iput p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$1;->val$security:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 1002
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "afterTextChanged "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1004
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$1;->val$security:I

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->validateInputText(ILjava/lang/String;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

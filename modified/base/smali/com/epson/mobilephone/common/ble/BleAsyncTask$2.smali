.class Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;
.super Ljava/lang/Object;
.source "BleAsyncTask.java"

# interfaces
.implements Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    check-cast p1, Ljava/util/List;

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$302(Lcom/epson/mobilephone/common/ble/BleAsyncTask;Ljava/util/List;)Ljava/util/List;

    .line 150
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u767a\u898b  "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$300(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 151
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$300(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 155
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$400(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_1

    .line 156
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$600(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Landroid/app/Activity;

    move-result-object p1

    new-instance v0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2$1;-><init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 165
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$700(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-nez p1, :cond_2

    .line 166
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$2;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$402(Lcom/epson/mobilephone/common/ble/BleAsyncTask;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    :cond_2
    return-void
.end method

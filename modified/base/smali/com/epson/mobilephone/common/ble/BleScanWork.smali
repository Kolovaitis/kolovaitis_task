.class public Lcom/epson/mobilephone/common/ble/BleScanWork;
.super Ljava/lang/Object;
.source "BleScanWork.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/ble/BleScanWork$SingletonHolder;
    }
.end annotation


# static fields
.field private static final COMPANY_IDENTIFIER_EPSON:I = 0x40


# instance fields
.field private mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothLeScanner:Landroid/bluetooth/le/BluetoothLeScanner;

.field private mFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private mIsFiltering:Z

.field private mIsScanning:Z

.field private mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private mScanCallback:Landroid/bluetooth/le/ScanCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private mScannedDeviceList:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 46
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 48
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 51
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScannedDeviceList:Ljava/util/List;

    .line 80
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 81
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleScanWork$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleScanWork$1;-><init>(Lcom/epson/mobilephone/common/ble/BleScanWork;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScanCallback:Landroid/bluetooth/le/ScanCallback;

    .line 170
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleScanWork$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleScanWork$2;-><init>(Lcom/epson/mobilephone/common/ble/BleScanWork;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/mobilephone/common/ble/BleScanWork$1;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleScanWork;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/mobilephone/common/ble/BleScanWork;)Ljava/util/List;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScannedDeviceList:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$300(Lcom/epson/mobilephone/common/ble/BleScanWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-object p0
.end method

.method static synthetic access$400(Lcom/epson/mobilephone/common/ble/BleScanWork;)Z
    .locals 0

    .line 34
    iget-boolean p0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mIsFiltering:Z

    return p0
.end method

.method public static getInstace()Lcom/epson/mobilephone/common/ble/BleScanWork;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 57
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleScanWork$SingletonHolder;->access$000()Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getScannedDeviceList()Ljava/util/ArrayList;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
            ">;"
        }
    .end annotation

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScannedDeviceList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public init_scan(Landroid/content/Context;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 238
    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->isBLESupported(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 243
    :cond_0
    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->getManager(Landroid/content/Context;)Landroid/bluetooth/BluetoothManager;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 245
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 247
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez p1, :cond_2

    return v1

    .line 250
    :cond_2
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    .line 254
    :cond_3
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-lt p1, v0, :cond_4

    .line 255
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBluetoothLeScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    .line 256
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBluetoothLeScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    if-nez p1, :cond_4

    return v1

    .line 261
    :cond_4
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleScanWork;->stopScan()V

    const-string p1, "\u2605 stopScan"

    .line 262
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method

.method search(Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 161
    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    const/4 p1, 0x1

    .line 163
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->startScan(Z)V

    return-void
.end method

.method startScan(Z)V
    .locals 3

    const-string v0, "\u2606startScan\u2606"

    .line 274
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScannedDeviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 277
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mIsFiltering:Z

    .line 279
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mIsScanning:Z

    if-nez p1, :cond_2

    .line 281
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-ge p1, v0, :cond_0

    .line 283
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    goto :goto_1

    .line 284
    :cond_0
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p1, v0, :cond_2

    .line 287
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 289
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mIsFiltering:Z

    const/4 v1, 0x2

    if-eqz v0, :cond_1

    .line 291
    new-instance v0, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v0}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    const-string v2, "802a0000-4ef4-4e59-b573-2bed4a4ac158"

    invoke-static {v2}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/bluetooth/le/ScanFilter$Builder;->setServiceUuid(Landroid/os/ParcelUuid;)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v0

    .line 292
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    new-instance v0, Landroid/bluetooth/le/ScanSettings$Builder;

    invoke-direct {v0}, Landroid/bluetooth/le/ScanSettings$Builder;-><init>()V

    .line 294
    invoke-virtual {v0, v1}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanMode(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v0

    const-wide/16 v1, 0x0

    .line 295
    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/le/ScanSettings$Builder;->setReportDelay(J)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v0

    .line 296
    invoke-virtual {v0}, Landroid/bluetooth/le/ScanSettings$Builder;->build()Landroid/bluetooth/le/ScanSettings;

    move-result-object v0

    goto :goto_0

    .line 301
    :cond_1
    new-instance v0, Landroid/bluetooth/le/ScanSettings$Builder;

    invoke-direct {v0}, Landroid/bluetooth/le/ScanSettings$Builder;-><init>()V

    .line 302
    invoke-virtual {v0, v1}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanMode(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Landroid/bluetooth/le/ScanSettings$Builder;->build()Landroid/bluetooth/le/ScanSettings;

    move-result-object v0

    .line 309
    :goto_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBluetoothLeScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    if-eqz v1, :cond_2

    .line 310
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScanCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v1, p1, v0, v2}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V

    const/4 p1, 0x1

    .line 311
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mIsScanning:Z

    :cond_2
    :goto_1
    return-void
.end method

.method public stopScan()V
    .locals 3

    const-string v0, "stopScan"

    .line 323
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_1

    .line 325
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    .line 326
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    const-string v0, "mLeScanCallback"

    .line 327
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 328
    iput-boolean v2, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mIsScanning:Z

    goto :goto_0

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mBluetoothLeScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    if-eqz v0, :cond_1

    .line 331
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScanCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V

    const-string v0, "mScanCallback"

    .line 332
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 333
    iput-boolean v2, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mIsScanning:Z

    :cond_1
    :goto_0
    return-void
.end method

.method public update(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)Z
    .locals 6
    .param p1    # Landroid/bluetooth/BluetoothDevice;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 215
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 220
    :cond_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScannedDeviceList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    .line 221
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 223
    invoke-virtual {v2, p2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->setRssi(I)V

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_3

    .line 229
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleScanWork;->mScannedDeviceList:Ljava/util/List;

    new-instance v1, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;-><init>(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "\u2606\u2606 = "

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ":"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    return v3

    :cond_3
    return v0

    :cond_4
    :goto_1
    return v0
.end method

.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$1;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;)V
    .locals 0

    .line 873
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 884
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "afterTextChanged "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 886
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const/16 v1, 0x64

    invoke-virtual {v0, v1, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->validateInputText(ILjava/lang/String;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

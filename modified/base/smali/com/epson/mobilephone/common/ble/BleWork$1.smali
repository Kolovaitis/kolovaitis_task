.class Lcom/epson/mobilephone/common/ble/BleWork$1;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "BleWork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleWork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWork;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWork;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    return-void
.end method

.method private IncrementSequence()V
    .locals 3

    .line 607
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v0

    .line 608
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->values()[Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    move-result-object v1

    .line 609
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    iput-object v0, v2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 610
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u0394\u0394\u3000mSequence = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    return-void
.end method

.method private notifySet(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothGattCharacteristic;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 646
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Notification set ERROR"

    .line 647
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_0
    const-string v0, "00002902-0000-1000-8000-00805f9b34fb"

    .line 651
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object p1

    .line 652
    sget-object v0, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_NOTIFICATION_VALUE:[B

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    const-string v0, "notify set"

    .line 653
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 654
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "Notification write ERROR"

    .line 655
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private readCharacteristicData(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 6
    .param p1    # Landroid/bluetooth/BluetoothGattCharacteristic;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 435
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string p1, "characteristic NULL !!"

    .line 438
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 440
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 441
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "802a0002-4ef4-4e59-b573-2bed4a4ac158"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_1
    const-string v1, "00002a25-0000-1000-8000-00805f9b34fb"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "802a0001-4ef4-4e59-b573-2bed4a4ac158"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v1, "00002a29-0000-1000-8000-00805f9b34fb"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_4
    const-string v1, "802a0005-4ef4-4e59-b573-2bed4a4ac158"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, -0x1

    :goto_1
    const/16 v1, 0x12

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_4

    .line 496
    :pswitch_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object p1

    .line 498
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    array-length v1, p1

    iput v1, v0, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->bufSize:I

    const/4 v0, 0x0

    .line 500
    :goto_2
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v1

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    iget v1, v1, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->bufSize:I

    if-ge v0, v1, :cond_2

    .line 501
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v1

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->buf:[B

    aget-byte v5, p1, v0

    aput-byte v5, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 504
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1600(Lcom/epson/mobilephone/common/ble/BleWork;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 505
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1700(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibParseResponseAsStr(Lcom/epson/mobilephone/common/escpr/MIBCommand;)Ljava/lang/String;

    move-result-object p1

    .line 506
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string p1, "[Read]SNMP Response :end!"

    .line 507
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 508
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1, v4}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1602(Lcom/epson/mobilephone/common/ble/BleWork;Z)Z

    .line 509
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1, v4}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1202(Lcom/epson/mobilephone/common/ble/BleWork;B)B

    .line 511
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$800(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-interface {p1, v0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 513
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Read]SNMP Response :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1100(Lcom/epson/mobilephone/common/ble/BleWork;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 516
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->getSSIDList()V

    goto/16 :goto_4

    .line 519
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mSequence   "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 520
    sget-object p1, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v0

    aget p1, p1, v0

    packed-switch p1, :pswitch_data_1

    goto/16 :goto_4

    .line 573
    :pswitch_1
    new-array p1, v3, [I

    aput v2, p1, v4

    .line 574
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1700(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibParseResponseAsInt(Lcom/epson/mobilephone/common/escpr/MIBCommand;[I)I

    move-result v0

    if-nez v0, :cond_7

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Read]SNMP Response :sucess mSequence   "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " val:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget p1, p1, v4

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 577
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_SKIP:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, v0, :cond_5

    .line 578
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_LOGOUT:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v0, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    goto :goto_3

    .line 579
    :cond_5
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, v0, :cond_6

    .line 580
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1, v4}, Lcom/epson/mobilephone/common/ble/BleWork;->access$402(Lcom/epson/mobilephone/common/ble/BleWork;Z)Z

    goto/16 :goto_4

    .line 584
    :cond_6
    :goto_3
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork$1;->IncrementSequence()V

    .line 586
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mSequence   "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 587
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result p1

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v0

    if-gt p1, v0, :cond_e

    .line 588
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1900(Lcom/epson/mobilephone/common/ble/BleWork;)V

    goto/16 :goto_4

    :cond_7
    const-string p1, "[Read]SNMP Response :error "

    .line 592
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 555
    :pswitch_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mibIdentifier   requestId =  "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/mobilephone/common/escpr/MIBCommand;->mibIdentifier:Lcom/epson/mobilephone/common/escpr/MIBIdentifier;

    iget v0, v0, Lcom/epson/mobilephone/common/escpr/MIBIdentifier;->requestId:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 557
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1700(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v0

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2000(Lcom/epson/mobilephone/common/ble/BleWork;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibParseSecResponse(Lcom/epson/mobilephone/common/escpr/MIBCommand;[B)I

    move-result p1

    .line 558
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSequence   "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " ret = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-eqz p1, :cond_8

    const-string p1, "[Read]SNMP Response :end!"

    .line 560
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 562
    :cond_8
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork$1;->IncrementSequence()V

    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Read]SNMP Response :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " mSequence   "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 564
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1900(Lcom/epson/mobilephone/common/ble/BleWork;)V

    goto/16 :goto_4

    .line 538
    :pswitch_3
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1700(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibParseResponseAsStr(Lcom/epson/mobilephone/common/escpr/MIBCommand;)Ljava/lang/String;

    move-result-object p1

    .line 540
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v3

    if-nez v0, :cond_9

    const-string p1, "[Read]SNMP Response :end!"

    .line 542
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 545
    :cond_9
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork$1;->IncrementSequence()V

    .line 546
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Read]SNMP Response :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " mSequence   "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 547
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1900(Lcom/epson/mobilephone/common/ble/BleWork;)V

    goto/16 :goto_4

    .line 523
    :pswitch_4
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1700(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibParseResponseAsMacaddress(Lcom/epson/mobilephone/common/escpr/MIBCommand;)Ljava/lang/String;

    move-result-object p1

    .line 524
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 525
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1802(Lcom/epson/mobilephone/common/ble/BleWork;Ljava/lang/String;)Ljava/lang/String;

    .line 526
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v1, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 528
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Read]SNMP Response :sucess:MacAddress \u2605"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u2605 mSequence "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 529
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1900(Lcom/epson/mobilephone/common/ble/BleWork;)V

    goto/16 :goto_4

    :cond_a
    const-string p1, "[Read]SNMP Response :failed"

    .line 532
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 469
    :pswitch_5
    invoke-virtual {p1, v1, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    .line 470
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p1

    .line 471
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Read]Version "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 475
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1300(Lcom/epson/mobilephone/common/ble/BleWork;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1300(Lcom/epson/mobilephone/common/ble/BleWork;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_e

    .line 476
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork$1;->IncrementSequence()V

    const-string p1, " @@  checkSSID  @@ "

    .line 477
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 478
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object p1

    if-nez p1, :cond_b

    const-string p1, "mConnGatt  NULL !"

    .line 479
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 482
    :cond_b
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->getSecurityType()S

    move-result p1

    const/16 v0, 0xff

    if-ne p1, v0, :cond_c

    const-string p1, " skip  "

    .line 484
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 485
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$800(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-eqz p1, :cond_e

    .line 486
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$800(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-interface {p1, v0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 490
    :cond_c
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1400(Lcom/epson/mobilephone/common/ble/BleWork;)V

    goto :goto_4

    .line 453
    :pswitch_6
    invoke-virtual {p1, v1, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    .line 454
    invoke-virtual {p1}, Ljava/lang/Integer;->shortValue()S

    move-result p1

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p1

    .line 455
    invoke-virtual {p1}, Ljava/lang/Short;->toString()Ljava/lang/String;

    move-result-object v0

    .line 456
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "802a0001-4ef4-4e59-b573-2bed4a4ac158\n[Read]Password "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 458
    invoke-virtual {p1}, Ljava/lang/Short;->shortValue()S

    move-result p1

    const v0, 0xffff

    if-ne p1, v0, :cond_d

    const-string p1, "password error !"

    .line 459
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 460
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->FAILED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v0, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    goto :goto_4

    .line 463
    :cond_d
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork$1;->IncrementSequence()V

    .line 464
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->checkVersion()V

    goto :goto_4

    .line 448
    :pswitch_7
    invoke-virtual {p1, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object p1

    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "00002a29-0000-1000-8000-00805f9b34fb\n[Read]Serial Number "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    goto :goto_4

    .line 443
    :pswitch_8
    invoke-virtual {p1, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object p1

    .line 444
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "00002a29-0000-1000-8000-00805f9b34fb\n[Read]Manufacturer Name "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    :cond_e
    :goto_4
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x37625f33 -> :sswitch_4
        -0x34884074 -> :sswitch_3
        0x235e0ec9 -> :sswitch_2
        0x26382d88 -> :sswitch_1
        0x4cadf34a -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 3
    .param p2    # Landroid/bluetooth/BluetoothGattCharacteristic;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 383
    invoke-super {p0, p1, p2}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    const-string p1, "onCharacteristicChanged"

    .line 384
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const-string p1, "802a0004-4ef4-4e59-b573-2bed4a4ac158"

    .line 386
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x0

    const/16 v1, 0x11

    if-eqz p1, :cond_1

    .line 387
    invoke-virtual {p2, v1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    .line 388
    invoke-virtual {p1}, Ljava/lang/Integer;->shortValue()S

    move-result p1

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p1

    .line 389
    invoke-virtual {p1}, Ljava/lang/Short;->toString()Ljava/lang/String;

    move-result-object p2

    .line 390
    invoke-virtual {p1}, Ljava/lang/Short;->shortValue()S

    move-result p1

    if-nez p1, :cond_0

    .line 391
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "[Notify]SNMP Status"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 394
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1000(Lcom/epson/mobilephone/common/ble/BleWork;)V

    goto/16 :goto_0

    .line 396
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "[Notify]SNMP Status"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 397
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    goto/16 :goto_0

    :cond_1
    const-string p1, "802a0007-4ef4-4e59-b573-2bed4a4ac158"

    .line 399
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 400
    invoke-virtual {p2, v1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    .line 401
    invoke-virtual {p1}, Ljava/lang/Integer;->shortValue()S

    move-result p1

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p1

    .line 402
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-virtual {p2, v0}, Lcom/epson/mobilephone/common/ble/BleWork;->setSecurityType(S)V

    .line 403
    invoke-virtual {p1}, Ljava/lang/Short;->toString()Ljava/lang/String;

    move-result-object p2

    .line 404
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Notify]Security Type\u3000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 405
    invoke-virtual {p1}, Ljava/lang/Short;->shortValue()S

    move-result p1

    if-lez p1, :cond_2

    .line 406
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$800(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 407
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$800(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-interface {p1, p2}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 411
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1100(Lcom/epson/mobilephone/common/ble/BleWork;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 412
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->getSSIDList()V

    goto :goto_0

    .line 416
    :cond_3
    invoke-virtual {p2, v1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    .line 417
    invoke-virtual {p1}, Ljava/lang/Integer;->shortValue()S

    move-result p1

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p1

    .line 418
    invoke-virtual {p1}, Ljava/lang/Short;->toString()Ljava/lang/String;

    move-result-object p1

    .line 419
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "[Notify]unKnown type"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 1
    .param p2    # Landroid/bluetooth/BluetoothGattCharacteristic;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 425
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    const-string v0, "Read"

    invoke-static {p1, v0, p3}, Lcom/epson/mobilephone/common/ble/BleWork;->access$000(Lcom/epson/mobilephone/common/ble/BleWork;Ljava/lang/String;I)V

    if-nez p3, :cond_0

    .line 428
    invoke-direct {p0, p2}, Lcom/epson/mobilephone/common/ble/BleWork$1;->readCharacteristicData(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    goto :goto_0

    .line 430
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1202(Lcom/epson/mobilephone/common/ble/BleWork;B)B

    :goto_0
    return-void
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 0

    .line 615
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    const-string p2, "Write"

    invoke-static {p1, p2, p3}, Lcom/epson/mobilephone/common/ble/BleWork;->access$000(Lcom/epson/mobilephone/common/ble/BleWork;Ljava/lang/String;I)V

    if-nez p3, :cond_1

    .line 618
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->INITIALIZE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, p2, :cond_0

    .line 619
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1000(Lcom/epson/mobilephone/common/ble/BleWork;)V

    goto :goto_0

    .line 620
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, p2, :cond_4

    .line 621
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2100(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 622
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2100(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-interface {p1, p2}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 626
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$1202(Lcom/epson/mobilephone/common/ble/BleWork;B)B

    .line 628
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->INITIALIZE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, p2, :cond_2

    .line 630
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    goto :goto_0

    .line 631
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, p2, :cond_3

    .line 632
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2100(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 633
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2100(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-interface {p1, p2}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 636
    :cond_3
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2200(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 637
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2200(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-interface {p1, p2}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 2

    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "status = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "  newState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-nez p2, :cond_2

    const/4 v0, 0x2

    if-ne p3, v0, :cond_2

    .line 243
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "STATE_CONNECTED  -- "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 244
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iput p3, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    .line 245
    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;

    move-result-object p2

    if-nez p2, :cond_0

    const-string p2, "mConnGatt  NULL !"

    .line 246
    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_0
    if-nez p1, :cond_1

    const-string p2, "gatt  NULL !"

    .line 249
    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 252
    :cond_1
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2, p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$302(Lcom/epson/mobilephone/common/ble/BleWork;Landroid/bluetooth/BluetoothGatt;)Landroid/bluetooth/BluetoothGatt;

    .line 253
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->handlerWrite:Landroid/os/Handler;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 254
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->handlerWrite:Landroid/os/Handler;

    new-instance p2, Lcom/epson/mobilephone/common/ble/BleWork$1$1;

    invoke-direct {p2, p0}, Lcom/epson/mobilephone/common/ble/BleWork$1$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWork$1;)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    :cond_2
    const/4 p1, 0x3

    if-nez p2, :cond_5

    if-nez p3, :cond_3

    goto :goto_0

    :cond_3
    const/4 p2, 0x1

    if-ne p3, p2, :cond_4

    const-string p1, "STATE_CONNECTING"

    .line 328
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_4
    if-ne p3, p1, :cond_d

    const-string p1, "STATE_DISCONNECTING"

    .line 330
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 270
    :cond_5
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "STATE_DISCONNECTED\u3000"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " mReconnect = "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$400(Lcom/epson/mobilephone/common/ble/BleWork;)Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, " mRetry  "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->access$500(Lcom/epson/mobilephone/common/ble/BleWork;)I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 272
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$400(Lcom/epson/mobilephone/common/ble/BleWork;)Z

    move-result p2

    if-nez p2, :cond_6

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p2, v0, :cond_6

    .line 274
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    return-void

    .line 280
    :cond_6
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$400(Lcom/epson/mobilephone/common/ble/BleWork;)Z

    move-result p2

    if-nez p2, :cond_7

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, v0, :cond_7

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, v0, :cond_7

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->DISCONNECTED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, v0, :cond_7

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, v0, :cond_7

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->AFTER_SETTING_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, v0, :cond_7

    .line 287
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    return-void

    :cond_7
    const-string p2, "\u2605 stopScan"

    .line 291
    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 292
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleScanWork;->getInstace()Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object p2

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/ble/BleScanWork;->stopScan()V

    .line 294
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iput p3, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    .line 296
    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p3, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->AFTER_SETTING_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, p3, :cond_8

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p3, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->BEFORE_CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, p3, :cond_8

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p3, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, p3, :cond_8

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p3, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p2, p3, :cond_a

    .line 298
    :cond_8
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$400(Lcom/epson/mobilephone/common/ble/BleWork;)Z

    move-result p2

    if-nez p2, :cond_a

    .line 299
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne p1, p2, :cond_9

    .line 300
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$600(Lcom/epson/mobilephone/common/ble/BleWork;)V

    .line 302
    :cond_9
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, " BREAK \u2605 mSequence "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    return-void

    .line 307
    :cond_a
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$600(Lcom/epson/mobilephone/common/ble/BleWork;)V

    .line 309
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p3, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p2, p3, :cond_b

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$400(Lcom/epson/mobilephone/common/ble/BleWork;)Z

    move-result p2

    if-eqz p2, :cond_d

    .line 310
    :cond_b
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$500(Lcom/epson/mobilephone/common/ble/BleWork;)I

    move-result p2

    if-ge p2, p1, :cond_c

    .line 312
    :try_start_0
    sget-object p1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p2, 0x5

    invoke-virtual {p1, p2, p3}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 314
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 316
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "retry "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$500(Lcom/epson/mobilephone/common/ble/BleWork;)I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;)V

    .line 318
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$508(Lcom/epson/mobilephone/common/ble/BleWork;)I

    const-string p1, "init"

    .line 319
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 320
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$700(Lcom/epson/mobilephone/common/ble/BleWork;)Z

    goto :goto_2

    .line 323
    :cond_c
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    :cond_d
    :goto_2
    return-void
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 1

    .line 225
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V

    .line 226
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    const-string v0, "DescriptorWrite"

    invoke-static {p1, v0, p3}, Lcom/epson/mobilephone/common/ble/BleWork;->access$000(Lcom/epson/mobilephone/common/ble/BleWork;Ljava/lang/String;I)V

    if-nez p3, :cond_0

    .line 228
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$100(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 229
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$100(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 233
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$200(Lcom/epson/mobilephone/common/ble/BleWork;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onMtuChanged(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 0

    .line 219
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onMtuChanged(Landroid/bluetooth/BluetoothGatt;II)V

    .line 220
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "onMtuChanged "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    return-void
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 3

    .line 336
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 340
    :try_start_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/bluetooth/BluetoothGattService;

    if-eqz p2, :cond_0

    .line 341
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, "802a0000-4ef4-4e59-b573-2bed4a4ac158"

    .line 346
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\u767a\u898b!! EPSON SNMP \u30b5\u30fc\u30d3\u30b9"

    .line 347
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 350
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 351
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    .line 352
    invoke-interface {v1, v2}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 356
    :cond_2
    sget-object p2, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_3

    .line 362
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    const/4 v1, 0x0

    invoke-static {p2, v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$402(Lcom/epson/mobilephone/common/ble/BleWork;Z)Z

    .line 365
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->access$800(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p2

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v2, v2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-interface {p2, v2}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    .line 367
    new-instance p2, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-direct {p2, v2, v0}, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;-><init>(Lcom/epson/mobilephone/common/ble/BleWork;Lcom/epson/mobilephone/common/ble/BleWork$1;)V

    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {p2, v0}, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 358
    :cond_3
    new-instance p1, Lcom/epson/mobilephone/common/ble/BleException;

    const/4 p2, 0x1

    const-string v0, "getCharacteristic Error !"

    invoke-direct {p1, p2, v0}, Lcom/epson/mobilephone/common/ble/BleException;-><init>(ILjava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/epson/mobilephone/common/ble/BleException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 372
    :try_start_1
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleException;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 373
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleException;->printStackTrace()V

    .line 374
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    return-void

    .line 375
    :goto_2
    throw p1
.end method

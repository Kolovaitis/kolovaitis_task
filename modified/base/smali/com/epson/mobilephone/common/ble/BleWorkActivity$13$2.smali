.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

.field final synthetic val$editView:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;Landroid/widget/EditText;)V
    .locals 0

    .line 1013
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->val$editView:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1015
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->val$editView:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1016
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->val$editView:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWork;->setSSIDPassword(Ljava/lang/String;)V

    .line 1017
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_MACADDRESS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object p2, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 1019
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->connect()V

    .line 1021
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$2500(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    const-string p1, "waitingCommunicationDaialog"

    .line 1024
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 1025
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$2300(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    .line 1028
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const-string p2, "input_method"

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    .line 1029
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;->val$editView:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object p2

    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

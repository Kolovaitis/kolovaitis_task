.class public Lcom/epson/mobilephone/common/ble/DeviceAdapter;
.super Landroid/widget/ArrayAdapter;
.source "DeviceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
        ">;"
    }
.end annotation


# static fields
.field private static final PREFIX_RSSI:Ljava/lang/String; = "RSSI:"


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List<",
            "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 39
    iput p2, p0, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->mResId:I

    .line 40
    iput-object p3, p0, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->mList:Ljava/util/List;

    const-string p2, "layout_inflater"

    .line 41
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method getThresholdIcon(I)Landroid/graphics/drawable/Drawable;
    .locals 3

    .line 83
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07014c

    const/16 v2, -0x3c

    if-lt p1, v2, :cond_0

    const-string p1, "HIGH_SIGNAL"

    .line 88
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const v1, 0x7f07014e

    goto :goto_0

    :cond_0
    const/16 v2, -0x46

    if-lt p1, v2, :cond_1

    const-string p1, "MID_SIGNAL"

    .line 91
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const v1, 0x7f07014d

    goto :goto_0

    :cond_1
    const/16 v2, -0x4b

    if-lt p1, v2, :cond_2

    const-string p1, "LOW_SIGNAL"

    .line 94
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 98
    :cond_2
    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .line 46
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    if-nez p2, :cond_0

    .line 49
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget p3, p0, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->mResId:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    const p3, 0x7f080107

    .line 52
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 53
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-static {}, Lepson/print/IprintApplication;->isReleaseUnlimited()Z

    move-result v0

    const v1, 0x7f080108

    const v2, 0x7f080106

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 58
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RSSI:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getRssi()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, -0x333334

    .line 61
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 64
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const/16 v0, 0x14

    const/4 v1, 0x0

    .line 65
    invoke-virtual {p3, v1, v0, v1, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_0
    const p3, 0x7f080300

    .line 68
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 70
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getRssi()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->getThresholdIcon(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 71
    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object p2
.end method

.method public update(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)V
    .locals 5

    if-eqz p1, :cond_4

    .line 103
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 108
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    .line 109
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    .line 111
    invoke-virtual {v2, p2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->setRssi(I)V

    :cond_2
    if-nez v0, :cond_3

    .line 117
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->mList:Ljava/util/List;

    new-instance v1, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;-><init>(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_3
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->notifyDataSetChanged()V

    return-void

    :cond_4
    :goto_0
    return-void
.end method

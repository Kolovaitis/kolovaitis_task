.class public Lcom/epson/mobilephone/common/ble/ProbePrinter;
.super Ljava/lang/Object;
.source "ProbePrinter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/ble/ProbePrinter$scanProbeTask;,
        Lcom/epson/mobilephone/common/ble/ProbePrinter$printerProbeTask;
    }
.end annotation


# static fields
.field static final PROBE_TIMEOUT_SEC:I = 0xb4

.field private static final lock:Ljava/lang/Object;

.field private static mScanner:Lepson/scan/lib/escanLib;


# instance fields
.field private final BIND_SERVICE:I

.field private final CANCEL_FIND_PRINTER:I

.field private final DELAY:I

.field private final SEARCH_PRINTER:I

.field private final SELECT_PRINTER:I

.field private completion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

.field private context:Landroid/content/Context;

.field private volatile isPrinterFound:Z

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field private mHandler:Landroid/os/Handler;

.field private macAddress:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->lock:Ljava/lang/Object;

    .line 56
    new-instance v0, Lepson/scan/lib/escanLib;

    invoke-direct {v0}, Lepson/scan/lib/escanLib;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mScanner:Lepson/scan/lib/escanLib;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 39
    iput v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->SEARCH_PRINTER:I

    const/4 v0, 0x2

    .line 40
    iput v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->CANCEL_FIND_PRINTER:I

    const/4 v0, 0x4

    .line 41
    iput v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->SELECT_PRINTER:I

    const/16 v0, 0x11

    .line 42
    iput v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->BIND_SERVICE:I

    const/16 v0, 0x64

    .line 43
    iput v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->DELAY:I

    .line 166
    new-instance v0, Lcom/epson/mobilephone/common/ble/ProbePrinter$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/ProbePrinter$1;-><init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 190
    new-instance v0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;-><init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    .line 259
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/epson/mobilephone/common/ble/ProbePrinter$3;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/ProbePrinter$3;-><init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$002(Lcom/epson/mobilephone/common/ble/ProbePrinter;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$200(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Z
    .locals 0

    .line 37
    iget-boolean p0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->isPrinterFound:Z

    return p0
.end method

.method static synthetic access$202(Lcom/epson/mobilephone/common/ble/ProbePrinter;Z)Z
    .locals 0

    .line 37
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->isPrinterFound:Z

    return p1
.end method

.method static synthetic access$300(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Ljava/lang/String;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->macAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Landroid/os/Handler;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$500(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->context:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$600(Lcom/epson/mobilephone/common/ble/ProbePrinter;Lepson/print/MyPrinter;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->selectPrinter(Lepson/print/MyPrinter;)V

    return-void
.end method

.method static synthetic access$700()Lepson/scan/lib/escanLib;
    .locals 1

    .line 37
    sget-object v0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mScanner:Lepson/scan/lib/escanLib;

    return-object v0
.end method

.method public static create()Lcom/epson/mobilephone/common/ble/ProbePrinter;
    .locals 1

    .line 61
    new-instance v0, Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;-><init>()V

    return-object v0
.end method

.method private selectPrinter(Lepson/print/MyPrinter;)V
    .locals 3

    .line 302
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/MyPrinter;->setCurPrinter(Landroid/content/Context;)V

    .line 305
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->context:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "PrintSetting"

    const-string v1, "RE_SEARCH"

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 308
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->context:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "printer"

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bindEpsonService()V
    .locals 4

    .line 126
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->context:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lepson/print/service/EpsonService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_1

    const-string v0, "mEpsonService NULL"

    .line 131
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_1
    const-string v0, "bindEpsonService OK !!"

    .line 134
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method callbackCompletion(Z)V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->completion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    if-eqz v0, :cond_0

    .line 254
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;->call(Ljava/lang/Object;)V

    const/4 p1, 0x0

    .line 255
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->completion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    :cond_0
    return-void
.end method

.method public interruptSearch()V
    .locals 2

    .line 108
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->d()V

    .line 111
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 113
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "cancelSearchPrinter"

    .line 115
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 118
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 121
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->unbindEpsonService()V

    return-void
.end method

.method public search()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->completion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->macAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->completion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->context:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lepson/print/service/EpsonService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 95
    :try_start_0
    sget-object v0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->lock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    .line 96
    :try_start_1
    iput-boolean v1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->isPrinterFound:Z

    .line 97
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v0, "mEpsonService.searchPrinters"

    .line 98
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    const/16 v1, 0xb4

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->setTimeOut(I)V

    .line 100
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v1, v3}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 97
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    .line 103
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Required field is not set."

    .line 88
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required field is not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setBleCallback(Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;)Lcom/epson/mobilephone/common/ble/ProbePrinter;
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->completion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    return-object p0
.end method

.method public setContext(Landroid/content/Context;)Lcom/epson/mobilephone/common/ble/ProbePrinter;
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->context:Landroid/content/Context;

    return-object p0
.end method

.method public setMacAddress(Ljava/lang/String;)Lcom/epson/mobilephone/common/ble/ProbePrinter;
    .locals 2

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u2605"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u2605\u3000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->macAddress:Ljava/lang/String;

    return-object p0
.end method

.method unbindEpsonService()V
    .locals 4

    .line 142
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 145
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_1

    .line 147
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->isPrinterFound:Z

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const-string v0, "1"

    .line 149
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    const-string v0, "2"

    .line 153
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const-string v0, "unregisterCallback"

    .line 154
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 156
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    .line 157
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter;->mEpsonService:Lepson/print/service/IEpsonService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "unregisterCallback   mEpsonService"

    .line 159
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :goto_0
    const-string v0, "3"

    .line 162
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

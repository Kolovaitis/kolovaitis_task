.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->callBackFuncs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 2

    .line 200
    instance-of v0, p1, Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const-string v0, " type mismatch !"

    .line 201
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 203
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "result = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const/16 v1, 0xd2

    iput v1, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "count = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget v1, v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$000(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    .line 212
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    new-instance v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;)V

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->resetSequence()V

    .line 225
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$200(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Z)V

    return-void
.end method

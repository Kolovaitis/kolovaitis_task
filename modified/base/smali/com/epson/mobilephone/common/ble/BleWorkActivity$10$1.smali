.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

.field final synthetic val$items:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;[Ljava/lang/String;)V
    .locals 0

    .line 772
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->val$items:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 775
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->val$items:[Ljava/lang/String;

    aget-object p1, p1, p2

    .line 776
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "select = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 778
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->val$items:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_0

    .line 780
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$1800(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    goto/16 :goto_1

    .line 782
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u2606"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "::"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    iget v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 783
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/epson/mobilephone/common/ble/BleWork;->setSSID(Ljava/lang/String;)V

    .line 784
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->DISCONNECTED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq p1, p2, :cond_2

    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    iget p1, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    if-nez p1, :cond_1

    goto :goto_0

    .line 793
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "\u2606\u2606\u2606\u2606 "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p2

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "::"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p2

    iget p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 794
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->setSSIDType()V

    goto :goto_1

    .line 785
    :cond_2
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "\u2606\u2606 "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p2

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "::"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p2

    iget p2, p2, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 787
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->BEFORE_CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object p2, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 788
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->connect()V

    const-string p1, "              getInfoForConnectDialog"

    .line 790
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 791
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p1, p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    iget-object p2, p2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p2

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/ble/BleWork;->getDeviceName()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$1900(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.class Lcom/epson/mobilephone/common/ble/ProbePrinter$2;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "ProbePrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/ProbePrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;)V
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 202
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "find      :"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ":"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ":\u2605"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\u2605:"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-static {p5}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const-string p5, "FINISH"

    .line 203
    invoke-virtual {p1, p5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p5

    if-eqz p5, :cond_0

    const-string p1, " !!  FINISH  !!"

    .line 204
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 206
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$200(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 207
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->unbindEpsonService()V

    const-string p1, " !!  \u78ba\u8a8d\u5931\u6557  !!"

    .line 209
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 210
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->callbackCompletion(Z)V

    goto :goto_0

    .line 213
    :cond_0
    iget-object p5, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p5}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$300(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p3, p5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p5

    if-eqz p5, :cond_1

    .line 215
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, " !!  FIND    OK    !! :"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$300(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ": \u25c6"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-static {p5}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 217
    iget-object p5, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    const/4 v0, 0x1

    invoke-static {p5, v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$202(Lcom/epson/mobilephone/common/ble/ProbePrinter;Z)Z

    .line 220
    new-instance p5, Lepson/print/MyPrinter;

    invoke-direct {p5, p1, p2, p3, p4}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$400(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Landroid/os/Handler;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    .line 222
    iput-object p5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/4 p2, 0x4

    .line 223
    iput p2, p1, Landroid/os/Message;->what:I

    .line 225
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$400(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Landroid/os/Handler;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 226
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$2;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$400(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Landroid/os/Handler;

    move-result-object p1

    const/4 p2, 0x2

    const-wide/16 p3, 0x64

    invoke-virtual {p1, p2, p3, p4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 197
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 193
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 235
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    return-void
.end method

.method public onNotifyEndJob(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 243
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 239
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    return-void
.end method

.method public onNotifyProgress(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 231
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    return-void
.end method

.class public Lcom/epson/mobilephone/common/ble/BleWorkActivity$ScannedDeviceComparator;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScannedDeviceComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method public constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 571
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$ScannedDeviceComparator;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/epson/mobilephone/common/ble/util/ScannedDevice;Lcom/epson/mobilephone/common/ble/util/ScannedDevice;)I
    .locals 2

    .line 574
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getRssi()I

    move-result v0

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getRssi()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 577
    :cond_0
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getRssi()I

    move-result p1

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getRssi()I

    move-result p2

    if-ge p1, p2, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 571
    check-cast p1, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    check-cast p2, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-virtual {p0, p1, p2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$ScannedDeviceComparator;->compare(Lcom/epson/mobilephone/common/ble/util/ScannedDevice;Lcom/epson/mobilephone/common/ble/util/ScannedDevice;)I

    move-result p1

    return p1
.end method

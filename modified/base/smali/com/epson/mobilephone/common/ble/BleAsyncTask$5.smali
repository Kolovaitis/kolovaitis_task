.class Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;
.super Ljava/lang/Object;
.source "BleAsyncTask.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleAsyncTask;->showAdvanceToSettingDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const-string p1, "\u2605 stopScan"

    .line 231
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 232
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$100(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->stopScan()V

    .line 235
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$100(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object p2

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/ble/BleScanWork;->getScannedDeviceList()Ljava/util/ArrayList;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$302(Lcom/epson/mobilephone/common/ble/BleAsyncTask;Ljava/util/List;)Ljava/util/List;

    .line 238
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 239
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$300(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    .line 240
    new-instance v1, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getRssi()I

    move-result v3

    .line 241
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getHardwareType()[B

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;-><init>(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)V

    .line 240
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 244
    :cond_0
    new-instance p2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$600(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "ScannedDevice"

    .line 245
    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 246
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$5;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$600(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.class synthetic Lcom/epson/mobilephone/common/ble/BleWork$5;
.super Ljava/lang/Object;
.source "BleWork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleWork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 520
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->values()[Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    :try_start_0
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_MACADDRESS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_LOGIN:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_SET_PASS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_LOGOUT:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SET_SECURITY_TYPE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_SKIP:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->INITIALIZE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->GET_VERSION:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_SSID_LIST:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    return-void
.end method

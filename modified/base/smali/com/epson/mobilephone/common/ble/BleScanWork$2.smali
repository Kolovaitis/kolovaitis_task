.class Lcom/epson/mobilephone/common/ble/BleScanWork$2;
.super Ljava/lang/Object;
.source "BleScanWork.java"

# interfaces
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleScanWork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleScanWork;)V
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$2;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 7
    .param p3    # [B
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    array-length v3, p3

    const/4 v4, 0x7

    const/16 v5, 0x2b

    if-lt v3, v5, :cond_2

    const/16 v3, 0x1b

    :goto_0
    const/16 v5, 0xc

    if-lt v3, v5, :cond_0

    .line 181
    aget-byte v5, p3, v3

    and-int/lit16 v5, v5, 0xff

    invoke-static {v5}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->IntToHex2(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x6

    :goto_1
    const/4 v5, 0x5

    if-lt v3, v5, :cond_1

    .line 185
    aget-byte v5, p3, v3

    and-int/lit16 v5, v5, 0xff

    invoke-static {v5}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->IntToHex2(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x7

    :goto_2
    const/16 v5, 0x9

    if-gt v3, v5, :cond_2

    .line 189
    aget-byte v5, p3, v3

    and-int/lit16 v5, v5, 0xff

    invoke-static {v5}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->IntToHex2(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 193
    :cond_2
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$2;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {v3}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$400(Lcom/epson/mobilephone/common/ble/BleScanWork;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "802a0000-4ef4-4e59-b573-2bed4a4ac158"

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, -0x4b

    if-lt p2, v0, :cond_3

    const/16 v0, 0x7f

    if-ge p2, v0, :cond_3

    .line 195
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x3

    .line 196
    new-array v3, v3, [B

    const/4 v5, 0x0

    .line 197
    array-length v6, v3

    invoke-static {p3, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " Company information = "

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " getManufacturerSpecificData = "

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 200
    iget-object p3, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$2;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-virtual {p3, p1, p2, v0, v3}, Lcom/epson/mobilephone/common/ble/BleScanWork;->update(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 202
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$2;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$300(Lcom/epson/mobilephone/common/ble/BleScanWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    if-eqz p1, :cond_3

    if-eqz v0, :cond_3

    .line 204
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "mScannedDeviceList --- "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$2;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$200(Lcom/epson/mobilephone/common/ble/BleScanWork;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 205
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$2;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$300(Lcom/epson/mobilephone/common/ble/BleScanWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/BleScanWork$2;->this$0:Lcom/epson/mobilephone/common/ble/BleScanWork;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/BleScanWork;->access$200(Lcom/epson/mobilephone/common/ble/BleScanWork;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

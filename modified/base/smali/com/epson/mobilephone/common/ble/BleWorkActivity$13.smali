.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setSSIDPassword()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 968
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const-string v0, "inputs = "

    .line 971
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 973
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSecurityType()S

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    goto/16 :goto_1

    .line 987
    :cond_0
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x21

    .line 988
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 989
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v2, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$2000(Lcom/epson/mobilephone/common/ble/BleWorkActivity;I)[Landroid/text/InputFilter;

    move-result-object v2

    .line 990
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 991
    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$1;

    invoke-direct {v2, p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;I)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1007
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 1008
    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v5, 0x7f0e0019

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1009
    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v5, 0x7f0e0018

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v4}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v8

    invoke-virtual {v8}, Lcom/epson/mobilephone/common/ble/BleWork;->getSSID()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v4, v5, v7}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1010
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1013
    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v5, 0x7f0e03ff

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;

    invoke-direct {v5, p0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$2;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;Landroid/widget/EditText;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1034
    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v5, 0x7f0e03f2

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;

    invoke-direct {v5, p0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13$3;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;Landroid/widget/EditText;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1053
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1054
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDialog:Landroid/app/AlertDialog;

    .line 1055
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1056
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDialog:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    const/16 v2, 0xff

    if-ne v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    :goto_0
    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1060
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sequence = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v1

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 1061
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->AFTER_SETTING_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v1, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 1062
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/epson/mobilephone/common/ble/BleWork;->disconnect(Z)V

    .line 1063
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sequence = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v1

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    return-void

    .line 979
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v1

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$2400(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V

    return-void
.end method

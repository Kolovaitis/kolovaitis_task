.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setSSID()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 863
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const-string v0, "inputs = "

    .line 866
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 869
    new-instance v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x21

    .line 870
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 871
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const/16 v2, 0x64

    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$2000(Lcom/epson/mobilephone/common/ble/BleWorkActivity;I)[Landroid/text/InputFilter;

    move-result-object v1

    .line 872
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 873
    new-instance v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 892
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 893
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v4, 0x7f0e0017

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 894
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v4, 0x7f0e0016

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 895
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 898
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v4, 0x7f0e03ff

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$2;

    invoke-direct {v4, p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$2;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 910
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v4, 0x7f0e03f2

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$3;

    invoke-direct {v4, p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11$3;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 928
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 929
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDialog:Landroid/app/AlertDialog;

    .line 930
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 931
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method

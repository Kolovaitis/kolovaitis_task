.class public Lcom/epson/mobilephone/common/ble/BleWorkActivity;
.super Lepson/print/ActivityIACommon;
.source "BleWorkActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/ble/BleWorkActivity$PasswordFilter;,
        Lcom/epson/mobilephone/common/ble/BleWorkActivity$ScannedDeviceComparator;,
        Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;
    }
.end annotation


# static fields
.field private static final CONNECT_TIME_OUT:I = 0xea60

.field private static final FIRST_DELAY:I = 0x2710

.field private static final FIRST_DELAY_SEC:I = 0xa

.field private static final FIRST_PROGRESS_PER:I = 0x5a

.field private static final FIRST_PROGRESS_SEC:I = 0x5a

.field private static final REQUEST_CODE_EPSON_CONNECT_REGISTRATION:I = 0xd4

.field private static final SECOND_DELAY:I = 0x30d40

.field private static final TOTAL_MARGIN_SEC:I = 0x1e

.field private static final TOTAL_PROGRESS_SEC:I = 0xd2


# instance fields
.field count:I

.field private isAlive:Z

.field isStopSetUpProgress:Z

.field mAlertDialog:Landroid/app/AlertDialog;

.field private mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

.field private mCompletion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

.field private mContext:Landroid/content/Context;

.field private mDeviceAdapter:Lcom/epson/mobilephone/common/ble/DeviceAdapter;

.field mDialog:Landroid/app/AlertDialog;

.field private mNext:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

.field mPercentView:Landroid/widget/TextView;

.field mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

.field mProgressPercent:Landroid/widget/ProgressBar;

.field private mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

.field private mSequenceCallbackSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

.field private mTimeoutHandler:Landroid/os/Handler;

.field private mWifiSearchHandler:Landroid/os/Handler;

.field private runnableSetUpProgress:Ljava/lang/Runnable;

.field private runnableWifiSearch:Ljava/lang/Runnable;

.field private setUpProgressHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 65
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 68
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    .line 70
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mNext:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    .line 71
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mCompletion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    .line 72
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mSequenceCallbackSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 73
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    const/4 v0, 0x0

    .line 112
    iput v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    .line 453
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isStopSetUpProgress:Z

    return-void
.end method

.method private SSIDfailedDialog(Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V
    .locals 1

    .line 694
    new-instance v0, Lcom/epson/mobilephone/common/ble/-$$Lambda$BleWorkActivity$kQM42E2XQE5mRSFm5tHycw_DO2g;

    invoke-direct {v0, p0, p1}, Lcom/epson/mobilephone/common/ble/-$$Lambda$BleWorkActivity$kQM42E2XQE5mRSFm5tHycw_DO2g;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->stopCallbacks()V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->failedDialog(Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Landroid/content/Context;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/epson/mobilephone/common/ble/BleWorkActivity;I)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->updateProgress(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Landroid/os/Handler;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setUpProgressHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/DeviceAdapter;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDeviceAdapter:Lcom/epson/mobilephone/common/ble/DeviceAdapter;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/util/ScannedDevice;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->selectDevice(Lcom/epson/mobilephone/common/ble/util/ScannedDevice;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Z
    .locals 0

    .line 65
    iget-boolean p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isAlive:Z

    return p0
.end method

.method static synthetic access$1700(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Ljava/util/List;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->sortSSIDlist(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setSSID()V

    return-void
.end method

.method static synthetic access$1900(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Ljava/lang/String;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getInfoForConnectDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Z)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->settingResultDailog(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/epson/mobilephone/common/ble/BleWorkActivity;I)[Landroid/text/InputFilter;
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getInputFilters(I)[Landroid/text/InputFilter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$2100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mNext:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    return-object p0
.end method

.method static synthetic access$2200(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Ljava/lang/Runnable;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableSetUpProgress:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$2300(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->waitingCommunicationDaialog()V

    return-void
.end method

.method static synthetic access$2400(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->SSIDfailedDialog(Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setConnectTimeOut()V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->stopWifiSearch()V

    return-void
.end method

.method static synthetic access$400(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Landroid/os/Handler;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mWifiSearchHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$402(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mWifiSearchHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$500(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Ljava/lang/Runnable;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableWifiSearch:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$502(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableWifiSearch:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mCompletion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    return-object p0
.end method

.method static synthetic access$602(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mCompletion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    return-object p1
.end method

.method static synthetic access$700(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->stopConnectTimeOut()V

    return-void
.end method

.method static synthetic access$800(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setSSIDPassword()V

    return-void
.end method

.method static synthetic access$900(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->showSSIDList()V

    return-void
.end method

.method private callBackFuncs()V
    .locals 1

    .line 193
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mCompletion:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    .line 230
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mNext:Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    .line 305
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$3;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mSequenceCallbackSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 344
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$4;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$4;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-void
.end method

.method private failedDialog(Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V
    .locals 1

    .line 659
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;

    invoke-direct {v0, p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getInfoForConnectDialog(Ljava/lang/String;)V
    .locals 3

    .line 605
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 606
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 607
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0a0042

    const/4 v2, 0x0

    .line 608
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 609
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    const-string v0, "line.separator"

    .line 611
    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 612
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const p1, 0x7f0e001a

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 613
    iget-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isAlive:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isFinishing()Z

    move-result p1

    if-nez p1, :cond_0

    .line 614
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    .line 617
    :cond_0
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setConnectTimeOut()V

    return-void
.end method

.method private getInputFilters(I)[Landroid/text/InputFilter;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, 0x7

    const/16 v1, 0x1a

    const/16 v2, 0x3f

    if-eq p1, v0, :cond_1

    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    const/16 v1, 0x3f

    goto :goto_0

    :pswitch_0
    const/16 v1, 0xa

    goto :goto_0

    :cond_0
    const/16 v1, 0x20

    goto :goto_0

    :cond_1
    :pswitch_1
    const/16 v1, 0x3f

    :goto_0
    :pswitch_2
    const/4 v0, 0x2

    .line 1097
    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    .line 1098
    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v0, v2

    const/4 v1, 0x1

    .line 1099
    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$PasswordFilter;

    invoke-direct {v2, p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$PasswordFilter;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;I)V

    aput-object v2, v0, v1

    return-object v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private init(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
            ">;)V"
        }
    .end annotation

    const v0, 0x7f0801dc

    .line 547
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 549
    new-instance v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$ScannedDeviceComparator;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$ScannedDeviceComparator;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 551
    new-instance v1, Lcom/epson/mobilephone/common/ble/DeviceAdapter;

    if-eqz p1, :cond_0

    move-object v2, p1

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    const v3, 0x7f0a0084

    invoke-direct {v1, p0, v3, v2}, Lcom/epson/mobilephone/common/ble/DeviceAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDeviceAdapter:Lcom/epson/mobilephone/common/ble/DeviceAdapter;

    .line 554
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDeviceAdapter:Lcom/epson/mobilephone/common/ble/DeviceAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 556
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 557
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDeviceAdapter:Lcom/epson/mobilephone/common/ble/DeviceAdapter;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/epson/mobilephone/common/ble/DeviceAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->selectDevice(Lcom/epson/mobilephone/common/ble/util/ScannedDevice;)V

    .line 560
    :cond_1
    new-instance p1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$7;

    invoke-direct {p1, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$7;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public static synthetic lambda$SSIDfailedDialog$1(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V
    .locals 3

    .line 695
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->stopCallbacks()V

    .line 698
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sequence = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 699
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz p1, :cond_0

    .line 700
    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    const/4 p1, 0x0

    .line 702
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 704
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->getSsidLis()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 706
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e001b

    .line 707
    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    if-eqz p1, :cond_1

    const v1, 0x7f0e0473

    goto :goto_0

    :cond_1
    const v1, 0x7f0e0021

    .line 708
    :goto_0
    invoke-virtual {p0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/mobilephone/common/ble/-$$Lambda$BleWorkActivity$QPAmrfjsEI2roxdI1XBPnZrUxwM;

    invoke-direct {v2, p0, p1}, Lcom/epson/mobilephone/common/ble/-$$Lambda$BleWorkActivity$QPAmrfjsEI2roxdI1XBPnZrUxwM;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 726
    iget-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isAlive:Z

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isFinishing()Z

    move-result p1

    if-nez p1, :cond_2

    .line 727
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 728
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    :cond_2
    return-void
.end method

.method public static synthetic lambda$null$0(Lcom/epson/mobilephone/common/ble/BleWorkActivity;ZLandroid/content/DialogInterface;I)V
    .locals 1

    .line 711
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->Button:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;->Home:Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;

    .line 712
    :goto_0
    iget-object p3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p3}, Lcom/epson/mobilephone/common/ble/BleWork;->getDeviceName()Ljava/lang/String;

    move-result-object p3

    if-eqz p1, :cond_1

    sget-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Error119:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;->Error106:Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;

    :goto_1
    invoke-static {p3, p2, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendSetup(Ljava/lang/String;Lcom/epson/iprint/prtlogger/Analytics$SetUpPath;Lcom/epson/iprint/prtlogger/Analytics$SetUpResult;)V

    if-eqz p1, :cond_2

    .line 716
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_SSID_LIST:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object p2, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 717
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ble/BleWork;->setSecurityType(S)V

    .line 718
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->showSSIDList()V

    goto :goto_2

    .line 720
    :cond_2
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->disconnect()V

    .line 722
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->finish()V

    :goto_2
    return-void
.end method

.method private selectDevice(Lcom/epson/mobilephone/common/ble/util/ScannedDevice;)V
    .locals 4
    .param p1    # Lcom/epson/mobilephone/common/ble/util/ScannedDevice;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 585
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 587
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleScanWork;->getInstace()Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleScanWork;->stopScan()V

    .line 589
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWork;->resetSequence()V

    .line 590
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mSequenceCallbackSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    invoke-virtual {v1, v2, v3}, Lcom/epson/mobilephone/common/ble/BleWork;->setSequenceCallback(Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V

    const-string v1, "              getInfoForConnectDialog"

    .line 592
    invoke-static {v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 593
    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getInfoForConnectDialog(Ljava/lang/String;)V

    .line 595
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {v0, p0, p1}, Lcom/epson/mobilephone/common/ble/BleWork;->init(Landroid/content/Context;Lcom/epson/mobilephone/common/ble/util/ScannedDevice;)Z

    return-void
.end method

.method private setConnectTimeOut()V
    .locals 4

    .line 625
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mTimeoutHandler:Landroid/os/Handler;

    .line 626
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mTimeoutHandler:Landroid/os/Handler;

    new-instance v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$8;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private setSSID()V
    .locals 1

    .line 863
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$11;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private setSSIDPassword()V
    .locals 2

    .line 942
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 944
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSSIDPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    .line 945
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSecurityType()S

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    .line 946
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSecurityType()S

    move-result v0

    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->SET_PASSWORD:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 968
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$13;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void

    .line 949
    :cond_1
    :goto_0
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$12;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$12;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private settingResultDailog(Z)V
    .locals 1

    .line 383
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;

    invoke-direct {v0, p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$5;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Z)V

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private showSSIDList()V
    .locals 1

    .line 735
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$10;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private sortSSIDlist(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 815
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 817
    invoke-static {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    const-string v2, "-"

    .line 818
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 824
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 825
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "_"

    const-string v5, "-"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "-A$"

    const-string v5, "-G"

    .line 827
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 828
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    const-string v4, "-A-"

    const-string v5, "-G-"

    .line 832
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 833
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    const-string v4, "-5G$"

    const-string v5, ""

    .line 838
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 839
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    const-string v4, "-5GHZ$"

    const-string v5, ""

    .line 843
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 844
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    const/4 v2, -0x1

    :goto_1
    if-lez v2, :cond_5

    .line 853
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    .line 854
    invoke-interface {p1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_5
    return-void
.end method

.method private stopCallbacks()V
    .locals 1

    .line 355
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->stopConnectTimeOut()V

    .line 356
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->stopWifiSearch()V

    const/4 v0, 0x1

    .line 357
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isStopSetUpProgress:Z

    .line 358
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->stopSetUpProgress()V

    return-void
.end method

.method private stopConnectTimeOut()V
    .locals 2

    .line 646
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mTimeoutHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 647
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 648
    iput-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mTimeoutHandler:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method private stopSetUpProgress()V
    .locals 2

    .line 362
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setUpProgressHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 363
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableSetUpProgress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 364
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setUpProgressHandler:Landroid/os/Handler;

    .line 365
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableSetUpProgress:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private stopWifiSearch()V
    .locals 2

    .line 370
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mWifiSearchHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 371
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableWifiSearch:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 372
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mWifiSearchHandler:Landroid/os/Handler;

    .line 373
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableWifiSearch:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private updateProgress(I)V
    .locals 2

    .line 538
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 539
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mPercentView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " %"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 540
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Landroid/widget/ProgressBar;->invalidate()V

    .line 541
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mPercentView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->invalidate()V

    return-void
.end method

.method private waitingCommunicationDaialog()V
    .locals 4

    .line 458
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "dismiss "

    .line 459
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 460
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 461
    iput-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    :cond_0
    const/4 v0, 0x0

    .line 464
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isStopSetUpProgress:Z

    .line 466
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 467
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 469
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0a0042

    .line 470
    invoke-virtual {v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 471
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    const v2, 0x7f08029d

    .line 474
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProgressPercent:Landroid/widget/ProgressBar;

    .line 475
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const v2, 0x7f08025b

    .line 476
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mPercentView:Landroid/widget/TextView;

    .line 477
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mPercentView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    const v2, 0x7f080302

    .line 479
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 481
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 482
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProgressPercent:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 483
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mPercentView:Landroid/widget/TextView;

    const-string v2, "0%"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 485
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0026

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 486
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 488
    iput v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->count:I

    .line 489
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->stopSetUpProgress()V

    .line 490
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setUpProgressHandler:Landroid/os/Handler;

    .line 492
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$6;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableSetUpProgress:Ljava/lang/Runnable;

    .line 534
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setUpProgressHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->runnableSetUpProgress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    const/16 p2, 0xd4

    if-ne p1, p2, :cond_0

    .line 449
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 120
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 121
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x5

    .line 122
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->requestWindowFeature(I)Z

    const p1, 0x7f0a003c

    .line 123
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setContentView(I)V

    const p1, 0x7f0e0020

    const/4 v0, 0x1

    .line 126
    invoke-virtual {p0, p1, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->setActionBar(IZ)V

    const p1, 0x7f080298

    .line 128
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 130
    iput-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mContext:Landroid/content/Context;

    .line 133
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->callBackFuncs()V

    .line 136
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/16 v0, 0x80

    invoke-virtual {p1, v0}, Landroid/view/Window;->addFlags(I)V

    .line 138
    invoke-static {}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->create()Lcom/epson/mobilephone/common/ble/ProbePrinter;

    move-result-object p1

    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->setContext(Landroid/content/Context;)Lcom/epson/mobilephone/common/ble/ProbePrinter;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    .line 140
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleWork;->getInstace()Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    .line 142
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mBleWork:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->resetSequence()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 185
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 186
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->unbindEpsonService()V

    return-void
.end method

.method protected onPause()V
    .locals 0

    .line 164
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 165
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 148
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    .line 150
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->bindEpsonService()V

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSequence = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleWork;->getInstace()Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v1

    iget-object v1, v1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDeviceAdapter:Lcom/epson/mobilephone/common/ble/DeviceAdapter;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleWork;->getInstace()Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne v0, v1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ScannedDevice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 157
    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->init(Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 170
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStart()V

    const/4 v0, 0x1

    .line 171
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isAlive:Z

    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 176
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    .line 177
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    const/4 v0, 0x0

    .line 178
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isAlive:Z

    .line 180
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method public validateInputText(ILjava/lang/String;)V
    .locals 9

    .line 1152
    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "^[^ -~\uff61-\uff9f]+$"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_2

    .line 1153
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    const/4 v0, 0x7

    const/16 v3, 0x3f

    const/16 v4, 0x8

    if-eq p1, v0, :cond_1

    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1a

    const/16 v5, 0xd

    const/16 v6, 0xa

    const/4 v7, 0x2

    const/4 v8, 0x5

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    if-eqz p2, :cond_3

    if-eq p2, v8, :cond_3

    if-lt p2, v4, :cond_2

    if-gt p2, v3, :cond_2

    goto/16 :goto_0

    :pswitch_0
    if-lt p2, v4, :cond_2

    if-gt p2, v3, :cond_2

    goto/16 :goto_0

    .line 1170
    :pswitch_1
    new-array p1, v7, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    .line 1171
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 1163
    :pswitch_2
    new-array p1, v7, [Ljava/lang/Integer;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    .line 1164
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :pswitch_3
    const/4 p1, 0x4

    .line 1156
    new-array p1, p1, [Ljava/lang/Integer;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v7

    const/4 v3, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v3

    .line 1157
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_0
    if-lt p2, v2, :cond_2

    const/16 p1, 0x20

    if-gt p2, p1, :cond_2

    goto :goto_0

    :cond_1
    :pswitch_4
    if-lt p2, v4, :cond_2

    if-gt p2, v3, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 1200
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mDialog:Landroid/app/AlertDialog;

    const/4 p2, -0x1

    invoke-virtual {p1, p2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

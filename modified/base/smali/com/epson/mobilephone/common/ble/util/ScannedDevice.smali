.class public Lcom/epson/mobilephone/common/ble/util/ScannedDevice;
.super Ljava/lang/Object;
.source "ScannedDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/epson/mobilephone/common/ble/util/ScannedDevice;",
            ">;"
        }
    .end annotation
.end field

.field public static final HIGH_SIGNAL:I = -0x3c

.field public static final INVALID_SIGNAL:I = 0x7f

.field public static final LOW_SIGNAL:I = -0x4b

.field public static final MID_SIGNAL:I = -0x46

.field public static final TYPE:Ljava/lang/String; = "ScannedDevice"

.field private static final UNKNOWN:Ljava/lang/String; = "Unknown"


# instance fields
.field private mDevice:Landroid/bluetooth/BluetoothDevice;

.field private mDisplayName:Ljava/lang/String;

.field private mHardwareType:[B

.field private mRssi:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    new-instance v0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice$1;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice$1;-><init>()V

    sput-object v0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/bluetooth/BluetoothDevice;ILjava/lang/String;[B)V
    .locals 1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_3

    .line 74
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 75
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-lt p1, v0, :cond_0

    .line 76
    iput-object p3, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDisplayName:Ljava/lang/String;

    goto :goto_0

    .line 78
    :cond_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDisplayName:Ljava/lang/String;

    .line 80
    :goto_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDisplayName:Ljava/lang/String;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_2

    :cond_1
    const-string p1, "Unknown"

    .line 81
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDisplayName:Ljava/lang/String;

    .line 83
    :cond_2
    iput p2, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mRssi:I

    .line 84
    iput-object p4, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mHardwareType:[B

    return-void

    .line 72
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "BluetoothDevice is null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-class v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mRssi:I

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDisplayName:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mHardwareType:[B

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getHardwareType()[B
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mHardwareType:[B

    return-object v0
.end method

.method public getRssi()I
    .locals 1

    .line 92
    iget v0, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mRssi:I

    return v0
.end method

.method public setRssi(I)V
    .locals 0

    .line 96
    iput p1, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mRssi:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 64
    iget p2, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mRssi:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->mHardwareType:[B

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

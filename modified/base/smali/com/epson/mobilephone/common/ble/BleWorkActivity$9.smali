.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity;->failedDialog(Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

.field final synthetic val$jobSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity;Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;)V
    .locals 0

    .line 659
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->val$jobSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 661
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$000(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)V

    .line 663
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->val$jobSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 664
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sequence = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 665
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 668
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 670
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$1100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 671
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e0022

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 672
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    const v2, 0x7f0e0021

    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9$1;

    invoke-direct {v2, p0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 681
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$1600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 682
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, v1, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 683
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$9;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method

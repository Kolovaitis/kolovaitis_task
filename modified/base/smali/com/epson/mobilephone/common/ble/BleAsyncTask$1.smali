.class Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;
.super Ljava/lang/Object;
.source "BleAsyncTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {v1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$000(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)J

    move-result-wide v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "CancellationException"

    .line 108
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    nop

    const-string v0, "Running task has timed out!!"

    .line 102
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$100(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleAsyncTask$1;->this$0:Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$100(Lcom/epson/mobilephone/common/ble/BleAsyncTask;)Lcom/epson/mobilephone/common/ble/BleScanWork;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/BleScanWork;->stopScan()V

    .line 106
    :cond_0
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->access$200()Lcom/epson/mobilephone/common/ble/BleAsyncTask;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->cancel(Z)Z

    goto :goto_0

    :catch_2
    const-string v0, "ExecutionException"

    .line 100
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    goto :goto_0

    :catch_3
    const-string v0, "InterruptedException"

    .line 98
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

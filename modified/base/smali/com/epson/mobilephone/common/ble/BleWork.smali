.class public Lcom/epson/mobilephone/common/ble/BleWork;
.super Ljava/lang/Object;
.source "BleWork.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;,
        Lcom/epson/mobilephone/common/ble/BleWork$SingletonHolder;,
        Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;
    }
.end annotation


# static fields
.field private static final EPS_COMM_BID:I = 0x2

.field private static final GATT_DELAY:I = 0x0

.field private static final GATT_DELAY2:I = 0x1f4

.field private static final RETRY_NUMS:I = 0x3

.field private static final SCAN_ADVERTISING_PACKET_TIMEOUT:I = 0x2710


# instance fields
.field handlerRead:Landroid/os/Handler;

.field handlerWrite:Landroid/os/Handler;

.field private mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mConnGatt:Landroid/bluetooth/BluetoothGatt;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDevice:Landroid/bluetooth/BluetoothDevice;

.field private mDeviceMacAddress:Ljava/lang/String;

.field private mDevicePassword:Ljava/lang/String;

.field private final mEngineID:[B

.field private mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

.field private mFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private mGattcallback:Landroid/bluetooth/BluetoothGattCallback;

.field private mInSequence:Z

.field private mIndex:B

.field private mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private mReconnect:Z

.field private mResposne:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private mRetry:I

.field private mSSID:Ljava/lang/String;

.field private mSSIDPassword:Ljava/lang/String;

.field private mSecurityType:S

.field private mSelectItem:Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

.field public mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

.field private mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private mSequenceCallbackSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private mSsidLis:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mStatus:I

.field private mSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private mTimeOut:J


# direct methods
.method private constructor <init>()V
    .locals 4

    .line 664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 84
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 86
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 88
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequenceCallbackSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 90
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 92
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mResposne:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSsidLis:Ljava/util/List;

    const-wide/16 v1, -0x1

    .line 117
    iput-wide v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mTimeOut:J

    .line 121
    invoke-static {}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getInstance()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    .line 123
    new-instance v1, Lcom/epson/mobilephone/common/escpr/MIBCommand;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/escpr/MIBCommand;-><init>()V

    iput-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    const/4 v1, 0x0

    .line 126
    iput-byte v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mIndex:B

    .line 127
    iput-boolean v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mInSequence:Z

    .line 128
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/epson/mobilephone/common/escpr/EscprLib;->getEngineId(Landroid/content/Context;)[B

    move-result-object v2

    iput-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEngineID:[B

    .line 131
    iput-boolean v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    .line 134
    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 209
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mGattcallback:Landroid/bluetooth/BluetoothGattCallback;

    .line 212
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mGattcallback:Landroid/bluetooth/BluetoothGattCallback;

    if-eqz v0, :cond_0

    const-string v0, "mGattcallback != null"

    .line 213
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 215
    :cond_0
    new-instance v0, Lcom/epson/mobilephone/common/ble/BleWork$1;

    invoke-direct {v0, p0}, Lcom/epson/mobilephone/common/ble/BleWork$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWork;)V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mGattcallback:Landroid/bluetooth/BluetoothGattCallback;

    .line 954
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerRead:Landroid/os/Handler;

    .line 1173
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerWrite:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/mobilephone/common/ble/BleWork$1;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/ble/BleWork;Ljava/lang/String;I)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/epson/mobilephone/common/ble/BleWork;->showStatus(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mResposne:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/epson/mobilephone/common/ble/BleWork;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->characteristicRead()V

    return-void
.end method

.method static synthetic access$1100(Lcom/epson/mobilephone/common/ble/BleWork;)Ljava/util/List;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSsidLis:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$1202(Lcom/epson/mobilephone/common/ble/BleWork;B)B
    .locals 0

    .line 72
    iput-byte p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mIndex:B

    return p1
.end method

.method static synthetic access$1300(Lcom/epson/mobilephone/common/ble/BleWork;)Ljava/lang/String;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/epson/mobilephone/common/ble/BleWork;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->checkSSID()V

    return-void
.end method

.method static synthetic access$1500(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/MIBCommand;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/epson/mobilephone/common/ble/BleWork;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mInSequence:Z

    return p0
.end method

.method static synthetic access$1602(Lcom/epson/mobilephone/common/ble/BleWork;Z)Z
    .locals 0

    .line 72
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mInSequence:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/escpr/EscprLib;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    return-object p0
.end method

.method static synthetic access$1802(Lcom/epson/mobilephone/common/ble/BleWork;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDeviceMacAddress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/epson/mobilephone/common/ble/BleWork;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->setSSIDWork()V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/mobilephone/common/ble/BleWork;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->sequenceError()V

    return-void
.end method

.method static synthetic access$2000(Lcom/epson/mobilephone/common/ble/BleWork;)[B
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEngineID:[B

    return-object p0
.end method

.method static synthetic access$2100(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-object p0
.end method

.method static synthetic access$2200(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-object p0
.end method

.method static synthetic access$2500(Lcom/epson/mobilephone/common/ble/BleWork;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->startSetSSID()V

    return-void
.end method

.method static synthetic access$2600(Lcom/epson/mobilephone/common/ble/BleWork;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWork;->notifySet(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/epson/mobilephone/common/ble/BleWork;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWork;->setNotifyCallback(Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V

    return-void
.end method

.method static synthetic access$300(Lcom/epson/mobilephone/common/ble/BleWork;)Landroid/bluetooth/BluetoothGatt;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    return-object p0
.end method

.method static synthetic access$302(Lcom/epson/mobilephone/common/ble/BleWork;Landroid/bluetooth/BluetoothGatt;)Landroid/bluetooth/BluetoothGatt;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    return-object p1
.end method

.method static synthetic access$400(Lcom/epson/mobilephone/common/ble/BleWork;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    return p0
.end method

.method static synthetic access$402(Lcom/epson/mobilephone/common/ble/BleWork;Z)Z
    .locals 0

    .line 72
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    return p1
.end method

.method static synthetic access$500(Lcom/epson/mobilephone/common/ble/BleWork;)I
    .locals 0

    .line 72
    iget p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mRetry:I

    return p0
.end method

.method static synthetic access$508(Lcom/epson/mobilephone/common/ble/BleWork;)I
    .locals 2

    .line 72
    iget v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mRetry:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mRetry:I

    return v0
.end method

.method static synthetic access$600(Lcom/epson/mobilephone/common/ble/BleWork;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->gattClose()V

    return-void
.end method

.method static synthetic access$700(Lcom/epson/mobilephone/common/ble/BleWork;)Z
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->init()Z

    move-result p0

    return p0
.end method

.method static synthetic access$800(Lcom/epson/mobilephone/common/ble/BleWork;)Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequenceCallbackSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-object p0
.end method

.method private characteristicRead()V
    .locals 5

    .line 958
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "read "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 959
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_REBOOT_NW:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 964
    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v1, v0, v1

    .line 965
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerRead:Landroid/os/Handler;

    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWork$3;

    invoke-direct {v2, p0, v0}, Lcom/epson/mobilephone/common/ble/BleWork$3;-><init>(Lcom/epson/mobilephone/common/ble/BleWork;[I)V

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static chechSSIDforBle(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 724
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-le v0, v1, :cond_0

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->canAccessWiFiInfo(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 726
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->getInsetance(Landroid/content/Context;)Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;

    move-result-object p0

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiNetworkManager;->isWiFiValidated()Z

    move-result p0

    return p0

    .line 728
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 729
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private checkSSID()V
    .locals 3

    .line 1236
    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1238
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1241
    :goto_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    const-string v1, "UTF8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 1242
    sget-object v1, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v2, "802a0006-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 1243
    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    .line 1244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SSID write :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1246
    invoke-direct {p0, v1}, Lcom/epson/mobilephone/common/ble/BleWork;->gattWriteCharacter(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    return-void
.end method

.method private gattClose()V
    .locals 1

    const-string v0, " !! mConnGatt close"

    .line 817
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 819
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_0

    .line 820
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V

    const/4 v0, 0x0

    .line 821
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    :cond_0
    return-void
.end method

.method private gattWriteCharacter(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 5

    const/4 v0, 0x1

    .line 1176
    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v1, v0, v1

    .line 1177
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerWrite:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1178
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->handlerWrite:Landroid/os/Handler;

    new-instance v2, Lcom/epson/mobilephone/common/ble/BleWork$4;

    invoke-direct {v2, p0, p1, v0}, Lcom/epson/mobilephone/common/ble/BleWork$4;-><init>(Lcom/epson/mobilephone/common/ble/BleWork;Landroid/bluetooth/BluetoothGattCharacteristic;[I)V

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static getInstace()Lcom/epson/mobilephone/common/ble/BleWork;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 669
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleWork$SingletonHolder;->access$2300()Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v0

    return-object v0
.end method

.method public static getSSID(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 708
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    .line 709
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-eqz p0, :cond_0

    const-string v0, "<unknown ssid>"

    .line 711
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string p0, ""

    :cond_1
    return-object p0
.end method

.method public static getScanAdvertisingPacketTimeout()I
    .locals 1

    const/16 v0, 0x2710

    return v0
.end method

.method private init()Z
    .locals 7

    .line 1062
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperInitDriver(Landroid/content/Context;I)I

    .line 1065
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->isBLESupported(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1070
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->getManager(Landroid/content/Context;)Landroid/bluetooth/BluetoothManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1072
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 1074
    :cond_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_2

    return v1

    .line 1080
    :cond_2
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_3

    .line 1081
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSelectItem:Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 1082
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_3

    const-string v0, "mDevice is null !!!"

    .line 1083
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    return v1

    .line 1089
    :cond_3
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSelectItem:Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getHardwareType()[B

    move-result-object v0

    .line 1092
    array-length v3, v0

    const/4 v4, 0x3

    const/4 v5, 0x1

    if-lt v3, v4, :cond_4

    const/4 v3, 0x4

    .line 1093
    new-array v3, v3, [B

    aput-byte v1, v3, v1

    aput-byte v1, v3, v5

    aget-byte v6, v0, v2

    aput-byte v6, v3, v2

    aget-byte v0, v0, v5

    aput-byte v0, v3, v4

    .line 1094
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    rsub-int v0, v0, 0x1000

    goto :goto_0

    :cond_4
    const/16 v0, 0xfff

    .line 1099
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "password = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 1100
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevicePassword:Ljava/lang/String;

    .line 1103
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_5

    iget v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    if-nez v0, :cond_5

    const-string v0, "connectGatt"

    .line 1105
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1106
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mGattcallback:Landroid/bluetooth/BluetoothGattCallback;

    invoke-virtual {v0, v2, v1, v3}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1107
    iput v5, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    .line 1108
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    if-nez v0, :cond_7

    .line 1109
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    goto :goto_1

    .line 1112
    :cond_5
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_6

    const-string v0, "re-connect and re-discover Services"

    .line 1114
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1115
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 1116
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mGattcallback:Landroid/bluetooth/BluetoothGattCallback;

    invoke-virtual {v0, v2, v1, v3}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1117
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->connect()Z

    .line 1118
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    .line 1119
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    if-nez v0, :cond_7

    .line 1120
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->INITIALIZE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    goto :goto_1

    :cond_6
    const-string v0, "  STATE_CONNECTING"

    .line 1123
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1124
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mGattcallback:Landroid/bluetooth/BluetoothGattCallback;

    invoke-virtual {v0, v2, v1, v3}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1125
    iput v5, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    .line 1126
    iget-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    if-nez v0, :cond_7

    .line 1127
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->DISCONNECTED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    :cond_7
    :goto_1
    return v5
.end method

.method public static isStartBleProcess(Landroid/content/Context;Ljava/lang/Boolean;)Z
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 741
    invoke-static {p0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->getManager(Landroid/content/Context;)Landroid/bluetooth/BluetoothManager;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 743
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    return v1

    .line 747
    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    .line 754
    :cond_1
    invoke-static {p0}, Lcom/epson/mobilephone/common/ble/util/BLEUtility;->isBLESupported(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 759
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 760
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    return v1

    .line 766
    :cond_3
    invoke-static {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->chechSSIDforBle(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_4

    return v1

    :cond_4
    const/4 p0, 0x1

    return p0
.end method

.method private notifySet(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothGattCharacteristic;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 848
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Notification set ERROR"

    .line 849
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_0
    const-string v0, "00002902-0000-1000-8000-00805f9b34fb"

    .line 853
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object p1

    .line 854
    sget-object v0, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_NOTIFICATION_VALUE:[B

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    const-string v0, "notify set"

    .line 855
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 856
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, " writeDescriptor ERROR"

    .line 857
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private sequenceError()V
    .locals 0

    .line 1376
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    return-void
.end method

.method private setNotifyCallback(Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V
    .locals 0

    .line 779
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mResposne:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-void
.end method

.method private setSSIDWork()V
    .locals 2

    .line 838
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/epson/mobilephone/common/ble/BleWork$2;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/BleWork$2;-><init>(Lcom/epson/mobilephone/common/ble/BleWork;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private showStatus(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method private startSetSSID()V
    .locals 7

    .line 1261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSequence = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " mSSID =  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1262
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mInSequence:Z

    const-string v1, ""

    const-string v2, ""

    .line 1267
    sget-object v3, Lcom/epson/mobilephone/common/ble/BleWork$5;->$SwitchMap$com$epson$mobilephone$common$ble$BleWork$jobSequence:[I

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v4}, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    const/4 v3, 0x0

    goto/16 :goto_1

    :pswitch_1
    const-string v1, "epsBluetoothOperationSetupEnd"

    const-string v2, "mibRebootNW  "

    .line 1351
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibRebootNWwithBLE(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I

    move-result v3

    goto/16 :goto_1

    :pswitch_2
    const-string v1, "epsWlanSecurityType"

    const-string v2, "mibSetSecurityType "

    .line 1292
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSIDPassword:Ljava/lang/String;

    if-nez v3, :cond_0

    const-string v3, ""

    iput-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSIDPassword:Ljava/lang/String;

    .line 1293
    :cond_0
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSIDPassword:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/16 v3, 0xff

    .line 1294
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MIB_SET_SECURITY_TYPE SecurityType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1296
    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v5, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    invoke-virtual {v4, v5, v3}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibSetSecurityType(Lcom/epson/mobilephone/common/escpr/MIBCommand;I)I

    move-result v4

    if-nez v3, :cond_2

    .line 1300
    sget-object v3, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_SEC_SKIP:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    :cond_2
    move v3, v4

    goto/16 :goto_1

    :pswitch_3
    const-string v1, "epsAdminMibAccessLogOutOperation"

    const-string v2, "mibSecLogout "

    .line 1339
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEngineID:[B

    if-nez v3, :cond_3

    const-string v3, "mEngineID"

    .line 1340
    invoke-static {v3}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1341
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    .line 1343
    :cond_3
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v5, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEngineID:[B

    invoke-virtual {v3, v4, v5}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibSecLogout(Lcom/epson/mobilephone/common/escpr/MIBCommand;[B)I

    move-result v3

    goto/16 :goto_1

    :pswitch_4
    const-string v1, "epsWlanCommonPassPhrase"

    const-string v2, "mibSecSetPass "

    .line 1321
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSIDPassword:Ljava/lang/String;

    .line 1322
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSSIDPassword  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSIDPassword:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1323
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "SSIDPassword is empty"

    .line 1324
    invoke-static {v4}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;)V

    .line 1326
    :cond_4
    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEngineID:[B

    if-nez v4, :cond_5

    const-string v4, "mEngineID"

    .line 1327
    invoke-static {v4}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1328
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    .line 1331
    :cond_5
    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v5, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v6, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEngineID:[B

    invoke-virtual {v4, v5, v6, v3}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibSecSetPassword(Lcom/epson/mobilephone/common/escpr/MIBCommand;[BLjava/lang/String;)I

    move-result v3

    goto :goto_1

    :pswitch_5
    const-string v1, "epsAdminMibAccessLoginOperation"

    const-string v2, "mibSecLogin "

    .line 1308
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEngineID:[B

    if-nez v3, :cond_6

    const-string v3, "mEngineID"

    .line 1309
    invoke-static {v3}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1310
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    .line 1313
    :cond_6
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v5, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEngineID:[B

    invoke-virtual {v3, v4, v5}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibSecLogin(Lcom/epson/mobilephone/common/escpr/MIBCommand;[B)I

    move-result v3

    goto :goto_1

    :pswitch_6
    const-string v1, "epsWlanConfSSID"

    const-string v2, "mibSetSSID"

    .line 1278
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSSID  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1279
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v0, "set SSID :  empty"

    .line 1281
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;)V

    return-void

    .line 1284
    :cond_7
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v5, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibSetSSID(Lcom/epson/mobilephone/common/escpr/MIBCommand;Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    :pswitch_7
    const-string v1, "epsGetMacAddress"

    const-string v2, "mibGetMacadress"

    .line 1271
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibGetMacadress(Lcom/epson/mobilephone/common/escpr/MIBCommand;)I

    move-result v3

    :goto_1
    if-eqz v3, :cond_8

    .line 1358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ret = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1360
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->sequenceError()V

    return-void

    .line 1364
    :cond_8
    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v2, v2, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    iget v2, v2, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->bufSize:I

    new-array v2, v2, [B

    .line 1365
    iget-object v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v3, v3, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    iget-object v3, v3, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->buf:[B

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v4, v4, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    iget v4, v4, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->bufSize:I

    invoke-static {v3, v0, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SNMP Request write :"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " mSequence   "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1368
    sget-object v0, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v1, "802a0003-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 1369
    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    .line 1371
    invoke-direct {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWork;->gattWriteCharacter(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public checkPassword()V
    .locals 5

    .line 1157
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 1158
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevicePassword:Ljava/lang/String;

    .line 1159
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1162
    :cond_0
    sget-object v1, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v2, "802a0001-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 1164
    invoke-static {v0}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v2

    const/16 v3, 0x12

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue(III)Z

    .line 1165
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Password write :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1167
    invoke-direct {p0, v1}, Lcom/epson/mobilephone/common/ble/BleWork;->gattWriteCharacter(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    :goto_0
    return-void
.end method

.method public checkVersion()V
    .locals 0

    .line 1208
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->characteristicRead()V

    return-void
.end method

.method public connect()V
    .locals 1

    const-string v0, "\u518d\u63a5\u7d9a"

    .line 830
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 831
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    const/4 v0, 0x0

    .line 832
    iput v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mRetry:I

    const-string v0, "init"

    .line 833
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 834
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->init()Z

    return-void
.end method

.method public disconnect()V
    .locals 1

    const/4 v0, 0x1

    .line 826
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWork;->disconnect(Z)V

    return-void
.end method

.method public disconnect(Z)V
    .locals 2

    .line 795
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSequence == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ":: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 796
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->DISCONNECTED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    :goto_0
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 798
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_2

    .line 799
    iget v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_1

    const-string v0, " !! mConnGatt disconnect"

    .line 800
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 801
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 803
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " !! mConnGatt close "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mStatus:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 805
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    if-eqz p1, :cond_2

    .line 807
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->gattClose()V

    :cond_2
    return-void
.end method

.method failedProcessing()V
    .locals 2

    .line 1384
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->disconnect()V

    .line 1385
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    if-eqz v0, :cond_0

    .line 1386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1387
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    invoke-interface {v0, v1}, Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public getDeviceMacAddress()Ljava/lang/String;
    .locals 3

    .line 199
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDeviceMacAddress:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    const-string v1, "\""

    const-string v2, ""

    .line 202
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSelectItem:Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/util/ScannedDevice;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSSID()Ljava/lang/String;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    return-object v0
.end method

.method public getSSIDList()V
    .locals 4

    const/4 v0, 0x1

    .line 1213
    iput-boolean v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mInSequence:Z

    .line 1214
    sget-object v1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_SSID_LIST:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 1215
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mEscprlib:Lcom/epson/mobilephone/common/escpr/EscprLib;

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-byte v3, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mIndex:B

    add-int/2addr v3, v0

    int-to-byte v0, v3

    iput-byte v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mIndex:B

    invoke-virtual {v1, v2, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->mibGetESSIDList(Lcom/epson/mobilephone/common/escpr/MIBCommand;B)I

    move-result v0

    if-eqz v0, :cond_0

    .line 1218
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mibGetESSIDList ret = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    return-void

    .line 1222
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v0, v0, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    iget v0, v0, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->bufSize:I

    new-array v0, v0, [B

    .line 1223
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    iget-object v1, v1, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->buf:[B

    iget-object v2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mMibCommand:Lcom/epson/mobilephone/common/escpr/MIBCommand;

    iget-object v2, v2, Lcom/epson/mobilephone/common/escpr/MIBCommand;->commandBlock:Lcom/epson/mobilephone/common/escpr/MIBDataBlock;

    iget v2, v2, Lcom/epson/mobilephone/common/escpr/MIBDataBlock;->bufSize:I

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1225
    sget-object v1, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v2, "802a0003-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 1227
    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    .line 1230
    invoke-direct {p0, v1}, Lcom/epson/mobilephone/common/ble/BleWork;->gattWriteCharacter(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    return-void
.end method

.method public getSSIDPassword()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSIDPassword:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public declared-synchronized getSecurityType()S
    .locals 1

    monitor-enter p0

    .line 184
    :try_start_0
    iget-short v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSecurityType:S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSsidLis()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSsidLis:Ljava/util/List;

    return-object v0
.end method

.method public init(Landroid/content/Context;Lcom/epson/mobilephone/common/ble/util/ScannedDevice;)Z
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/epson/mobilephone/common/ble/util/ScannedDevice;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 1142
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    .line 1143
    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSelectItem:Lcom/epson/mobilephone/common/ble/util/ScannedDevice;

    const/4 p1, 0x0

    .line 1144
    iput p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mRetry:I

    .line 1145
    sget-object p2, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 1146
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWork;->setSecurityType(S)V

    const/4 p1, 0x1

    .line 1147
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    .line 1149
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    .line 1150
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    const-string p1, "init"

    .line 1152
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1153
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->init()Z

    move-result p1

    return p1
.end method

.method public resetSequence()V
    .locals 2

    .line 685
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->UNINITIALIZED:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    const/4 v0, 0x0

    .line 687
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 689
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSIDPassword:Ljava/lang/String;

    .line 691
    iget-object v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSsidLis:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    .line 693
    iput-boolean v1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mReconnect:Z

    .line 695
    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    return-void
.end method

.method public setRetryFinish()V
    .locals 1

    const/4 v0, 0x3

    .line 172
    iput v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mRetry:I

    return-void
.end method

.method setSSID(Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V
    .locals 0

    .line 783
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 784
    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 785
    sget-object p1, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->MIB_GET_MACADDRESS:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 786
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->setSSIDWork()V

    return-void
.end method

.method public setSSID(Ljava/lang/String;)V
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSID:Ljava/lang/String;

    return-void
.end method

.method public setSSIDPassword(Ljava/lang/String;)V
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSSIDPassword:Ljava/lang/String;

    return-void
.end method

.method public setSSIDType()V
    .locals 1

    .line 1251
    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->CHECK_SSID:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 1252
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mConnGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    const-string v0, "mConnGatt  NULL !"

    .line 1253
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 1256
    :cond_0
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ble/BleWork;->checkSSID()V

    return-void
.end method

.method public declared-synchronized setSecurityType(S)V
    .locals 0

    monitor-enter p0

    .line 180
    :try_start_0
    iput-short p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSecurityType:S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method setSequenceCallback(Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V
    .locals 0

    .line 774
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequenceCallbackSuccess:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    .line 775
    iput-object p2, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mSequenceCallbackFailed:Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;

    return-void
.end method

.method public setTimeOut(J)V
    .locals 0

    .line 195
    iput-wide p1, p0, Lcom/epson/mobilephone/common/ble/BleWork;->mTimeOut:J

    return-void
.end method

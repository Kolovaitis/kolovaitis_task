.class Lcom/epson/mobilephone/common/ble/ProbePrinter$1;
.super Ljava/lang/Object;
.source "ProbePrinter.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/ProbePrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/ProbePrinter;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$1;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    .line 169
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 170
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$1;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p2}, Lepson/print/service/IEpsonService$Stub;->asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$002(Lcom/epson/mobilephone/common/ble/ProbePrinter;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    .line 174
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$1;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$000(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 176
    :try_start_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$1;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p1}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$000(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$1;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-static {p2}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$100(Lcom/epson/mobilephone/common/ble/ProbePrinter;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object p2

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->registerCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 178
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ComponentName "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 185
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/ProbePrinter$1;->this$0:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->access$002(Lcom/epson/mobilephone/common/ble/ProbePrinter;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    return-void
.end method

.class public Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;
.super Landroid/os/AsyncTask;
.source "BleWork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/ble/BleWork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NotifySetThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field stop:Z

.field success:Z

.field final synthetic this$0:Lcom/epson/mobilephone/common/ble/BleWork;


# direct methods
.method private constructor <init>(Lcom/epson/mobilephone/common/ble/BleWork;)V
    .locals 0

    .line 866
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    .line 867
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 p1, 0x0

    .line 863
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->stop:Z

    .line 864
    iput-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->success:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/epson/mobilephone/common/ble/BleWork;Lcom/epson/mobilephone/common/ble/BleWork$1;)V
    .locals 0

    .line 862
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;-><init>(Lcom/epson/mobilephone/common/ble/BleWork;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 905
    sget-object p1, Lcom/epson/mobilephone/common/ble/util/BLEUuid;->GattCharsMap:Ljava/util/Map;

    const-string v0, "802a0007-4ef4-4e59-b573-2bed4a4ac158"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 906
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2600(Lcom/epson/mobilephone/common/ble/BleWork;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 907
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 912
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x7530

    cmp-long p1, v2, v4

    if-lez p1, :cond_1

    const-string p1, " set failed! "

    .line 914
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 915
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_1
    const-wide/16 v2, 0x64

    .line 918
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 921
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 922
    iget-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->stop:Z

    if-nez p1, :cond_2

    goto :goto_1

    .line 926
    :cond_2
    :goto_0
    iget-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->stop:Z

    if-eqz p1, :cond_0

    :goto_1
    const-string p1, " set ended "

    .line 928
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 930
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 931
    iget-boolean p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->success:Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 862
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    .line 936
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 937
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 939
    :try_start_0
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x1f4

    invoke-virtual {p1, v0, v1}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 941
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 945
    :goto_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    sget-object v0, Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;->INITIALIZE:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    iput-object v0, p1, Lcom/epson/mobilephone/common/ble/BleWork;->mSequence:Lcom/epson/mobilephone/common/ble/BleWork$jobSequence;

    .line 946
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->checkPassword()V

    goto :goto_1

    :cond_0
    const-string p1, "setNotify"

    .line 948
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 949
    iget-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ble/BleWork;->failedProcessing()V

    :goto_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 862
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 872
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 873
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    const-wide/16 v0, 0x1f4

    .line 876
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 879
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 882
    :goto_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;->this$0:Lcom/epson/mobilephone/common/ble/BleWork;

    new-instance v1, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread$1;-><init>(Lcom/epson/mobilephone/common/ble/BleWork$NotifySetThread;)V

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/ble/BleWork;->access$2700(Lcom/epson/mobilephone/common/ble/BleWork;Lcom/epson/mobilephone/common/ble/util/BLEUtility$BleWorkCallback;)V

    return-void
.end method

.class Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;
.super Ljava/lang/Object;
.source "BleWorkActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->call(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field postCount:I

.field final synthetic this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;)V
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 245
    iput p1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->postCount:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 249
    iget v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->postCount:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-lt v0, v3, :cond_2

    const-string v0, "postCount >= 2 "

    .line 250
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->interruptSearch()V

    .line 252
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 254
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iput-object v2, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;->call(Ljava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x1

    if-ne v0, v3, :cond_4

    .line 264
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v4, v4, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v4, v4, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v4, v4, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v4}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$100(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWork;

    move-result-object v4

    invoke-virtual {v4}, Lcom/epson/mobilephone/common/ble/BleWork;->getDeviceMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->setMacAddress(Ljava/lang/String;)Lcom/epson/mobilephone/common/ble/ProbePrinter;

    .line 265
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    iget-object v4, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v4, v4, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v4, v4, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v4, v4, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v4}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->setBleCallback(Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;)Lcom/epson/mobilephone/common/ble/ProbePrinter;

    .line 267
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->search()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 269
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mProbePrinter:Lcom/epson/mobilephone/common/ble/ProbePrinter;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ble/ProbePrinter;->interruptSearch()V

    .line 271
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 272
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 273
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    iput-object v2, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 275
    :cond_3
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 276
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;->call(Ljava/lang/Object;)V

    .line 280
    :cond_4
    :goto_0
    iget v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->postCount:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->postCount:I

    .line 281
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$600(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Lcom/epson/mobilephone/common/ble/BleWorkActivity$CallbackWork;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "postDelayed  postCount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->postCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " delay "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->postCount:I

    if-ne v1, v3, :cond_5

    const/16 v1, 0x2710

    goto :goto_1

    :cond_5
    const v1, 0x30d40

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->this$3:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1;->this$2:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1;->this$1:Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;

    iget-object v0, v0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2;->this$0:Lcom/epson/mobilephone/common/ble/BleWorkActivity;

    invoke-static {v0}, Lcom/epson/mobilephone/common/ble/BleWorkActivity;->access$400(Lcom/epson/mobilephone/common/ble/BleWorkActivity;)Landroid/os/Handler;

    move-result-object v0

    iget v1, p0, Lcom/epson/mobilephone/common/ble/BleWorkActivity$2$1$1$1;->postCount:I

    if-ne v1, v3, :cond_6

    const-wide/16 v1, 0x2710

    goto :goto_2

    :cond_6
    const-wide/32 v1, 0x30d40

    :goto_2
    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_7
    return-void
.end method

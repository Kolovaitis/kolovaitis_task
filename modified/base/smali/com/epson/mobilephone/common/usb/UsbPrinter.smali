.class public Lcom/epson/mobilephone/common/usb/UsbPrinter;
.super Ljava/lang/Object;
.source "UsbPrinter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/usb/UsbPrinter$RequestType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UsbPrinter"

.field protected static final USBLP_BULK_TIMEOUT:I = 0x1388

.field protected static final USBLP_CTL_TIMEOUT:I = 0x1388

.field protected static final USBLP_REQ_GET_ID:I = 0x0

.field protected static final USBLP_REQ_GET_STATUS:I = 0x1

.field protected static final USBLP_REQ_RESET:I = 0x2

.field public static final USB_OP_FAIL:I = -0x1

.field public static final USB_OP_SUCCESS:I


# instance fields
.field private endPointIn:Landroid/hardware/usb/UsbEndpoint;

.field private endPointOut:Landroid/hardware/usb/UsbEndpoint;

.field private startTime:J

.field private usbDevice:Landroid/hardware/usb/UsbDevice;

.field private usbInterface:Landroid/hardware/usb/UsbInterface;

.field private usbManager:Landroid/hardware/usb/UsbManager;

.field private usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;


# direct methods
.method public constructor <init>(Landroid/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbInterface;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V
    .locals 2

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 68
    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbManager:Landroid/hardware/usb/UsbManager;

    .line 73
    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbDevice:Landroid/hardware/usb/UsbDevice;

    .line 78
    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 83
    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    .line 88
    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->endPointOut:Landroid/hardware/usb/UsbEndpoint;

    .line 93
    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->endPointIn:Landroid/hardware/usb/UsbEndpoint;

    const-wide/16 v0, 0x0

    .line 98
    iput-wide v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->startTime:J

    .line 107
    iput-object p1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbManager:Landroid/hardware/usb/UsbManager;

    .line 108
    iput-object p2, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbDevice:Landroid/hardware/usb/UsbDevice;

    .line 109
    iput-object p3, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 110
    iput-object p4, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->endPointOut:Landroid/hardware/usb/UsbEndpoint;

    .line 111
    iput-object p5, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->endPointIn:Landroid/hardware/usb/UsbEndpoint;

    return-void
.end method

.method private getElapsedTime()J
    .locals 4

    .line 220
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->startTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private setStartTime()V
    .locals 2

    .line 216
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->startTime:J

    return-void
.end method


# virtual methods
.method public declared-synchronized closePort()V
    .locals 1

    monitor-enter p0

    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 295
    monitor-exit p0

    return-void

    .line 298
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    const/4 v0, 0x0

    .line 299
    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 301
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x1

    if-ne p1, p0, :cond_1

    return v1

    .line 328
    :cond_1
    instance-of v2, p1, Lcom/epson/mobilephone/common/usb/UsbPrinter;

    if-nez v2, :cond_2

    return v0

    .line 329
    :cond_2
    check-cast p1, Lcom/epson/mobilephone/common/usb/UsbPrinter;

    .line 330
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object p1

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result p1

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v2

    if-ne p1, v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method public declared-synchronized getDeviceIdString([BI)J
    .locals 11

    monitor-enter p0

    .line 232
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, -0x1

    if-nez v0, :cond_0

    .line 233
    monitor-exit p0

    return-wide v1

    .line 236
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 238
    iget-object v3, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v4, 0xa1

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 242
    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v0

    shl-int/lit8 v7, v0, 0x8

    const/16 v10, 0x1388

    move-object v8, p1

    move v9, p2

    .line 238
    invoke-virtual/range {v3 .. v10}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p2

    if-lez p2, :cond_1

    const-string v0, "EPSON"

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeviceIdString = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/String;

    add-int/lit8 v3, p2, -0x2

    const/4 v4, 0x2

    invoke-direct {v2, p1, v4, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_1
    iget-object p1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long p1, p2

    .line 252
    monitor-exit p0

    return-wide p1

    .line 255
    :cond_2
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getDeviceNumbers()I
    .locals 3

    .line 132
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v0

    .line 133
    div-int/lit16 v1, v0, 0x3e8

    mul-int/lit16 v2, v1, 0x3e8

    sub-int/2addr v0, v2

    shl-int/lit8 v1, v1, 0x10

    add-int/2addr v1, v0

    return v1
.end method

.method public getUsbDevice()Landroid/hardware/usb/UsbDevice;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbDevice:Landroid/hardware/usb/UsbDevice;

    return-object v0
.end method

.method public getUsbPrinter()Landroid/hardware/usb/UsbDeviceConnection;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    return-object v0
.end method

.method public declared-synchronized openPort()I
    .locals 3

    monitor-enter p0

    const/4 v0, -0x1

    .line 146
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbManager:Landroid/hardware/usb/UsbManager;

    iget-object v2, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    .line 147
    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 148
    monitor-exit p0

    return v0

    .line 154
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->getFileDescriptor()I

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 151
    :catch_0
    monitor-exit p0

    return v0
.end method

.method public declared-synchronized readPort([BI)J
    .locals 5

    monitor-enter p0

    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, -0x1

    if-nez v0, :cond_0

    .line 167
    monitor-exit p0

    return-wide v1

    .line 170
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    invoke-direct {p0}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->setStartTime()V

    const-string v0, "UsbPrinter"

    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bulkTransfer length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->endPointIn:Landroid/hardware/usb/UsbEndpoint;

    const/16 v2, 0x1388

    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p1

    const-string p2, "UsbPrinter"

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bulkTransfer readByte="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-gez p1, :cond_1

    .line 175
    invoke-direct {p0}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getElapsedTime()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide v2, 0x40b1940000000000L    # 4500.0

    cmpl-double p2, v0, v2

    if-ltz p2, :cond_1

    const/4 p1, 0x0

    const-string p2, "UsbPrinter"

    const-string v0, "bulkTransfer timeout occurred"

    .line 177
    invoke-static {p2, v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_1
    iget-object p2, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {p2, v0}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long p1, p1

    .line 180
    monitor-exit p0

    return-wide p1

    .line 183
    :cond_2
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized softReset()I
    .locals 10

    monitor-enter p0

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    .line 266
    monitor-exit p0

    return v1

    .line 269
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    iget-object v2, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v3, 0x23

    const/4 v4, 0x2

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 275
    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x1388

    .line 271
    invoke-virtual/range {v2 .. v9}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v0

    .line 280
    iget-object v2, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v2, v3}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ltz v0, :cond_1

    const/4 v0, 0x0

    .line 283
    monitor-exit p0

    return v0

    .line 287
    :cond_1
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deviceName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " : endPointOut = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->endPointOut:Landroid/hardware/usb/UsbEndpoint;

    .line 309
    invoke-virtual {v1}, Landroid/hardware/usb/UsbEndpoint;->getAddress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " : endPointIn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->endPointIn:Landroid/hardware/usb/UsbEndpoint;

    .line 310
    invoke-virtual {v1}, Landroid/hardware/usb/UsbEndpoint;->getAddress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 311
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 312
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " : productName = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 313
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " : SerialNo = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public declared-synchronized writePort([BI)J
    .locals 5

    monitor-enter p0

    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, -0x1

    if-nez v0, :cond_0

    .line 196
    monitor-exit p0

    return-wide v1

    .line 199
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    invoke-direct {p0}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->setStartTime()V

    const-string v0, "UsbPrinter"

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bulkTransfer length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->endPointOut:Landroid/hardware/usb/UsbEndpoint;

    const/16 v2, 0x1388

    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p1

    const-string p2, "UsbPrinter"

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bulkTransfer writtenByte="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-gez p1, :cond_1

    .line 204
    invoke-direct {p0}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getElapsedTime()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide v2, 0x40b1940000000000L    # 4500.0

    cmpl-double p2, v0, v2

    if-ltz p2, :cond_1

    const/4 p1, 0x0

    const-string p2, "UsbPrinter"

    const-string v0, "bulkTransfer timeout occurred"

    .line 206
    invoke-static {p2, v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_1
    iget-object p2, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbPrinter:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrinter;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {p2, v0}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long p1, p1

    .line 209
    monitor-exit p0

    return-wide p1

    .line 212
    :cond_2
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

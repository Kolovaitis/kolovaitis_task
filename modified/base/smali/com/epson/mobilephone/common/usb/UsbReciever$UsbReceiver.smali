.class public Lcom/epson/mobilephone/common/usb/UsbReciever$UsbReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UsbReciever.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/mobilephone/common/usb/UsbReciever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UsbReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .line 124
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    .line 126
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Tag"

    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    .line 127
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "device"

    .line 129
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p2

    check-cast p2, Landroid/hardware/usb/UsbDevice;

    if-eqz p2, :cond_0

    .line 132
    invoke-static {p1}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/usb/UsbPrintDriver;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->deletePrinterPermmited(Landroid/hardware/usb/UsbDevice;)V

    :cond_0
    return-void
.end method

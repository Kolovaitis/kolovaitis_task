.class public Lcom/epson/mobilephone/common/usb/UsbReciever;
.super Landroid/app/Activity;
.source "UsbReciever.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/usb/UsbReciever$UsbReceiver;,
        Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;
    }
.end annotation


# static fields
.field public static final ACTION_USB_PERMISSION:Ljava/lang/String; = "com.epson.otg.USB_PERMISSION"

.field private static permissionGrantedcallback:Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public getPermission(Landroid/hardware/usb/UsbDevice;Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;)Z
    .locals 3

    .line 148
    invoke-static {p0}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/usb/UsbPrintDriver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->getUsbManager()Landroid/hardware/usb/UsbManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.epson.otg.USB_PERMISSION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 153
    invoke-static {p0, v2, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 154
    invoke-static {p0}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/usb/UsbPrintDriver;

    move-result-object v2

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->getUsbManager()Landroid/hardware/usb/UsbManager;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Landroid/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V

    .line 157
    sput-object p2, Lcom/epson/mobilephone/common/usb/UsbReciever;->permissionGrantedcallback:Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;

    goto :goto_0

    :cond_0
    const-string p1, "EPSON"

    const-string v1, "HAS PERMISSION"

    .line 160
    invoke-static {p1, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    .line 162
    invoke-interface {p2, p0, p1}, Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;->permissionGranted(Landroid/content/Context;Z)V

    :cond_1
    :goto_0
    return v0
.end method

.method protected onResume()V
    .locals 4

    .line 45
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 47
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/usb/UsbReciever;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    .line 50
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    const-string v1, "Tag"

    const-string v2, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    .line 51
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "device"

    .line 53
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbDevice;

    if-eqz v0, :cond_4

    .line 55
    invoke-static {p0}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/usb/UsbPrintDriver;

    move-result-object v1

    .line 57
    sget v2, Lcom/epson/mobilephone/common/PrintingLib/define/Constants;->EPSON_VENDERID:I

    invoke-virtual {v1, v3, v2}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->findPrinters(ZI)Ljava/util/ArrayList;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/mobilephone/common/usb/UsbPrinter;

    .line 60
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v2

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v3

    if-ne v2, v3, :cond_0

    const-string v1, "Tag"

    const-string v2, "EPSON Printer ATTACHED"

    .line 62
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    new-instance v1, Lcom/epson/mobilephone/common/usb/UsbReciever$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/usb/UsbReciever$1;-><init>(Lcom/epson/mobilephone/common/usb/UsbReciever;)V

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/usb/UsbReciever;->getPermission(Landroid/hardware/usb/UsbDevice;Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;)Z

    goto :goto_1

    :cond_1
    const-string v2, "com.epson.otg.USB_PERMISSION"

    .line 95
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 96
    monitor-enter p0

    :try_start_0
    const-string v1, "device"

    .line 97
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    const-string v2, "permission"

    .line 99
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    const-string v0, "EPSON"

    const-string v1, "HAS PERMISSION"

    .line 100
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    sget-object v0, Lcom/epson/mobilephone/common/usb/UsbReciever;->permissionGrantedcallback:Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;

    if-eqz v0, :cond_3

    .line 102
    sget-object v0, Lcom/epson/mobilephone/common/usb/UsbReciever;->permissionGrantedcallback:Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;->permissionGranted(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_2
    const-string v0, "EPSON"

    const-string v1, "permission denied for device"

    .line 105
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    sget-object v0, Lcom/epson/mobilephone/common/usb/UsbReciever;->permissionGrantedcallback:Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;

    if-eqz v0, :cond_3

    .line 107
    sget-object v0, Lcom/epson/mobilephone/common/usb/UsbReciever;->permissionGrantedcallback:Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;

    invoke-interface {v0, p0, v3}, Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;->permissionGranted(Landroid/content/Context;Z)V

    :cond_3
    :goto_0
    const/4 v0, 0x0

    .line 112
    sput-object v0, Lcom/epson/mobilephone/common/usb/UsbReciever;->permissionGrantedcallback:Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;

    .line 113
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :goto_1
    return-void
.end method

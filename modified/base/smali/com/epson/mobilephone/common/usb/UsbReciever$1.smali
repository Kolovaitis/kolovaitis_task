.class Lcom/epson/mobilephone/common/usb/UsbReciever$1;
.super Ljava/lang/Object;
.source "UsbReciever.java"

# interfaces
.implements Lcom/epson/mobilephone/common/usb/UsbReciever$UsbPermissionReceiverCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/mobilephone/common/usb/UsbReciever;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/mobilephone/common/usb/UsbReciever;


# direct methods
.method constructor <init>(Lcom/epson/mobilephone/common/usb/UsbReciever;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/epson/mobilephone/common/usb/UsbReciever$1;->this$0:Lcom/epson/mobilephone/common/usb/UsbReciever;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public permissionGranted(Landroid/content/Context;Z)V
    .locals 2

    if-eqz p2, :cond_0

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p2

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    .line 70
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object p2

    .line 73
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    .line 74
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.category.LAUNCHER"

    .line 75
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    :try_start_0
    iget-object p1, p0, Lcom/epson/mobilephone/common/usb/UsbReciever$1;->this$0:Lcom/epson/mobilephone/common/usb/UsbReciever;

    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/usb/UsbReciever;->startActivity(Landroid/content/Intent;)V

    const-string p1, "EPSON"

    const-string p2, "UsbReciever ACTION_MAIN"

    .line 79
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 82
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    const-string p1, "EPSON"

    const-string p2, "UsbReciever finish()"

    .line 86
    invoke-static {p1, p2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object p1, p0, Lcom/epson/mobilephone/common/usb/UsbReciever$1;->this$0:Lcom/epson/mobilephone/common/usb/UsbReciever;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/usb/UsbReciever;->finish()V

    return-void
.end method

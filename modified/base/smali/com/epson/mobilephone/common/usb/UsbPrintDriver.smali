.class public Lcom/epson/mobilephone/common/usb/UsbPrintDriver;
.super Ljava/lang/Object;
.source "UsbPrintDriver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UsbPrintDriver"

.field private static instance:Lcom/epson/mobilephone/common/usb/UsbPrintDriver;


# instance fields
.field private context:Landroid/content/Context;

.field private usbManager:Landroid/hardware/usb/UsbManager;

.field private usbPrintersPemmited:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/mobilephone/common/usb/UsbPrinter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 38
    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    const-string v0, "usb"

    .line 46
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    iput-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbManager:Landroid/hardware/usb/UsbManager;

    .line 47
    iput-object p1, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->context:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/epson/mobilephone/common/usb/UsbPrintDriver;
    .locals 2

    const-class v0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;

    monitor-enter v0

    .line 57
    :try_start_0
    sget-object v1, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->instance:Lcom/epson/mobilephone/common/usb/UsbPrintDriver;

    if-nez v1, :cond_0

    .line 58
    new-instance v1, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->instance:Lcom/epson/mobilephone/common/usb/UsbPrintDriver;

    .line 60
    :cond_0
    sget-object p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->instance:Lcom/epson/mobilephone/common/usb/UsbPrintDriver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public closePort(I)V
    .locals 2

    const-string v0, "UsbPrintDriver"

    const-string v1, "Call closePort"

    .line 320
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    monitor-enter p0

    .line 323
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->findPrinterOpened(I)Lcom/epson/mobilephone/common/usb/UsbPrinter;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 326
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->closePort()V

    .line 328
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public deletePrinterPermmited(Landroid/hardware/usb/UsbDevice;)V
    .locals 4

    .line 89
    monitor-enter p0

    if-nez p1, :cond_0

    .line 91
    :try_start_0
    monitor-exit p0

    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 95
    monitor-exit p0

    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/mobilephone/common/usb/UsbPrinter;

    .line 99
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v2

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v3

    if-ne v2, v3, :cond_2

    const-string p1, "Tag"

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EPSON delete usbPrinter = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->closePort()V

    .line 102
    iget-object p1, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "Tag"

    const-string v0, "EPSON delete success"

    .line 103
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public findDevice(I[I)I
    .locals 4

    const-string v0, "UsbPrintDriver"

    const-string v1, "Call findDevice"

    .line 190
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    monitor-enter p0

    const/4 v0, 0x1

    if-nez p1, :cond_0

    .line 194
    :try_start_0
    sget v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants;->EPSON_VENDERID:I

    invoke-virtual {p0, v0, v1}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->findPrinters(ZI)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :cond_0
    :goto_0
    if-ltz p1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 198
    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/mobilephone/common/usb/UsbPrinter;

    const/4 v2, 0x0

    .line 200
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v3

    aput v3, p2, v2

    .line 201
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v2

    aput v2, p2, v0

    const/4 v2, 0x2

    .line 202
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getDeviceNumbers()I

    move-result v1

    aput v1, p2, v2

    add-int/2addr p1, v0

    .line 204
    monitor-exit p0

    return p1

    .line 206
    :cond_1
    monitor-exit p0

    const/4 p1, -0x1

    return p1

    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public findPrinterOpened(I)Lcom/epson/mobilephone/common/usb/UsbPrinter;
    .locals 3

    .line 244
    monitor-enter p0

    const/4 v0, 0x0

    .line 245
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 246
    iget-object v1, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/mobilephone/common/usb/UsbPrinter;

    .line 248
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbPrinter()Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 254
    :cond_0
    invoke-virtual {v2}, Landroid/hardware/usb/UsbDeviceConnection;->getFileDescriptor()I

    move-result v2

    if-ne p1, v2, :cond_1

    .line 255
    monitor-exit p0

    return-object v1

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 258
    :cond_2
    monitor-exit p0

    const/4 p1, 0x0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public declared-synchronized findPrinters(ZI)Ljava/util/ArrayList;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI)",
            "Ljava/util/ArrayList<",
            "Lcom/epson/mobilephone/common/usb/UsbPrinter;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    monitor-enter p0

    .line 121
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 123
    iget-object v2, v1, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v2

    .line 124
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 126
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 127
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 128
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/usb/UsbDevice;

    if-eqz p1, :cond_2

    .line 130
    iget-object v5, v1, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v5, v4}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    move/from16 v11, p2

    goto :goto_0

    .line 132
    :cond_2
    :goto_1
    invoke-virtual {v4}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v5

    move/from16 v11, p2

    if-ne v5, v11, :cond_0

    .line 135
    invoke-virtual {v4}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v12

    const/4 v14, 0x0

    :goto_2
    if-ge v14, v12, :cond_0

    .line 137
    invoke-virtual {v4, v14}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v8

    const/4 v5, 0x7

    .line 141
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    move-result v6

    if-ne v5, v6, :cond_6

    .line 142
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getInterfaceSubclass()I

    move-result v5

    const/4 v6, 0x1

    if-ne v6, v5, :cond_6

    .line 143
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I

    move-result v5

    const/4 v6, 0x2

    if-ne v6, v5, :cond_6

    .line 149
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v5

    const/4 v7, 0x0

    move-object v9, v7

    move-object v10, v9

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v5, :cond_5

    .line 151
    invoke-virtual {v8, v7}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v15

    if-nez v9, :cond_3

    .line 153
    invoke-virtual {v15}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v13

    if-ne v6, v13, :cond_3

    .line 154
    invoke-virtual {v15}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v13

    if-nez v13, :cond_3

    move-object v9, v15

    goto :goto_4

    :cond_3
    if-nez v10, :cond_4

    .line 159
    invoke-virtual {v15}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v13

    if-ne v6, v13, :cond_4

    const/16 v13, 0x80

    .line 160
    invoke-virtual {v15}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v6

    if-ne v13, v6, :cond_4

    move-object v10, v15

    :cond_4
    :goto_4
    add-int/lit8 v7, v7, 0x1

    const/4 v6, 0x2

    goto :goto_3

    :cond_5
    if-eqz v9, :cond_6

    if-eqz v10, :cond_6

    .line 168
    new-instance v13, Lcom/epson/mobilephone/common/usb/UsbPrinter;

    iget-object v6, v1, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbManager:Landroid/hardware/usb/UsbManager;

    move-object v5, v13

    move-object v7, v4

    invoke-direct/range {v5 .. v10}, Lcom/epson/mobilephone/common/usb/UsbPrinter;-><init>(Landroid/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbInterface;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V

    const-string v5, "EPSON"

    .line 169
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found Printer "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 179
    :cond_7
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDeviceIdString(I[BI)J
    .locals 2

    const-string v0, "UsbPrintDriver"

    const-string v1, "Call getDeviceIdString"

    .line 292
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    monitor-enter p0

    .line 295
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->findPrinterOpened(I)Lcom/epson/mobilephone/common/usb/UsbPrinter;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 298
    invoke-virtual {p1, p2, p3}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getDeviceIdString([BI)J

    move-result-wide p1

    monitor-exit p0

    return-wide p1

    .line 300
    :cond_0
    monitor-exit p0

    const-wide/16 p1, -0x1

    return-wide p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getUsbManager()Landroid/hardware/usb/UsbManager;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbManager:Landroid/hardware/usb/UsbManager;

    return-object v0
.end method

.method public isExistPermitedDevice()Z
    .locals 3

    .line 76
    monitor-enter p0

    const/4 v0, 0x3

    .line 77
    :try_start_0
    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->findDevice(I[I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    .line 78
    monitor-exit p0

    return v0

    .line 80
    :cond_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    .line 81
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public openPort([I)I
    .locals 5

    const-string v0, "UsbPrintDriver"

    const-string v1, "Call openPort"

    .line 218
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 221
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 222
    iget-object v2, p0, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->usbPrintersPemmited:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/mobilephone/common/usb/UsbPrinter;

    .line 224
    aget v3, p1, v0

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    aget v3, p1, v3

    .line 225
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getUsbDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v3, 0x2

    aget v3, p1, v3

    .line 226
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->getDeviceNumbers()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 228
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->openPort()I

    move-result p1

    monitor-exit p0

    return p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    :cond_1
    monitor-exit p0

    const/4 p1, -0x1

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public readPort(I[BI)J
    .locals 2

    const-string v0, "UsbPrintDriver"

    const-string v1, "Call readPort"

    .line 264
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    monitor-enter p0

    .line 267
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->findPrinterOpened(I)Lcom/epson/mobilephone/common/usb/UsbPrinter;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 270
    invoke-virtual {p1, p2, p3}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->readPort([BI)J

    move-result-wide p1

    monitor-exit p0

    return-wide p1

    .line 272
    :cond_0
    monitor-exit p0

    const-wide/16 p1, -0x1

    return-wide p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public softReset(I)I
    .locals 2

    const-string v0, "UsbPrintDriver"

    const-string v1, "Call softReset"

    .line 306
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    monitor-enter p0

    .line 309
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->findPrinterOpened(I)Lcom/epson/mobilephone/common/usb/UsbPrinter;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 312
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->softReset()I

    move-result p1

    monitor-exit p0

    return p1

    .line 314
    :cond_0
    monitor-exit p0

    const/4 p1, -0x1

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public writePort(I[BI)J
    .locals 2

    const-string v0, "UsbPrintDriver"

    const-string v1, "Call writePort"

    .line 278
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    monitor-enter p0

    .line 281
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/epson/mobilephone/common/usb/UsbPrintDriver;->findPrinterOpened(I)Lcom/epson/mobilephone/common/usb/UsbPrinter;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 284
    invoke-virtual {p1, p2, p3}, Lcom/epson/mobilephone/common/usb/UsbPrinter;->writePort([BI)J

    move-result-wide p1

    monitor-exit p0

    return-wide p1

    .line 286
    :cond_0
    monitor-exit p0

    const-wide/16 p1, -0x1

    return-wide p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.class public Lcom/epson/mobilephone/common/ReviewInvitationDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ReviewInvitationDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;
    }
.end annotation


# static fields
.field private static final GOOGLE_PLAY_STORE_ID:Ljava/lang/String; = "market://details?id=epson.print"


# instance fields
.field private mOnClickListener:Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/mobilephone/common/ReviewInvitationDialog;Z)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->doCallback(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/mobilephone/common/ReviewInvitationDialog;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->startReview()V

    return-void
.end method

.method private doCallback(Z)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->mOnClickListener:Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;

    invoke-interface {v0, p1}, Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;->invitationDialogClicked(Z)V

    return-void
.end method

.method public static newInstance()Lcom/epson/mobilephone/common/ReviewInvitationDialog;
    .locals 1

    .line 28
    new-instance v0, Lcom/epson/mobilephone/common/ReviewInvitationDialog;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;-><init>()V

    return-object v0
.end method

.method private startReview()V
    .locals 4

    .line 57
    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 63
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 65
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "market://details?id=epson.print"

    .line 66
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 67
    invoke-virtual {v2, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 68
    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/content/Context;)V

    .line 80
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;

    iput-object v0, p0, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->mOnClickListener:Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 82
    :catch_0
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement ReviewInvitationDialog.OnClickListener"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 34
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0e044e

    .line 35
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/epson/mobilephone/common/ReviewInvitationDialog$2;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ReviewInvitationDialog$2;-><init>(Lcom/epson/mobilephone/common/ReviewInvitationDialog;)V

    const v2, 0x7f0e044f

    .line 36
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/epson/mobilephone/common/ReviewInvitationDialog$1;

    invoke-direct {v1, p0}, Lcom/epson/mobilephone/common/ReviewInvitationDialog$1;-><init>(Lcom/epson/mobilephone/common/ReviewInvitationDialog;)V

    const v2, 0x7f0e044d

    .line 44
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v0, 0x0

    .line 51
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->setCancelable(Z)V

    .line 53
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public onDetach()V
    .locals 1

    .line 88
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDetach()V

    const/4 v0, 0x0

    .line 89
    iput-object v0, p0, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->mOnClickListener:Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;

    return-void
.end method

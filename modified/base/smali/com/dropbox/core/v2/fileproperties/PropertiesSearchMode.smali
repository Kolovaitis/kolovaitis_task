.class public final Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;
.super Ljava/lang/Object;
.source "PropertiesSearchMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Serializer;,
        Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;
    }
.end annotation


# static fields
.field public static final OTHER:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;


# instance fields
.field private _tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

.field private fieldNameValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 58
    new-instance v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    invoke-direct {v0}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;->OTHER:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    invoke-direct {v0, v1}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->withTag(Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;)Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    move-result-object v0

    sput-object v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->OTHER:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;)Ljava/lang/String;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->fieldNameValue:Ljava/lang/String;

    return-object p0
.end method

.method public static fieldName(Ljava/lang/String;)Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;
    .locals 2

    if-eqz p0, :cond_0

    .line 142
    new-instance v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    invoke-direct {v0}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;->FIELD_NAME:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    invoke-direct {v0, v1, p0}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->withTagAndFieldName(Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;Ljava/lang/String;)Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    move-result-object p0

    return-object p0

    .line 140
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Value is null"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private withTag(Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;)Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;
    .locals 1

    .line 75
    new-instance v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    invoke-direct {v0}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;-><init>()V

    .line 76
    iput-object p1, v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    return-object v0
.end method

.method private withTagAndFieldName(Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;Ljava/lang/String;)Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;
    .locals 1

    .line 90
    new-instance v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    invoke-direct {v0}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;-><init>()V

    .line 91
    iput-object p1, v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    .line 92
    iput-object p2, v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->fieldNameValue:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 190
    :cond_1
    instance-of v2, p1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    if-eqz v2, :cond_5

    .line 191
    check-cast p1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;

    .line 192
    iget-object v2, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    iget-object v3, p1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    if-eq v2, v3, :cond_2

    return v1

    .line 195
    :cond_2
    sget-object v2, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$1;->$SwitchMap$com$dropbox$core$v2$fileproperties$PropertiesSearchMode$Tag:[I

    iget-object v3, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    invoke-virtual {v3}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    return v1

    :pswitch_0
    return v0

    .line 197
    :pswitch_1
    iget-object v2, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->fieldNameValue:Ljava/lang/String;

    iget-object p1, p1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->fieldNameValue:Ljava/lang/String;

    if-eq v2, p1, :cond_4

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :cond_4
    :goto_0
    return v0

    :cond_5
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getFieldNameValue()Ljava/lang/String;
    .locals 3

    .line 156
    iget-object v0, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    sget-object v1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;->FIELD_NAME:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    if-ne v0, v1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->fieldNameValue:Ljava/lang/String;

    return-object v0

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid tag: required Tag.FIELD_NAME, but was Tag."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    invoke-virtual {v2}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    .line 175
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->fieldNameValue:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isFieldName()Z
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    sget-object v1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;->FIELD_NAME:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOther()Z
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    sget-object v1, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;->OTHER:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public tag()Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode;->_tag:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Tag;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 211
    sget-object v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Serializer;->INSTANCE:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Serializer;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Serializer;->serialize(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toStringMultiline()Ljava/lang/String;
    .locals 2

    .line 223
    sget-object v0, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Serializer;->INSTANCE:Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Serializer;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/core/v2/fileproperties/PropertiesSearchMode$Serializer;->serialize(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

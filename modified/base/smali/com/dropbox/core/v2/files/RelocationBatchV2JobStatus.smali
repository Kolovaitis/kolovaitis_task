.class public final Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;
.super Ljava/lang/Object;
.source "RelocationBatchV2JobStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Serializer;,
        Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;
    }
.end annotation


# static fields
.field public static final IN_PROGRESS:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;


# instance fields
.field private _tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

.field private completeValue:Lcom/dropbox/core/v2/files/RelocationBatchV2Result;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 50
    new-instance v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;->IN_PROGRESS:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    invoke-direct {v0, v1}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->withTag(Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;)Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    move-result-object v0

    sput-object v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->IN_PROGRESS:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;)Lcom/dropbox/core/v2/files/RelocationBatchV2Result;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->completeValue:Lcom/dropbox/core/v2/files/RelocationBatchV2Result;

    return-object p0
.end method

.method public static complete(Lcom/dropbox/core/v2/files/RelocationBatchV2Result;)Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;
    .locals 2

    if-eqz p0, :cond_0

    .line 150
    new-instance v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;->COMPLETE:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    invoke-direct {v0, v1, p0}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->withTagAndComplete(Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;Lcom/dropbox/core/v2/files/RelocationBatchV2Result;)Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    move-result-object p0

    return-object p0

    .line 148
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Value is null"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private withTag(Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;)Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;
    .locals 1

    .line 71
    new-instance v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;-><init>()V

    .line 72
    iput-object p1, v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    return-object v0
.end method

.method private withTagAndComplete(Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;Lcom/dropbox/core/v2/files/RelocationBatchV2Result;)Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;
    .locals 1

    .line 90
    new-instance v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;-><init>()V

    .line 91
    iput-object p1, v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    .line 92
    iput-object p2, v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->completeValue:Lcom/dropbox/core/v2/files/RelocationBatchV2Result;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 188
    :cond_1
    instance-of v2, p1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    if-eqz v2, :cond_5

    .line 189
    check-cast p1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;

    .line 190
    iget-object v2, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    iget-object v3, p1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    if-eq v2, v3, :cond_2

    return v1

    .line 193
    :cond_2
    sget-object v2, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$1;->$SwitchMap$com$dropbox$core$v2$files$RelocationBatchV2JobStatus$Tag:[I

    iget-object v3, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    invoke-virtual {v3}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    return v1

    .line 197
    :pswitch_0
    iget-object v2, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->completeValue:Lcom/dropbox/core/v2/files/RelocationBatchV2Result;

    iget-object p1, p1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->completeValue:Lcom/dropbox/core/v2/files/RelocationBatchV2Result;

    if-eq v2, p1, :cond_4

    invoke-virtual {v2, p1}, Lcom/dropbox/core/v2/files/RelocationBatchV2Result;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :cond_4
    :goto_0
    return v0

    :pswitch_1
    return v0

    :cond_5
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getCompleteValue()Lcom/dropbox/core/v2/files/RelocationBatchV2Result;
    .locals 3

    .line 164
    iget-object v0, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;->COMPLETE:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    if-ne v0, v1, :cond_0

    .line 167
    iget-object v0, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->completeValue:Lcom/dropbox/core/v2/files/RelocationBatchV2Result;

    return-object v0

    .line 165
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid tag: required Tag.COMPLETE, but was Tag."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    invoke-virtual {v2}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    .line 172
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->completeValue:Lcom/dropbox/core/v2/files/RelocationBatchV2Result;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 176
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v1, v0

    return v1
.end method

.method public isComplete()Z
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;->COMPLETE:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInProgress()Z
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;->IN_PROGRESS:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public tag()Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus;->_tag:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Tag;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 209
    sget-object v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Serializer;->INSTANCE:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Serializer;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Serializer;->serialize(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toStringMultiline()Ljava/lang/String;
    .locals 2

    .line 221
    sget-object v0, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Serializer;->INSTANCE:Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Serializer;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/core/v2/files/RelocationBatchV2JobStatus$Serializer;->serialize(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

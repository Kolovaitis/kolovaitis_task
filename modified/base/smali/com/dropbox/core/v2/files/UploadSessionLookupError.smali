.class public final Lcom/dropbox/core/v2/files/UploadSessionLookupError;
.super Ljava/lang/Object;
.source "UploadSessionLookupError.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dropbox/core/v2/files/UploadSessionLookupError$Serializer;,
        Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;
    }
.end annotation


# static fields
.field public static final CLOSED:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

.field public static final NOT_CLOSED:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

.field public static final NOT_FOUND:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

.field public static final OTHER:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

.field public static final TOO_LARGE:Lcom/dropbox/core/v2/files/UploadSessionLookupError;


# instance fields
.field private _tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

.field private incorrectOffsetValue:Lcom/dropbox/core/v2/files/UploadSessionOffsetError;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 78
    new-instance v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->NOT_FOUND:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    invoke-direct {v0, v1}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->withTag(Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    move-result-object v0

    sput-object v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->NOT_FOUND:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    .line 83
    new-instance v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->CLOSED:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    invoke-direct {v0, v1}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->withTag(Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    move-result-object v0

    sput-object v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->CLOSED:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    .line 87
    new-instance v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->NOT_CLOSED:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    invoke-direct {v0, v1}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->withTag(Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    move-result-object v0

    sput-object v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->NOT_CLOSED:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    .line 92
    new-instance v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->TOO_LARGE:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    invoke-direct {v0, v1}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->withTag(Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    move-result-object v0

    sput-object v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->TOO_LARGE:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    .line 100
    new-instance v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->OTHER:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    invoke-direct {v0, v1}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->withTag(Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    move-result-object v0

    sput-object v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->OTHER:Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/dropbox/core/v2/files/UploadSessionLookupError;)Lcom/dropbox/core/v2/files/UploadSessionOffsetError;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->incorrectOffsetValue:Lcom/dropbox/core/v2/files/UploadSessionOffsetError;

    return-object p0
.end method

.method public static incorrectOffset(Lcom/dropbox/core/v2/files/UploadSessionOffsetError;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;
    .locals 2

    if-eqz p0, :cond_0

    .line 201
    new-instance v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->INCORRECT_OFFSET:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    invoke-direct {v0, v1, p0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->withTagAndIncorrectOffset(Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;Lcom/dropbox/core/v2/files/UploadSessionOffsetError;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    move-result-object p0

    return-object p0

    .line 199
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Value is null"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private withTag(Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;
    .locals 1

    .line 117
    new-instance v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;-><init>()V

    .line 118
    iput-object p1, v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    return-object v0
.end method

.method private withTagAndIncorrectOffset(Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;Lcom/dropbox/core/v2/files/UploadSessionOffsetError;)Lcom/dropbox/core/v2/files/UploadSessionLookupError;
    .locals 1

    .line 135
    new-instance v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadSessionLookupError;-><init>()V

    .line 136
    iput-object p1, v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    .line 137
    iput-object p2, v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->incorrectOffsetValue:Lcom/dropbox/core/v2/files/UploadSessionOffsetError;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 286
    :cond_1
    instance-of v2, p1, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    if-eqz v2, :cond_5

    .line 287
    check-cast p1, Lcom/dropbox/core/v2/files/UploadSessionLookupError;

    .line 288
    iget-object v2, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    iget-object v3, p1, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    if-eq v2, v3, :cond_2

    return v1

    .line 291
    :cond_2
    sget-object v2, Lcom/dropbox/core/v2/files/UploadSessionLookupError$1;->$SwitchMap$com$dropbox$core$v2$files$UploadSessionLookupError$Tag:[I

    iget-object v3, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    invoke-virtual {v3}, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    return v1

    :pswitch_0
    return v0

    :pswitch_1
    return v0

    :pswitch_2
    return v0

    :pswitch_3
    return v0

    .line 295
    :pswitch_4
    iget-object v2, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->incorrectOffsetValue:Lcom/dropbox/core/v2/files/UploadSessionOffsetError;

    iget-object p1, p1, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->incorrectOffsetValue:Lcom/dropbox/core/v2/files/UploadSessionOffsetError;

    if-eq v2, p1, :cond_4

    invoke-virtual {v2, p1}, Lcom/dropbox/core/v2/files/UploadSessionOffsetError;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :cond_4
    :goto_0
    return v0

    :pswitch_5
    return v0

    :cond_5
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getIncorrectOffsetValue()Lcom/dropbox/core/v2/files/UploadSessionOffsetError;
    .locals 3

    .line 219
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->INCORRECT_OFFSET:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    if-ne v0, v1, :cond_0

    .line 222
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->incorrectOffsetValue:Lcom/dropbox/core/v2/files/UploadSessionOffsetError;

    return-object v0

    .line 220
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid tag: required Tag.INCORRECT_OFFSET, but was Tag."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    invoke-virtual {v2}, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    .line 271
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->incorrectOffsetValue:Lcom/dropbox/core/v2/files/UploadSessionOffsetError;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isClosed()Z
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->CLOSED:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isIncorrectOffset()Z
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->INCORRECT_OFFSET:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNotClosed()Z
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->NOT_CLOSED:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNotFound()Z
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->NOT_FOUND:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOther()Z
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->OTHER:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTooLarge()Z
    .locals 2

    .line 255
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;->TOO_LARGE:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public tag()Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadSessionLookupError;->_tag:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Tag;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 315
    sget-object v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Serializer;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Serializer;->serialize(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toStringMultiline()Ljava/lang/String;
    .locals 2

    .line 327
    sget-object v0, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/files/UploadSessionLookupError$Serializer;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/core/v2/files/UploadSessionLookupError$Serializer;->serialize(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

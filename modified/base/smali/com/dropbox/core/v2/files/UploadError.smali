.class public final Lcom/dropbox/core/v2/files/UploadError;
.super Ljava/lang/Object;
.source "UploadError.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dropbox/core/v2/files/UploadError$Serializer;,
        Lcom/dropbox/core/v2/files/UploadError$Tag;
    }
.end annotation


# static fields
.field public static final OTHER:Lcom/dropbox/core/v2/files/UploadError;


# instance fields
.field private _tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

.field private pathValue:Lcom/dropbox/core/v2/files/UploadWriteFailed;

.field private propertiesErrorValue:Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 64
    new-instance v0, Lcom/dropbox/core/v2/files/UploadError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadError$Tag;->OTHER:Lcom/dropbox/core/v2/files/UploadError$Tag;

    invoke-direct {v0, v1}, Lcom/dropbox/core/v2/files/UploadError;->withTag(Lcom/dropbox/core/v2/files/UploadError$Tag;)Lcom/dropbox/core/v2/files/UploadError;

    move-result-object v0

    sput-object v0, Lcom/dropbox/core/v2/files/UploadError;->OTHER:Lcom/dropbox/core/v2/files/UploadError;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/dropbox/core/v2/files/UploadError;)Lcom/dropbox/core/v2/files/UploadWriteFailed;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/dropbox/core/v2/files/UploadError;->pathValue:Lcom/dropbox/core/v2/files/UploadWriteFailed;

    return-object p0
.end method

.method static synthetic access$100(Lcom/dropbox/core/v2/files/UploadError;)Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/dropbox/core/v2/files/UploadError;->propertiesErrorValue:Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;

    return-object p0
.end method

.method public static path(Lcom/dropbox/core/v2/files/UploadWriteFailed;)Lcom/dropbox/core/v2/files/UploadError;
    .locals 2

    if-eqz p0, :cond_0

    .line 165
    new-instance v0, Lcom/dropbox/core/v2/files/UploadError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadError$Tag;->PATH:Lcom/dropbox/core/v2/files/UploadError$Tag;

    invoke-direct {v0, v1, p0}, Lcom/dropbox/core/v2/files/UploadError;->withTagAndPath(Lcom/dropbox/core/v2/files/UploadError$Tag;Lcom/dropbox/core/v2/files/UploadWriteFailed;)Lcom/dropbox/core/v2/files/UploadError;

    move-result-object p0

    return-object p0

    .line 163
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Value is null"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static propertiesError(Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;)Lcom/dropbox/core/v2/files/UploadError;
    .locals 2

    if-eqz p0, :cond_0

    .line 214
    new-instance v0, Lcom/dropbox/core/v2/files/UploadError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadError;-><init>()V

    sget-object v1, Lcom/dropbox/core/v2/files/UploadError$Tag;->PROPERTIES_ERROR:Lcom/dropbox/core/v2/files/UploadError$Tag;

    invoke-direct {v0, v1, p0}, Lcom/dropbox/core/v2/files/UploadError;->withTagAndPropertiesError(Lcom/dropbox/core/v2/files/UploadError$Tag;Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;)Lcom/dropbox/core/v2/files/UploadError;

    move-result-object p0

    return-object p0

    .line 212
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Value is null"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private withTag(Lcom/dropbox/core/v2/files/UploadError$Tag;)Lcom/dropbox/core/v2/files/UploadError;
    .locals 1

    .line 82
    new-instance v0, Lcom/dropbox/core/v2/files/UploadError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadError;-><init>()V

    .line 83
    iput-object p1, v0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    return-object v0
.end method

.method private withTagAndPath(Lcom/dropbox/core/v2/files/UploadError$Tag;Lcom/dropbox/core/v2/files/UploadWriteFailed;)Lcom/dropbox/core/v2/files/UploadError;
    .locals 1

    .line 97
    new-instance v0, Lcom/dropbox/core/v2/files/UploadError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadError;-><init>()V

    .line 98
    iput-object p1, v0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    .line 99
    iput-object p2, v0, Lcom/dropbox/core/v2/files/UploadError;->pathValue:Lcom/dropbox/core/v2/files/UploadWriteFailed;

    return-object v0
.end method

.method private withTagAndPropertiesError(Lcom/dropbox/core/v2/files/UploadError$Tag;Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;)Lcom/dropbox/core/v2/files/UploadError;
    .locals 1

    .line 113
    new-instance v0, Lcom/dropbox/core/v2/files/UploadError;

    invoke-direct {v0}, Lcom/dropbox/core/v2/files/UploadError;-><init>()V

    .line 114
    iput-object p1, v0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    .line 115
    iput-object p2, v0, Lcom/dropbox/core/v2/files/UploadError;->propertiesErrorValue:Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 265
    :cond_1
    instance-of v2, p1, Lcom/dropbox/core/v2/files/UploadError;

    if-eqz v2, :cond_7

    .line 266
    check-cast p1, Lcom/dropbox/core/v2/files/UploadError;

    .line 267
    iget-object v2, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    iget-object v3, p1, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    if-eq v2, v3, :cond_2

    return v1

    .line 270
    :cond_2
    sget-object v2, Lcom/dropbox/core/v2/files/UploadError$1;->$SwitchMap$com$dropbox$core$v2$files$UploadError$Tag:[I

    iget-object v3, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    invoke-virtual {v3}, Lcom/dropbox/core/v2/files/UploadError$Tag;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    return v1

    :pswitch_0
    return v0

    .line 274
    :pswitch_1
    iget-object v2, p0, Lcom/dropbox/core/v2/files/UploadError;->propertiesErrorValue:Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;

    iget-object p1, p1, Lcom/dropbox/core/v2/files/UploadError;->propertiesErrorValue:Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;

    if-eq v2, p1, :cond_4

    invoke-virtual {v2, p1}, Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :cond_4
    :goto_0
    return v0

    .line 272
    :pswitch_2
    iget-object v2, p0, Lcom/dropbox/core/v2/files/UploadError;->pathValue:Lcom/dropbox/core/v2/files/UploadWriteFailed;

    iget-object p1, p1, Lcom/dropbox/core/v2/files/UploadError;->pathValue:Lcom/dropbox/core/v2/files/UploadWriteFailed;

    if-eq v2, p1, :cond_6

    invoke-virtual {v2, p1}, Lcom/dropbox/core/v2/files/UploadWriteFailed;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    :cond_6
    :goto_1
    return v0

    :cond_7
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getPathValue()Lcom/dropbox/core/v2/files/UploadWriteFailed;
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadError$Tag;->PATH:Lcom/dropbox/core/v2/files/UploadError$Tag;

    if-ne v0, v1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadError;->pathValue:Lcom/dropbox/core/v2/files/UploadWriteFailed;

    return-object v0

    .line 180
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid tag: required Tag.PATH, but was Tag."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    invoke-virtual {v2}, Lcom/dropbox/core/v2/files/UploadError$Tag;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPropertiesErrorValue()Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;
    .locals 3

    .line 230
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadError$Tag;->PROPERTIES_ERROR:Lcom/dropbox/core/v2/files/UploadError$Tag;

    if-ne v0, v1, :cond_0

    .line 233
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadError;->propertiesErrorValue:Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;

    return-object v0

    .line 231
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid tag: required Tag.PROPERTIES_ERROR, but was Tag."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    invoke-virtual {v2}, Lcom/dropbox/core/v2/files/UploadError$Tag;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    .line 249
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/dropbox/core/v2/files/UploadError;->pathValue:Lcom/dropbox/core/v2/files/UploadWriteFailed;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/dropbox/core/v2/files/UploadError;->propertiesErrorValue:Lcom/dropbox/core/v2/fileproperties/InvalidPropertyGroupError;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isOther()Z
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadError$Tag;->OTHER:Lcom/dropbox/core/v2/files/UploadError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPath()Z
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadError$Tag;->PATH:Lcom/dropbox/core/v2/files/UploadError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPropertiesError()Z
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    sget-object v1, Lcom/dropbox/core/v2/files/UploadError$Tag;->PROPERTIES_ERROR:Lcom/dropbox/core/v2/files/UploadError$Tag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public tag()Lcom/dropbox/core/v2/files/UploadError$Tag;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/dropbox/core/v2/files/UploadError;->_tag:Lcom/dropbox/core/v2/files/UploadError$Tag;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 288
    sget-object v0, Lcom/dropbox/core/v2/files/UploadError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/files/UploadError$Serializer;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/core/v2/files/UploadError$Serializer;->serialize(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toStringMultiline()Ljava/lang/String;
    .locals 2

    .line 300
    sget-object v0, Lcom/dropbox/core/v2/files/UploadError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/files/UploadError$Serializer;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/core/v2/files/UploadError$Serializer;->serialize(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
